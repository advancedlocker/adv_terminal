// DialogComSettings.cpp : implementation file
//

#include "stdafx.h"
#include "CePrint.h"
#include "DialogComSettings.h"

#define XON		0x11
#define XOFF	0x13

// CDialogComSettings dialog

IMPLEMENT_DYNAMIC(CDialogComSettings, CDialog)

CDialogComSettings::CDialogComSettings(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogComSettings::IDD, pParent)
{

}

CDialogComSettings::~CDialogComSettings()
{
}

void CDialogComSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_COMBO_STOP, m_comboStop);
	DDX_Control(pDX, IDC_COMBO_PARITY, m_comboParity);
	DDX_Control(pDX, IDC_COMBO_FC, m_comboFc);
	DDX_Control(pDX, IDC_COMBO_DATA, m_comboData);
	DDX_Control(pDX, IDC_COMBO_BAUD, m_comboBaud);
}


BEGIN_MESSAGE_MAP(CDialogComSettings, CDialog)
END_MESSAGE_MAP()


// CDialogComSettings message handlers
BOOL CDialogComSettings::OnInitDialog() 
{
	DWORD i;
	CDialog::OnInitDialog();
	
	// init combo box
	m_comboBaud.ResetContent();
	for (i=0;i<NBAUDRATE;i++)
	{
		m_comboBaud.AddString(defBaudrate[i]);
	}
	m_comboBaud.SetCurSel(0);

	m_comboData.ResetContent();
	for (i=0;i<NDATABYTE;i++)
	{
		m_comboData.AddString(defDataByte[i]);
	}
	m_comboData.SetCurSel(0);

	m_comboFc.ResetContent();
	for (i=0;i<NFCBYTE;i++)
	{
		m_comboFc.AddString(defFcByte[i]);
	}
	m_comboFc.SetCurSel(0);

	m_comboParity.ResetContent();
	for (i=0;i<NPARITY;i++)
	{
		m_comboParity.AddString(defParity[i]);
	}
	m_comboParity.SetCurSel(0);

	m_comboStop.ResetContent();
	for (i=0;i<NSTOPBYTE;i++)
	{
		m_comboStop.AddString(defStopByte[i]);
	}
	m_comboStop.SetCurSel(0);


	SetDefaultValue();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDialogComSettings::SetDefaultValue() 
{
	DWORD i;
	CString strApp;
	CString strAppDef;
	DWORD	dwApp;
	DWORD	dwAppDef;

	// Set initial baud rate value
	for(i=0;i<NBAUDRATE;i++)
	{
		// check value 
		dwAppDef = m_dcb.BaudRate;
		m_comboBaud.GetLBText(i, strApp);
		dwApp = _wtoi((LPCTSTR)strApp);
		
		// found... break
		if (dwApp == dwAppDef)
			break;
	}
	// Set value
	if (i != NBAUDRATE)
		m_comboBaud.SetCurSel(i);


	// Set initial number of bits
	for(i=0;i<NDATABYTE;i++)
	{
		// check value 
		dwAppDef = m_dcb.ByteSize;
		m_comboData.GetLBText(i, strApp);
		dwApp = _wtoi((LPCTSTR)strApp);
		
		// found... break
		if (dwApp == dwAppDef)
			break;
	}

	// Set value
	if (i != NBAUDRATE)
		m_comboData.SetCurSel(i);

	// handshake
	if (m_dcb.fOutxCtsFlow)
	{
		if (m_dcb.fDtrControl == DTR_CONTROL_HANDSHAKE)
			m_comboFc.SetCurSel(1);
		else
			m_comboFc.SetCurSel(0);
	}
	else
		m_comboFc.SetCurSel(2);

	// Parity
	if (m_dcb.Parity == EVENPARITY)
		m_comboParity.SetCurSel(0);
	else if (m_dcb.Parity == ODDPARITY)
		m_comboParity.SetCurSel(1);
	else if (m_dcb.Parity == NOPARITY)
		m_comboParity.SetCurSel(2);

	// Stop Bit
	if (m_dcb.StopBits == ONESTOPBIT)
		m_comboStop.SetCurSel(0);
	else if (m_dcb.StopBits == TWOSTOPBITS)
		m_comboStop.SetCurSel(1);

	UpdateData(FALSE);
}

void CDialogComSettings::GetDefaultValue() 
{
	DWORD	dwApp;

	UpdateData(TRUE);

	// Baudrate
	m_dcb.BaudRate = _wtoi((LPCTSTR)defBaudrate[m_comboBaud.GetCurSel()]);

	// Bits numbers
	m_dcb.ByteSize = _wtoi((LPCTSTR)defDataByte[m_comboData.GetCurSel()]);

	// Parity
	if (m_comboParity.GetCurSel() == 0)
		m_dcb.Parity = EVENPARITY;
	else if (m_comboParity.GetCurSel() == 1)
		m_dcb.Parity = ODDPARITY;
	else if (m_comboParity.GetCurSel() == 2)
		m_dcb.Parity = NOPARITY;

	// Stop Bits
	if (m_comboStop.GetCurSel() == 0)
		m_dcb.StopBits = ONESTOPBIT;
	else if (m_comboStop.GetCurSel() == 1)
		m_dcb.StopBits = TWOSTOPBITS;

	dwApp = m_comboFc.GetCurSel();

	// andshake
	if (dwApp == 0)		// SOFTWARE
	{
		m_dcb.fOutxCtsFlow = 0x00000000;
		m_dcb.fOutxDsrFlow = 0x00000000;
		m_dcb.fDtrControl = 0x00000001;
		m_dcb.fOutX = 0x00000001;
		m_dcb.fInX = 0x00000001;
		m_dcb.fRtsControl = 0x00000001;
		m_dcb.XonChar = XON;
		m_dcb.XoffChar = XOFF;
	}
	if (dwApp == 1)		// HARDWARE
	{
		m_dcb.fOutxCtsFlow = 0x00000001;
		m_dcb.fOutxDsrFlow = 0x00000001;
		m_dcb.fDtrControl = 0x00000002;
		m_dcb.fOutX = 0x00000000;
		m_dcb.fInX = 0x00000000;
		m_dcb.fRtsControl = 0x00000002;
		m_dcb.XonChar = 0x00;
		m_dcb.XoffChar = 0x00;
	}
	if (dwApp == 2)		// NONE
	{
		m_dcb.fOutxCtsFlow = 0x00000000;
		m_dcb.fOutxDsrFlow = 0x00000000;
		m_dcb.fDtrControl = 0x00000001;
		m_dcb.fOutX = 0x00000000;
		m_dcb.fInX = 0x00000000;
		m_dcb.fRtsControl = 0x00000001;
		m_dcb.XonChar = 0x00;
		m_dcb.XoffChar = 0x00;
	}
}

void CDialogComSettings::OnOK() 
{
	// TODO: Add extra validation here
	//
	GetDefaultValue();
	CDialog::OnOK();
}
