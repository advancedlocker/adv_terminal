// CePrintDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CePrint.h"
#include "CePrintDlg.h"

#include "CeComTimeout.h"
#include "DialogComSettings.h"

#define HS_NONE			0
#define HS_HW			1
#define HS_SW			2

// Define for Bar Code Systems
#define	UPC_A			65
#define UPC_E			66
#define EAN13			67
#define EAN8			68
#define CODE39			69
#define ITF				70
#define CODABAR			71
#define CODE93			72
#define CODE128			73
#define CODE32			90

// #define _DIRECT_ACCESS_MODE_		// if enable you can get information by printer
									// build at application level the printer command
 
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CCePrintDlg dialog
CCePrintDlg::CCePrintDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCePrintDlg::IDD, pParent)
	, m_strPortName(_T(""))
	, m_strGSI(_T(""))
	, m_strPrnAnswerHex(_T(""))
	, m_strLibError(_T(""))
	, m_strSysError(_T(""))
	, m_strHRComCfg(_T(""))
	, m_strPrnFilePath(_T(""))
	, m_strPrnName(_T(""))
	, m_strEditBoxGSI(_T(""))
	, m_strEditUsbStatus(_T(""))
	, m_strPrnFilePathUsb(_T(""))
	, m_strEditGetFwRelUsb(_T(""))
	, m_strUnicodeText(_T(""))
	, m_uiCodePage(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hLib = NULL;
	m_hComObj = NULL;
	m_hUSBObj = NULL;
	m_strPortName = _T("COM1");
	m_strGSI = _T("");
	m_strHRComCfg.LoadString(IDS_STR_COMPORT_NOT_INIT);

	m_lpfnCeOpenCom = NULL;
	m_lpfnCeCloseCom = NULL;
	m_lpfnCeWriteCom = NULL;
	m_lpfnCeWriteReadCom = NULL;
	m_lpfnCeGetGSI = NULL;

	m_lpfnCeGetStrComConfig = NULL;

	m_lpfnCeGetDefComCfg = NULL;
	m_lpfnCeGetComCfg = NULL;
	m_lpfnCeSetComCfg = NULL;

	m_lpfnCeGetDefComTimeout = NULL;
	m_lpfnCeGetComTimeout = NULL;
	m_lpfnCeSetComTimeout = NULL;

	m_lpfnCeGethPortCom = NULL;
	m_lpfnCeSetComCfgHR = NULL;

	m_lpfnCeGetSts = NULL;
	m_lpfnCeGetFWRel = NULL;

	m_lpfnCeInitUSBLayer = NULL;

	m_strPrnName = _T("CUSTOM VKP80 II CH");

	m_lpfnCeSpoolBufferToPrn = NULL;
	m_lpfnCeInitUSBLayer = NULL;

	m_lpfnCeOpenUSBDev = NULL;
//	m_lpfnCeCloseUSBDev = NULL;

	m_lpfnCeWriteUSBDev = NULL;
	m_lpfnCeWriteReadUSBDev = NULL;
}

void CCePrintDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_INITCOM, m_bttInitCom);
	DDX_Control(pDX, IDCANCEL, m_bttClose);
	DDX_Control(pDX, IDOK, m_bttOK);
	DDX_Text(pDX, IDC_EDIT_COM_NAME, m_strPortName);
	DDX_Text(pDX, IDC_EDIT_GSI, m_strGSI);
	DDX_Text(pDX, IDC_EDIT_PRN_ANSWER, m_strPrnAnswerHex);
	DDX_Text(pDX, IDC_EDIT_LIB_ERR, m_strLibError);
	DDX_Text(pDX, IDC_EDIT_SYS_ERR, m_strSysError);
	DDX_Text(pDX, IDC_EDIT_STR_COM_CFG, m_strHRComCfg);
	DDX_Control(pDX, IDC_BUTTON_GET_STATUS, m_bttGetStatus);
	DDX_Control(pDX, IDC_BUTTON_GET_DEF_COM_CFG, m_bttGetDefComCfg);
	DDX_Control(pDX, IDC_BUTTON_GET_CURR_COM_CFG, m_bttGetComCfg);
	DDX_Control(pDX, IDC_BUTTON_SET_DEF_COM_CFG3, m_bttSetDefComCfg);
	DDX_Control(pDX, IDC_BUTTON_GET_DEF_TIMEOUT, m_bttGetDefTimeout);
	DDX_Control(pDX, IDC_BUTTON_GET_CUR_TIMEOUT, m_bttGetTimeout);
	DDX_Control(pDX, IDC_BUTTON_SET_CUR_TIMEOUT, m_bttSetTimeout);
	DDX_Text(pDX, IDC_EDIT_PATH_FILE, m_strPrnFilePath);
	DDX_Text(pDX, IDC_EDIT_PRN_NAME, m_strPrnName);
	DDX_Control(pDX, IDC_BUTTON_NEW_SET_COM, m_bttSetNewComCfg);
	DDX_Text(pDX, IDC_EDIT_GSI_USB, m_strEditBoxGSI);
	DDX_Text(pDX, IDC_EDIT_STATUS_USB, m_strEditUsbStatus);
	DDX_Text(pDX, IDC_EDIT_PATH_FILE_USB, m_strPrnFilePathUsb);
	DDX_Text(pDX, IDC_EDIT_FW_REL_USB, m_strEditGetFwRelUsb);
	DDX_Text(pDX, IDC_EDIT_UNICODE_STRING, m_strUnicodeText);
	DDX_Text(pDX, IDC_EDIT_SEL_CODE_PAGE, m_uiCodePage);
}

BEGIN_MESSAGE_MAP(CCePrintDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_INITCOM, &CCePrintDlg::OnBnClickedButtonInitcom)
	ON_BN_CLICKED(IDC_BUTTON_GET_GSI, &CCePrintDlg::OnBnClickedButtonGetGsi)
	ON_BN_CLICKED(IDCANCEL, &CCePrintDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDOK, &CCePrintDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUTTON_GET_DEF_TIMEOUT, &CCePrintDlg::OnBnClickedButtonGetDefTimeout)
	ON_BN_CLICKED(IDC_BUTTON_GET_CUR_TIMEOUT, &CCePrintDlg::OnBnClickedButtonGetCurTimeout)
	ON_BN_CLICKED(IDC_BUTTON_SET_CUR_TIMEOUT, &CCePrintDlg::OnBnClickedButtonSetCurTimeout)
	ON_BN_CLICKED(IDC_BUTTON_GET_DEF_COM_CFG, &CCePrintDlg::OnBnClickedButtonGetDefComCfg)
	ON_BN_CLICKED(IDC_BUTTON_GET_CURR_COM_CFG, &CCePrintDlg::OnBnClickedButtonGetCurrComCfg)
	ON_BN_CLICKED(IDC_BUTTON_SET_DEF_COM_CFG3, &CCePrintDlg::OnBnClickedButtonSetDefComCfg3)
	ON_BN_CLICKED(IDC_BUTTON_GET_STATUS, &CCePrintDlg::OnBnClickedButtonGetStatus)
	ON_BN_CLICKED(IDC_BUTTON_GET_COM_HND, &CCePrintDlg::OnBnClickedButtonGetCOMhnd)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE_COM, &CCePrintDlg::OnBnClickedButtonCloseCom)
	ON_BN_CLICKED(IDC_BUTTON_WRITE_PRN_FILE, &CCePrintDlg::OnBnClickedButtonWritePrnFile)
	ON_BN_CLICKED(IDC_BUTTON_WRITE_PRN_FILE2, &CCePrintDlg::OnBnClickedButtonWritePrnFile2)
	ON_BN_CLICKED(IDC_BUTTON_SPOOL_FILE, &CCePrintDlg::OnBnClickedButtonSpoolFile)
	ON_BN_CLICKED(IDC_BUTTON_TEST, &CCePrintDlg::OnBnClickedButtonTest)
	ON_BN_CLICKED(IDC_BUTTON_NEW_SET_COM, &CCePrintDlg::OnBnClickedButtonNewSetCom)
	ON_BN_CLICKED(IDC_BUTTON_INITUSB, &CCePrintDlg::OnBnClickedButtonInitusb)
	ON_BN_CLICKED(IDC_BUTTON_DEINITUSB, &CCePrintDlg::OnBnClickedButtonDeinitusb)
	ON_BN_CLICKED(IDC_BUTTON_OPENUSBDEV, &CCePrintDlg::OnBnClickedButtonOpenusbdev)
	ON_BN_CLICKED(IDC_BUTTON_GET_GSI_USB, &CCePrintDlg::OnBnClickedButtonGetGsiUsb)
	ON_BN_CLICKED(IDC_BUTTON_GET_STATUS_USB, &CCePrintDlg::OnBnClickedButtonGetStatusUsb)
	ON_BN_CLICKED(IDC_BUTTON_WRITE_PRN_FILE_USB, &CCePrintDlg::OnBnClickedButtonWritePrnFileUsb)
	ON_BN_CLICKED(IDC_BUTTON_OPEN_PRN_FILE_USB, &CCePrintDlg::OnBnClickedButtonOpenPrnFileUsb)
	ON_BN_CLICKED(IDC_BUTTON_SPOOL_FILE_USB, &CCePrintDlg::OnBnClickedButtonSpoolFileUsb)
	ON_BN_CLICKED(IDC_BUTTON_GET_FW_REL_USB, &CCePrintDlg::OnBnClickedButtonGetFwRelUsb)
	ON_BN_CLICKED(IDC_BUTTON_CFG_COM, &CCePrintDlg::OnBnClickedButtonCfgCom)
	ON_BN_CLICKED(IDC_BUTTON_GET_FWREL, &CCePrintDlg::OnBnClickedButtonGetFwrel)
	ON_BN_CLICKED(IDC_BUTTON_TEST_Q1, &CCePrintDlg::OnBnClickedButtonTestQ1)
	ON_BN_CLICKED(IDC_BUTTON_TEST_TG2460, &CCePrintDlg::OnBnClickedButtonTestTg2460)
	ON_BN_CLICKED(IDC_BUTTON_TEST_TG2480, &CCePrintDlg::OnBnClickedButtonTestTg2480)
	ON_BN_CLICKED(IDC_BUTTON_TEST_VKP80II, &CCePrintDlg::OnBnClickedButtonTestVkp80ii)
	ON_BN_CLICKED(IDC_BUTTON_TEST_TG248_USB, &CCePrintDlg::OnBnClickedButtonTestTg2480Usb)
	ON_BN_CLICKED(IDC_BUTTON_TEST_VKP80II_USB, &CCePrintDlg::OnBnClickedButtonTestVkp80iiUsb)
	ON_BN_CLICKED(IDC_BUTTON_TEST_TG2460_USB, &CCePrintDlg::OnBnClickedButtonTestTg2460Usb)
	ON_BN_CLICKED(IDC_BUTTON_TEST_Q1_USB, &CCePrintDlg::OnBnClickedButtonTestQ1Usb)
	ON_BN_CLICKED(IDC_BUTTON_TEST_BAR_CODES_USB, &CCePrintDlg::OnBnClickedButtonTestBarCodesUsb)
	ON_BN_CLICKED(IDC_BUTTON_TEST_BAR_CODES_COM, &CCePrintDlg::OnBnClickedButtonTestBarCodesCom)
	ON_BN_CLICKED(IDC_BUTTON_PRINT_UNICODE_STRING, &CCePrintDlg::OnBnClickedButtonPrintUnicodeString)
	ON_BN_CLICKED(IDC_BUTTON_TEST_VKP80IICH_USB, &CCePrintDlg::OnBnClickedButtonTestVkp80iichUsb)
	ON_BN_CLICKED(IDC_BUTTON_TEST_VKP80IICH_COM, &CCePrintDlg::OnBnClickedButtonTestVkp80iichCom)
END_MESSAGE_MAP()


// CCePrintDlg message handlers

BOOL CCePrintDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	//
	// Load library
	SetLastError(ERROR_SUCCESS);

#ifdef _DEBUG
	CString strLibPath = _T("C:\\CustomDLL_VC8_DM\\Win32\\CePrnLib.dll");	
#else
	CString strLibPath = _T("CePrnLib.dll");
#endif
	m_hLib = LoadLibrary((LPCTSTR)strLibPath);
	// Verify if library was loaded
	if(!m_hLib)
	{// Library not loaded. Show an error message and return.
		DWORD dwSystemErr = 0;
		CString strError = _T("");
		CString strApp = _T("");

		dwSystemErr = GetLastError();
		strApp.LoadStringW(IDS_STR_LIB_NOT_LOADED);		
		strError.FormatMessage(strApp, strLibPath, dwSystemErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);		
	}
	else
	{	// load function pointer, for late use...				
		m_lpfnCeOpenCom = (LPFNDLLFUNC_000)GetProcAddress(m_hLib, "CeOpenCom");
		m_lpfnCeCloseCom = (LPFNDLLFUNC_001)GetProcAddress(m_hLib, "CeCloseCom");
		m_lpfnCeWriteCom = (LPFNDLLFUNC_002)GetProcAddress(m_hLib, "CeWriteCom");
		m_lpfnCeWriteReadCom = (LPFNDLLFUNC_003)GetProcAddress(m_hLib, "CeWriteReadCom");		

		m_lpfnCeGetStrComConfig = (LPFNDLLFUNC_004)GetProcAddress(m_hLib, "CeGetStrComConfig");

		m_lpfnCeGetDefComCfg = (LPFNDLLFUNC_005)GetProcAddress(m_hLib, "CeGetDefComCfg");
		m_lpfnCeGetComCfg = (LPFNDLLFUNC_005)GetProcAddress(m_hLib, "CeGetComCfg");
		m_lpfnCeSetComCfg = (LPFNDLLFUNC_006)GetProcAddress(m_hLib, "CeSetComCfg");

		m_lpfnCeGetDefComTimeout = (LPFNDLLFUNC_007)GetProcAddress(m_hLib, "CeGetDefComTimeout");
		m_lpfnCeGetComTimeout = (LPFNDLLFUNC_007)GetProcAddress(m_hLib, "CeGetComTimeout");
		m_lpfnCeSetComTimeout = (LPFNDLLFUNC_008)GetProcAddress(m_hLib, "CeSetComTimeout");

		m_lpfnCeGethPortCom = (LPFNDLLFUNC_009)GetProcAddress(m_hLib, "CeGethPortCom");

		m_lpfnCeSpoolBufferToPrn = (LPFNDLLFUNC_010)GetProcAddress(m_hLib, "CeSpoolBufferToPrn");
		m_lpfnCeSetComCfgHR = (LPFNDLLFUNC_011)GetProcAddress(m_hLib, "CeSetComCfgHR");

		m_lpfnCeGetSts = (LPFNDLLFUNC_012)GetProcAddress(m_hLib, "CeGetSts");
		m_lpfnCeGetGSI = (LPFNDLLFUNC_012)GetProcAddress(m_hLib, "CeGetGSI");
		m_lpfnCeGetFWRel = (LPFNDLLFUNC_013)GetProcAddress(m_hLib, "CeGetFWRel");

		m_lpfnCeInitUSBLayer = (LPFNDLLFUNC_020)GetProcAddress(m_hLib, "CeInitUSBLayer");
		m_lpfnCeDeInitUSBLayer = (LPFNDLLFUNC_021)GetProcAddress(m_hLib, "CeDeInitUSBLayer");

		m_lpfnCeOpenUSBDev = (LPFNDLLFUNC_022)GetProcAddress(m_hLib, "CeOpenUSBDev");
		//m_lpfnCeCloseUSBDev = (LPFNDLLFUNC_023)GetProcAddress(m_hLib, "CeCloseUSBDev");

		m_lpfnCeWriteUSBDev = (LPFNDLLFUNC_023)GetProcAddress(m_hLib, "CeWriteUSBDev");
		m_lpfnCeWriteReadUSBDev = (LPFNDLLFUNC_024)GetProcAddress(m_hLib, "CeWriteReadUSBDev");
	
	}

	//
	// enable software function
	//
	CButton* pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_INITCOM);
	if(!m_lpfnCeOpenCom) pButton->EnableWindow(FALSE);
	
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_CLOSE_COM);
	if(pButton) pButton->EnableWindow(FALSE);
	
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_GSI);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_FWREL);
	if(pButton) pButton->EnableWindow(FALSE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_CFG_COM);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_STATUS);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_Q1);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_TG2460);
	if(pButton) pButton->EnableWindow(FALSE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_TG2480);
	if(pButton) pButton->EnableWindow(FALSE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_VKP80II);
	if(pButton) pButton->EnableWindow(FALSE);		

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_DEF_COM_CFG);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_CURR_COM_CFG);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_SET_DEF_COM_CFG3);
	if(pButton) pButton->EnableWindow(FALSE);
	
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_CUR_TIMEOUT);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_DEF_TIMEOUT);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_SET_CUR_TIMEOUT);
	if(pButton) pButton->EnableWindow(FALSE);
	
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_COM_HND);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE2);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_NEW_SET_COM);
	if(pButton) pButton->EnableWindow(FALSE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_SPOOL_FILE);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_INITUSB);
	if(!m_lpfnCeInitUSBLayer) pButton->EnableWindow(FALSE);
		
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_DEINITUSB);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_OPENUSBDEV);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE_USB);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_SPOOL_FILE_USB);
	if(pButton) pButton->EnableWindow(FALSE);

	// library soccesfully load

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CCePrintDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCePrintDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCePrintDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CCePrintDlg::FreeAllResource()
{
	// Create necessary variables
	DWORD dwCodedErr = 0;		// Error coded in error table
	DWORD dwSysErr = 0;		// Socket error
	DWORD dwAnswer = 0;

	// Verify if library was loaded
	if(m_hLib)
	{
		if(m_hComObj != NULL)
		{
			// Call function
			dwCodedErr = m_lpfnCeCloseCom(m_hComObj);

			// Verify result
			if (dwCodedErr)
			{// Error				
				CString strError = _T("");
				CString strApp = _T("");
				strApp.LoadStringW(IDS_STR_PROC_ERR);
				strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

				// Show error message in a dialog
				MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);

				// Show error to the user

				// Show result
				UpdateData(FALSE);
			}
			else
				m_hComObj = NULL;
		}

		if(m_hUSBObj != NULL)
		{
			if(m_lpfnCeInitUSBLayer)
				dwCodedErr = m_lpfnCeDeInitUSBLayer(m_hUSBObj);
		}

		FreeLibrary(m_hLib);
		m_hLib = NULL;
	}
}

void CCePrintDlg::ErrorUnsupFnct()
{
	CString strError = _T("");
	strError.LoadStringW(IDS_STR_FNCT_PNTR_NULL);
	MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
}

void CCePrintDlg::OnBnClickedButtonInitcom()
{
	// TODO: Add your control notification handler code here
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	TCHAR strHRSerCfg[MAX_PATH];
	DWORD dwHRSerCfgSize = sizeof(strHRSerCfg)/sizeof(TCHAR);

	// 
	UpdateData(TRUE);

	if(m_lpfnCeOpenCom)
		dwCodedErr = m_lpfnCeOpenCom(&m_hComObj, (LPCTSTR)m_strPortName, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);	
	}
	else
	{		
		if(!m_lpfnCeGetStrComConfig(m_hComObj, strHRSerCfg, &dwHRSerCfgSize))
			m_strHRComCfg = CString(strHRSerCfg, dwHRSerCfgSize);
		else
			m_strHRComCfg.LoadString(IDS_STR_COMPORT_NOT_INIT);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// set up some other string data
	m_strPrnAnswerHex = _T("");
	m_strGSI = _T("");

	// Show result
	UpdateData(FALSE);

	if(dwCodedErr)
		return;

	CButton* pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_INITCOM);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_CLOSE_COM);
	if(pButton) pButton->EnableWindow(TRUE);
	
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_GSI);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_FWREL);
	if(pButton) pButton->EnableWindow(TRUE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_STATUS);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_Q1);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_TG2460);
	if(pButton) pButton->EnableWindow(TRUE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_TG2480);
	if(pButton) pButton->EnableWindow(TRUE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_VKP80II);
	if(pButton) pButton->EnableWindow(TRUE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_CFG_COM);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_DEF_COM_CFG);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_CURR_COM_CFG);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_SET_DEF_COM_CFG3);
	if(pButton) pButton->EnableWindow(TRUE);
	
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_CUR_TIMEOUT);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_DEF_TIMEOUT);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_SET_CUR_TIMEOUT);
	if(pButton) pButton->EnableWindow(TRUE);
	
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_COM_HND);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_NEW_SET_COM);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_SPOOL_FILE);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE2);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_BAR_CODES_COM);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_PRINT_UNICODE_STRING);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_VKP80IICH_COM);
	if(pButton) pButton->EnableWindow(TRUE);
}

void CCePrintDlg::OnBnClickedButtonGetGsi()
{
	// TODO: Add your control notification handler code here
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
#ifdef _DIRECT_ACCESS_MODE_
	BYTE bytTxCmd[] = {0x1D, 'I', 0x01};
	DWORD dwTxCmdSiz = sizeof(bytTxCmd)/sizeof(BYTE);
	DWORD dwTxCmdByteSend = 0; 

	BYTE bytRxCmd[MAX_PATH];
	DWORD dwRxCmdSiz = 1;
	DWORD dwRxCmdByteRead = 0; 

	ZeroMemory(bytRxCmd, dwRxCmdSiz);

	if(m_lpfnCeWriteReadCom)
		dwCodedErr = m_lpfnCeWriteReadCom(m_hComObj, bytTxCmd, dwTxCmdSiz, &dwTxCmdByteSend,
										  bytRxCmd, dwRxCmdSiz, &dwRxCmdByteRead, &dwSysErr);
#else
	DWORD dwPrnGSI = 0;
	DWORD dwRxCmdByteRead = 0;
	if(m_lpfnCeGetGSI)
		dwCodedErr = m_lpfnCeGetGSI(m_hComObj, &dwPrnGSI, &dwSysErr);
#endif	// #ifdef _DIRECT_ACCESS_MODE_	
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}
	else
	{
		// Format output string
		// Show answer to the user.
#ifdef _DIRECT_ACCESS_MODE_
		m_strPrnAnswerHex.Format(_T("0x%02x | num. byte: %d"), (BYTE)bytRxCmd[0], dwRxCmdByteRead);
		m_strGSI.Format(_T("GSI: 0x%02x"), (BYTE)bytRxCmd[0]);
#else
		dwRxCmdByteRead = 1;	// only one valid byte
		m_strPrnAnswerHex.Format(_T("0x%02x | num. byte: %d"), (BYTE)dwPrnGSI, dwRxCmdByteRead);
		m_strGSI.Format(_T("GSI: 0x%02x"), (BYTE)dwPrnGSI);
#endif

		// Format answer as a string with Ascii chars
//		m_strPrnAnswerAscii.Format(_T("%d"), dwPrnStatus);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);
	return;
}

void CCePrintDlg::OnBnClickedButtonGetStatus()
{
	// TODO: Add your control notification handler code here
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;

#ifdef _DIRECT_ACCESS_MODE_
	BYTE bytTxCmd[] = {0x10, 0x04, 0x14};
	DWORD dwTxCmdSiz = sizeof(bytTxCmd)/sizeof(BYTE);
	DWORD dwTxCmdByteSend = 0; 

	BYTE bytRxCmd[MAX_PATH];
	DWORD dwRxCmdSiz = 6;
	DWORD dwRxCmdByteRead = 0; 

	ZeroMemory(bytRxCmd, dwRxCmdSiz);

	if(m_lpfnCeWriteReadCom)
		dwCodedErr = m_lpfnCeWriteReadCom(m_hComObj, bytTxCmd, dwTxCmdSiz, &dwTxCmdByteSend,
										  bytRxCmd, dwRxCmdSiz, &dwRxCmdByteRead, &dwSysErr);
#else
	DWORD dwPrnSts = 0;
	if(m_lpfnCeGetSts)
		dwCodedErr = m_lpfnCeGetSts(m_hComObj, &dwPrnSts, &dwSysErr);
#endif	// #ifdef _DIRECT_ACCESS_MODE_
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}
	else
	{
		// Format output string
		// Show answer to the user...
		// to decode printer status information please referring to the status command on
		// printer manual
		//
#ifdef _DIRECT_ACCESS_MODE_
		m_strPrnAnswerHex.Format(_T("0x%02x|0x%02x|0x%02x|0x%02x | num. byte: %d"), bytRxCmd[2], bytRxCmd[3], bytRxCmd[4], bytRxCmd[5], dwRxCmdByteRead);
#else
		m_strPrnAnswerHex.Format(_T("0x%02x|0x%02x|0x%02x|0x%02x | num. byte: %d"), (BYTE)(dwPrnSts >> 24), (BYTE)(dwPrnSts >> 16), (BYTE)(dwPrnSts >> 8), (BYTE)dwPrnSts, sizeof(dwPrnSts)/sizeof(BYTE));
#endif // #ifdef _DIRECT_ACCESS_MODE_

		// Format answer as a string with Ascii chars
//		m_strPrnAnswerAscii.Format(_T("%d"), dwPrnStatus);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);
	return;
}

void CCePrintDlg::OnBnClickedButtonGetFwrel()
{
	// TODO: Add your control notification handler code here
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	BYTE bytRxCmd[MAX_PATH];
	DWORD dwRxCmdSiz = sizeof(bytRxCmd)/sizeof(BYTE);

	DWORD dwPrnSts = 0;
	if(m_lpfnCeGetFWRel)
		dwCodedErr = m_lpfnCeGetFWRel(m_hComObj, bytRxCmd, &dwRxCmdSiz, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}
	else
	{
		// Format output string
		// Show answer to the user...
		// to decode printer status information please referring to the status command on
		// printer manual
		//
		m_strPrnAnswerHex.Format(_T("%c%c%c%c | num. byte: %d"), bytRxCmd[0], bytRxCmd[1], bytRxCmd[2], bytRxCmd[3], sizeof(dwPrnSts)/sizeof(BYTE));

		// Format answer as a string with Ascii chars
//		m_strPrnAnswerAscii.Format(_T("%d"), dwPrnStatus);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);
	return;
}

void CCePrintDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	FreeAllResource();
	OnCancel();
}

void CCePrintDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	FreeAllResource();
	OnOK();
}

void CCePrintDlg::OnBnClickedButtonGetDefTimeout()
{
	// TODO: Add your control notification handler code here
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	COMMTIMEOUTS deftout;

	ZeroMemory(&deftout, sizeof(COMMTIMEOUTS)/sizeof(BYTE));

	if(m_lpfnCeGetDefComTimeout)
		dwCodedErr = m_lpfnCeGetDefComTimeout(m_hComObj, &deftout);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);

	if(dwCodedErr = 0)
	{
		CCeComTimeout cCeComTimeout;

		cCeComTimeout.Set_m_CommTimeout(deftout);
		cCeComTimeout.DoModal();
	}
}

void CCePrintDlg::OnBnClickedButtonGetCurTimeout()
{
	// TODO: Add your control notification handler code here
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	COMMTIMEOUTS tout;

	ZeroMemory(&tout, sizeof(COMMTIMEOUTS)/sizeof(BYTE));

	if(m_lpfnCeGetComTimeout)
		dwCodedErr = m_lpfnCeGetComTimeout(m_hComObj, &tout);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);

	if(dwCodedErr = 0)
	{
		CCeComTimeout cCeComTimeout;

		cCeComTimeout.Set_m_CommTimeout(tout);
		cCeComTimeout.DoModal();
	}
}

void CCePrintDlg::OnBnClickedButtonSetCurTimeout()
{
	// TODO: Add your control notification handler code here
	//
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	COMMTIMEOUTS tout;
	COMMTIMEOUTS newtout;

	ZeroMemory(&tout, sizeof(COMMTIMEOUTS)/sizeof(BYTE));
	ZeroMemory(&newtout, sizeof(COMMTIMEOUTS)/sizeof(BYTE));

	if(m_lpfnCeGetDefComTimeout)
		dwCodedErr = m_lpfnCeGetDefComTimeout(m_hComObj, &tout);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	CCeComTimeout cCeComTimeout;
	cCeComTimeout.Set_m_CommTimeout(tout);
	cCeComTimeout.Set_m_bfReadOnly(TRUE);
	INT_PTR nAnswer = cCeComTimeout.DoModal();
	if(nAnswer == IDOK)
	{
		cCeComTimeout.Get_m_CommTimeout(&newtout);
	}
	else
		return;

	if(m_lpfnCeSetComTimeout)
		dwCodedErr = m_lpfnCeSetComTimeout(m_hComObj, newtout, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return;
	}
}

void CCePrintDlg::OnBnClickedButtonGetDefComCfg()
{
	// TODO: Add your control notification handler code here
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	DCB defdcb;

	ZeroMemory(&defdcb, sizeof(DCB)/sizeof(BYTE));

	if(m_lpfnCeGetDefComCfg)
		dwCodedErr = m_lpfnCeGetDefComCfg(m_hComObj, &defdcb);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonGetCurrComCfg()
{
	// TODO: Add your control notification handler code here
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	DCB dcb;

	ZeroMemory(&dcb, sizeof(DCB)/sizeof(BYTE));

	if(m_lpfnCeGetComCfg)
		dwCodedErr = m_lpfnCeGetComCfg(m_hComObj, &dcb);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonSetDefComCfg3()
{
	// TODO: Add your control notification handler code here
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	DCB dcb;

	ZeroMemory(&dcb, sizeof(DCB)/sizeof(BYTE));

	if(m_lpfnCeGetComCfg)
		dwCodedErr = m_lpfnCeGetComCfg(m_hComObj, &dcb);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	if(m_lpfnCeSetComCfg)
		dwCodedErr = m_lpfnCeSetComCfg(m_hComObj, dcb, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonGetCOMhnd()
{
	// TODO: Add your control notification handler code here
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	HANDLE hPort = NULL;
	DWORD dwLineCommStatus = 0;
	CString strError = _T("");
	CString strApp = _T("");

	if(m_lpfnCeGethPortCom)
		dwCodedErr = m_lpfnCeGethPortCom(m_hComObj, &hPort);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);

	if(hPort == NULL)
	{
		strError.LoadStringW(IDS_STR_INVALID_COM_HND);
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
		return;
	}

	//
	// don't release/close the handle returned by CeGethPortCom library function
	// this handle will be closed by CeClosePort(...) library execution
	//
	// by this handle you can use the Microsoft API for handle all Serial
	// Communication Port
	//
	if(!GetCommModemStatus(hPort, &dwLineCommStatus))
	{	// bad error code
		strError = _T("");
		dwSysErr = GetLastError();
		strError.LoadStringW(IDS_STR_API_COM_ERROR);
		strApp.Format(strError, _T("GetCommModemStatus"), dwSysErr);
		MessageBox(strApp, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);		
		return;
	}

	return;
}

void CCePrintDlg::OnBnClickedButtonCloseCom()
{
	// TODO: Add your control notification handler code here
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;

	if(m_hComObj != NULL)
	{
		// Call function
		dwCodedErr = m_lpfnCeCloseCom(m_hComObj);

		// Verify result
		if (dwCodedErr)
		{// Error				
			CString strError = _T("");
			CString strApp = _T("");
			strApp.LoadStringW(IDS_STR_PROC_ERR);
			strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

			// Show error message in a dialog
			MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
		}
		else
			m_hComObj = NULL;
	}

	m_strPrnAnswerHex = _T("");
	m_strLibError = _T("");
	m_strSysError = _T("");
	m_strHRComCfg = _T("");
	m_strPrnFilePath = _T("");
	m_strHRComCfg.LoadString(IDS_STR_COMPORT_NOT_INIT);
	m_strGSI = _T("");
	// m_strPrnName = _T("Custom KUBE 80mm (200dpi)); 

	// Update View
	UpdateData(FALSE);
	CButton* pButton = NULL;

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_INITCOM);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_CLOSE_COM);
	if(pButton) pButton->EnableWindow(FALSE);
	
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_GSI);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_FWREL);
	if(pButton) pButton->EnableWindow(FALSE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_STATUS);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_Q1);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_TG2460);
	if(pButton) pButton->EnableWindow(FALSE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_TG2480);
	if(pButton) pButton->EnableWindow(FALSE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_VKP80II);
	if(pButton) pButton->EnableWindow(FALSE);	

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_DEF_COM_CFG);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_CURR_COM_CFG);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_SET_DEF_COM_CFG3);
	if(pButton) pButton->EnableWindow(FALSE);
	
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_CUR_TIMEOUT);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_DEF_TIMEOUT);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_SET_CUR_TIMEOUT);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_CFG_COM);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_NEW_SET_COM);
	if(pButton) pButton->EnableWindow(FALSE);	
	
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_COM_HND);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE2);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_SPOOL_FILE);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_BAR_CODES_COM);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_PRINT_UNICODE_STRING);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_VKP80IICH_COM);
	if(pButton) pButton->EnableWindow(FALSE);
}

void CCePrintDlg::OnBnClickedButtonWritePrnFile()
{
	CString strNomeFile = m_strPrnFilePath;
	SendFileToCom(strNomeFile);
}

// Flus the buffer to a file
//
BOOL CCePrintDlg::SendFileToCom(CString strNomeFile)
{
	// TODO: Add your control notification handler code here
	PBYTE pbtFileBuff = NULL;
	CString strMess, strTitl;
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	DWORD dwTxCmdByteSend = 0;

	// set the filename
	CString NomeFile = _T("");
	if(strNomeFile.GetLength() != 0)
		NomeFile = strNomeFile;

	CFile fFile;	
	if(!fFile.Open(NomeFile, CFile::modeRead))
		return FALSE;

	DWORD dwSize = (DWORD)fFile.GetLength();
	
	pbtFileBuff = new BYTE [dwSize];

	if(!pbtFileBuff)
	{
		strTitl.LoadString(IDS_STR_ERROR);
		strMess.LoadString(IDS_STR_NO_ENOUGHT_RESOURCE_TO_LOAD_FILES);
		MessageBox(strMess, strTitl, MB_ICONSTOP | MB_OK);
		return FALSE;
	}

	UINT nBytesRead = fFile.Read(pbtFileBuff, dwSize);

	if(nBytesRead != dwSize)
	{
		strTitl.LoadString(IDS_STR_ERROR);
		strMess.LoadString(IDS_STR_NO_ALL_BYTE_FILES_READ);
		MessageBox(strMess, strTitl, MB_ICONSTOP | MB_OK);

		delete[] pbtFileBuff;
		return FALSE;
	}

	// send data to COM
	if(m_lpfnCeWriteCom)
		dwCodedErr = m_lpfnCeWriteCom(m_hComObj, pbtFileBuff, dwSize, &dwTxCmdByteSend, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return FALSE;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);

	delete[] pbtFileBuff;

	return TRUE;
}

void CCePrintDlg::OnBnClickedButtonWritePrnFile2()
{
	// TODO: Add your control notification handler code here
	//
	CString strMess, strTitl;

	CString szFilter = _T("BMP files (*.bmp)|*.bmp|All Files (*.*)|*.*||");
	CString strExtFile = _T(".bmp");
	DWORD dwDialogStyle = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;

	TCHAR tchBuffer[MAX_PATH];
	if(!GetCurrentDirectory(MAX_PATH, tchBuffer))
	{		
		strTitl.LoadString(IDS_STR_ERROR);
		strMess.LoadString(IDS_STR_GET_CURRENT_PATH);
		MessageBox(strMess, strTitl, MB_ICONSTOP | MB_OK);
		return;
	}

	CString strAppPath = CString(tchBuffer);
	CString fileName = _T("");

	szFilter = _T("PRN files (*.prn)|*.prn|All Files (*.*)|*.*||");
	strExtFile = _T(".prn");
	dwDialogStyle = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;

	CFileDialog dlgFile(TRUE, LPCTSTR(strExtFile), _T(""), dwDialogStyle, LPCTSTR(szFilter), NULL);
	dlgFile.m_ofn.lpstrInitialDir = strAppPath;

	if(dlgFile.DoModal() == IDOK)
	{
		m_strPrnFilePath = dlgFile.GetPathName();
		CButton* pButton = NULL;

		if(m_hComObj)
		{
			pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE);
			if(pButton) pButton->EnableWindow(TRUE);

			pButton = (CButton*)GetDlgItem(IDC_BUTTON_SPOOL_FILE);
			if(pButton) pButton->EnableWindow(FALSE);
		}
		else
		{
			pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE);
			if(pButton) pButton->EnableWindow(FALSE);

			pButton = (CButton*)GetDlgItem(IDC_BUTTON_SPOOL_FILE);
			if(pButton) pButton->EnableWindow(TRUE);
		}
	}
	else
		return;

	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonSpoolFile()
{
	// TODO: Add your control notification handler code here
	// TODO: Add your control notification handler code here
	PBYTE pbtFileBuff = NULL;
	CString strMess, strTitl;
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	DWORD dwTxCmdByteSend = 0;

	// set the filename
	CString NomeFile = m_strPrnFilePath;
//	if(strNomeFile.GetLength() != 0)
//		NomeFile = strNomeFile;

	CFile fFile;	
	if(!fFile.Open(NomeFile, CFile::modeRead))
		return;

	DWORD dwSize = (DWORD)fFile.GetLength();
	
	pbtFileBuff = new BYTE [dwSize];

	if(!pbtFileBuff)
	{
		strTitl.LoadString(IDS_STR_ERROR);
		strMess.LoadString(IDS_STR_NO_ENOUGHT_RESOURCE_TO_LOAD_FILES);
		MessageBox(strMess, strTitl, MB_ICONSTOP | MB_OK);
		return;
	}

	UINT nBytesRead = fFile.Read(pbtFileBuff, dwSize);

	if(nBytesRead != dwSize)
	{
		strTitl.LoadString(IDS_STR_ERROR);
		strMess.LoadString(IDS_STR_NO_ALL_BYTE_FILES_READ);
		MessageBox(strMess, strTitl, MB_ICONSTOP | MB_OK);

		delete[] pbtFileBuff;
		return;
	}

	// send data to Printer
	if(m_lpfnCeSpoolBufferToPrn)
		dwCodedErr = m_lpfnCeSpoolBufferToPrn((LPCTSTR)m_strPrnName, pbtFileBuff, dwSize, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);

	delete[] pbtFileBuff;

	return;
}

void CCePrintDlg::OnBnClickedButtonTest()
{
	// TODO: Add your control notification handler code here
	COMMTIMEOUTS tout;
	COMMTIMEOUTS newtout;
	ZeroMemory(&tout, sizeof(COMMTIMEOUTS)/sizeof(BYTE));
	ZeroMemory(&newtout, sizeof(COMMTIMEOUTS)/sizeof(BYTE));
	CCeComTimeout cCeComTimeout;	

	tout.ReadIntervalTimeout = 100;
	tout.ReadTotalTimeoutMultiplier = 200;
	tout.ReadTotalTimeoutConstant = 300;
	tout.WriteTotalTimeoutConstant = 400;
	tout.WriteTotalTimeoutMultiplier = 500;

	cCeComTimeout.Set_m_CommTimeout(tout);
	cCeComTimeout.Set_m_bfReadOnly(TRUE);
	INT_PTR nAnswer = cCeComTimeout.DoModal();
	if(nAnswer == IDOK)
	{
		cCeComTimeout.Get_m_CommTimeout(&newtout);
	}
}

void CCePrintDlg::OnBnClickedButtonNewSetCom()
{
	// TODO: Add your control notification handler code here
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	DWORD dwBaudRate = 115200;
	BYTE bytStopBit = ONESTOPBIT; // ONE5STOPBITS or TWOSTOPBITS
	BYTE bytByteSize = 8;
	BYTE bytParity = NOPARITY; // EVENPARITY, MARKPARITY, ODDPARITY, SPACEPARITY
	DWORD dwFlowCtrl = HS_HW;

	TCHAR strHRSerCfg[MAX_PATH];
	DWORD dwHRSerCfgSize = sizeof(strHRSerCfg)/sizeof(TCHAR);

	// send data to Printer
	if(m_lpfnCeSetComCfgHR)
		dwCodedErr = m_lpfnCeSetComCfgHR(m_hComObj, dwBaudRate, bytStopBit, bytByteSize, bytParity, dwFlowCtrl, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}
	else
	{
		if(!m_lpfnCeGetStrComConfig(m_hComObj, strHRSerCfg, &dwHRSerCfgSize))
			m_strHRComCfg = CString(strHRSerCfg, dwHRSerCfgSize);
		else
			m_strHRComCfg.LoadString(IDS_STR_COMPORT_NOT_INIT);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonInitusb()
{
	// TODO: Add your control notification handler code here
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;

	// 
	UpdateData(TRUE);

	if(m_lpfnCeInitUSBLayer)
		dwCodedErr = m_lpfnCeInitUSBLayer(&m_hUSBObj, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);	
	}


	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);

	UpdateData(FALSE);

	if(dwCodedErr)
		return;

	CButton* pButton = (CButton*)GetDlgItem(IDC_BUTTON_OPENUSBDEV);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_DEINITUSB);
	if(pButton) pButton->EnableWindow(TRUE);
}

void CCePrintDlg::OnBnClickedButtonDeinitusb()
{
	// TODO: Add your control notification handler code here
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;

	// 
	UpdateData(TRUE);

	if(m_lpfnCeInitUSBLayer)
		dwCodedErr = m_lpfnCeDeInitUSBLayer(m_hUSBObj);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);	
	}
	else
		m_hUSBObj = NULL;

	m_strPrnAnswerHex = _T("");
	m_strLibError = _T("");
	m_strSysError = _T("");
	m_strHRComCfg = _T("");
	m_strPrnFilePath = _T("");
	m_strHRComCfg.LoadString(IDS_STR_COMPORT_NOT_INIT);
	m_strEditBoxGSI = _T("");
	m_strEditGetFwRelUsb = _T("");
	m_strEditUsbStatus = _T("");

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);

	CButton* pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_INITUSB);
	if(pButton) pButton->EnableWindow(TRUE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_DEINITUSB);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_OPENUSBDEV);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_GSI_USB);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_STATUS_USB);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_FW_REL_USB);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE_USB);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_TG248_USB);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_VKP80II_USB);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_TG2460_USB);
	if(pButton) pButton->EnableWindow(FALSE);

	pButton = NULL;
	pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_Q1_USB);
	if(pButton) pButton->EnableWindow(FALSE);

	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonOpenusbdev()
{
	// TODO: Add your control notification handler code here
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;

	// 
	UpdateData(TRUE);

	if(m_lpfnCeOpenUSBDev)
		dwCodedErr = m_lpfnCeOpenUSBDev(m_hUSBObj, (LPCTSTR)m_strPrnName, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);	
	}
	else
	{
		CButton* pButton = NULL;
		pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_GSI_USB);
		if(pButton) pButton->EnableWindow(TRUE);

		pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_STATUS_USB);
		if(pButton) pButton->EnableWindow(TRUE);

		pButton = NULL;
		pButton = (CButton*)GetDlgItem(IDC_BUTTON_GET_FW_REL_USB);
		if(pButton) pButton->EnableWindow(TRUE);


		pButton = NULL;
		pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_TG248_USB);
		if(pButton) pButton->EnableWindow(TRUE);

		pButton = NULL;
		pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_VKP80II_USB);
		if(pButton) pButton->EnableWindow(TRUE);

		pButton = NULL;
		pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_TG2460_USB);
		if(pButton) pButton->EnableWindow(TRUE);

		pButton = NULL;
		pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_Q1_USB);
		if(pButton) pButton->EnableWindow(TRUE);

		pButton = NULL;
		pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_BAR_CODES_USB);
		if(pButton) pButton->EnableWindow(TRUE);

		pButton = NULL;
		pButton = (CButton*)GetDlgItem(IDC_BUTTON_PRINT_UNICODE_STRING);
		if(pButton) pButton->EnableWindow(TRUE);

		pButton = NULL;
		pButton = (CButton*)GetDlgItem(IDC_BUTTON_TEST_VKP80IICH_USB);
		if(pButton) pButton->EnableWindow(TRUE);
	}

	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonGetGsiUsb()
{
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;

#ifdef _DIRECT_ACCESS_MODE_
	BYTE bytTxCmd[] = {0x1D, 'I', 0x01};
	//BYTE bytTxCmd[] = {0x1B, 0x76};
	DWORD dwTxCmdSiz = sizeof(bytTxCmd)/sizeof(BYTE);
	DWORD dwTxCmdByteSend = 0; 

	BYTE bytRxCmd[MAX_PATH];
	DWORD dwRxCmdSiz = sizeof(bytRxCmd)/sizeof(BYTE);
	DWORD dwRxCmdByteRead = 0; 

	ZeroMemory(bytRxCmd, dwRxCmdSiz);

	// 
	UpdateData(TRUE);

	if(m_lpfnCeWriteReadUSBDev)
		dwCodedErr = m_lpfnCeWriteReadUSBDev(m_hUSBObj, (LPBYTE)bytTxCmd, dwTxCmdSiz, &dwTxCmdByteSend, bytRxCmd, dwRxCmdSiz, &dwRxCmdByteRead, &dwSysErr);
#else
	DWORD dwPrnGSI = 0;
	DWORD dwRxCmdByteRead = 0;
	if(m_lpfnCeGetGSI)
		dwCodedErr = m_lpfnCeGetGSI(m_hUSBObj, &dwPrnGSI, &dwSysErr);
#endif	// #ifdef _DIRECT_ACCESS_MODE_	
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);	
	}
	else
	{
		// Show error to the user (if no error, they MUST be zero)
		m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
		m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);

#ifdef _DIRECT_ACCESS_MODE_
		// we expected only one byte from printer
		m_strEditBoxGSI.Format(_T("0x%02X"), bytRxCmd[0]);
#else
		m_strEditBoxGSI.Format(_T("0x%02X"), (BYTE)dwPrnGSI);
#endif	// #ifdef _DIRECT_ACCESS_MODE_	
	}

	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonGetStatusUsb()
{
	// 
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;

#ifdef _DIRECT_ACCESS_MODE_
	// Set command for get status from printer
	BYTE bytTxCmd[] = {0x10, 0x04, 0x14};
	DWORD dwTxCmdSiz = sizeof(bytTxCmd)/sizeof(BYTE);
	DWORD dwTxCmdByteSend = 0; 

	BYTE bytRxCmd[MAX_PATH];
	DWORD dwRxCmdSiz = sizeof(bytRxCmd)/sizeof(BYTE);;
	DWORD dwRxCmdByteRead = 0; 

	ZeroMemory(bytRxCmd, dwRxCmdSiz);

	// 
	UpdateData(TRUE);

	if(m_lpfnCeWriteReadUSBDev)
		dwCodedErr = m_lpfnCeWriteReadUSBDev(m_hUSBObj, (LPBYTE)bytTxCmd, dwTxCmdSiz, &dwTxCmdByteSend, bytRxCmd, dwRxCmdSiz, &dwRxCmdByteRead, &dwSysErr);
#else
	DWORD dwPrnSts = 0;
	if(m_lpfnCeGetSts)
		dwCodedErr = m_lpfnCeGetSts(m_hUSBObj, &dwPrnSts, &dwSysErr);
#endif	// #ifdef _DIRECT_ACCESS_MODE_
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);	
	}
	else
	{
		// Show error to the user (if no error, they MUST be zero)
		m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
		m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);

		// we expected six bytes from printer
		// to decode printer status information please referring to the status command on
		// printer manual
		//
#ifdef _DIRECT_ACCESS_MODE_
		m_strEditUsbStatus.Format(_T("0x%02x|0x%02x|0x%02x|0x%02x | num. byte: %d"), bytRxCmd[2], bytRxCmd[3], bytRxCmd[4], bytRxCmd[5], dwRxCmdByteRead);
#else
		m_strEditUsbStatus.Format(_T("0x%02x|0x%02x|0x%02x|0x%02x | num. byte: %d"), (BYTE)(dwPrnSts >> 24), (BYTE)(dwPrnSts >> 16), (BYTE)(dwPrnSts >> 8), (BYTE)dwPrnSts, sizeof(dwPrnSts)/sizeof(BYTE));
#endif // #ifdef _DIRECT_ACCESS_MODE_
	}

	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonWritePrnFileUsb()
{
	PBYTE pbtFileBuff = NULL;
	CString strMess, strTitl;
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	DWORD dwTxCmdByteSend = 0;

	CString strNomeFile = m_strPrnFilePathUsb;

	// set the filename
	CString NomeFile = _T("");
	if(strNomeFile.GetLength() != 0)
		NomeFile = strNomeFile;

	CFile fFile;	
	if(!fFile.Open(NomeFile, CFile::modeRead))
		return;

	DWORD dwSize = (DWORD)fFile.GetLength();
	
	pbtFileBuff = new BYTE [dwSize];

	if(!pbtFileBuff)
	{
		strTitl.LoadString(IDS_STR_ERROR);
		strMess.LoadString(IDS_STR_NO_ENOUGHT_RESOURCE_TO_LOAD_FILES);
		MessageBox(strMess, strTitl, MB_ICONSTOP | MB_OK);
		return;
	}

	UINT nBytesRead = fFile.Read(pbtFileBuff, dwSize);

	if(nBytesRead != dwSize)
	{
		strTitl.LoadString(IDS_STR_ERROR);
		strMess.LoadString(IDS_STR_NO_ALL_BYTE_FILES_READ);
		MessageBox(strMess, strTitl, MB_ICONSTOP | MB_OK);

		delete[] pbtFileBuff;
		return;
	}

	// send data to USB
	if(m_lpfnCeWriteUSBDev)
		dwCodedErr = m_lpfnCeWriteUSBDev(m_hUSBObj, pbtFileBuff, dwSize, &dwTxCmdByteSend, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);

	delete[] pbtFileBuff;

	return;
}

void CCePrintDlg::OnBnClickedButtonOpenPrnFileUsb()
{
	CString strMess, strTitl;

	CString szFilter = _T("PRN files (*.prn)|*.prn|All Files (*.*)|*.*||");
	CString strExtFile = _T(".prn");
	DWORD dwDialogStyle = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;

	UpdateData(TRUE);

	TCHAR tchBuffer[MAX_PATH];
	if(!GetCurrentDirectory(MAX_PATH, tchBuffer))
	{		
		strTitl.LoadString(IDS_STR_ERROR);
		strMess.LoadString(IDS_STR_GET_CURRENT_PATH);
		MessageBox(strMess, strTitl, MB_ICONSTOP | MB_OK);
		return;
	}

	CString strAppPath = CString(tchBuffer);
	CString fileName = _T("");

	CFileDialog dlgFile(TRUE, LPCTSTR(strExtFile), _T(""), dwDialogStyle, LPCTSTR(szFilter), NULL);
	dlgFile.m_ofn.lpstrInitialDir = strAppPath;

	if(dlgFile.DoModal() == IDOK)
	{
		m_strPrnFilePathUsb = dlgFile.GetPathName();
		CButton* pButton = NULL;

		if(m_hUSBObj)
		{
			pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE_USB);
			if(pButton) pButton->EnableWindow(TRUE);

			pButton = (CButton*)GetDlgItem(IDC_BUTTON_SPOOL_FILE_USB);
			if(pButton) pButton->EnableWindow(FALSE);
		}
		else
		{
			pButton = (CButton*)GetDlgItem(IDC_BUTTON_WRITE_PRN_FILE_USB);
			if(pButton) pButton->EnableWindow(FALSE);

			pButton = (CButton*)GetDlgItem(IDC_BUTTON_SPOOL_FILE_USB);
			if(pButton) pButton->EnableWindow(TRUE);
		}
	}
	else
		return;

	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonSpoolFileUsb()
{
	PBYTE pbtFileBuff = NULL;
	CString strMess, strTitl;
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	DWORD dwTxCmdByteSend = 0;

	UpdateData(TRUE);

	// set the filename
	CString NomeFile = m_strPrnFilePathUsb;
//	if(strNomeFile.GetLength() != 0)
//		NomeFile = strNomeFile;

	CFile fFile;	
	if(!fFile.Open(NomeFile, CFile::modeRead))
		return;

	DWORD dwSize = (DWORD)fFile.GetLength();
	
	pbtFileBuff = new BYTE [dwSize];

	if(!pbtFileBuff)
	{
		strTitl.LoadString(IDS_STR_ERROR);
		strMess.LoadString(IDS_STR_NO_ENOUGHT_RESOURCE_TO_LOAD_FILES);
		MessageBox(strMess, strTitl, MB_ICONSTOP | MB_OK);
		return;
	}

	UINT nBytesRead = fFile.Read(pbtFileBuff, dwSize);

	if(nBytesRead != dwSize)
	{
		strTitl.LoadString(IDS_STR_ERROR);
		strMess.LoadString(IDS_STR_NO_ALL_BYTE_FILES_READ);
		MessageBox(strMess, strTitl, MB_ICONSTOP | MB_OK);

		delete[] pbtFileBuff;
		return;
	}

	// send data to Printer
	if(m_lpfnCeSpoolBufferToPrn)
		dwCodedErr = m_lpfnCeSpoolBufferToPrn((LPCTSTR)m_strPrnName, pbtFileBuff, dwSize, &dwSysErr);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

	// Show result
	UpdateData(FALSE);

	delete[] pbtFileBuff;

	return;
}

void CCePrintDlg::OnBnClickedButtonGetFwRelUsb()
{
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;

#ifdef _DIRECT_ACCESS_MODE_
	BYTE bytTxCmd[] = {0x1D, 'I', 0x03};
	DWORD dwTxCmdSiz = sizeof(bytTxCmd)/sizeof(BYTE);
	DWORD dwTxCmdByteSend = 0; 
#endif

	BYTE bytRxCmd[MAX_PATH];
	DWORD dwRxCmdSiz = sizeof(bytRxCmd)/sizeof(BYTE);
	DWORD dwRxCmdByteRead = 0; 

	ZeroMemory(bytRxCmd, dwRxCmdSiz);

	// 
	UpdateData(TRUE);

#ifdef _DIRECT_ACCESS_MODE_
	if(m_lpfnCeWriteReadUSBDev)
		dwCodedErr = m_lpfnCeWriteReadUSBDev(m_hUSBObj, (LPBYTE)bytTxCmd, dwTxCmdSiz, &dwTxCmdByteSend, bytRxCmd, dwRxCmdSiz, &dwRxCmdByteRead, &dwSysErr);
#else
	if(m_lpfnCeGetFWRel)
		dwCodedErr = m_lpfnCeGetFWRel(m_hUSBObj, bytRxCmd, &dwRxCmdSiz, &dwSysErr);
#endif // #ifdef _DIRECT_ACCESS_MODE_
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);	
	}
	else
	{
		// Show error to the user (if no error, they MUST be zero)
		m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
		m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);

		// format printer answer
		m_strEditGetFwRelUsb.Format(_T("%c%c%c%c"), bytRxCmd[0], bytRxCmd[1], bytRxCmd[2], bytRxCmd[3]);
	}

	UpdateData(FALSE);
}

void CCePrintDlg::OnBnClickedButtonCfgCom()
{
	DWORD dwCodedErr = 0;
	DWORD dwSysErr = 0;
	DCB dcb;
	TCHAR strHRSerCfg[MAX_PATH];
	DWORD dwHRSerCfgSize = sizeof(strHRSerCfg)/sizeof(TCHAR);

	ZeroMemory(&dcb, sizeof(DCB)/sizeof(BYTE));

	if(m_lpfnCeGetComCfg)
		dwCodedErr = m_lpfnCeGetComCfg(m_hComObj, &dcb);
	else
	{
		ErrorUnsupFnct();
		return;
	}

	// Verify result
	if (dwCodedErr)
	{// Error
		CString strError = _T("");
		CString strApp = _T("");
		strApp.LoadStringW(IDS_STR_PROC_ERR);
		strError.FormatMessage(strApp, dwCodedErr, dwSysErr);

		// Show error message in a dialog
		MessageBox(strError, AfxGetAppName(), MB_OK|MB_ICONWARNING|MB_SYSTEMMODAL);
	}

	// Show error to the user (if no error, they MUST be zero)
	m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
	m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);

	CDialogComSettings dialogSettings;

	dialogSettings.Set_m_dcb(dcb);
	if (dialogSettings.DoModal() == IDOK)
	{
		dcb = dialogSettings.Get_m_dcb();
		if(m_lpfnCeSetComCfg)
			dwCodedErr = m_lpfnCeSetComCfg(m_hComObj, dcb, &dwSysErr);
		else
		{
			ErrorUnsupFnct();
			return;
		}

		// Show error to the user (if no error, they MUST be zero)
		m_strLibError.Format(_T("Lib. Error: %d"), dwCodedErr);
		m_strSysError.Format(_T("Sys. Error: %d"), dwSysErr);		

		if(!m_lpfnCeGetStrComConfig(m_hComObj, strHRSerCfg, &dwHRSerCfgSize))
			m_strHRComCfg = CString(strHRSerCfg, dwHRSerCfgSize);
		else
			m_strHRComCfg.LoadString(IDS_STR_COMPORT_NOT_INIT);

		// Show result
		UpdateData(FALSE);
	}

	// Show result
	UpdateData(FALSE);
}

void CCePrintDlg::TestQ1(HANDLE hObj)
{
	DWORD dwSysError = 0;
	DWORD dwError = 0;
	DWORD dwCashDraweerMode = 0x00008080;
	DWORD dwFeed = 0x000000A0;
	BYTE bytNLF = 3;
	BYTE bytTextFormatType = 0x01;
	BYTE bytFontFormat = 0x00;

	// 1) get needed Exported API CePrint.Dll library
	LPFNDLLFUNC_040 pCeWriteTxt = NULL;
	LPFNDLLFUNC_041 pCeTCut = NULL;
	LPFNDLLFUNC_041 pCePCut = NULL;
	LPFNDLLFUNC_043 pCePaperFeed = NULL;
	LPFNDLLFUNC_042 pCeLF = NULL;
	LPFNDLLFUNC_042 pCeCR = NULL;
	LPFNDLLFUNC_043 pCeCashDrawer = NULL;
	LPFNDLLFUNC_042 pCeSetJustification = NULL;
	LPFNDLLFUNC_042 pCeSetTextMode = NULL;
	LPFNDLLFUNC_042 pCeSelectHRIPosition = NULL;
	LPFNDLLFUNC_042 pCeSelectHRIFont = NULL;
	LPFNDLLFUNC_042 pCeSetBarCodeHeigth = NULL;
	LPFNDLLFUNC_042 pCeSetBarCodeWidth = NULL;
	LPFNDLLFUNC_045 pCePrintBarCode = NULL;


	pCeWriteTxt = (LPFNDLLFUNC_040)GetProcAddress(m_hLib, "CeWriteTxt");
	pCeTCut = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeTCut");
	pCePCut = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CePCut");
	pCePaperFeed = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CePaperFeed");
	pCeLF = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeLF");
	pCeCR = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeCR");
	pCeCashDrawer = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CeCashDrawer");
	pCeSetJustification = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSetJustification");
	pCeSetTextMode = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSetTextMode");
	pCeSelectHRIPosition = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSelectHRIPosition");
	pCeSelectHRIFont = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSelectHRIFont");
	pCeSetBarCodeHeigth = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSetBarCodeHeigth");
	pCeSetBarCodeWidth = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSetBarCodeWidth");
	pCePrintBarCode = (LPFNDLLFUNC_045)GetProcAddress(m_hLib, "CePrintBarCode");
	
	if( (pCeWriteTxt && pCeTCut && pCePCut && pCePaperFeed &&
		 pCeLF && pCeCashDrawer) == NULL )
		return;	

	if(hObj)
	{
		bytFontFormat = 0x01;	// centered
		dwError = pCeSetJustification(hObj, bytFontFormat, &dwSysError);

		bytTextFormatType = 0x70;
		dwError = pCeSetTextMode(hObj, bytTextFormatType, &dwSysError);
		dwError = pCeWriteTxt(hObj, " Test For Q1 Printer ", &dwSysError);

		bytFontFormat = 0x00;	// left
		dwError = pCeSetJustification(hObj, bytFontFormat, &dwSysError);

		bytTextFormatType = 0x00;
		dwError = pCeSetTextMode(hObj, bytTextFormatType, &dwSysError);

		dwError = pCeLF(hObj, bytNLF, &dwSysError);		

		dwError = pCeWriteTxt(hObj, "*********** Test For Q1 Printer *********", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "              Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "******** End Test For Q1 Printer ********", &dwSysError);

		dwError = pCeCashDrawer(hObj, dwCashDraweerMode, &dwSysError); // just for test
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError); // just for test
		dwError = pCeLF(hObj, bytNLF, &dwSysError);		
		dwError = pCeTCut(hObj, &dwSysError);
	}
}


void CCePrintDlg::TestBarCodes(HANDLE hObj)
{
	// Print Bar Code

	DWORD dwSysError = 0;
	DWORD dwError = 0;
	BYTE bytNLF = 3;
	BYTE bytTextFormatType = 0x01;
	BYTE bytFontFormat = 0x00;

	// 1) get needed Exported API CePrint.Dll library
	LPFNDLLFUNC_040 pCeWriteTxt = NULL;
	LPFNDLLFUNC_041 pCeTCut = NULL;
	LPFNDLLFUNC_042 pCeLF = NULL;
	LPFNDLLFUNC_042 pCeCR = NULL;
	LPFNDLLFUNC_042 pCeSetJustification = NULL;
	LPFNDLLFUNC_042 pCeSetTextMode = NULL;
	LPFNDLLFUNC_042 pCeSelectHRIPosition = NULL;
	LPFNDLLFUNC_042 pCeSelectHRIFont = NULL;
	LPFNDLLFUNC_042 pCeSetBarCodeHeigth = NULL;
	LPFNDLLFUNC_042 pCeSetBarCodeWidth = NULL;
	LPFNDLLFUNC_045 pCePrintBarCode = NULL;
	LPFNDLLFUNC_041 pCeEject = NULL;


	pCeWriteTxt = (LPFNDLLFUNC_040)GetProcAddress(m_hLib, "CeWriteTxt");
	pCeTCut = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeTCut");
	pCeLF = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeLF");
	pCeCR = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeCR");
	pCeSetJustification = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSetJustification");
	pCeSetTextMode = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSetTextMode");
	pCeSelectHRIPosition = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSelectHRIPosition");
	pCeSelectHRIFont = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSelectHRIFont");
	pCeSetBarCodeHeigth = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSetBarCodeHeigth");
	pCeSetBarCodeWidth = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSetBarCodeWidth");
	pCePrintBarCode = (LPFNDLLFUNC_045)GetProcAddress(m_hLib, "CePrintBarCode");
	pCeEject = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeEject");
	
	if( (pCeWriteTxt && pCeTCut && pCeCR && pCeSetJustification &&
		 pCeLF && pCeSetTextMode && pCeSelectHRIPosition &&
		 pCeSelectHRIFont && pCeSetBarCodeHeigth && pCeSetBarCodeWidth &&
		 pCePrintBarCode) == NULL )
		return;	

	if(hObj)
	{
		bytFontFormat = 0x01;	// centered
		dwError = pCeSetJustification(hObj, bytFontFormat, &dwSysError);

		bytTextFormatType = 0x00;
		dwError = pCeSetTextMode(hObj, bytTextFormatType, &dwSysError);

		dwError = pCeWriteTxt(hObj, "** UPC-A Bar Code Example **", &dwSysError);
		dwError = pCeCR(hObj, 1, &dwSysError);		
		dwError = pCeLF(hObj, 1, &dwSysError);	


		BYTE bytHRIFont = 0x00;	// Font A
		dwError = pCeSelectHRIFont(hObj, bytHRIFont, &dwSysError);

		BYTE bytBarCodeHeigth = 200;	// 25mm
		dwError = pCeSetBarCodeHeigth(hObj, bytBarCodeHeigth, &dwSysError);

		BYTE bytBarCodeWidth = 0x03;	//0.375mm
		dwError = pCeSetBarCodeWidth(hObj, bytBarCodeWidth, &dwSysError);

		BYTE bytHRIPosition = 0x02; // HRI below the bar code
		dwError = pCeSelectHRIPosition(hObj, bytHRIPosition, &dwSysError);

		LPCSTR pstrAsciiCode = "051197895475";
		UINT uiCharSize = (UINT)strlen(pstrAsciiCode);
		LPBYTE lpData = NULL;
		lpData = new BYTE[uiCharSize];
		CopyMemory(lpData, pstrAsciiCode, uiCharSize);

		//Print UPC-A bar code
		dwError = pCePrintBarCode(hObj, UPC_A, uiCharSize, lpData, &dwSysError);
		if(lpData)
			delete[] lpData;

		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		
		// Line Feed
		dwError = pCeLF(hObj, 2, &dwSysError);	

		//Print UPC-E bar code

		dwError = pCeWriteTxt(hObj, "** Equivalent UPC-E **", &dwSysError);
		dwError = pCeCR(hObj, 1, &dwSysError);		
		dwError = pCeLF(hObj, 1, &dwSysError);	

		pstrAsciiCode = "051197895475";
		uiCharSize = (UINT)strlen(pstrAsciiCode);
		lpData = NULL;
		lpData = new BYTE[uiCharSize];
		CopyMemory(lpData, pstrAsciiCode, uiCharSize);

		dwError = pCePrintBarCode(hObj, UPC_E, uiCharSize, lpData, &dwSysError);
		if(lpData)
			delete[] lpData;

		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		
		// Line Feed
		dwError = pCeLF(hObj, 2, &dwSysError);	


		//Print EAN13(JAN) bar code

		dwError = pCeWriteTxt(hObj, "** EAN13(JAN) Bar Code Example **", &dwSysError);
		dwError = pCeCR(hObj, 1, &dwSysError);		
		dwError = pCeLF(hObj, 1, &dwSysError);	

		pstrAsciiCode = "0511978954759";
		uiCharSize = (UINT)strlen(pstrAsciiCode);
		lpData = NULL;
		lpData = new BYTE[uiCharSize];
		CopyMemory(lpData, pstrAsciiCode, uiCharSize);

		dwError = pCePrintBarCode(hObj, EAN13, uiCharSize, lpData, &dwSysError);
		if(lpData)
			delete[] lpData;


		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		
		// Line Feed
		dwError = pCeLF(hObj, 2, &dwSysError);	


		//Print EAN8(JAN) bar code

		dwError = pCeWriteTxt(hObj, "** EAN8(JAN) Bar Code Example **", &dwSysError);
		dwError = pCeCR(hObj, 1, &dwSysError);		
		dwError = pCeLF(hObj, 1, &dwSysError);	

		pstrAsciiCode = "05497526";
		uiCharSize = (UINT)strlen(pstrAsciiCode);
		lpData = NULL;
		lpData = new BYTE[uiCharSize];
		CopyMemory(lpData, pstrAsciiCode, uiCharSize);

		dwError = pCePrintBarCode(hObj, EAN8, uiCharSize, lpData, &dwSysError);
		if(lpData)
			delete[] lpData;


		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		
		// Line Feed
		dwError = pCeLF(hObj, 2, &dwSysError);	


		//Print CODE39 bar code

		dwError = pCeWriteTxt(hObj, "** CODE39 Bar Code Example **", &dwSysError);
		dwError = pCeCR(hObj, 1, &dwSysError);		
		dwError = pCeLF(hObj, 1, &dwSysError);	

		pstrAsciiCode = "TEST5689";
		uiCharSize = (UINT)strlen(pstrAsciiCode);
		lpData = NULL;
		lpData = new BYTE[uiCharSize];
		CopyMemory(lpData, pstrAsciiCode, uiCharSize);

		dwError = pCePrintBarCode(hObj, CODE39, uiCharSize, lpData, &dwSysError);
		if(lpData)
			delete[] lpData;


		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		
		// Line Feed
		dwError = pCeLF(hObj, 2, &dwSysError);	


		//Print ITF bar code

		dwError = pCeWriteTxt(hObj, "** ITF Bar Code Example **", &dwSysError);
		dwError = pCeCR(hObj, 1, &dwSysError);		
		dwError = pCeLF(hObj, 1, &dwSysError);	

		pstrAsciiCode = "9863548712598457";
		uiCharSize = (UINT)strlen(pstrAsciiCode);
		lpData = NULL;
		lpData = new BYTE[uiCharSize];
		CopyMemory(lpData, pstrAsciiCode, uiCharSize);

		dwError = pCePrintBarCode(hObj, ITF, uiCharSize, lpData, &dwSysError);
		if(lpData)
			delete[] lpData;


		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		
		// Line Feed
		dwError = pCeLF(hObj, 2, &dwSysError);	


		//Print CODABAR bar code

		dwError = pCeWriteTxt(hObj, "** CODABAR Bar Code Example **", &dwSysError);
		dwError = pCeCR(hObj, 1, &dwSysError);		
		dwError = pCeLF(hObj, 1, &dwSysError);	

		pstrAsciiCode = "ABCD457";
		uiCharSize = (UINT)strlen(pstrAsciiCode);
		lpData = NULL;
		lpData = new BYTE[uiCharSize];
		CopyMemory(lpData, pstrAsciiCode, uiCharSize);

		dwError = pCePrintBarCode(hObj, CODABAR, uiCharSize, lpData, &dwSysError);
		if(lpData)
			delete[] lpData;


		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		
		// Line Feed
		dwError = pCeLF(hObj, 2, &dwSysError);	


		//Print CODE93 bar code

		dwError = pCeWriteTxt(hObj, "** CODE93 Bar Code Example **", &dwSysError);
		dwError = pCeCR(hObj, 1, &dwSysError);		
		dwError = pCeLF(hObj, 1, &dwSysError);	

		pstrAsciiCode = "CODE{93}";
		uiCharSize = (UINT)strlen(pstrAsciiCode);
		lpData = NULL;
		lpData = new BYTE[uiCharSize];
		CopyMemory(lpData, pstrAsciiCode, uiCharSize);

		dwError = pCePrintBarCode(hObj, CODE93, uiCharSize, lpData, &dwSysError);
		if(lpData)
			delete[] lpData;


		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		
		// Line Feed
		dwError = pCeLF(hObj, 2, &dwSysError);	


		//Print CODE32 bar code

		dwError = pCeWriteTxt(hObj, "** CODE32 Bar Code Example **", &dwSysError);
		dwError = pCeCR(hObj, 1, &dwSysError);		
		dwError = pCeLF(hObj, 1, &dwSysError);	

		pstrAsciiCode = "98574214";
		uiCharSize = (UINT)strlen(pstrAsciiCode);
		lpData = NULL;
		lpData = new BYTE[uiCharSize];
		CopyMemory(lpData, pstrAsciiCode, uiCharSize);

		dwError = pCePrintBarCode(hObj, CODE32, uiCharSize, lpData, &dwSysError);
		if(lpData)
			delete[] lpData;


		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		
		// Line Feed
		dwError = pCeLF(hObj, 2, &dwSysError);	


		//Print CODE128 bar code

		bytBarCodeWidth = 0x02;	//0.375mm
		dwError = pCeSetBarCodeWidth(hObj, bytBarCodeWidth, &dwSysError);

		dwError = pCeWriteTxt(hObj, "** CODE128 Bar Code Example **", &dwSysError);
		dwError = pCeCR(hObj, 1, &dwSysError);		
		dwError = pCeLF(hObj, 1, &dwSysError);	

		pstrAsciiCode = "{A$WIN${C2500{B3)ds{C14";
		uiCharSize = (UINT)strlen(pstrAsciiCode);
		lpData = NULL;
		lpData = new BYTE[uiCharSize];
		CopyMemory(lpData, pstrAsciiCode, uiCharSize);

		dwError = pCePrintBarCode(hObj, CODE128, uiCharSize, lpData, &dwSysError);
		if(lpData)
			delete[] lpData;



		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		
		// Line Feed
		dwError = pCeLF(hObj, 6, &dwSysError);	

		// Cut
		dwError = pCeTCut(hObj, &dwSysError);

		// Eject
		dwError = pCeEject(hObj, &dwSysError);	
	}
}

void CCePrintDlg::OnBnClickedButtonTestQ1()
{
	// TODO: Add your control notification handler code here
	if(m_hComObj)
		TestQ1(m_hComObj);
}

void CCePrintDlg::TestTg2460(HANDLE hObj)
{
	// TODO: Add your control notification handler code here
	DWORD dwSysError = 0;
	DWORD dwError = 0;
	DWORD dwFeed = 0x000001E0;
	BYTE bytNLF = 3;

	// 1) get needed Exported API CePrint.Dll library
	LPFNDLLFUNC_040 pCeWriteTxt = NULL;
	LPFNDLLFUNC_041 pCeTCut = NULL;
	LPFNDLLFUNC_043 pCePaperFeedTG2460 = NULL;

	pCeWriteTxt = (LPFNDLLFUNC_040)GetProcAddress(m_hLib, "CeWriteTxt");
	pCeTCut = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeTCut");
	pCePaperFeedTG2460 = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CePaperFeedTG2460");	
	
	if( (pCeWriteTxt && pCeTCut && pCePaperFeedTG2460) == NULL )
		return;

	if(hObj)
	{
		dwError = pCeWriteTxt(hObj, "*** Test For TG2460 Printer ***", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "              Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "* End Test For TG2460 Printer *", &dwSysError);

		dwError = pCePaperFeedTG2460(hObj, dwFeed, &dwSysError);	// OK				
		dwError = pCeTCut(hObj, &dwSysError);	// OK
	}
}

void CCePrintDlg::PrintUnicodeText(HANDLE hObj)
{
	// TODO: Add your control notification handler code here
	DWORD dwSysError = 0;
	DWORD dwError = 0;
	BYTE bytNLF = 3;
	BYTE bytPresZize = 5;
	DWORD dwFeed = 40;

	// 1) get needed Exported API CePrint.Dll library
	LPFNDLLFUNC_041 pCeTCut = NULL;
	LPFNDLLFUNC_046 pCeWriteUnicodeTxt = NULL;
	LPFNDLLFUNC_043	pCePresent = NULL;
	LPFNDLLFUNC_043 pCePaperFeed = NULL;
	LPFNDLLFUNC_041 pCeEject = NULL;
	LPFNDLLFUNC_042 pCeCR = NULL;

	pCeTCut = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeTCut");
	pCeWriteUnicodeTxt = (LPFNDLLFUNC_046)GetProcAddress(m_hLib, "CeWriteUnicodeTxt");	
	pCePresent = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CePresent");
	pCePaperFeed = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CePaperFeed");
	pCeEject = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeEject");
	pCeCR = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeCR");
	
	if( (pCeWriteUnicodeTxt && pCeTCut && pCePresent) == NULL )
		return;

	if(hObj)
	{
//		dwError = pCeWriteUnicodeTxt(hObj, (LPCWSTR)m_strUnicodeText, 20936, &dwSysError);
		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		

		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
//		dwError = pCeWriteUnicodeTxt(hObj, (LPCWSTR)m_strUnicodeText, 20936, &dwSysError);
		// Carriage Return
		dwError = pCeCR(hObj, 1, &dwSysError);		

		dwError = pCeTCut(hObj, &dwSysError);	// OK
		dwError = pCePresent(hObj, bytPresZize, &dwSysError);
		dwError = pCeEject(hObj, &dwSysError);	// OK
	}
}

void CCePrintDlg::OnBnClickedButtonTestTg2460()
{
	// TODO: Add your control notification handler code here
	if(m_hComObj)
		TestTg2460(m_hComObj);
}

void CCePrintDlg::TestTg2480(HANDLE hObj)
{
	// TODO: Add your control notification handler code here
	DWORD dwSysError = 0;
	DWORD dwError = 0;
	DWORD dwFeed = 0x00000030;

	// 1) get needed Exported API CePrint.Dll library
	LPFNDLLFUNC_040 pCeWriteTxt = NULL;
	LPFNDLLFUNC_041 pCeTCut = NULL;
	LPFNDLLFUNC_043 pCePaperFeedTG2460 = NULL;

	pCeWriteTxt = (LPFNDLLFUNC_040)GetProcAddress(m_hLib, "CeWriteTxt");
	pCeTCut = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeTCut");
	pCePaperFeedTG2460 = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CePaperFeedTG2460");
	
	if( (pCeWriteTxt && pCeTCut && pCePaperFeedTG2460) == NULL )
		return;

	if(hObj)
	{
		dwError = pCeWriteTxt(hObj, "******** Test For TG2480 Printer *******", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "              Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "              Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "***** End Test For TG2480 Printer *****", &dwSysError);

		dwError = pCePaperFeedTG2460(hObj, dwFeed, &dwSysError);				
		dwError = pCeTCut(hObj, &dwSysError);
	}
}

void CCePrintDlg::OnBnClickedButtonTestTg2480()
{
	// TODO: Add your control notification handler code here
	if(m_hComObj)
		TestTg2480(m_hComObj);
}

void CCePrintDlg::TestVkp80ii(HANDLE hObj)
{
	DWORD dwSysError = 0;
	DWORD dwError = 0;
	DWORD dwFeed = 40;
	DWORD dwNotchDist = 0x00000960; // 2400 dot line 300mm (200dpi)
	BYTE bytPresZize = 5;
	BYTE bytTimeOut = 5;
	BYTE bytNLF = 3;

	// 1) get needed Exported API CePrint.Dll library
	LPFNDLLFUNC_040 pCeWriteTxt = NULL;
	LPFNDLLFUNC_041 pCeTCut = NULL;
	LPFNDLLFUNC_043 pCePaperFeed = NULL;

	LPFNDLLFUNC_043 pCeSetNotchDist = NULL;
	LPFNDLLFUNC_042 pCePrnContinousMode = NULL;
	LPFNDLLFUNC_041 pCeNotchAlgnCut = NULL;
	LPFNDLLFUNC_041 pCeNotchAlgnPrn = NULL;

	LPFNDLLFUNC_043	pCePresent = NULL;
	LPFNDLLFUNC_044 pCePresentTimeout = NULL;
	LPFNDLLFUNC_041 pCeEject = NULL;
	LPFNDLLFUNC_041 pCeRetract = NULL;

	pCeWriteTxt = (LPFNDLLFUNC_040)GetProcAddress(m_hLib, "CeWriteTxt");
	pCeTCut = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeTCut");
	pCePaperFeed = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CePaperFeed");
	
	pCeSetNotchDist = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CeSetNotchDist");
	pCeNotchAlgnCut = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeNotchAlgnCut");
	pCeNotchAlgnPrn = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeNotchAlgnPrn");
	pCePrnContinousMode = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CePrnContinousMode");

	pCePresent = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CePresent");
	pCePresentTimeout = (LPFNDLLFUNC_044)GetProcAddress(m_hLib, "CePresentTimeout");
	pCeEject = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeEject");
	pCeRetract = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeRetract");

	if( (pCeWriteTxt && pCeSetNotchDist && pCeSetNotchDist && pCeNotchAlgnPrn && 
		 pCePresent && pCePresentTimeout && pCeEject && pCeRetract && pCePaperFeed) == NULL )
		return;

	if(hObj)
	{
		dwError = pCePrnContinousMode(hObj, 0x00, &dwSysError);
		//dwError = pCePrnContinousMode(hObj, 0x01, &dwSysError);

		//dwError = pCeSetNotchDist(hObj, dwNotchDist, &dwSysError);		
		//dwError = pCeNotchAlgnPrn(hObj, &dwSysError);

		dwError = pCeWriteTxt(hObj, "******** Test For VKP80 Printer *******", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "              Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "              Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "            Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "          Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "        Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "      Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "    Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "  Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "Line of Text", &dwSysError);
		dwError = pCeWriteTxt(hObj, "***** End Test For VKP80 Printer *****", &dwSysError);

		//dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);

		//dwError = pCeNotchAlgnCut(hObj, &dwSysError);
		dwError = pCeTCut(hObj, &dwSysError);

		//dwError = pCePresent(hObj, bytPresZize, &dwSysError);		
		// dwError = pCeEject(hObj, &dwSysError);	// OK
		// dwError = pCeRetract(hObj, &dwSysError);	// OK		
		dwError = pCePresentTimeout(hObj, bytPresZize, bytTimeOut, &dwSysError); // OK
	}
}

void CCePrintDlg::TestVkp80iiCH(HANDLE hObj)
{
	DWORD dwSysError = 0;
	DWORD dwError = 0;
	DWORD dwFeed = 40;
	DWORD dwNotchDist = 0x00000960; // 2400 dot line 300mm (200dpi)
	BYTE bytPresZize = 5;
	BYTE bytTimeOut = 5;
	BYTE bytNLF = 3;
	BYTE bytTextFormatType = 0x01;
	BYTE bytFontFormat = 0x00;

	// 1) get needed Exported API CePrint.Dll library
	LPFNDLLFUNC_040 pCeWriteTxt = NULL;
	LPFNDLLFUNC_046 pCeWriteUnicodeTxt = NULL;
	LPFNDLLFUNC_041 pCeTCut = NULL;
	LPFNDLLFUNC_043 pCePaperFeed = NULL;
	LPFNDLLFUNC_002 pCeWrite = NULL;

	LPFNDLLFUNC_043 pCeSetNotchDist = NULL;
	LPFNDLLFUNC_042 pCePrnContinousMode = NULL;
	LPFNDLLFUNC_041 pCeNotchAlgnCut = NULL;
	LPFNDLLFUNC_041 pCeNotchAlgnPrn = NULL;

	LPFNDLLFUNC_043	pCePresent = NULL;
	LPFNDLLFUNC_044 pCePresentTimeout = NULL;
	LPFNDLLFUNC_041 pCeEject = NULL;
	LPFNDLLFUNC_041 pCeRetract = NULL;
	LPFNDLLFUNC_042 pCeCR = NULL;
	LPFNDLLFUNC_042 pCeSetJustification = NULL;
	LPFNDLLFUNC_042 pCeSetTextMode = NULL;

	pCeWriteTxt = (LPFNDLLFUNC_040)GetProcAddress(m_hLib, "CeWriteTxt");
	pCeTCut = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeTCut");
	pCePaperFeed = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CePaperFeed");
	
	pCeSetNotchDist = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CeSetNotchDist");
	pCeNotchAlgnCut = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeNotchAlgnCut");
	pCeNotchAlgnPrn = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeNotchAlgnPrn");
	pCePrnContinousMode = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CePrnContinousMode");
	pCeSetJustification = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSetJustification");
	pCeSetTextMode = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeSetTextMode");

	pCePresent = (LPFNDLLFUNC_043)GetProcAddress(m_hLib, "CePresent");
	pCePresentTimeout = (LPFNDLLFUNC_044)GetProcAddress(m_hLib, "CePresentTimeout");
	pCeEject = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeEject");
	pCeRetract = (LPFNDLLFUNC_041)GetProcAddress(m_hLib, "CeRetract");
	pCeWriteUnicodeTxt = (LPFNDLLFUNC_046)GetProcAddress(m_hLib, "CeWriteUnicodeTxt");
	pCeWrite = (LPFNDLLFUNC_002)GetProcAddress(m_hLib, "CeWrite");
	pCeCR = (LPFNDLLFUNC_042)GetProcAddress(m_hLib, "CeCR");

	if( (pCeWriteTxt && pCeSetNotchDist && pCeSetNotchDist && pCeNotchAlgnPrn && 
		 pCePresent && pCePresentTimeout && pCeEject && pCeRetract && pCePaperFeed) == NULL )
		return;

	if(hObj)
	{
		dwError = pCePrnContinousMode(hObj, 0x00, &dwSysError);
		bytFontFormat = 0x00;	// left
		dwError = pCeSetJustification(hObj, bytFontFormat, &dwSysError);

		bytTextFormatType = 0x00;
		dwError = pCeSetTextMode(hObj, bytTextFormatType, &dwSysError);

		dwError = pCeWriteUnicodeTxt(hObj, _T("   ******* Test For VKP80 Printer *******"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("    ********** 试验VKP80打印机 **********"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  Line of Text"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  行文字"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("    Line of Text"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("    行文字"), 6, 0, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("     ******* Printer Features *******"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("      ********* 打印机功能 **********"), 6, 0, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T(" ╔═══════════════════════════════════════╗"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  Paper Width:			60-82.5mm"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  纸张宽度:			60-82.5mm"), 6, 0, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  Printing Speed:		220mm/s"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  打印速度:			220mm/s"), 6, 0, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  Resolution:			203dpi"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  分辨率:			203dpi"), 6, 0, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  Power Supply:			24V"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  电力供应:			24V"), 6, 0, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  Interface:			RS232/USB"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("  接口:				RS232/USB"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T(" ╚═══════════════════════════════════════╝"), 6, 0, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("   ***** End Test For VKP80 Printer *****"), 6, 0, &dwSysError);
		dwError = pCeWriteUnicodeTxt(hObj, _T("    ******* 结束试验VKP80打印机r *******"), 6, 0, &dwSysError);
		dwError = pCePaperFeed(hObj, dwFeed, &dwSysError);
		
		dwError = pCeTCut(hObj, &dwSysError);

		//dwError = pCePresent(hObj, bytPresZize, &dwSysError);		
		// dwError = pCeEject(hObj, &dwSysError);	// OK
		// dwError = pCeRetract(hObj, &dwSysError);	// OK		
		dwError = pCePresentTimeout(hObj, bytPresZize, bytTimeOut, &dwSysError); // OK

	}
}

void CCePrintDlg::OnBnClickedButtonTestVkp80ii()
{
	// TODO: Add your control notification handler code here
	if(m_hComObj)
		TestVkp80ii(m_hComObj);
}

void CCePrintDlg::OnBnClickedButtonTestTg2480Usb()
{
	// TODO: Add your control notification handler code here
	if(m_hUSBObj)
		TestTg2480(m_hUSBObj);
}

void CCePrintDlg::OnBnClickedButtonTestVkp80iiUsb()
{
	// TODO: Add your control notification handler code here
	if(m_hUSBObj)
		TestVkp80ii(m_hUSBObj);
}

void CCePrintDlg::OnBnClickedButtonTestTg2460Usb()
{
	// TODO: Add your control notification handler code here
	if(m_hUSBObj)
		TestTg2460(m_hUSBObj);
}

void CCePrintDlg::OnBnClickedButtonTestQ1Usb()
{
	// TODO: Add your control notification handler code here
	if(m_hUSBObj)
		TestQ1(m_hUSBObj);
}

void CCePrintDlg::OnBnClickedButtonTestBarCodesUsb()
{
	// TODO: Add your control notification handler code here
	if(m_hUSBObj)
		TestBarCodes(m_hUSBObj);
}

void CCePrintDlg::OnBnClickedButtonTestBarCodesCom()
{
	// TODO: Add your control notification handler code here
	if(m_hComObj)
		TestBarCodes(m_hComObj);
}

void CCePrintDlg::OnBnClickedButtonPrintUnicodeString()
{
	// TODO: Add your control notification handler code here
	UpdateData(TRUE);

	if(m_hComObj)
		PrintUnicodeText(m_hComObj);
	else if(m_hUSBObj)
		PrintUnicodeText(m_hUSBObj);
}

void CCePrintDlg::OnBnClickedButtonTestVkp80iichUsb()
{
	// TODO: Add your control notification handler code here
	if(m_hUSBObj)
		TestVkp80iiCH(m_hUSBObj);
}

void CCePrintDlg::OnBnClickedButtonTestVkp80iichCom()
{
	// TODO: Add your control notification handler code here
	if(m_hComObj)
		TestVkp80iiCH(m_hComObj);
}
