//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CePrint.rc
//
#define VS_VERSION_INFO_x64             2
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CEPRINT_DIALOG              102
#define IDS_STR_LIB_NOT_LOADED          102
#define IDS_STR_PROC_ERR                103
#define IDS_STR_FNCT_PNTR_NULL          104
#define IDS_STR_COMPORT_NOT_INIT        105
#define IDS_STR_INVALID_COM_HND         106
#define IDS_STR_API_COM_ERROR           107
#define IDS_STR_ERROR                   108
#define IDS_STR_NO_ENOUGHT_RESOURCE_TO_LOAD_FILES 109
#define IDS_STR_NO_ALL_BYTE_FILES_READ  110
#define IDS_STR_GET_CURRENT_PATH        111
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_COM_TIMEOUT          129
#define IDD_DIALOG_COM_SETTINGS         132
#define IDC_EDIT_PRN_ANSWER             1000
#define IDC_BUTTON_INITCOM              1001
#define IDC_EDIT_LIB_ERR                1002
#define IDC_EDIT_COM_NAME               1003
#define IDC_BUTTON_GET_GSI              1004
#define IDC_EDIT_GSI                    1005
#define IDC_EDIT_SYS_ERR                1006
#define IDC_EDIT_STR_COM_CFG            1007
#define IDC_BUTTON_GET_DEF_TIMEOUT      1008
#define IDC_BUTTON_GET_CUR_TIMEOUT      1009
#define IDC_BUTTON_SET_CUR_TIMEOUT      1010
#define IDC_BUTTON_GET_DEF_COM_CFG      1011
#define IDC_BUTTON_GET_CURR_COM_CFG     1012
#define IDC_BUTTON_SET_DEF_COM_CFG3     1013
#define IDC_BUTTON_GET_STATUS           1014
#define IDC_BUTTON_GET_COM_HND          1015
#define IDC_BUTTON_WRITE_PRN_FILE       1016
#define IDC_BUTTON_CLOSE_COM            1017
#define IDC_EDIT_PATH_FILE              1018
#define IDC_EDIT_PRN_NAME               1019
#define IDC_EDIT_RIT                    1019
#define IDC_BUTTON_WRITE_PRN_FILE2      1020
#define IDC_EDIT_RTTM                   1020
#define IDC_EDIT_RTTC                   1021
#define IDC_BUTTON_TEST                 1021
#define IDC_EDIT_WTTM                   1022
#define IDC_BUTTON_NEW_SET_COM          1022
#define IDC_BUTTON_SPOOL_FILE           1023
#define IDC_EDIT_WTTC                   1023
#define IDC_BUTTON_INITUSB              1024
#define IDC_BUTTON_DEINITUSB            1025
#define IDC_BUTTON_OPENUSBDEV           1026
#define IDC_BUTTON_GET_GSI_USB          1027
#define IDC_EDIT_GSI_USB                1028
#define IDC_BUTTON_GET_STATUS_USB       1029
#define IDC_EDIT_STATUS_USB             1030
#define IDC_BUTTON_WRITE_PRN_FILE_USB   1031
#define IDC_BUTTON_OPEN_PRN_FILE_USB    1032
#define IDC_BUTTON_SPOOL_FILE_USB       1033
#define IDC_EDIT_PATH_FILE_USB          1034
#define IDC_BUTTON_GET_FW_REL_USB       1035
#define IDC_EDIT_FW_REL_USB             1036
#define IDC_BUTTON_GET_FWREL            1037
#define IDC_BUTTON_CFG_COM              1038
#define IDC_STATIC_TICKET_TEST          1039
#define IDC_BUTTON_TEST_Q1              1040
#define IDC_STATIC_TICKET_TEST_USB      1041
#define IDC_BUTTON_TEST_TG2460          1042
#define IDC_BUTTON_TEST_TG2480          1043
#define IDC_BUTTON_TEST_VKP80II         1044
#define IDC_BUTTON_TEST_TG248_USB       1045
#define IDC_BUTTON_TEST_Q1_USB          1046
#define IDC_BUTTON_TEST_TG2460_USB      1047
#define IDC_COMBO_BAUD                  1048
#define IDC_BUTTON_TEST_VKP80II2        1048
#define IDC_BUTTON_TEST_VKP80II_USB     1048
#define IDC_BUTTON_TEST_BAR_CODES_USB   1049
#define IDC_COMBO_DATA                  1050
#define IDC_STATIC_TICKET_TEST_USB2     1050
#define IDC_BUTTON_TEST_BAR_CODES_COM   1051
#define IDC_COMBO_PARITY                1052
#define IDC_STATIC_TICKET_TEST_USB3     1052
#define IDC_BUTTON_PRINT_UNICODE_STRING 1053
#define IDC_COMBO_STOP                  1054
#define IDC_STATIC_TICKET_TEST_USB4     1054
#define IDC_EDIT_PRN_NAME2              1055
#define IDC_EDIT_UNICODE_STRING         1055
#define IDC_COMBO_FC                    1056
#define IDC_EDIT_SEL_CODE_PAGE          1056
#define IDC_BUTTON_TEST_BAR_CODES_USB2  1057
#define IDC_BUTTON_TEST_VKP80IICH_USB   1057
#define IDC_STATIC_TICKET_TEST_USB5     1058
#define IDC_STATIC_TICKET_TEST_USB6     1060
#define IDC_BUTTON_TEST_VKP80IICH_COM   1061

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        134
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1062
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
