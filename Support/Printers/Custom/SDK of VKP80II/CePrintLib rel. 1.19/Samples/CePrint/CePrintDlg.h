// CePrintDlg.h : header file
//

#pragma once
#include "afxwin.h"

// function pointer typedef
typedef DWORD (*LPFNDLLFUNC_000)(LPHANDLE, LPCTSTR, LPDWORD);
typedef DWORD (*LPFNDLLFUNC_001)(HANDLE);
typedef DWORD (*LPFNDLLFUNC_002)(HANDLE, LPBYTE, DWORD, LPDWORD, LPDWORD);
typedef DWORD (*LPFNDLLFUNC_003)(HANDLE, LPBYTE, DWORD, LPDWORD, LPBYTE, DWORD, LPDWORD, LPDWORD);
typedef DWORD (*LPFNDLLFUNC_004)(HANDLE, LPTSTR, LPDWORD);

typedef DWORD (*LPFNDLLFUNC_005)(HANDLE, LPDCB);
typedef DWORD (*LPFNDLLFUNC_006)(HANDLE, DCB, LPDWORD);

typedef DWORD (*LPFNDLLFUNC_007)(HANDLE, COMMTIMEOUTS*);
typedef DWORD (*LPFNDLLFUNC_008)(HANDLE, COMMTIMEOUTS, LPDWORD);

typedef DWORD (*LPFNDLLFUNC_009)(HANDLE, LPHANDLE);
typedef DWORD (*LPFNDLLFUNC_010)(LPCTSTR, LPBYTE, DWORD, LPDWORD);

typedef DWORD (*LPFNDLLFUNC_011)(HANDLE, DWORD, BYTE, BYTE, BYTE, DWORD, LPDWORD);
typedef DWORD (*LPFNDLLFUNC_012)(HANDLE, LPDWORD, LPDWORD);
typedef DWORD (*LPFNDLLFUNC_013)(HANDLE, LPBYTE, LPDWORD, LPDWORD);

typedef DWORD (*LPFNDLLFUNC_020)(LPHANDLE, LPDWORD);
typedef DWORD (*LPFNDLLFUNC_021)(HANDLE);

typedef DWORD (*LPFNDLLFUNC_022)(HANDLE, LPCTSTR, LPDWORD);
//typedef DWORD (*LPFNDLLFUNC_023)(HANDLE, LPCTSTR);

typedef DWORD (*LPFNDLLFUNC_023)(HANDLE, LPBYTE, DWORD, LPDWORD, LPDWORD);
typedef DWORD (*LPFNDLLFUNC_024)(HANDLE, LPBYTE, DWORD, LPDWORD, LPBYTE, DWORD, LPDWORD, LPDWORD);


typedef DWORD (*LPFNDLLFUNC_040)(HANDLE, LPCSTR, LPDWORD);		// CeWriteTxt typedef pointer
typedef DWORD (*LPFNDLLFUNC_041)(HANDLE, LPDWORD);				// CeTCut, CePCut, CeTCutTG2460, CeEject,
																// CeRetract, CeNotchAlgnCut, CeNotchAlgnPrn  typedef pointer

typedef DWORD (*LPFNDLLFUNC_042)(HANDLE, BYTE, LPDWORD);		// CePrnContinousMode, CePresent, CeLF, CeSetTextMode, CeSetJustification, CeSelectHRIPosition, CeSelectHRIFont, CeSetBarCodeHeigth, CeSetBarCodeWidth
typedef DWORD (*LPFNDLLFUNC_043)(HANDLE, DWORD, LPDWORD);		// CePaperFeed, CePaperFeedTG2460, CeCashDrawer, CeSetNotchDist
typedef DWORD (*LPFNDLLFUNC_044)(HANDLE, BYTE, BYTE, LPDWORD);	// CePresentTimeout
typedef DWORD (*LPFNDLLFUNC_045)(HANDLE, BYTE, UINT, PBYTE, LPDWORD);	// CePrintBarCode
typedef DWORD (*LPFNDLLFUNC_046)(HANDLE, LPCWSTR, UINT, UINT, LPDWORD);		// CeWriteUnicodeTxt

// CCePrintDlg dialog
class CCePrintDlg : public CDialog
{
// Construction
public:
	CCePrintDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_CEPRINT_DIALOG };

protected:
	void FreeAllResource();
	void ErrorUnsupFnct();

	LPFNDLLFUNC_000 m_lpfnCeOpenCom;
	LPFNDLLFUNC_001 m_lpfnCeCloseCom;
	LPFNDLLFUNC_002 m_lpfnCeWriteCom;
	LPFNDLLFUNC_003 m_lpfnCeWriteReadCom;
	LPFNDLLFUNC_004 m_lpfnCeGetStrComConfig;

	LPFNDLLFUNC_005 m_lpfnCeGetDefComCfg;
	LPFNDLLFUNC_005 m_lpfnCeGetComCfg;
	LPFNDLLFUNC_006 m_lpfnCeSetComCfg;

	LPFNDLLFUNC_007 m_lpfnCeGetDefComTimeout;
	LPFNDLLFUNC_007 m_lpfnCeGetComTimeout;
	LPFNDLLFUNC_008 m_lpfnCeSetComTimeout;

	LPFNDLLFUNC_009 m_lpfnCeGethPortCom;
	LPFNDLLFUNC_010 m_lpfnCeSpoolBufferToPrn;

	LPFNDLLFUNC_011 m_lpfnCeSetComCfgHR;
	LPFNDLLFUNC_012 m_lpfnCeGetSts;
	LPFNDLLFUNC_012 m_lpfnCeGetGSI;
	LPFNDLLFUNC_013 m_lpfnCeGetFWRel;

	BOOL SendFileToCom(CString strNomeFile);

	LPFNDLLFUNC_020 m_lpfnCeInitUSBLayer;
	LPFNDLLFUNC_021 m_lpfnCeDeInitUSBLayer;

	LPFNDLLFUNC_022 m_lpfnCeOpenUSBDev;
//	LPFNDLLFUNC_023 m_lpfnCeCloseUSBDev;

	LPFNDLLFUNC_023 m_lpfnCeWriteUSBDev;
	LPFNDLLFUNC_024 m_lpfnCeWriteReadUSBDev;

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	void TestQ1(HANDLE hObj);
	void TestTg2460(HANDLE hObj);
	void TestTg2480(HANDLE hObj);
	void TestVkp80ii(HANDLE hObj);
	void TestVkp80iiCH(HANDLE hObj);
	void TestBarCodes(HANDLE hObj);
	void PrintUnicodeText(HANDLE hObj);

// Implementation
protected:
	HICON m_hIcon;
	HINSTANCE m_hLib;
	HANDLE m_hComObj;

	HANDLE m_hUSBObj;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonInitcom();
	afx_msg void OnBnClickedButtonGetGsi();	
	afx_msg void OnBnClickedCancel();
	CButton m_bttInitCom;
	CButton m_bttClose;
	CButton m_bttOK;
	CButton m_bttGetStatus;
	CButton m_bttGetDefComCfg;
	CButton m_bttGetComCfg;
	CButton m_bttSetDefComCfg;
	CButton m_bttGetDefTimeout;
	CButton m_bttGetTimeout;
	CButton m_bttSetTimeout;
	CButton m_bttSetNewComCfg;

	CString m_strPortName;
	CString m_strGSI;
	afx_msg void OnBnClickedOk();
	CString m_strPrnAnswerHex;
	CString m_strLibError;
	CString m_strSysError;
	CString m_strHRComCfg;
	CString m_strPrnFilePath;
	CString m_strPrnName;


	afx_msg void OnBnClickedButtonGetDefTimeout();
	afx_msg void OnBnClickedButtonGetCurTimeout();
	afx_msg void OnBnClickedButtonSetCurTimeout();
	afx_msg void OnBnClickedButtonGetDefComCfg();
	afx_msg void OnBnClickedButtonGetCurrComCfg();
	afx_msg void OnBnClickedButtonSetDefComCfg3();
	afx_msg void OnBnClickedButtonGetStatus();
	afx_msg void OnBnClickedButtonGetCOMhnd();
	afx_msg void OnBnClickedButtonCloseCom();
	afx_msg void OnBnClickedButtonWritePrnFile();
	afx_msg void OnBnClickedButtonWritePrnFile2();	
	afx_msg void OnBnClickedButtonSpoolFile();	
	afx_msg void OnBnClickedButtonTest();
	afx_msg void OnBnClickedButtonNewSetCom();
public:
	afx_msg void OnBnClickedButtonInitusb();
	afx_msg void OnBnClickedButtonDeinitusb();
	afx_msg void OnBnClickedButtonOpenusbdev();
	afx_msg void OnBnClickedButtonGetGsiUsb();
	CString m_strEditBoxGSI;
	CString m_strEditUsbStatus;
	afx_msg void OnBnClickedButtonGetStatusUsb();
	afx_msg void OnBnClickedButtonWritePrnFileUsb();
	afx_msg void OnBnClickedButtonOpenPrnFileUsb();
	CString m_strPrnFilePathUsb;
	afx_msg void OnBnClickedButtonSpoolFileUsb();
	CString m_strEditGetFwRelUsb;
	afx_msg void OnBnClickedButtonGetFwRelUsb();
	afx_msg void OnBnClickedButtonCfgCom();
	afx_msg void OnBnClickedButtonGetFwrel();
	afx_msg void OnBnClickedButtonTestQ1();
	afx_msg void OnBnClickedButtonTestTg2460();
	afx_msg void OnBnClickedButtonTestTg2480();
	afx_msg void OnBnClickedButtonTestVkp80ii();
	afx_msg void OnBnClickedButtonTestTg2480Usb();
	afx_msg void OnBnClickedButtonTestVkp80iiUsb();
	afx_msg void OnBnClickedButtonTestTg2460Usb();
	afx_msg void OnBnClickedButtonTestQ1Usb();
public:
	afx_msg void OnBnClickedButtonTestBarCodesUsb();
public:
	afx_msg void OnBnClickedButtonTestBarCodesCom();
public:
	CString m_strUnicodeText;
public:
	UINT m_uiCodePage;
public:
	afx_msg void OnBnClickedButtonPrintUnicodeString();
	afx_msg void OnBnClickedButtonTestVkp80iichUsb();
	afx_msg void OnBnClickedButtonTestVkp80iichCom();
};
