#pragma once

// Const for BaudRate
const BYTE NBAUDRATE	= 8;
const CString defBaudrate[NBAUDRATE] = {

	_T("1200"),
	_T("2400"),
	_T("4800"),
	_T("9600"),
	_T("19200"),
	_T("38400"),
	_T("57600"),
	_T("115200")
								};

// Const for Data
const BYTE NDATABYTE	= 2;
const CString defDataByte[NDATABYTE] = {

	_T("7"),
	_T("8")
								};

// Const for Flow Control
const BYTE NFCBYTE	= 3;
const CString defFcByte[NFCBYTE] = {

	_T("Xon / Xoff"),
	_T("Hardware"),
	_T("None")
								};

// Const for Parity
const BYTE NPARITY	= 3;
const CString defParity[NPARITY] = {

	_T("Even"),
	_T("Odd"),
	_T("None")
								};

// Const for Stop
const BYTE NSTOPBYTE	= 2;
const CString defStopByte[NSTOPBYTE] = {

	_T("1"),
	_T("2")
								};


// CDialogComSettings dialog

class CDialogComSettings : public CDialog
{
	DECLARE_DYNAMIC(CDialogComSettings)

public:
	CDialogComSettings(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDialogComSettings();

	void Set_m_dcb(DCB _dcb){m_dcb = _dcb;}
	DCB  Get_m_dcb(){ return m_dcb;}
	void SetDefaultValue();
	void GetDefaultValue();

// Dialog Data
	enum { IDD = IDD_DIALOG_COM_SETTINGS };
	CComboBox	m_comboStop;
	CComboBox	m_comboParity;
	CComboBox	m_comboFc;
	CComboBox	m_comboData;
	CComboBox	m_comboBaud;

// Implementation
protected:
	DCB m_dcb;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	virtual BOOL OnInitDialog();
	virtual void OnOK();

	DECLARE_MESSAGE_MAP()
};
