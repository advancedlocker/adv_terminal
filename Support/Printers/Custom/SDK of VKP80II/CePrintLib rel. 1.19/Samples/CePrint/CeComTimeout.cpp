// CeComTimeout.cpp : implementation file
//

#include "stdafx.h"
#include "CePrint.h"
#include "CeComTimeout.h"


// CCeComTimeout dialog

IMPLEMENT_DYNAMIC(CCeComTimeout, CDialog)

CCeComTimeout::CCeComTimeout(CWnd* pParent /*=NULL*/)
	: CDialog(CCeComTimeout::IDD, pParent)
	, m_dwReadIntervalTimeout(0)
	, m_dwReadTotalTimeoutMultiplier(0)
	, m_dwReadTotalTimeoutConstant(0)
	, m_dwWriteTotalTimeoutMultiplier(0)
	, m_dwWriteTotalTimeoutConstant(0)
{
	ZeroMemory(&m_CommTimeout, sizeof(COMMTIMEOUTS)/sizeof(BYTE));
	m_bfReadOnly = FALSE;
}

CCeComTimeout::~CCeComTimeout()
{
}

void CCeComTimeout::DoDataExchange(CDataExchange* pDX)
{
	DDX_Text(pDX, IDC_EDIT_RIT, m_dwReadIntervalTimeout);
	DDV_MinMaxDWord(pDX, m_dwReadIntervalTimeout, 0, 1000);

	DDX_Text(pDX, IDC_EDIT_RTTM, m_dwReadTotalTimeoutMultiplier);
	DDV_MinMaxDWord(pDX, m_dwReadTotalTimeoutMultiplier, 0, 1000);

	DDX_Text(pDX, IDC_EDIT_RTTC, m_dwReadTotalTimeoutConstant);
	DDV_MinMaxDWord(pDX, m_dwReadTotalTimeoutConstant, 0, 1000);

	DDX_Text(pDX, IDC_EDIT_WTTM, m_dwWriteTotalTimeoutMultiplier);
	DDV_MinMaxDWord(pDX, m_dwWriteTotalTimeoutMultiplier, 0, 1000);

	DDX_Text(pDX, IDC_EDIT_WTTC, m_dwWriteTotalTimeoutConstant);
	DDV_MinMaxDWord(pDX, m_dwWriteTotalTimeoutConstant, 0, 1000);

	CDialog::DoDataExchange(pDX);
}

void CCeComTimeout::Set_m_CommTimeout(COMMTIMEOUTS InCommTimeout)
{
	CopyMemory(&m_CommTimeout, &InCommTimeout, sizeof(COMMTIMEOUTS)/sizeof(BYTE));
	m_dwReadIntervalTimeout = m_CommTimeout.ReadIntervalTimeout;
	m_dwReadTotalTimeoutMultiplier = m_CommTimeout.ReadTotalTimeoutMultiplier;
	m_dwReadTotalTimeoutConstant = m_CommTimeout.ReadTotalTimeoutConstant;
	m_dwWriteTotalTimeoutMultiplier = m_CommTimeout.WriteTotalTimeoutMultiplier;
	m_dwWriteTotalTimeoutConstant = m_CommTimeout.WriteTotalTimeoutConstant;
}

void CCeComTimeout::Get_m_CommTimeout(LPCOMMTIMEOUTS pOutCommTimeout)
{	
	m_CommTimeout.ReadIntervalTimeout = m_dwReadIntervalTimeout;
	m_CommTimeout.ReadTotalTimeoutMultiplier = m_dwReadTotalTimeoutMultiplier;
	m_CommTimeout.ReadTotalTimeoutConstant = m_dwReadTotalTimeoutConstant;
	m_CommTimeout.WriteTotalTimeoutMultiplier = m_dwWriteTotalTimeoutMultiplier;
	m_CommTimeout.WriteTotalTimeoutConstant = m_dwWriteTotalTimeoutConstant;
	CopyMemory(pOutCommTimeout, &m_CommTimeout, sizeof(COMMTIMEOUTS)/sizeof(BYTE));
}

BEGIN_MESSAGE_MAP(CCeComTimeout, CDialog)
	ON_BN_CLICKED(IDOK, &CCeComTimeout::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CCeComTimeout::OnBnClickedCancel)
END_MESSAGE_MAP()


// CCeComTimeout message handlers
BOOL CCeComTimeout::OnInitDialog()
{
	CDialog::OnInitDialog();

	CEdit* pEdit = (CEdit*) GetDlgItem(IDC_EDIT_RIT);
	if(pEdit) pEdit->EnableWindow(m_bfReadOnly);

	pEdit = (CEdit*) GetDlgItem(IDC_EDIT_RTTM);
	if(pEdit) pEdit->EnableWindow(m_bfReadOnly);

	pEdit = (CEdit*) GetDlgItem(IDC_EDIT_RTTC);
	if(pEdit) pEdit->EnableWindow(m_bfReadOnly);

	pEdit = (CEdit*) GetDlgItem(IDC_EDIT_WTTM);
	if(pEdit) pEdit->EnableWindow(m_bfReadOnly);

	pEdit = (CEdit*) GetDlgItem(IDC_EDIT_WTTC);
	if(pEdit) pEdit->EnableWindow(m_bfReadOnly);

	return TRUE;  // return TRUE  unless you set the focus to a control
}
void CCeComTimeout::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	// Ghet result
	UpdateData(TRUE);
	OnOK();
}

void CCeComTimeout::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	// Ghet result
	UpdateData(TRUE);
	OnCancel();
}
