#pragma once


// CCeComTimeout dialog

class CCeComTimeout : public CDialog
{
	DECLARE_DYNAMIC(CCeComTimeout)

public:
	CCeComTimeout(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCeComTimeout();
	void Set_m_CommTimeout(COMMTIMEOUTS InCommTimeout);
	void Get_m_CommTimeout(LPCOMMTIMEOUTS pOutCommTimeout);
	void Set_m_bfReadOnly(BOOL bfReadOnly){m_bfReadOnly = bfReadOnly;}

// Dialog Data
	enum { IDD = IDD_DIALOG_COM_TIMEOUT };

protected:
	COMMTIMEOUTS m_CommTimeout;
	BOOL m_bfReadOnly;
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
	unsigned long m_dwReadIntervalTimeout;
	unsigned long m_dwReadTotalTimeoutMultiplier;
	unsigned long m_dwReadTotalTimeoutConstant;
	unsigned long m_dwWriteTotalTimeoutMultiplier;
	unsigned long m_dwWriteTotalTimeoutConstant;
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedCancel();
};
