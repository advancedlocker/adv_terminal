﻿

namespace Lockers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO.Ports;
    using System.Threading;
    using System.Diagnostics;
    using ITLlib;

//    public static class LibraryHandler  // SMC Changed
    public class LibraryHandler
    {
        SSPComms m_LibHandle = new SSPComms();
        Stopwatch m_StopWatch = new Stopwatch();
        Exception m_LastEx;
        Object m_Lock = new Object();
        string m_PortName = "";
        bool m_PortOpen = false;

        public Exception LastException
        {
            get { return m_LastEx; }
            set { throw (new Exception("Can't modify this value!")); }
        }

        // This helper allows one port to be open at once
        public bool OpenPort(ref SSP_COMMAND cmd)
        {
            try
            {
                // Lock critical section
                lock (m_Lock)
                {
                    // If this exact port is already open, return true
                    if (cmd.ComPort == m_PortName && m_PortOpen)
                        return true;

                    // Open port
                    if (m_LibHandle.OpenSSPComPort(cmd))
                    {
                        // Remember details
                        m_PortName = cmd.ComPort;
                        m_PortOpen = true;
                        return true;
                    }

//                    m_LibHandle.CloseComPort();

                    return false;
                }
            }
            catch (Exception ex)
            {
                m_LastEx = ex;
                return false;
            }
        }

        public void ClosePort()
        {
            try
            {
                lock (m_Lock)
                {
                    // Close the COM port and reset vars
                    m_LibHandle.CloseComPort();
                    m_PortName = "";
                    m_PortOpen = false;
                }
            }
            catch (Exception ex)
            {
                m_LastEx = ex;
            }
        }

        public bool SendCommand(ref SSP_COMMAND cmd, ref SSP_COMMAND_INFO inf)
        {
            try
            {
                // Lock critical section to prevent multiple commands being sent simultaneously
                lock (m_Lock)
                {
                    string port = "Unknown";

                    switch (cmd.ComPort)
                    {
                        case "COM77":
                            port = "hopper";
                            break;

                        case "COMM76":
                            port = "NV11";
                            break;

                        case "COM76":
                            port = "SmartPayout";
                            break;

                    }

                    Console.WriteLine(DateTime.Now.ToLongTimeString() + "  - SSPCommandPortr = " + cmd.ComPort + " " + port);
                    return m_LibHandle.SSPSendCommand(cmd, inf);
                }
            }
            catch (Exception ex)
            {
                m_LastEx = ex;
                return false;
            }
        }

        public bool InitiateKeys(ref SSP_KEYS keys, ref SSP_COMMAND cmd)
        {
            try
            {
                lock (m_Lock)
                {
                    return m_LibHandle.InitiateSSPHostKeys(keys, cmd);
                }
            }
            catch (Exception ex)
            {
                m_LastEx = ex;
                return false;
            }
        }

        public bool CreateFullKey(ref SSP_KEYS keys)
        {
            try
            {
                lock (m_Lock)
                {
                    return m_LibHandle.CreateSSPHostEncryptionKey(keys);
                }
            }
            catch (Exception ex)
            {
                m_LastEx = ex;
                return false;
            }
        }
    }
}
