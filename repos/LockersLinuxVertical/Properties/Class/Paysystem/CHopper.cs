﻿

namespace Lockers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using ITLlib;
    using System.Threading;
    using System.Runtime.InteropServices;
    using Microsoft.Win32.SafeHandles;

    public class CHopper:IDisposable 
    {
        // handles to SSP classes
        SSP_COMMAND m_cmd;
        SSP_KEYS m_keys;
        SSP_FULL_KEY m_sspKey;
        SSP_COMMAND_INFO m_info;

        LibraryHandler HopperlibHdlr = new LibraryHandler();

        // variable declarations

        // The protocol version this validator is using, set in setup request
        int m_ProtocolVersion;

        // The number of channels being used in this dataset.
        int m_NumberOfChannels;

        // A boolean indicating whether the coin mech is enabled or disabled (globally inhibited).
        bool m_CoinMechEnabled;

        // The class representing the comms log. Deals with logging info both visually and to file.
        //CCommsWindow m_Comms;

        // A variable to hold the type of the unit, obtained in setup request
        char m_UnitType;

        // A list of dataset data, sorted by value. Holds the info on channel number, value, currency,
        // level and whether it is being recycled.
        List<ChannelData> m_UnitDataList;

        // An array to hold the current poll response, this allows the command structure to be used
        // while polling without losing the remainder of the poll response
        byte[] m_CurrentPollResponse;
        byte m_CurrentPollResponseLength;


        //=======vic=================================
        decimal totalcoin;
        Boolean coinempty;
        Boolean isdisable;
        Boolean coinin = false;
        Boolean coindispense = false;
        float coininserted;

        /// <summary>
        /// Locking object used for thread safety.
        /// </summary>
        private static readonly object NVLocker = new object();

        private byte PrevPoll;

        // Flag: Has Dispose already been called? 
        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);


        public event EventHandler<LogMessageEventArgs> LogEventMessage;
        public event EventHandler<PaidEventArgs> PaidAmountEvent;
        public event EventHandler<PaidEventArgs> CoinDispened;
        public event EventHandler<EventArgs> CoinDispensing;
        public event EventHandler<EventArgs> HopperDisabled;
        public event EventHandler<EventArgs> CoinLow;
        public event EventHandler<EventArgs> UnitEmpty;
        public event EventHandler<EventArgs> Jammed;
        public event EventHandler<EventArgs> Halted;
        public event EventHandler<EventArgs> IncompletePayout;
        public event EventHandler<EventArgs> CoinMech_Jammed;
        public event EventHandler<EventArgs> FraudAttempt;
        public event EventHandler<EventArgs> Cashbox_Paid;
        //===========================================
        // constructor
        public CHopper()
        {
            // init SSP handles
            m_cmd = new SSP_COMMAND();
            m_keys = new SSP_KEYS();
            m_sspKey = new SSP_FULL_KEY();
            m_info = new SSP_COMMAND_INFO();

            m_NumberOfChannels = 0;
            m_ProtocolVersion = 0;
            m_CoinMechEnabled = true;
            //m_Comms = new CCommsWindow("SMART Hopper");
            m_UnitDataList = new List<ChannelData>();
            m_CurrentPollResponse = new byte[255];

            //if (Properties.Settings.Default.Comms)
            //    Comms.Show();
        }
        public Boolean Is_Disable
        {
            get { return isdisable; }
            set { isdisable = value; }
        }

        public Boolean COIN_DISPENSED
        {
            get { return coindispense; }
            set { coindispense = value; }
        }

        public Boolean COIN_IN
        {
            get { return coinin; }
            set { coinin = value; }
        }

        /* Variable Access */
        public decimal TOTAL_COIN
        {
            get { return totalcoin; }
        }
        public float COIN_INSERTED
        {
            get { return coininserted; }
            set { coininserted = value; }
        }

        public Boolean CoinsEmpty
        {
            get { return this.coinempty; }
        }
        //public CCommsWindow Comms
        //{
        //    get { return m_Comms; }
        //}

        // access to number of channels
        public int NumberOfChannels
        {
            get { return m_NumberOfChannels; }
            set { m_NumberOfChannels = value; }
        }

        // access to coin mech bool
        public bool CoinMechEnabled
        {
            get { return m_CoinMechEnabled; }
            set { m_CoinMechEnabled = value; }
        }

        // access to the command structure
        public SSP_COMMAND CommandStructure
        {
            get { return m_cmd; }
            set { m_cmd = value; }
        }

        // access to the info structure
        public SSP_COMMAND_INFO InfoStructure
        {
            get { return m_info; }
            set { m_info = value; }
        }

        // access to the unit's type
        public char UnitType
        {
            get { return m_UnitType; }
        }

        // access to the dataset data
        public List<ChannelData> UnitDataList
        {
            get { return m_UnitDataList; }
        }

        #region events
        /// <summary>
        /// event trigered 
        /// </summary>
        /// <param name="e"></param>
        protected void OnLogEventMessage(LogMessageEventArgs e)
        {
            EventHandler<LogMessageEventArgs> logeventmessage = this.LogEventMessage;
            if (logeventmessage != null) logeventmessage(this, e);
        }

        protected void OnPaidAmountEvent(PaidEventArgs e)
        {
            EventHandler<PaidEventArgs> paidamountevent = this.PaidAmountEvent;
            if (paidamountevent != null) paidamountevent(this, e);
        }

        protected void OnCoinDispensed(PaidEventArgs e)
        {
            EventHandler<PaidEventArgs> coindispensed = this.CoinDispened;
            if (coindispensed != null) coindispensed(this, e);
        }

        protected void OnCoinDispensing(EventArgs e)
        {
            EventHandler<EventArgs> coindispensing = this.CoinDispensing;
            if (coindispensing != null) coindispensing = this.CoinDispensing;
        }

        protected void OnHopperDisabled(EventArgs e)
        {
            EventHandler<EventArgs> hopperdisabled = this.HopperDisabled;
            if (hopperdisabled != null) hopperdisabled(this, e);
        }

        protected void OnCoin_Low(EventArgs e)
        {
            EventHandler<EventArgs> coinlow = this.CoinLow;
            if (coinlow != null) coinlow(this, e);
        }

        protected void OnUnit_Empty(EventArgs e)
        {
            EventHandler<EventArgs> unitempty = this.UnitEmpty;
            if (unitempty != null) unitempty(this, e);
        }

        protected void OnJammed(EventArgs e)
        {
            EventHandler<EventArgs> jammed = this.Jammed;
            if (jammed != null) jammed(this, e);
        }

        protected void OnHalted(EventArgs e)
        {
            EventHandler<EventArgs> halted = this.Halted;
            if (halted != null) halted(this, e);
        }

        protected void OnIncompletePayout(EventArgs e)
        {
            EventHandler<EventArgs> incomplete = this.IncompletePayout;
            if (incomplete != null) incomplete(this, e);
        }

        protected void OnCoinJammed(EventArgs e)
        {
            EventHandler<EventArgs> coinjammed = this.CoinMech_Jammed;
            if (coinjammed != null) coinjammed(this, e);
        }

        protected void OnFraud_Attempt(EventArgs e)
        {
            EventHandler<EventArgs> fraud = this.FraudAttempt;
            if (fraud != null) fraud(this, e);
        }

        protected void OnCashbox_Paid(EventArgs e)
        {
            EventHandler<EventArgs> cashboxpaid = this.Cashbox_Paid;
            if (cashboxpaid != null) cashboxpaid(this, e);
        }


        #endregion

        // get a channel value
        public int GetChannelValue(int channelNum)
        {
            if (channelNum > 0 && channelNum <= m_NumberOfChannels)
            {
                foreach (ChannelData d in m_UnitDataList)
                {
                    if (d.Channel == channelNum)
                        return d.Value;
                }
            }
            return -1;
        }

        // Command functions

        // This function send the SYNC command to the validator. It returns true if it receives an OK response.
        public bool SendSync()
        {
            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_SYNC;
            m_cmd.CommandDataLength = 1;
            if (!SendCommand()) return false;
            if (CheckGenericResponses())
                return true;
            return false;
        }

        // The enable command allows the validator to receive and act on commands.
        public void EnableValidator()
        {
            if (!Properties.Settings.Default.HopperEnabled)
                return;

            short retryCount = 5;
            bool commandsuccess = false;
            bool responsesuccess = false;

            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_ENABLE;
            m_cmd.CommandDataLength = 1;

            commandsuccess = SendCommand();
            while (!commandsuccess && retryCount > 0)
            {
                retryCount--;
                lock (NVLocker)
                {
                    commandsuccess = SendCommand();
                    Thread.Sleep(10);
                }
            }
            if (!commandsuccess) return;
            retryCount = 5;
            //while (!responsesuccess && retryCount > 0)
            //{
            //    retryCount--;
            //    lock (NVLocker)
            //    {
            responsesuccess = CheckGenericResponses();
            //Thread.Sleep(50);
            //    }
            //}
            if (responsesuccess)
            {
                isdisable = false;
                this.OnLogEventMessage(new LogMessageEventArgs("SMART Hopper enabled\r\n"));
                //try
                //{
                //    if (log != null) log.Invoke(new Action(() => log.AppendText("SMART Hopper enabled\r\n")));
                //}
                //catch { }
            }
            ////===========================================

            //if (!SendCommand(log)) return;
            //// check response
            //if (CheckGenericResponses(log))
            //{
            //    if (log != null)
            //    {
            //        isdisable = false;
            //        log.Invoke(new Action(() => log.AppendText("SMART Hopper enabled\r\n")));
            //    }
            //}
        }

        //======================================================

        // Empty device moves all the coins in the device to the cashbox using command EMPTY ALL. It then
        // sets the channel levels to 0.
        public void EmptyDevice()
        {
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_EMPTY_ALL;
            m_cmd.CommandDataLength = 1;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                this.OnLogEventMessage(new LogMessageEventArgs("Emptying all stored coins to cashbox...\r\n"));
                //try
                //{
                //    log.Invoke(new Action(() => log.AppendText("Emptying all stored coins to cashbox...\r\n")));
                //}
                //catch { }
                foreach (ChannelData d in m_UnitDataList)
                    d.Level = 0;
            }
        }

        // Set a channel to route to cashbox, this sends the SET ROUTING command.
        public void RouteChannelToCashbox(int channelNumber)
        {
            // setup command
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_SET_ROUTING;
            m_cmd.CommandData[1] = 0x01; // cashbox

            // coin to route

            // Get value of coin (4 byte protocol 6)
            byte[] b = CHelpers.ConvertInt32ToBytes(GetChannelValue(channelNumber));
            m_cmd.CommandData[2] = b[0];
            m_cmd.CommandData[3] = b[1];
            m_cmd.CommandData[4] = b[2];
            m_cmd.CommandData[5] = b[3];

            // Add country code, locate from dataset
            foreach (ChannelData d in m_UnitDataList)
            {
                if (d.Channel == channelNumber)
                {
                    m_cmd.CommandData[6] = (byte)d.Currency[0];
                    m_cmd.CommandData[7] = (byte)d.Currency[1];
                    m_cmd.CommandData[8] = (byte)d.Currency[2];
                    break;
                }
            }

            m_cmd.CommandDataLength = 9;

            // send command
            if (!SendCommand()) return;

            if (CheckGenericResponses())
            {
                // update list
                foreach (ChannelData d in m_UnitDataList)
                {
                    if (d.Channel == channelNumber)
                    {
                        d.Recycling = false;
                        break;
                    }
                }
                this.OnLogEventMessage(new LogMessageEventArgs("Successfully routed coin on channel " + channelNumber.ToString() + " to cashbox\r\n"));
                //try
                //{
                //    if (log != null) log.Invoke(new Action(() => log.AppendText("Successfully routed coin on channel " + channelNumber.ToString() + " to cashbox\r\n")));
                //}
                //catch { }
            }
        }
        // Set a channel to route to storage, this sends the SET ROUTING command.
        public void RouteChannelToStorage(int channelNumber)
        {
            // setup command
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_SET_ROUTING;
            m_cmd.CommandData[1] = 0x00; // storage

            // coin to route

            // Get value of coin (4 byte protocol 6)
            byte[] b = CHelpers.ConvertInt32ToBytes(GetChannelValue(channelNumber));
            m_cmd.CommandData[2] = b[0];
            m_cmd.CommandData[3] = b[1];
            m_cmd.CommandData[4] = b[2];
            m_cmd.CommandData[5] = b[3];

            // Add country code, locate from dataset
            foreach (ChannelData d in m_UnitDataList)
            {
                if (d.Channel == channelNumber)
                {
                    m_cmd.CommandData[6] = (byte)d.Currency[0];
                    m_cmd.CommandData[7] = (byte)d.Currency[1];
                    m_cmd.CommandData[8] = (byte)d.Currency[2];
                    break;
                }
            }

            m_cmd.CommandDataLength = 9;

            // send command
            if (!SendCommand()) return;

            if (CheckGenericResponses())
            {
                // update list
                foreach (ChannelData d in m_UnitDataList)
                {
                    if (d.Channel == channelNumber)
                    {
                        d.Recycling = true;
                        break;
                    }
                }
                this.OnLogEventMessage(new LogMessageEventArgs("Successfully routed coin on channel " + channelNumber.ToString() + " to storage\r\n"));
                //if (log != null)
                //    log.Invoke(new Action(() => log.AppendText("Successfully routed coin on channel " + channelNumber.ToString() + " to storage\r\n")));
            }
        }

        // Disable command stops the unit accepting commands and acting on them.
        public void DisableValidator()
        {
            if (!Properties.Settings.Default.HopperEnabled) return;

            short retryCount = 3;                   // Retries left.
            bool commandsuccess = false;
            bool responsesuccess = false;

            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_DISABLE;
            m_cmd.CommandDataLength = 1;
            while (!commandsuccess && retryCount >= 0)
            {
                retryCount--;
                lock (NVLocker)
                {
                    commandsuccess = SendCommand();
                    Thread.Sleep(10);
                }
            }
            //if (!SendCommand(log)) return;
            if (!commandsuccess) return;
            isdisable = true;
            retryCount = 3;
            // check response
            //if (CheckGenericResponses(log) && log != null)
            //{
            //    isdisable = true;
            //    log.Invoke(new Action(() => log.AppendText("SMART Hopper disabled\r\n")));
            //}

            //while (!responsesuccess && retryCount > 0)
            //{
            //    retryCount--;
            //    lock (NVLocker)
            //    {
            responsesuccess = CheckGenericResponses();
            //Thread.Sleep(50);
            //    }
            //}
            if (responsesuccess)
            {
                
                this.OnLogEventMessage(new LogMessageEventArgs("SMART Hopper disabled\r\n"));
                //if (log != null) log.Invoke(new Action(() => log.AppendText("SMART Hopper disabled\r\n")));

            }

        }


        // The reset command instructs the validator to restart (same effect as switching on and off)
        public void Reset()
        {
            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_RESET;
            m_cmd.CommandDataLength = 1;

            if (!SendCommand()) return;
            CheckGenericResponses();
        }

        // This uses the PAYOUT AMOUNT command to payout a value specified by the param amountToPayout.
        // Protocol 6+ - We can use an option byte to test whether the payout is possible (0x19), and if
        // it is then we can resend with the option byte 0x58 to do the payout.
        public bool PayoutAmount(int amountToPayout, char[] currency)
        {
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_PAYOUT_AMOUNT;

            // Value to payout
            byte[] b = CHelpers.ConvertInt32ToBytes(amountToPayout);
            m_cmd.CommandData[1] = b[0];
            m_cmd.CommandData[2] = b[1];
            m_cmd.CommandData[3] = b[2];
            m_cmd.CommandData[4] = b[3];

            // Country code
            m_cmd.CommandData[5] = (byte)currency[0];
            m_cmd.CommandData[6] = (byte)currency[1];
            m_cmd.CommandData[7] = (byte)currency[2];

            m_cmd.CommandData[8] = 0x58; // payout option (0x19 for test, 0x58 for real)

            m_cmd.CommandDataLength = 9;

            if (!SendCommand()) return false;

            if (CheckGenericResponses())
                return true;
            return false;
        }

        // Payout by denomination. This function allows a developer to payout specified amounts of certain
        // coins. Due to the variable length of the data that could be passed to the function, the user 
        // passes an array containing the data to payout and the length of that array along with the number
        // of denominations they are paying out.
        public void PayoutByDenomination(byte numDenoms, byte[] data, byte dataLength)
        {
            // First is the command byte
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_PAYOUT_BY_DENOMINATION;

            // Next is the number of denominations to be paid out
            m_cmd.CommandData[1] = numDenoms;

            // Copy over data byte array parameter into command structure
            int currentIndex = 2;
            for (int i = 0; i < dataLength; i++)
                m_cmd.CommandData[currentIndex++] = data[i];

            // Perform a real payout (0x19 for test)
            m_cmd.CommandData[currentIndex++] = 0x58;

            // Length of command data (add 3 to cover the command byte, num of denoms and real/test byte)
            dataLength += 3;
            m_cmd.CommandDataLength = dataLength;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                this.OnLogEventMessage(new LogMessageEventArgs("Paying out by denomination...\r\n"));
                //if (log != null)
                //    log.Invoke(new Action(() => log.AppendText("Paying out by denomination...\r\n")));
            }
        }

        // This function uses the COIN MECH GLOBAL INHIBIT command to disable the coin mech.
        public bool DisableCoinMech()
        {
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_COIN_MECH_GLOBAL_INHIBIT;
            m_cmd.CommandData[1] = 0x00; // 0 for disable
            m_cmd.CommandDataLength = 2;

            if (!SendCommand()) return false;
            if (CheckGenericResponses())
            {
                m_CoinMechEnabled = false;
                this.OnLogEventMessage(new LogMessageEventArgs("Disabled coin mech\r\n"));
                //log.Invoke(new Action(() => log.AppendText("Disabled coin mech\r\n")));
                return true;
            }
            return false;
        }

        // This function uses the COIN MECH GLOBAL INHIBIT command to enable the coin mech.
        public bool EnableCoinMech()
        {
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_COIN_MECH_GLOBAL_INHIBIT;
            m_cmd.CommandData[1] = 0x01; // 1 for enable
            m_cmd.CommandDataLength = 2;

            if (!SendCommand()) return false;
            if (CheckGenericResponses())
            {
                m_CoinMechEnabled = true;
                this.OnLogEventMessage(new LogMessageEventArgs("Enabled coin mech\r\n"));
                //log.Invoke(new Action(() => log.AppendText("Enabled coin mech\r\n")));
                return true;
            }
            return false;
        }

        // This function uses the command SMARTY EMPTY which empties all the coins to the cashbox but keeps a 
        // count of what was put in, the data of what coins were emptied can be accessed with the command
        // CASHBOX PAYOUT OPERATION DATA
        public void SmartEmpty()
        {
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_SMART_EMPTY;
            m_cmd.CommandDataLength = 1;
            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                this.OnLogEventMessage(new LogMessageEventArgs("SMART empyting...\r\n"));
                //if (log != null) log.Invoke(new Action(() => log.AppendText("SMART empyting...\r\n")));
            }
        }

        // This uses the SET COIN AMOUNT command to increase a channel level by passing over the channel and the amount to increment by
        public void SetCoinLevelsByChannel(int channel, short amount)
        {
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_SET_COIN_AMOUNT;
            // Level to set
            byte[] b = CHelpers.ConvertInt16ToBytes(amount);
            m_cmd.CommandData[1] = b[0];
            m_cmd.CommandData[2] = b[1];

            // Coin(channel) to set
            b = CHelpers.ConvertInt32ToBytes(GetChannelValue(channel));
            m_cmd.CommandData[3] = b[0];
            m_cmd.CommandData[4] = b[1];
            m_cmd.CommandData[5] = b[2];
            m_cmd.CommandData[6] = b[3];

            // Add country code, locate from dataset
            foreach (ChannelData d in m_UnitDataList)
            {
                if (d.Channel == channel)
                {
                    m_cmd.CommandData[7] = (byte)d.Currency[0];
                    m_cmd.CommandData[8] = (byte)d.Currency[1];
                    m_cmd.CommandData[9] = (byte)d.Currency[2];
                    break;
                }
            }

            m_cmd.CommandDataLength = 10;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                // Update the level
                foreach (ChannelData d in m_UnitDataList)
                {
                    if (d.Channel == channel)
                    {
                        d.Level += amount;
                        break;
                    }
                }

                this.OnLogEventMessage(new LogMessageEventArgs("Changed coin value " + CHelpers.FormatToCurrency(GetChannelValue(channel)).ToString() +
                        "'s level to " + amount.ToString() + "\r\n"));
                //if (log != null)
                //{
                //    log.Invoke(new Action(() => log.AppendText("Changed coin value " + CHelpers.FormatToCurrency(GetChannelValue(channel)).ToString() +
                //        "'s level to " + amount.ToString() + "\r\n")));
                //}
            }

        }

        // This uses the SET COIN AMOUNT command to increase a channel level by passing over the coin value and the amount to increment by
        public void SetCoinLevelsByCoin(int coin, char[] currency, short amount)
        {
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_SET_COIN_AMOUNT;
            byte[] b = CHelpers.ConvertInt16ToBytes(amount);
            m_cmd.CommandData[1] = b[0];
            m_cmd.CommandData[2] = b[1];
            b = CHelpers.ConvertInt32ToBytes(coin);
            m_cmd.CommandData[3] = b[0];
            m_cmd.CommandData[4] = b[1];
            m_cmd.CommandData[5] = b[2];
            m_cmd.CommandData[6] = b[3];
            m_cmd.CommandData[7] = (byte)currency[0];
            m_cmd.CommandData[8] = (byte)currency[1];
            m_cmd.CommandData[9] = (byte)currency[2];
            m_cmd.CommandDataLength = 10;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                // Update the level
                foreach (ChannelData d in m_UnitDataList)
                {
                    if (d.Value == coin)
                    {
                        d.Level += amount;
                        break;
                    }
                }
                this.OnLogEventMessage(new LogMessageEventArgs("Increased coin value " + CHelpers.FormatToCurrency(coin).ToString() + "'s level by " + amount.ToString() + "\r\n"));
                //if (log != null)
                //    log.Invoke(new Action(() => log.AppendText("Increased coin value " + CHelpers.FormatToCurrency(coin).ToString() + "'s level by " + amount.ToString() + "\r\n")));

            }
        }

        // This uses the GET COIN AMOUNT command to query the validator on a specified coin it has stored, it returns
        // the level as an int.
        public short CheckCoinLevel(int coinValue, char[] currency)
        {
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_GET_COIN_AMOUNT;
            byte[] b = CHelpers.ConvertInt32ToBytes(coinValue);
            m_cmd.CommandData[1] = b[0];
            m_cmd.CommandData[2] = b[1];
            m_cmd.CommandData[3] = b[2];
            m_cmd.CommandData[4] = b[3];
            m_cmd.CommandData[5] = (byte)currency[0];
            m_cmd.CommandData[6] = (byte)currency[1];
            m_cmd.CommandData[7] = (byte)currency[2];
            m_cmd.CommandDataLength = 8;

            if (!SendCommand()) return -1;
            if (CheckGenericResponses())
            {
                return CHelpers.ConvertBytesToInt16(m_cmd.ResponseData, 1);
            }
            return -1;
        }

        // This function just updates all the coin levels in the list
        public void UpdateData()
        {
            foreach (ChannelData d in m_UnitDataList)
            {
                d.Level = CheckCoinLevel(d.Value, d.Currency);
                IsCoinRecycling(d.Value, d.Currency, ref d.Recycling);
            }
        }

        public void UpdateData(bool checkrecycling)
        {
            foreach (ChannelData d in m_UnitDataList)
            {
                d.Level = CheckCoinLevel(d.Value, d.Currency);
                if (checkrecycling) IsCoinRecycling(d.Value, d.Currency, ref d.Recycling);
            }
        }

        // This function uses the GET ROUTING command to see if a specified coin is recycling. The
        // caller passes a bool across which is set by the function.
        public void IsCoinRecycling(int coinValue, char[] currency, ref bool response)
        {
            // First determine if the coin is currently being recycled
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_GET_ROUTING;
            byte[] b = CHelpers.ConvertInt32ToBytes(coinValue);
            m_cmd.CommandData[1] = b[0];
            m_cmd.CommandData[2] = b[1];
            m_cmd.CommandData[3] = b[2];
            m_cmd.CommandData[4] = b[3];

            // Add currency
            m_cmd.CommandData[5] = (byte)currency[0];
            m_cmd.CommandData[6] = (byte)currency[1];
            m_cmd.CommandData[7] = (byte)currency[2];
            m_cmd.CommandDataLength = 8;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                // True if it is currently being recycled
                if (m_cmd.ResponseData[1] == 0x00)
                {
                    response = true;
                    this.OnLogEventMessage(new LogMessageEventArgs(CHelpers.FormatToCurrency(coinValue) + " is recycling\r\n"));

                    //try
                    //{
                    //    if (log != null)
                    //        log.Invoke(new Action(() => log.AppendText(CHelpers.FormatToCurrency(coinValue) + " is recycling\r\n")));
                    //}
                    //catch { }
                }
                // False if not
                else if (m_cmd.ResponseData[1] == 0x01)
                {
                    response = false;
                    this.OnLogEventMessage(new LogMessageEventArgs(CHelpers.FormatToCurrency(coinValue) + " is not recycling\r\n"));

                    //try
                    //{
                    //    if (log != null)
                    //        log.Invoke(new Action(() => log.AppendText(CHelpers.FormatToCurrency(coinValue) + " is not recycling\r\n")));
                    //}
                    //catch { }
                }
            }
        }

        // This function returns a member of the internal array to check whether a channel is
        // recycling, this can't be called before setup request or it will be inaccurate.
        public bool IsChannelRecycling(int channel)
        {
            if (channel > 0 && channel <= m_NumberOfChannels)
            {
                foreach (ChannelData d in m_UnitDataList)
                {
                    if (d.Channel == channel)
                    {
                        this.OnLogEventMessage(new LogMessageEventArgs("Channel " + channel + " recycling status: " + d.Recycling.ToString() + "\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("Channel " + channel + " recycling status: " + d.Recycling.ToString() + "\r\n")));
                        //}
                        //catch { }
                        return d.Recycling;
                    }
                }
            }
            return false;
        }

        // This function gets the CASHBOX PAYOUT OPERATION DATA from the validator and returns it as a string
        public void GetCashboxPayoutOpData()
        {
            // first send the command
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_CASHBOX_PAYOUT_OP_DATA;
            m_cmd.CommandDataLength = 1;

            if (!SendCommand()) return;

            // now deal with the response data
            if (CheckGenericResponses())
            {
                // number of different coins
                int n = m_cmd.ResponseData[1];
                string displayString = "Number of Total Coins: " + n.ToString() + "\r\n\r\n";
                int i = 0;
                for (i = 2; i < (9 * n); i += 9)
                {
                    displayString += "Moved " + CHelpers.ConvertBytesToInt16(m_cmd.ResponseData, i) +
                        " x " + CHelpers.FormatToCurrency(CHelpers.ConvertBytesToInt32(m_cmd.ResponseData, i + 2)) +
                        " " + (char)m_cmd.ResponseData[i + 6] + (char)m_cmd.ResponseData[i + 7] + (char)m_cmd.ResponseData[i + 8]
                        + " to cashbox\r\n";
                }
                displayString += CHelpers.ConvertBytesToInt32(m_cmd.ResponseData, i) + " coins not recognised\r\n";
                this.OnLogEventMessage(new LogMessageEventArgs(displayString));
                //try
                //{
                //    if (log != null) log.Invoke(new Action(() => log.AppendText(displayString)));
                //}
                //catch { }
            }
        }

        // This function uses the FLOAT AMOUNT command to set the float amount. The Hopper will empty
        // coins into the cashbox leaving the requested floating amount in the payout. The minimum payout
        // is also setup so the validator will leave itself the ability to payout the minimum value requested.
        public bool SetFloat(short minPayout, int floatAmount, char[] currency)
        {
            m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_FLOAT_AMOUNT;

            // Min payout
            byte[] b = CHelpers.ConvertInt16ToBytes(minPayout);
            m_cmd.CommandData[1] = b[0];
            m_cmd.CommandData[2] = b[1];

            // Amount to payout
            b = CHelpers.ConvertInt32ToBytes(floatAmount);
            m_cmd.CommandData[3] = b[0];
            m_cmd.CommandData[4] = b[1];
            m_cmd.CommandData[5] = b[2];
            m_cmd.CommandData[6] = b[3];

            // Country code
            m_cmd.CommandData[7] = (byte)currency[0];
            m_cmd.CommandData[8] = (byte)currency[1];
            m_cmd.CommandData[9] = (byte)currency[2];

            m_cmd.CommandData[10] = 0x58; // real float

            m_cmd.CommandDataLength = 11;

            if (!SendCommand()) return false;

            if (CheckGenericResponses())
            {
                this.OnLogEventMessage(new LogMessageEventArgs("Set float successfully\r\n"));
                //try
                //{
                //    if (log != null)
                //        log.Invoke(new Action(() => log.AppendText("Set float successfully\r\n")));
                //}
                //catch { }
                return true;
            }
            return false;
        }

        // This function sets the protocol version using the command HOST PROTOCOL VERSION.
        public bool SetProtocolVersion(byte b)
        {
            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_HOST_PROTOCOL_VERSION;
            m_cmd.CommandData[1] = b;
            m_cmd.CommandDataLength = 2;

            if (!SendCommand()) return false;
            if (CheckGenericResponses())
            {
                this.OnLogEventMessage(new LogMessageEventArgs("Setting protocol version " + b.ToString() + "\r\n"));
                //try
                //{
                //    if (log != null) log.Invoke(new Action(() => log.AppendText("Setting protocol version " + b.ToString() + "\r\n")));
                //}
                //catch { }
            }
            return true;
        }

        // This function opens the com port identified in the command structure, using the SSP library.
        public bool OpenPort()
        {
            // open com port
            this.OnLogEventMessage(new LogMessageEventArgs("Opening Hopper com port:" + m_cmd.ComPort + "\r\n"));
            //try
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Opening com port\r\n")));
            //}
            //catch { }
            return HopperlibHdlr.OpenPort(ref m_cmd);
        }

        // This function performs a number of commands in order to setup the encryption between the host and the validator.
        public bool NegotiateKeys()
        {
            byte i;

            // make sure encryption is off
            m_cmd.EncryptionStatus = false;

            this.OnLogEventMessage(new LogMessageEventArgs("Syncing... "));
            //try
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Syncing... ")));
            //}
            //catch { }

            // send sync
            if (!SendSync()) return false;
            this.OnLogEventMessage(new LogMessageEventArgs("Success\r\n"));
            //try
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Success\r\n")));
            //}
            //catch { }

            HopperlibHdlr.InitiateKeys(ref m_keys, ref m_cmd);

            // send generator
            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_SET_GENERATOR;
            m_cmd.CommandDataLength = 9;
            this.OnLogEventMessage(new LogMessageEventArgs("Setting generator... "));
            //try
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Setting generator... ")));
            //}
            //catch { }

            for (i = 0; i < 8; i++)
            {
                m_cmd.CommandData[i + 1] = (byte)(m_keys.Generator >> (8 * i));
            }

            if (!SendCommand()) return false;
            this.OnLogEventMessage(new LogMessageEventArgs("Success\r\n"));
            //try
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Success\r\n")));
            //}
            //catch { }

            // send modulus
            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_SET_MODULUS;
            m_cmd.CommandDataLength = 9;
            this.OnLogEventMessage(new LogMessageEventArgs("Sending modulus... "));
            //try
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Sending modulus... ")));
            //}
            //catch { }

            for (i = 0; i < 8; i++)
            {
                m_cmd.CommandData[i + 1] = (byte)(m_keys.Modulus >> (8 * i));
            }

            if (!SendCommand()) return false;
            this.OnLogEventMessage(new LogMessageEventArgs("Success\r\n"));
            //try
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Success\r\n")));
            //}
            //catch { }

            // send key exchange
            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_REQUEST_KEY_EXCHANGE;
            m_cmd.CommandDataLength = 9;
            this.OnLogEventMessage(new LogMessageEventArgs("Exchanging keys... "));
            //try
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Exchanging keys... ")));
            //}
            //catch { }

            for (i = 0; i < 8; i++)
            {
                m_cmd.CommandData[i + 1] = (byte)(m_keys.HostInter >> (8 * i));
            }

            if (!SendCommand()) return false;
            this.OnLogEventMessage(new LogMessageEventArgs("Success\r\n"));
            //try
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Success\r\n")));
            //}
            //catch { }

            m_keys.SlaveInterKey = 0;
            for (i = 0; i < 8; i++)
            {
                m_keys.SlaveInterKey += (UInt64)m_cmd.ResponseData[1 + i] << (8 * i);
            }

            HopperlibHdlr.CreateFullKey(ref m_keys);

            // get full encryption key
            m_cmd.Key.FixedKey = 0x0123456701234567;
            m_cmd.Key.VariableKey = m_keys.KeyHost;
            this.OnLogEventMessage(new LogMessageEventArgs("Keys successfully negotiated\r\n"));
            //try
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Keys successfully negotiated\r\n")));
            //}
            //catch { }

            return true;
        }

        // This function uses the setup request command to get all the information about the validator. It can optionally
        // output to a specified textbox.
        public void SetupRequest()
        {
            // send setup request
            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_SETUP_REQUEST;
            m_cmd.CommandDataLength = 1;

            if (!SendCommand()) return;

            // display setup request

            // unit type
            string displayString = "Unit Type: ";
            int index = 1;
            m_UnitType = (char)m_cmd.ResponseData[index++];
            switch (m_UnitType)
            {
                case (char)0x00: displayString += "Validator"; break;
                case (char)0x03: displayString += "SMART Hopper"; break;
                case (char)0x06: displayString += "SMART Payout"; break;
                case (char)0x07: displayString += "NV11"; break;
                default: displayString += "Unknown Type"; break;
            }

            // firmware
            displayString += "\r\nFirmware: ";
            for (int i = index; i < index + 4; i++)
            {
                displayString += (char)m_cmd.ResponseData[i];
                if (i == 3) displayString += ".";
            }
            index += 4;

            // country code
            // legacy code for old protocol versions, protocol 6+ allows each channel to have a currency
            // for multi currency datasets
            index += 3; // +3 to skip country code

            // protocol version
            m_ProtocolVersion = m_cmd.ResponseData[index++];
            displayString += "\r\nProtocol Version: " + m_ProtocolVersion;

            // number of coin values
            m_NumberOfChannels = m_cmd.ResponseData[index++];
            displayString += "\r\nNumber of Coin Values: " + m_NumberOfChannels;

            // coin values
            int[] channelValuesTemp = new int[m_NumberOfChannels + 1];
            int counter = 1;
            displayString += "\r\nCoin Values: ";
            for (int i = index; i < index + (m_NumberOfChannels * 2) - 1; i += 2, counter++)
            {
                short x = BitConverter.ToInt16(m_cmd.ResponseData, i);
                channelValuesTemp[counter] = x; // update array
                displayString += " (" + x + ") ";
            }
            index += (m_NumberOfChannels * 2);

            // country codes for values (protocol 6+ only)
            char[] channelCurrencyTemp = new char[(m_NumberOfChannels + 1) * 3];
            counter = 3;
            displayString += "\r\nCoin Currencies: ";
            for (int i = index; i < index + (3 * m_NumberOfChannels); i += 3)
            {
                channelCurrencyTemp[counter] = (char)m_cmd.ResponseData[i];
                displayString += channelCurrencyTemp[counter++];
                channelCurrencyTemp[counter] = (char)m_cmd.ResponseData[i + 1];
                displayString += channelCurrencyTemp[counter++];
                channelCurrencyTemp[counter] = (char)m_cmd.ResponseData[i + 2];
                displayString += channelCurrencyTemp[counter++] + " ";
            }
            index += (3 * m_NumberOfChannels);

            // create list entry for each channel
            m_UnitDataList.Clear(); // clear old table
            for (byte i = 1; i <= m_NumberOfChannels; i++)
            {
                ChannelData d = new ChannelData();
                d.Channel = i;
                d.Value = channelValuesTemp[i];
                d.Currency[0] = channelCurrencyTemp[0 + (i * 3)];
                d.Currency[1] = channelCurrencyTemp[1 + (i * 3)];
                d.Currency[2] = channelCurrencyTemp[2 + (i * 3)];
                d.Level = CheckCoinLevel(d.Value, d.Currency);
                IsCoinRecycling(d.Value, d.Currency, ref d.Recycling);

                m_UnitDataList.Add(d);
            }

            // Sort the list of data by the value.
            m_UnitDataList.Sort(delegate(ChannelData d1, ChannelData d2) { return d1.Value.CompareTo(d2.Value); });

            displayString += "\r\n\r\n";
            this.OnLogEventMessage(new LogMessageEventArgs(displayString));
            //try
            //{
            //    if (outputBox != null) outputBox.Invoke(new Action(() => outputBox.AppendText(displayString)));
            //}
            //catch { }
        }

        // This function sends the set coin mech inhibits command to set which coins are accepted on the Hopper.
        // Please note: The response data of this command if it is sent with no coin mech attached will be
        // WRONG PARAMETERS.
        public void SetInhibits()
        {
            // set inhibits on each coin
            for (int i = 0; i < m_NumberOfChannels; i++)
            {
                m_cmd.CommandData[0] = CCommands.Hopper.SSP_CMD_SET_COIN_MECH_INHIBITS;
                m_cmd.CommandData[1] = 0x01; // coin accepted

                // convert values to byte array and set command data
                byte[] b = BitConverter.GetBytes(GetChannelValue(i));
                m_cmd.CommandData[2] = b[0];
                m_cmd.CommandData[3] = b[1];

                // currency
                ChannelData d = m_UnitDataList[i];
                m_cmd.CommandData[4] = (byte)d.Currency[0];
                m_cmd.CommandData[5] = (byte)d.Currency[1];
                m_cmd.CommandData[6] = (byte)d.Currency[2];

                m_cmd.CommandDataLength = 7;

                if (!SendCommand()) return;
                if (CheckGenericResponses())
                {
                    this.OnLogEventMessage(new LogMessageEventArgs("Inhibits set on channel " + (i + 1).ToString() + "\r\n"));
                    //try
                    //{
                    //    log.Invoke(new Action(() => log.AppendText("Inhibits set on channel " + (i + 1).ToString() + "\r\n")));
                    //}
                    //catch { }
                }
            }
        }

        // This function is called repeatedly to poll the validator about what events are happening. It
        // can optionally output these events to a textbox.
        public bool DoPoll()
        {
            if (!Properties.Settings.Default.HopperEnabled) return false;
            byte i;

            //send poll
            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_ENABLE;
            m_cmd.CommandDataLength = 1;

            if (!SendCommand()) return false;

            //send poll
            m_cmd.CommandData[0] = CCommands.Generic.SSP_CMD_POLL;
            m_cmd.CommandDataLength = 1;

            if (!SendCommand()) return false;

            // Check unit hasn't lost key (could be due to power loss or reset)
            if (m_cmd.ResponseData[0] == 0xFA) return false;

            // isolate poll response to allow reuse of the SSP_COMMAND structure
            m_CurrentPollResponseLength = m_cmd.ResponseDataLength;
            m_cmd.ResponseData.CopyTo(m_CurrentPollResponse, 0);

            //parse poll response
            int coin = 0;
            string currency = "";
            for (i = 1; i < m_CurrentPollResponseLength; i++)
            {
                switch (m_CurrentPollResponse[i])
                {
                    // This response indicates that the unit was reset and this is the first time a poll
                    // has been called since the reset.
                    case CCommands.Hopper.SSP_POLL_RESET:
                        UpdateData();
                        break;
                    // This response is given when the unit is disabled.
                    case CCommands.Hopper.SSP_POLL_DISABLED:
                        this.OnLogEventMessage(new LogMessageEventArgs("Unit disabled (coin hopper)...\r\n"));
                        this.OnHopperDisabled(EventArgs.Empty);

                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("Unit disabled (coin hopper)...\r\n")));
                        //}
                        //catch { }
                        break;
                    // The unit is in the process of paying out a coin or series of coins, this will continue to poll
                    // until the coins have been fully dispensed
                    case CCommands.Hopper.SSP_POLL_DISPENSING:
                        this.OnLogEventMessage(new LogMessageEventArgs("Dispensing coin(s)...\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("Dispensing coin(s)...\r\n")));
                        //}
                        //catch { }
                        // Now the index needs to be moved on to skip over the data provided by this response so it
                        // is not parsed as a normal poll response.
                        // In this response, the data includes the number of countries being dispensed (1 byte), then a 4 byte value
                        // and 3 byte currency code for each country. 
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        break;
                    // This is polled when a unit has finished a dispense operation. The following 4 bytes give the 
                    // value of the coin(s) dispensed.
                    case CCommands.Hopper.SSP_POLL_DISPENSED:
                        for (int j = 0; j < m_CurrentPollResponse[i + 1] * 7; j += 7)
                        {
                            coin = CHelpers.ConvertBytesToInt32(m_cmd.ResponseData, i + j + 2); // get coin data from response
                            // get currency from response
                            currency = "";
                            currency += (char)m_CurrentPollResponse[i + j + 6];
                            currency += (char)m_CurrentPollResponse[i + j + 7];
                            currency += (char)m_CurrentPollResponse[i + j + 8];
                            this.OnLogEventMessage(new LogMessageEventArgs(CHelpers.FormatToCurrency(coin) + " " + currency + " coin(s) dispensed\r\n"));
                            this.OnCoinDispensed(new PaidEventArgs("coins", coin * 0.01f));
                        }

                        UpdateData();
                        EnableValidator();
                        coindispense = true;
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        break;
                    // The coins being recycled inside the unit are running low.
                    case CCommands.Hopper.SSP_POLL_COINS_LOW:
                        this.OnLogEventMessage(new LogMessageEventArgs("Coins low\r\n"));
                        this.OnCoin_Low(EventArgs.Empty);

                        break;
                    // There are no coins left in the unit.
                    case CCommands.Hopper.SSP_POLL_EMPTY:
                        this.OnLogEventMessage(new LogMessageEventArgs("Unit empty\r\n"));
                        this.OnUnit_Empty(EventArgs.Empty);
                        this.coinempty = true;
                        break;
                    // The mechanism inside the unit is jammed.
                    case CCommands.Hopper.SSP_POLL_JAMMED:
                        this.OnLogEventMessage(new LogMessageEventArgs("Jammed\r\n"));
                        this.OnJammed(EventArgs.Empty);

                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        break;
                    // A dispense, SMART empty or float operation has been halted.
                    case CCommands.Hopper.SSP_POLL_HALTED:
                        this.OnLogEventMessage(new LogMessageEventArgs("operation has been halted\r\n"));
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        this.OnHalted(EventArgs.Empty);
                        break;
                    // The device is 'floating' a specified amount of coins. It will transfer some to the cashbox and
                    // leave the specified amount in the device.
                    case CCommands.Hopper.SSP_POLL_FLOATING:
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        break;
                    // The float operation has completed.
                    case CCommands.Hopper.SSP_POLL_FLOATED:
                        UpdateData();
                        EnableValidator();
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        break;
                    // This poll appears when the SMART Hopper has been searching for a coin but cannot find it within
                    // the timeout period.
                    case CCommands.Hopper.SSP_POLL_TIME_OUT:
                        this.OnLogEventMessage(new LogMessageEventArgs("Search for suitable coins failed\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("Search for suitable coins failed\r\n")));
                        //}
                        //catch { }
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        break;
                    // A payout was interrupted in some way. The amount paid out does not match what was requested. The value
                    // of the dispensed and requested amount is contained in the response.
                    case CCommands.Hopper.SSP_POLL_INCOMPLETE_PAYOUT:
                        this.OnLogEventMessage(new LogMessageEventArgs("Incomplete payout detected...\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("Incomplete payout detected...\r\n")));
                        //}
                        //catch { }
                        i += (byte)((m_CurrentPollResponse[i + 1] * 11) + 1);

                        this.OnIncompletePayout(EventArgs.Empty);
                        break;
                    // A float was interrupted in some way. The amount floated does not match what was requested. The value
                    // of the dispensed and requested amount is contained in the response.
                    case CCommands.Hopper.SSP_POLL_INCOMPLETE_FLOAT:
                        this.OnLogEventMessage(new LogMessageEventArgs("Incomplete float detected...\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("Incomplete float detected...\r\n")));
                        //}
                        //catch { }
                        i += (byte)((m_CurrentPollResponse[i + 1] * 11) + 1);
                        break;
                    // This poll appears when coins have been dropped to the cashbox whilst making a payout. The value of
                    // coins and the currency is reported in the response.
                    case CCommands.Hopper.SSP_POLL_CASHBOX_PAID:
                        this.OnLogEventMessage(new LogMessageEventArgs("Cashbox paid\r\n"));
                        this.OnCashbox_Paid(EventArgs.Empty);

                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        break;
                    // A credit event has been detected, this is when the coin mech has accepted a coin as legal currency.
                    case CCommands.Hopper.SSP_POLL_COIN_CREDIT:
                        coin = CHelpers.ConvertBytesToInt16(m_cmd.ResponseData, i + 1);
                        //vic add code here for coin mech=====================================================================================================
                        this.coininserted += coin * 0.01f;
                        this.coinin = true;
                        //====================================================================================================================================
                        currency = "";
                        currency += (char)m_CurrentPollResponse[i + 5];
                        currency += (char)m_CurrentPollResponse[i + 6];
                        currency += (char)m_CurrentPollResponse[i + 7];
                        this.OnLogEventMessage(new LogMessageEventArgs(CHelpers.FormatToCurrency(coin) + " " + currency + " credited\r\n"));
                        this.OnPaidAmountEvent(new PaidEventArgs("coins", (float)(Math.Round((double)(coin * 0.01f), 2))));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText(CHelpers.FormatToCurrency(coin) + " " + currency + " credited\r\n")));
                        //}
                        //catch { }
                        UpdateData(false);

                        i += 7;
                        break;
                    // The coin mech has become jammed.
                    case CCommands.Hopper.SSP_POLL_COIN_MECH_JAMMED:
                        this.OnLogEventMessage(new LogMessageEventArgs("Coin mech jammed\r\n"));
                        this.OnCoinJammed(EventArgs.Empty);

                        break;
                    // The return button on the coin mech has been pressed.
                    case CCommands.Hopper.SSP_POLL_COIN_MECH_RETURN_PRESSED:
                        this.OnLogEventMessage(new LogMessageEventArgs("Return button pressed\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("Return button pressed\r\n")));
                        //}
                        //catch { }
                        break;
                    // The unit is in the process of dumping all the coins stored inside it into the cashbox.
                    case CCommands.Hopper.SSP_POLL_EMPTYING:
                        if (this.PrevPoll != CCommands.Hopper.SSP_POLL_EMPTYING) this.OnLogEventMessage(new LogMessageEventArgs("Emptying...\r\n"));

                        this.PrevPoll = m_CurrentPollResponse[i];
                        break;
                    // The unit has finished dumping coins to the cashbox.
                    case CCommands.Hopper.SSP_POLL_EMPTIED:
                        this.OnLogEventMessage(new LogMessageEventArgs("Emptied\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("Emptied\r\n")));
                        //}
                        //catch { }
                        UpdateData();
                        EnableValidator();
                        break;
                    // A fraud attempt has been detected.
                    case CCommands.Hopper.SSP_POLL_FRAUD_ATTEMPT:
                        this.OnLogEventMessage(new LogMessageEventArgs("Fraud attempted\r\n"));

                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);

                        this.OnFraud_Attempt(EventArgs.Empty);
                        break;
                    // The unit is in the process of dumping all the coins stored inside it into the cashbox.
                    // This poll means that the unit is keeping track of what it empties.
                    case CCommands.Hopper.SSP_POLL_SMART_EMPTYING:
                        if (this.PrevPoll != CCommands.Hopper.SSP_POLL_SMART_EMPTYING) this.OnLogEventMessage(new LogMessageEventArgs("SMART emptying...\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("SMART emptying...\r\n")));
                        //}
                        //catch { }
                        this.PrevPoll = m_CurrentPollResponse[i];
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        break;
                    // The unit has finished SMART emptying. The info on what has been dumped can be obtained
                    // by sending the CASHBOX PAYOUT OPERATION DATA command.
                    case CCommands.Hopper.SSP_POLL_SMART_EMPTIED:
                        GetCashboxPayoutOpData();
                        UpdateData();
                        EnableValidator();
                        this.OnLogEventMessage(new LogMessageEventArgs("SMART emptied\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("SMART emptied\r\n")));
                        //}
                        //catch { }
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        break;
                    // A coin has had its routing changed to either cashbox or recycling.
                    case CCommands.Hopper.SSP_POLL_COIN_ROUTED:
                        this.OnLogEventMessage(new LogMessageEventArgs("Routed coin\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("Routed coin\r\n")));
                        //}
                        //catch { }
                        UpdateData();
                        break;
                    default:
                        this.OnLogEventMessage(new LogMessageEventArgs("Unsupported poll response received: " + (int)m_CurrentPollResponse[i] + "\r\n"));
                        //try
                        //{
                        //    if (log != null) log.Invoke(new Action(() => log.AppendText("Unsupported poll response received: " + (int)m_CurrentPollResponse[i] + "\r\n")));
                        //}
                        //catch { }
                        break;
                }

            }
            return true;
        }

        // Non-Command functions 

        // This is used to send a command via SSP to the validator
        public bool SendCommand()
        {
            // attempt to send the command
            if (!HopperlibHdlr.SendCommand(ref m_cmd, ref m_info))
            {
                //m_Comms.UpdateLog(m_info, true);
                this.OnLogEventMessage(new LogMessageEventArgs("Sending command failed\r\nPort status: " + m_cmd.ResponseStatus.ToString() + "\r\n"));
                //try
                //{
                //    if (log != null && log.IsHandleCreated) log.Invoke(new Action(() => log.AppendText("Sending command failed\r\nPort status: " + m_cmd.ResponseStatus.ToString() + "\r\n")));
                //}
                //catch { }
                return false;
            }
            // update the log after every command
            //m_Comms.UpdateLog(m_info);
            return true;
        }


        // This returns the currency of a specified channel.
        public char[] GetChannelCurrency(int channel)
        {
            if (channel > 0 && channel <= m_NumberOfChannels)
            {
                ChannelData d = m_UnitDataList[channel - 1];
                return d.Currency;
            }
            return null;
        }

        // This returns the level of a specified channel.
        public int GetChannelLevel(int channel)
        {
            if (channel > 0 && channel <= m_NumberOfChannels)
            {
                ChannelData d = m_UnitDataList[channel - 1];
                return d.Level;
            }
            return -1;
        }

        // This is similar to the above function but instead returns all the channels as a
        // nicely formatted string
        public string GetChannelLevelInfo()
        {
            string s = "";
            decimal totcoin = 0;
            foreach (ChannelData d in m_UnitDataList)
            {
                s += (d.Value * 0.01f).ToString("0.00") + " " + d.Currency[0] + d.Currency[1] + d.Currency[2];
                s += " [" + d.Level + "] = " + ((d.Level * d.Value) * 0.01f).ToString("0.00");
                s += " " + d.Currency[0] + d.Currency[1] + d.Currency[2] + "\r\n";
                totcoin += ((d.Level * d.Value));
            }

            this.totalcoin = totcoin;
            if (totalcoin != 0) this.coinempty = false;
            return s;
        }

        // This takes a coin value and returns the channel number
        public int GetChannelofCoin(int coin)
        {
            foreach (ChannelData d in m_UnitDataList)
            {
                if (d.Value == coin)
                    return d.Channel;
            }
            return -1;
        }

        // Exception and Error Handling

        // This is used for generic response error catching, it outputs the info in a
        // meaningful way.
        public bool CheckGenericResponses()
        {
            if (m_cmd.ResponseData[0] == CCommands.Generic.SSP_RESPONSE_OK)
                return true;
            else
            {
                switch (m_cmd.ResponseData[0])
                {
                    case CCommands.Generic.SSP_RESPONSE_CMD_CANNOT_BE_PROCESSED:
                        if (m_cmd.ResponseData[1] == 0x03)
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Unit responded with a \"Busy\" response, command cannot be " +
                                    "processed at this time: Hopper\r\n"));

                        }
                        else
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Command response is CANNOT PROCESS COMMAND, error code - 0x"
                               + BitConverter.ToString(m_cmd.ResponseData, 1, 1) + ": Hopper\r\n"));

                        }
                        return false;
                    case CCommands.Generic.SSP_RESPONSE_FAIL:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is FAIL: Hopper\r\n"));

                        return false;
                    case CCommands.Generic.SSP_RESPONSE_KEY_NOT_SET:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is KEY NOT SET: Hopper\r\n"));

                        return false;
                    case CCommands.Generic.SSP_RESPONSE_PARAMS_OUT_OF_RANGE:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is PARAM OUT OF RANGE: Hopper\r\n"));

                        return false;
                    case CCommands.Generic.SSP_RESPONSE_SOFTWARE_ERROR:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is SOFTWARE ERROR: Hopper\r\n"));

                        return false;
                    case CCommands.Generic.SSP_RESPONSE_COMMAND_NOT_KNOWN:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is UNKNOWN: Hopper\r\n"));

                        return false;
                    case CCommands.Generic.SSP_RESPONSE_WRONG_NUMBER_OF_PARAMS:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is WRONG PARAMETERS: Hopper\r\n"));

                        return false;
                    default:
                        return false;
                }
            }

        }

        public void Dispose()
        {
            //throw new NotImplementedException();
            //CHelpers.Shutdown = true;
            //this.PayMonitor.CancelAsync();
            //this.PayMonitor.Dispose();
            //this.PayMonitor = null;

            Dispose(true);
            GC.SuppressFinalize(this);

        }
        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
                // Free any other managed objects here. 
                //
            }

            // Free any unmanaged objects here. 
            //
            disposed = true;
        }

        public void ClosePort()
        {
            HopperlibHdlr.ClosePort();

            return;
        }

    }
}
