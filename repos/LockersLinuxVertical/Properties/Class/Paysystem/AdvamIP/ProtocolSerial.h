#pragma once

void PSInit(void);
void PSDispose(void);

void PSWrite(BYTE* pbyMessage);
void PSRead(BYTE* pbyMessage);