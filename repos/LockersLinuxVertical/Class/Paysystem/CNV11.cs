﻿

namespace Lockers
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;
    using System.Timers;
    using ITLlib;
    using System.Threading;
    using System.Runtime.InteropServices;
    using Microsoft.Win32.SafeHandles;

    public class CNV11:IDisposable 
    {
        // ssp library variables
        SSP_COMMAND cmd;
        SSP_KEYS keys;
        SSP_FULL_KEY sspKey;
        SSP_COMMAND_INFO info;

        LibraryHandler NV11libHdlr = new LibraryHandler();

        // variable declarations
        //CCommsWindow m_Comms;
        // Variables to hold the number of notes accepted and dispensed
        int m_TotalNotesAccepted, m_TotalNotesDispensed;

        // The number of channels used in this validator
        int m_NumberOfChannels;

        // The multiplier by which the channel values are multiplied to get their
        // true penny value.
        int m_ValueMultiplier;

        // The type of unit this class represents, set in the setup request
        char m_UnitType;

        // Integer array to hold the value of each note stored in the payout
        int[] m_NotePositionValues;

        // Current poll response and length
        byte[] m_CurrentPollResponse;
        byte m_CurrentPollResponseLength;

        // A list of dataset data, sorted by value. Holds the info on channel number, value, currency,
        // level and whether it is being recycled.
        List<ChannelData> m_UnitDataList;

        //=============vic=============
        private decimal totalnotes;
        private float paidamnt = 0;
        private float price = 0;
        private Boolean ispaid;
        private Boolean isdisabled;
        private Boolean isdispensed;
        private Boolean haserror;
        private Boolean isdispensing;
        private Boolean inbezel;
        private int integer_part;
        private decimal decimal_part;
        private decimal floatamnt;
        private decimal coinfloat;
        private float payout;
        //private BackgroundWorker PayMonitor;
        private byte PrevPoll;
        /// <summary>
        /// Locking object used for thread safety.
        /// </summary>
        private static readonly object NVLocker = new object();
        //=============================
        // constructor
        public CNV11()
        {
            cmd = new SSP_COMMAND();
            keys = new SSP_KEYS();
            sspKey = new SSP_FULL_KEY();
            info = new SSP_COMMAND_INFO();

            //m_Comms = new CCommsWindow("NV11");
            m_TotalNotesAccepted = 0;
            m_TotalNotesDispensed = 0;
            m_NumberOfChannels = 0;
            m_ValueMultiplier = 1;
            m_CurrentPollResponse = new byte[256];
            m_CurrentPollResponseLength = 0;
            m_UnitDataList = new List<ChannelData>();
            m_NotePositionValues = new int[30];//note position level

            //=====vic====================================
            //this.PayMonitor = new BackgroundWorker();
            //this.PayMonitor.WorkerSupportsCancellation = true;
            //this.PayMonitor.WorkerReportsProgress = true;
            //// this.PayMonitor.DoWork += this.PayMonitor_Dowork;
            //this.PayMonitor.ProgressChanged += this.PayMonitor_ProgressChanged;
            //this.PayMonitor.RunWorkerCompleted += this.PayMonitor_RunWorkerCompleted;
            //=============================================

        }

        #region Events
        /// <summary>
        /// Occurs whenever a customer has paid for a treat.
        /// </summary>
        public event EventHandler<EventArgs> PaymentReceived;

        public event EventHandler<LogMessageEventArgs> LogEventMessage;

        public event EventHandler<PaidEventArgs> PaidAmountEvent;

        public event EventHandler<EventArgs> Dispensing;

        public event EventHandler<EventArgs> Dispensed;

        public event EventHandler<PaidEventArgs> NoteStored;

        public event EventHandler<EventArgs> RejectedNote;

        public event EventHandler<EventArgs> NoEnoughPayout;

        public event EventHandler<EventArgs> ESCROW;

        public event EventHandler<EventArgs> ReadingNote;

        public event EventHandler<EventArgs> CashBoxFull;

        public event EventHandler<EventArgs> OutOfService;

        public event EventHandler<EventArgs> ValidatorEnable;

        public event EventHandler<EventArgs> ValidatorDisable;

        public event EventHandler<EventArgs> FraudAttemp;

        public event EventHandler<EventArgs> UnSafe_Jam;

        public event EventHandler<EventArgs> Safe_Jam;

        // Flag: Has Dispose already been called? 
        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);
        #endregion

        #region properties

        public float PAYOUT
        {
            get { return payout; }
            set { payout = value; }
        }
        public int Integer_Part
        {
            get { return integer_part; }
            set { integer_part = value; }
        }

        public decimal Decimap_Part
        {
            get { return decimal_part; }
            set { decimal_part = value; }
        }

        public Boolean isDispensing
        {
            get { return isdispensing; }
            set { isdispensing = value; }
        }

        /* Variable Access */
        public Boolean HasError
        {
            get { return haserror; }
            set { haserror = value; }
        }

        public Boolean isDispensed
        {
            get { return isdispensed; }
            set { isdispensed = value; }
        }

        public Boolean inBezel
        {
            get { return inbezel; }
        }

        public Boolean isDISABLED
        {
            get { return isdisabled; }
            set { isdisabled = value; }
        }

        public Boolean isPaid
        {
            get { return ispaid; }
            set { ispaid = value; }
        }

        public float PRICE
        {
            get { return price; }
            set { price = value; }
        }

        public float PAID_AMOUNT
        {
            get { return paidamnt; }
            set { paidamnt = value; }
        }

        public decimal TOTAL_NOTES
        {
            get { return totalnotes; }
        }

        public decimal MinFloat_AMOUNT
        {
            get { return floatamnt; }
            set { floatamnt = value; }
        }

        public decimal CoinFloat
        {
            get { return this.coinfloat; }
            set { this.coinfloat = value; }
        }

        public SSP_COMMAND CommandStructure
        {
            get { return cmd; }
            set { cmd = value; }
        }

        public SSP_COMMAND_INFO InfoStructure
        {
            get { return info; }
            set { info = value; }
        }

        //// access to comms
        //public CCommsWindow CommsLog
        //{
        //    get { return m_Comms; }
        //    set { m_Comms = value; }
        //}
        // access to notes accepted
        public int NotesAccepted
        {
            get { return m_TotalNotesAccepted; }
            set { m_TotalNotesAccepted = value; }
        }

        // access to notes dispensed
        public int NotesDispensed
        {
            get { return m_TotalNotesDispensed; }
            set { m_TotalNotesDispensed = value; }
        }

        // access to number of channels
        public int NumberOfChannels
        {
            get { return m_NumberOfChannels; }
            set { m_NumberOfChannels = value; }
        }

        // access to value multiplier
        public int Multiplier
        {
            get { return m_ValueMultiplier; }
            set { m_ValueMultiplier = value; }
        }

        // access to the list of data items
        public List<ChannelData> UnitDataList
        {
            get { return m_UnitDataList; }
        }
        #endregion

        protected void OnRejectedNote(EventArgs e)
        {
            EventHandler<EventArgs> rejected = this.RejectedNote;
            if (rejected != null)
            {
                rejected(this, e);
            }

        }

        protected void OnNoEnoughPayout(EventArgs e)
        {
            EventHandler<EventArgs> noenoughpayout = this.NoEnoughPayout;
            if (noenoughpayout != null)
            {
                noenoughpayout(this, e);
            }
        }

        protected void OnESCROW(EventArgs e)
        {
            EventHandler<EventArgs> escrow = this.ESCROW;
            if (escrow != null)
            {
                escrow(this, e);
            }
        }

        protected void OnReadingNote(EventArgs e)
        {
            EventHandler<EventArgs> readingnote = this.ReadingNote;
            if (readingnote != null) readingnote(this, e);

        }

        protected void OnCashBoxFull(EventArgs e)
        {
            EventHandler<EventArgs> cashboxfull = this.CashBoxFull;
            if (cashboxfull != null) cashboxfull(this, e);

        }

        protected void onOutofService(EventArgs e)
        {
            EventHandler<EventArgs> outofservice = this.OutOfService;
            if (outofservice != null) outofservice(this, e);

        }

        protected void OnEnable(EventArgs e)
        {
            EventHandler<EventArgs> validatorenable = this.ValidatorEnable;
            if (validatorenable != null) validatorenable(this, e);
        }

        protected void OnDisable(EventArgs e)
        {
            EventHandler<EventArgs> validatordisable = this.ValidatorDisable;
            if (validatordisable != null) validatordisable(this, e);
        }

        protected void OnFraudAttempt(EventArgs e)
        {
            EventHandler<EventArgs> fraud = this.FraudAttemp;
            if (fraud != null) fraud(this, e);
        }

        protected void OnUnSafeJam(EventArgs e)
        {
            EventHandler<EventArgs> unsafejam = this.UnSafe_Jam;
            if (unsafejam != null) unsafejam(this, e);

        }

        protected void OnSafeJam(EventArgs e)
        {
            EventHandler<EventArgs> safejam = this.Safe_Jam;
            if (safejam != null) safejam(this, e);
        }

        protected void OnLogEventMessage(LogMessageEventArgs e)
        {
            EventHandler<LogMessageEventArgs> logeventmessage = this.LogEventMessage;
            if (logeventmessage != null) logeventmessage(this, e);

        }

        protected void OnPaymentReceived(EventArgs e)
        {
            EventHandler<EventArgs> paymentReceived = this.PaymentReceived;

            if (paymentReceived != null)
            {
                paymentReceived(this, e);
            }
        }

        protected void OnDispensing(EventArgs e)
        {
            EventHandler<EventArgs> dispensing = this.Dispensing;
            if (dispensing != null)
            {
                dispensing(this, e);
            }
        }

        protected void OnDispensed(EventArgs e)
        {
            EventHandler<EventArgs> dispensed = this.Dispensed;
            if (dispensed != null)
            {
                dispensed(this, e);
            }

        }

        protected void OnNoteStored(PaidEventArgs e)
        {
            EventHandler<PaidEventArgs> notestored = this.NoteStored;
            if (notestored != null)
            {
                notestored(this, e);
            }
        }

        protected void OnPaidAmountEvent(PaidEventArgs e)
        {
            EventHandler<PaidEventArgs> paidamountevent = this.PaidAmountEvent;
            if (paidamountevent != null) paidamountevent(this, e);
        }

        // get a channel value
        public int GetChannelValue(int channelNum)
        {
            if (channelNum >= 1 && channelNum <= m_NumberOfChannels)
            {
                foreach (ChannelData d in m_UnitDataList)
                {
                    if (d.Channel == channelNum)
                        return d.Value;
                }
            }
            return -1;
        }

        /* Command functions */
        // The enable command allows the validator to receive and act on commands sent to it.
        public void EnableValidator()
        {
            short retryCount = 5;
            bool commandsuccess = false;
            bool responsesuccess = false;

            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_ENABLE;
            cmd.CommandDataLength = 1;

            //if (!SendCommand(log)) return;
            // check response
            //if (CheckGenericResponses(log))
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Validator enabled\r\n")));
            //    isdisabled = false;
            //}
            while (!commandsuccess && retryCount > 0)
            {
                retryCount--;
                lock (NVLocker)
                {
                    commandsuccess = SendCommand();
                    Thread.Sleep(20);
                }
            }
            if (!commandsuccess) return;
            retryCount = 5;
            //while (!responsesuccess && retryCount > 0)
            //{
            //    retryCount--;
            //    lock (NVLocker)
            //    {
            responsesuccess = CheckGenericResponses();
            //Thread.Sleep(50);
            //    }
            //}
            if (responsesuccess)
            {
                try
                {
                    this.OnLogEventMessage(new LogMessageEventArgs("Validator enabled\r\n"));
                    this.OnEnable(EventArgs.Empty);
                    //if (log != null) log.Invoke(new Action(() => log.AppendText("Validator enabled\r\n")));
                }
                catch { }
                isdisabled = false;
            }
        }

        //=========================================

        // Disable command stops the validator from acting on commands.
        public void DisableValidator()
        {
            short retryCount = 3;                   // Retries left.
            bool commandsuccess = false;
            bool responsesuccess = false;

            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_DISABLE;
            cmd.CommandDataLength = 1;
            //if (!SendCommand(log)) return;
            //// check response
            //if (CheckGenericResponses(log))
            //{
            //    if (log != null) log.Invoke(new Action(() => log.AppendText("Validator disabled\r\n")));
            //    isdisabled = true;
            //}
            while (!commandsuccess && retryCount > 0)
            {
                retryCount--;
                lock (NVLocker)
                {
                    commandsuccess = SendCommand();
                    Thread.Sleep(20);
                }
            }

            if (!commandsuccess) return;
            isdisabled = true;
            retryCount = 3;
            //while (!responsesuccess && retryCount > 0)
            //{
            //    retryCount--;
            //    lock (NVLocker)
            //    {
            responsesuccess = CheckGenericResponses();
            //Thread.Sleep(50);
            //    }
            //}
            if (responsesuccess)
            {
                
                this.OnLogEventMessage(new LogMessageEventArgs("Validator disabled\r\n"));
                //if (log != null) log.Invoke(new Action(() => log.AppendText("Validator disabled\r\n")));
                
            }
        }

        // Enable payout allows the validator to payout and store notes.
        public void EnablePayout()
        {
            if (m_UnitType == (char)0x07)
            {
                short retryCount = 4;                   // Retries left.
                bool commandsuccess = false;
                bool responsesuccess = false;

                cmd.CommandData[0] = CCommands.NV11.SSP_CMD_ENABLE_PAYOUT;
                cmd.CommandData[1] = 0x01; // second byte to enable note value to be sent with stored event
                cmd.CommandDataLength = 2;

                //if (!SendCommand(log)) return;
                //if (CheckGenericResponses(log))
                //    if (log != null)log.Invoke(new Action(() => log.AppendText("Payout enabled\r\n")));

                while (!commandsuccess && retryCount > 0)
                {
                    retryCount--;
                    lock (NVLocker)
                    {
                        commandsuccess = SendCommand();
                    }
                }
                if (!commandsuccess) return;
                retryCount = 4;
                //while (!responsesuccess && retryCount > 0)
                //{
                //    retryCount--;
                //    lock (NVLocker)
                //    {
                responsesuccess = CheckGenericResponses();
                //    }
                //}
                if (responsesuccess)
                {
                    try
                    {
                        this.OnLogEventMessage(new LogMessageEventArgs("Payout enabled\r\n"));
                        //if (log != null) log.Invoke(new Action(() => log.AppendText("Payout enabled\r\n")));
                    }
                    catch { }
                    isdisabled = false;
                }
            }
        }

        // Disable payout stops the validator being able to store/payout notes.
        public void DisablePayout()
        {
            if (m_UnitType != (char)0x07) return;

            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_DISABLE_PAYOUT;
            cmd.CommandDataLength = 1;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                isdisabled = true;
                this.OnLogEventMessage(new LogMessageEventArgs("Payout disabled\r\n"));
                //if (log != null) log.Invoke(new Action(() => log.AppendText("Payout disabled\r\n")));
            }
        }

        // Empty payout device takes all the notes stored and moves them to the cashbox. This function also
        // updates the cashbox and stored totals.
        public void EmptyPayoutDevice()
        {
            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_EMPTY;
            cmd.CommandDataLength = 1;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                this.OnLogEventMessage(new LogMessageEventArgs("Emptying payout device\r\n"));
                //if (log != null) log.Invoke(new Action(() => log.AppendText("Emptying payout device\r\n")));
            }
        }

        // Payout last note command takes the last note paid in and dispenses it first (LIFO system). 
        public void PayoutNextNote()
        {
            short retryCount = 4;
            bool commandsuccess = false;
            bool responsesuccess = false;

            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_PAYOUT_LAST_NOTE;
            cmd.CommandDataLength = 1;

            while (!commandsuccess && retryCount >= 0)
            {
                retryCount--;
                lock (NVLocker)
                {
                    commandsuccess = SendCommand();
                }
            }
            if (!commandsuccess) return;
            retryCount = 4;

            responsesuccess = CheckGenericResponses();
            if (responsesuccess)
            {
                this.OnLogEventMessage(new LogMessageEventArgs("Paying out next note\r\n"));
                //if (log != null) log.Invoke(new Action(() => log.AppendText("Paying out next note\r\n")));
                //isdisabled = true;
            }
        }

        public void Hold()
        {
            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_HOLD;//    .NV11.SSP_CMD_PAYOUT_LAST_NOTE;
            cmd.CommandDataLength = 1;
            if (!SendCommand()) return;

            if (CheckGenericResponses())
            {
                this.isdispensed = false;
                this.OnLogEventMessage(new LogMessageEventArgs("Hold\r\n"));
                //if (log != null) log.Invoke(new Action(() => log.AppendText("Hold\r\n")));
            }
        }

        public void Reject_Note()
        {
            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_REJECTNOTE;
            cmd.CommandDataLength = 1;
            if (!SendCommand()) return;

            if (CheckGenericResponses())
            {
                this.isdispensed = false;
                this.OnLogEventMessage(new LogMessageEventArgs("rejecting notes\r\n"));
                this.OnRejectedNote(EventArgs.Empty);
                //if (log != null) log.Invoke(new Action(() => log.AppendText("rejecting notes\r\n")));
            }
        }

        public void Return_Note()
        {
            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_REJECTNOTE;
            cmd.CommandDataLength = 1;
            if (!SendCommand()) return;

            if (CheckGenericResponses())
            {
                this.isdispensed = false;
                this.OnLogEventMessage(new LogMessageEventArgs("returning notes\r\n"));
                //this.OnRejectedNote(EventArgs.Empty);
                //if (log != null) log.Invoke(new Action(() => log.AppendText("rejecting notes\r\n")));
            }
        }

        // Set value reporting type changes the validator to return either the value of the note, or the channel it is stored on
        // depending on what byte is sent after the command (0x01 is channel, 0x00 is 4 bit value).
        public void SetValueReportingType(bool byChannel)
        {
            if (m_UnitType != (char)0x07) return;
            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_SET_VALUE_REPORTING_TYPE;
            if (byChannel)
                cmd.CommandData[1] = 0x01; // report by channel number
            else
                cmd.CommandData[1] = 0x00; // report by 4 bit value
            cmd.CommandDataLength = 2;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
                try
                {
                    this.OnLogEventMessage(new LogMessageEventArgs("Value reporting type changed\r\n"));
                    //if (log != null) log.Invoke(new Action(() => log.AppendText("Value reporting type changed\r\n")));
                }
                catch { }
        }

        // The set routing command changes the way the validator deals with a note, either it can send the note straight to the cashbox
        // or it can store the note for payout. This is specified in the second byte (0x00 to store for payout, 0x01 for cashbox). The 
        // bytes after this represent the 4 bit value of the note, or the channel (see SetValueReportingType()).
        // This function allows the note to be specified as an int in the param note, the stack bool is true for cashbox, false for storage.
        public void ChangeNoteRoute(int note, char[] currency, bool stack)
        {
            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_SET_ROUTING;

            // if this note is being changed to stack (cashbox)
            if (stack)
                cmd.CommandData[1] = 0x01;
            // note being stored (payout)
            else
                cmd.CommandData[1] = 0x00;

            // get the note as a byte array
            byte[] b = BitConverter.GetBytes(note);
            cmd.CommandData[2] = b[0];
            cmd.CommandData[3] = b[1];
            cmd.CommandData[4] = b[2];
            cmd.CommandData[5] = b[3];

            // send country code
            cmd.CommandData[6] = (byte)currency[0];
            cmd.CommandData[7] = (byte)currency[1];
            cmd.CommandData[8] = (byte)currency[2];

            cmd.CommandDataLength = 9;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                string s;
                if (stack) s = " to cashbox)\r\n";
                else s = " to storage)\r\n";
                try
                {
                    if (!stack)
                    {
                        this.OnLogEventMessage(new LogMessageEventArgs("Note routing successful (" + CHelpers.FormatToCurrency(note) + s));
                    }
                    //if (log != null) log.Invoke(new Action(() => log.AppendText("Note routing successful (" + CHelpers.FormatToCurrency(note) + s)));
                    //else Globals.LogFile("Note routing successful (" + CHelpers.FormatToCurrency(note) + s);
                }
                catch { }
            }
            //just to stabilized
            CHelpers.Pause(500);
        }

        // This function sends the command LAST REJECT CODE which gives info about why a note has been rejected. It then
        // outputs the info to a passed across textbox.
        public void QueryRejection()
        {
            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_LAST_REJECT_CODE;
            cmd.CommandDataLength = 1;
            if (!SendCommand()) return;

            if (CheckGenericResponses())
            {
                switch (cmd.ResponseData[1])
                {
                    case 0x00: this.OnLogEventMessage(new LogMessageEventArgs("Note accepted\r\n")); break;
                    case 0x01: this.OnLogEventMessage(new LogMessageEventArgs("Note length incorrect\r\n")); break;
                    case 0x02: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note\r\n")); break;
                    case 0x03: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note\r\n")); break;
                    case 0x04: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note\r\n")); break;
                    case 0x05: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note\r\n")); break;
                    case 0x06: this.OnLogEventMessage(new LogMessageEventArgs("Channel inhibited\r\n")); break;
                    case 0x07: this.OnLogEventMessage(new LogMessageEventArgs("Second note inserted during read\r\n")); break;
                    case 0x08: this.OnLogEventMessage(new LogMessageEventArgs("Host rejected note\r\n")); break;
                    case 0x09: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note\r\n")); break;
                    case 0x0A: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note read\r\n")); break;
                    case 0x0B: this.OnLogEventMessage(new LogMessageEventArgs("Note too long\r\n")); break;
                    case 0x0C: this.OnLogEventMessage(new LogMessageEventArgs("Validator disabled\r\n")); break;
                    case 0x0D: this.OnLogEventMessage(new LogMessageEventArgs("Mechanism slow/stalled\r\n")); break;
                    case 0x0E: this.OnLogEventMessage(new LogMessageEventArgs("Strim attempt\r\n")); break;
                    case 0x0F: this.OnLogEventMessage(new LogMessageEventArgs("Fraud channel reject\r\n")); break;
                    case 0x10: this.OnLogEventMessage(new LogMessageEventArgs("No notes inserted\r\n")); break;
                    case 0x11: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note read\r\n")); break;
                    case 0x12: this.OnLogEventMessage(new LogMessageEventArgs("Twisted note detected\r\n")); break;
                    case 0x13: this.OnLogEventMessage(new LogMessageEventArgs("Escrow time-out\r\n")); break;
                    case 0x14: this.OnLogEventMessage(new LogMessageEventArgs("Bar code scan fail\r\n")); break;
                    case 0x15: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note read\r\n")); break;
                    case 0x16: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note read\r\n")); break;
                    case 0x17: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note read\r\n")); break;
                    case 0x18: this.OnLogEventMessage(new LogMessageEventArgs("Invalid note read\r\n")); break;
                    case 0x19: this.OnLogEventMessage(new LogMessageEventArgs("Incorrect note width\r\n")); break;
                    case 0x1A: this.OnLogEventMessage(new LogMessageEventArgs("Note too short\r\n")); break;
                }
            }
        }

        // The get note positions command instructs the validator to return in the second byte the number of
        // notes stored and then in the following bytes, the values/channel (see SetValueReportingType()) of the stored
        // notes. The length of the response will vary based on the number of stored notes.
        public void CheckForStoredNotes()
        {
            //check only if device is NV11
            if (m_UnitType != (char)0x07) return;

            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_GET_NOTE_POSITIONS;
            cmd.CommandDataLength = 1;
            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                byte numNotes = cmd.ResponseData[1];
                Array.Clear(m_NotePositionValues, 0, m_NotePositionValues.Length);
                m_NotePositionValues = new int[numNotes];
                for (int i = 0; i < numNotes; ++i)
                {
                    try
                    {
                        m_NotePositionValues[i] = BitConverter.ToInt32(cmd.ResponseData, i * 4 + 2);
                    }
                    catch { }

                }
            }
        }

        // The stack last note command is similar to the payout last note command (PayoutNextNote()) except it
        // moves the stored notes to the cashbox instead of dispensing.
        public void StackNextNote()
        {
            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_STACK_LAST_NOTE;
            cmd.CommandDataLength = 1;
            if (!SendCommand()) return;

            if (CheckGenericResponses())
            {
                this.OnLogEventMessage(new LogMessageEventArgs("Moved note from storage to cashbox\r\n"));
                //if (log != null) log.Invoke(new Action(() => log.AppendText("Moved note from storage to cashbox\r\n")));
            }
        }

        // The reset command instructs the validator to restart (same effect as switching on and off)
        public void Reset()
        {
            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_RESET;
            cmd.CommandDataLength = 1;

            if (!SendCommand()) return;
            CheckGenericResponses();
        }

        // This function sets the protocol version in the validator to the version passed across. Whoever calls
        // this needs to check the response to make sure the version is supported.
        public void SetProtocolVersion(byte pVersion)
        {
            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_HOST_PROTOCOL_VERSION;
            cmd.CommandData[1] = pVersion;
            cmd.CommandDataLength = 2;
            if (!SendCommand()) return;
        }

        // This function performs a number of commands in order to setup the encryption between the host and the validator.
        public bool NegotiateKeys()
        {
            byte i;
            string s = "";

            // make sure encryption is off
            cmd.EncryptionStatus = false;

            // send sync
            s += "Syncing... ";
            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_SYNC;
            cmd.CommandDataLength = 1;

            if (!SendCommand()) return false;
            s += "Success\r\n";

            NV11libHdlr.InitiateKeys(ref keys, ref cmd);

            // send generator
            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_SET_GENERATOR;
            cmd.CommandDataLength = 9;
            s += "Setting generator... ";
            for (i = 0; i < 8; i++)
            {
                cmd.CommandData[i + 1] = (byte)(keys.Generator >> (8 * i));
            }

            if (!SendCommand()) return false;
            s += "Success\r\n";

            // send modulus
            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_SET_MODULUS;
            cmd.CommandDataLength = 9;
            s += "Sending modulus... ";
            for (i = 0; i < 8; i++)
            {
                cmd.CommandData[i + 1] = (byte)(keys.Modulus >> (8 * i));
            }

            if (!SendCommand()) return false;
            s += "Success\r\n";

            // send key exchange
            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_REQUEST_KEY_EXCHANGE;
            cmd.CommandDataLength = 9;
            s += "Exchanging keys... ";
            for (i = 0; i < 8; i++)
            {
                cmd.CommandData[i + 1] = (byte)(keys.HostInter >> (8 * i));
            }

            if (!SendCommand()) return false;
            s += "Success\r\n";

            keys.SlaveInterKey = 0;
            for (i = 0; i < 8; i++)
            {
                keys.SlaveInterKey += (UInt64)cmd.ResponseData[1 + i] << (8 * i);
            }

            NV11libHdlr.CreateFullKey(ref keys);

            // get full encryption key
            cmd.Key.FixedKey = 0x0123456701234567;
            cmd.Key.VariableKey = keys.KeyHost;

            s += "Keys successfully negotiated\r\n";
            this.OnLogEventMessage(new LogMessageEventArgs(s));

            return true;
        }

        // This function uses the setup request command to get all the information about the validator.
        public void SetupRequest()
        {
            // send setup request
            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_SETUP_REQUEST;
            cmd.CommandDataLength = 1;

            if (!SendCommand()) return;

            // display setup request
            string displayString = "Unit Type: ";
            int index = 1;

            // unit type (table 0-1)
            m_UnitType = (char)cmd.ResponseData[index++];
            switch (m_UnitType)
            {
                case (char)0x00: displayString += "Validator"; break;
                case (char)0x03: displayString += "SMART Hopper"; break;
                case (char)0x06: displayString += "SMART Payout"; break;
                case (char)0x07: displayString += "NV11"; break;
                default: displayString += "Unknown Type"; break;
            }

            displayString += "\r\nFirmware: ";

            // firmware (table 2-5)
            while (index <= 5)
            {
                displayString += (char)cmd.ResponseData[index++];
                if (index == 4)
                    displayString += ".";
            }

            // country code (table 6-8)
            // this is legacy code, in protocol version 6+ each channel has a seperate currency
            index = 9; // to skip country code

            // value multiplier (table 9-11) 
            // also legacy code, a real value multiplier appears later in the response
            index = 12; // to skip value multiplier

            displayString += "\r\nNumber of Channels: ";
            int numChannels = cmd.ResponseData[index++];
            m_NumberOfChannels = numChannels;

            displayString += numChannels + "\r\n";
            // channel values (table 13 to 13+n)
            // the channel values located here in the table are legacy, protocol 6+ provides a set of expanded
            // channel values.
            index = 13 + m_NumberOfChannels; // Skip channel values

            // channel security (table 13+n to 13+(n*2))
            // channel security values are also legacy code
            index = 13 + (m_NumberOfChannels * 2); // Skip channel security

            displayString += "Real Value Multiplier: ";

            // real value multiplier (table 13+(n*2) to 15+(n*2))
            // (big endian)
            m_ValueMultiplier = cmd.ResponseData[index + 2];
            m_ValueMultiplier += cmd.ResponseData[index + 1] << 8;
            m_ValueMultiplier += cmd.ResponseData[index] << 16;
            displayString += m_ValueMultiplier + "\r\nProtocol Version: ";
            index += 3;

            // protocol version (table 16+(n*2))
            index = 16 + (m_NumberOfChannels * 2);
            int protocol = cmd.ResponseData[index++];
            displayString += protocol + "\r\n";

            // protocol 6+ only

            // channel currency country code (table 17+(n*2) to 17+(n*5))
            index = 17 + (m_NumberOfChannels * 2);
            int sectionEnd = 17 + (m_NumberOfChannels * 5);
            int count = 0;
            byte[] channelCurrencyTemp = new byte[3 * m_NumberOfChannels];
            while (index < sectionEnd)
            {
                displayString += "Channel " + ((count / 3) + 1) + ", currency: ";
                channelCurrencyTemp[count] = cmd.ResponseData[index++];
                displayString += (char)channelCurrencyTemp[count++];
                channelCurrencyTemp[count] = cmd.ResponseData[index++];
                displayString += (char)channelCurrencyTemp[count++];
                channelCurrencyTemp[count] = cmd.ResponseData[index++];
                displayString += (char)channelCurrencyTemp[count++];
                displayString += "\r\n";
            }

            // expanded channel values (table 17+(n*5) to 17+(n*9))
            index = sectionEnd;
            displayString += "Expanded channel values:\r\n";
            sectionEnd = 17 + (m_NumberOfChannels * 9);
            int n = 0;
            count = 0;
            int[] channelValuesTemp = new int[m_NumberOfChannels];
            while (index < sectionEnd)
            {
                n = CHelpers.ConvertBytesToInt32(cmd.ResponseData, index);
                channelValuesTemp[count] = n;
                index += 4;
                displayString += "Channel " + ++count + ", value = " + n + "\r\n";
            }

            // Create list entry for each channel
            m_UnitDataList.Clear(); // clear old table
            for (byte i = 0; i < m_NumberOfChannels; i++)
            {
                ChannelData d = new ChannelData();
                d.Channel = i;
                d.Channel++; // Offset from array index by 1
                d.Value = channelValuesTemp[i] * Multiplier;
                d.Currency[0] = (char)channelCurrencyTemp[0 + (i * 3)];
                d.Currency[1] = (char)channelCurrencyTemp[1 + (i * 3)];
                d.Currency[2] = (char)channelCurrencyTemp[2 + (i * 3)];
                d.Level = 0; // Can only store notes in a LIFO system so level not used
                switch (m_UnitType)
                {
                    case (char)0x00: d.Recycling = false; break; // displayString += "Validator"; break;
                    case (char)0x07: IsNoteRecycling(d.Value, d.Currency, ref d.Recycling); break;
                }
                
                m_UnitDataList.Add(d);
            }

            // Sort the list of data by the value.
            m_UnitDataList.Sort(delegate(ChannelData d1, ChannelData d2) { return d1.Value.CompareTo(d2.Value); });
            this.OnLogEventMessage(new LogMessageEventArgs(displayString));
            //if (log != null && log.InvokeRequired)
            //    log.Invoke(new Action(() => log.AppendText(displayString)));
        }
        //======================end setup request=====================================

        // This function sends the set inhibits command to set the inhibits on the validator. An additional two
        // bytes are sent along with the command byte to indicate the status of the inhibits on the channels.
        // For example 0xFF and 0xFF in binary is 11111111 11111111. This indicates all 16 channels supported by
        // the validator are uninhibited. If a user wants to inhibit channels 8-16, they would send 0x00 and 0xFF.
        public void SetInhibits()
        {
            // set inhibits
            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_SET_INHIBITS;
            cmd.CommandData[1] = 0xFF;
            cmd.CommandData[2] = 0xFF;
            cmd.CommandDataLength = 3;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                try
                {
                    this.OnLogEventMessage(new LogMessageEventArgs("Inhibits set\r\n"));
                    //if (log != null) log.Invoke(new Action(() => log.AppendText("Inhibits set\r\n")));
                }
                catch { }
            }
        }


        // This function uses the GET ROUTING command to determine whether a particular note
        // is recycling.
        void IsNoteRecycling(int note, char[] currency, ref bool b)
        {
            cmd.CommandData[0] = CCommands.NV11.SSP_CMD_GET_ROUTING;
            byte[] byteArr = BitConverter.GetBytes(note);
            cmd.CommandData[1] = byteArr[0];
            cmd.CommandData[2] = byteArr[1];
            cmd.CommandData[3] = byteArr[2];
            cmd.CommandData[4] = byteArr[3];
            cmd.CommandData[5] = (byte)currency[0];
            cmd.CommandData[6] = (byte)currency[1];
            cmd.CommandData[7] = (byte)currency[2];
            cmd.CommandDataLength = 8;

            if (!SendCommand()) return;
            if (CheckGenericResponses())
            {
                if (cmd.ResponseData[1] == 0x00)
                    b = true;
                else
                    b = false;
            }
        }

        /// <summary>
        /// check for payout amount
        /// </summary>
        /// <param name="amnt"></param>
        /// <returns></returns>
        private bool EnoughPayout(float amnt)
        {
            decimal integral = (Decimal)Math.Truncate(amnt);
            decimal fractional = ((Decimal)amnt - (integral));
            if (integral == 0 && fractional != 0)
            {
                if (fractional > CoinFloat) return false;
            }
            else if ((integral != 0 && fractional == 0))
            {
                if (integral > TOTAL_NOTES && integral > CoinFloat) return false;
            }
            else
            {
                if (integral > TOTAL_NOTES && integral > CoinFloat && ((Decimal)amnt) > CoinFloat) return false;
            }
            return true;
        }

        // The poll function is called repeatedly to poll to validator for information, it returns as
        // a response in the command structure what events are currently happening.
        public bool DoPoll()
        {
            byte i;

            //send poll
            cmd.CommandData[0] = CCommands.Generic.SSP_CMD_POLL;
            cmd.CommandDataLength = 1;

            if (!SendCommand()) return false;

            // Check unit hasn't lost key (could be due to power loss or reset)
            if (cmd.ResponseData[0] == 0xFA) return false;

            // Store poll response to avoid corruption if the cmd structure is accessed whilst polling
            cmd.ResponseData.CopyTo(m_CurrentPollResponse, 0);
            m_CurrentPollResponseLength = cmd.ResponseDataLength;

            //parse poll m_CurrentPollResponse
            int noteVal = 0;
            for (i = 1; i < m_CurrentPollResponseLength; i++)
            {
                switch (m_CurrentPollResponse[i])
                {
                    // This m_CurrentPollResponse indicates that the unit was reset and this is the first time a poll
                    // has been called since the reset.
                    case CCommands.NV11.SSP_POLL_RESET:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Unit reset\r\n"));

                        }
                        catch { }

                        UpdateData();
                        break;
                    // A note is currently being read by the validator sensors. The second byte of this response
                    // is zero until the note's type has been determined, it then changes to the channel of the 
                    // scanned note.
                    case CCommands.NV11.SSP_POLL_NOTE_READ:
                        if (m_CurrentPollResponse[i + 1] > 0)
                        {
                            noteVal = GetChannelValue(m_CurrentPollResponse[i + 1]);

                            if (this.PrevPoll != CCommands.NV11.SSP_POLL_NOTE_READ) this.OnLogEventMessage(new LogMessageEventArgs("Note in escrow, amount: " + CHelpers.FormatToCurrency(noteVal) + "\r\n"));
                            //trigger ESCROW event=========================
                            this.OnESCROW(EventArgs.Empty);

                            //if (!Globals.Maintenance)
                            //{
                            //    float initAmnt = (noteVal * 0.01f) + this.paidamnt;

                            //    if (initAmnt > this.price)
                            //    {
                            //        if (!EnoughPayout(initAmnt - price))
                            //        {
                            //            EnablePayout();
                            //            this.Reject_Note();

                            //            this.OnLogEventMessage(new LogMessageEventArgs("no enough payout rejected: " + CHelpers.FormatToCurrency(noteVal) + "\r\n"));

                            //            this.OnNoEnoughPayout(EventArgs.Empty);
                            //        }
                            //    }
                            //}

                        }
                        else
                        {
                            this.OnReadingNote(EventArgs.Empty);

                            if (this.PrevPoll != CCommands.NV11.SSP_POLL_NOTE_READ) this.OnLogEventMessage(new LogMessageEventArgs("Reading note\r\n"));

                        }
                        i++;
                        break;
                    // A credit event has been detected, this is when the validator has accepted a note as legal currency.
                    case CCommands.NV11.SSP_POLL_CREDIT:
                        noteVal = GetChannelValue(m_CurrentPollResponse[i + 1]);

                        this.OnLogEventMessage(new LogMessageEventArgs("Credit " + CHelpers.FormatToCurrency(noteVal) + "\r\n"));
                        //===================== accepted notes========================================================
                        this.paidamnt += noteVal * 0.01f;
                        this.OnPaidAmountEvent(new PaidEventArgs("bills", (float)(Math.Round((double)(noteVal * 0.01f), 2))));
                        //============================================================================
                        NotesAccepted++;
                        UpdateData();
                        i++;
                        break;
                    // A note is being rejected from the validator. This will carry on polling while the note is in transit.
                    case CCommands.NV11.SSP_POLL_REJECTING:
                        break;
                    // A note has been rejected from the validator. This response only appears once.
                    case CCommands.NV11.SSP_POLL_REJECTED:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Note rejected\r\n"));

                        }
                        catch { }

                        QueryRejection();
                        UpdateData();
                        //this.OnRejectedNote(EventArgs.Empty);
                        break;
                    // A note is in transit to the cashbox.
                    case CCommands.NV11.SSP_POLL_STACKING:
                        try
                        {
                            if (this.PrevPoll != CCommands.NV11.SSP_POLL_STACKING) this.OnLogEventMessage(new LogMessageEventArgs("Stacking note\r\n"));

                        }
                        catch { }
                        break;
                    // A note has reached the cashbox.
                    case CCommands.NV11.SSP_POLL_STACKED:
                        this.OnNoteStored(new PaidEventArgs("bills", this.paidamnt));
                        this.OnLogEventMessage(new LogMessageEventArgs("Note stacked in cash box\r\n"));

                        break;
                    // A safe jam has been detected. This is where the user has inserted a note and the note
                    // is jammed somewhere that the user cannot reach.
                    case CCommands.NV11.SSP_POLL_SAFE_JAM:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Safe jam\r\n"));
                            if (this.PrevPoll != CCommands.NV11.SSP_POLL_SAFE_JAM) this.OnSafeJam(EventArgs.Empty);
                        }
                        catch { }
                        break;
                    // An unsafe jam has been detected. This is where a user has inserted a note and the note
                    // is jammed somewhere that the user can potentially recover the note from.
                    case CCommands.NV11.SSP_POLL_UNSAFE_JAM:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Unsafe jam\r\n"));
                            if (this.PrevPoll != CCommands.NV11.SSP_POLL_UNSAFE_JAM) this.OnUnSafeJam(EventArgs.Empty);

                        }
                        catch { }
                        break;
                    // The validator is disabled, it will not execute any commands or do any actions until enabled.
                    case CCommands.NV11.SSP_POLL_DISABLED:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Unit disabled (NV11)...\r\n"));
                            if (this.PrevPoll != CCommands.NV11.SSP_POLL_DISABLED) this.OnDisable(EventArgs.Empty);

                        }
                        catch { }

                        break;
                    // A fraud attempt has been detected. The second byte indicates the channel of the note that a fraud
                    // has been attempted on.
                    case CCommands.NV11.SSP_POLL_FRAUD_ATTEMPT:
                        this.OnLogEventMessage(new LogMessageEventArgs("Fraud attempt, note type: " + GetChannelValue(m_CurrentPollResponse[i + 1]) + "\r\n"));
                        if (this.PrevPoll != CCommands.NV11.SSP_POLL_FRAUD_ATTEMPT) this.OnFraudAttempt(EventArgs.Empty);
                        i++;
                        break;
                    // The stacker (cashbox) is full.
                    case CCommands.NV11.SSP_POLL_STACKER_FULL:
                        if (this.PrevPoll != CCommands.NV11.SSP_POLL_STACKER_FULL) this.OnCashBoxFull(EventArgs.Empty);
                        if (this.PrevPoll != CCommands.NV11.SSP_POLL_STACKER_FULL) this.OnLogEventMessage(new LogMessageEventArgs("Stacker full\r\n"));
                        break;
                    // A note was detected somewhere inside the validator on startup and was rejected from the front of the
                    // unit.
                    case CCommands.NV11.SSP_POLL_NOTE_CLEARED_FROM_FRONT:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs(GetChannelValue(m_CurrentPollResponse[i + 1]) + " note cleared from front at reset." + "\r\n"));
                        }
                        catch { }
                        i++;
                        break;
                    // A note was detected somewhere inside the validator on startup and was cleared into the cashbox.
                    case CCommands.NV11.SSP_POLL_NOTE_CLEARED_TO_CASHBOX:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs(GetChannelValue(m_CurrentPollResponse[i + 1]) + " note cleared to stacker at reset." + "\r\n"));
                        }
                        catch { }
                        i++;
                        break;
                    // The cashbox has been removed from the unit. This will continue to poll until the cashbox is replaced.
                    case CCommands.NV11.SSP_POLL_CASHBOX_REMOVED:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Cashbox removed\r\n"));
                        }
                        catch { }
                        break;
                    // The cashbox has been replaced, this will only display on a poll once.
                    case CCommands.NV11.SSP_POLL_CASHBOX_REPLACED:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Cashbox replaced\r\n"));
                        }
                        catch { }
                        break;
                    // A note has been stored in the payout device to be paid out instead of going into the cashbox.
                    case CCommands.NV11.SSP_POLL_NOTE_STORED:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Note stored in recyler\r\n"));

                        }
                        catch { }
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        UpdateData();
                        this.OnNoteStored(new PaidEventArgs("bills", this.paidamnt));

                        break;
                    // The validator is in the process of paying out a note, this will continue to poll until the note has 
                    // been fully dispensed and removed from the front of the validator by the user.
                    case CCommands.NV11.SSP_POLL_NOTE_DISPENSING:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Dispensing note\r\n"));
                        }
                        catch { }
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        this.isdispensed = false;
                        this.isdispensing = true;
                        this.OnDispensing(EventArgs.Empty);
                        break;
                    // The note has been dispensed and removed from the bezel by the user.
                    case CCommands.NV11.SSP_POLL_NOTE_DISPENSED:
                        for (int j = 0; j < m_CurrentPollResponse[i + 1]; j += 7)
                        {
                            try
                            {
                                this.OnLogEventMessage(new LogMessageEventArgs("Dispensed " + (CHelpers.ConvertBytesToInt32(m_CurrentPollResponse, j + i + 2) / 100).ToString() +
                                    " " + (char)m_CurrentPollResponse[j + i + 6] + (char)m_CurrentPollResponse[j + i + 7] +
                                    (char)m_CurrentPollResponse[j + i + 8] + "\r\n"));
                            }
                            catch { }
                        }
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        NotesDispensed++;
                        UpdateData();
                        this.inbezel = false;
                        this.isdispensed = true;
                        this.isdispensing = false;
                        EnableValidator();

                        this.OnDispensed(EventArgs.Empty);
                        break;
                    // A note has been transferred from the payout storage to the cashbox recycler is full
                    case CCommands.NV11.SSP_POLL_NOTE_TRANSFERRED_TO_STACKER:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Note stacked recycler is full => note strasferred \r\n"));
                        }
                        catch { }
                        UpdateData();
                        EnableValidator();
                        break;
                    // This single poll response indicates that the payout device has finished emptying.
                    case CCommands.NV11.SSP_POLL_EMPTIED:
                        try
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Device emptied\r\n"));
                        }
                        catch { }
                        UpdateData();
                        EnableValidator();
                        break;
                    // This response indicates a note is being dispensed and is resting in the bezel waiting to be removed
                    // before the validator can continue
                    case CCommands.NV11.SSP_POLL_NOTE_HELD_IN_BEZEL:
                        for (int j = 0; j < m_CurrentPollResponse[i + 1]; j += 7)
                        {
                            try
                            {
                                this.OnLogEventMessage(new LogMessageEventArgs((CHelpers.ConvertBytesToInt32(m_CurrentPollResponse, j + i + 2) / 100).ToString() +
                                    " " + (char)m_CurrentPollResponse[j + i + 6] + (char)m_CurrentPollResponse[j + i + 7] +
                                    (char)m_CurrentPollResponse[j + i + 8] + " held in bezel...\r\n"));

                            }
                            catch { }
                            inbezel = true;
                            this.isdispensed = false;
                        }
                        i += (byte)((m_CurrentPollResponse[i + 1] * 7) + 1);
                        UpdateData();
                        break;
                    case CCommands.NV11.SSP_POLL_OUTOFSERVICE:
                        if (this.PrevPoll != CCommands.NV11.SSP_POLL_OUTOFSERVICE) this.onOutofService(EventArgs.Empty);
                        if (this.PrevPoll != CCommands.NV11.SSP_POLL_OUTOFSERVICE) this.OnLogEventMessage(new LogMessageEventArgs("Payout out of service \r\n"));

                        break;
                    default:
                        break;
                }
                this.PrevPoll = m_CurrentPollResponse[i];
            }
            return true;
        }

        /* Non-Command functions */

        // Opens the COM port for the device using the Library handler
        public bool OpenPort()
        {
            // open com port
            try
            {
                this.OnLogEventMessage(new LogMessageEventArgs("Opening Note Validator com port:" + cmd.ComPort + "\r\n"));
                //if (log != null) log.Invoke(new Action(() => log.AppendText("Opening com port\r\n")));
            }
            catch { }
            return NV11libHdlr.OpenPort(ref cmd);
        }

        // This function uses the set routing command to send all notes to the cashbox, it just calls the
        // ChangeNoteRoute() function for each channel.

        public void RouteAllToStack()
        {
            // all notes from channels need setting
            foreach (ChannelData d in m_UnitDataList)
            {
                ChangeNoteRoute(d.Value, d.Currency, true);
                //if (d.Level == 0)
                //{ ChangeNoteRoute(d.Value, d.Currency, false); }
                //else
                //{
                //    ChangeNoteRoute(d.Value, d.Currency, true);
                //}
            }
        }

        // This function returns a formatted string of what notes are available in the NV11 storage device.
        public string GetStorageInfo()
        {
            string s = "";
            decimal totalNote = 0;
            for (int i = m_NotePositionValues.Length - 1; i >= 0; --i)
            {
                if (m_NotePositionValues[i] > 0)
                    s += "Position " + i + ": " + (m_NotePositionValues[i] / 100).ToString("00.00") + "\r\n";
                totalNote += (m_NotePositionValues[i]);  // / 100);
            }
            this.totalnotes = totalNote;
            return s;
        }

        // This function updates the internal structures to check they are in sync with the validator.
        void UpdateData()
        {
            foreach (ChannelData d in m_UnitDataList)
            {
                IsNoteRecycling(d.Value, d.Currency, ref d.Recycling);
            }
            CheckForStoredNotes();
        }

        // This function returns the last note that was stored by the payout
        public int GetLastNoteValue()
        {
            if (m_NotePositionValues.Length > 0)
                return m_NotePositionValues[m_NotePositionValues.Length - 1];
            return 0;
        }

        public int GetLastNoteValue(int index)
        {
            if (m_NotePositionValues.Length > 0)
                return m_NotePositionValues[index];
            return 0;
        }

        /* Exception and Error Handling */

        // This is used for generic response error catching, it outputs the info in a
        // meaningful way.
        private bool CheckGenericResponses()
        {
            if (cmd.ResponseData[0] == CCommands.Generic.SSP_RESPONSE_OK)
                return true;
            else
            {

                switch (cmd.ResponseData[0])
                {
                    case CCommands.Generic.SSP_RESPONSE_CMD_CANNOT_BE_PROCESSED:
                        if (cmd.ResponseData[1] == 0x03)
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Validator has responded with \"Busy\", command cannot be processed at this time: NV\r\n"));
                        }
                        else
                        {
                            this.OnLogEventMessage(new LogMessageEventArgs("Command response is CANNOT PROCESS COMMAND, error code - 0x"
                                + BitConverter.ToString(cmd.ResponseData, 1, 1) + ": NV\r\n"));

                        }
                        return false;
                    case CCommands.Generic.SSP_RESPONSE_FAIL:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is FAIL: NV\r\n"));

                        return false;
                    case CCommands.Generic.SSP_RESPONSE_KEY_NOT_SET:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is KEY NOT SET, renegotiate keys: NV\r\n"));
                        return false;
                    case CCommands.Generic.SSP_RESPONSE_PARAMS_OUT_OF_RANGE:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is PARAM OUT OF RANGE: NV\r\n"));
                        return false;
                    case CCommands.Generic.SSP_RESPONSE_SOFTWARE_ERROR:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is SOFTWARE ERROR: NV\r\n"));
                        return false;
                    case CCommands.Generic.SSP_RESPONSE_COMMAND_NOT_KNOWN:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is UNKNOWN: NV\r\n"));
                        return false;
                    case CCommands.Generic.SSP_RESPONSE_WRONG_NUMBER_OF_PARAMS:
                        this.OnLogEventMessage(new LogMessageEventArgs("Command response is WRONG PARAMETERS: NV\r\n"));
                        return false;
                    default:
                        return false;

                }
            }
        }

        public bool SendCommand()
        {
            // Backup data and length in case we need to retry
            byte[] backup = new byte[255];
            cmd.CommandData.CopyTo(backup, 0);
            byte length = cmd.CommandDataLength;

            // attempt to send the command
            if (!NV11libHdlr.SendCommand(ref cmd, ref info))
            {
                //m_Comms.UpdateLog(info, true); // Update on fail
                this.OnLogEventMessage(new LogMessageEventArgs("Sending command failed\r\nPort status: " + cmd.ResponseStatus.ToString() + "\r\n"));
                return false;
            }

            // update the log after every command
            //m_Comms.UpdateLog(info);
            return true;
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
            //CHelpers.Shutdown = true;
            //this.PayMonitor.CancelAsync();
            //this.PayMonitor.Dispose();
            //this.PayMonitor = null;

            Dispose(true);
            GC.SuppressFinalize(this);

        }
        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
                // Free any other managed objects here. 
                //
            }

            // Free any unmanaged objects here. 
            //
            disposed = true;
        }

        public void ClosePort()
        {
            NV11libHdlr.ClosePort();

            return;
        }
    }
}
