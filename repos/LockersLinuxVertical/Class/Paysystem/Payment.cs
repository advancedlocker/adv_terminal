﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lockers
{
    public class Payment
    {
        #region fields
        private float Amount;
        private PaySource psource;
        #endregion

        #region properties
        public float AmountInserted
        {
            get { return this.Amount; }
            set { this.Amount = value; }
        }

        public PaySource PSource
        {
            get { return this.psource; }
            set { this.psource = value; }
        }
        //constructor
        public Payment(){}
        #endregion
    }
}
