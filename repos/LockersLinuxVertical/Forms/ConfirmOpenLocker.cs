﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace Lockers
{
    public partial class OpenLocker : Form
    {
        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        private Button prevBtn;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void ResetBackground()
        {
//            this.BackgroundImage = ConfigurationData.GetBackground("Language" + Globals.LanguageCode.ToString());
            String ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\Re-enter_Locker_1182x788.tiff";
            this.BackgroundImage = Image.FromFile(ImageFilename);

        }

        private void btnLanguage_Click(object sender, EventArgs e)
        {

            Button btn = (Button)sender;
            Globals.LanguageCode = Convert.ToInt32(btn.Tag);
            //================reset font ========================
            Globals.prevBtn.Font = new Font
         (
            btn.Font,
            FontStyle.Regular
         );
            //======================================

            btn.Font = new Font
         (
            btn.Font,
            FontStyle.Underline | FontStyle.Bold
         );
            ResetBackground();
            Globals.prevBtn = btn;

        }

        public OpenLocker()
        {
            InitializeComponent();

            ResetBackground();

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
            timIdle.Start();
        }

        private void bBack_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            timer1.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void OpenLocker_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
        }

        private void bEndRental_Click(object sender, EventArgs e)
        {
            Globals.Process = ProcessType.endrent;
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void bOpenLocker_Click(object sender, EventArgs e)
        {
            Globals.Process = ProcessType.open;
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
