﻿namespace Lockers
{
    partial class Unlocked
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Unlocked));
            this.button10 = new System.Windows.Forms.Button();
            this.btnback = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.lblremaintime = new System.Windows.Forms.Label();
            this.lblLocknum = new System.Windows.Forms.Label();
            this.picBoxArrow = new System.Windows.Forms.PictureBox();
            this.labBack = new System.Windows.Forms.Label();
            this.labOK = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxArrow)).BeginInit();
            this.SuspendLayout();
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.Transparent;
            this.button10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button10.BackgroundImage")));
            this.button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Location = new System.Drawing.Point(659, 430);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(65, 65);
            this.button10.TabIndex = 5;
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // btnback
            // 
            this.btnback.BackColor = System.Drawing.Color.Transparent;
            this.btnback.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnback.BackgroundImage")));
            this.btnback.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnback.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnback.FlatAppearance.BorderSize = 0;
            this.btnback.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnback.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnback.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnback.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnback.Location = new System.Drawing.Point(48, 420);
            this.btnback.Name = "btnback";
            this.btnback.Size = new System.Drawing.Size(65, 65);
            this.btnback.TabIndex = 7;
            this.btnback.UseVisualStyleBackColor = false;
            this.btnback.Click += new System.EventHandler(this.btnback_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(17, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "TIME";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(12, 48);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(167, 40);
            this.lbltime.TabIndex = 1;
            this.lbltime.Text = "01:22 pm";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // lblremaintime
            // 
            this.lblremaintime.AutoSize = true;
            this.lblremaintime.BackColor = System.Drawing.Color.Transparent;
            this.lblremaintime.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblremaintime.Location = new System.Drawing.Point(60, 335);
            this.lblremaintime.Name = "lblremaintime";
            this.lblremaintime.Size = new System.Drawing.Size(119, 46);
            this.lblremaintime.TabIndex = 24;
            this.lblremaintime.Text = "00:00";
            this.lblremaintime.Visible = false;
            // 
            // lblLocknum
            // 
            this.lblLocknum.AutoSize = true;
            this.lblLocknum.BackColor = System.Drawing.Color.Transparent;
            this.lblLocknum.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocknum.Location = new System.Drawing.Point(60, 210);
            this.lblLocknum.Name = "lblLocknum";
            this.lblLocknum.Size = new System.Drawing.Size(42, 46);
            this.lblLocknum.TabIndex = 25;
            this.lblLocknum.Text = "0";
            // 
            // picBoxArrow
            // 
            this.picBoxArrow.Image = ((System.Drawing.Image)(resources.GetObject("picBoxArrow.Image")));
            this.picBoxArrow.Location = new System.Drawing.Point(298, 210);
            this.picBoxArrow.Name = "picBoxArrow";
            this.picBoxArrow.Size = new System.Drawing.Size(176, 132);
            this.picBoxArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxArrow.TabIndex = 26;
            this.picBoxArrow.TabStop = false;
            this.picBoxArrow.Visible = false;
            // 
            // labBack
            // 
            this.labBack.AutoSize = true;
            this.labBack.BackColor = System.Drawing.Color.Transparent;
            this.labBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labBack.ForeColor = System.Drawing.Color.White;
            this.labBack.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labBack.Location = new System.Drawing.Point(45, 485);
            this.labBack.Name = "labBack";
            this.labBack.Size = new System.Drawing.Size(66, 29);
            this.labBack.TabIndex = 39;
            this.labBack.Text = "Back";
            this.labBack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labBack.Click += new System.EventHandler(this.btnback_Click);
            // 
            // labOK
            // 
            this.labOK.AutoSize = true;
            this.labOK.BackColor = System.Drawing.Color.Transparent;
            this.labOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labOK.ForeColor = System.Drawing.Color.White;
            this.labOK.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labOK.Location = new System.Drawing.Point(666, 488);
            this.labOK.Name = "labOK";
            this.labOK.Size = new System.Drawing.Size(48, 29);
            this.labOK.TabIndex = 38;
            this.labOK.Text = "OK";
            this.labOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labOK.Click += new System.EventHandler(this.button10_Click);
            // 
            // Unlocked
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(768, 526);
            this.Controls.Add(this.labBack);
            this.Controls.Add(this.labOK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.picBoxArrow);
            this.Controls.Add(this.lblremaintime);
            this.Controls.Add(this.lblLocknum);
            this.Controls.Add(this.btnback);
            this.Controls.Add(this.button10);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Unlocked";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Unlocked";
            this.Load += new System.EventHandler(this.Unlocked_Load);
            this.Click += new System.EventHandler(this.Unlocked_Click);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxArrow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button btnback;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timIdle;
        private System.Windows.Forms.Label lblremaintime;
        private System.Windows.Forms.Label lblLocknum;
        private System.Windows.Forms.PictureBox picBoxArrow;
        private System.Windows.Forms.Label labBack;
        private System.Windows.Forms.Label labOK;
    }
}