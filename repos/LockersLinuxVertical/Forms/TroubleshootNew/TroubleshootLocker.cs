﻿using System;
using System.Windows.Forms;
using System.Threading;

namespace Lockers
{
    public partial class TroubleshootLocker : Form
    {


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        int focusEntry = 0;
        TextBox t;
        string[] RentedLockers;
        string[] FreeLockers;

        public TroubleshootLocker()
        {
            InitializeComponent();
            this.txtAccessCode.Enter += new EventHandler(textBox_Enter); // enter event==get focus event 
        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            t = (TextBox)sender;
            //TextBox t = (TextBox)sender;
            //focusEntry=t.Name=="txtFrom" ? 0:1;
        }

        String AccessCode = "";
        private void bbtnNum_Click(object sender, EventArgs e)
        {
            if (t == null) return;

            Button b = (Button)sender;
            if (b.Text == "")
            {
                if (t.Text.Length > 1)
                {
                    if (t == txtAccessCode)
                        AccessCode = AccessCode.Substring(0, t.Text.Length - 1);

                    t.Text = t.Text.Substring(0, t.Text.Length - 1);
                }
                else
                {
                    t.Text = "";
                }

            }
            else
            {
                if (t.Text == "0")
                    t.Clear();

                if (t == txtAccessCode)
                {
                    t.Text += "*";
                    AccessCode += b.Text;
                }
                else
                    t.Text += b.Text;
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            LockerController frmController = new LockerController();
            frmController.BringToFront();
            frmController.TopMost = true;

            frmController.ShowDialog();
        }

        private void TroubleshootLocker_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            this.BackgroundImage = ConfigurationData.GetBackground("Blank");



            // Get the used lockers and populate the combo box
            LockerDatabase LockerData = new LockerDatabase();

            UpdateLockerComboBoxes();
        }

        void UpdateLockerComboBoxes()
        {
            RentedLockers = new string[1];
            FreeLockers = new string[1];

            RentedLockers[0] = "";
            FreeLockers[0] = "";

            if (!SelectSmall && !SelectMedium && !SelectLarge && !SelectXLarge)
            {
                string[] Rent = Globals.LockData.GetRentedLockers();
                string[] Free = Globals.LockData.GetAvailableLockers();

                if (Rent.Length > 0)
                {
                    int len = RentedLockers.Length;
                    Array.Resize<string>(ref RentedLockers, len + Rent.Length);
                    Array.Copy(Rent, 0, RentedLockers, len, Rent.Length);
                }

                if (Free.Length > 0)
                {
                    int len = FreeLockers.Length;
                    Array.Resize<string>(ref FreeLockers, len + Free.Length);
                    Array.Copy(Free, 0, FreeLockers, len, Free.Length);
                }
            }
            else
            {

                for (int count = 0; count < 4; count++)
                {
                    bool getdata = false;
                    string[] Rent = new string[0];
                    string[] Free = new string[0];

                    switch (count)
                    {
                        case 0:
                            if (SelectSmall)
                                getdata = true;
                            break;
                        case 1:
                            if (SelectMedium)
                                getdata = true;
                            break;
                        case 2:
                            if (SelectLarge)
                                getdata = true;
                            break;
                        case 3:
                            if (SelectXLarge)
                                getdata = true;
                            break;
                    }

                    if (getdata)
                    {
                        Rent = Globals.LockData.GetRentedLockers(count);
                        Free = Globals.LockData.GetAvailableLockers(count);
                    }

                    if (Rent.GetUpperBound(0) > 0)
                    {
                        int len = RentedLockers.Length;
                        Array.Resize<string>(ref RentedLockers, len + Rent.Length);
                        Array.Copy(Rent, 0, RentedLockers, len, Rent.Length);
                    }

                    if (Free.GetUpperBound(0) > 0)
                    {
                        int len = FreeLockers.Length;
                        Array.Resize<string>(ref FreeLockers, len + Free.Length);
                        Array.Copy(Free, 0, FreeLockers, len, Free.Length);
                    }
                }
            }

            int hired = RentedLockers.GetUpperBound(0);
            labHiredLockers.Text = hired.ToString();

            string[] Disabled = Globals.LockData.GetDisabledLockers();
            labDoorFaulty.Text = Disabled.Length.ToString();

            if (cbIncludeDisabled.Checked)
            {
                int len = FreeLockers.Length;
                Array.Resize<string>(ref FreeLockers, len + Disabled.Length);
                Array.Copy(Disabled, 0, FreeLockers, len, Disabled.Length);
            }

            cboxCurrentLocker.Items.Clear();
            cboxNewLocker.Items.Clear();

            if (RentedLockers.Length > 1)   // Empty element prepended
            {
                for (int count = 0; count < RentedLockers.Length; count++)
                    cboxCurrentLocker.Items.Add(RentedLockers[count]);
            }

            if (FreeLockers.Length > 1)     // Empty element prepended
            {
                for (int count = 0; count < FreeLockers.Length; count++)
                    cboxNewLocker.Items.Add(FreeLockers[count]);
            }
        }

        void Open_Locker(int lockerNumber)
        {
            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            // USE SQL to update the address
            // TODO : Fix this serial number conbversion to byte array
            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };

            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetRear.CopyTo(sum, packetFront.Length + packetAddress.Length);

            Globals.client.Send(sum);
        }

        void Close_Locker(int lockerNumber)
        {
            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetAddress.CopyTo(sum, packetAddress.Length);

            Globals.client.Send(sum);
        }

        private void bUnlock_Click(object sender, EventArgs e)
        {
            int LockerNumber = 0;
            try
            {
                if (cboxCurrentLocker.Text != "")
                    LockerNumber = Convert.ToInt16(cboxCurrentLocker.Text);
                else
                    LockerNumber = Convert.ToInt16(cboxNewLocker.Text);

                if (LockerNumber > 0)
                {
                    for (int lcount = 0; lcount < Properties.Settings.Default.LockerCommsRetries; lcount++)
                    {
                        Open_Locker(LockerNumber);
                        Thread.Sleep(Properties.Settings.Default.LockerCommsRetryPeriod);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        private void bLock_Click(object sender, EventArgs e)
        {
            int LockerNumber = 0;
            try
            {
                if (cboxCurrentLocker.Text != "")
                    LockerNumber = Convert.ToInt16(cboxCurrentLocker.Text);
                else
                    LockerNumber = Convert.ToInt16(cboxNewLocker.Text);

                if (LockerNumber > 0)
                {
                    for (int lcount = 0; lcount < Properties.Settings.Default.LockerCommsRetries; lcount++)
                    {
                        Close_Locker(LockerNumber);
                        Thread.Sleep(Properties.Settings.Default.LockerCommsRetryPeriod);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        private void bDeactivate_Click(object sender, EventArgs e)
        {
            int LockerNumber = 0;

            if (cboxCurrentLocker.Text != "")
                LockerNumber = Convert.ToInt16(cboxCurrentLocker.Text);
            else
                LockerNumber = Convert.ToInt16(cboxNewLocker.Text);

            Globals.LockData.DisableLocker(LockerNumber.ToString());
            cboxCurrentLocker.Text = "";
            cboxNewLocker.Text = "";
            txtAccessCode.Text = "";
            AccessCode = "";

            bLockActivate.BackColor = System.Drawing.Color.LightGray;
            bDeactivate.BackColor = System.Drawing.Color.LightPink;

            MessageBox.Show("Locker " + LockerNumber.ToString() + " deactivated", "Locker Deactivated", MessageBoxButtons.OK);
        }


        public int BoxFocus = 0;
        private void txtAccessCode_Enter(object sender, EventArgs e)
        {
            BoxFocus = 2;
        }

        private void bUpdateCode_Click(object sender, EventArgs e)
        {
            if (cboxCurrentLocker.Text != "")
            {
                if ((AccessCode.Length >= Properties.Settings.Default.MinUserPinLength) && (AccessCode.Length <= Properties.Settings.Default.MaxUserPinLen))
                {
                    // Update the access code
                    Globals.LockData.UpdatePINonLocker(cboxCurrentLocker.Text, AccessCode);
                    cboxCurrentLocker.Text = "";
                    cboxNewLocker.Text = "";
                    txtAccessCode.Text = "";
                    AccessCode = "";

                    MessageBox.Show("Locker " + cboxNewLocker.Text + " code Updated", "Code Updated", MessageBoxButtons.OK);
                }
            }
        }

        private void bSwapLocker_Click(object sender, EventArgs e)
        {
            if (cboxNewLocker.Text != "")
            {
                Globals.LockData.SwapLocker(cboxCurrentLocker.Text, cboxNewLocker.Text);
                cboxCurrentLocker.Text = "";
                cboxNewLocker.Text = "";
                txtAccessCode.Text = "";
                AccessCode = "";

                UpdateLockerComboBoxes();
            }

            MessageBox.Show("Locker Swapped from "+cboxCurrentLocker.Text + " to " + cboxNewLocker.Text, "Locker Swapped", MessageBoxButtons.OK);
        }

        private void bFreeHire_Click(object sender, EventArgs e)
        {
            // Hire a locker for no money. 
            if (cboxNewLocker.Text != "")
            {
                if ((AccessCode != "") && (AccessCode.Length >= 4) && (AccessCode.Length <= 10))
                {
                    Globals.LockData.PINCode = AccessCode;
                    Globals.LockData.PINIcon = "1";
                    Globals.LockData.LockNumber = Convert.ToInt32(cboxNewLocker.Text);
                    Globals.LockData.UPrice = Convert.ToDecimal(0.00);

                    Globals.LockData.Save_Rent();

                    cboxCurrentLocker.Text = "";
                    cboxNewLocker.Text = "";
                    txtAccessCode.Text = "";

                    UpdateLockerComboBoxes();

                    MessageBox.Show("Locker " + cboxNewLocker.Text + " Hired ", "Locker Hired", MessageBoxButtons.OK);
                }
                cboxCurrentLocker.Text = "";
            }
        }

        private void cBoxFreeHireSmall_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }

        private void cBoxFreeHireMedium_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }

        private void cBoxFreeHireLarge_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }

        private void cBoxFreeHireXLarge_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }


        bool SelectSmall = false;
        bool SelectMedium = false;
        bool SelectLarge = false;
        bool SelectXLarge = false;

        private void bSmall_Click(object sender, EventArgs e)
        {
            if (!SelectSmall)
            {
                bSmall.BackColor = System.Drawing.Color.LightGreen;
                SelectSmall = true;
            }
            else
            {
                bSmall.BackColor = System.Drawing.Color.LightGray;
                SelectSmall = false;
            }

            UpdateLockerComboBoxes();

        }

        private void bMedium_Click(object sender, EventArgs e)
        {
            if (!SelectMedium)
            {
                bMedium.BackColor = System.Drawing.Color.LightGreen;
                SelectMedium = true;
            }
            else
            {
                bMedium.BackColor = System.Drawing.Color.LightGray;
                SelectMedium = false;
            }

            UpdateLockerComboBoxes();
        }

        private void bLarge_Click(object sender, EventArgs e)
        {
            if (!SelectLarge)
            {
                bLarge.BackColor = System.Drawing.Color.LightGreen;
                SelectLarge = true;
            }
            else
            {
                bLarge.BackColor = System.Drawing.Color.LightGray;
                SelectLarge = false;
            }

            UpdateLockerComboBoxes();
        }

        private void bXLarge_Click(object sender, EventArgs e)
        {
            if (!SelectXLarge)
            {
                bXLarge.BackColor = System.Drawing.Color.LightGreen;
                SelectXLarge = true;
            }
            else
            {
                bXLarge.BackColor = System.Drawing.Color.LightGray;
                SelectXLarge = false;
            }

            UpdateLockerComboBoxes();
        }

        private void bClearAllHires_Click(object sender, EventArgs e)
        {
            Globals.LockData.ClearAllHires();
            UpdateLockerComboBoxes();
        }

        private void cboxCurrentLocker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxCurrentLocker.Text != "")
            {
                if(Globals.LockData.IsLockerActive(cboxCurrentLocker.Text))
                {
                    bLockActivate.BackColor = System.Drawing.Color.LightGreen;
                    bDeactivate.BackColor = System.Drawing.Color.LightGray;
                }
                else
                {
                    bLockActivate.BackColor = System.Drawing.Color.LightGray;
                    bDeactivate.BackColor = System.Drawing.Color.LightPink;
                }
            }
        }

        private void cboxNewLocker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxNewLocker.Text != "")
            {
                if (Globals.LockData.IsLockerActive(cboxNewLocker.Text))
                {
                    bLockActivate.BackColor = System.Drawing.Color.LightGreen;
                    bDeactivate.BackColor = System.Drawing.Color.LightGray;
                }
                else
                {
                    bLockActivate.BackColor = System.Drawing.Color.LightGray;
                    bDeactivate.BackColor = System.Drawing.Color.LightPink;
                }
            }

        }

        private void bLockActivate_Click(object sender, EventArgs e)
        {
            int LockerNumber = 0;

            if (cboxCurrentLocker.Text != "")
                LockerNumber = Convert.ToInt16(cboxCurrentLocker.Text);
            else
                LockerNumber = Convert.ToInt16(cboxNewLocker.Text);

            Globals.LockData.EnableLocker(LockerNumber.ToString());
            cboxCurrentLocker.Text = "";
            cboxNewLocker.Text = "";
            txtAccessCode.Text = "";
            AccessCode = "";

            bLockActivate.BackColor = System.Drawing.Color.LightGreen;
            bDeactivate.BackColor = System.Drawing.Color.LightGray;

            MessageBox.Show("Locker " + LockerNumber.ToString() + " activated", "Locker Activated", MessageBoxButtons.OK);
        }

        private void cbIncludeDisabled_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }

        private void bTaskBarToggle_Click(object sender, EventArgs e)
        {
            SendKeys.Send("%{t}");
        }

        private void bEndHire_Click(object sender, EventArgs e)
        {
            Globals.LockData.EndHire(Convert.ToInt16(cboxCurrentLocker.Text));
            UpdateLockerComboBoxes();
        }

        private void cbFreeLockers_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }

        private void cbReserved_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }

        private void bReserveLocker_Click(object sender, EventArgs e)
        {
            // Reserve a locker for future use.
            Globals.LockData.ReserveHire(Convert.ToInt16(cboxNewLocker.Text));
            UpdateLockerComboBoxes();
        }
    }
}
