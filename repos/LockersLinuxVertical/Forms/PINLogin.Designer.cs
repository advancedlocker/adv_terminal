﻿namespace Lockers
{
    partial class PINLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PINLogin));
            this.button12 = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.bNumber9 = new System.Windows.Forms.Button();
            this.bNumber8 = new System.Windows.Forms.Button();
            this.bNumber7 = new System.Windows.Forms.Button();
            this.bNumber4 = new System.Windows.Forms.Button();
            this.bNumber5 = new System.Windows.Forms.Button();
            this.bNumber6 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bNumber3 = new System.Windows.Forms.Button();
            this.bNumber2 = new System.Windows.Forms.Button();
            this.bNumber1 = new System.Windows.Forms.Button();
            this.lblmsg = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.bNumber0 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblerror = new System.Windows.Forms.Label();
            this.labCodeMinSize = new System.Windows.Forms.Label();
            this.labCodeMaxSize = new System.Windows.Forms.Label();
            this.labHeading = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Transparent;
            this.button12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button12.BackgroundImage")));
            this.button12.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Location = new System.Drawing.Point(35, 642);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(79, 111);
            this.button12.TabIndex = 2;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            this.bOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bOK.BackgroundImage")));
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bOK.Location = new System.Drawing.Point(907, 642);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(81, 112);
            this.bOK.TabIndex = 3;
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Click += new System.EventHandler(this.button10_Click);
            // 
            // bNumber9
            // 
            this.bNumber9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNumber9.Location = new System.Drawing.Point(576, 377);
            this.bNumber9.Name = "bNumber9";
            this.bNumber9.Size = new System.Drawing.Size(74, 75);
            this.bNumber9.TabIndex = 4;
            this.bNumber9.Text = "9";
            this.bNumber9.UseVisualStyleBackColor = true;
            this.bNumber9.Click += new System.EventHandler(this.button1_Click);
            // 
            // bNumber8
            // 
            this.bNumber8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNumber8.Location = new System.Drawing.Point(475, 377);
            this.bNumber8.Name = "bNumber8";
            this.bNumber8.Size = new System.Drawing.Size(74, 75);
            this.bNumber8.TabIndex = 5;
            this.bNumber8.Text = "8";
            this.bNumber8.UseVisualStyleBackColor = true;
            this.bNumber8.Click += new System.EventHandler(this.button1_Click);
            // 
            // bNumber7
            // 
            this.bNumber7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNumber7.Location = new System.Drawing.Point(374, 377);
            this.bNumber7.Name = "bNumber7";
            this.bNumber7.Size = new System.Drawing.Size(74, 75);
            this.bNumber7.TabIndex = 6;
            this.bNumber7.Text = "7";
            this.bNumber7.UseVisualStyleBackColor = true;
            this.bNumber7.Click += new System.EventHandler(this.button1_Click);
            // 
            // bNumber4
            // 
            this.bNumber4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNumber4.Location = new System.Drawing.Point(374, 278);
            this.bNumber4.Name = "bNumber4";
            this.bNumber4.Size = new System.Drawing.Size(74, 75);
            this.bNumber4.TabIndex = 7;
            this.bNumber4.Text = "4";
            this.bNumber4.UseVisualStyleBackColor = true;
            this.bNumber4.Click += new System.EventHandler(this.button1_Click);
            // 
            // bNumber5
            // 
            this.bNumber5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNumber5.Location = new System.Drawing.Point(475, 278);
            this.bNumber5.Name = "bNumber5";
            this.bNumber5.Size = new System.Drawing.Size(74, 75);
            this.bNumber5.TabIndex = 8;
            this.bNumber5.Text = "5";
            this.bNumber5.UseVisualStyleBackColor = true;
            this.bNumber5.Click += new System.EventHandler(this.button1_Click);
            // 
            // bNumber6
            // 
            this.bNumber6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNumber6.Location = new System.Drawing.Point(576, 278);
            this.bNumber6.Name = "bNumber6";
            this.bNumber6.Size = new System.Drawing.Size(74, 75);
            this.bNumber6.TabIndex = 9;
            this.bNumber6.Text = "6";
            this.bNumber6.UseVisualStyleBackColor = true;
            this.bNumber6.Click += new System.EventHandler(this.button1_Click);
            // 
            // bClear
            // 
            this.bClear.BackColor = System.Drawing.Color.Transparent;
            this.bClear.FlatAppearance.BorderSize = 0;
            this.bClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bClear.Location = new System.Drawing.Point(576, 478);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(74, 75);
            this.bClear.TabIndex = 10;
            this.bClear.Tag = "0";
            this.bClear.UseVisualStyleBackColor = false;
            this.bClear.Click += new System.EventHandler(this.button1_Click);
            // 
            // bNumber3
            // 
            this.bNumber3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNumber3.Location = new System.Drawing.Point(576, 176);
            this.bNumber3.Name = "bNumber3";
            this.bNumber3.Size = new System.Drawing.Size(74, 75);
            this.bNumber3.TabIndex = 11;
            this.bNumber3.Text = "3";
            this.bNumber3.UseVisualStyleBackColor = true;
            this.bNumber3.Click += new System.EventHandler(this.button1_Click);
            // 
            // bNumber2
            // 
            this.bNumber2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNumber2.Location = new System.Drawing.Point(475, 176);
            this.bNumber2.Name = "bNumber2";
            this.bNumber2.Size = new System.Drawing.Size(74, 75);
            this.bNumber2.TabIndex = 12;
            this.bNumber2.Text = "2";
            this.bNumber2.UseVisualStyleBackColor = true;
            this.bNumber2.Click += new System.EventHandler(this.button1_Click);
            // 
            // bNumber1
            // 
            this.bNumber1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNumber1.Location = new System.Drawing.Point(374, 176);
            this.bNumber1.Name = "bNumber1";
            this.bNumber1.Size = new System.Drawing.Size(74, 75);
            this.bNumber1.TabIndex = 13;
            this.bNumber1.Text = "1";
            this.bNumber1.UseVisualStyleBackColor = true;
            this.bNumber1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblmsg
            // 
            this.lblmsg.AutoSize = true;
            this.lblmsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmsg.ForeColor = System.Drawing.Color.Red;
            this.lblmsg.Location = new System.Drawing.Point(338, 553);
            this.lblmsg.Name = "lblmsg";
            this.lblmsg.Size = new System.Drawing.Size(0, 20);
            this.lblmsg.TabIndex = 18;
            this.lblmsg.Visible = false;
            // 
            // lbl1
            // 
            this.lbl1.BackColor = System.Drawing.Color.Transparent;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.ForeColor = System.Drawing.Color.Black;
            this.lbl1.Location = new System.Drawing.Point(724, 480);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(218, 54);
            this.lbl1.TabIndex = 19;
            this.lbl1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // bNumber0
            // 
            this.bNumber0.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bNumber0.Location = new System.Drawing.Point(475, 479);
            this.bNumber0.Name = "bNumber0";
            this.bNumber0.Size = new System.Drawing.Size(74, 75);
            this.bNumber0.TabIndex = 5;
            this.bNumber0.Text = "0";
            this.bNumber0.UseVisualStyleBackColor = true;
            this.bNumber0.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.labHeading);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 151);
            this.panel1.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "TIME";
            // 
            // button14
            // 
            this.button14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button14.BackgroundImage")));
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button14.Location = new System.Drawing.Point(928, 24);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(79, 81);
            this.button14.TabIndex = 2;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(7, 47);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(225, 55);
            this.lbltime.TabIndex = 1;
            this.lbltime.Text = "01:22 pm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(940, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 29);
            this.label2.TabIndex = 20;
            this.label2.Text = "help";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblerror
            // 
            this.lblerror.AutoSize = true;
            this.lblerror.BackColor = System.Drawing.Color.Transparent;
            this.lblerror.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblerror.ForeColor = System.Drawing.Color.SeaGreen;
            this.lblerror.Location = new System.Drawing.Point(728, 523);
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(184, 29);
            this.lblerror.TabIndex = 22;
            this.lblerror.Text = "Please try again";
            this.lblerror.Visible = false;
            // 
            // labCodeMinSize
            // 
            this.labCodeMinSize.AutoSize = true;
            this.labCodeMinSize.BackColor = System.Drawing.Color.Transparent;
            this.labCodeMinSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labCodeMinSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.labCodeMinSize.ForeColor = System.Drawing.Color.White;
            this.labCodeMinSize.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labCodeMinSize.Location = new System.Drawing.Point(176, 456);
            this.labCodeMinSize.Name = "labCodeMinSize";
            this.labCodeMinSize.Size = new System.Drawing.Size(42, 46);
            this.labCodeMinSize.TabIndex = 23;
            this.labCodeMinSize.Text = "1";
            // 
            // labCodeMaxSize
            // 
            this.labCodeMaxSize.AutoSize = true;
            this.labCodeMaxSize.BackColor = System.Drawing.Color.Transparent;
            this.labCodeMaxSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labCodeMaxSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.labCodeMaxSize.ForeColor = System.Drawing.Color.White;
            this.labCodeMaxSize.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labCodeMaxSize.Location = new System.Drawing.Point(250, 456);
            this.labCodeMaxSize.Name = "labCodeMaxSize";
            this.labCodeMaxSize.Size = new System.Drawing.Size(42, 46);
            this.labCodeMaxSize.TabIndex = 24;
            this.labCodeMaxSize.Text = "1";
            // 
            // labHeading
            // 
            this.labHeading.AutoSize = true;
            this.labHeading.BackColor = System.Drawing.Color.Transparent;
            this.labHeading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labHeading.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.labHeading.ForeColor = System.Drawing.Color.White;
            this.labHeading.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labHeading.Location = new System.Drawing.Point(301, 47);
            this.labHeading.Name = "labHeading";
            this.labHeading.Size = new System.Drawing.Size(376, 46);
            this.labHeading.TabIndex = 21;
            this.labHeading.Text = "Choose a PIN Code";
            // 
            // PINLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.labCodeMaxSize);
            this.Controls.Add(this.labCodeMinSize);
            this.Controls.Add(this.lblerror);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.lblmsg);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bNumber9);
            this.Controls.Add(this.bNumber0);
            this.Controls.Add(this.bNumber8);
            this.Controls.Add(this.bNumber7);
            this.Controls.Add(this.bNumber4);
            this.Controls.Add(this.bNumber5);
            this.Controls.Add(this.bNumber6);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bNumber3);
            this.Controls.Add(this.bNumber2);
            this.Controls.Add(this.bNumber1);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "PINLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PINLogin";
            this.Load += new System.EventHandler(this.PINLogin_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bNumber9;
        private System.Windows.Forms.Button bNumber8;
        private System.Windows.Forms.Button bNumber7;
        private System.Windows.Forms.Button bNumber4;
        private System.Windows.Forms.Button bNumber5;
        private System.Windows.Forms.Button bNumber6;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bNumber3;
        private System.Windows.Forms.Button bNumber2;
        private System.Windows.Forms.Button bNumber1;
        private System.Windows.Forms.Label lblmsg;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Button bNumber0;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblerror;
        private System.Windows.Forms.Label labHeading;
        private System.Windows.Forms.Label labCodeMinSize;
        private System.Windows.Forms.Label labCodeMaxSize;
    }
}