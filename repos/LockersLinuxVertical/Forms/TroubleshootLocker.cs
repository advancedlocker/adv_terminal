﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Drawing;
using System.Diagnostics;

namespace Lockers
{
    public partial class TroubleshootLocker : Form
    {
        //        private LockerDatabase LockData;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        int focusEntry = 0;
        TextBox t;
        string[] RentedLockers;
        string[] FreeLockers;

        public TroubleshootLocker()
        {
            InitializeComponent();
            this.tboxAccessCode.Enter += new EventHandler(textBox_Enter); // enter event==get focus event 
//            LockData = new LockerDatabase();
        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            t = (TextBox)sender;
            //TextBox t = (TextBox)sender;
            //focusEntry=t.Name=="txtFrom" ? 0:1;
        }

        String AccessCode = "";
        private void bbtnNum_Click(object sender, EventArgs e)
        {
            if (t == null) return;

            Button b = (Button)sender;
            if (b.Text == "")
            {
                if (t.Text.Length > 1)
                {
                    if (t == tboxAccessCode)
                        AccessCode = AccessCode.Substring(0, t.Text.Length - 1);

//                    if (t == tboxFree)
//                        cboxCurrentLocker.Text += AccessCode.Substring(0, t.Text.Length - 1);

                    t.Text = t.Text.Substring(0, t.Text.Length - 1);
                }
                else
                {
                    t.Text = "";
                }

            }
            else
            {
                if (t.Text == "0")
                    t.Clear();

                if (t == tboxAccessCode)
                {
                    t.Text += "*";
                    AccessCode += b.Text;
                }
                else
                    t.Text += b.Text;
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            LockerController frmController = new LockerController();
            frmController.BringToFront();
            frmController.TopMost = true;

            frmController.ShowDialog();
        }

        private void TroubleshootLocker_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
            /*
                        Size WinSize = Globals.GetWindowSize();
                        this.Size = WinSize;
                        this.CenterToScreen();
            */

            label11.ForeColor = Properties.Settings.Default.TextColor;
            labDoorFaulty.ForeColor = Properties.Settings.Default.TextColor;
            labUsed.ForeColor = Properties.Settings.Default.TextColor;
            label2.ForeColor = Properties.Settings.Default.TextColor;
            label3.ForeColor = Properties.Settings.Default.TextColor;
            labFree.ForeColor = Properties.Settings.Default.TextColor;
            label5.ForeColor = Properties.Settings.Default.TextColor;
            label7.ForeColor = Properties.Settings.Default.TextColor;
            label9.ForeColor = Properties.Settings.Default.TextColor;
            labHiredLockers.ForeColor = Properties.Settings.Default.TextColor;
            labTotalLockersA.ForeColor = Properties.Settings.Default.TextColor;
            labTotalLockersB.ForeColor = Properties.Settings.Default.TextColor;

/*
            Globals.UpdateLabelDimensions(ref labUsed);
            Globals.UpdateLabelDimensions(ref labDoorFaulty);
            Globals.UpdateLabelDimensions(ref label11);
            Globals.UpdateLabelDimensions(ref label2);
            Globals.UpdateLabelDimensions(ref label3);
            Globals.UpdateLabelDimensions(ref labFree);
            Globals.UpdateLabelDimensions(ref label5);
            Globals.UpdateLabelDimensions(ref label7);
            Globals.UpdateLabelDimensions(ref label9);
            Globals.UpdateLabelDimensions(ref labHiredLockers);
            Globals.UpdateLabelDimensions(ref labTotalLockersA);
            Globals.UpdateLabelDimensions(ref labTotalLockersB);

            Globals.UpdatePanelDimensions(ref panel1);

            Globals.UpdateButtonDimensions(ref bClearAllHires);
            Globals.UpdateButtonDimensions(ref bDeactivate);
            Globals.UpdateButtonDimensions(ref bLockActivate);
            Globals.UpdateButtonDimensions(ref bSmall);
            Globals.UpdateButtonDimensions(ref bMedium);
            Globals.UpdateButtonDimensions(ref bLarge);
            Globals.UpdateButtonDimensions(ref bXLarge);
            Globals.UpdateButtonDimensions(ref bLock);
            Globals.UpdateButtonDimensions(ref bUnlock);
            Globals.UpdateButtonDimensions(ref bSwapLocker);
            Globals.UpdateButtonDimensions(ref bUpdateCode);
            Globals.UpdateButtonDimensions(ref bFreeHire);
            Globals.UpdateButtonDimensions(ref bClearAllHires);
            Globals.UpdateButtonDimensions(ref bSwapLocker);
            Globals.UpdateButtonDimensions(ref button1);
            Globals.UpdateButtonDimensions(ref button2);
            Globals.UpdateButtonDimensions(ref button3);
            Globals.UpdateButtonDimensions(ref button4);
            Globals.UpdateButtonDimensions(ref button5);
            Globals.UpdateButtonDimensions(ref button6);
            Globals.UpdateButtonDimensions(ref button7);
            Globals.UpdateButtonDimensions(ref button8);
            Globals.UpdateButtonDimensions(ref button9);
            Globals.UpdateButtonDimensions(ref button11);
            Globals.UpdateButtonDimensions(ref button13);
            Globals.UpdateButtonDimensions(ref button16);
            Globals.UpdateButtonDimensions(ref button18);
            Globals.UpdateButtonDimensions(ref bTaskBarToggle);

            Globals.UpdateTextBoxDimensions(ref tboxFree);
            Globals.UpdateTextBoxDimensions(ref tboxUsed);
            Globals.UpdateTextBoxDimensions(ref tboxAccessCode);

            Globals.UpdateCheckBoxDimensions(ref cbIncludeDisabled);

            Globals.UpdateComboDimensions(ref cboxCurrentLocker);
            Globals.UpdateComboDimensions(ref cboxNewLocker);

            this.Update();

            Size WinSize = Globals.GetWindowSize();
            this.Size = WinSize;
            this.CenterToScreen();
*/

            if (Properties.Settings.Default.ReleaseType == "Linux")
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Properties.Settings.Default.DefaultLanguage + "\\Blank_1182x789.tiff");
            else
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Properties.Settings.Default.DefaultLanguage + "\\Blank_1182x789.tiff");

            // Get the used lockers and populate the combo box
            LockerDatabase LockerData = new LockerDatabase();

            int num = LockerData.GetNumLockers();
            labTotalLockersA.Text = num.ToString();
            labTotalLockersB.Text = num.ToString();

            UpdateLockerComboBoxes();
        }

        void UpdateLockerComboBoxes()
        {
            RentedLockers = new string[1];
            FreeLockers = new string[1];

            RentedLockers[0] = "";
            FreeLockers[0] = "";

            if (!SelectSmall && !SelectMedium && !SelectLarge && !SelectXLarge)
            {
                string[] Rent = Globals.LockData.GetRentedLockers();
                string[] Free = Globals.LockData.GetAvailableLockers();

                if (Rent.Length > 0)
                {
                    int len = RentedLockers.Length;
                    Array.Resize<string>(ref RentedLockers, len + Rent.Length);
                    Array.Copy(Rent, 0, RentedLockers, len, Rent.Length);
                }

                if (Free.Length > 0)
                {
                    int len = FreeLockers.Length;
                    Array.Resize<string>(ref FreeLockers, len + Free.Length);
                    Array.Copy(Free, 0, FreeLockers, len, Free.Length);
                }
            }
            else
            {

                for (int count = 0; count < 4; count++)
                {
                    bool getdata = false;
                    string[] Rent = new string[0];
                    string[] Free = new string[0];

                    switch (count)
                    {
                        case 0:
                            if (SelectSmall)
                                getdata = true;
                            break;
                        case 1:
                            if (SelectMedium)
                                getdata = true;
                            break;
                        case 2:
                            if (SelectLarge)
                                getdata = true;
                            break;
                        case 3:
                            if (SelectXLarge)
                                getdata = true;
                            break;
                    }

                    if (getdata)
                    {
                        Rent = Globals.LockData.GetRentedLockers((eLockerSize)count);
                        Free = Globals.LockData.GetAvailableLockers((eLockerSize)count);
                    }

                    if (Rent.GetUpperBound(0) > 0)
                    {
                        int len = RentedLockers.Length;
                        Array.Resize<string>(ref RentedLockers, len + Rent.Length);
                        Array.Copy(Rent, 0, RentedLockers, len, Rent.Length);
                    }

                    if (Free.GetUpperBound(0) > 0)
                    {
                        int len = FreeLockers.Length;
                        Array.Resize<string>(ref FreeLockers, len + Free.Length);
                        Array.Copy(Free, 0, FreeLockers, len, Free.Length);
                    }
                }
            }

            int hired = RentedLockers.GetUpperBound(0);
            labHiredLockers.Text = hired.ToString();

            string[] Disabled = Globals.LockData.GetDisabledLockers();
            labDoorFaulty.Text = Disabled.Length.ToString();

            if (cbIncludeDisabled.Checked)
            {
                int len = FreeLockers.Length;
                Array.Resize<string>(ref FreeLockers, len + Disabled.Length);
                Array.Copy(Disabled, 0, FreeLockers, len, Disabled.Length);
            }

            cboxCurrentLocker.Items.Clear();
            cboxNewLocker.Items.Clear();

            if (RentedLockers.Length > 1)   // Empty element prepended
            {
                for (int count = 0; count < RentedLockers.Length; count++)
                    cboxCurrentLocker.Items.Add(RentedLockers[count]);
            }

            if (FreeLockers.Length > 1)     // Empty element prepended
            {
                for (int count = 0; count < FreeLockers.Length; count++)
                    cboxNewLocker.Items.Add(FreeLockers[count]);
            }
        }

        void AOpen_Locker(int lockerNumber)
        {
            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            // USE SQL to update the address
            // TODO : Fix this serial number conbversion to byte array
            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };

            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetRear.CopyTo(sum, packetFront.Length + packetAddress.Length);

            Globals.LockerClient.Send(sum);
        }

        void AClose_Locker(int lockerNumber)
        {
            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetAddress.CopyTo(sum, packetAddress.Length);

            Globals.LockerClient.Send(sum);
        }

        private void bUnlock_Click(object sender, EventArgs e)
        {
            int LockerNumber = 0;
            try
            {
                if (cboxCurrentLocker.Visible == true)
                {
                    if (cboxCurrentLocker.Text != "")
                        LockerNumber = Convert.ToInt16(cboxCurrentLocker.Text);
                }
                else
                {
                    if (tboxUsed.Text != "")
                        LockerNumber = Convert.ToInt16(tboxUsed.Text);
                }

                if (LockerNumber == 0)
                {
                    if (cboxNewLocker.Visible == true)
                    {
                        if (cboxNewLocker.Text != "")
                            LockerNumber = Convert.ToInt16(cboxNewLocker.Text);
                    }
                    else
                    {
                        if (tboxFree.Text != "")
                            LockerNumber = Convert.ToInt16(tboxFree.Text);
                    }
                }


                if (LockerNumber > 0)
                {
                    for (int lcount = 0; lcount < Properties.Settings.Default.LockerCommsRetries; lcount++)
                    {
                        Globals.Open_Locker(LockerNumber);
                        Thread.Sleep(Properties.Settings.Default.LockerCommsRetryPeriod);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        private void bLock_Click(object sender, EventArgs e)
        {
            int LockerNumber = 0;
            try
            {
                if (cboxCurrentLocker.Visible == true)
                {
                    if (cboxCurrentLocker.Text != "")
                        LockerNumber = Convert.ToInt16(cboxCurrentLocker.Text);
                }
                else
                {
                    if (tboxUsed.Text != "")
                        LockerNumber = Convert.ToInt16(tboxUsed.Text);
                }

                if (LockerNumber == 0)
                {
                    if (cboxNewLocker.Visible == true)
                    {
                        if (cboxNewLocker.Text != "")
                            LockerNumber = Convert.ToInt16(cboxNewLocker.Text);
                    }
                    else
                    {
                        if (tboxFree.Text != "")
                            LockerNumber = Convert.ToInt16(tboxFree.Text);
                    }
                }

                if (LockerNumber > 0)
                {
                    for (int lcount = 0; lcount < Properties.Settings.Default.LockerCommsRetries; lcount++)
                    {
                        Globals.Close_Locker(LockerNumber);
                        Thread.Sleep(Properties.Settings.Default.LockerCommsRetryPeriod);
                    }
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        private void bDeactivate_Click(object sender, EventArgs e)
        {
            int LockerNumber = 0;

            if (cboxCurrentLocker.Visible == true)
            {
                if (cboxCurrentLocker.Text != "")
                    LockerNumber = Convert.ToInt16(cboxCurrentLocker.Text);
            }
            else
            {
                if (tboxUsed.Text != "")
                    LockerNumber = Convert.ToInt16(tboxUsed.Text);
            }

            if (LockerNumber == 0)
            {
                if (cboxNewLocker.Visible == true)
                {
                    if (cboxNewLocker.Text != "")
                        LockerNumber = Convert.ToInt16(cboxNewLocker.Text);
                }
                else
                {
                    if (tboxFree.Text != "")
                        LockerNumber = Convert.ToInt16(tboxFree.Text);
                }
            }

            if (LockerNumber != 0)
            {
                Globals.LockData.DisableLocker(LockerNumber.ToString());

                bLockActivate.BackColor = System.Drawing.Color.LightGray;
                bDeactivate.BackColor = System.Drawing.Color.LightPink;

                MessageBox.Show("Locker " + LockerNumber.ToString() + " deactivated", "Locker Deactivated", MessageBoxButtons.OK);
            }

            cboxCurrentLocker.Text = "";
            cboxNewLocker.Text = "";
            tboxAccessCode.Text = "";
            AccessCode = "";
        }


        public int BoxFocus = 0;
        private void txtAccessCode_Enter(object sender, EventArgs e)
        {
            BoxFocus = 2;
        }

        private void bUpdateCode_Click(object sender, EventArgs e)
        {
            if (cboxCurrentLocker.Visible)
            {
                if (cboxCurrentLocker.Text != "")
                {
                    if ((AccessCode.Length >= Properties.Settings.Default.MinUserPinLength) && (AccessCode.Length <= Properties.Settings.Default.MaxUserPinLen))
                    {
                        // Update the access code
                        Globals.LockData.UpdatePINonLocker(cboxCurrentLocker.Text, AccessCode);
                        cboxCurrentLocker.Text = "";
                        cboxNewLocker.Text = "";
                        tboxAccessCode.Text = "";
                        AccessCode = "";

                        MessageBox.Show("Locker " + cboxNewLocker.Text + " code Updated", "Code Updated", MessageBoxButtons.OK);
                    }
                }
            }
            else
            {
                if (tboxUsed.Text != "")
                {
                    if ((AccessCode.Length >= Properties.Settings.Default.MinUserPinLength) && (AccessCode.Length <= Properties.Settings.Default.MaxUserPinLen))
                    {
                        // Update the access code
                        Globals.LockData.UpdatePINonLocker(tboxUsed.Text, AccessCode);

                        tboxUsed.Text = "";
                        cboxCurrentLocker.Text = "";
                        cboxNewLocker.Text = "";
                        tboxAccessCode.Text = "";
                        AccessCode = "";
                        MessageBox.Show("Locker " + tboxUsed.Text + " code Updated", "Code Updated", MessageBoxButtons.OK);
                    }
                }
            }
        }

        private void bSwapLocker_Click(object sender, EventArgs e)
        {
            if (cboxNewLocker.Visible == true)
            {
                if (cboxNewLocker.Text != "")
                {
                    if (cboxCurrentLocker.Visible == true)
                    {
                        Globals.LockData.SwapLocker(cboxCurrentLocker.Text, cboxNewLocker.Text);
                        cboxCurrentLocker.Text = "";
                    }
                    else
                        Globals.LockData.SwapLocker(tboxUsed.Text, cboxNewLocker.Text);
                }
            }
            else
            {
                if (tboxFree.Text != "")
                {
                    if (cboxCurrentLocker.Visible == true)
                    {
                        Globals.LockData.SwapLocker(cboxCurrentLocker.Text, tboxFree.Text);
                        cboxCurrentLocker.Text = "";
                    }
                    else
                        Globals.LockData.SwapLocker(tboxUsed.Text, tboxFree.Text);
                }
            }

            tboxUsed.Text = "";
            tboxFree.Text = "";
            cboxCurrentLocker.Text = "";
            cboxNewLocker.Text = "";
            tboxAccessCode.Text = "";
            AccessCode = "";

            UpdateLockerComboBoxes();

            MessageBox.Show("Locker Swapped from "+cboxCurrentLocker.Text + " to " + cboxNewLocker.Text, "Locker Swapped", MessageBoxButtons.OK);
        }

        private void bFreeHire_Click(object sender, EventArgs e)
        {
            if (cboxNewLocker.Visible == true)
            {
                // Hire a locker for no money. 
                if (cboxNewLocker.Text != "")
                {
                    if ((AccessCode != "") && (AccessCode.Length >= 4) && (AccessCode.Length <= 10))
                    {
                        Globals.LockData.PINCode_Set(AccessCode);
                        Globals.LockData.PINIcon = "1";
                        Globals.LockData.LockNumber_Set(Convert.ToInt32(cboxNewLocker.Text));
                        Globals.LockData.UPrice = Convert.ToDecimal(0.00);

                        Globals.Open_Locker(Globals.LockData.LockNumber);
                        Globals.LockData.Save_Rent();

                        MessageBox.Show("Locker " + cboxNewLocker.Text + " Hired ", "Locker Hired", MessageBoxButtons.OK);
                    }
                }
            }
            else
            {
                if (tboxFree.Text != "")
                {
                    if ((AccessCode != "") && (AccessCode.Length >= 4) && (AccessCode.Length <= 10))
                    {
                        Globals.LockData.PINCode_Set(AccessCode);
                        Globals.LockData.PINIcon = "1";
                        Globals.LockData.LockNumber_Set(Convert.ToInt32(tboxFree.Text));
                        Globals.LockData.UPrice = Convert.ToDecimal(0.00);

                        Globals.Open_Locker(Globals.LockData.LockNumber);
                        Globals.LockData.Save_Rent();

                        MessageBox.Show("Locker " + tboxFree.Text + " Hired ", "Locker Hired", MessageBoxButtons.OK);
                    }
                }
            }

            cboxCurrentLocker.Text = "";
            tboxUsed.Text = "";
            tboxFree.Text = "";
            cboxNewLocker.Text = "";
            tboxAccessCode.Text = "";

            UpdateLockerComboBoxes();
        }

        private void cBoxFreeHireSmall_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }

        private void cBoxFreeHireMedium_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }

        private void cBoxFreeHireLarge_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }

        private void cBoxFreeHireXLarge_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }


        bool SelectSmall = false;
        bool SelectMedium = false;
        bool SelectLarge = false;
        bool SelectXLarge = false;

        private void bSmall_Click(object sender, EventArgs e)
        {
            if (!SelectSmall)
            {
                bSmall.BackColor = System.Drawing.Color.LightGreen;
                SelectSmall = true;
            }
            else
            {
                bSmall.BackColor = System.Drawing.Color.LightGray;
                SelectSmall = false;
            }

            UpdateLockerComboBoxes();

        }

        private void bMedium_Click(object sender, EventArgs e)
        {
            if (!SelectMedium)
            {
                bMedium.BackColor = System.Drawing.Color.LightGreen;
                SelectMedium = true;
            }
            else
            {
                bMedium.BackColor = System.Drawing.Color.LightGray;
                SelectMedium = false;
            }

            UpdateLockerComboBoxes();
        }

        private void bLarge_Click(object sender, EventArgs e)
        {
            if (!SelectLarge)
            {
                bLarge.BackColor = System.Drawing.Color.LightGreen;
                SelectLarge = true;
            }
            else
            {
                bLarge.BackColor = System.Drawing.Color.LightGray;
                SelectLarge = false;
            }

            UpdateLockerComboBoxes();
        }

        private void bXLarge_Click(object sender, EventArgs e)
        {
            if (!SelectXLarge)
            {
                bXLarge.BackColor = System.Drawing.Color.LightGreen;
                SelectXLarge = true;
            }
            else
            {
                bXLarge.BackColor = System.Drawing.Color.LightGray;
                SelectXLarge = false;
            }

            UpdateLockerComboBoxes();
        }

        private void bClearAllHires_Click(object sender, EventArgs e)
        {
            DialogResult dres = MessageBox.Show("Are you sure?", "Chear All Hires", MessageBoxButtons.YesNo);

            if (dres == DialogResult.Yes)
                Globals.LockData.ClearAllHires();

            UpdateLockerComboBoxes();
        }

        private void cboxCurrentLocker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxCurrentLocker.Text != "")
            {
                if(Globals.LockData.IsLockerActive(cboxCurrentLocker.Text))
                {
                    bLockActivate.BackColor = System.Drawing.Color.LightGreen;
                    bDeactivate.BackColor = System.Drawing.Color.LightGray;
                }
                else
                {
                    bLockActivate.BackColor = System.Drawing.Color.LightGray;
                    bDeactivate.BackColor = System.Drawing.Color.LightPink;
                }
            }
        }

        private void cboxNewLocker_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboxNewLocker.Text != "")
            {
                if (Globals.LockData.IsLockerActive(cboxNewLocker.Text))
                {
                    bLockActivate.BackColor = System.Drawing.Color.LightGreen;
                    bDeactivate.BackColor = System.Drawing.Color.LightGray;
                }
                else
                {
                    bLockActivate.BackColor = System.Drawing.Color.LightGray;
                    bDeactivate.BackColor = System.Drawing.Color.LightPink;
                }
            }

        }

        private void bLockActivate_Click(object sender, EventArgs e)
        {
            int LockerNumber = 0;

            if (cboxCurrentLocker.Visible == true)
            {
                if (cboxCurrentLocker.Text != "")
                    LockerNumber = Convert.ToInt16(cboxCurrentLocker.Text);
            }
            else
            {
                if (tboxUsed.Text != "")
                    LockerNumber = Convert.ToInt16(tboxUsed.Text);
            }

            if (LockerNumber == 0)
            {
                if (cboxNewLocker.Visible == true)
                {
                    if (cboxNewLocker.Text != "")
                        LockerNumber = Convert.ToInt16(cboxNewLocker.Text);
                }
                else
                {
                    if (tboxFree.Text != "")
                        LockerNumber = Convert.ToInt16(tboxFree.Text);
                }
            }

            if (LockerNumber != 0)
            {
                Globals.LockData.EnableLocker(LockerNumber.ToString());

                bLockActivate.BackColor = System.Drawing.Color.LightGreen;
                bDeactivate.BackColor = System.Drawing.Color.LightGray;

                MessageBox.Show("Locker " + LockerNumber.ToString() + " activated", "Locker Activated", MessageBoxButtons.OK);
            }

            cboxCurrentLocker.Text = "";
            cboxNewLocker.Text = "";
            tboxAccessCode.Text = "";
            AccessCode = "";
        }

        private void cbIncludeDisabled_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLockerComboBoxes();
        }

        private bool taskbaRhidden = true;
        private void bTaskBarToggle_Click(object sender, EventArgs e)
        {
            //            SendKeys.Send("%{t}");
            Globals.HideTaskbar();
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            if(cboxCurrentLocker.Visible == true)
            {
                cboxCurrentLocker.Text = "";
                cboxCurrentLocker.Visible = false;
                tboxUsed.Visible = true;
                tboxUsed.Focus();
            }
            else
            {
                cboxCurrentLocker.Visible = true;
                tboxUsed.Text = "";
                tboxUsed.Visible = false;
                cboxCurrentLocker.Focus();
            }
        }

        private void CboxCurrentLocker_Enter(object sender, EventArgs e)
        {
            BoxFocus = 0;
        }

        private void CboxNewLocker_Enter(object sender, EventArgs e)
        {
            BoxFocus = 1;
        }

        private void LabFree_Click(object sender, EventArgs e)
        {
            if (cboxNewLocker.Visible == true)
            {
                cboxNewLocker.Text = "";
                cboxNewLocker.Visible = false;
                tboxFree.Visible = true;
                tboxFree.Focus();
            }
            else
            {
                cboxNewLocker.Visible = true;
                tboxFree.Visible = false;
                tboxFree.Text = "";
                cboxNewLocker.Focus();
            }
        }

        private void TboxUsed_Enter(object sender, EventArgs e)
        {
            t = (TextBox)sender;

        }

        private void TboxFree_TextChanged(object sender, EventArgs e)
        {

        }

        private void TboxFree_Enter(object sender, EventArgs e)
        {
            t = (TextBox)sender;
        }

        private void BTaskShow_Click(object sender, EventArgs e)
        {
            Globals.ShowTaskbar();
        }
    }
}
