﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lockers.Forms
{
    public partial class Credit_NoResponse : Form
    {
        public Credit_NoResponse()
        {
            InitializeComponent();
        }

        private void Credit_NoResponse_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }


            if (Properties.Settings.Default.ReleaseType == "Linux")
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Properties.Settings.Default.DefaultLanguage + "/CardPaymentNoReponse_1182x789.tiff");
            else
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Properties.Settings.Default.DefaultLanguage + "\\CardPaymentNoReponse_1182x789.tiff");

            timer1.Interval = 5000;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            this.Close();
        }
    }
}
