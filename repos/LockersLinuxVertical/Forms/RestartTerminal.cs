﻿using System.Diagnostics;
using System.Windows.Forms;

namespace Lockers
{
    public partial class RestartTerminal : Form
    {
        public RestartTerminal()
        {
            InitializeComponent();
        }

        private void RestartTerminal_Load(object sender, System.EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

        }

        private void BTermOff_Click(object sender, System.EventArgs e)
        {
            DialogResult dres = MessageBox.Show("Are you sure?", "This will power down the terminal", MessageBoxButtons.YesNo);

            if (dres != DialogResult.Yes)
                return;

            Process.Start("shutdown", "/s /t 0");    // starts the shutdown application 
                                                     // the argument /s is to shut down the computer
                                                     // the argument /t 0 is to tell the process that 
                                                     // the specified operation needs to be completed 
                                                     // after 0 seconds
        }

        private void BTermRestart_Click(object sender, System.EventArgs e)
        {
            DialogResult dres = MessageBox.Show("Are you sure?", "This will restart the terminal", MessageBoxButtons.YesNo);

            if (dres != DialogResult.Yes)
                return;

            Process.Start("shutdown","/r /t 0"); // the argument /r is to restart the computer
        }

        private void Button3_Click(object sender, System.EventArgs e)
        {
            if (Properties.Settings.Default.MaintenanceLockout == false)
            {
                bMaintLock.BackColor = System.Drawing.Color.Red;
                Properties.Settings.Default.MaintenanceLockout = true;
            }
            else
            {
                bMaintLock.BackColor = System.Drawing.Color.Green;
                Properties.Settings.Default.MaintenanceLockout = false;
            }
        }

        private void RestartTerminal_Shown(object sender, System.EventArgs e)
        {
            if (Properties.Settings.Default.MaintenanceLockout == true)
                bMaintLock.BackColor = System.Drawing.Color.Red;
            else
                bMaintLock.BackColor = System.Drawing.Color.Green;

            if (Properties.Settings.Default.RentalHold == true)
                bHoldRentals.BackColor = System.Drawing.Color.Red;
            else
                bHoldRentals.BackColor = System.Drawing.Color.Green;
        }

        private void BHoldRentals_Click(object sender, System.EventArgs e)
        {
            if(Properties.Settings.Default.RentalHold == true)
            {
                Properties.Settings.Default.RentalHold = false;
                bHoldRentals.BackColor = System.Drawing.Color.Green;
            }
            else
            {
                Properties.Settings.Default.RentalHold = true;
                bHoldRentals.BackColor = System.Drawing.Color.Red;
            }
        }
    }
}
