﻿namespace Lockers
{
    partial class SelectSize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectSize));
            this.bSmallSize = new System.Windows.Forms.Button();
            this.bMediumSize = new System.Windows.Forms.Button();
            this.bLargeSize = new System.Windows.Forms.Button();
            this.bExtraLargewSize = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bBack = new System.Windows.Forms.Button();
            this.labTimeHead = new System.Windows.Forms.Label();
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.lbltime = new System.Windows.Forms.Label();
            this.labSelectSize = new System.Windows.Forms.Label();
            this.labBack = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bSmallSize
            // 
            this.bSmallSize.BackColor = System.Drawing.Color.White;
            this.bSmallSize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bSmallSize.BackgroundImage")));
            this.bSmallSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bSmallSize.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.bSmallSize.FlatAppearance.BorderSize = 0;
            this.bSmallSize.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bSmallSize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bSmallSize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bSmallSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSmallSize.Location = new System.Drawing.Point(38, 194);
            this.bSmallSize.Margin = new System.Windows.Forms.Padding(0);
            this.bSmallSize.Name = "bSmallSize";
            this.bSmallSize.Size = new System.Drawing.Size(166, 189);
            this.bSmallSize.TabIndex = 0;
            this.bSmallSize.Tag = "SMALL";
            this.bSmallSize.UseVisualStyleBackColor = false;
            this.bSmallSize.Click += new System.EventHandler(this.btnSize_Click);
            // 
            // bMediumSize
            // 
            this.bMediumSize.BackColor = System.Drawing.Color.White;
            this.bMediumSize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bMediumSize.BackgroundImage")));
            this.bMediumSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bMediumSize.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.bMediumSize.FlatAppearance.BorderSize = 0;
            this.bMediumSize.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bMediumSize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bMediumSize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bMediumSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bMediumSize.Location = new System.Drawing.Point(207, 156);
            this.bMediumSize.Name = "bMediumSize";
            this.bMediumSize.Size = new System.Drawing.Size(166, 227);
            this.bMediumSize.TabIndex = 0;
            this.bMediumSize.Tag = "MEDIUM";
            this.bMediumSize.UseVisualStyleBackColor = false;
            this.bMediumSize.Click += new System.EventHandler(this.btnSize_Click);
            // 
            // bLargeSize
            // 
            this.bLargeSize.BackColor = System.Drawing.Color.White;
            this.bLargeSize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bLargeSize.BackgroundImage")));
            this.bLargeSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bLargeSize.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.bLargeSize.FlatAppearance.BorderSize = 0;
            this.bLargeSize.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bLargeSize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bLargeSize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bLargeSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bLargeSize.Location = new System.Drawing.Point(379, 117);
            this.bLargeSize.Name = "bLargeSize";
            this.bLargeSize.Size = new System.Drawing.Size(166, 266);
            this.bLargeSize.TabIndex = 0;
            this.bLargeSize.Tag = "LARGE";
            this.bLargeSize.UseVisualStyleBackColor = false;
            this.bLargeSize.Click += new System.EventHandler(this.btnSize_Click);
            // 
            // bExtraLargewSize
            // 
            this.bExtraLargewSize.BackColor = System.Drawing.Color.White;
            this.bExtraLargewSize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bExtraLargewSize.BackgroundImage")));
            this.bExtraLargewSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bExtraLargewSize.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.bExtraLargewSize.FlatAppearance.BorderSize = 0;
            this.bExtraLargewSize.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bExtraLargewSize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bExtraLargewSize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bExtraLargewSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bExtraLargewSize.Location = new System.Drawing.Point(551, 76);
            this.bExtraLargewSize.Name = "bExtraLargewSize";
            this.bExtraLargewSize.Size = new System.Drawing.Size(166, 307);
            this.bExtraLargewSize.TabIndex = 0;
            this.bExtraLargewSize.Tag = "XLARGE";
            this.bExtraLargewSize.UseVisualStyleBackColor = false;
            this.bExtraLargewSize.Click += new System.EventHandler(this.btnSize_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            this.bBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBack.BackgroundImage")));
            this.bBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBack.Location = new System.Drawing.Point(31, 420);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(65, 65);
            this.bBack.TabIndex = 1;
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // labTimeHead
            // 
            this.labTimeHead.AutoSize = true;
            this.labTimeHead.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHead.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labTimeHead.ForeColor = System.Drawing.Color.White;
            this.labTimeHead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labTimeHead.Location = new System.Drawing.Point(12, 26);
            this.labTimeHead.Name = "labTimeHead";
            this.labTimeHead.Size = new System.Drawing.Size(71, 29);
            this.labTimeHead.TabIndex = 1;
            this.labTimeHead.Text = "TIME";
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(21, 55);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(89, 55);
            this.lbltime.TabIndex = 1;
            this.lbltime.Text = "     ";
            // 
            // labSelectSize
            // 
            this.labSelectSize.AutoSize = true;
            this.labSelectSize.BackColor = System.Drawing.Color.Transparent;
            this.labSelectSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labSelectSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            this.labSelectSize.ForeColor = System.Drawing.Color.White;
            this.labSelectSize.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labSelectSize.Location = new System.Drawing.Point(283, 26);
            this.labSelectSize.Name = "labSelectSize";
            this.labSelectSize.Size = new System.Drawing.Size(163, 36);
            this.labSelectSize.TabIndex = 35;
            this.labSelectSize.Text = "Select Size";
            this.labSelectSize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labBack
            // 
            this.labBack.AutoSize = true;
            this.labBack.BackColor = System.Drawing.Color.Transparent;
            this.labBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labBack.ForeColor = System.Drawing.Color.White;
            this.labBack.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labBack.Location = new System.Drawing.Point(33, 488);
            this.labBack.Name = "labBack";
            this.labBack.Size = new System.Drawing.Size(66, 29);
            this.labBack.TabIndex = 38;
            this.labBack.Text = "Back";
            this.labBack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // SelectSize
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(768, 526);
            this.Controls.Add(this.labBack);
            this.Controls.Add(this.labSelectSize);
            this.Controls.Add(this.labTimeHead);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bExtraLargewSize);
            this.Controls.Add(this.bLargeSize);
            this.Controls.Add(this.bMediumSize);
            this.Controls.Add(this.bSmallSize);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectSize";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SelectSize";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SelectSize_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bSmallSize;
        private System.Windows.Forms.Button bMediumSize;
        private System.Windows.Forms.Button bLargeSize;
        private System.Windows.Forms.Button bExtraLargewSize;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Label labTimeHead;
        private System.Windows.Forms.Timer timIdle;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label labSelectSize;
        private System.Windows.Forms.Label labBack;
    }
}