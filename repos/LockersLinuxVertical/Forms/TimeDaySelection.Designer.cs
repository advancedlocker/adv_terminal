﻿namespace Lockers
{
    partial class TimeDaySelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimeDaySelection));
            this.txtDays = new System.Windows.Forms.TextBox();
            this.bDaysMinus = new System.Windows.Forms.Button();
            this.bAddDays = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labTimeHead = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bOK = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.lblCost = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblrate = new System.Windows.Forms.Label();
            this.lblsize = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtDays
            // 
            this.txtDays.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDays.Location = new System.Drawing.Point(465, 291);
            this.txtDays.Name = "txtDays";
            this.txtDays.Size = new System.Drawing.Size(100, 91);
            this.txtDays.TabIndex = 20;
            this.txtDays.Text = "00";
            this.txtDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bDaysMinus
            // 
            this.bDaysMinus.BackColor = System.Drawing.Color.Transparent;
            this.bDaysMinus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bDaysMinus.BackgroundImage")));
            this.bDaysMinus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bDaysMinus.FlatAppearance.BorderSize = 0;
            this.bDaysMinus.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bDaysMinus.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bDaysMinus.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bDaysMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bDaysMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bDaysMinus.Location = new System.Drawing.Point(469, 438);
            this.bDaysMinus.Name = "bDaysMinus";
            this.bDaysMinus.Size = new System.Drawing.Size(85, 79);
            this.bDaysMinus.TabIndex = 18;
            this.bDaysMinus.UseVisualStyleBackColor = false;
            this.bDaysMinus.Click += new System.EventHandler(this.bDaysMinus_Click);
            // 
            // bAddDays
            // 
            this.bAddDays.BackColor = System.Drawing.Color.Transparent;
            this.bAddDays.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bAddDays.BackgroundImage")));
            this.bAddDays.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bAddDays.FlatAppearance.BorderSize = 0;
            this.bAddDays.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bAddDays.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bAddDays.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bAddDays.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bAddDays.Location = new System.Drawing.Point(469, 147);
            this.bAddDays.Name = "bAddDays";
            this.bAddDays.Size = new System.Drawing.Size(79, 80);
            this.bAddDays.TabIndex = 19;
            this.bAddDays.UseVisualStyleBackColor = false;
            this.bAddDays.Click += new System.EventHandler(this.bAddDays_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.labTimeHead);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 140);
            this.panel1.TabIndex = 24;
            // 
            // labTimeHead
            // 
            this.labTimeHead.AutoSize = true;
            this.labTimeHead.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHead.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labTimeHead.ForeColor = System.Drawing.Color.White;
            this.labTimeHead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labTimeHead.Location = new System.Drawing.Point(12, 27);
            this.labTimeHead.Name = "labTimeHead";
            this.labTimeHead.Size = new System.Drawing.Size(71, 29);
            this.labTimeHead.TabIndex = 1;
            this.labTimeHead.Text = "TIME";
            // 
            // button14
            // 
            this.button14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button14.BackgroundImage")));
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button14.Location = new System.Drawing.Point(928, 24);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(79, 81);
            this.button14.TabIndex = 2;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(4, 47);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(0, 55);
            this.lbltime.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(940, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 29);
            this.label2.TabIndex = 20;
            this.label2.Text = "help";
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(811, 548);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 65);
            this.label5.TabIndex = 25;
            this.label5.Text = "$";
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            this.bOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bOK.BackgroundImage")));
            this.bOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bOK.Location = new System.Drawing.Point(889, 645);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(81, 112);
            this.bOK.TabIndex = 30;
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            this.bBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBack.BackgroundImage")));
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBack.Location = new System.Drawing.Point(17, 645);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(79, 111);
            this.bBack.TabIndex = 29;
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // lblCost
            // 
            this.lblCost.AutoSize = true;
            this.lblCost.BackColor = System.Drawing.Color.Transparent;
            this.lblCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCost.ForeColor = System.Drawing.Color.Black;
            this.lblCost.Location = new System.Drawing.Point(851, 548);
            this.lblCost.Margin = new System.Windows.Forms.Padding(0);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(114, 53);
            this.lblCost.TabIndex = 28;
            this.lblCost.Text = "0.00";
            this.lblCost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(99, 548);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 62);
            this.label3.TabIndex = 26;
            this.label3.Text = "$";
            // 
            // lblrate
            // 
            this.lblrate.AutoSize = true;
            this.lblrate.BackColor = System.Drawing.Color.Transparent;
            this.lblrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrate.ForeColor = System.Drawing.Color.White;
            this.lblrate.Location = new System.Drawing.Point(142, 548);
            this.lblrate.Name = "lblrate";
            this.lblrate.Size = new System.Drawing.Size(321, 59);
            this.lblrate.TabIndex = 27;
            this.lblrate.Text = "0.00 Per Day";
            // 
            // lblsize
            // 
            this.lblsize.AutoSize = true;
            this.lblsize.BackColor = System.Drawing.Color.Transparent;
            this.lblsize.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsize.ForeColor = System.Drawing.Color.White;
            this.lblsize.Location = new System.Drawing.Point(51, 168);
            this.lblsize.Margin = new System.Windows.Forms.Padding(0);
            this.lblsize.Name = "lblsize";
            this.lblsize.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblsize.Size = new System.Drawing.Size(249, 59);
            this.lblsize.TabIndex = 31;
            this.lblsize.Text = "X-LARGE";
            this.lblsize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // TimeDaySelection
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(195)))), ((int)(((byte)(65)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lblsize);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.lblCost);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblrate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtDays);
            this.Controls.Add(this.bDaysMinus);
            this.Controls.Add(this.bAddDays);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TimeDaySelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TimeDaySelection";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.TimeDaySelection_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtDays;
        private System.Windows.Forms.Button bDaysMinus;
        private System.Windows.Forms.Button bAddDays;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labTimeHead;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Label lblCost;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblrate;
        private System.Windows.Forms.Label lblsize;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timIdle;
    }
}