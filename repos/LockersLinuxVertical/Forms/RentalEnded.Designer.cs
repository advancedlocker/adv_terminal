﻿namespace Lockers
{
    partial class RentalEnded
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RentalEnded));
            this.bOK = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblremaintime = new System.Windows.Forms.Label();
            this.lblLocknum = new System.Windows.Forms.Label();
            this.labBack = new System.Windows.Forms.Label();
            this.labOK = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            this.bOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bOK.BackgroundImage")));
            this.bOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bOK.Location = new System.Drawing.Point(649, 429);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(65, 65);
            this.bOK.TabIndex = 5;
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            this.bBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBack.BackgroundImage")));
            this.bBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBack.Location = new System.Drawing.Point(36, 417);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(65, 65);
            this.bBack.TabIndex = 8;
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "TIME";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 27F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(12, 47);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(167, 40);
            this.lbltime.TabIndex = 1;
            this.lbltime.Text = "01:22 pm";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblremaintime
            // 
            this.lblremaintime.AutoSize = true;
            this.lblremaintime.BackColor = System.Drawing.Color.Transparent;
            this.lblremaintime.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblremaintime.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblremaintime.Location = new System.Drawing.Point(68, 297);
            this.lblremaintime.Name = "lblremaintime";
            this.lblremaintime.Size = new System.Drawing.Size(119, 46);
            this.lblremaintime.TabIndex = 26;
            this.lblremaintime.Text = "00:00";
            this.lblremaintime.Visible = false;
            // 
            // lblLocknum
            // 
            this.lblLocknum.AutoSize = true;
            this.lblLocknum.BackColor = System.Drawing.Color.Transparent;
            this.lblLocknum.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocknum.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lblLocknum.Location = new System.Drawing.Point(68, 199);
            this.lblLocknum.Name = "lblLocknum";
            this.lblLocknum.Size = new System.Drawing.Size(42, 46);
            this.lblLocknum.TabIndex = 27;
            this.lblLocknum.Text = "0";
            // 
            // labBack
            // 
            this.labBack.AutoSize = true;
            this.labBack.BackColor = System.Drawing.Color.Transparent;
            this.labBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labBack.ForeColor = System.Drawing.Color.White;
            this.labBack.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labBack.Location = new System.Drawing.Point(35, 485);
            this.labBack.Name = "labBack";
            this.labBack.Size = new System.Drawing.Size(66, 29);
            this.labBack.TabIndex = 39;
            this.labBack.Text = "Back";
            this.labBack.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // labOK
            // 
            this.labOK.AutoSize = true;
            this.labOK.BackColor = System.Drawing.Color.Transparent;
            this.labOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labOK.ForeColor = System.Drawing.Color.White;
            this.labOK.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labOK.Location = new System.Drawing.Point(666, 488);
            this.labOK.Name = "labOK";
            this.labOK.Size = new System.Drawing.Size(48, 29);
            this.labOK.TabIndex = 38;
            this.labOK.Text = "OK";
            this.labOK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // RentalEnded
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(768, 526);
            this.Controls.Add(this.labBack);
            this.Controls.Add(this.labOK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.lblremaintime);
            this.Controls.Add(this.lblLocknum);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bOK);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RentalEnded";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RentalEnded";
            this.Load += new System.EventHandler(this.RentalEnded_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblremaintime;
        private System.Windows.Forms.Label lblLocknum;
        private System.Windows.Forms.Label labBack;
        private System.Windows.Forms.Label labOK;
    }
}