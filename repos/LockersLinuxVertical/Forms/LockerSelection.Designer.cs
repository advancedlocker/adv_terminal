﻿namespace Lockers
{
    partial class LockerSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.bHelp = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.bOpenLocker = new System.Windows.Forms.Button();
            this.bRentLocker = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.bHelp);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 109);
            this.panel1.TabIndex = 7;
            // 
            // bHelp
            // 
            this.bHelp.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bHelp.Location = new System.Drawing.Point(928, 24);
            this.bHelp.Name = "bHelp";
            this.bHelp.Size = new System.Drawing.Size(66, 63);
            this.bHelp.TabIndex = 2;
            this.bHelp.Text = "button3";
            this.bHelp.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Black;
            this.lbltime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(9, 24);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(121, 48);
            this.lbltime.TabIndex = 1;
            this.lbltime.Text = "01:22";
            // 
            // bOpenLocker
            // 
            this.bOpenLocker.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bOpenLocker.Location = new System.Drawing.Point(470, 261);
            this.bOpenLocker.Name = "bOpenLocker";
            this.bOpenLocker.Size = new System.Drawing.Size(224, 183);
            this.bOpenLocker.TabIndex = 5;
            this.bOpenLocker.Text = "Open Locker";
            this.bOpenLocker.UseVisualStyleBackColor = true;
            this.bOpenLocker.Click += new System.EventHandler(this.button2_Click);
            // 
            // bRentLocker
            // 
            this.bRentLocker.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bRentLocker.Location = new System.Drawing.Point(146, 261);
            this.bRentLocker.Name = "bRentLocker";
            this.bRentLocker.Size = new System.Drawing.Size(224, 183);
            this.bRentLocker.TabIndex = 6;
            this.bRentLocker.Text = "Rent Locker";
            this.bRentLocker.UseVisualStyleBackColor = true;
            this.bRentLocker.Click += new System.EventHandler(this.button1_Click);
            // 
            // bBack
            // 
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.Location = new System.Drawing.Point(23, 686);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(71, 50);
            this.bBack.TabIndex = 8;
            this.bBack.Text = "<--- Back";
            this.bBack.UseVisualStyleBackColor = true;
            // 
            // LockerSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bOpenLocker);
            this.Controls.Add(this.bRentLocker);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LockerSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LockerSelection";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.LockerSelection_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bHelp;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Button bOpenLocker;
        private System.Windows.Forms.Button bRentLocker;
        private System.Windows.Forms.Button bBack;
    }
}