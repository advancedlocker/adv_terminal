﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace Lockers
{
    public partial class BlackScreen : Form
    {
       
        public BlackScreen()
        {
            InitializeComponent();
            if(Properties.Settings.Default.ReleaseType == "Linux")
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Properties.Settings.Default.DefaultLanguage + "/Blank_1182x789.tiff");
            else
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Properties.Settings.Default.DefaultLanguage + "\\Blank_1182x789.tiff");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           
        }

        private void BlackScreen_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
        }
    }
}
