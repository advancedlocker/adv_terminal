﻿


namespace Lockers
{

    using System;
    using System.Data;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Forms;

    public partial class TimeSelection : Form
    {
        private delegate void ComputeDelegate(int value1, int value2);
        private ComputeDelegate computeDelegate = null;

        int timehrs = 1;
        int timeMin = 0;

        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        decimal mRate= Properties.Settings.Default.rate;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        public TimeSelection()
        {

            

            InitializeComponent();

            String ImageFilename = Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Globals.CurrentLanguage + "\\Select_Duration_HM.tiff";

            if (Properties.Settings.Default.ReleaseType == "Linux")
                ImageFilename = Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Globals.CurrentLanguage + "\\Select_Duration_HM.tiff";
            else
                ImageFilename = Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Globals.CurrentLanguage + "\\Select_Duration_HM.tiff";

            this.BackgroundImage = Image.FromFile(ImageFilename);

            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
            this.computeDelegate = new ComputeDelegate(ComputeCost);

           

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);

            //mRate = Globals.LockData.Locker_UPrice();
            int lockersize = (int)Globals.LockData.Locker_Size;
            var mvarrate = (from Rows in Globals.LockData.Lockers.AsEnumerable()
                            where Rows.Field<string>("lockersize") == lockersize.ToString()
                            select new
                            {
                                RateLock = Rows.Field<decimal>("uprice")
                       
                           }).First();
            mRate = mvarrate.RateLock;
            lblrate.Text = mRate.ToString("#,##0.00") + " Per hour";

            timer1.Start();
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
            timIdle.Start();
        }

        private void LockData_LockerDataError(object sender, LogMessageEventArgs e)
        {
            MessageBox.Show(e.Messsage,"Error",MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void TimeSelection_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            switch (Globals.LockData.Locker_Size)
                {
                case eLockerSize.SMALL:
                    lblsize.Text = "SMALL";
                    break;
                case eLockerSize.MEDIUM:
                    lblsize.Text = "MEDIUM";
                    break;
                case eLockerSize.LARGE:
                    lblsize.Text = "LARGE";
                    break;
                case eLockerSize.XLARGE:
                    lblsize.Text = "X-LARGE";
                    break;
                }
            
            UpdateDisplay();
        }

        private void ComputeCost(int hrs, int min)
        {
            decimal m_price = mRate;
            decimal totalhrs = hrs + ((decimal)min / 60);
            decimal cost = totalhrs * m_price;
            lblCost.Text = cost.ToString("#,##0.00");
            Globals.DueAmnt = (decimal) Math.Round(cost,2);
            Globals.LockData.COST = (decimal)Math.Round(cost, 2);
            Globals.LockData.Hours = hrs;
            Globals.LockData.Minutes = min;
        }

       private void UpdateDisplay()
        {
            txthrs.Text = timehrs.ToString();
            txtmin.Text = timeMin.ToString();
            this.Invoke(this.computeDelegate, new object[] { timehrs, timeMin }); 
        }

        private void btnAddHrs_Click(object sender, EventArgs e)
        {
            if (timehrs < 24)
            {
                timehrs = (timehrs + 1) % 25;
            }
            else
            {
                timehrs = 1;
            }
            UpdateDisplay();
        }

        private void btnMinusHrs_Click(object sender, EventArgs e)
        {
            if (timehrs > 1)
            {
                timehrs = (timehrs - 1) % 24;
               
            }
            else
            {
                timehrs = 24;
            }
            UpdateDisplay();
        }

        private void btnaddMin_Click(object sender, EventArgs e)
        {
            if (timehrs < 60)
            {
                timeMin = (timeMin + 1) % 61;
            }
            else
            {
                timeMin = 1;
            }
            UpdateDisplay();
        }

        private void btnMinusMin_Click(object sender, EventArgs e)
        {
            if (timeMin >= 1)
            {
                timeMin = (timeMin - 1) % 61;

            }
            else
            {
                timeMin = 60;
            }
            UpdateDisplay();
        }

        private void bback_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            timer1.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void TimeSelection_FormClosed(object sender, FormClosedEventArgs e)
        {
            Globals.LockData.LockerDatabaseError -= LockData_LockerDataError;
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
