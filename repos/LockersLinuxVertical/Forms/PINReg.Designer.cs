﻿namespace Lockers
{
    partial class PINReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PINReg));
            this.bNumber1 = new System.Windows.Forms.Button();
            this.bNumber2 = new System.Windows.Forms.Button();
            this.bNumber3 = new System.Windows.Forms.Button();
            this.bNumber6 = new System.Windows.Forms.Button();
            this.bNumber5 = new System.Windows.Forms.Button();
            this.bNumber4 = new System.Windows.Forms.Button();
            this.bNumber7 = new System.Windows.Forms.Button();
            this.bNumber8 = new System.Windows.Forms.Button();
            this.bNumber9 = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.lblCode = new System.Windows.Forms.Label();
            this.bClear = new System.Windows.Forms.Button();
            this.lblmsg = new System.Windows.Forms.Label();
            this.bBack = new System.Windows.Forms.Button();
            this.bNumber0 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.labTimeHdg = new System.Windows.Forms.Label();
            this.labHdgChoosePin = new System.Windows.Forms.Label();
            this.labHdgPressOK = new System.Windows.Forms.Label();
            this.labRemember1 = new System.Windows.Forms.Label();
            this.labCodeLength = new System.Windows.Forms.Label();
            this.labDigitsLong = new System.Windows.Forms.Label();
            this.labRemember2 = new System.Windows.Forms.Label();
            this.labRemember3 = new System.Windows.Forms.Label();
            this.labYourCode = new System.Windows.Forms.Label();
            this.labOK = new System.Windows.Forms.Label();
            this.labBack = new System.Windows.Forms.Label();
            this.bGhost = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bNumber1
            // 
            this.bNumber1.BackColor = System.Drawing.Color.White;
            this.bNumber1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber1, "bNumber1");
            this.bNumber1.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.bNumber1.Name = "bNumber1";
            this.bNumber1.UseVisualStyleBackColor = false;
            this.bNumber1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bNumber1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // bNumber2
            // 
            this.bNumber2.BackColor = System.Drawing.Color.White;
            this.bNumber2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber2, "bNumber2");
            this.bNumber2.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.bNumber2.Name = "bNumber2";
            this.bNumber2.UseVisualStyleBackColor = false;
            this.bNumber2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bNumber2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // bNumber3
            // 
            this.bNumber3.BackColor = System.Drawing.Color.White;
            this.bNumber3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber3, "bNumber3");
            this.bNumber3.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.bNumber3.Name = "bNumber3";
            this.bNumber3.UseVisualStyleBackColor = false;
            this.bNumber3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bNumber3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // bNumber6
            // 
            this.bNumber6.BackColor = System.Drawing.Color.White;
            this.bNumber6.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber6.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber6, "bNumber6");
            this.bNumber6.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.bNumber6.Name = "bNumber6";
            this.bNumber6.UseVisualStyleBackColor = false;
            this.bNumber6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bNumber6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // bNumber5
            // 
            this.bNumber5.BackColor = System.Drawing.Color.White;
            this.bNumber5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber5.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber5, "bNumber5");
            this.bNumber5.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.bNumber5.Name = "bNumber5";
            this.bNumber5.UseVisualStyleBackColor = false;
            this.bNumber5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bNumber5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // bNumber4
            // 
            this.bNumber4.BackColor = System.Drawing.Color.White;
            this.bNumber4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber4, "bNumber4");
            this.bNumber4.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.bNumber4.Name = "bNumber4";
            this.bNumber4.UseVisualStyleBackColor = false;
            this.bNumber4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bNumber4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // bNumber7
            // 
            this.bNumber7.BackColor = System.Drawing.Color.White;
            this.bNumber7.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber7.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber7, "bNumber7");
            this.bNumber7.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.bNumber7.Name = "bNumber7";
            this.bNumber7.UseVisualStyleBackColor = false;
            this.bNumber7.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bNumber7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // bNumber8
            // 
            this.bNumber8.BackColor = System.Drawing.Color.White;
            this.bNumber8.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber8.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber8, "bNumber8");
            this.bNumber8.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.bNumber8.Name = "bNumber8";
            this.bNumber8.UseVisualStyleBackColor = false;
            this.bNumber8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bNumber8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // bNumber9
            // 
            this.bNumber9.BackColor = System.Drawing.Color.White;
            this.bNumber9.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber9.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber9, "bNumber9");
            this.bNumber9.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.bNumber9.Name = "bNumber9";
            this.bNumber9.UseVisualStyleBackColor = false;
            this.bNumber9.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bNumber9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bOK, "bOK");
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.Name = "bOK";
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // lblCode
            // 
            this.lblCode.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.lblCode, "lblCode");
            this.lblCode.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.lblCode.Name = "lblCode";
            // 
            // bClear
            // 
            this.bClear.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bClear, "bClear");
            this.bClear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bClear.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bClear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bClear.Name = "bClear";
            this.bClear.Tag = "0";
            this.bClear.UseVisualStyleBackColor = false;
            this.bClear.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bClear.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // lblmsg
            // 
            resources.ApplyResources(this.lblmsg, "lblmsg");
            this.lblmsg.BackColor = System.Drawing.Color.Transparent;
            this.lblmsg.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.lblmsg.Name = "lblmsg";
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bBack, "bBack");
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.Name = "bBack";
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // bNumber0
            // 
            this.bNumber0.BackColor = System.Drawing.Color.White;
            this.bNumber0.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber0.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber0.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber0.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber0, "bNumber0");
            this.bNumber0.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.bNumber0.Name = "bNumber0";
            this.bNumber0.UseVisualStyleBackColor = false;
            this.bNumber0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            this.bNumber0.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bMouse_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // labTimeHdg
            // 
            resources.ApplyResources(this.labTimeHdg, "labTimeHdg");
            this.labTimeHdg.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHdg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHdg.ForeColor = System.Drawing.Color.White;
            this.labTimeHdg.Name = "labTimeHdg";
            // 
            // labHdgChoosePin
            // 
            resources.ApplyResources(this.labHdgChoosePin, "labHdgChoosePin");
            this.labHdgChoosePin.BackColor = System.Drawing.Color.Transparent;
            this.labHdgChoosePin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labHdgChoosePin.ForeColor = System.Drawing.Color.White;
            this.labHdgChoosePin.Name = "labHdgChoosePin";
            // 
            // labHdgPressOK
            // 
            resources.ApplyResources(this.labHdgPressOK, "labHdgPressOK");
            this.labHdgPressOK.BackColor = System.Drawing.Color.Transparent;
            this.labHdgPressOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labHdgPressOK.ForeColor = System.Drawing.Color.White;
            this.labHdgPressOK.Name = "labHdgPressOK";
            // 
            // labRemember1
            // 
            resources.ApplyResources(this.labRemember1, "labRemember1");
            this.labRemember1.BackColor = System.Drawing.Color.Transparent;
            this.labRemember1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labRemember1.ForeColor = System.Drawing.Color.White;
            this.labRemember1.Name = "labRemember1";
            // 
            // labCodeLength
            // 
            resources.ApplyResources(this.labCodeLength, "labCodeLength");
            this.labCodeLength.BackColor = System.Drawing.Color.Transparent;
            this.labCodeLength.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labCodeLength.ForeColor = System.Drawing.Color.White;
            this.labCodeLength.Name = "labCodeLength";
            // 
            // labDigitsLong
            // 
            resources.ApplyResources(this.labDigitsLong, "labDigitsLong");
            this.labDigitsLong.BackColor = System.Drawing.Color.Transparent;
            this.labDigitsLong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labDigitsLong.ForeColor = System.Drawing.Color.White;
            this.labDigitsLong.Name = "labDigitsLong";
            // 
            // labRemember2
            // 
            resources.ApplyResources(this.labRemember2, "labRemember2");
            this.labRemember2.BackColor = System.Drawing.Color.Transparent;
            this.labRemember2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labRemember2.ForeColor = System.Drawing.Color.White;
            this.labRemember2.Name = "labRemember2";
            // 
            // labRemember3
            // 
            resources.ApplyResources(this.labRemember3, "labRemember3");
            this.labRemember3.BackColor = System.Drawing.Color.Transparent;
            this.labRemember3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labRemember3.ForeColor = System.Drawing.Color.White;
            this.labRemember3.Name = "labRemember3";
            // 
            // labYourCode
            // 
            resources.ApplyResources(this.labYourCode, "labYourCode");
            this.labYourCode.BackColor = System.Drawing.Color.Transparent;
            this.labYourCode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labYourCode.ForeColor = System.Drawing.Color.White;
            this.labYourCode.Name = "labYourCode";
            // 
            // labOK
            // 
            resources.ApplyResources(this.labOK, "labOK");
            this.labOK.BackColor = System.Drawing.Color.Transparent;
            this.labOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labOK.ForeColor = System.Drawing.Color.White;
            this.labOK.Name = "labOK";
            this.labOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // labBack
            // 
            resources.ApplyResources(this.labBack, "labBack");
            this.labBack.BackColor = System.Drawing.Color.Transparent;
            this.labBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labBack.ForeColor = System.Drawing.Color.White;
            this.labBack.Name = "labBack";
            this.labBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // bGhost
            // 
            this.bGhost.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bGhost, "bGhost");
            this.bGhost.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bGhost.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bGhost.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bGhost.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bGhost.Name = "bGhost";
            this.bGhost.Tag = "0";
            this.bGhost.UseVisualStyleBackColor = false;
            this.bGhost.Click += new System.EventHandler(this.PINReg_KeyPress);
            this.bGhost.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PINReg_KeyPress);
            // 
            // PINReg
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.bGhost);
            this.Controls.Add(this.labBack);
            this.Controls.Add(this.labOK);
            this.Controls.Add(this.labYourCode);
            this.Controls.Add(this.labRemember3);
            this.Controls.Add(this.labRemember2);
            this.Controls.Add(this.labDigitsLong);
            this.Controls.Add(this.labCodeLength);
            this.Controls.Add(this.labRemember1);
            this.Controls.Add(this.labHdgPressOK);
            this.Controls.Add(this.labHdgChoosePin);
            this.Controls.Add(this.labTimeHdg);
            this.Controls.Add(this.lblmsg);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bNumber0);
            this.Controls.Add(this.bNumber9);
            this.Controls.Add(this.bNumber8);
            this.Controls.Add(this.bNumber7);
            this.Controls.Add(this.bNumber4);
            this.Controls.Add(this.bNumber5);
            this.Controls.Add(this.bNumber6);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bNumber3);
            this.Controls.Add(this.bNumber2);
            this.Controls.Add(this.bNumber1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PINReg";
            this.Load += new System.EventHandler(this.PINReg_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bNumber1;
        private System.Windows.Forms.Button bNumber2;
        private System.Windows.Forms.Button bNumber3;
        private System.Windows.Forms.Button bNumber6;
        private System.Windows.Forms.Button bNumber5;
        private System.Windows.Forms.Button bNumber4;
        private System.Windows.Forms.Button bNumber7;
        private System.Windows.Forms.Button bNumber8;
        private System.Windows.Forms.Button bNumber9;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Label lblmsg;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Button bNumber0;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timIdle;
        private System.Windows.Forms.Label labTimeHdg;
        private System.Windows.Forms.Label labHdgChoosePin;
        private System.Windows.Forms.Label labHdgPressOK;
        private System.Windows.Forms.Label labRemember1;
        private System.Windows.Forms.Label labCodeLength;
        private System.Windows.Forms.Label labDigitsLong;
        private System.Windows.Forms.Label labRemember2;
        private System.Windows.Forms.Label labRemember3;
        private System.Windows.Forms.Label labYourCode;
        private System.Windows.Forms.Label labOK;
        private System.Windows.Forms.Label labBack;
        private System.Windows.Forms.Button bGhost;
    }
}