﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace Lockers
{
    public partial class OpenLocker : Form
    {
        DateTimeFormatInfo fi = new DateTimeFormatInfo();

//        private Button prevBtn;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void ResetBackground()
        {
        //            String ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\Re-enter_Locker_1182x788.tiff";
//            String ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\Blank_1182x789.tiff";
//            this.BackgroundImage = Image.FromFile(ImageFilename);

        }

        private void btnLanguage_Click(object sender, EventArgs e)
        {

            Button btn = (Button)sender;
            Globals.LanguageCode = Convert.ToInt32(btn.Tag);
            //================reset font ========================
            Globals.prevBtn.Font = new Font
         (
            btn.Font,
            FontStyle.Regular
         );
            //======================================

            btn.Font = new Font
         (
            btn.Font,
            FontStyle.Underline | FontStyle.Bold
         );
            ResetBackground();
            Globals.prevBtn = btn;

        }

        public OpenLocker()
        {
            InitializeComponent();

            ResetBackground();

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timRTC.Start();
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
            timIdle.Start();
        }

        private void bBack_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            timRTC.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void OpenLocker_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            lbltime.ForeColor = Properties.Settings.Default.TextColor;
            labComing.ForeColor = Properties.Settings.Default.TextColor;
            label1.ForeColor = Properties.Settings.Default.TextColor;
            label3.ForeColor = Properties.Settings.Default.TextColor;
            label4.ForeColor = Properties.Settings.Default.TextColor;
            labFinished.ForeColor = Properties.Settings.Default.TextColor;
            labBack.ForeColor = Properties.Settings.Default.TextColor;

            string[] LockoutTime = Properties.Settings.Default.EndRentalLockoutStartTime.Split(':');
            System.DateTime DT = System.DateTime.Now;

            if ((DT.Hour >= Convert.ToInt32(LockoutTime[0])) && (DT.Minute >= Convert.ToInt32(LockoutTime[1])))
            {
                bEndRental.Visible = false;
                labFinished.Visible = false;
                label4.Visible = false;
            }

            InvokeLanguage();
        }

        int EndButtonClick = 0;
        private void bEndRental_Click(object sender, EventArgs e)
        {
            if(EndButtonClick == 0)
            {
                String ImageFilename = "";

                /*                if(Properties.Settings.Default.ReleaseType == "Linux")
                                    ImageFilename = Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Globals.CurrentLanguage + "/Re-enter_Confirm_1182x788.tiff";
                                else
                                    ImageFilename = Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Globals.GetLanguage() + "\\Re-enter_Confirm_1182x788.tiff";
                                this.BackgroundImage = Image.FromFile(ImageFilename);
                                bEndRental.BackColor = Color.Transparent;
                                bOpenLocker.BackColor = Color.Transparent;
                */

                labComing.Text = "No, not yet.";
                label3.Text = "Ill keep it";
                labFinished.Text = "Im sure";
                label4.Text = "Im finished";

                this.Update();
                EndButtonClick++;
            }
            else
            {
                Globals.UserProcessType = ProcessType.endrent;
                timRTC.Stop();
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }

        private void bOpenLocker_Click(object sender, EventArgs e)
        {
            Globals.UserProcessType = ProcessType.open;
            timRTC.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            timRTC.Stop();
            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void InvokeLanguage()
        {
            String language = Globals.GetLanguageCode();

            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            ComponentResourceManager resources = new ComponentResourceManager(typeof(OpenLocker));

            foreach (Control C in this.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    foreach (Control D in C.Controls)
                    {
                        resources.ApplyResources(D, D.Name, new CultureInfo(language));
                    }
                }
                else
                {
                    resources.ApplyResources(C, C.Name, new CultureInfo(language));
                }
            }

            this.Update();
        }
    }
}
