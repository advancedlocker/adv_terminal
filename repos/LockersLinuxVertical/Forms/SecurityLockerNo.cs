﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace Lockers
{
    public partial class SecurityLockerNo : Form
    {

        string LockerNo;

        int[] Keymap = new int[10];

        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        public SecurityLockerNo()
        {
            InitializeComponent();

/*
            if (Globals.CurrentLanguage == null)
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\SelectLocker.tiff");
            else
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\SelectLocker.tiff");
*/
            bOK.Visible = false;

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timRTC.Start();
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
            timIdle.Start();


            // Randomise the buttons
            if (Properties.Settings.Default.SecurityModalKeypad)
                RandomiseButtons();

            InvokeLanguage();
        }


        private void RandomiseButtons()
        {
            bOK.Visible = false;

            // Randomise the numbers
            Random randomGen = new Random();
            Boolean RandomisationFinished = false;
            int[] CheckMap = new int[10];

            for (int count = 0; count < 10; count++)
            {
                Keymap[count] = 10;
                CheckMap[count] = 0;
            }

            int randomSeqA = randomGen.Next(0123456789, 2147483647);      // Random 32bit number
            int tmp = randomSeqA;

            for (int count = 0; count < 10; count++)
            {
                int CellValue = randomSeqA % 10;
                randomSeqA /= 10;
                if (CheckMap[CellValue] == 0)
                {
                    CheckMap[CellValue] = 1;
                    Keymap[count] = CellValue;
                }
            }

            RandomisationFinished = true;
            for (int count = 0; count < 10; count++)
            {
                if (CheckMap[count] == 0)
                    RandomisationFinished = false;
            }

            while (RandomisationFinished == false)
            {
                randomSeqA = randomGen.Next(0123456789, 2147483647);      // Random 32bit number
                                                                          //                int randomSeqB = randomGen.Next(2147483647, 2147483647);      // Random 32bit number

                tmp = randomSeqA;

                for (int count = 0; count < 10; count++)
                {
                    int CellValue = randomSeqA % 10;
                    randomSeqA /= 10;
                    if (CheckMap[CellValue] == 0)
                    {
                        CheckMap[CellValue] = 1;
                        for (int j = 0; j < 10; j++)
                        {
                            if (Keymap[j] == 10)
                            {
                                Keymap[j] = CellValue;
                                break;
                            }
                        }
                    }
                }

                // Crawl checkkeymap to add the missing numbers
                RandomisationFinished = true;
                for (int count = 0; count < 10; count++)
                {
                    if (CheckMap[count] == 0)
                        RandomisationFinished = false;
                }
            }

            bNumber0.Text = Keymap[0].ToString();
            bNumber1.Text = Keymap[1].ToString();
            bNumber2.Text = Keymap[2].ToString();
            bNumber3.Text = Keymap[3].ToString();
            bNumber4.Text = Keymap[4].ToString();
            bNumber5.Text = Keymap[5].ToString();
            bNumber6.Text = Keymap[6].ToString();
            bNumber7.Text = Keymap[7].ToString();
            bNumber8.Text = Keymap[8].ToString();
            bNumber9.Text = Keymap[9].ToString();

            this.Update();
        }

        private void bNumeric_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (b.Name == "bClear")
            {
                lbl1.Text = "";
                bOK.Visible = false;
                LockerNo = "";
            }
            else
            {
                if (lbl1.Text.Length < Properties.Settings.Default.MaxUserPinLen)
                {
                    lbl1.Text += b.Text;
                    LockerNo += b.Text;
                    bOK.Visible = true;

                    // TODO : validate the locker number
                    /*
                                        if (m_PIN.Length >= Properties.Settings.Default.MinUserPinLength)
                                            bOK.Visible = true;
                                        else
                                            bOK.Visible = false;
                    */
                }
            }
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            // Get Locker for Pin Code
            string PinCode = Globals.LockData.PINCode_Get();
            string lockers = Globals.LockData.GetLockersForPinCode(PinCode);
            string[] lockersArray = lockers.Split(',');

            Globals.LockData.LockNumber_Set(Convert.ToInt32(LockerNo));

            if (lockers.Contains(LockerNo))
                this.DialogResult = DialogResult.OK;
            else
                this.DialogResult = DialogResult.Cancel;

            timRTC.Stop();
        }

        private void PINReg_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            label1.ForeColor = Properties.Settings.Default.TextColor;
            labEnterLockerNo.ForeColor = Properties.Settings.Default.TextColor;
            labEnterLockerNoB.ForeColor = Properties.Settings.Default.TextColor;
            labTime.ForeColor = Properties.Settings.Default.TextColor;
            labOK.ForeColor = Properties.Settings.Default.TextColor;
            labBack.ForeColor = Properties.Settings.Default.TextColor;
        }

        private void bBack_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            timRTC.Stop();
            this.DialogResult = DialogResult.Cancel;
//            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            timRTC.Stop();
            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void InvokeLanguage()
        {
            String language = Globals.GetLanguageCode();

            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            ComponentResourceManager resources = new ComponentResourceManager(typeof(SecurityLockerNo));

            foreach (Control C in this.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    foreach (Control D in C.Controls)
                    {
                        resources.ApplyResources(D, D.Name, new CultureInfo(language));
                    }
                }
                else
                {
                    resources.ApplyResources(C, C.Name, new CultureInfo(language));
                }
            }

            this.Update();
        }

        private void Lbltime_Click(object sender, EventArgs e)
        {

        }

        private void LabTime_Click(object sender, EventArgs e)
        {

        }
    }
}
