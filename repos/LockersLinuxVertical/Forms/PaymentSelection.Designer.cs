﻿namespace Lockers
{
    partial class PaymentSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaymentSelection));
            this.button2 = new System.Windows.Forms.Button();
            this.bCreditPay = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.bCashPay = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.lbldue = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.labPaymentSelectHeading = new System.Windows.Forms.Label();
            this.labLocalCurrency = new System.Windows.Forms.Label();
            this.labBack = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // bCreditPay
            // 
            this.bCreditPay.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bCreditPay, "bCreditPay");
            this.bCreditPay.FlatAppearance.BorderSize = 0;
            this.bCreditPay.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bCreditPay.Name = "bCreditPay";
            this.bCreditPay.UseVisualStyleBackColor = false;
            this.bCreditPay.Click += new System.EventHandler(this.bCreditPay_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // bCashPay
            // 
            this.bCashPay.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bCashPay, "bCashPay");
            this.bCashPay.FlatAppearance.BorderSize = 0;
            this.bCashPay.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bCashPay.Name = "bCashPay";
            this.bCashPay.UseVisualStyleBackColor = false;
            this.bCashPay.Click += new System.EventHandler(this.bCashPay_Click);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bBack, "bBack");
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.Name = "bBack";
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Transparent;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button11, "button11");
            this.button11.Name = "button11";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Transparent;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button12, "button12");
            this.button12.Name = "button12";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Transparent;
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.button13, "button13");
            this.button13.Name = "button13";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Name = "label1";
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            // 
            // lbldue
            // 
            resources.ApplyResources(this.lbldue, "lbldue");
            this.lbldue.BackColor = System.Drawing.Color.Transparent;
            this.lbldue.ForeColor = System.Drawing.Color.White;
            this.lbldue.Name = "lbldue";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // labPaymentSelectHeading
            // 
            resources.ApplyResources(this.labPaymentSelectHeading, "labPaymentSelectHeading");
            this.labPaymentSelectHeading.BackColor = System.Drawing.Color.Transparent;
            this.labPaymentSelectHeading.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labPaymentSelectHeading.ForeColor = System.Drawing.Color.White;
            this.labPaymentSelectHeading.Name = "labPaymentSelectHeading";
            // 
            // labLocalCurrency
            // 
            resources.ApplyResources(this.labLocalCurrency, "labLocalCurrency");
            this.labLocalCurrency.BackColor = System.Drawing.Color.Transparent;
            this.labLocalCurrency.ForeColor = System.Drawing.Color.White;
            this.labLocalCurrency.Name = "labLocalCurrency";
            // 
            // labBack
            // 
            resources.ApplyResources(this.labBack, "labBack");
            this.labBack.BackColor = System.Drawing.Color.Transparent;
            this.labBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labBack.ForeColor = System.Drawing.Color.White;
            this.labBack.Name = "labBack";
            this.labBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // PaymentSelection
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.labBack);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.labLocalCurrency);
            this.Controls.Add(this.labPaymentSelectHeading);
            this.Controls.Add(this.lbldue);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bCashPay);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.bCreditPay);
            this.Controls.Add(this.button2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PaymentSelection";
            this.Load += new System.EventHandler(this.PaymentSelection_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bCreditPay;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button bCashPay;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label lbldue;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timIdle;
        private System.Windows.Forms.Label labPaymentSelectHeading;
        private System.Windows.Forms.Label labLocalCurrency;
        private System.Windows.Forms.Label labBack;
    }
}