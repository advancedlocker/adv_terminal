﻿namespace Lockers
{
    partial class SetupPaysystemBasic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label11 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtCurrencyCode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnRun = new System.Windows.Forms.Button();
            this.btnHalt = new System.Windows.Forms.Button();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnHopperDisabled = new System.Windows.Forms.Button();
            this.btnNV11Disabled = new System.Windows.Forms.Button();
            this.btnHopperEnabled = new System.Windows.Forms.Button();
            this.btnResetNoteFloat = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.cbChannels = new System.Windows.Forms.ComboBox();
            this.txtValue4 = new System.Windows.Forms.TextBox();
            this.txtValue3 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnDone = new System.Windows.Forms.Button();
            this.lblChan3 = new System.Windows.Forms.Label();
            this.txtValue2 = new System.Windows.Forms.TextBox();
            this.lblChan2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtValue1 = new System.Windows.Forms.TextBox();
            this.lblChan1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnResetHopper = new System.Windows.Forms.Button();
            this.bCashEnable = new System.Windows.Forms.Button();
            this.btnEmptyHopper = new System.Windows.Forms.Button();
            this.btnNV11Enabled = new System.Windows.Forms.Button();
            this.btnNoteFloatStackAll = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.comboHopperComPort = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPayoutNextNote = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.comboNV11ComPort = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbCoinLevels = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSmartEmptyHopper = new System.Windows.Forms.Button();
            this.btnStackNextNote = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.cbRecycleChannelNV11 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbNotesStored = new System.Windows.Forms.TextBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.comboAdvamPort = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.bCreditEnabled = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(727, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(189, 20);
            this.label11.TabIndex = 271;
            this.label11.Text = "Status";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.txtCurrencyCode);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Location = new System.Drawing.Point(709, 512);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(189, 66);
            this.panel4.TabIndex = 270;
            this.panel4.Visible = false;
            // 
            // txtCurrencyCode
            // 
            this.txtCurrencyCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurrencyCode.Location = new System.Drawing.Point(3, 28);
            this.txtCurrencyCode.Name = "txtCurrencyCode";
            this.txtCurrencyCode.Size = new System.Drawing.Size(181, 26);
            this.txtCurrencyCode.TabIndex = 235;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(58, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 16);
            this.label6.TabIndex = 236;
            this.label6.Text = "Currency";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnRun
            // 
            this.btnRun.BackColor = System.Drawing.Color.ForestGreen;
            this.btnRun.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnRun.FlatAppearance.BorderSize = 2;
            this.btnRun.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnRun.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRun.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.ForeColor = System.Drawing.Color.White;
            this.btnRun.Location = new System.Drawing.Point(13, 733);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(224, 36);
            this.btnRun.TabIndex = 268;
            this.btnRun.Text = "&Run Pay system";
            this.btnRun.UseVisualStyleBackColor = false;
            this.btnRun.Visible = false;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // btnHalt
            // 
            this.btnHalt.BackColor = System.Drawing.Color.ForestGreen;
            this.btnHalt.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHalt.FlatAppearance.BorderSize = 2;
            this.btnHalt.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnHalt.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnHalt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHalt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHalt.ForeColor = System.Drawing.Color.White;
            this.btnHalt.Location = new System.Drawing.Point(255, 733);
            this.btnHalt.Name = "btnHalt";
            this.btnHalt.Size = new System.Drawing.Size(401, 36);
            this.btnHalt.TabIndex = 269;
            this.btnHalt.Text = "&Halt";
            this.btnHalt.UseVisualStyleBackColor = false;
            this.btnHalt.Visible = false;
            this.btnHalt.Click += new System.EventHandler(this.btnHalt_Click);
            // 
            // txtStatus
            // 
            this.txtStatus.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatus.Location = new System.Drawing.Point(709, 48);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtStatus.Size = new System.Drawing.Size(307, 455);
            this.txtStatus.TabIndex = 267;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnHopperDisabled);
            this.panel1.Controls.Add(this.btnNV11Disabled);
            this.panel1.Controls.Add(this.btnHopperEnabled);
            this.panel1.Controls.Add(this.btnResetNoteFloat);
            this.panel1.Controls.Add(this.panel7);
            this.panel1.Controls.Add(this.btnResetHopper);
            this.panel1.Controls.Add(this.bCashEnable);
            this.panel1.Controls.Add(this.btnEmptyHopper);
            this.panel1.Controls.Add(this.btnNV11Enabled);
            this.panel1.Controls.Add(this.btnNoteFloatStackAll);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.btnPayoutNextNote);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.tbCoinLevels);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.btnSmartEmptyHopper);
            this.panel1.Controls.Add(this.btnStackNextNote);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.cbRecycleChannelNV11);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.tbNotesStored);
            this.panel1.Location = new System.Drawing.Point(13, 48);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(447, 573);
            this.panel1.TabIndex = 265;
            // 
            // btnHopperDisabled
            // 
            this.btnHopperDisabled.BackColor = System.Drawing.Color.ForestGreen;
            this.btnHopperDisabled.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHopperDisabled.FlatAppearance.BorderSize = 2;
            this.btnHopperDisabled.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnHopperDisabled.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnHopperDisabled.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHopperDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHopperDisabled.ForeColor = System.Drawing.Color.White;
            this.btnHopperDisabled.Location = new System.Drawing.Point(219, 489);
            this.btnHopperDisabled.Name = "btnHopperDisabled";
            this.btnHopperDisabled.Size = new System.Drawing.Size(191, 30);
            this.btnHopperDisabled.TabIndex = 272;
            this.btnHopperDisabled.Text = "Disabled";
            this.btnHopperDisabled.UseVisualStyleBackColor = false;
            this.btnHopperDisabled.Visible = false;
            this.btnHopperDisabled.Click += new System.EventHandler(this.btnHopperDisabled_Click);
            // 
            // btnNV11Disabled
            // 
            this.btnNV11Disabled.BackColor = System.Drawing.Color.ForestGreen;
            this.btnNV11Disabled.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNV11Disabled.FlatAppearance.BorderSize = 2;
            this.btnNV11Disabled.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnNV11Disabled.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnNV11Disabled.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNV11Disabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNV11Disabled.ForeColor = System.Drawing.Color.White;
            this.btnNV11Disabled.Location = new System.Drawing.Point(4, 499);
            this.btnNV11Disabled.Name = "btnNV11Disabled";
            this.btnNV11Disabled.Size = new System.Drawing.Size(195, 30);
            this.btnNV11Disabled.TabIndex = 261;
            this.btnNV11Disabled.Text = "Disabled";
            this.btnNV11Disabled.UseVisualStyleBackColor = false;
            this.btnNV11Disabled.Visible = false;
            this.btnNV11Disabled.Click += new System.EventHandler(this.btnNV11Disabled_Click);
            // 
            // btnHopperEnabled
            // 
            this.btnHopperEnabled.BackColor = System.Drawing.Color.ForestGreen;
            this.btnHopperEnabled.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHopperEnabled.FlatAppearance.BorderSize = 2;
            this.btnHopperEnabled.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnHopperEnabled.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnHopperEnabled.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHopperEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHopperEnabled.ForeColor = System.Drawing.Color.White;
            this.btnHopperEnabled.Location = new System.Drawing.Point(216, 450);
            this.btnHopperEnabled.Name = "btnHopperEnabled";
            this.btnHopperEnabled.Size = new System.Drawing.Size(195, 30);
            this.btnHopperEnabled.TabIndex = 271;
            this.btnHopperEnabled.Text = "Enabled";
            this.btnHopperEnabled.UseVisualStyleBackColor = false;
            this.btnHopperEnabled.Visible = false;
            this.btnHopperEnabled.Click += new System.EventHandler(this.btnHopperEnabled_Click);
            // 
            // btnResetNoteFloat
            // 
            this.btnResetNoteFloat.BackColor = System.Drawing.Color.ForestGreen;
            this.btnResetNoteFloat.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnResetNoteFloat.FlatAppearance.BorderSize = 2;
            this.btnResetNoteFloat.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnResetNoteFloat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnResetNoteFloat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResetNoteFloat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetNoteFloat.ForeColor = System.Drawing.Color.White;
            this.btnResetNoteFloat.Location = new System.Drawing.Point(4, 538);
            this.btnResetNoteFloat.Name = "btnResetNoteFloat";
            this.btnResetNoteFloat.Size = new System.Drawing.Size(195, 30);
            this.btnResetNoteFloat.TabIndex = 260;
            this.btnResetNoteFloat.Text = "Reset Note Accpeter";
            this.btnResetNoteFloat.UseVisualStyleBackColor = false;
            this.btnResetNoteFloat.Click += new System.EventHandler(this.btnResetNoteFloat_Click);
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.cbChannels);
            this.panel7.Controls.Add(this.txtValue4);
            this.panel7.Controls.Add(this.txtValue3);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Controls.Add(this.btnDone);
            this.panel7.Controls.Add(this.lblChan3);
            this.panel7.Controls.Add(this.txtValue2);
            this.panel7.Controls.Add(this.lblChan2);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Controls.Add(this.txtValue1);
            this.panel7.Controls.Add(this.lblChan1);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.label13);
            this.panel7.Location = new System.Drawing.Point(223, 352);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(207, 119);
            this.panel7.TabIndex = 270;
            this.panel7.Visible = false;
            // 
            // cbChannels
            // 
            this.cbChannels.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbChannels.FormattingEnabled = true;
            this.cbChannels.Location = new System.Drawing.Point(7, 47);
            this.cbChannels.Name = "cbChannels";
            this.cbChannels.Size = new System.Drawing.Size(123, 24);
            this.cbChannels.TabIndex = 278;
            // 
            // txtValue4
            // 
            this.txtValue4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValue4.Location = new System.Drawing.Point(95, 139);
            this.txtValue4.Name = "txtValue4";
            this.txtValue4.Size = new System.Drawing.Size(54, 26);
            this.txtValue4.TabIndex = 277;
            this.txtValue4.Visible = false;
            // 
            // txtValue3
            // 
            this.txtValue3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValue3.Location = new System.Drawing.Point(95, 107);
            this.txtValue3.Name = "txtValue3";
            this.txtValue3.Size = new System.Drawing.Size(54, 26);
            this.txtValue3.TabIndex = 277;
            this.txtValue3.Visible = false;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 141);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 17);
            this.label15.TabIndex = 276;
            this.label15.Text = "4";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label15.Visible = false;
            // 
            // btnDone
            // 
            this.btnDone.BackColor = System.Drawing.Color.ForestGreen;
            this.btnDone.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnDone.FlatAppearance.BorderSize = 2;
            this.btnDone.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnDone.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnDone.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDone.ForeColor = System.Drawing.Color.White;
            this.btnDone.Location = new System.Drawing.Point(25, 77);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(152, 30);
            this.btnDone.TabIndex = 269;
            this.btnDone.Text = "Set Level";
            this.btnDone.UseVisualStyleBackColor = false;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // lblChan3
            // 
            this.lblChan3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChan3.Location = new System.Drawing.Point(3, 109);
            this.lblChan3.Name = "lblChan3";
            this.lblChan3.Size = new System.Drawing.Size(86, 17);
            this.lblChan3.TabIndex = 276;
            this.lblChan3.Text = "3";
            this.lblChan3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblChan3.Visible = false;
            // 
            // txtValue2
            // 
            this.txtValue2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValue2.Location = new System.Drawing.Point(95, 75);
            this.txtValue2.Name = "txtValue2";
            this.txtValue2.Size = new System.Drawing.Size(54, 26);
            this.txtValue2.TabIndex = 275;
            this.txtValue2.Visible = false;
            // 
            // lblChan2
            // 
            this.lblChan2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChan2.Location = new System.Drawing.Point(4, 77);
            this.lblChan2.Name = "lblChan2";
            this.lblChan2.Size = new System.Drawing.Size(86, 17);
            this.lblChan2.TabIndex = 274;
            this.lblChan2.Text = "2";
            this.lblChan2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblChan2.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(133, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 17);
            this.label16.TabIndex = 273;
            this.label16.Text = "Value";
            // 
            // txtValue1
            // 
            this.txtValue1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValue1.Location = new System.Drawing.Point(136, 47);
            this.txtValue1.Name = "txtValue1";
            this.txtValue1.Size = new System.Drawing.Size(54, 26);
            this.txtValue1.TabIndex = 272;
            this.txtValue1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.txtValue1_MouseUp);
            // 
            // lblChan1
            // 
            this.lblChan1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChan1.Location = new System.Drawing.Point(3, 48);
            this.lblChan1.Name = "lblChan1";
            this.lblChan1.Size = new System.Drawing.Size(86, 17);
            this.lblChan1.TabIndex = 271;
            this.lblChan1.Text = "1";
            this.lblChan1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblChan1.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 17);
            this.label14.TabIndex = 270;
            this.label14.Text = "Channels";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 17);
            this.label13.TabIndex = 269;
            this.label13.Text = "Set Levels";
            // 
            // btnResetHopper
            // 
            this.btnResetHopper.BackColor = System.Drawing.Color.ForestGreen;
            this.btnResetHopper.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnResetHopper.FlatAppearance.BorderSize = 2;
            this.btnResetHopper.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnResetHopper.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnResetHopper.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResetHopper.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResetHopper.ForeColor = System.Drawing.Color.White;
            this.btnResetHopper.Location = new System.Drawing.Point(219, 525);
            this.btnResetHopper.Name = "btnResetHopper";
            this.btnResetHopper.Size = new System.Drawing.Size(191, 30);
            this.btnResetHopper.TabIndex = 263;
            this.btnResetHopper.Text = "Reset Hopper";
            this.btnResetHopper.UseVisualStyleBackColor = false;
            this.btnResetHopper.Click += new System.EventHandler(this.btnResetHopper_Click);
            // 
            // bCashEnable
            // 
            this.bCashEnable.BackColor = System.Drawing.Color.ForestGreen;
            this.bCashEnable.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.bCashEnable.FlatAppearance.BorderSize = 2;
            this.bCashEnable.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.bCashEnable.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.bCashEnable.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCashEnable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bCashEnable.ForeColor = System.Drawing.Color.White;
            this.bCashEnable.Location = new System.Drawing.Point(123, 25);
            this.bCashEnable.Name = "bCashEnable";
            this.bCashEnable.Size = new System.Drawing.Size(191, 30);
            this.bCashEnable.TabIndex = 274;
            this.bCashEnable.Text = "Enabled";
            this.bCashEnable.UseVisualStyleBackColor = false;
            this.bCashEnable.Click += new System.EventHandler(this.BCashEnable_Click);
            // 
            // btnEmptyHopper
            // 
            this.btnEmptyHopper.BackColor = System.Drawing.Color.ForestGreen;
            this.btnEmptyHopper.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnEmptyHopper.FlatAppearance.BorderSize = 2;
            this.btnEmptyHopper.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnEmptyHopper.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnEmptyHopper.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEmptyHopper.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmptyHopper.ForeColor = System.Drawing.Color.White;
            this.btnEmptyHopper.Location = new System.Drawing.Point(224, 309);
            this.btnEmptyHopper.Name = "btnEmptyHopper";
            this.btnEmptyHopper.Size = new System.Drawing.Size(206, 30);
            this.btnEmptyHopper.TabIndex = 265;
            this.btnEmptyHopper.Text = "Empty All to Cashbox";
            this.btnEmptyHopper.UseVisualStyleBackColor = false;
            this.btnEmptyHopper.Click += new System.EventHandler(this.btnEmptyHopper_Click);
            // 
            // btnNV11Enabled
            // 
            this.btnNV11Enabled.BackColor = System.Drawing.Color.ForestGreen;
            this.btnNV11Enabled.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNV11Enabled.FlatAppearance.BorderSize = 2;
            this.btnNV11Enabled.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnNV11Enabled.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnNV11Enabled.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNV11Enabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNV11Enabled.ForeColor = System.Drawing.Color.White;
            this.btnNV11Enabled.Location = new System.Drawing.Point(4, 463);
            this.btnNV11Enabled.Name = "btnNV11Enabled";
            this.btnNV11Enabled.Size = new System.Drawing.Size(195, 30);
            this.btnNV11Enabled.TabIndex = 259;
            this.btnNV11Enabled.Text = "Enabled";
            this.btnNV11Enabled.UseVisualStyleBackColor = false;
            this.btnNV11Enabled.Visible = false;
            this.btnNV11Enabled.Click += new System.EventHandler(this.btnNV11Enabled_Click);
            // 
            // btnNoteFloatStackAll
            // 
            this.btnNoteFloatStackAll.BackColor = System.Drawing.Color.ForestGreen;
            this.btnNoteFloatStackAll.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNoteFloatStackAll.FlatAppearance.BorderSize = 2;
            this.btnNoteFloatStackAll.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnNoteFloatStackAll.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnNoteFloatStackAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNoteFloatStackAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoteFloatStackAll.ForeColor = System.Drawing.Color.White;
            this.btnNoteFloatStackAll.Location = new System.Drawing.Point(4, 420);
            this.btnNoteFloatStackAll.Name = "btnNoteFloatStackAll";
            this.btnNoteFloatStackAll.Size = new System.Drawing.Size(195, 30);
            this.btnNoteFloatStackAll.TabIndex = 258;
            this.btnNoteFloatStackAll.Text = "Stack All";
            this.btnNoteFloatStackAll.UseVisualStyleBackColor = false;
            this.btnNoteFloatStackAll.Visible = false;
            this.btnNoteFloatStackAll.Click += new System.EventHandler(this.btnNoteFloatStackAll_Click);
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.comboHopperComPort);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Location = new System.Drawing.Point(220, 67);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(214, 57);
            this.panel3.TabIndex = 254;
            // 
            // comboHopperComPort
            // 
            this.comboHopperComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboHopperComPort.FormattingEnabled = true;
            this.comboHopperComPort.Location = new System.Drawing.Point(3, 19);
            this.comboHopperComPort.Name = "comboHopperComPort";
            this.comboHopperComPort.Size = new System.Drawing.Size(190, 28);
            this.comboHopperComPort.TabIndex = 241;
            this.comboHopperComPort.SelectedIndexChanged += new System.EventHandler(this.ComboHopperComPort_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(7, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 16);
            this.label4.TabIndex = 229;
            this.label4.Text = "Hopper Com Port";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPayoutNextNote
            // 
            this.btnPayoutNextNote.BackColor = System.Drawing.Color.ForestGreen;
            this.btnPayoutNextNote.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnPayoutNextNote.FlatAppearance.BorderSize = 2;
            this.btnPayoutNextNote.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnPayoutNextNote.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnPayoutNextNote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayoutNextNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPayoutNextNote.ForeColor = System.Drawing.Color.White;
            this.btnPayoutNextNote.Location = new System.Drawing.Point(4, 386);
            this.btnPayoutNextNote.Name = "btnPayoutNextNote";
            this.btnPayoutNextNote.Size = new System.Drawing.Size(195, 30);
            this.btnPayoutNextNote.TabIndex = 257;
            this.btnPayoutNextNote.Text = "Payout Next Note";
            this.btnPayoutNextNote.UseVisualStyleBackColor = false;
            this.btnPayoutNextNote.Visible = false;
            this.btnPayoutNextNote.Click += new System.EventHandler(this.btnPayoutNextNote_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(223, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 20);
            this.label10.TabIndex = 6;
            this.label10.Text = "Coil Levels :";
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.comboNV11ComPort);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Location = new System.Drawing.Point(7, 67);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(207, 57);
            this.panel5.TabIndex = 256;
            // 
            // comboNV11ComPort
            // 
            this.comboNV11ComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboNV11ComPort.FormattingEnabled = true;
            this.comboNV11ComPort.Location = new System.Drawing.Point(3, 19);
            this.comboNV11ComPort.Name = "comboNV11ComPort";
            this.comboNV11ComPort.Size = new System.Drawing.Size(190, 28);
            this.comboNV11ComPort.TabIndex = 240;
            this.comboNV11ComPort.SelectedIndexChanged += new System.EventHandler(this.ComboNV11ComPort_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 16);
            this.label1.TabIndex = 225;
            this.label1.Text = "Note Com Port";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbCoinLevels
            // 
            this.tbCoinLevels.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbCoinLevels.Location = new System.Drawing.Point(224, 149);
            this.tbCoinLevels.Multiline = true;
            this.tbCoinLevels.Name = "tbCoinLevels";
            this.tbCoinLevels.ReadOnly = true;
            this.tbCoinLevels.Size = new System.Drawing.Size(206, 116);
            this.tbCoinLevels.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(166, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 19);
            this.label9.TabIndex = 252;
            this.label9.Text = "Cash System";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSmartEmptyHopper
            // 
            this.btnSmartEmptyHopper.BackColor = System.Drawing.Color.ForestGreen;
            this.btnSmartEmptyHopper.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSmartEmptyHopper.FlatAppearance.BorderSize = 2;
            this.btnSmartEmptyHopper.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnSmartEmptyHopper.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnSmartEmptyHopper.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSmartEmptyHopper.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSmartEmptyHopper.ForeColor = System.Drawing.Color.White;
            this.btnSmartEmptyHopper.Location = new System.Drawing.Point(224, 272);
            this.btnSmartEmptyHopper.Name = "btnSmartEmptyHopper";
            this.btnSmartEmptyHopper.Size = new System.Drawing.Size(206, 30);
            this.btnSmartEmptyHopper.TabIndex = 4;
            this.btnSmartEmptyHopper.Text = "SMART Empty";
            this.btnSmartEmptyHopper.UseVisualStyleBackColor = false;
            this.btnSmartEmptyHopper.Click += new System.EventHandler(this.btnSmartEmptyHopper_Click_1);
            // 
            // btnStackNextNote
            // 
            this.btnStackNextNote.BackColor = System.Drawing.Color.ForestGreen;
            this.btnStackNextNote.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnStackNextNote.FlatAppearance.BorderSize = 2;
            this.btnStackNextNote.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.btnStackNextNote.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.btnStackNextNote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStackNextNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStackNextNote.ForeColor = System.Drawing.Color.White;
            this.btnStackNextNote.Location = new System.Drawing.Point(4, 352);
            this.btnStackNextNote.Name = "btnStackNextNote";
            this.btnStackNextNote.Size = new System.Drawing.Size(195, 30);
            this.btnStackNextNote.TabIndex = 250;
            this.btnStackNextNote.Text = "Stack Next Note";
            this.btnStackNextNote.UseVisualStyleBackColor = false;
            this.btnStackNextNote.Visible = false;
            this.btnStackNextNote.Click += new System.EventHandler(this.btnStackNextNote_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 293);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 20);
            this.label7.TabIndex = 249;
            this.label7.Text = "Note to Recycle:";
            this.label7.Visible = false;
            // 
            // cbRecycleChannelNV11
            // 
            this.cbRecycleChannelNV11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRecycleChannelNV11.FormattingEnabled = true;
            this.cbRecycleChannelNV11.Location = new System.Drawing.Point(7, 318);
            this.cbRecycleChannelNV11.Name = "cbRecycleChannelNV11";
            this.cbRecycleChannelNV11.Size = new System.Drawing.Size(192, 28);
            this.cbRecycleChannelNV11.TabIndex = 247;
            this.cbRecycleChannelNV11.Visible = false;
            this.cbRecycleChannelNV11.SelectedIndexChanged += new System.EventHandler(this.cbRecycleChannelNV11_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 20);
            this.label8.TabIndex = 248;
            this.label8.Text = "Note Levels:";
            // 
            // tbNotesStored
            // 
            this.tbNotesStored.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbNotesStored.Location = new System.Drawing.Point(8, 150);
            this.tbNotesStored.Multiline = true;
            this.tbNotesStored.Name = "tbNotesStored";
            this.tbNotesStored.ReadOnly = true;
            this.tbNotesStored.Size = new System.Drawing.Size(206, 116);
            this.tbNotesStored.TabIndex = 246;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.BackColor = System.Drawing.Color.Transparent;
            this.titleLabel.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.ForeColor = System.Drawing.Color.Black;
            this.titleLabel.Location = new System.Drawing.Point(302, 4);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(319, 41);
            this.titleLabel.TabIndex = 264;
            this.titleLabel.Text = "Pay System Setup";
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(760, 619);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(123, 44);
            this.btnSave.TabIndex = 272;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSavePrice_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(889, 619);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(123, 44);
            this.btnClose.TabIndex = 272;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.panel9);
            this.panel6.Controls.Add(this.bCreditEnabled);
            this.panel6.Controls.Add(this.label22);
            this.panel6.Controls.Add(this.label23);
            this.panel6.Controls.Add(this.textBox5);
            this.panel6.Location = new System.Drawing.Point(466, 48);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(237, 574);
            this.panel6.TabIndex = 273;
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel9.Controls.Add(this.comboAdvamPort);
            this.panel9.Controls.Add(this.label21);
            this.panel9.Location = new System.Drawing.Point(3, 67);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(214, 57);
            this.panel9.TabIndex = 254;
            // 
            // comboAdvamPort
            // 
            this.comboAdvamPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboAdvamPort.FormattingEnabled = true;
            this.comboAdvamPort.Location = new System.Drawing.Point(3, 19);
            this.comboAdvamPort.Name = "comboAdvamPort";
            this.comboAdvamPort.Size = new System.Drawing.Size(190, 28);
            this.comboAdvamPort.TabIndex = 241;
            this.comboAdvamPort.SelectedIndexChanged += new System.EventHandler(this.ComboAdvamPort_SelectedIndexChanged);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(7, 1);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(123, 16);
            this.label21.TabIndex = 229;
            this.label21.Text = "Advam Com Port";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bCreditEnabled
            // 
            this.bCreditEnabled.BackColor = System.Drawing.Color.ForestGreen;
            this.bCreditEnabled.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.bCreditEnabled.FlatAppearance.BorderSize = 2;
            this.bCreditEnabled.FlatAppearance.MouseDownBackColor = System.Drawing.Color.HotPink;
            this.bCreditEnabled.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PaleVioletRed;
            this.bCreditEnabled.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCreditEnabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bCreditEnabled.ForeColor = System.Drawing.Color.White;
            this.bCreditEnabled.Location = new System.Drawing.Point(15, 25);
            this.bCreditEnabled.Name = "bCreditEnabled";
            this.bCreditEnabled.Size = new System.Drawing.Size(190, 30);
            this.bCreditEnabled.TabIndex = 271;
            this.bCreditEnabled.Text = "Enabled";
            this.bCreditEnabled.UseVisualStyleBackColor = false;
            this.bCreditEnabled.Click += new System.EventHandler(this.BCreditEnabled_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(46, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(116, 19);
            this.label22.TabIndex = 253;
            this.label22.Text = "Credit System";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(6, 126);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(127, 20);
            this.label23.TabIndex = 6;
            this.label23.Text = "Last Transaction";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox5.Location = new System.Drawing.Point(7, 149);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(206, 335);
            this.textBox5.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(163, 633);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 17);
            this.label3.TabIndex = 276;
            this.label3.Text = "Cash System Enabled";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(489, 633);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(151, 17);
            this.label5.TabIndex = 277;
            this.label5.Text = "Credit System Enabled";
            // 
            // SetupPaysystemBasic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.btnHalt);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.titleLabel);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SetupPaysystemBasic";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.SetupPaysystemBasic_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtCurrencyCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Button btnHalt;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnNV11Disabled;
        private System.Windows.Forms.Button btnResetNoteFloat;
        private System.Windows.Forms.Button btnNV11Enabled;
        private System.Windows.Forms.Button btnNoteFloatStackAll;
        private System.Windows.Forms.Button btnPayoutNextNote;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox comboNV11ComPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnStackNextNote;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbRecycleChannelNV11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbNotesStored;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSmartEmptyHopper;
        private System.Windows.Forms.TextBox tbCoinLevels;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox comboHopperComPort;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnResetHopper;
        private System.Windows.Forms.Button btnEmptyHopper;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.ComboBox cbChannels;
        private System.Windows.Forms.TextBox txtValue4;
        private System.Windows.Forms.TextBox txtValue3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Label lblChan3;
        private System.Windows.Forms.TextBox txtValue2;
        private System.Windows.Forms.Label lblChan2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtValue1;
        private System.Windows.Forms.Label lblChan1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnHopperEnabled;
        private System.Windows.Forms.Button btnHopperDisabled;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button bCreditEnabled;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.ComboBox comboAdvamPort;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button bCashEnable;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
    }
}

