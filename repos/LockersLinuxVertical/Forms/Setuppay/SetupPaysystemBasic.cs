﻿using System;
using System.Drawing;
using System.IO.Ports;
using System.Windows.Forms;

namespace Lockers
{
    public partial class SetupPaysystemBasic : Form
    {
        string[] comPorts;
        //bool FormSetup = false;
        private SmartPay smartpaysetup;
        delegate void OutputMessage(string s);

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
// TODO : Fix this.
/*
            this.smartpaysetup.VALIDATOR.LogEventMessage -= this.Smartpay_LogMessage;
            this.smartpaysetup.VALIDATOR.ValidatorEnable -= this.Smartpay_ValidatorEnable;

            this.smartpaysetup.HOPPER.LogEventMessage -= this.Smartpay_LogMessage;

            this.smartpaysetup.LogMessageEvent -= this.Smartpay_LogMessage;
            this.smartpaysetup.NVConnected -= this.Smartpay_NVConnected;
            this.smartpaysetup.DeviceInit -= this.Smartpay_DeviceInit;
            this.smartpaysetup.Update_UI -= this.Smartpay_UpdateUI;

            this.smartpaysetup.VALIDATOR.DisableValidator();
            CHelpers.Pause(300);
            this.smartpaysetup.HOPPER.DisableValidator();
            CHelpers.Pause(200);
*/
        }

        public SetupPaysystemBasic(SmartPay smartpaysetup)
        {
            InitializeComponent();
            Globals.Maintenance = true;

            //this.smartpaysetup = new SmartPay(textBox1);
            this.smartpaysetup = smartpaysetup;

            // TODO : Fix this.
            /*
                        this.smartpaysetup.VALIDATOR.LogEventMessage += this.Smartpay_LogMessage;
                        this.smartpaysetup.VALIDATOR.ValidatorEnable += this.Smartpay_ValidatorEnable;

                        this.smartpaysetup.HOPPER.LogEventMessage += this.Smartpay_LogMessage;

                        this.smartpaysetup.LogMessageEvent += this.Smartpay_LogMessage;
                        this.smartpaysetup.NVConnected += this.Smartpay_NVConnected;
                        this.smartpaysetup.DeviceInit += this.Smartpay_DeviceInit;
                        this.smartpaysetup.Update_UI += this.Smartpay_UpdateUI;
            */
            if (Properties.Settings.Default.ReleaseType == "Linux")
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Properties.Settings.Default.DefaultLanguage + "/Blank_1182x789.tiff");
            else
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Properties.Settings.Default.DefaultLanguage + "\\Blank_1182x789.tiff");

            
            //SearchUSB();
            if (SearchComPorts() > 0)
            {
                //NV11 combobox
                comboNV11ComPort.Items.AddRange(comPorts);
                comboNV11ComPort.Text = Properties.Settings.Default.NV11ComPort;

                //Hopper combobox
                comboHopperComPort.Items.AddRange(comPorts);
                comboHopperComPort.Text = Properties.Settings.Default.HopperComPortWindows;
            }
            else
            {
                MessageBox.Show("No Comport found.");
                comboNV11ComPort.Text = Properties.Settings.Default.NV11ComPort;
                comboHopperComPort.Text = Properties.Settings.Default.HopperComPortWindows;

            }
            //timer1.Interval = pollTimer;
            this.Location = new Point(Screen.PrimaryScreen.Bounds.X, Screen.PrimaryScreen.Bounds.Y);
            btnHalt.Enabled = false;

            //this.smartpaysetup.CurrencyCode = Properties.Settings.Default.CurrencyCode;
            //this.smartpaysetup.Channel = Properties.Settings.Default.NoteRecycle;
            //this.smartpaysetup.Update_UI += this.Smartpay_UpdateUI;
            //this.smartpaysetup.DeviceOn += this.Smartpay_DiviceOn;
            //this.smartpaysetup.NV11Reconnect += this.Smartpay_NVReconnect;

// TODO : Fix this.
/*
            this.Setup_RecycleChannel();
            this.HopperChannel_combo();
            this.Recycle_cbo();

            this.smartpaysetup.VALIDATOR.EnableValidator();
            this.smartpaysetup.HOPPER.EnableValidator();
*/
            Globals.OnpaySetup = true;
        }

        #region events
        private void Smartpay_DeviceInit(object sender, EventArgs e)
        {
            this.Setup_RecycleChannel();
            this.HopperChannel_combo();
            this.Recycle_cbo();

        }

        private void Smartpay_NVConnected(object sender, EventArgs e)
        {
            this.smartpaysetup.VALIDATOR.EnableValidator();
        }

        private void Smartpay_ValidatorEnable(object sender, EventArgs e)
        {
            this.smartpaysetup.HOPPER.EnableValidator();
        }

        private void Smartpay_LogMessage(Object sender, LogMessageEventArgs e)
        {
            try
            {
                //if (!IsHandleCreated)
                //    this.CreateControl();

                //if (InvokeRequired)
                //{
                //this.Invoke(new Action(() =>
                //{
                //    txtStatus.AppendText(e.Messsage);
                //}));

                if (IsHandleCreated)
                {
                    this.Invoke((MethodInvoker)delegate
                    {
                        txtStatus.AppendText(e.Messsage);

                    });

                }
                    //}
                    //else { txtStatus.AppendText(e.Messsage); }
                }
            catch { }
        }

        void UpdateUI()
        {
            // Get stored notes info from NV11
            tbNotesStored.Text = this.smartpaysetup.NoteStoredInfo;

            // Get channel info from hopper
            tbCoinLevels.Text = this.smartpaysetup.CoinLevel;
        }

        // This is a one off function that is called the first time the MainLoop()
        // function runs, it just sets up a few of the UI elements that only need
        // updating once.
        private void SetupFormLayout()
        {
            // Note float UI setup

            // Find number and value of channels in NV11 and update combo box
            cbRecycleChannelNV11.Items.Add("No recycling");
            foreach (ChannelData d in this.smartpaysetup.VALIDATOR.UnitDataList)
            {
                cbRecycleChannelNV11.Items.Add(d.Value / 100 + " " + new String(d.Currency));
            }
            cbRecycleChannelNV11.SelectedIndex = 1;

            // Get channel levels in hopper
            tbCoinLevels.Text = this.smartpaysetup.CoinLevel;   // Hopper.GetChannelLevelInfo();

            //// setup list of recyclable channel tick boxes
            //int x = 0, y = 0;
            //x = 465;
            //y = 30;

            //Label lbl = new Label();
            //lbl.Location = new Point(x, y);
            //lbl.Size = new Size(70, 35);
            //lbl.Name = "lbl";
            //lbl.Text = "Recycle\nChannels:";
            //Controls.Add(lbl);

            //y += 20;
            //for (int i = 1; i <= this.smartpay.HOPPER.NumberOfChannels; i++)
            //{
            //    CheckBox c = new CheckBox();
            //    c.Location = new Point(x, y + (i * 20));
            //    c.Name = i.ToString();
            //    c.Text = CHelpers.FormatToCurrency(this.smartpay.HOPPER.GetChannelValue(i)) + " " + new String(this.smartpay.HOPPER.GetChannelCurrency(i));
            //    c.Checked = this.smartpay.HOPPER.IsChannelRecycling(i);
            //    c.CheckedChanged += new EventHandler(recycleBox_CheckedChange);
            //    Controls.Add(c);
            //}
        }

        #endregion
        public int SearchComPorts()
        {

            comPorts = SerialPort.GetPortNames();
            return comPorts.Length;
        }


        private void LoadPaySystemSettings()
        {
            // btnSave.Enabled = false;
            comboNV11ComPort.Text = Properties.Settings.Default.SmartPayoutPortWindows.ToString();
            comboHopperComPort.Text = Properties.Settings.Default.HopperComPortWindows.ToString();
            comboHopperComPort.Text = Properties.Settings.Default.AdvamReaderPortWindows.ToString();

            //            txtNV11SSPAddress.Text = Properties.Settings.Default.SSP1.ToString();
            //            txtHopperSSPAddress.Text = Properties.Settings.Default.SSP2.ToString();
            //            cbRecycleChannelNV11.Text = Properties.Settings.Default.NoteRecycle.ToString();
            //            txtCurrencyCode.Text = Properties.Settings.Default.CurrencyCode.ToString();
        }

        private void enableSave()
        {
            btnSave.Enabled = true;
            //btnSave.BackColor = Color.Lime;
        }
        private void disableSave()
        {
            btnSave.Enabled = false;
            // btnSave.BackColor = Color.Gainsboro;
        }

        private void textBox_TextChange(object sender, EventArgs e)
        {
            this.btnSave.Enabled = true;
            //this.enableSave();
        }

        private void btnSavePrice_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.NV11ComPort = comboNV11ComPort.Text;
            Properties.Settings.Default.HopperComPortWindows = comboHopperComPort.Text;
//            Properties.Settings.Default.SSP1 = byte.Parse(txtNV11SSPAddress.Text);
//            Properties.Settings.Default.SSP2 = byte.Parse(txtHopperSSPAddress.Text);
//            Properties.Settings.Default.NoteRecycle = cbRecycleChannelNV11.Text;
            Properties.Settings.Default.CurrencyCode = txtCurrencyCode.Text;
            Properties.Settings.Default.Save();
            disableSave();
        }

        //private void btnShowKeyboard_Click(object sender, EventArgs e)
        //{
        //    var virtualKeyboard = new System.Diagnostics.Process();
        //    virtualKeyboard = System.Diagnostics.Process.Start("osk.exe"); // open
        //}
        //#region nv11 buttons
        private void btnRun_Click(object sender, EventArgs e)
        {

            try
            {
                btnRun.Enabled = false;
                btnHalt.Enabled = true;

                Globals.Maintenance = true;

                tbCoinLevels.Text = this.smartpaysetup.CoinLevel;
                tbNotesStored.Text = this.smartpaysetup.NoteStoredInfo;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Setup_RecycleChannel()
        {
            int x = 0, y = 0;
            x = 3;
            y = -14;
            for (int i = 1; i <= smartpaysetup.HOPPER.NumberOfChannels; i++)
            {
                CheckBox c = new CheckBox();
                c.Location = new Point(x, y + (i * 20));
                c.Name = i.ToString();
                c.Text = CHelpers.FormatToCurrency(smartpaysetup.HOPPER.GetChannelValue(i)) + " " + new String(smartpaysetup.HOPPER.GetChannelCurrency(i));

                c.Checked = smartpaysetup.HOPPER.IsChannelRecycling(i);
                c.CheckedChanged += new EventHandler(recycleBox_CheckedChange);
                if (InvokeRequired)
                {
                    Invoke(new Action(() =>
                    {
                        panel6.Controls.Add(c);
                    }
                                        ));
                }
                else
                {
                    panel6.Controls.Add(c);
                }

                //Controls.Add(c);
            }

        }

        private void HopperChannel_combo()
        {


            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    cbChannels.Items.Clear();
                }
                                    ));
            }
            else
            {
                cbChannels.Items.Clear();
            }
            for (int i = 0; i < smartpaysetup.HOPPER.NumberOfChannels; i++)
            {
                if (InvokeRequired)
                {
                    Invoke(new Action(() =>
                    {
                        cbChannels.Items.Add("Channel " + (i + 1).ToString());
                    }
                                        ));
                }
                else
                {
                    cbChannels.Items.Add("Channel " + (i + 1).ToString());
                }

            }
        }


        private void Recycle_cbo()
        {

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    cbRecycleChannelNV11.Items.Clear();
                    cbRecycleChannelNV11.Items.Add("No recycling");
                }
                                    ));
            }
            else
            {
                cbRecycleChannelNV11.Items.Clear();
                cbRecycleChannelNV11.Items.Add("No recycling");
            }
            foreach (ChannelData d in smartpaysetup.VALIDATOR.UnitDataList)
            {
                if (InvokeRequired)
                {
                    Invoke(new Action(() =>
                    {
                        cbRecycleChannelNV11.Items.Add(d.Value / 100 + " " + new String(d.Currency));
                    }
                                        ));
                }
                else
                {
                    cbRecycleChannelNV11.Items.Add(d.Value / 100 + " " + new String(d.Currency));
                }

            }

            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    comboNV11ComPort.Text = Properties.Settings.Default.NV11ComPort;
                }
                                    ));
            }
            else
            {
                comboNV11ComPort.Text = Properties.Settings.Default.NV11ComPort;
            }
        }

        private void Smartpay_UpdateUI(object sender, EventArgs e)
        {
            try
            {
                if (InvokeRequired)
                {
                    Invoke(new Action(() =>
                    {
                        tbCoinLevels.Text = this.smartpaysetup.CoinLevel;
                        tbNotesStored.Text = this.smartpaysetup.NoteStoredInfo;
                    }));
                }
                else
                {
                    tbCoinLevels.Text = this.smartpaysetup.CoinLevel;
                    tbNotesStored.Text = this.smartpaysetup.NoteStoredInfo;
                }
            }
            catch { };
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //timer1.Enabled = false;
        }

        private void btnHalt_Click(object sender, EventArgs e)
        {
            //textBox1.AppendText("Poll loop stopped\r\n");
            //hopperRunning = false;
            //NV11Running = false;
            //btnRun.Enabled = true;
            this.smartpaysetup.StopPaysystem();
            btnHalt.Enabled = false;
            btnRun.Enabled = true;
        }
        private void recycleBox_CheckedChange(object sender, EventArgs e)
        {
            CheckBox c;
            if (sender is CheckBox)
            {
                c = sender as CheckBox;
                try
                {
                    int n = Int32.Parse(c.Name);
                    if (c.Checked)
                        this.smartpaysetup.HOPPER.RouteChannelToStorage(n);
                    else
                        this.smartpaysetup.HOPPER.RouteChannelToCashbox(n);
                }
                catch
                {
                    return;
                }
            }
        }
        private void cbRecycleChannelNV11_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnSave.Enabled = true;

            if (cbRecycleChannelNV11.Text == "No recycling")
            {
                // switch all notes to stacking by disabling the payout
                //this.smartpaysetup.EnableNoteValidator(0);
                this.smartpaysetup.VALIDATOR.DisablePayout();
            }
            else
            {
                // route all to stack to begin with
                this.smartpaysetup.VALIDATOR.RouteAllToStack();

                // switch selected note to recycle after enabling payout
                this.smartpaysetup.VALIDATOR.EnablePayout();
                string name = cbRecycleChannelNV11.Items[cbRecycleChannelNV11.SelectedIndex].ToString();
                string[] sArr = name.Split(' ');
                try
                {
                    this.smartpaysetup.VALIDATOR.ChangeNoteRoute(Int32.Parse(sArr[0]) * 100, sArr[1].ToCharArray(), false);
                }
                catch (Exception ex)
                {
                    Globals g = new Globals();
                    g.LogFile("ChangeNoteRoute:" + ex.ToString(), "PaysystemLog.txt");
                    //MessageBox.Show(ex.ToString());
                    return;
                }
            }
        }

        private void btnStackNextNote_Click(object sender, EventArgs e)
        {
            this.smartpaysetup.VALIDATOR.StackNextNote();
        }

        private void btnSmartEmptyHopper_Click_1(object sender, EventArgs e)
        {
            this.smartpaysetup.HOPPER.SmartEmpty();
        }
        private void reconnectionTimer_Tick(object sender, EventArgs e)
        {
            if (sender is Timer)
            {
                Timer t = sender as Timer;
                t.Enabled = false;
            }
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            Globals.Maintenance = false;
            //this.timer2.Enabled = false;

            this.Dispose();
        }

        private void btnEmptyHopper_Click(object sender, EventArgs e)
        {
            this.smartpaysetup.HOPPER.EmptyDevice();
        }

        private void btnNoteFloatStackAll_Click(object sender, EventArgs e)
        {
            this.smartpaysetup.VALIDATOR.EmptyPayoutDevice();
        }

        private void btnPayoutNextNote_Click(object sender, EventArgs e)
        {
            this.smartpaysetup.VALIDATOR.PayoutNextNote();
        }

        private void btnResetNoteFloat_Click(object sender, EventArgs e)
        {
            this.txtStatus.Clear();
            this.smartpaysetup.VALIDATOR.Reset();
        }

        private void btnResetHopper_Click(object sender, EventArgs e)
        {
            this.smartpaysetup.HOPPER.Reset();
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtValue1.Text.Trim() != "")
                {
                    this.smartpaysetup.HOPPER.SetCoinLevelsByChannel(cbChannels.SelectedIndex + 1, 0);
                    System.Threading.Thread.Sleep(600);
                    this.smartpaysetup.HOPPER.SetCoinLevelsByChannel(cbChannels.SelectedIndex + 1, Int16.Parse(txtValue1.Text));
                }

                this.smartpaysetup.HOPPER.Reset();

                txtValue1.Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "EXCEPTION");
                return;
            }
        }

        private void btnNV11Enabled_Click(object sender, EventArgs e)
        {
            this.smartpaysetup.VALIDATOR.EnableValidator();
        }

        private void btnNV11Disabled_Click(object sender, EventArgs e)
        {
            this.smartpaysetup.VALIDATOR.DisableValidator();
        }

        private void btnHopperEnabled_Click(object sender, EventArgs e)
        {
            this.smartpaysetup.HOPPER.EnableValidator();
        }

        private void btnHopperDisabled_Click(object sender, EventArgs e)
        {
            this.smartpaysetup.HOPPER.DisableValidator();
        }

        //private void timer2_Tick(object sender, EventArgs e)
        //{
        //    this.UpdateUI();
        //}

        private void SetupPaySystemBasic_Shown(object sender, EventArgs e)
        {

            //this.timer2.Enabled = true;
        }

        private void txtValue1_MouseUp(object sender, MouseEventArgs e)
        {
            NumericKeypad kpad = new NumericKeypad();
            kpad.Data = txtValue1.Text;
            DialogResult dr = kpad.ShowDialog();
            if (dr == DialogResult.OK)
            {
                txtValue1.Text = kpad.Data;
            }

            kpad.Dispose();
            kpad = null;
        }

        private void txtHopperSSPAddress_MouseUp(object sender, MouseEventArgs e)
        {
            NumericKeypad kpad = new NumericKeypad();
//            kpad.Data = txtHopperSSPAddress.Text;
            DialogResult dr = kpad.ShowDialog();
            if (dr == DialogResult.OK)
            {
//                txtHopperSSPAddress.Text = kpad.Data;
            }

            kpad.Dispose();
            kpad = null;
        }

        private void txtNV11SSPAddress_MouseUp(object sender, MouseEventArgs e)
        {
            NumericKeypad kpad = new NumericKeypad();
//            kpad.Data = txtNV11SSPAddress.Text;
            DialogResult dr = kpad.ShowDialog();
            if (dr == DialogResult.OK)
            {
//                txtNV11SSPAddress.Text = kpad.Data;
            }

            kpad.Dispose();
            kpad = null;
        }

        private void SetupPaysystemBasic_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
            LoadPaySystemSettings();

            if (Properties.Settings.Default.PaymentEnableCredit == false)
            {
                bCreditEnabled.Text = "Disabled";
                bCreditEnabled.BackColor = Color.DarkRed;
            }
            else
            {
                bCreditEnabled.Text = "Enabled";
                bCreditEnabled.BackColor = Color.DarkGreen;
            }

            if (Properties.Settings.Default.PaymentEnableCash == false)
            {
                bCashEnable.Text = "Disabled";
                bCashEnable.BackColor = Color.DarkRed;
            }
            else
            {
                bCashEnable.Text = "Enabled";
                bCashEnable.BackColor = Color.DarkGreen;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Globals.Maintenance = false;


            this.Dispose();
        }

        private void ComboNV11ComPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboNV11ComPort.SelectedText != "")
            {
                if (Properties.Settings.Default.ReleaseType == "linux")
                    Properties.Settings.Default.SmartPayoutPortLinux = comboNV11ComPort.SelectedText;
                else
                    Properties.Settings.Default.SmartPayoutPortWindows = comboNV11ComPort.SelectedText;
            }
        }

        private void ComboHopperComPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboHopperComPort.SelectedText != "")
            {
                if(Properties.Settings.Default.ReleaseType == "linux")
                    Properties.Settings.Default.HopperComPortLinux = comboHopperComPort.SelectedText;
                else
                    Properties.Settings.Default.HopperComPortWindows = comboHopperComPort.SelectedText;
            }
        }

        private void ComboAdvamPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboAdvamPort.SelectedText != "")
            {
                if(Properties.Settings.Default.ReleaseType == "Linux")
                    Properties.Settings.Default.AdvamReaderPortLinux = comboAdvamPort.SelectedText;
                else
                    Properties.Settings.Default.AdvamReaderPortWindows = comboAdvamPort.SelectedText;
            }
        }

        private void BCreditEnabled_Click(object sender, EventArgs e)
        {
            if(Properties.Settings.Default.PaymentEnableCredit == false)
            {
                Properties.Settings.Default.PaymentEnableCredit = true;
                bCreditEnabled.Text = "Enabled";
                bCreditEnabled.BackColor = Color.DarkGreen;
            }
            else
            {
                Properties.Settings.Default.PaymentEnableCredit = false;
                bCreditEnabled.Text = "Disabled";
                bCreditEnabled.BackColor = Color.DarkRed;
            }
        }

        private void BCashEnable_Click(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.PaymentEnableCash == false)
            {
                Properties.Settings.Default.PaymentEnableCash = true;
                bCashEnable.Text = "Enabled";
                bCashEnable.BackColor = Color.DarkGreen;
            }
            else
            {
                Properties.Settings.Default.PaymentEnableCash = false;
                bCashEnable.Text = "Disabled";
                bCashEnable.BackColor = Color.DarkRed;
            }
        }
    }
}
