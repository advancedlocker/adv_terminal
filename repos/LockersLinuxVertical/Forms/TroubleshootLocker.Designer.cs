﻿namespace Lockers
{
    partial class TroubleshootLocker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TroubleshootLocker));
            this.panel1 = new System.Windows.Forms.Panel();
            this.button9 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.bUnlock = new System.Windows.Forms.Button();
            this.bUpdateCode = new System.Windows.Forms.Button();
            this.bDeactivate = new System.Windows.Forms.Button();
            this.bFreeHire = new System.Windows.Forms.Button();
            this.labUsed = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button18 = new System.Windows.Forms.Button();
            this.bLock = new System.Windows.Forms.Button();
            this.tboxAccessCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bSwapLocker = new System.Windows.Forms.Button();
            this.labFree = new System.Windows.Forms.Label();
            this.cboxCurrentLocker = new System.Windows.Forms.ComboBox();
            this.cboxNewLocker = new System.Windows.Forms.ComboBox();
            this.bSmall = new System.Windows.Forms.Button();
            this.bMedium = new System.Windows.Forms.Button();
            this.bXLarge = new System.Windows.Forms.Button();
            this.bLarge = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.labHiredLockers = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labTotalLockersA = new System.Windows.Forms.Label();
            this.labTotalLockersB = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labDoorFaulty = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.bClearAllHires = new System.Windows.Forms.Button();
            this.bLockActivate = new System.Windows.Forms.Button();
            this.cbIncludeDisabled = new System.Windows.Forms.CheckBox();
            this.bTaskBarToggle = new System.Windows.Forms.Button();
            this.tboxUsed = new System.Windows.Forms.TextBox();
            this.tboxFree = new System.Windows.Forms.TextBox();
            this.bTaskShow = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button17);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button11);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Location = new System.Drawing.Point(370, 258);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(320, 256);
            this.panel1.TabIndex = 0;
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(215, 127);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(100, 56);
            this.button9.TabIndex = 14;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(109, 193);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(100, 56);
            this.button13.TabIndex = 15;
            this.button13.Text = "0";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Location = new System.Drawing.Point(109, 127);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(100, 56);
            this.button8.TabIndex = 16;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(3, 127);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(100, 56);
            this.button7.TabIndex = 17;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(3, 65);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(100, 56);
            this.button6.TabIndex = 18;
            this.button6.Text = "4";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // button17
            // 
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.Location = new System.Drawing.Point(3, 193);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(100, 56);
            this.button17.TabIndex = 4;
            this.button17.Text = "APPLY";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Visible = false;
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(109, 65);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(100, 56);
            this.button5.TabIndex = 19;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(215, 65);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(100, 56);
            this.button4.TabIndex = 20;
            this.button4.Text = "6";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.button11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Image = ((System.Drawing.Image)(resources.GetObject("button11.Image")));
            this.button11.Location = new System.Drawing.Point(215, 193);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(100, 56);
            this.button11.TabIndex = 21;
            this.button11.Tag = "0";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(215, 3);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 56);
            this.button3.TabIndex = 22;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(109, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 56);
            this.button2.TabIndex = 23;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 56);
            this.button1.TabIndex = 24;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.bbtnNum_Click);
            // 
            // bUnlock
            // 
            this.bUnlock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bUnlock.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bUnlock.Location = new System.Drawing.Point(115, 139);
            this.bUnlock.Name = "bUnlock";
            this.bUnlock.Size = new System.Drawing.Size(75, 45);
            this.bUnlock.TabIndex = 1;
            this.bUnlock.Text = "Unlock";
            this.bUnlock.UseVisualStyleBackColor = true;
            this.bUnlock.Click += new System.EventHandler(this.bUnlock_Click);
            // 
            // bUpdateCode
            // 
            this.bUpdateCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bUpdateCode.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bUpdateCode.Location = new System.Drawing.Point(196, 139);
            this.bUpdateCode.Name = "bUpdateCode";
            this.bUpdateCode.Size = new System.Drawing.Size(75, 45);
            this.bUpdateCode.TabIndex = 2;
            this.bUpdateCode.Text = "Update Code";
            this.bUpdateCode.UseVisualStyleBackColor = true;
            this.bUpdateCode.Click += new System.EventHandler(this.bUpdateCode_Click);
            // 
            // bDeactivate
            // 
            this.bDeactivate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bDeactivate.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bDeactivate.Location = new System.Drawing.Point(112, 190);
            this.bDeactivate.Name = "bDeactivate";
            this.bDeactivate.Size = new System.Drawing.Size(75, 45);
            this.bDeactivate.TabIndex = 3;
            this.bDeactivate.Text = "Deactivate  Locker";
            this.bDeactivate.UseVisualStyleBackColor = true;
            this.bDeactivate.Click += new System.EventHandler(this.bDeactivate_Click);
            // 
            // bFreeHire
            // 
            this.bFreeHire.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bFreeHire.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bFreeHire.Location = new System.Drawing.Point(112, 241);
            this.bFreeHire.Name = "bFreeHire";
            this.bFreeHire.Size = new System.Drawing.Size(75, 45);
            this.bFreeHire.TabIndex = 4;
            this.bFreeHire.Text = "Free Hire";
            this.bFreeHire.UseVisualStyleBackColor = true;
            this.bFreeHire.Click += new System.EventHandler(this.bFreeHire_Click);
            // 
            // labUsed
            // 
            this.labUsed.AutoSize = true;
            this.labUsed.BackColor = System.Drawing.Color.Transparent;
            this.labUsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            this.labUsed.Location = new System.Drawing.Point(359, 89);
            this.labUsed.Name = "labUsed";
            this.labUsed.Size = new System.Drawing.Size(183, 36);
            this.labUsed.TabIndex = 7;
            this.labUsed.Text = "Used Locker";
            this.labUsed.Click += new System.EventHandler(this.Label1_Click);
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.Transparent;
            this.button16.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button16.BackgroundImage")));
            this.button16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button16.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button16.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button16.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Location = new System.Drawing.Point(31, 445);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(60, 77);
            this.button16.TabIndex = 8;
            this.button16.UseVisualStyleBackColor = false;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(190, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(385, 36);
            this.label2.TabIndex = 9;
            this.label2.Text = "Troubleshoot Locker Task";
            // 
            // button18
            // 
            this.button18.Enabled = false;
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.Location = new System.Drawing.Point(31, 376);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(89, 63);
            this.button18.TabIndex = 4;
            this.button18.Text = "Locker Settings";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // bLock
            // 
            this.bLock.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bLock.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bLock.Location = new System.Drawing.Point(31, 139);
            this.bLock.Name = "bLock";
            this.bLock.Size = new System.Drawing.Size(75, 45);
            this.bLock.TabIndex = 10;
            this.bLock.Text = "Lock";
            this.bLock.UseVisualStyleBackColor = true;
            this.bLock.Click += new System.EventHandler(this.bLock_Click);
            // 
            // tboxAccessCode
            // 
            this.tboxAccessCode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tboxAccessCode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.tboxAccessCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboxAccessCode.Location = new System.Drawing.Point(577, 199);
            this.tboxAccessCode.Name = "tboxAccessCode";
            this.tboxAccessCode.Size = new System.Drawing.Size(113, 53);
            this.tboxAccessCode.TabIndex = 11;
            this.tboxAccessCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tboxAccessCode.Enter += new System.EventHandler(this.txtAccessCode_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(359, 212);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(191, 36);
            this.label3.TabIndex = 12;
            this.label3.Text = "Access Code";
            // 
            // bSwapLocker
            // 
            this.bSwapLocker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bSwapLocker.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bSwapLocker.Location = new System.Drawing.Point(31, 241);
            this.bSwapLocker.Name = "bSwapLocker";
            this.bSwapLocker.Size = new System.Drawing.Size(75, 45);
            this.bSwapLocker.TabIndex = 13;
            this.bSwapLocker.Text = "Swap Locker";
            this.bSwapLocker.UseVisualStyleBackColor = true;
            this.bSwapLocker.Click += new System.EventHandler(this.bSwapLocker_Click);
            // 
            // labFree
            // 
            this.labFree.AutoSize = true;
            this.labFree.BackColor = System.Drawing.Color.Transparent;
            this.labFree.Font = new System.Drawing.Font("Microsoft Sans Serif", 22F);
            this.labFree.Location = new System.Drawing.Point(359, 140);
            this.labFree.Name = "labFree";
            this.labFree.Size = new System.Drawing.Size(173, 36);
            this.labFree.TabIndex = 16;
            this.labFree.Text = "Free Locker";
            this.labFree.Click += new System.EventHandler(this.LabFree_Click);
            // 
            // cboxCurrentLocker
            // 
            this.cboxCurrentLocker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxCurrentLocker.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboxCurrentLocker.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxCurrentLocker.FormattingEnabled = true;
            this.cboxCurrentLocker.IntegralHeight = false;
            this.cboxCurrentLocker.Location = new System.Drawing.Point(577, 76);
            this.cboxCurrentLocker.Name = "cboxCurrentLocker";
            this.cboxCurrentLocker.Size = new System.Drawing.Size(113, 54);
            this.cboxCurrentLocker.TabIndex = 22;
            this.cboxCurrentLocker.SelectedIndexChanged += new System.EventHandler(this.cboxCurrentLocker_SelectedIndexChanged);
            this.cboxCurrentLocker.Enter += new System.EventHandler(this.CboxCurrentLocker_Enter);
            // 
            // cboxNewLocker
            // 
            this.cboxNewLocker.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboxNewLocker.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxNewLocker.FormattingEnabled = true;
            this.cboxNewLocker.IntegralHeight = false;
            this.cboxNewLocker.Location = new System.Drawing.Point(577, 139);
            this.cboxNewLocker.MaxDropDownItems = 6;
            this.cboxNewLocker.Name = "cboxNewLocker";
            this.cboxNewLocker.Size = new System.Drawing.Size(113, 54);
            this.cboxNewLocker.Sorted = true;
            this.cboxNewLocker.TabIndex = 23;
            this.cboxNewLocker.SelectedIndexChanged += new System.EventHandler(this.cboxNewLocker_SelectedIndexChanged);
            this.cboxNewLocker.Enter += new System.EventHandler(this.CboxNewLocker_Enter);
            // 
            // bSmall
            // 
            this.bSmall.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bSmall.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bSmall.Location = new System.Drawing.Point(34, 88);
            this.bSmall.Name = "bSmall";
            this.bSmall.Size = new System.Drawing.Size(75, 45);
            this.bSmall.TabIndex = 24;
            this.bSmall.Text = "Small";
            this.bSmall.UseVisualStyleBackColor = true;
            this.bSmall.Click += new System.EventHandler(this.bSmall_Click);
            // 
            // bMedium
            // 
            this.bMedium.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bMedium.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bMedium.Location = new System.Drawing.Point(115, 88);
            this.bMedium.Name = "bMedium";
            this.bMedium.Size = new System.Drawing.Size(75, 45);
            this.bMedium.TabIndex = 25;
            this.bMedium.Text = "Medium";
            this.bMedium.UseVisualStyleBackColor = true;
            this.bMedium.Click += new System.EventHandler(this.bMedium_Click);
            // 
            // bXLarge
            // 
            this.bXLarge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bXLarge.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bXLarge.Location = new System.Drawing.Point(278, 88);
            this.bXLarge.Name = "bXLarge";
            this.bXLarge.Size = new System.Drawing.Size(75, 45);
            this.bXLarge.TabIndex = 27;
            this.bXLarge.Text = "XLarge";
            this.bXLarge.UseVisualStyleBackColor = true;
            this.bXLarge.Click += new System.EventHandler(this.bXLarge_Click);
            // 
            // bLarge
            // 
            this.bLarge.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bLarge.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bLarge.Location = new System.Drawing.Point(196, 88);
            this.bLarge.Name = "bLarge";
            this.bLarge.Size = new System.Drawing.Size(75, 45);
            this.bLarge.TabIndex = 26;
            this.bLarge.Text = "Large";
            this.bLarge.UseVisualStyleBackColor = true;
            this.bLarge.Click += new System.EventHandler(this.bLarge_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(130, 297);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 25);
            this.label5.TabIndex = 28;
            this.label5.Text = "Hired : ";
            // 
            // labHiredLockers
            // 
            this.labHiredLockers.AutoSize = true;
            this.labHiredLockers.BackColor = System.Drawing.Color.Transparent;
            this.labHiredLockers.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labHiredLockers.Location = new System.Drawing.Point(210, 297);
            this.labHiredLockers.Name = "labHiredLockers";
            this.labHiredLockers.Size = new System.Drawing.Size(23, 25);
            this.labHiredLockers.TabIndex = 29;
            this.labHiredLockers.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(247, 297);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 25);
            this.label7.TabIndex = 30;
            this.label7.Text = "/";
            // 
            // labTotalLockersA
            // 
            this.labTotalLockersA.AutoSize = true;
            this.labTotalLockersA.BackColor = System.Drawing.Color.Transparent;
            this.labTotalLockersA.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTotalLockersA.Location = new System.Drawing.Point(276, 297);
            this.labTotalLockersA.Name = "labTotalLockersA";
            this.labTotalLockersA.Size = new System.Drawing.Size(34, 25);
            this.labTotalLockersA.TabIndex = 31;
            this.labTotalLockersA.Text = "50";
            // 
            // labTotalLockersB
            // 
            this.labTotalLockersB.AutoSize = true;
            this.labTotalLockersB.BackColor = System.Drawing.Color.Transparent;
            this.labTotalLockersB.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTotalLockersB.Location = new System.Drawing.Point(276, 331);
            this.labTotalLockersB.Name = "labTotalLockersB";
            this.labTotalLockersB.Size = new System.Drawing.Size(34, 25);
            this.labTotalLockersB.TabIndex = 35;
            this.labTotalLockersB.Text = "50";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(247, 331);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(18, 25);
            this.label9.TabIndex = 34;
            this.label9.Text = "/";
            // 
            // labDoorFaulty
            // 
            this.labDoorFaulty.AutoSize = true;
            this.labDoorFaulty.BackColor = System.Drawing.Color.Transparent;
            this.labDoorFaulty.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labDoorFaulty.Location = new System.Drawing.Point(210, 331);
            this.labDoorFaulty.Name = "labDoorFaulty";
            this.labDoorFaulty.Size = new System.Drawing.Size(23, 25);
            this.labDoorFaulty.TabIndex = 33;
            this.labDoorFaulty.Text = "1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(76, 331);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 25);
            this.label11.TabIndex = 32;
            this.label11.Text = "Door Faults : ";
            // 
            // bClearAllHires
            // 
            this.bClearAllHires.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bClearAllHires.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bClearAllHires.Location = new System.Drawing.Point(196, 241);
            this.bClearAllHires.Name = "bClearAllHires";
            this.bClearAllHires.Size = new System.Drawing.Size(75, 45);
            this.bClearAllHires.TabIndex = 36;
            this.bClearAllHires.Text = "Clear All Hires";
            this.bClearAllHires.UseVisualStyleBackColor = true;
            this.bClearAllHires.Click += new System.EventHandler(this.bClearAllHires_Click);
            // 
            // bLockActivate
            // 
            this.bLockActivate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bLockActivate.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bLockActivate.Location = new System.Drawing.Point(31, 190);
            this.bLockActivate.Name = "bLockActivate";
            this.bLockActivate.Size = new System.Drawing.Size(75, 45);
            this.bLockActivate.TabIndex = 37;
            this.bLockActivate.Text = "Activate  Locker";
            this.bLockActivate.UseVisualStyleBackColor = true;
            this.bLockActivate.Click += new System.EventHandler(this.bLockActivate_Click);
            // 
            // cbIncludeDisabled
            // 
            this.cbIncludeDisabled.AutoSize = true;
            this.cbIncludeDisabled.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIncludeDisabled.Location = new System.Drawing.Point(365, 179);
            this.cbIncludeDisabled.Name = "cbIncludeDisabled";
            this.cbIncludeDisabled.Size = new System.Drawing.Size(146, 24);
            this.cbIncludeDisabled.TabIndex = 38;
            this.cbIncludeDisabled.Text = "Include Disabled";
            this.cbIncludeDisabled.UseVisualStyleBackColor = true;
            this.cbIncludeDisabled.CheckedChanged += new System.EventHandler(this.cbIncludeDisabled_CheckedChanged);
            // 
            // bTaskBarToggle
            // 
            this.bTaskBarToggle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bTaskBarToggle.Location = new System.Drawing.Point(126, 376);
            this.bTaskBarToggle.Name = "bTaskBarToggle";
            this.bTaskBarToggle.Size = new System.Drawing.Size(100, 63);
            this.bTaskBarToggle.TabIndex = 39;
            this.bTaskBarToggle.Text = "TaskBar Hide";
            this.bTaskBarToggle.UseVisualStyleBackColor = true;
            this.bTaskBarToggle.Click += new System.EventHandler(this.bTaskBarToggle_Click);
            // 
            // tboxUsed
            // 
            this.tboxUsed.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tboxUsed.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.tboxUsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboxUsed.Location = new System.Drawing.Point(585, 77);
            this.tboxUsed.Name = "tboxUsed";
            this.tboxUsed.Size = new System.Drawing.Size(113, 53);
            this.tboxUsed.TabIndex = 40;
            this.tboxUsed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tboxUsed.Visible = false;
            this.tboxUsed.Enter += new System.EventHandler(this.TboxUsed_Enter);
            // 
            // tboxFree
            // 
            this.tboxFree.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.tboxFree.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.RecentlyUsedList;
            this.tboxFree.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tboxFree.Location = new System.Drawing.Point(585, 139);
            this.tboxFree.Name = "tboxFree";
            this.tboxFree.Size = new System.Drawing.Size(113, 53);
            this.tboxFree.TabIndex = 41;
            this.tboxFree.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tboxFree.Visible = false;
            this.tboxFree.TextChanged += new System.EventHandler(this.TboxFree_TextChanged);
            this.tboxFree.Enter += new System.EventHandler(this.TboxFree_Enter);
            // 
            // bTaskShow
            // 
            this.bTaskShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bTaskShow.Location = new System.Drawing.Point(234, 376);
            this.bTaskShow.Name = "bTaskShow";
            this.bTaskShow.Size = new System.Drawing.Size(100, 63);
            this.bTaskShow.TabIndex = 42;
            this.bTaskShow.Text = "TaskBar Show";
            this.bTaskShow.UseVisualStyleBackColor = true;
            this.bTaskShow.Click += new System.EventHandler(this.BTaskShow_Click);
            // 
            // TroubleshootLocker
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(195)))), ((int)(((byte)(65)))));
            this.ClientSize = new System.Drawing.Size(768, 526);
            this.Controls.Add(this.bTaskShow);
            this.Controls.Add(this.tboxFree);
            this.Controls.Add(this.tboxUsed);
            this.Controls.Add(this.bTaskBarToggle);
            this.Controls.Add(this.cbIncludeDisabled);
            this.Controls.Add(this.bLockActivate);
            this.Controls.Add(this.bClearAllHires);
            this.Controls.Add(this.labTotalLockersB);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.labDoorFaulty);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.labTotalLockersA);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labHiredLockers);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bXLarge);
            this.Controls.Add(this.bLarge);
            this.Controls.Add(this.bMedium);
            this.Controls.Add(this.bSmall);
            this.Controls.Add(this.cboxNewLocker);
            this.Controls.Add(this.cboxCurrentLocker);
            this.Controls.Add(this.labFree);
            this.Controls.Add(this.bFreeHire);
            this.Controls.Add(this.bSwapLocker);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tboxAccessCode);
            this.Controls.Add(this.bLock);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.labUsed);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.bDeactivate);
            this.Controls.Add(this.bUpdateCode);
            this.Controls.Add(this.bUnlock);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TroubleshootLocker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "TroubleshootLocker";
            this.Load += new System.EventHandler(this.TroubleshootLocker_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bUnlock;
        private System.Windows.Forms.Button bUpdateCode;
        private System.Windows.Forms.Button bDeactivate;
        private System.Windows.Forms.Button bFreeHire;
        private System.Windows.Forms.Label labUsed;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button bLock;
        private System.Windows.Forms.TextBox tboxAccessCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bSwapLocker;
        private System.Windows.Forms.Label labFree;
        private System.Windows.Forms.ComboBox cboxCurrentLocker;
        private System.Windows.Forms.ComboBox cboxNewLocker;
        private System.Windows.Forms.Button bSmall;
        private System.Windows.Forms.Button bMedium;
        private System.Windows.Forms.Button bXLarge;
        private System.Windows.Forms.Button bLarge;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labHiredLockers;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labTotalLockersA;
        private System.Windows.Forms.Label labTotalLockersB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labDoorFaulty;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button bClearAllHires;
        private System.Windows.Forms.Button bLockActivate;
        private System.Windows.Forms.CheckBox cbIncludeDisabled;
        private System.Windows.Forms.Button bTaskBarToggle;
        private System.Windows.Forms.TextBox tboxUsed;
        private System.Windows.Forms.TextBox tboxFree;
        private System.Windows.Forms.Button bTaskShow;
    }
}