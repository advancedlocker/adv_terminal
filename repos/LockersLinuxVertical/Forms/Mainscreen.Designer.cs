﻿namespace Lockers
{
    partial class Mainscreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mainscreen));
            this.bOpen = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnAdmin = new System.Windows.Forms.Button();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lbltime = new System.Windows.Forms.Label();
            this.labTimeHead = new System.Windows.Forms.Label();
            this.bRent = new System.Windows.Forms.Button();
            this.labTapToRent = new System.Windows.Forms.Label();
            this.labTapToAccess = new System.Windows.Forms.Label();
            this.labRent = new System.Windows.Forms.Label();
            this.labLockerA = new System.Windows.Forms.Label();
            this.labLockerB = new System.Windows.Forms.Label();
            this.labOpen = new System.Windows.Forms.Label();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.panUpper = new System.Windows.Forms.Panel();
            this.panLower = new System.Windows.Forms.Panel();
            this.panMain = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panUpper.SuspendLayout();
            this.panMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // bOpen
            // 
            this.bOpen.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bOpen, "bOpen");
            this.bOpen.FlatAppearance.BorderSize = 0;
            this.bOpen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bOpen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bOpen.Name = "bOpen";
            this.bOpen.UseVisualStyleBackColor = false;
            this.bOpen.Click += new System.EventHandler(this.BOpen_Click_1);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnAdmin
            // 
            this.btnAdmin.BackColor = System.Drawing.Color.Transparent;
            this.btnAdmin.FlatAppearance.BorderSize = 0;
            this.btnAdmin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnAdmin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAdmin, "btnAdmin");
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.UseVisualStyleBackColor = false;
            this.btnAdmin.Click += new System.EventHandler(this.btnAdmin_Click);
            // 
            // timer2
            // 
            this.timer2.Interval = 10000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            this.lbltime.Click += new System.EventHandler(this.Lbltime_Click);
            // 
            // labTimeHead
            // 
            resources.ApplyResources(this.labTimeHead, "labTimeHead");
            this.labTimeHead.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHead.ForeColor = System.Drawing.Color.White;
            this.labTimeHead.Name = "labTimeHead";
            this.labTimeHead.Click += new System.EventHandler(this.LabTimeHead_Click);
            // 
            // bRent
            // 
            this.bRent.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bRent, "bRent");
            this.bRent.FlatAppearance.BorderSize = 0;
            this.bRent.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bRent.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bRent.Name = "bRent";
            this.bRent.UseVisualStyleBackColor = false;
            this.bRent.Click += new System.EventHandler(this.bRent_Click);
            // 
            // labTapToRent
            // 
            this.labTapToRent.BackColor = System.Drawing.Color.Transparent;
            this.labTapToRent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labTapToRent, "labTapToRent");
            this.labTapToRent.ForeColor = System.Drawing.Color.White;
            this.labTapToRent.Name = "labTapToRent";
            this.labTapToRent.Click += new System.EventHandler(this.LabTapToRent_Click);
            // 
            // labTapToAccess
            // 
            this.labTapToAccess.BackColor = System.Drawing.Color.Transparent;
            this.labTapToAccess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labTapToAccess, "labTapToAccess");
            this.labTapToAccess.ForeColor = System.Drawing.Color.White;
            this.labTapToAccess.Name = "labTapToAccess";
            this.labTapToAccess.Click += new System.EventHandler(this.LabTapToAccess_Click);
            // 
            // labRent
            // 
            this.labRent.BackColor = System.Drawing.Color.White;
            this.labRent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labRent, "labRent");
            this.labRent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labRent.Name = "labRent";
            this.labRent.Click += new System.EventHandler(this.LabRent_Click);
            // 
            // labLockerA
            // 
            this.labLockerA.BackColor = System.Drawing.Color.White;
            this.labLockerA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labLockerA, "labLockerA");
            this.labLockerA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labLockerA.Name = "labLockerA";
            this.labLockerA.Click += new System.EventHandler(this.LabRent_Click);
            // 
            // labLockerB
            // 
            this.labLockerB.BackColor = System.Drawing.Color.White;
            this.labLockerB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labLockerB, "labLockerB");
            this.labLockerB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labLockerB.Name = "labLockerB";
            this.labLockerB.Click += new System.EventHandler(this.LabOpen_Click);
            // 
            // labOpen
            // 
            this.labOpen.BackColor = System.Drawing.Color.White;
            this.labOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labOpen, "labOpen");
            this.labOpen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labOpen.Name = "labOpen";
            this.labOpen.Click += new System.EventHandler(this.LabOpen_Click);
            // 
            // timer3
            // 
            this.timer3.Enabled = true;
            this.timer3.Interval = 20000;
            this.timer3.Tick += new System.EventHandler(this.Timer3_Tick);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Name = "label1";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // panUpper
            // 
            this.panUpper.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panUpper, "panUpper");
            this.panUpper.Controls.Add(this.panel5);
            this.panUpper.Name = "panUpper";
            // 
            // panLower
            // 
            this.panLower.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panLower, "panLower");
            this.panLower.Name = "panLower";
            // 
            // panMain
            // 
            this.panMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(195)))), ((int)(((byte)(65)))));
            this.panMain.Controls.Add(this.labTimeHead);
            this.panMain.Controls.Add(this.labRent);
            this.panMain.Controls.Add(this.label1);
            this.panMain.Controls.Add(this.labOpen);
            this.panMain.Controls.Add(this.btnAdmin);
            this.panMain.Controls.Add(this.labLockerB);
            this.panMain.Controls.Add(this.lbltime);
            this.panMain.Controls.Add(this.labLockerA);
            this.panMain.Controls.Add(this.labTapToRent);
            this.panMain.Controls.Add(this.labTapToAccess);
            this.panMain.Controls.Add(this.bRent);
            this.panMain.Controls.Add(this.bOpen);
            resources.ApplyResources(this.panMain, "panMain");
            this.panMain.Name = "panMain";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.Name = "panel5";
            // 
            // Mainscreen
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(195)))), ((int)(((byte)(65)))));
            this.Controls.Add(this.panMain);
            this.Controls.Add(this.panLower);
            this.Controls.Add(this.panUpper);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "Mainscreen";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mainscreen_FormClosing);
            this.Load += new System.EventHandler(this.Mainscreen_Load);
            this.Shown += new System.EventHandler(this.Mainscreen_Shown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Mainscreen_KeyPress);
            this.panUpper.ResumeLayout(false);
            this.panMain.ResumeLayout(false);
            this.panMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bRent;
        private System.Windows.Forms.Button bOpen;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnAdmin;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label labTimeHead;
        private System.Windows.Forms.Label labTapToRent;
        private System.Windows.Forms.Label labTapToAccess;
        private System.Windows.Forms.Label labRent;
        private System.Windows.Forms.Label labLockerA;
        private System.Windows.Forms.Label labLockerB;
        private System.Windows.Forms.Label labOpen;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panUpper;
        private System.Windows.Forms.Panel panLower;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panMain;
    }
}