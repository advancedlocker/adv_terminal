﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace Lockers
{
    public partial class PINReg : Form
    {
        bool Rental = false;
        string m_PIN;
        string m_PIN2;
        int Regmode = 0;

        int[] Keymap = new int[10];

        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        public PINReg(int rental)
        {
            InitializeComponent();

            this.KeyPress += new KeyPressEventHandler(Form_KeyPress);

            bOK.Visible = false;
            labOK.Visible = false;

            if (rental == 0)
            {
                this.Rental = false;
                labHdgChoosePin.Text = "Enter your PIN number";
            }
            else
            {
                this.Rental = true;
                labHdgChoosePin.Text = "Choose a PIN number";
            }
            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            labTimeHdg.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
            timIdle.Start();

// TODO : Fix This Insertion
//            labCodeMinSize.Text = Properties.Settings.Default.MinUserPinLength.ToString();
//            labCodeMaxSize.Text = Properties.Settings.Default.MaxUserPinLen.ToString();

            // Randomise the buttons
            if (Properties.Settings.Default.SecurityModalKeypad)
                RandomiseButtons();
        }

        private void Form_KeyPress(object sender, KeyPressEventArgs e)
        {
//            MessageBox.Show("Form.KeyPress: '" +
//                e.KeyChar.ToString() + "' pressed.");
        }

        private void RandomiseButtons()
        {
            Globals.LogFile(Globals.LogSource.PinReg, "RandomiseButtons()", "Randomising buttons");
            bOK.Visible = false;
            labOK.Visible = false;

            // Randomise the numbers
            Random randomGen = new Random();
            Boolean RandomisationFinished = false;
            int[] CheckMap = new int[10];

            for (int count = 0; count < 10; count++)
            {
                Keymap[count] = 10;
                CheckMap[count] = 0;
            }

            int randomSeqA = randomGen.Next(0123456789, 2147483647);      // Random 32bit number
            int tmp = randomSeqA;

            for (int count = 0; count < 10; count++)
            {
                int CellValue = randomSeqA % 10;
                randomSeqA /= 10;
                if (CheckMap[CellValue] == 0)
                {
                    CheckMap[CellValue] = 1;
                    Keymap[count] = CellValue;
                }
            }

            RandomisationFinished = true;
            for (int count = 0; count < 10; count++)
            {
                if (CheckMap[count] == 0)
                    RandomisationFinished = false;
            }

            while (RandomisationFinished == false)
            {
                randomSeqA = randomGen.Next(0123456789, 2147483647);      // Random 32bit number
                                                                          //                int randomSeqB = randomGen.Next(2147483647, 2147483647);      // Random 32bit number

                tmp = randomSeqA;

                for (int count = 0; count < 10; count++)
                {
                    int CellValue = randomSeqA % 10;
                    randomSeqA /= 10;
                    if (CheckMap[CellValue] == 0)
                    {
                        CheckMap[CellValue] = 1;
                        for (int j = 0; j < 10; j++)
                        {
                            if (Keymap[j] == 10)
                            {
                                Keymap[j] = CellValue;
                                break;
                            }
                        }
                    }
                }

                // Crawl checkkeymap to add the missing numbers
                RandomisationFinished = true;
                for (int count = 0; count < 10; count++)
                {
                    if (CheckMap[count] == 0)
                        RandomisationFinished = false;
                }
            }

            bNumber0.Text = Keymap[0].ToString();
            bNumber1.Text = Keymap[1].ToString();
            bNumber2.Text = Keymap[2].ToString();
            bNumber3.Text = Keymap[3].ToString();
            bNumber4.Text = Keymap[4].ToString();
            bNumber5.Text = Keymap[5].ToString();
            bNumber6.Text = Keymap[6].ToString();
            bNumber7.Text = Keymap[7].ToString();
            bNumber8.Text = Keymap[8].ToString();
            bNumber9.Text = Keymap[9].ToString();

            this.Update();
        }

        private void bNumeric_Click(object sender, EventArgs e)
        {
            lblmsg.Text = "";
            lblmsg.Visible = false;

            Button b = (Button)sender;

            if (b.Name == "bGhost")     // Enter Key
            {
                if (bOK.Visible == true)
                    bOK.PerformClick();

                return;
            }

            if (b.Name == "bClear")
            {
                lblCode.Text = "";
                bOK.Visible = false;
                labOK.Visible = false;
                if (Regmode == 0)
                    m_PIN = "";
                else
                    m_PIN2 = "";
            }
            else
            {
                if (lblCode.Text.Length < Properties.Settings.Default.MaxUserPinLen)
                {
                    lblCode.Text += "*";
                    if (Regmode == 0)
                    {
                        m_PIN += b.Text;
                        if (m_PIN.Length >= Properties.Settings.Default.MinUserPinLength)
                        {
                            bOK.Visible = true;
                            labOK.Visible = true;
                        }
                        else
                        {
                            bOK.Visible = false;
                            labOK.Visible = false;
                        }
                    }
                    else
                    {
                        m_PIN2 += b.Text;
                        if (m_PIN2.Length >= Properties.Settings.Default.MinUserPinLength)
                        {
                            bOK.Visible = true;
                            labOK.Visible = true;
                        }
                        else
                        {
                            bOK.Visible = false;
                            labOK.Visible = false;
                        }
                    }
                }
            }
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            switch (Regmode)
            {
                case 0:
                    if(Properties.Settings.Default.SecurityModalKeypad)
                        RandomiseButtons();

                    Globals.LogFile(Globals.LogSource.PinReg, "bOK_Click()", "m_PIN = " + m_PIN);

                    if (Globals.UserProcessType == ProcessType.rent)
                    {
                        Regmode = 1;
// Todo : fix label
                        labHdgChoosePin.Text = "Re-enter your PIN"; 
                        lblmsg.Text = "Please re-enter your PIN code";
                        lblmsg.Visible = true;
                        lblCode.Text = "";

                        bNumber0.Focus();
                    }
                    else
                    {
                        Globals.LogFile(Globals.LogSource.PinReg, "Setting PIN", "m_PIN = " + m_PIN);
                        Globals.LockData.PINCode_Set(m_PIN);
                        timer1.Stop();
                        this.DialogResult = DialogResult.OK;
                        //                        this.Close();
                    }

                    break;

                case 1:
                    Globals.LogFile(Globals.LogSource.PinReg, "bOK_Click()", "m_PIN2 = " + m_PIN2);

                    if (m_PIN2 == m_PIN)
                    {
                        Globals.LockData.PINCode_Set(m_PIN);
                        timer1.Stop();
                        this.DialogResult = DialogResult.OK;
//                        this.Close();
                    }
                    else
                    {

                        lblmsg.Text = "Second PIN Number does not match";
                        lblmsg.Visible = true;
                        bOK.Visible = false;
                        labOK.Visible = false;
                        m_PIN2 = "";
                        lblCode.Text = "";
                    }
                    break;
            }
        }

        private void PINReg_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            InvokeLanguage();

            if (this.Rental == false)
                labHdgChoosePin.Text = "Enter your PIN number";
            else
                labHdgChoosePin.Text = "Choose a PIN number";

            labCodeLength.ForeColor = Properties.Settings.Default.TextColor;
            labDigitsLong.ForeColor = Properties.Settings.Default.TextColor;
            labHdgChoosePin.ForeColor = Properties.Settings.Default.TextColor;
            labHdgPressOK.ForeColor = Properties.Settings.Default.TextColor;
            labRemember1.ForeColor = Properties.Settings.Default.TextColor;
            labRemember2.ForeColor = Properties.Settings.Default.TextColor;
            labRemember3.ForeColor = Properties.Settings.Default.TextColor;
            labTimeHdg.ForeColor = Properties.Settings.Default.TextColor;
            labYourCode.ForeColor = Properties.Settings.Default.TextColor;
            lblCode.ForeColor = Properties.Settings.Default.TextColor;
            lblmsg.ForeColor = Properties.Settings.Default.TextColor;
            labOK.ForeColor = Properties.Settings.Default.TextColor;
            labBack.ForeColor = Properties.Settings.Default.TextColor;

            bGhost.Focus();
            bfocus = true;

        }

        private void bBack_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            timer1.Stop();
            this.DialogResult = DialogResult.Cancel;
//            this.Close();
        }

        bool bfocus = false;
        int RTC_count = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (Control con in this.Controls)
            {
                // Every control has a Focused property.
                if (con.Focused == true)
                {
                    Console.WriteLine(con.Name.ToString() + "has focus.");
                    break;
                }
            }

            if (RTC_count == 10)
            {
                RTC_count = 0;
                labTimeHdg.Text = DateTime.Now.ToString("hh:mm tt", fi);
            }
            else
                RTC_count++;
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.PinReg, "bOK_Click()", "Timeout");

            timer1.Stop();
            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void InvokeLanguage()
        {
            String language = Globals.GetLanguageCode();

            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            ComponentResourceManager resources = new ComponentResourceManager(typeof(PINReg));

            foreach (Control C in this.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    foreach (Control D in C.Controls)
                    {
                        resources.ApplyResources(D, D.Name, new CultureInfo(language));
                    }
                }
                else
                {
                    resources.ApplyResources(C, C.Name, new CultureInfo(language));
                }
            }

            this.Update();
        }

        private void PINReg_KeyPress(object sender, KeyPressEventArgs e)
        {
            lblmsg.Text = "";
            lblmsg.Visible = false;

            bfocus = true;
            bGhost.Focus();
            
            //            if (e.KeyChar == "bClear")
            if (e.KeyChar == 0x1B)          // Cancel Key
            {
                this.bBack.PerformClick();
                return;
            }

            if (e.KeyChar == 0x08)          // Clear Key
            {
                lblCode.Text = "";
                bOK.Visible = false;
                labOK.Visible = false;
                if (Regmode == 0)
                    m_PIN = "";
                else
                    m_PIN2 = "";
                return;
            }

            if ((e.KeyChar == '-') || (e.KeyChar == '+'))          // Not used
            {
                return;
            }

            if (e.KeyChar.ToString() == "X")
            {
                lblCode.Text = "";
                bOK.Visible = false;
                labOK.Visible = false;
                if (Regmode == 0)
                    m_PIN = "";
                else
                    m_PIN2 = "";
            }
            else
            {
                if (lblCode.Text.Length < Properties.Settings.Default.MaxUserPinLen)
                {
                    lblCode.Text += "*";
                    if (Regmode == 0)
                    {
                        m_PIN += e.KeyChar.ToString();
                        if (m_PIN.Length >= Properties.Settings.Default.MinUserPinLength)
                        {
                            bOK.Visible = true;
                            labOK.Visible = false;
                        }
                        else
                        {
                            bOK.Visible = false;
                            labOK.Visible = false;
                        }
                    }
                    else
                    {
                        m_PIN2 += e.KeyChar.ToString();
                        if (m_PIN2.Length >= Properties.Settings.Default.MinUserPinLength)
                        {
                            bOK.Visible = true;
                            labOK.Visible = true;
                        }
                        else
                        {
                            bOK.Visible = false;
                            labOK.Visible = false;
                        }
                    }
                }
            }

//            MessageBox.Show("Form.KeyPress: '" + e.KeyChar.ToString() + "' pressed.");
        }

        private void bMouse_Click(object sender, MouseEventArgs e)
        {
            lblmsg.Text = "";
            lblmsg.Visible = false;

            Button b = (Button)sender;

            if (b.Name == "bClear")
            {
                lblCode.Text = "";
                bOK.Visible = false;
                labOK.Visible = false;
                if (Regmode == 0)
                    m_PIN = "";
                else
                    m_PIN2 = "";
            }
            else
            {
                if (lblCode.Text.Length < Properties.Settings.Default.MaxUserPinLen)
                {
                    lblCode.Text += "*";
                    if (Regmode == 0)
                    {
                        m_PIN += b.Text;
                        if (m_PIN.Length >= Properties.Settings.Default.MinUserPinLength)
                        {
                            bOK.Visible = true;
                            labOK.Visible = true;
                        }
                        else
                        {
                            bOK.Visible = false;
                            labOK.Visible = false;
                        }
                    }
                    else
                    {
                        m_PIN2 += b.Text;
                        if (m_PIN2.Length >= Properties.Settings.Default.MinUserPinLength)
                        {
                            bOK.Visible = true;
                            labOK.Visible = true;
                        }
                        else
                        {
                            bOK.Visible = false;
                            labOK.Visible = false;
                        }
                    }
                }
            }

            //            bGhost.Focus();
            bGhost.Select();
            bfocus = true;
            this.Focus();
            this.Update();
        }

        private void PINReg_KeyPress(object sender, EventArgs e)
        {
            if (bOK.Visible == true)
                bOK.PerformClick();
        }
    }
}
