﻿
// </summary>
// version locker01 - vic
// -----------------------------------------------------------------------

using System;
using System.Threading;

namespace Lockers
{
    #region namespace
    using System;
    using System.Data;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Windows.Forms;
    using System.Xml;
    #endregion
    public partial class Mainscreen : Form
    {
//        private String Country;
//        private String[] Languages;
//        private String[] Flags;
//        Button[] LanguageButtons;

//        DialogResult diagRes;
        private int runStep;
        private int mode = 0;
        private int PINStep = 0;

        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        private LockerSelection lockerselection;
        private SelectSize selectsize;
        private SelectLogin selectlogin;
        private TimeSelection timeselection;
        private TimeDaySelection timedayselection;
        private PINReg pinreg;
        private PINLogin pinlogin;
        private PINImage pinimage;
        private PaymentSelection paymentscreen;
        private Unlocked unlocked;
        private RentalEnded rentend;
        private CashPayment cashpayment;
        private Paydone paydonescreen;
        private DoneForm donescreen;
        private OpenLocker openlocker;
        private LockerDatabase oLockerData;
        //        private BlackScreen blanckscreen;
        private LockerSize lsize;

        //private Button prevBtn;


        public SmartPay smartpay;
        public Boolean deviceState = false;

        private Locker_TCPClient client;
        private int mCRC16 = Properties.Settings.Default.CRC16;
        private int sKey;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        //  SMC : Removed smart pay declaration
        //        public Mainscreen(SmartPay smartpay)

        public Mainscreen()
        {
            InitializeComponent();

            timer2.Interval = Properties.Settings.Default.idletime;

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";

            // SMC removed
            //            this.smartpay = smartpay;
            this.oLockerData = new LockerDatabase();


            // Ensure that some security features are enabled if not enable LockerNumbers by default
            if (
                    (Properties.Settings.Default.SecurityFeatureAnimals == false) &&
                    (Properties.Settings.Default.SecurityFeatureLockerNumber == false)
                )
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen()", "No security set - defaulting to animals");
                Properties.Settings.Default.SecurityFeatureLockerNumber = true;
            }





            // SMC removed
            //            this.blanckscreen = new BlackScreen();

            //this.prevBtn = b1;
            //            Globals.prevBtn = Country1;

            //this.smartpay.VALIDATOR.PaidAmountEvent += this.Smartpay_PaidAmount;
            //this.smartpay.VALIDATOR.NoteStored += this.Validator_NoteStored;
            //this.smartpay.VALIDATOR.OutOfService += this.Validator_OutofService;
            //this.smartpay.VALIDATOR.CashBoxFull += this.Stacker_Full;
            //this.smartpay.VALIDATOR.FraudAttemp += this.Fraud_Attempt;
            //this.smartpay.VALIDATOR.UnSafe_Jam += this.UnSafe_Jam;
            //this.smartpay.VALIDATOR.Safe_Jam += this.Safe_Jam;
            //this.smartpay.VALIDATOR.RejectedNote += this.RejectedNotes;

            //this.smartpay.HOPPER.PaidAmountEvent += this.Smartpay_CoinPaidAmount;

            //this.smartpay.VALIDATOR.DisableValidator();
            //CHelpers.Pause(300);
            //this.smartpay.HOPPER.DisableValidator();
            //CHelpers.Pause(200);

            //ManualResetEvent syncEvent = new ManualResetEvent(false);
            //Thread t1 = new Thread(
            //    () =>
            //    {
            //        // Do some work...

            //        this.smartpay.VALIDATOR.DisableValidator();
            //        syncEvent.Set();
            //    }
            //);
            //t1.Start();

            //Thread t2 = new Thread(
            //    () =>
            //    {
            //        syncEvent.WaitOne();
            //        this.smartpay.HOPPER.DisableValidator();
            //                // Do some work...
            //            }
            //);
            //t2.Start();
        }



        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            CHelpers.Shutdown = true;
            if (this.smartpay != null)
            {
                this.smartpay.StopPaysystem();
                if (!this.smartpay.VALIDATOR.isDISABLED)
                {
                    this.smartpay.VALIDATOR.DisableValidator();
                    CHelpers.Pause(200);
                }

                if (!this.smartpay.HOPPER.Is_Disable)
                {
                    this.smartpay.HOPPER.DisableValidator();
                    CHelpers.Pause(200);
                }

                this.smartpay.Dispose();
            }
            //if (deviceState == true)
            //{
            //    Dll_Camera.ReleaseDevice();
            //}
            //else
            //{
            //    Dll_Camera.ReleaseLostDevice();
            //}

            if (e.CloseReason == CloseReason.WindowsShutDown) return;



        }


        private void Mainscreen_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen()", "EnableAllowFocusChange");
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen()", "EnablePreventFocusChange");
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            ResetBackground();
            timer1.Start();

            //start locker controller
            Start_LockControl();
            //            this.Run();

            /*  // Advam Test
                        CreditPayment Cred = new CreditPayment();
                        if (Cred.ShowDialog() == DialogResult.Cancel)
                        {
                            this.Show();
                            Globals.AbortForm = false;
                            timer2.Start();
                            return;
                        }
            */
        }

        private void Mainscreen_Shown(object sender, EventArgs e)
        {
            welcome welcomescreen = new welcome(this.smartpay);
            welcomescreen.BringToFront();

            DialogResult start = welcomescreen.ShowDialog();
            Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Calling WelcomeScreen");
            if (start == DialogResult.OK)
            {
                ResetBackground();
                timer2.Start();
            }

            try
            {
                if (Globals.LockData == null)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData == null");
                    Globals.LockData = new LockerDatabase();
                }

                if (Globals.LockData.Lockers == null)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData.Lockers == null");
                    Globals.LockData.Lockers = new DataTable();
                }

                Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

                if (Globals.LockData.Lockers.Rows.Count == 0)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData.Lockers.Rows.Count == 0  -   Renting Disabled");
                    bRent.Enabled = false;
                    try
                    {
                        if (Globals.CurrentLanguage == null)
                            bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\Icons\\NoLocker_482x427.tiff");
                        else
                            bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\Icons\\NoLocker_482x427.tiff");
                    }
                    catch(Exception Ex)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "bRent.BackgroundImage not found, Exception - " + Ex.ToString());
                    }
                }
                else
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData.Lockers.Rows.Count > 0  - Renting Enabled");
                    bRent.Enabled = true;
                    try
                    {
                        if (Globals.CurrentLanguage == null)
                            bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\Icons\\RentLocker_482x427.tiff");
                        else
                            bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\Icons\\RentLocker_482x427.tiff");
                    }
                    catch(Exception Ex)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "bRent.BackgroundImage not found, Exception - " + Ex.ToString());
                    }
                }
            }
            catch (Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "MainScreen_Shown()", "Exception - " + Ex.ToString());
                Console.WriteLine("Exception = " + Ex.ToString());
            }
        }

        private void Start_LockControl()
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "Starting the lock controller");

            if (Globals.client == null)
                Globals.client = new Locker_TCPClient(IPAddress.Parse(Properties.Settings.Default.ControllerIP), Properties.Settings.Default.ControllerPort, false);
            client = Globals.client;

            if (client != null)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "lock client = null");
                client.DataReceived -= new Locker_TCPClient.delDataReceived(Globals.Client_DataReceived);
                //                Globals.client.DataReceived -= new Locker_TCPClient.delDataReceived();
                client.ConnectionStatusChanged -= new Locker_TCPClient.delConnectionStatusChanged(Globals.Client_ConnectionStatusChanged);
                client.Dispose();
            }

            Globals.client = new Locker_TCPClient(IPAddress.Parse(Properties.Settings.Default.ControllerIP), Properties.Settings.Default.ControllerPort, false);
            client = Globals.client;

            //Initialize the events
            client.DataReceived += new Locker_TCPClient.delDataReceived(Globals.Client_DataReceived);
            client.ConnectionStatusChanged += new Locker_TCPClient.delConnectionStatusChanged(Globals.Client_ConnectionStatusChanged);

            Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "calling client connect()");
            this.client.Connect();
        }


        void Open_Locker(int lockerNumber)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "Open_Locker()", "Opening Locker");

            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            // USE SQL to update the address
            // TODO : Fix this serial number conbversion to byte array
            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };

            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetRear.CopyTo(sum, packetFront.Length + packetAddress.Length);

            try
            {
                client.Send(sum);
            }
            catch(Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Open_Locker()", "SocketSend Exception - " + Ex.ToString());
            }
        }

        void Close_Locker(int lockerNumber)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "Close_Locker()", "Closing Locker");

            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetAddress.CopyTo(sum, packetAddress.Length);

            try { 
                client.Send(sum);
            }
            catch(Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Close_Locker()", "SocketSend Exception - " + Ex.ToString());
            }
        }


#if (false)
        void client_ConnectionStatusChanged(Locker_TCPClient sender, Locker_TCPClient.ConnectionStatus status)
        {
            //Check if this event was fired on a different thread, if it is then we must invoke it on the UI thread
            if (InvokeRequired)
            {
                Invoke(new Locker_TCPClient.delConnectionStatusChanged(client_ConnectionStatusChanged), sender, status);
                return;
            }
            //richTextBox1.Text += "Connection: " + status.ToString() + Environment.NewLine;
            Globals.LogFile("client_ConnectionStatusChanged() - connect status = " + status.ToString() + "\n");
            if (status.ToString() == Locker_TCPClient.ConnectionStatus.Connected.ToString())
            {
                client.SourceKey = Properties.Settings.Default.sourceKey;
                Globals.client.GetSession();
            }

        }
        //Fired when new data is received in the TCP client
        void client_DataReceived(Locker_TCPClient sender, object data)
        {
            //Again, check if this needs to be invoked in the UI thread
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new Locker_TCPClient.delDataReceived(client_DataReceived), sender, data);
                }
                catch
                { }
                return;
            }
            //Interpret the received data object as a string
            string strData = data as string;
            Globals.LogFile("client_DataReceived() - data = " + strData + "\n");
            string values = strData.Split('-').Select(sValue => sValue.Trim()).First();
            /*
                        switch(Globals.client.LockerCommand)
                        {
                            case Locker_cmd.Basic.GetSession:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.AssignLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.UnassignLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.AvailableLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.UsedLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.OpenLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.CloseLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.LockStatus:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            default:
                                break;
                        }
            */
        }
#endif 

        private void Run()
        {
            //            this.runStep = 0;

            if (this.runStep == 0)
                this.runStep = 5;
            //            this.NextRunStep(this, EventArgs.Empty);   // trigger first build step

            SimpleStateMachine(this, EventArgs.Empty);   // trigger first build step
        }

        public void SimpleStateMachine(object sender, EventArgs e)
        {
            /*
                        //            timer2.Start();
                        if (Globals.AbortForm == true)
                        {
                            //                this.blanckscreen.Hide();
                            this.runStep = 7;
                            this.Show();
                            ResetBackground();


                        }
                        else
                        {

                        }
            */

            this.Hide();

            if (Globals.Process == ProcessType.rent)
            {
                //                this.client.GetAvailableLocks(int sessionkey, int CRC16);

                Globals.LockData = new LockerDatabase();
                Globals.LockData.LockNumber = 1;
                Globals.CurrentLanguage = "English";


                // TOD : Check if only one locker. If so then dont display the selection screen
                int LockerTypes = GetNumLockerTypes();

                // Select the locker size
                switch (LockerTypes)
                {
                    case 0:
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "LockerTypes == 0");
                            // TODO : Have error page for no lockers left to hire.
                        }
                        break;

                    case 1:
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "LockerTypes == 1");

                        if (Globals.LockData.Lockers == null)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Globals.LockData.Lockers == null");
                            Globals.LockData.Lockers = new DataTable();
                        }

                        Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

// TODO : Make this more generic so that any size can be single type of any size
                        Globals.LockData.Locker_Size = LockerSize.MEDIUM;
                        this.Get_LockAvailable();
                        break;

                    default:
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "LockerTypes > 1 - Select a type to rent");
                            SelectSize ssel = new SelectSize();

                            if (ssel.ShowDialog() == DialogResult.Cancel)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "SelectSize returned DialogResult.Cancel");
                                this.Show();
                                Globals.AbortForm = false;
                                //                    ssel.Close();
                                timer2.Start();
                                return;
                            }
                        }
                        break;
                }


                // Select the time for the locker
                // TODO : Check if rental type is daily 
                if (Properties.Settings.Default.VariableRentalTime == true)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "VariableRentalTime == true");
                    if (Properties.Settings.Default.HireTypePeriod == "Hour")
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "HireTypePeriod == Hour");
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "call TimeSelection");

                        // Enable hourly hire screen
                        TimeSelection TimSet = new TimeSelection();
                        if (TimSet.ShowDialog() == DialogResult.Cancel)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "TimeSelection returned DialogResult.Cancel");

                            this.Show();
                            Globals.AbortForm = false;
                            timer2.Start();
                            return;
                        }
                    }
                    else
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "HireTypePeriod != Hour");
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "call TimeDaySelection");

                        // Enable daily hire screen
                        TimeDaySelection TimSet = new TimeDaySelection();
                        if (TimSet.ShowDialog() == DialogResult.Cancel)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "TimeDaySelection returned DialogResult.Cancel");

                            this.Show();
                            Globals.AbortForm = false;
                            timer2.Start();
                            return;
                        }
                    }
                }
                else
                {
                    // TODO : Fix the rental price based on the fixed period.
                    switch (Globals.LockData.Locker_Size)
                    {
                        case LockerSize.MEDIUM:
                            break;

                        default :
                            break;
                    }
                }

                // Select the pin code
                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Call PINReg");

                Globals.LoginSelected = LoginTypes.PIN;
                PINReg PINReg = new PINReg();
                if (PINReg.ShowDialog() == DialogResult.Cancel)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PINReg returned DialogResult.Cancel");

                    this.Show();
                    Globals.AbortForm = false;
                    timer2.Start();
                    return;
                }

                // Select the security code
                if (Properties.Settings.Default.SecurityFeatureAnimals)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Properties.Settings.Default.SecurityFeatureAnimals -- call PINImage");

                    PINImage PINImage = new PINImage();
                    if (PINImage.ShowDialog() == DialogResult.Cancel)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PINImage returned DialogResult.Cancel");

                        this.Show();
                        Globals.AbortForm = false;
                        timer2.Start();
                        return;
                    }
                }
                else
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Properties.Settings.Default.SecurityFeatureAnimals -- call PINImage");

// Todo : Set the price based on the locker size. 
                    Globals.DueAmnt = (double)(12.00);
                    Globals.LockData.COST = (decimal)(12.00);

                    Globals.LockData.PINIcon = "1";
                }

                // Select the payment type
                //                if (Properties.Settings.Default.AdvamReaderEnabled == true)
                if (Properties.Settings.Default.PaymentEnableCredit && Properties.Settings.Default.PaymentEnableCash)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Properties.Settings.Default.SecurityFeatureAnimals -- call PINImage");

                    PaymentSelection Psel = new PaymentSelection();
                    if (Psel.ShowDialog() == DialogResult.Cancel)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PaymentSelection returned DialogResult.Cancel");
                        this.Show();
                        Globals.AbortForm = false;
                        timer2.Start();
                        return;
                    }
                }
                else
                {
                    if (Properties.Settings.Default.PaymentEnableCash)
                        Globals.PayTypes = PaymentTypes.Cash;

                    if (Properties.Settings.Default.PaymentEnableCredit)
                        Globals.PayTypes = PaymentTypes.CreditCard;
                }
                // Make the payment
                if (Properties.Settings.Default.SkipPhysicalPayment == false)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Payment Required");
                    switch (Globals.PayTypes)
                    {
                        case PaymentTypes.Cash:
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Startting Cash System");

                            CashPayment cashpay = new CashPayment(this.smartpay);
                            if (Globals.AbortForm == true)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "CashPayment returned DialogResult.Cancel");
                                this.Show();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }

                            break;
                        case PaymentTypes.CreditCard:
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Starting Credit Payment");

                            CreditPayment Cred = new CreditPayment();
                            if (Cred.ShowDialog() == DialogResult.Cancel)
                            {
                                Forms.CreditFailure Cfail = new Forms.CreditFailure();
                                Cfail.ShowDialog();

                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "CreditPayment returned DialogResult.Cancel");
                                this.Show();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }
                            break;
                    }
                }

                for (int lcount = 0; lcount < Properties.Settings.Default.LockerCommsRetries; lcount++)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Opening Locker  : Number - " + Globals.LockData.LockNumber.ToString());
                    Open_Locker(Globals.LockData.LockNumber);
                    Thread.Sleep(Properties.Settings.Default.LockerCommsRetryPeriod);
                }

                // Complete the payment
                Paydone paydone = new Paydone();
                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Payment Complete");
                if (paydone.ShowDialog() == DialogResult.Cancel)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Paydone returned DialogResult.Cancel");
                    ResetBackground();
                    this.Show();
                    timer2.Start();
                    return;
                }

                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Saving New rental");
                Save_Rent();

                DoneForm donescreen = new DoneForm();
                DialogResult donescreen_result = donescreen.ShowDialog();

            }
            else
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Opening Locker");

                // Select the pin code
                Globals.LoginSelected = LoginTypes.PIN;
                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Get Pin");

                PINReg PINReg = new PINReg();
                if (PINReg.ShowDialog() == DialogResult.Cancel)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PINReg returned DialogResult.Cancel");

                    this.Show();
                    Globals.AbortForm = false;
                    timer2.Start();
                    return;
                }

                LockerDatabase LockDB = new LockerDatabase();
                int LockerNumber = 0;

                // TODO : Check is PIN is unique then dont do security check.
                if (Globals.LockData.IsPINActive(Globals.LockData.PINCode))
                {
                    if (!Globals.LockData.IsPINUnique(Globals.LockData.PINCode))
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PIN not unique");

                        // Select the security code
                        if (Properties.Settings.Default.SecurityFeatureAnimals)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Security Animal Selected");
                            PINImage PINImage = new PINImage();
                            if (PINImage.ShowDialog() == DialogResult.Cancel)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Incorrect Code Entered");
                                Forms.IncorrectCode inc = new Forms.IncorrectCode();
                                inc.ShowDialog();

                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PINImage returned DialogResult.Cancel");

                                this.Show();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }

                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Opening Locker");
                            if (LockDB.Check_Access(Globals.LockData.PINCode, Globals.LockData.PINIcon) == true)
                            {
                                Globals.LockData.LockNumber = LockDB.LockNumber;
                                //                    Globals.client.OpenLock(LockerNumber);
                                for (int lcount = 0; lcount < Properties.Settings.Default.LockerCommsRetries; lcount++)
                                {
                                    Open_Locker(Globals.LockData.LockNumber);
                                    Thread.Sleep(Properties.Settings.Default.LockerCommsRetryPeriod);
                                }
                            }
                        }
                        else if (Properties.Settings.Default.SecurityFeatureLockerNumber)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Security Locker Number Selected");
                            SecurityLockerNo SecLockNo = new SecurityLockerNo();
                            if (SecLockNo.ShowDialog() == DialogResult.Cancel)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Incorrect Code Entered");
                                Forms.IncorrectCode inc = new Forms.IncorrectCode();
                                inc.ShowDialog();

                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "SecurityLockerNo returned DialogResult.Cancel");

                                this.Show();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }

                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Opening Locker");
                            if (LockDB.Check_Access(Globals.LockData.PINCode, Globals.LockData.LockNumber) == true)
                            {
                                Globals.LockData.LockNumber = LockDB.LockNumber;
                                //                    Globals.client.OpenLock(LockerNumber);
                                for (int lcount = 0; lcount < Properties.Settings.Default.LockerCommsRetries; lcount++)
                                {
                                    Open_Locker(Globals.LockData.LockNumber);
                                    Thread.Sleep(Properties.Settings.Default.LockerCommsRetryPeriod);
                                }
                            }
                        }
                    }
                    else
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Unique Access Code Detected");

                        string LockerNumbers = LockDB.GetLockersForPinCode(Globals.LockData.PINCode);
                        string[] LNUM = LockerNumbers.Split(',');
                        LockerNumber = Convert.ToInt32(LNUM[0]);

                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Opening Locker");
                        Globals.LockData.LockNumber = LockerNumber;
                        for (int lcount = 0; lcount < Properties.Settings.Default.LockerCommsRetries; lcount++)
                        {
                            Open_Locker(Globals.LockData.LockNumber);
                            Thread.Sleep(Properties.Settings.Default.LockerCommsRetryPeriod);
                        }
                    }

                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Show open locker dialog");
                    OpenLocker openLocker = new OpenLocker();
                    DialogResult openLocker_result = openLocker.ShowDialog();
                    //                this.OpenLockers();

                    if (Globals.Process == ProcessType.open)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Continue rental");

                        Unlocked UnlockScreen = new Unlocked();
                        if (UnlockScreen.ShowDialog() == DialogResult.Cancel)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Unlocked returned DialogResult.Cancel");

                            this.Show();
                            Globals.AbortForm = false;
                            timer2.Start();
                            return;
                        }
                        else
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Update the rental info");
                            this.UPdate_Rent();
                        }
                    }
                    else
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "End rental");

                        RentalEnded RentalEnded = new RentalEnded();
                        if (RentalEnded.ShowDialog() == DialogResult.Cancel)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "RentalEnded returned DialogResult.Cancel");

                            this.Show();
                            Globals.AbortForm = false;
                            timer2.Start();
                            return;
                        }
                        else
                            this.Done_rent();
                    }
                }
                else
                {
                    // PIN is inactive
                    Forms.GeneralFailure GenFail = new Forms.GeneralFailure();
                    GenFail.ShowDialog();
                }

                DoneForm donescreen = new DoneForm();
                DialogResult donescreen_result = donescreen.ShowDialog();
            }

            this.timer2_Tick(this, EventArgs.Empty);
        }

        private int GetNumLockerTypes()
        {
            if (Globals.LockData.LockerTypes == null)
                Globals.LockData.LockerTypes = new DataTable();

            Globals.LockData.LockerTypes = Globals.LockData.Get_Locker_Types();

            int lockercount = 0;
            try
            {
                var Sizes = (from Rows in Globals.LockData.LockerTypes.AsEnumerable()
                             select Rows["enabled"]).Distinct().ToList();

                lockercount = Globals.LockData.LockerTypes.Rows.Count;
                Globals.LogFile(Globals.LogSource.Mainscreen, "GetNumLockerTypes()", "Lockercount = " + lockercount.ToString());
            }
            catch (Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "GetNumLockerTypes()", "Exception - " + Ex.ToString());
            }
            return (lockercount);
        }

        private Decimal GetLockerPrice(int LockerType)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "GetLockerPrice()", "LockerType - " + LockerType.ToString());

            if (Globals.LockData.LockerTypes == null)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "GetLockerPrice()", "Globals.LockData.LockerTypes == null");
                Globals.LockData.LockerTypes = new DataTable();
            }

            Globals.LockData.LockerTypes = Globals.LockData.Get_Locker_Types();

            var Type = (from Rows in Globals.LockData.LockerTypes.AsEnumerable()
                        select Rows["sizecode"]).Distinct().ToList();

            var Price = (from Rows in Globals.LockData.LockerTypes.AsEnumerable()
                         select Rows["uprice"]).Distinct().ToList();

            Decimal cost = 0;
            /*
                        decimal cost = 0.00;
                        for(int count = 0; count < Type.Count; count++)
                        {
                            if(LockerType == Convert.ToInt16(Type[count]))
                            {
                                cost = Convert.ToDecimal(Price[count]);
                            }
                        }
            */
            return (cost);
        }

        public void NextRunStep(object sender, EventArgs e)
        {
            //            timer2.Start();
            if (Globals.AbortForm == true)
            {
                //                this.blanckscreen.Hide();
                this.runStep = 7;
                this.Show();
                ResetBackground();


            }
            else
            {

            }
            switch (this.runStep)
            {
                case 0:

                    //                    this.blanckscreen.Show();
                    this.Hide();

                    if (Globals.Process == ProcessType.rent)
                    {
                        //                        this.SizeSelection();
                        SelectSize ssel = new SelectSize();
                        DialogResult ssel_result = ssel.ShowDialog();
                        //                        if (ssel_result == DialogResult.OK)
                        this.runStep++;
                    }
                    else
                    {
                        this.OpenLockers();
                        this.runStep++;

                    }
                    break;
                case 1:
                    //                    this.TimeSettings();
                    TimeSelection TimSet = new TimeSelection();
                    DialogResult TimSet_result = TimSet.ShowDialog();
                    this.runStep++;
                    break;
                //case 2:
                //    this.LoginSelection();
                //    break;
                case 2:
                    //goes here if rent, open and end lock security register and login
                    Globals.LoginSelected = LoginTypes.PIN;
                    this.LoginType();
                    break;
                case 3:
                    // SMC : removed payment selection for the moment. Cash only at this time. 
                    //                        this.PaymentSelect();
                    this.runStep++;
                    NextRunStep(this, EventArgs.Empty);
                    break;
                case 4:
                    //make payment
                    //if (this.mode == 0)
                    //{

                    // SMC  : Moved this into the payment screen for th emoment.
                    //                    this.smartpay.RunPaysystem();
                    this.Payment();
                    //}
                    //else
                    //{


                    //}
                    break;

                case 5:
                    if (this.mode == 0)
                    {
                        this.PayDone();
                    }
                    else
                    {
                        if (Globals.Process == ProcessType.open)
                        {
                            this.UnLockScreen();
                        }
                        else
                        {
                            this.RentEndScreen();
                        }
                        //this.runStep = 5;
                    }
                    break;
                case 6:
                    this.DoneScreens();
                    break;
                case 7:
                    this.Show();
                    this.Refresh();
                    this.Invalidate();
                    Globals.AbortForm = false;
                    this.Update();

                    // SMC : Removed smart payout for the moment. 
                    //                    this.smartpay.Payout = 0;
                    //                    this.smartpay.VALIDATOR.PAID_AMOUNT = 0;
                    //                    this.smartpay.StopPaysystem();

                    timer2.Start();
                    //this.Show();
                    //ManualResetEvent syncEvent = new ManualResetEvent(false);
                    //Thread t1 = new Thread(
                    //    () =>
                    //    {
                    //        // Do some work...
                    //        this.smartpay.VALIDATOR.DisableValidator();
                    //        syncEvent.Set();
                    //    }
                    //);
                    //t1.Start();

                    //Thread t2 = new Thread(
                    //    () =>
                    //    {
                    //        syncEvent.WaitOne();
                    //        this.smartpay.HOPPER.DisableValidator();
                    //        // Do some work...
                    //    }
                    //);
                    //t2.Start();
                    break;

                default:
                    this.runStep = 6;
                    break;


            }
            this.runStep++;
            //            timer2.Stop();
        }

        private void ResetBackground()
        {
            String ImageFilename = "";

            try
            {
                if (Globals.CurrentLanguage == null)
                    ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\RentalStart_1189x789.tiff";
                else
                    ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\RentalStart_1189x789.tiff";

                this.BackgroundImage = Image.FromFile(ImageFilename);
            }
            catch(Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "ResetBackground()", "Exception - " + Ex.ToString());
            }
        }

        private void DoneScreens()
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "DoneScreens()", "Entry");
            this.donescreen = new DoneForm();
            this.donescreen.FormClosed += this.NextRunStep;
            this.donescreen.Show(this);
        }

        private void Donescreen_Load(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "Donescreen_Load()", "Entry");

            Globals.client.OpenLock(Globals.LockData.LockNumber);

            switch (Globals.Process)
            {
                case ProcessType.rent:
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "Donescreen_Load()", "Saving new rental - Locker = " + Globals.LockData.LockNumber.ToString());
                        this.Save_Rent();
                    }
                    break;
                case ProcessType.open:
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "Donescreen_Load()", "Updating Existing rental - Locker = " + Globals.LockData.LockNumber.ToString());
                        this.UPdate_Rent();
                    }
                    break;
                case ProcessType.endrent:
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "Donescreen_Load()", "Ending rental - Locker = " + Globals.LockData.LockNumber.ToString());
                        this.Done_rent();
                    }
                    break;
            }

        }

        private void OpenLockers()
        {
            this.openlocker = new OpenLocker();
            this.openlocker.FormClosed += this.NextRunStep;
            this.openlocker.Show();
        }

        private void PayDoneOriginal()
        {
            this.paydonescreen = new Paydone();
            this.paydonescreen.FormClosed += this.NextRunStep;
            this.paydonescreen.Show();
        }

        private void PayDone()
        {
            paydonescreen = new Paydone();
            paydonescreen.FormClosed += this.NextRunStep;
            paydonescreen.Show();
        }


        public void Payment()
        {
            switch (Globals.PayTypes)
            {
                case PaymentTypes.Cash:
                    this.cashpayment = new CashPayment(this.smartpay);
                    this.cashpayment.FormClosed += this.NextRunStep;
                    //                    this.cashpayment.Show();
                    this.runStep++;
                    //                    this.runStep++;
                    NextRunStep(this, null);
                    break;
                case PaymentTypes.CreditCard:
                    break;
            }

        }

        private void RentEndScreen()
        {
            this.rentend = new RentalEnded();
            this.rentend.FormClosed += this.NextRunStep;
            this.rentend.Show(this);

        }

        private void UnLockScreen()
        {
            this.unlocked = new Unlocked();
            this.unlocked.FormClosed += this.NextRunStep;
            this.unlocked.Show(this);
        }

        private void PaymentSelect()
        {

            this.paymentscreen = new PaymentSelection();
            this.paymentscreen.FormClosed += this.NextRunStep;
            this.paymentscreen.Show(this);

        }

        public void NextPINReg(object sender, EventArgs e)
        {
            if (this.PINStep <= 1 && !Globals.AbortForm)
            {
                if (this.mode == 0 && this.PINStep == 0)
                {
                    this.pinreg = new PINReg();
                    this.pinreg.FormClosed += this.NextPINReg;
                    this.pinreg.Show();

                }
                else if (this.mode == 1 && this.PINStep == 0)
                {
                    this.pinlogin = new PINLogin();
                    this.pinlogin.FormClosed += this.NextPINReg;
                    this.pinlogin.Show();
                }
                else
                {
                    this.pinimage = new PINImage();
                    this.pinimage.FormClosed += this.NextPINReg;
                    this.pinimage.Show();
                }
                this.PINStep++;
            }
            else
            {
                if (!Globals.AbortForm)
                {
                    switch (this.mode)
                    {
                        case 0:
                            // SMC : Removed for the moment as two lockers can have the same access code. 
                            // Think about the validation when codes are the same. 
                            if (Globals.LockData.Used_Access(Globals.LockData.PINCode, Globals.LockData.PINIcon))
                            {
                                this.PINStep = 0;
                                this.runStep = 2;
                                MessageBox.Show("Access Code already used", "ACCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            break;
                        case 1:
                            if (!Globals.LockData.Check_Access(Globals.LockData.PINCode, Globals.LockData.PINIcon))
                            {
                                this.PINStep = 0;
                                this.runStep = 2;
                                MessageBox.Show("Invaled Access", "ACCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                if (Globals.LockData.Remaining.Minutes < 0)
                                {
                                    decimal m_price = Globals.LockData.UPrice;
                                    decimal totalhrs = Globals.LockData.Remaining.Hours + ((decimal)Globals.LockData.Remaining.Minutes / 60);
                                    decimal cost = (totalhrs * -1) * m_price;
                                    Globals.DueAmnt = (double)Math.Round(cost, 2);
                                    Globals.LockData.COST = (decimal)Math.Round(cost, 2);
                                    this.runStep = 3;
                                }
                                else
                                {

                                    this.runStep = 5;
                                }

                            }
                            break;

                    }
                }


                //else
                //{
                ResetBackground();
                this.NextRunStep(this, EventArgs.Empty);
                //}
            }
        }

        private void LoginType()
        {
            if (Globals.LoginSelected == LoginTypes.PIN)
            {
                this.NextPINReg(this, EventArgs.Empty);
            }
        }
        private void LoginSelection()
        {
            this.selectlogin = new SelectLogin();
            this.selectlogin.FormClosed += this.NextRunStep;
            this.selectlogin.Show();
        }


        private void TimeSettings()
        {

            this.timeselection = new TimeSelection();
            this.timeselection.FormClosed += this.NextRunStep;
            this.timeselection.Show();

        }


        private void LockerSelectionScreen()
        {

            this.lockerselection = new LockerSelection();
            this.lockerselection.FormClosed += Lockerselection_Closed;
            this.lockerselection.Show();

        }

        private void Lockerselection_Closed(object sender, EventArgs e)
        {
            lockerselection.Dispose();
            lockerselection = null;
            this.NextRunStep(this, EventArgs.Empty);
        }


        private void SizeSelection()
        {

            this.selectsize = new SelectSize();
            this.selectsize.FormClosed += this.NextRunStep;
            this.selectsize.Show();
            //                this.NextRunStep(this, EventArgs.Empty);
        }

        private void Mainscreen_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString().ToUpper().Equals("Q"))
            {
                // Quit - ends kiosk program
                e.Handled = true;
                Application.Exit();
            }
        }


        private void Save_Rent()
        {
            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
            Globals.LockData.Save_Rent();

        }

        private void Done_rent()
        {
            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
            Globals.LockData.Locker_Done();

        }

        private void UPdate_Rent()
        {
            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
            Globals.LockData.Update_Rent();
        }

        private void LockData_LockerDataError(object sender, LogMessageEventArgs e)
        {
            MessageBox.Show(e.Messsage, "Error Saving", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Globals.LockData.LockerDatabaseError -= LockData_LockerDataError;
        }

        private void bRent_Click(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "bRent_Click()", "\n\n\n\n Starting new rental \n\n\n\n");

            this.mode = 0;
            this.runStep = 0;
            this.PINStep = 0;
            Globals.Process = ProcessType.rent;
            //this.oLockerData = new LockerData();
            Globals.LockData = new LockerDatabase();
            timer2.Stop();

            Globals.LockData.Get_Lockers();
            //            this.NextRunStep(this, EventArgs.Empty);
            SimpleStateMachine(this, EventArgs.Empty);

            //if (Globals.LockData.Lock_Available() == true)
            //{
            //    this.NextRunStep(this, EventArgs.Empty);
            //}
            //else
            //{
            //    MessageBox.Show("No Locks available", "Locker", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}


        }

        private void bClose_Click(object sender, EventArgs e)
        {
            timer2.Stop();
            string mCode = "";
            NumericKeypad kpad = new NumericKeypad("CLOSE PROGRAM ACCESS KEY");
            //kpad.Data = txtValue1.Text;
            DialogResult dr = kpad.ShowDialog();
            if (dr == DialogResult.OK)
            {
                mCode = kpad.Data;
            }

            kpad.Dispose();
            kpad = null;

            if (mCode == Properties.Settings.Default.ExitCode.ToString())
            {
                //                this.blanckscreen.Close();
                this.Close();

            }
            else
            {
                timer2.Start();
            }
        }

        private void bOpen_Click(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "bOpen_Click()", "\n\n\n\n Opening Locker \n\n\n\n");

            this.mode = 1;
            this.runStep = 0;
            this.PINStep = 0;
            Globals.Process = ProcessType.open;
            Globals.LockData = new LockerDatabase();
            timer2.Stop();

            SimpleStateMachine(this, EventArgs.Empty);
            //            this.NextRunStep(this, EventArgs.Empty);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            //lblsec.Text = DateTime.Now.ToString("ss");
            //lblsec.Location = new Point(lbltime.Location.X  + lbltime.Width,lblsec.Location.Y);
        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            timer2.Stop();
            //SetupPaysystem setUpPaySystem = new SetupPaysystem(this.smartpay);
            //setUpPaySystem.BringToFront();
            //setUpPaySystem.ShowDialog();
            //setUpPaySystem = null;
            int mcode = 0;
            NumericKeypad kpad = new NumericKeypad("MAINTENANCE ACCESS KEY");
            //kpad.Data = txtValue1.Text;
            DialogResult dr = kpad.ShowDialog();
            if (dr == DialogResult.OK)
            {
                if (kpad.Data == "") return;
                mcode = int.Parse(kpad.Data);
            }

            kpad.Dispose();
            kpad = null;

            if (mcode == Properties.Settings.Default.Adminpwd)
            {
                AdminTask admin = new AdminTask(smartpay);
                admin.BringToFront();
                admin.ShowDialog();

                admin.Dispose();
            }


            if (mcode == Properties.Settings.Default.ExitCode)
            {
                Application.Exit();
            }

            timer2.Start();
        }

        private void Mainscreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            //try
            //{
            if (this.smartpay != null)
            {
                this.smartpay.StopPaysystem();
                this.smartpay = null;
            }

            Application.Exit();
            //}
            //catch { }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            try
            {
                if (Globals.LockData == null)
                    Globals.LockData = new LockerDatabase();

                if (Globals.LockData.Lockers == null)
                    Globals.LockData.Lockers = new DataTable();

                Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

                if (Globals.LockData.Lockers.Rows.Count == 0)
                {
                    bRent.Enabled = false;
                    if (Globals.CurrentLanguage == null)
                        bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\Icons\\NoLocker_482x427.tiff");
                    else
                        bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\Icons\\NoLocker_482x427.tiff");
                }
                else
                {
                    bRent.Enabled = true;
                    if (Globals.CurrentLanguage == null)
                        bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\Icons\\RentLocker_482x427.tiff");
                    else
                        bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\Icons\\RentLocker_482x427.tiff");
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception = " + Ex.ToString());
            }

            timer1.Stop();
            timer2.Stop();
            welcome welcomescreen = new welcome(this.smartpay);
            welcomescreen.BringToFront();
            DialogResult start = welcomescreen.ShowDialog();
            if (start == DialogResult.OK)
            {
                //                this.runStep = 0;
                //                this.NextRunStep(this, EventArgs.Empty);

                this.Show();

                ResetBackground();
                timer1.Start();
                timer2.Start();
            }
        }

        /*
         * // SMC - Unused
            private void btnLanguage_Click(object sender, EventArgs e)
            {
                    timer2.Stop();
                    Button btn = (Button)sender;
                    Globals.LanguageCode = Convert.ToInt32(btn.Tag);
                    //================reset font ========================
                    Globals.prevBtn.Font= new Font
                 (
                    btn.Font,
                    FontStyle.Regular 
                 );
                    //======================================

                    btn.Font = new Font
                 (
                    btn.Font,
                    FontStyle.Underline | FontStyle.Bold
                 );
                    ResetBackground();
                    Globals.prevBtn = btn;
                    timer2.Start();
            }
        */
        private void Get_LockAvailable()
        {
            int lockersize = (int)Globals.LockData.Locker_Size;
            var mlock = (from Rows in Globals.LockData.Lockers.AsEnumerable()
                         where Rows.Field<string>("lockersize") == lockersize.ToString()
                         select new
                         {
                             rentLock = Rows.Field<int>("lockerNum")

                         }).OrderBy(x => Guid.NewGuid()).Take(1);
            Globals.LockData.LockNumber = mlock.First().rentLock;
        }
    }
}
