﻿namespace Lockers
{
    partial class CreditPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreditPayment));
            this.bCancel = new System.Windows.Forms.Button();
            this.lbldue = new System.Windows.Forms.Label();
            this.lblpay = new System.Windows.Forms.Label();
            this.bOK = new System.Windows.Forms.Button();
            this.bTestPay = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.timerStartPay = new System.Windows.Forms.Timer(this.components);
            this.labTotTaken = new System.Windows.Forms.Label();
            this.labTotalDue = new System.Windows.Forms.Label();
            this.labYourLockOrder = new System.Windows.Forms.Label();
            this.labMakePayment = new System.Windows.Forms.Label();
            this.labCardPayment = new System.Windows.Forms.Label();
            this.labPleaseTap = new System.Windows.Forms.Label();
            this.labNote = new System.Windows.Forms.Label();
            this.labCardBelow = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bCancel
            // 
            this.bCancel.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bCancel, "bCancel");
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.FlatAppearance.BorderSize = 0;
            this.bCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bCancel.Name = "bCancel";
            this.bCancel.UseVisualStyleBackColor = false;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // lbldue
            // 
            resources.ApplyResources(this.lbldue, "lbldue");
            this.lbldue.BackColor = System.Drawing.Color.White;
            this.lbldue.Name = "lbldue";
            // 
            // lblpay
            // 
            resources.ApplyResources(this.lblpay, "lblpay");
            this.lblpay.BackColor = System.Drawing.Color.White;
            this.lblpay.Name = "lblpay";
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bOK, "bOK");
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bOK.Name = "bOK";
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bTestPay
            // 
            resources.ApplyResources(this.bTestPay, "bTestPay");
            this.bTestPay.Name = "bTestPay";
            this.bTestPay.UseVisualStyleBackColor = true;
            this.bTestPay.Click += new System.EventHandler(this.bTestPayout_Click);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Name = "panel1";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Name = "label1";
            // 
            // button14
            // 
            resources.ApplyResources(this.button14, "button14");
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button14.Name = "button14";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Name = "label2";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // timerStartPay
            // 
            this.timerStartPay.Tick += new System.EventHandler(this.timerStartPay_Tick);
            // 
            // labTotTaken
            // 
            resources.ApplyResources(this.labTotTaken, "labTotTaken");
            this.labTotTaken.BackColor = System.Drawing.Color.Transparent;
            this.labTotTaken.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTotTaken.ForeColor = System.Drawing.Color.White;
            this.labTotTaken.Name = "labTotTaken";
            // 
            // labTotalDue
            // 
            resources.ApplyResources(this.labTotalDue, "labTotalDue");
            this.labTotalDue.BackColor = System.Drawing.Color.Transparent;
            this.labTotalDue.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTotalDue.ForeColor = System.Drawing.Color.White;
            this.labTotalDue.Name = "labTotalDue";
            // 
            // labYourLockOrder
            // 
            resources.ApplyResources(this.labYourLockOrder, "labYourLockOrder");
            this.labYourLockOrder.BackColor = System.Drawing.Color.Transparent;
            this.labYourLockOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labYourLockOrder.ForeColor = System.Drawing.Color.White;
            this.labYourLockOrder.Name = "labYourLockOrder";
            // 
            // labMakePayment
            // 
            resources.ApplyResources(this.labMakePayment, "labMakePayment");
            this.labMakePayment.BackColor = System.Drawing.Color.Transparent;
            this.labMakePayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labMakePayment.ForeColor = System.Drawing.Color.White;
            this.labMakePayment.Name = "labMakePayment";
            // 
            // labCardPayment
            // 
            resources.ApplyResources(this.labCardPayment, "labCardPayment");
            this.labCardPayment.BackColor = System.Drawing.Color.Transparent;
            this.labCardPayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labCardPayment.ForeColor = System.Drawing.Color.White;
            this.labCardPayment.Name = "labCardPayment";
            // 
            // labPleaseTap
            // 
            resources.ApplyResources(this.labPleaseTap, "labPleaseTap");
            this.labPleaseTap.BackColor = System.Drawing.Color.Transparent;
            this.labPleaseTap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labPleaseTap.ForeColor = System.Drawing.Color.White;
            this.labPleaseTap.Name = "labPleaseTap";
            // 
            // labNote
            // 
            resources.ApplyResources(this.labNote, "labNote");
            this.labNote.BackColor = System.Drawing.Color.Transparent;
            this.labNote.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labNote.ForeColor = System.Drawing.Color.White;
            this.labNote.Name = "labNote";
            // 
            // labCardBelow
            // 
            resources.ApplyResources(this.labCardBelow, "labCardBelow");
            this.labCardBelow.BackColor = System.Drawing.Color.Transparent;
            this.labCardBelow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labCardBelow.ForeColor = System.Drawing.Color.White;
            this.labCardBelow.Name = "labCardBelow";
            // 
            // CreditPayment
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(254)))), ((int)(((byte)(205)))));
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.labCardBelow);
            this.Controls.Add(this.labNote);
            this.Controls.Add(this.labPleaseTap);
            this.Controls.Add(this.labCardPayment);
            this.Controls.Add(this.labTotTaken);
            this.Controls.Add(this.labTotalDue);
            this.Controls.Add(this.labYourLockOrder);
            this.Controls.Add(this.labMakePayment);
            this.Controls.Add(this.bTestPay);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.lblpay);
            this.Controls.Add(this.lbldue);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "CreditPayment";
            this.Load += new System.EventHandler(this.CreditPayment_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CreditPayment_KeyPress);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label lbldue;
        private System.Windows.Forms.Label lblpay;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bTestPay;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timIdle;
        private System.Windows.Forms.Timer timerStartPay;
        private System.Windows.Forms.Label labTotTaken;
        private System.Windows.Forms.Label labTotalDue;
        private System.Windows.Forms.Label labYourLockOrder;
        private System.Windows.Forms.Label labMakePayment;
        private System.Windows.Forms.Label labCardPayment;
        private System.Windows.Forms.Label labPleaseTap;
        private System.Windows.Forms.Label labNote;
        private System.Windows.Forms.Label labCardBelow;
    }
}