﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.ComponentModel;

namespace Lockers
{
    public partial class CreditPayment : Form
    {
        DateTimeFormatInfo fi = new DateTimeFormatInfo();
        Process process = new Process();

        string CCApproved = "CC_Approve.txt";
        string CCDeclined = "CC_Declined.txt";
        string CCFail = "CC_Fail.txt";
        string CCTimeout = "CC_Timeout.txt";
        string CCCancel = "CC_Cancel.txt";
        string CCError = "CC_Error.txt";
        string CCBusy = "CC_Busy.txt";
        string CCNotEmpty = "CC_NotEmpty.txt";
        string fpCancel = "CCReqCancel.txt";

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        protected override void OnClosed(EventArgs e)
        {
/*
            base.OnClosed(e);

            decimal mtender;
            decimal mchange;

            decimal.TryParse(lblpay.Text, out mtender);
            decimal.TryParse(lblchange.Text,out mchange);

            Globals.LockData.Amnt_Tender = mtender;
            Globals.LockData.Amnt_Change = mchange;

            this.totalpaid = 0;
            //Globals.Paymode = false;
*/

        }

        public CreditPayment()
        {
            InitializeComponent();

            String ImageFilename = "";
//            if (Properties.Settings.Default.ReleaseType == "Windows")
//                ImageFilename = Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + "\\English\\CardPayment_1182x789.tiff";
//            else
//                ImageFilename = Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + "English/CardPayment_1182x789.tiff";
//            this.BackgroundImage = Image.FromFile(ImageFilename);

            InvokeLanguage();

            #region enablecashdevice

            //-----------------------------
            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
            timIdle.Start();
            //------------------------------

            fi.AMDesignator = "am";

            timer1.Start();
            timerStartPay.Start();

            //            this.DialogResult = DialogResult.OK;
            #endregion
        }


        bool CCinProcess = false;

        private void CCProcess_Exited(object sender, System.EventArgs e)
        {
            CCinProcess = false;
            Console.WriteLine("Exit time:    {0}\r\n" +
              "Exit code:    {1}\r\n", process.ExitTime, process.ExitCode);
        }

        private void CCThread()
        {
            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Deleting Files");

            // allow 50ms for the files to delete
            try
            {
                File.Delete(CCApproved);
                File.Delete(CCDeclined);
                File.Delete(CCFail);
                File.Delete(CCTimeout);
                File.Delete(CCCancel);
                File.Delete(CCError);
                File.Delete(CCBusy);
                File.Delete(CCNotEmpty);
                File.Delete(fpCancel);
            }
            catch (Exception Ex)
            {
                Console.WriteLine("CCPayment Exception - " + Ex.ToString());
            }
            System.Threading.Thread.Sleep(100);

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Check if the files have been deleted");
            if (File.Exists(CCApproved))
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CCApproved didnt Delete .. Trying Again");
                File.Delete(CCApproved);
            }

            if (File.Exists(CCDeclined))
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CCDeclined didnt Delete .. Trying Again");
                File.Delete(CCDeclined);
            }

            if (File.Exists(CCFail))
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CCFail didnt Delete .. Trying Again");
                File.Delete(CCFail);
            }

            if (File.Exists(CCTimeout))
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CCTimeout didnt Delete .. Trying Again");
                File.Delete(CCTimeout);
            }

            if (File.Exists(CCCancel))
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CCCancel didnt Delete .. Trying Again");
                File.Delete(CCCancel);
            }

            if (File.Exists(CCError))
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CCError didnt Delete .. Trying Again");
                File.Delete(CCError);
            }

            if (File.Exists(CCBusy))
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CCBusy didnt Delete .. Trying Again");
                File.Delete(CCBusy);
            }

            if (File.Exists(CCNotEmpty))
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CCNotEmpty didnt Delete .. Trying Again");
                File.Delete(CCNotEmpty);
            }

            if (File.Exists(fpCancel))
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "fpCancel didnt Delete .. Trying Again");
                File.Delete(fpCancel);
            }

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Files Deleted \n Checking if the Advam process is alive ...");

            process.StartInfo.FileName = Properties.Settings.Default.ProjectRootWindows + "\\AdvamSerialDemo.exe";

            Process[] pname = Process.GetProcessesByName(process.StartInfo.FileName);
            if (pname.Length != 0)
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Advam process is already running ...");

                try
                {
                    foreach (Process proc in Process.GetProcessesByName(process.StartInfo.FileName))
                    {
                        proc.Kill();
                    }
                }
                catch (Exception ex)
                {
                    Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Advam process kill exception ... " + ex.ToString());
                }
            }

            if (Properties.Settings.Default.ReleaseType == "Windows")
                process.StartInfo.Arguments = " " + Properties.Settings.Default.AdvamReaderPortWindows + " " + Globals.DueAmnt.ToString($"F{2}");
            else
                process.StartInfo.Arguments = " " + Properties.Settings.Default.AdvamReaderPortLinux + " " + Globals.DueAmnt.ToString($"F{2}");

            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.RedirectStandardOutput = false;
            process.StartInfo.RedirectStandardError = false;

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Payment Setup complete");

//            process.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
//            process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);

            process.Exited += new EventHandler(CCProcess_Exited);
            process.Start();

            string output = "";
            string err = "";

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Payment Process started");

            try
            {
                //                output = process.StandardOutput.ReadToEnd();
                //                err = process.StandardError.ReadToEnd();

                //                process.BeginOutputReadLine();
                //                process.BeginErrorReadLine();

                while (!CCinProcess)
                {
                    if (File.Exists(CCApproved))
                    {
                        output = "Approved";
                        break;
                    }

                    if (File.Exists(CCDeclined))
                    {
                        output = "Declined";
                        break;
                    }

                    if (File.Exists(CCFail))
                    {
                        output = "CARD_READ_FAIL";
                        break;
                    }

                    if (File.Exists(CCCancel))
                    {
                        output = "TRANSACTION_CANCELLED";
                        break;
                    }

                    if (File.Exists(CCTimeout))
                    {
                        output = "Timeout";
                        break;
                    }

                    if (File.Exists(CCError))
                    {
                        output = "Error";
                        break;
                    }

                    if (File.Exists(CCBusy))
                    {
                        output = "TERMINAL BUSY";
                        break;
                    }

                    if (File.Exists(CCNotEmpty))
                    {
                        output = "NotEmpty";
                        break;
                    }

                    process.WaitForExit(50);
                    Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "!CCinProcess");
                    Application.DoEvents();
                    process.Refresh();
                    Thread.Sleep(25);

                    if (process.HasExited)
                    {
                        Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "process.HasExited");
                        CCinProcess = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                // Log error.
                Console.WriteLine(Ex.ToString());
            }

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "PaymentResult = " + output);

            if (output.Contains("Approved"))
            {
                this.DialogResult = DialogResult.Yes;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "(00) APPROVED");
            }
            else if (output.Contains("Declined"))
            {
                this.DialogResult = DialogResult.No;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "(05) DECLINED");
            }
            else if (
                output.Contains("CARD_PMT_FAIL") ||
                output.Contains("CARD_READ_FAIL") ||
                output.Contains("TRANSACTION_CANCELLED") ||
                output.Contains("CARD NOT ALLOWED") ||
                output.Contains("TERMINAL BUSY") 
//                output.Contains("MSGID007PMT_RESRCODE000RTEXT000TSTAN000RECPT000SCHID000TCARD000TAUTH000CTYPE000EAUTH000STATS0011")
            )
            {
                this.DialogResult = DialogResult.Cancel;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CREDIT Cancelled");
            }
            else
            {
                this.DialogResult = DialogResult.Abort;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CREDIT Timeout");
            }

            timer1.Stop();
            this.Close();
        }

        private void CCStateMachine()
        {
            Globals.LogFile(Globals.LogSource.CreditPayment, "CCStateMachine()", "Payment Setup");
/*
            if (Properties.Settings.Default.ReleaseType == "Windows")
                process.StartInfo.Arguments = " " + Properties.Settings.Default.AdvamReaderPortWindows + " " + Globals.DueAmnt.ToString($"F{2}");
            else
                process.StartInfo.Arguments = " " + Properties.Settings.Default.AdvamReaderPortLinux + " " + Globals.DueAmnt.ToString($"F{2}");
*/
            Globals.LogFile(Globals.LogSource.CreditPayment, "CCStateMachine()", "Payment Setup complete");

            string output = "";
            string err = "";

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCStateMachine()", "Payment Process started");

            // allow 50ms for the files to delete
            try
            {
                File.Delete(CCApproved);
                File.Delete(CCDeclined);
                File.Delete(CCFail);
                File.Delete(CCTimeout);
                File.Delete(CCCancel);
                File.Delete(CCError);
                File.Delete(CCBusy);
                File.Delete(CCNotEmpty);
                File.Delete(fpCancel);
            }
            catch (Exception Ex)
            {
                Console.WriteLine("CCPayment Exception - " + Ex.ToString());
            }
            System.Threading.Thread.Sleep(50);

            try
            {
//                while(!finished)
//                {
//                    this.update();
//                }
//
                while (!CCinProcess)
                {
                    if (File.Exists(CCApproved))
                        {
                        output = "Approved";
                        break;
                        }

                    if (File.Exists(CCDeclined))
                        {
                        output = "Declined";
                        break;
                        }

                    if (File.Exists(CCFail))
                        {
                        output = "CARD_READ_FAIL";
                        break;
                }

                if (File.Exists(CCCancel))
                    {
                    output = "TRANSACTION_CANCELLED";
                        break;
                    }

                if (File.Exists(CCTimeout))
                    {
                    output = "Timeout";
                        break;
                    }

                if (File.Exists(CCError))
                    {
                    output = "Error";
                        break;
                    }

                if (File.Exists(CCBusy))
                    {
                    output = "TERMINAL BUSY";
                        break;
                    }

                if (File.Exists(CCNotEmpty))
                    {
                    output = "NotEmpty";
                        break;
                    }

                    process.WaitForExit(50);
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCStateMachine()", "!CCinProcess");
                Application.DoEvents();
                    process.Refresh();
                    Thread.Sleep(25);

                    if (process.HasExited)
                    {
                        Globals.LogFile(Globals.LogSource.CreditPayment, "CCStateMachine()", "process.HasExited");
                        CCinProcess = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                // Log error.
                Console.WriteLine(Ex.ToString());
            }

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCStateMachine()", "PaymentResult = " + output);

            if (output.Contains("Approved"))
            {
                this.DialogResult = DialogResult.Yes;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCStateMachine()", "(00) APPROVED");
            }
            else if (output.Contains("Declined"))
            {
                this.DialogResult = DialogResult.No;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCStateMachine()", "(05) DECLINED");
            }
            else if (
                output.Contains("CARD_PMT_FAIL") ||
                output.Contains("CARD_READ_FAIL") ||
                output.Contains("TRANSACTION_CANCELLED") ||
                output.Contains("CARD NOT ALLOWED") ||
                output.Contains("TERMINAL BUSY")
            //                output.Contains("MSGID007PMT_RESRCODE000RTEXT000TSTAN000RECPT000SCHID000TCARD000TAUTH000CTYPE000EAUTH000STATS0011")
            )
            {
                this.DialogResult = DialogResult.Cancel;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCStateMachine()", "CREDIT Cancelled");
            }
            else
            {
                this.DialogResult = DialogResult.Abort;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCStateMachine()", "CREDIT Timeout");
            }

            timer1.Stop();
            this.Close();
        }


        private void updatePay(float value)
        {
            Globals.LogFile(Globals.LogSource.CreditPayment, "updatePay()", "updatePay method on UI");

            lblpay.Text = value.ToString("#,##0.00");
        }

        private void enableButton(bool value)
        {
            Globals.LogFile(Globals.LogSource.CreditPayment, "enableButton()", "Enable OK button");

            bOK.Enabled = value;
        }
        private void updatechange(Double  value)
        {
            Globals.LogFile(Globals.LogSource.CreditPayment, "updatechange()", "updatechange method");
//            lblchange.Text = value.ToString("#,##0.00");
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.CreditPayment, "bCancel_Click()", "bCancel_Click event");
            /*
                        try
                        {
                            process.Kill();
                        }
                        catch (Exception Ex)
                        {
                            Globals.LogFile(Globals.LogSource.CreditPayment, "bCancel_Click()", "CCProcess Kill Exception - " + Ex.ToString());
                        }
            */
            File.CreateText(fpCancel);
            Globals.AbortForm = true;
            this.DialogResult = DialogResult.Ignore;
            timer1.Stop();
            timerStartPay.Stop();
            timIdle.Stop();

            CHelpers.Shutdown = true;
            this.Close();
        }

        private void CreditPayment_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
            lbldue.Text = Globals.DueAmnt.ToString("#,##0.00");

            lbldue.ForeColor = Properties.Settings.Default.TextColor;
            lblpay.ForeColor = Properties.Settings.Default.TextColor;
            lbltime.ForeColor = Properties.Settings.Default.TextColor;
            labCardBelow.ForeColor = Properties.Settings.Default.TextColor;
            labCardPayment.ForeColor = Properties.Settings.Default.TextColor;
            label1.ForeColor = Properties.Settings.Default.TextColor;
            labMakePayment.ForeColor = Properties.Settings.Default.TextColor;
            labNote.ForeColor = Properties.Settings.Default.TextColor;
            labPleaseTap.ForeColor = Properties.Settings.Default.TextColor;
            labTotalDue.ForeColor = Properties.Settings.Default.TextColor;
            labTotTaken.ForeColor = Properties.Settings.Default.TextColor;
            labYourLockOrder.ForeColor = Properties.Settings.Default.TextColor;
            labBack.ForeColor = Properties.Settings.Default.TextColor;

        }

        private void CreditPayment_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString().ToUpper().Equals("P"))
            {
                // Quit - ends kiosk    program
                e.Handled = true;
                timer1.Stop();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void bOK_Click(object sender, EventArgs e)
        {
        }

        static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //* Do your stuff with the output (write to console/log/StringBuilder)
            Console.WriteLine(outLine.Data);
        }

        private void bTestPayout_Click(object sender, EventArgs e)
        {
            Globals.smartpay.Payout = (float)(2.00) * 100;// (float)changeamnt;
            Globals.smartpay.CalculatePayout();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.CreditPayment, "timIdle_Tick()", "Timeout");

            timer1.Stop();
            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void timerStartPay_Tick(object sender, EventArgs e)
        {
            timerStartPay.Stop();
            CCThread();
        }

        private void InvokeLanguage()
        {
            String language = Globals.GetLanguageCode();

            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            ComponentResourceManager resources = new ComponentResourceManager(typeof(CreditPayment));

            foreach (Control C in this.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    foreach (Control D in C.Controls)
                    {
                        resources.ApplyResources(D, D.Name, new CultureInfo(language));
                    }
                }
                else
                {
                    resources.ApplyResources(C, C.Name, new CultureInfo(language));
                }
            }

            this.Update();
        }
    }
}
