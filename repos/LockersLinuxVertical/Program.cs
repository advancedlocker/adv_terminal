﻿using System;
using System.Windows.Forms;

namespace Lockers
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

//  SMC - : moved to globals for the moment for clarity
//        private static SmartPay smartpay;
//        private static Locker_TCPClient _client;
        //static System.Timers.Timer aTimer;

        [STAThread]
        static void Main()
        {

// smc removed
//            smartpay = new SmartPay();
//            StartPaysystem();

            //aTimer = new System.Timers.Timer(1000);
            //aTimer.Elapsed += aTimer_Elapsed;
            //aTimer.Interval = 1000;
            //aTimer.Enabled = true;

            Globals.OpenCommsLogInterface();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Mainscreen());

//          SMC : removed payment interface for clarity
//            Application.Run(new Mainscreen(smartpay));


        }

        static void aTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //throw new NotImplementedException();
            //lbltime.Text = DateTime.Now.ToString("HH:mm");

        }

        private static void StartPaysystem()
        {
            //this.smartpay.INMAINTENANCE = false;
            Globals.Maintenance = false;
            //this.smartpay.RunPaysystem();

//  SMC : Disabled smart payment system for the moment
//            smartpay.Start();


        }
    }
}
