using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Web.Script.Serialization;
using System.Threading;
using System.Dynamic;

using Newtonsoft.Json;
using System.Globalization;

namespace Server
{
    //The commands for interaction between the server and the client
    enum Command
    {
        connect,
        login,      //Log into the server
        Logout,     //Logout of the server
        Message,    //Send a text message to all the chat clients
        List,      //Get a list of users in the chat room from the server
        Null       //No command
    }

    public partial class ServerForm : Form
    {
        //The ClientInfo structure holds the required information about every
        //client connected to the server
        struct ClientInfo
        {
            public Socket socket;   //Socket of the client
            public string strName;  //Name by which the user logged into the chat room
        }

        // State object for reading client data asynchronously
        public class StateObject
        {
            // Client  socket.
            public Socket workSocket = null;
            // Size of receive buffer.
            public const int BufferSize = 1024;
            // Receive buffer.
            public byte[] buffer = new byte[BufferSize];
            // Received data string.
            public StringBuilder sb = new StringBuilder();
        }

        public static ManualResetEvent allDone = new ManualResetEvent(false);

        //The collection of all clients logged into the room (an array of type ClientInfo)
        ArrayList clientList;
        int cnt = 0;
        //The main socket on which the server listens to the clients
        Socket serverSocket;
        Socket currentclientSocket;
        //Data msgReceived;
        public System.UInt64 sq;
        string m_vmc_no;
        string m_order;
        string m_qrtype;
        int m_amount = 0;

        int bytesRead = 0;
        byte[] byteData = new byte[1024];
        // Status delegate
        private delegate void UpdateStatusDelegate(string status);
        private UpdateStatusDelegate updateStatusDelegate = null;

        private delegate void UpdatecmdDelegate(string cmd);
        private UpdatecmdDelegate updatecmdDelegate = null;

        private delegate void UpdateIPDelegate(string cmd);
        private UpdateIPDelegate updateIPDelegate = null;

        private delegate void UpdatesendDelegate(string snd);
        private UpdatesendDelegate updatesendDelegate = null;

        private delegate void UpdateHexDelegate(string str);
        private UpdateHexDelegate updateHexDelegate = null;

        ClientInfo clientInfo = new ClientInfo();
        string prevCMD;
        dynamic response;
        
        public ServerForm()
        {
            clientList = new ArrayList();
            InitializeComponent();
        }

        #region Other Methods

        private void UpdateStatus(string status)
        {
            cnt += 1;
            if (cnt == 100)
            {
                cnt = 0;
                txtLog.Clear();
            }
            txtLog.AppendText(status+"\n" );
            txtLog.ScrollToCaret();

        }

        private void UpdateCMD(string cmd)
        {
            if (cnt == 100) { cnt = 0; txtcmd.Clear(); }
            txtcmd.AppendText("\n"+ cmd);
            txtcmd.ScrollToCaret();
        }

        private void UpdateHex(string str)
        {
           txthex.AppendText(str);
           txthex.ScrollToCaret();
        }

        private void UpdateIP(string ip)
        {
            lblIP.Text = ip;
        }

        private void UpdateSendcmd(string cmd)
        {
            richTextBox1.Text = cmd;
        }

        #endregion
        //=======================================
        [
    System.Runtime.InteropServices.StructLayout
    (
        System.Runtime.InteropServices.LayoutKind.Explicit
    )
]
        unsafe struct TcpKeepAlive
        {
            [System.Runtime.InteropServices.FieldOffset(0)]
            [
                System.Runtime.InteropServices.MarshalAs
                (
                    System.Runtime.InteropServices.UnmanagedType.ByValArray,
                    SizeConst = 12
                )
            ]
            public fixed byte Bytes[12];

            [System.Runtime.InteropServices.FieldOffset(0)]
            public uint On_Off;

            [System.Runtime.InteropServices.FieldOffset(4)]
            public uint KeepaLiveTime;

            [System.Runtime.InteropServices.FieldOffset(8)]
            public uint KeepaLiveInterval;
        }

        public int SetKeepAliveValues
            (
                System.Net.Sockets.Socket Socket,
                bool On_Off,
                uint KeepaLiveTime,
                uint KeepaLiveInterval
            )
        {
            int Result = -1;

            unsafe
            {
                TcpKeepAlive KeepAliveValues = new TcpKeepAlive();

                KeepAliveValues.On_Off = Convert.ToUInt32(On_Off);
                KeepAliveValues.KeepaLiveTime = KeepaLiveTime;
                KeepAliveValues.KeepaLiveInterval = KeepaLiveInterval;

                byte[] InValue = new byte[12];

                for (int I = 0; I < 12; I++)
                    InValue[I] = KeepAliveValues.Bytes[I];

                Result = Socket.IOControl(IOControlCode.KeepAliveValues, InValue, null);
            }

            return Result;
        }
        //=======================================
        private void JsonParse(Data msg)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                dynamic item = serializer.Deserialize<object>(msg.strName );
                 
                if (item["cmd"] != null)
                {
                    string test = item["cmd"];
                    this.Invoke(this.updatecmdDelegate, new object[] { test });
                }
            }
            catch { }
        }

        public static string ToHexString(string str)
        {
            var sb = new StringBuilder();

            var bytes = Encoding.Unicode.GetBytes(str);
            foreach (var t in bytes)
            {
                sb.Append(t.ToString("X2"));
            }

            return sb.ToString(); // returns: "48656C6C6F20776F726C64" for "Hello world"
        }


        private void Form1_Load(object sender, EventArgs e)
        {            
            try
            {
                this.updateStatusDelegate = new UpdateStatusDelegate(UpdateStatus);
                this.updatecmdDelegate = new UpdatecmdDelegate(UpdateCMD);
                this.updateIPDelegate = new UpdateIPDelegate(UpdateIP);
                this.updatesendDelegate = new UpdatesendDelegate(UpdateSendcmd);
                this.updateHexDelegate = new UpdateHexDelegate(UpdateHex);
                //We are using TCP sockets
                serverSocket = new Socket(AddressFamily.InterNetwork, 
                                          SocketType.Stream, 
                                          ProtocolType.Tcp);
                SetKeepAliveValues(serverSocket, true, 36000000, 1000);

                this.currentclientSocket=new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                //Assign the any IP of the machine and listen on port number 4567
                if (Properties.Settings.Default.IPType == "Dynamic")
                {
                    IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any,Properties.Settings.Default.PORT );
                    serverSocket.Bind(ipEndPoint);
                }
                else
                {
                    IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.IPAdd ), Properties.Settings.Default.PORT);
                    serverSocket.Bind(ipEndPoint);

                   
                }


                

                //Bind and listen on the given address
                
                //serverSocket.Listen(100);
                //allDone.Reset();
                ////Accept the incoming clients
                //serverSocket.BeginAccept(new AsyncCallback(OnAccept), null);
                //allDone.WaitOne();
            }
            catch (Exception ex)
            { 
                MessageBox.Show(ex.Message, "TCP Server Load",                     
                    MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }            
        }

        private void OnAccept(IAsyncResult ar)
        {
            //allDone.Set();
            int f = 1;
            try
            {
                Socket clientSocket = serverSocket.EndAccept(ar);
                f = 2;
                currentclientSocket = clientSocket;
                //Start listening for more clients
                f=3;
                serverSocket.BeginAccept(new AsyncCallback(OnAccept), null);
                f = 4;
                //Once the client connects then start receiving the commands from her
                clientSocket.BeginReceive(byteData, 0, byteData.Length, SocketFlags.None, 
                    new AsyncCallback(OnReceive), clientSocket);                
            }
            catch (Exception ex)
            { 
                MessageBox.Show(ex.Message + " : f"+f.ToString(), "TCP Server accept", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
        }

        private void OnReceive(IAsyncResult ar)
        {
            Socket clientSocket = (Socket)ar.AsyncState;
            var sb = new StringBuilder();;
            currentclientSocket = clientSocket;
            try
            {
                bytesRead = clientSocket.EndReceive(ar);

//===========================================
                // Initialise the IPEndPoint for the clients
                //IPEndPoint clients = new IPEndPoint(IPAddress.Any, 0);

                //// Initialise the EndPoint for the clients
                //EndPoint epSender = (EndPoint)clients;
                //bytesRead = serverSocket.EndReceiveFrom(ar, ref epSender);
//===========================================
                                
                 //Transform the array of bytes received from the user into an
                //intelligent form of object Data
                Data msgReceived = new Data(byteData, bytesRead);


                clientInfo.socket = clientSocket;
                clientInfo.strName = msgReceived.strName;


                //We will send this object in response the users request
                Data msgToSend = new Data();

                byte [] message;
                
                //If the message is to login, logout, or simple text message
                //then when send to others the type of the message remains the same
                msgToSend.cmdCommand = msgReceived.cmdCommand;
                

                //JsonParse(msgReceived);

                switch (msgReceived.cmdCommand)
                {

                    //case  "productdone":
                    //    sq++;
                    //    response = new ExpandoObject();
                    //    response.cmd = "products";
                    //    response.vmc_no = Int32.Parse( msgReceived.vmcNum);
                    //    response.product_id = msgReceived.prodID;
                    //    response.fastcode = 222;
                    //    response.paydone = true;
                    //    response.paytype = "fastcode";
                    //    msgToSend.strMessage = JsonConvert.SerializeObject(response);
              
                    //    message = msgToSend.ToByte(sq);

                    //        foreach (char letter in msgToSend.header)
                    //        {
                    //            int value = Convert.ToInt32(letter);
                    //            sb.Append(String.Format("{0:X}", value));
                    //        }
                    //        this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" +"\n\r"});
                    //        clientInfo.socket.BeginSend(message, 0, message.Length, SocketFlags.None,
                    //                new AsyncCallback(OnSend), clientInfo.socket);

                    //    this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName  });
                    //    break;
                    case "qrcode":
                        sq++;
                        response = new ExpandoObject();
                        response.cmd = "qrcode_r";
                        response.vmc_no = Int32.Parse( msgReceived.vmcNum);
                        response.qr_type = msgReceived.qrtype ;//"wx_pub";
                        if (msgReceived.qrtype == "wx_pub")
                        {
                            response.qrcode = "100c6400ec11ec11ec11ec11ec11ec11ec11ec";
                        }
                        else
                        {
                            response.qrcode = "20282dece000ec11ec11ec11ec11ec11ec11ec";
                        }
                        response.order_no = msgReceived.orderno;
                        m_order = msgReceived.orderno;
                        m_qrtype = msgReceived.qrtype;
                        m_amount = msgReceived.prodAmnt;
                        //msgToSend.strMessage = "{'cmd':'login_r','vmc_no':" + msgReceived.vmcNum + ",'ret':0}";
                        msgToSend.strMessage = JsonConvert.SerializeObject(response);
              
                        message = msgToSend.ToByte(sq);

                            foreach (char letter in msgToSend.header)
                            {
                                // Get the integral value of the character.
                                int value = Convert.ToInt32(letter);
                                // Convert the decimal value to a hexadecimal value in string form.
                                sb.Append(String.Format("{0:X}", value));
                                //Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
                            }
                            this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" +"\n\r"});

                        //string hex = ToHexString(msgToSend.strName);

                            clientInfo.socket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                    new AsyncCallback(OnSend), clientInfo.socket);
                        //this.Invoke(this.updateStatusDelegate, new object[] { msgReceived.strName });
                        this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName  });
                        //this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() });
                        break;

                    case "login":
                        sq++;
                            response = new ExpandoObject();
                            //response.cmd = "login_r";
                            //response.date_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            //response.vmc_no = Int32.Parse(msgReceived.vmcNum); ;
                            //response.ret = 0;

                            response.cmd = "login_r";
                            response.date_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            response.ret = 0;

                            msgToSend = new Data();
                            msgToSend.strMessage = JsonConvert.SerializeObject(response);
                            message = msgToSend.ToByte(sq);
                            m_vmc_no = msgReceived.vmcNum;
                            //msgToSend.strMessage = "v00010000000{\"cmd\":\"login_r\",\"date_time\":\"" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\",\"ret\":0}";
                            //byte[] message = System.Text.Encoding.ASCII.GetBytes(msgToSend.strMessage); 
                            // msgToSend.ToByte();
                            clientInfo.socket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                    new AsyncCallback(OnSend), clientInfo.socket);

                           
                            foreach (char letter in msgToSend.header)
                            {
                                // Get the integral value of the character.
                                int value = Convert.ToInt32(letter);
                                // Convert the decimal value to a hexadecimal value in string form.
                                sb.Append(String.Format("{0:X}", value));
                                //Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
                            }
                            this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" + "\n\r" });

                            
                                            
                               
                  
                        if (prevCMD != msgReceived.cmdCommand)
                        {
                            Properties.Settings.Default.clientIP =(((IPEndPoint)clientInfo.socket.RemoteEndPoint).Address.ToString());
                            Properties.Settings.Default.clientPort = ((IPEndPoint)clientInfo.socket.RemoteEndPoint).Port;
                            Properties.Settings.Default.Save();
                            string strIP = IPAddress.Parse(((IPEndPoint)clientInfo.socket.RemoteEndPoint).Address.ToString()) +
                                            ":" + ((IPEndPoint)clientInfo.socket.RemoteEndPoint).Port.ToString();
                            this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName});
                            this.Invoke(this.updateIPDelegate, new object[] { strIP });
                            this.Invoke(this.updatecmdDelegate, new object[] { "login_r" });
                        }               
 
                        break;

                    case "hb":
                        sq++;
                        m_vmc_no=  msgReceived.vmcNum;

                        response = new ExpandoObject();
                        response.cmd = "hb";
                        msgToSend.strMessage = JsonConvert.SerializeObject(response);
                      
                        message = msgToSend.ToByte(sq);

                        clientInfo.socket = clientSocket;
                        clientInfo.strName = msgReceived.strName;

                        //clientList.Add(clientInfo);

                        //foreach (ClientInfo client in clientList)
                        //{
                        //    if (client.socket == clientSocket)
                        //    {
                                //Send the message to all users
                                clientInfo.socket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                    new AsyncCallback(OnSend), clientInfo.socket);
                        //    }
                        //}
                      foreach (char letter in msgToSend.header)
                            {
                                int value = Convert.ToInt32(letter);
                                sb.Append(String.Format("{0:X}", value));
                            }
                      this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" + "\n\r" });

                        if (prevCMD != msgReceived.cmdCommand)
                        {
                     
                            this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName  });
                        }
                        break;

                    case "logout":
                        sq++;
                        int nIndex = 0;
                        foreach (ClientInfo client in clientList)
                        {
                            if (client.socket == clientSocket)
                            {
                                clientList.RemoveAt(nIndex);
                                break;
                            }
                            ++nIndex;
                        }
                        
                        clientSocket.Close();
                        
                        msgToSend.strMessage = "<<<Disconnected>>>";
                        break;

                    case "Message":

                        //Set the text of the message that we will broadcast to all users
                        //msgToSend.strMessage = msgReceived.strName + ": " + msgReceived.strMessage;
                        msgToSend.strMessage = ": " + msgReceived.strMessage;
                        //JsonParse(msgReceived);
                        break;

                    case "List":
                        sq++;
                        //Send the names of all users in the chat room to the new user
                        msgToSend.cmdCommand = "List";
                        msgToSend.strName = null;
                        msgToSend.strMessage = null;

                        //Collect the names of the user in the chat room
                        foreach (ClientInfo client in clientList)
                        {
                            //To keep things simple we use asterisk as the marker to separate the user names
                            msgToSend.strMessage += client.strName + "*";   
                        }

                        message = msgToSend.ToByte(sq);

                        //Send the name of the users in the chat room
                        clientSocket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                new AsyncCallback(OnSend), clientSocket);                        
                        break;
                }

                if (msgToSend.cmdCommand != "List")   //List messages are not broadcasted
                {
                    //send back the message
                    //message = msgToSend.ToByte();
                    #region sendtoall
                    //foreach (ClientInfo clientInfo in clientList)
                    //{
                    //    if (clientInfo.socket != clientSocket ||
                    //        msgToSend.cmdCommand != Command.Login)
                    //    {
                    //        //Send the message to all users
                    //        clientInfo.socket.BeginSend(message, 0, message.Length, SocketFlags.None,
                    //            new AsyncCallback(OnSend), clientInfo.socket);                            
                    //    }
                    //}
                    #endregion
                    //txtLog.Text += msgToSend.strMessage + "\r\n";
                    msgToSend.strMessage = msgReceived.strMessage;
                    if (prevCMD != msgReceived.cmdCommand || msgReceived.cmdCommand=="qrcode")
                    {
                        prevCMD = msgReceived.cmdCommand;
                        this.Invoke(this.updateStatusDelegate, new object[] { msgReceived.strName });
                        this.Invoke(this.updatecmdDelegate, new object[] { msgReceived.cmdCommand });
                    }
                    
                }

                //If the user is logging out then we need not listen from her
                if (msgReceived.cmdCommand != "Logout")
                {
                    //Start listening to the message send by the user
                   
                    if (clientSocket.Connected )
                        clientSocket.BeginReceive(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnReceive), clientSocket);
                }
            }
            catch (Exception ex)
            {

                //clientSocket.BeginReceive(byteData, 0, byteData.Length, SocketFlags.None, new AsyncCallback(OnReceive), clientSocket);
                MessageBox.Show(ex.Message, "TCP Server on recieve", MessageBoxButtons.OK, MessageBoxIcon.Error);
                serverSocket.BeginAccept(new AsyncCallback(OnAccept), null);
            }
        }

        public void OnSend(IAsyncResult ar)
        {
            try
            {
                Socket client = (Socket)ar.AsyncState;
                client.EndSend(ar);                
            }
            catch (Exception ex)
            { 
                MessageBox.Show(ex.Message, "TCP Server onsend", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //// Initialise the IPEndPoint for the clients
            //IPEndPoint clients = new IPEndPoint(IPAddress.Any, 0);

            //// Initialise the EndPoint for the clients
            //EndPoint epSender = (EndPoint)clients;

            if (button1.Text == "Start")
            {
                serverSocket.Listen(100);
                allDone.Reset();
                //Accept the incoming clients
                serverSocket.BeginAccept(new AsyncCallback(OnAccept), null);
                button1.Text = "End";
                //allDone.WaitOne();
            }
            else {
                //serverSocket.Shutdown(SocketShutdown.Both);
                serverSocket.Close();
                button1.Text = "Start";

                serverSocket = new Socket(AddressFamily.InterNetwork,
                                          SocketType.Stream,
                                          ProtocolType.Tcp);

                //Assign the any IP of the machine and listen on port number 4567
                if (Properties.Settings.Default.IPType == "Dynamic")
                {
                    IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, Properties.Settings.Default.PORT);
                    serverSocket.Bind(ipEndPoint);
                }
                else
                {
                    IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.IPAdd), Properties.Settings.Default.PORT);
                    serverSocket.Bind(ipEndPoint);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                sq++;
                response = new ExpandoObject();
                response.cmd = "reboot";
                response.vmc_no = Int32.Parse(m_vmc_no);
                Data msgToSend = new Data();
                msgToSend.strMessage = JsonConvert.SerializeObject(response);
                //When a user logs in to the server then we add her to our
                //list of clients
                byte[] message = msgToSend.ToByte(sq);

                //ClientInfo clientInfo = new ClientInfo();
                currentclientSocket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                        new AsyncCallback(OnSend), currentclientSocket);

                var sb = new StringBuilder();
                //char[] values = msgToSend.strName.ToCharArray();
                foreach (char letter in msgToSend.header)
                {
                    // Get the integral value of the character.
                    int value = Convert.ToInt32(letter);
                    // Convert the decimal value to a hexadecimal value in string form.
                    sb.Append(String.Format("{0:X}", value));
                    //Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
                }
                this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" + "\n\r" });
                
                
                prevCMD = response.cmd;
                this.Invoke(this.updatecmdDelegate, new object[] { response.cmd });
                this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "session error ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            sq++;
            response = new ExpandoObject();
            response.cmd = "hb";
            Data msgToSend = new Data();
            msgToSend.strMessage = JsonConvert.SerializeObject(response);
            //msgToSend.strMessage = "{'cmd':'hb'}";
            //When a user logs in to the server then we add her to our
            //list of clients
            byte[] message = msgToSend.ToByte(sq);

            //ClientInfo clientInfo = new ClientInfo();
            currentclientSocket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                    new AsyncCallback(OnSend), currentclientSocket);
            //==========================================================
            var sb = new StringBuilder();

            foreach (char letter in msgToSend.header)
            {
                // Get the integral value of the character.
                int value = Convert.ToInt32(letter);
                // Convert the decimal value to a hexadecimal value in string form.
                sb.Append(String.Format("{0:X}", value));
                //Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
            }
            this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" + "\n\r" });
            //=========================================================
            this.Invoke(this.updatecmdDelegate, new object[] { response.cmd });
            this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName  });
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                sq++;
                response = new ExpandoObject();
                response.cmd = "opendoor";
                response.vmc_no = Int32.Parse(m_vmc_no);
                Data msgToSend = new Data();
                msgToSend.strMessage = JsonConvert.SerializeObject(response);
                //When a user logs in to the server then we add her to our
                //list of clients
                byte[] message = msgToSend.ToByte(sq);

                //ClientInfo clientInfo = new ClientInfo();
                currentclientSocket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                        new AsyncCallback(OnSend), currentclientSocket);

                //=======================
                var sb = new StringBuilder();
                //char[] values = msgToSend.strName.ToCharArray();
                foreach (char letter in msgToSend.header)
                {
                    // Get the integral value of the character.
                    int value = Convert.ToInt32(letter);
                    // Convert the decimal value to a hexadecimal value in string form.
                    sb.Append(String.Format("{0:X}", value));
                    //Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
                }
                this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" + "\n\r" });
                //=======================
                prevCMD = response.cmd;
                this.Invoke(this.updatecmdDelegate, new object[] { response.cmd });
                this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName  });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "TCP Server send opendoor " , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
          
                sq++;
                response = new ExpandoObject();
                response.cmd = "login_r";
                response.date_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                response.ret = 0;
                Data msgToSend = new Data();
                msgToSend.strMessage=JsonConvert.SerializeObject(response);
                byte[] message = msgToSend.ToByte(sq);

                //ClientInfo clientInfo = new ClientInfo();
                currentclientSocket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                        new AsyncCallback(OnSend), currentclientSocket);
                //======================================
                var sb = new StringBuilder();
                //char[] values = msgToSend.strName.ToCharArray();
                foreach (char letter in msgToSend.header)
                {
                    // Get the integral value of the character.
                    int value = Convert.ToInt32(letter);
                    // Convert the decimal value to a hexadecimal value in string form.
                    sb.Append(String.Format("{0:X}", value));
                    //Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
                }
                this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" + "\n\r" });
                //======================================
                prevCMD = "Login_r";
                this.Invoke(this.updatecmdDelegate, new object[] { "login_r" });
                this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName  });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "TCP Server send opendoor ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                sq++;
                response = new ExpandoObject();
                response.cmd = "qrcode_r";
                response.vmc_no = Int32.Parse(m_vmc_no);
                response.qr_type = m_qrtype ;
                response.qrcode = "100c6400ec11ec11ec11ec11ec11ec11ec11ec";
                response.order_no = m_order;
                Data msgToSend = new Data();
                msgToSend.strMessage = JsonConvert.SerializeObject(response);
                byte[] message = msgToSend.ToByte(sq);


                var sb = new StringBuilder();
                foreach (char letter in msgToSend.header)
                {
                    // Get the integral value of the character.
                    int value = Convert.ToInt32(letter);
                    // Convert the decimal value to a hexadecimal value in string form.
                    sb.Append(String.Format("{0:X}", value));
                    //Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
                }
                this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" + "\n\r" });
                //ClientInfo clientInfo = new ClientInfo();
                currentclientSocket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                        new AsyncCallback(OnSend), currentclientSocket);
                prevCMD = "qrcode_r";
                this.Invoke(this.updatecmdDelegate, new object[] { "qrcode_r" });
                this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "TCP Server send qr ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                sq++;
                response = new ExpandoObject();
                response.cmd = "pay_done";
                response.vmc_no = Int32.Parse(m_vmc_no);
                response.product_amount = m_amount;
                response.order_no = m_order;
                response.paydone = "true";
                response.paytype = "qr_pay";
                Data msgToSend = new Data();
                msgToSend.strMessage = JsonConvert.SerializeObject(response);
                byte[] message = msgToSend.ToByte(sq);


                var sb = new StringBuilder();
                foreach (char letter in msgToSend.header)
                {
                    // Get the integral value of the character.
                    int value = Convert.ToInt32(letter);
                    // Convert the decimal value to a hexadecimal value in string form.
                    sb.Append(String.Format("{0:X}", value));
                    //Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
                }
                this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" + "\n\r" });
                //ClientInfo clientInfo = new ClientInfo();
                currentclientSocket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                        new AsyncCallback(OnSend), currentclientSocket);
                prevCMD = "pay_done";
                this.Invoke(this.updatecmdDelegate, new object[] { "pay_done" });
                this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "TCP Server send qr ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
             {
                sq++;
                response = new ExpandoObject();
                response.cmd = "GetSessionKey,1AE55FCA4568ABC";
                response.vmc_no = Int32.Parse(m_vmc_no);
                Data msgToSend = new Data();
                msgToSend.strMessage = JsonConvert.SerializeObject(response);
                //When a user logs in to the server then we add her to our
                //list of clients
                byte[] message = msgToSend.ToByte(sq);

                //ClientInfo clientInfo = new ClientInfo();
                currentclientSocket.BeginSend(message, 0, message.Length, SocketFlags.None,
                                        new AsyncCallback(OnSend), currentclientSocket);

                var sb = new StringBuilder();
                //char[] values = msgToSend.strName.ToCharArray();
                foreach (char letter in msgToSend.header)
                {
                    // Get the integral value of the character.
                    int value = Convert.ToInt32(letter);
                    // Convert the decimal value to a hexadecimal value in string form.
                    sb.Append(String.Format("{0:X}", value));
                    //Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
                }
                this.Invoke(this.updateHexDelegate, new object[] { sb.ToString() + "\n" + msgToSend.strName + "\n" + "----------------------------------------------------" + "\n\r" });
                
                
                prevCMD = response.cmd;
                this.Invoke(this.updatecmdDelegate, new object[] { response.cmd });
                this.Invoke(this.updatesendDelegate, new object[] { msgToSend.strName });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "TCP Server send opendoor ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }

    //The data structure by which the server and the client interact with 
    //each other
    class Data
    {
        //Default constructor
        public Data()
        {
            this.cmdCommand = "" ;
            this.strMessage = "";
            this.strName = "";
            this.vmcNum = "";
            this.orderno = "";
            this.qrtype = "";
            this.prodAmnt = 0;
            this.prodID = 0;
            this.paytype = "";
            this.header = new Char[12];
        }

        //Converts the bytes into an object of type Data
        public Data(byte[] data, int dlen)
        {
            int f = 0;
            if (dlen <= 0) return;
            try
            {
                int nameLen = BitConverter.ToInt32(data,0);
                //The first four bytes are for the Command
                f = 1;
                if (dlen > 12 && data.Length <= 1024)
                {
                    this.strMessage = Encoding.UTF8.GetString(data, 12, dlen - 12);

                    this.strName = Encoding.UTF8.GetString(data, 0, dlen);
                    //this.strName = Encoding.ASCII.GetString(data, 0, dlen);
                    try
                    {
                        //JavaScriptSerializer serializer = new JavaScriptSerializer();
                        //dynamic item = serializer.Deserialize<object>(this.strName);

                        dynamic item = JsonConvert.DeserializeObject<object>(this.strMessage );
                        if (item["cmd"] != null) this.cmdCommand = item["cmd"];
                        if (item["vmc_no"] != null) this.vmcNum = item["vmc_no"].ToString();
                        if (item["order_no"] != null) this.orderno= item["order_no"].ToString();
                        if (item["qr_type"] != null) this.qrtype  = item["qr_type"].ToString();
                        if (item["Amount"] != null) this.prodAmnt = item["Amount"].ToString();
                        if (item["product_id"] != null) this.prodID = item["product_id"].ToString();
                        if (item["paytype"] != null) this.paytype = item["paytype"].ToString();
                    }
                    catch { }

                }
                //else
                //{
                //    this.strMessage = Encoding.UTF8.GetString(data, 12, data.Length());

                //    this.strName = Encoding.UTF8.GetString(data, 12, dlen - 12); 
                
                //}

                

                //this.cmdCommand = (Command)BitConverter.ToInt32(data, 0);
                //f = 2;
                ////The next four store the length of the name
                //int nameLen = BitConverter.ToInt32(data, 4);
                //f = 3;
                ////The next four store the length of the message
                //int msgLen = BitConverter.ToInt32(data, 8);
                //f = 4;
                ////This check makes sure that strName has been passed in the array of bytes
                //if (nameLen > 0)
                //{
                //    f = 5;
                //    this.strName = Encoding.UTF8.GetString(data, 12, nameLen);
                //}
                //else
                //{
                //    f = 6;
                //    this.strName = null;
                //}
                ////This checks for a null message field
                //if (msgLen > 0)
                //{
                //    f = 7;
                //    this.strMessage = Encoding.UTF8.GetString(data, 12 + nameLen, msgLen);
                //}
                //else
                //{
                //    f = 8;
                //    this.strMessage = null; }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "TCP Server data conversion "+ f.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Converts the Data structure into an array of bytes
        public byte[] ToByte(System.UInt64 sq)
        {
            System.Text.Encoding Asciicoding = new System.Text.ASCIIEncoding();
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            List<byte> result = new List<byte>();
            if (strMessage != null)
            {
                m_seqno = 0;

                int length = strMessage.Length + 12;

                Char[] lengBuf = new Char[4]; //4
                Char[] seqnoBuf = new Char[8]; //8
                         //header = new Char[12];
                byte[] headerbyte = new byte[12];

                length += '0';

                //string s = Encoding.ASCII.GetString(BitConverter.GetBytes(length));
                string hexValue = length.ToString("X");
             
                //lengBuf = Encoding.ASCII.GetChars(BitConverter.GetBytes(length));

                int intValue = int.Parse(hexValue, NumberStyles.AllowHexSpecifier);
                char cnum = (char)intValue;
                lengBuf[0] += cnum; 
                for (int i = 1; i < 4; ++i)
                {
                    lengBuf[i] += Convert.ToChar('0');
                }

                seqnoBuf = Encoding.ASCII.GetChars(BitConverter.GetBytes(m_seqno)); ;//Asciicoding.GetBytes(m_seqno.ToString().PadRight(8, '0'));
                for (int i = 0; i < 8; ++i)
                {
                    seqnoBuf[i] += Convert.ToChar('0'); ;//Convert.ToByte('0');
                }
                lengBuf.CopyTo(header, 0);
                seqnoBuf.CopyTo(header, 4);


                int ndx = 0;
                    foreach (char ch in header)
                    {
                        try
                        {
                            headerbyte[ndx]= Convert.ToByte(ch);
                            ndx++;
                            //Console.WriteLine("{0} is converted to {1}.", ch, resultbyte);
                        }
                        catch (OverflowException)
                        {
                            Console.WriteLine("Unable to convert u+{0} to a byte.",
                                              Convert.ToInt16(ch).ToString("X4"));
                        }
                    }

                //string h = Encoding.ASCII.GetString(Encoding.ASCII.GetBytes(header));//Asciicoding.GetChars(header);
                //string h =System.Text.Encoding.Unicode.GetString (System.Text.Encoding.Unicode.GetBytes(header));
                //result.AddRange(Encoding.ASCII.GetBytes(h));
                
                result.AddRange(headerbyte);
                result.AddRange(Encoding.UTF8.GetBytes(strMessage));
                byte[] msg = result.ToArray();
                //this.strName = Encoding.ASCII.GetString(msg, 0, msg.Length);
                this.strName = System.Text.Encoding.UTF8.GetString(msg, 0, msg.Length);
            }

            return result.ToArray();
        }
        //string m_headmsg;
        System.UInt64 m_seqno;
        public string orderno;
        public string strName="";      //Name by which the client logs into the room
        public string strMessage="";   //Message text
        public string cmdCommand;  //Command type (login, logout, send message, etcetera)
        public string vmcNum=""; //machine number
        public string qrtype = "";
        public int prodAmnt = 0;
        public int prodID = 0;
        public string paytype = "";
        public Char[] header = new Char[12];
    } 
}

//Socket soc = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
//System.Net.IPAddress ipAdd = System.Net.IPAddress.Parse(server);
//System.Net.IPEndPoint remoteEP = new IPEndPoint(ipAdd, 3456);
//soc.Connect(remoteEP);
//For connecting to it. To send something:

////Start sending stuf..
//byte[] byData = System.Text.Encoding.ASCII.GetBytes("un:" + username + ";pw:" + password);
//soc.Send(byData);
//And for reading back..

//byte[] buffer = new byte[1024];
//int iRx = soc.Receive(buffer);
//char[] chars = new char[iRx];

//System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
//int charLen = d.GetChars(buffer, 0, iRx, chars, 0);
//System.String recv = new System.String(chars);