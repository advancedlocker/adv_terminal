﻿
//  Windows project root ::: C:\Users\Sandyc\Dropbox\ALK-Advanced_Lockers\Software\adv_terminal\Lockers\
//  Linux Mint project root ::: C:\Users\Sandyc\Dropbox\ALK-Advanced_Lockers\Software\adv_terminal\Lockers\
//  OrangePI project root ::: C:\Users\Sandyc\Dropbox\ALK-Advanced_Lockers\Software\adv_terminal\Lockers\


// Country Configuration Extension
// Windows ::: Lockers\Configuration\Country.config
// Linux Mint ::: \Configuration\Country.config
// OrangePI ::: \Configuration\Country.config

namespace Lockers
{
    using System;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Windows.Forms;

//    Globals globs = new Globals();

    public class Globals
    {
        public static Process WatchdogProcess;
        public static string SoftwareVersion = "1.0.7";

        public static bool MaintenanceLockout;
        public static bool Unservicable;

        private static bool abortfrm;
        private static LoginTypes login;
        private static PaymentTypes paytype;
        private static ProcessType process;
        private static bool accessgranted;
        private static bool onpaysetup;
//        private static Double dueAmnt = 0;
        private static decimal dueAmnt = 0;
        //        private static LockerDatabase lockdatabase;


        public static string PIN;

        public static String CurrentLanguage = "en";
        public static String CurrentLocalLanguage = "en";

        //        public static int LockerCommsRetries = 2;
        //        public static int LockerCommsRetryPeriod = 100;


        public static SmartPay smartpay;

        private static System.IO.Ports.SerialPort RTDebuggingPort;

        private static int langcode = Properties.Settings.Default.DefaultLangCode ;

        public static Button prevBtn { get; set; }

        //        public static LockerDatabase LockData;
        public static LockerDatabase LockData;

        public void Create_Database()
        {
            LockData = new LockerDatabase();
        }

/*        public LockerDatabase Get_Database()
        {
            return this.LockData;
        }
*/
        public static LockerInterface Client;
        public static LockerInterface LockerClient
        {
            get { return Client; }
            set { Client = value; }
        }

        public static int LanguageCode
        {
            get { return langcode; }
            set { langcode = value; }
        }

        public static Boolean AccessGranted
        {
            get { return accessgranted; }
            set { accessgranted = value; }
        }

        public static ProcessType UserProcessType
        {
            get { return process; }
            set { process = value; }
        }

        //        public static Double DueAmnt
        public static decimal DueAmnt
        {
            get { return dueAmnt; }
            set { dueAmnt = value; }
        }
        public static Boolean OnpaySetup
        {
            get { return onpaysetup; }
            set { onpaysetup = value; }
        }

        public static bool Maintenance
        { get; set; }

        public static bool UpdateLanguage
        { get; set; }

       
        public static PaymentTypes PayTypes
        {
            get { return paytype; }
            set { paytype = value; }
        }
        public static LoginTypes LoginSelected
        {
            get { return login; }
            set { login = value; }
        }
        public static bool AbortForm
        {
            get { return abortfrm;  }
            set { abortfrm = value; }
        }

        public static bool InvokeRequired { get; private set; }

        //module to create error log
        public enum LogSource {
            Welcomescreen,
            Mainscreen,
            SelectSize,
            SmartPay,
            CreditPayment,
            LockerInterface,
            LockerData,
            PinReg,
            Globals
        };

        public static void LogFile(LogSource source, string function, string Message)
        {
            // Write to the file:
            string SourceStr = "";
            switch (source)
            {
                case LogSource.Welcomescreen:
                    if (Properties.Settings.Default.RTLogMainScreenEnable)
                        SourceStr = "Welcomescreen[] - ";
                    else
                        return;
                    break;

                case LogSource.Mainscreen:
                    if (Properties.Settings.Default.RTLogMainScreenEnable)
                        SourceStr = "Mainscreen[] - ";
                    else
                        return;
                    break;

                case LogSource.SelectSize:
                    if (Properties.Settings.Default.RTLogSelectSizeEnable)
                        SourceStr = "SelectSize[] - ";
                    else
                        return;
                    break;

                case LogSource.LockerData:
                    if (Properties.Settings.Default.RTLogLockerDataEnable)
                        SourceStr = "LockerData[] - ";
                    else
                        return;
                    break;

                case LogSource.LockerInterface:
                    if (Properties.Settings.Default.RTLogLockerInterfaceEnable)
                        SourceStr = "LockerInterface[] - ";
                    else
                        return;
                    break;

                case LogSource.SmartPay:
                    if (Properties.Settings.Default.RTLogSmartPayoutEnable)
                        SourceStr = "SmartPay[] - ";
                    else
                        return;
                    break;

                case LogSource.CreditPayment:
                    if (Properties.Settings.Default.RTLogCreditEnable)
                        SourceStr = "CreditPayment[] - ";
                    else
                        return;
                    break;

                case LogSource.Globals:
                    if (Properties.Settings.Default.RTLogGlobalsEnable)
                        SourceStr = "Globals[] - ";
                    else
                        return;
                    break;

                case LogSource.PinReg:
//                    if (Properties.Settings.Default.RTLogPinReg)
                        SourceStr = "PinReg[] - ";
//                    else
//                       return;
                    break;

                default:
                    SourceStr = "Unknown[] - ";
                    break;
            }

            if (Properties.Settings.Default.RTLogReplicateOnConsole)
            {
                Console.WriteLine(DateTime.Now + SourceStr + " : " + function + " : " + Message);
            }

            try
            {
                SendCommsLogReport(Message);

                StreamWriter log;
                string logfile = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "PaySystemLog.txt");
                if (!File.Exists(logfile))
                {

                    log = new StreamWriter(logfile);

                }

                else
                {

                    log = File.AppendText(logfile);

                }

                log.WriteLine(DateTime.Now + SourceStr + " : " + function + " : " + Message);

                // Close the stream:

                log.Close();
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Log exception - " + Ex.ToString());
            }
        }

        public void LogFile(string sExceptionMessage, string filename)
        {

            StreamWriter log;
            try
            {
                SendCommsLogReport(sExceptionMessage);

                string logfile = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), filename);
                if (!File.Exists(logfile))
                {

                    log = new StreamWriter(logfile);

                }

                else
                {

                    log = File.AppendText(logfile);

                }

                // Write to the file:

                log.WriteLine(DateTime.Now + " :" + sExceptionMessage);

                // Close the stream:

                log.Close();
            }
            catch { }
        }

        public static void OpenCommsLogInterface()
        {
            if (Properties.Settings.Default.RTLogUartEnabled)
            {
                try
                {
                    if (RTDebuggingPort == null)
                    {
                        RTDebuggingPort = new System.IO.Ports.SerialPort();
                        RTDebuggingPort.BaudRate = Convert.ToInt32(Properties.Settings.Default.RTLogUartBaud);
                        RTDebuggingPort.PortName = Properties.Settings.Default.RTLogUartPort;

                        RTDebuggingPort.Open();
                        RTDebuggingPort.WriteLine("RTComms Port Opened\n");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

            }
        }

        public void DestroyCommsLogInterface()
        {
            try
            {
                if(RTDebuggingPort != null)
                {
                    if(RTDebuggingPort.IsOpen)
                    {
                        RTDebuggingPort.Close();
                    }

                    RTDebuggingPort.Dispose();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void SendCommsLogReport(string message)
        {

            if (Properties.Settings.Default.RTLogUartEnabled)
            {
                try
                {
                    if (!RTDebuggingPort.IsOpen)
                    {
                        RTDebuggingPort.Open();
                        RTDebuggingPort.WriteLine("RTComms Port Reopened");
                    }

                    RTDebuggingPort.WriteLine(DateTime.Now + " :" + message + "\n");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

#if (true)
        public static void Client_ConnectionStatusChanged(LockerInterface sender, LockerInterface.ConnectionStatus status)
        {
            //Check if this event was fired on a different thread, if it is then we must invoke it on the UI thread
            if (InvokeRequired)
            {
                Globals.LogFile(Globals.LogSource.Globals, "Client_ConnectionStatusChanged()", "Invoke Required");
                Invoke(new LockerInterface.delConnectionStatusChanged(Client_ConnectionStatusChanged), sender, status);
                return;
            }

            Globals.LogFile(Globals.LogSource.Globals, "Client_ConnectionStatusChanged()", "connect status = " + status.ToString() + "\n");

            if (status.ToString() == LockerInterface.ConnectionStatus.Connected.ToString())
            {
                Globals.LogFile(Globals.LogSource.Globals, "Client_ConnectionStatusChanged()", "Getting session");
                sender.SourceKey = Properties.Settings.Default.sourceKey;
                sender.GetSession();
            }

            Globals.LogFile(Globals.LogSource.Globals, "Client_ConnectionStatusChanged()", "Finished");
        }

        private static void Invoke(LockerInterface.delConnectionStatusChanged delConnectionStatusChanged, LockerInterface sender, LockerInterface.ConnectionStatus status)
        {
            throw new NotImplementedException();
        }

        //Fired when new data is received in the TCP client
        public static void Client_DataReceived(LockerInterface sender, object data)
        {
            //Again, check if this needs to be invoked in the UI thread
/*
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new LockerInterface.delDataReceived(Client_DataReceived), sender, data);
                }
                catch
                { }
                return;
            }
*/
            //Interpret the received data object as a string
            string strData = data as string;
            Globals.LogFile(Globals.LogSource.Globals, "client_DataReceived()", " - data = " + strData + "\n");
            //            string values = strData.Split('-').Select(sValue => sValue.Trim()).First();
/*
                        int sKey;
            switch (sender.LockerCommand)
            {
                case Locker_cmd.Basic.GetSession:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.AssignLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.UnassignLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.AvailableLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.UsedLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.OpenLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.CloseLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.LockStatus:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                default:
                    break;
            //}
*/
        }
#endif

        public static void SetLanguage(string language)
        {
//            string lang = "en";
            switch(language)
            {
                case "Afrikaan":          // Africans
                    CurrentLocalLanguage = "af";
                break;
                case "Arabic":          // Arabic
                    CurrentLocalLanguage = "ar";
                break;
                case "Japanese":          // Japanese
                    CurrentLocalLanguage = "ja";
                    break;
                case "Chinese":          // Chinese
                    CurrentLocalLanguage = "zh";
                    break;
                case "German":          // German
                    CurrentLocalLanguage = "de";
                break;
                case "Australian":       // Eng Australian
                    CurrentLocalLanguage = "en-AU";
                break;
                case "English":          // Eng Generic
                    CurrentLocalLanguage = "en";
                break;
                case "Spanish":          // Spanish
                    CurrentLocalLanguage = "es";
                break;
                case "French":          // French
                    CurrentLocalLanguage = "fr";
                break;
                case "Italian":          // Italian
                    CurrentLocalLanguage = "it";
                break;
                default:            // Goto English
                    CurrentLocalLanguage = "en";
                break;
            }
        }

        public static string GetLanguage()
        {
            string lang = "English";
            switch (CurrentLocalLanguage)
            {
                case "af":          // Africans
                    lang = "Afrikaan";
                    break;
                case "ar":          // Arabic
                    lang = "Arabic";
                    break;
                case "zh":          // Chinese
                    lang = "Chinese";
                    break;
                case "de":          // German
                    lang = "German";
                    break;
                case "en-AU":       // Eng Australian
                    lang = "Australian";
                    break;
                case "en":          // Eng Generic
                    lang = "English";
                    break;
                case "es":          // Spanish
                    lang = "Spanish";
                    break;
                case "fr":          // French
                    lang = "French";
                    break;
                case "it":          // Italian
                    lang = "Italian";
                    break;
                case "ja":          // Japanese
                    lang = "Japanese";
                    break;
                default:            // Goto English
                    lang = "English";
                    break;
            }

            return (lang);
        }
        public static string GetLanguageCode()
        {
            return (CurrentLocalLanguage);
        }

        public static void Open_Locker(int lockerNumber)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "Open_Locker()", "Entry");

            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            // USE SQL to update the address
            // TODO : Fix this serial number conbversion to byte array
            String Serial = Globals.LockData.GetLockerSerial(lockerNumber);
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };

            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetRear.CopyTo(sum, packetFront.Length + packetAddress.Length);

            try
            {
                if (Properties.Settings.Default.UseNewSocket)
                {
                    String IPAdd = Globals.LockData.GetLockerControllerIP(lockerNumber.ToString());
                    String IPPort = Globals.LockData.GetLockerControllerPort(lockerNumber.ToString());

                    Globals.LogFile(Globals.LogSource.Globals, "GetLockerControllerIP", "IPAdd = " + IPAdd + " Port = " + IPPort);


                    IPAddress ipAddress = IPAddress.Parse(IPAdd);
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, Convert.ToInt16(IPPort));

                    byte[] buffCMD = new byte[65535];
                    int cmdLength = 0;
                    byte[] buffResp = new byte[65535];
                    int respLength = 0;

                    Socket sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                    sender.ReceiveTimeout = 500;

                    sender.Connect(remoteEP);

                    while (!sender.Connected)
                        System.Threading.Thread.Sleep(50);

                    sender.Send(sum);

                    cmdLength = sender.Receive(buffCMD);

                    sender.Close();
                }
                else
                {
//                    Client.Send(sum);
                    //                Thread.Sleep(100);
                }
            }
            catch (Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Open_Locker()", "SocketSend Exception - " + Ex.ToString());
            }

            Globals.LogFile(Globals.LogSource.Mainscreen, "Open_Locker()", "Exit");
        }

        public static void Close_Locker(int lockerNumber)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "Close_Locker()", "Closing Locker");

            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetAddress.CopyTo(sum, packetAddress.Length);

            try
            {
                if (Properties.Settings.Default.UseNewSocket)
                {
                    String IPAdd = LockerData.GetLockerControllerIP(lockerNumber.ToString());
                    String IPPort = LockerData.GetLockerControllerPort(lockerNumber.ToString());

                    IPAddress ipAddress = IPAddress.Parse(IPAdd);
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, Convert.ToInt16(IPPort));

                    byte[] buffCMD = new byte[65535];
                    int cmdLength = 0;
                    byte[] buffResp = new byte[65535];
                    int respLength = 0;

                    Socket sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                    sender.Connect(remoteEP);

                    while (!sender.Connected)
                        System.Threading.Thread.Sleep(50);

                    sender.Send(sum);

                    System.Threading.Thread.Sleep(50);

                    cmdLength = sender.Receive(buffCMD);

                    System.Threading.Thread.Sleep(50);

                    respLength = sender.Receive(buffResp);

                    sender.Close();
                }
                else
                {
//                    client.Send(sum);
                }
            }
            catch (Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Close_Locker()", "SocketSend Exception - " + Ex.ToString());
            }
        }

        public static Size GetWindowSize()
        {

            Size WinSize = new Size();
            if (Properties.Settings.Default.WindowOrientation == "Portrait")
            {
                WinSize.Width = Properties.Settings.Default.WindowXPortrait;
                WinSize.Height = Properties.Settings.Default.WindowYPortrait;
            }
            else
            {
                WinSize.Width = Properties.Settings.Default.WindowXLandscape;
                WinSize.Height = Properties.Settings.Default.WindowYLandscape;
            }

            return (WinSize);
        }

        public static float GetWindowRatio(int axis)
        {
            float ratioX = 0;
            float ratioY = 0;

            if (Properties.Settings.Default.WindowOrientation == "Portrait")
            {
                ratioX = Properties.Settings.Default.WindowXPortrait;
                ratioY = Properties.Settings.Default.WindowYPortrait;
            }
            else
            {
                ratioX = Properties.Settings.Default.WindowXLandscape;
                ratioY = Properties.Settings.Default.WindowYLandscape;
            }

            ratioX /= (float)(Properties.Settings.Default.WindowXPortrait);
            ratioY /= (float)(Properties.Settings.Default.WindowYPortrait);

            if(axis == '1')
                return (ratioX);
            else
                return (ratioY);
        }

        public static Size ReziseObject(Size init)
        {
            float Ratx = GetWindowRatio(1);
            float Raty = GetWindowRatio(2);
            Size ObjSize = new Size();

            ObjSize.Width = (int)((float)(init.Width * Ratx));
            ObjSize.Height = (int)((float)(init.Height * Raty));

            return (ObjSize);
        }
        public static Point ReLocateObject(Point init)
        {
            float Ratx = GetWindowRatio(1);
            float Raty = GetWindowRatio(2);
            Point ObjLocation = new Point();
            Size WinSize = GetWindowSize();

            if (init.X > (WinSize.Width / 2))
                ObjLocation.X = (int)((float)(init.X * Ratx));
            else
                ObjLocation.X = (int)((float)(init.X * (1 - Ratx)));

            if (init.Y > (WinSize.Height / 2))
                ObjLocation.Y = (int)((float)(init.Y * Raty));
            else
                ObjLocation.Y = (int)((float)(init.Y * (1 - Raty)));

            return (ObjLocation);
        }

        public static void UpdateLabelDimensions(ref Label con)
        {
            float Ratx = GetWindowRatio(1);
            float Raty = GetWindowRatio(2);
            Size WinSize = GetWindowSize();

            // Update the location of the control
            Point pt = new Point();
            pt.X = (int)((float)(con.Location.X * Ratx));
            pt.Y = (int)((float)(con.Location.Y * Raty));
/*
            if (con.Location.X > (WinSize.Width / 2))
                pt.X = (int)((float)(con.Location.X * Ratx));
            else
                pt.X = (int)((float)(con.Location.X * (1 - Ratx)));

            if (con.Location.Y > (WinSize.Height / 2))
                pt.Y = (int)((float)(con.Location.Y * Raty));
            else
                pt.Y = (int)((float)(con.Location.Y * (1 - Raty)));
*/
            con.Location = pt;

            // Update the width and height of the control
            Size NewSize = new Size();

            NewSize.Width = (int)((float)(con.Size.Width * Ratx));
            NewSize.Height = (int)((float)(con.Size.Height * Raty));

            con.Size = NewSize;

            // Update the font info
            float newSize = 0;
            newSize = con.Font.Size * Ratx;
            string FontName = con.Font.Name;
            con.Font = new Font(FontName, (int)newSize);

        }

        public static void UpdateButtonDimensions(ref Button con)
        {
            float Ratx = GetWindowRatio(1);
            float Raty = GetWindowRatio(2);
            Size WinSize = GetWindowSize();

            // Update the location of the control
            Point pt = new Point();
            pt.X = (int)((float)(con.Location.X * Ratx));
            pt.Y = (int)((float)(con.Location.Y * Raty));

            con.Location = pt;

            // Update the width and height of the control
            Size NewSize = new Size();

            NewSize.Width = (int)((float)(con.Size.Width * Ratx));
            NewSize.Height = (int)((float)(con.Size.Height * Raty));
            con.Size = NewSize;

            // Update the font info

            if (con.Text != "")
            {
                float newSize = 0;
                newSize = con.Font.Size * Ratx;
                string FontName = con.Font.Name;
                con.Font = new Font(FontName, (int)newSize);
            }
        }
        public static void UpdateTextBoxDimensions(ref TextBox con)
        {
            float Ratx = GetWindowRatio(1);
            float Raty = GetWindowRatio(2);
            Size WinSize = GetWindowSize();

            // Update the location of the control
            Point pt = new Point();
            pt.X = (int)((float)(con.Location.X * Ratx));
            pt.Y = (int)((float)(con.Location.Y * Raty));
/*
            if (con.Location.X > (WinSize.Width / 2))
                pt.X = (int)((float)(con.Location.X * Ratx));
            else
                pt.X = (int)((float)(con.Location.X * (1 - Ratx)));

            if (con.Location.Y > (WinSize.Height / 2))
                pt.Y = (int)((float)(con.Location.Y * Raty));
            else
                pt.Y = (int)((float)(con.Location.Y * (1 - Raty)));
*/
            con.Location = pt;
            // Update the width and height of the control
            Size NewSize = new Size();

            NewSize.Width = (int)((float)(con.Size.Width * Ratx));
            NewSize.Height = (int)((float)(con.Size.Height * Raty));
            con.Size = NewSize;

            // Update the font info
            if (con.Text != "")
            {
                float newSize = 0;
                newSize = con.Font.Size * Ratx;
                string FontName = con.Font.Name;
                con.Font = new Font(FontName, (int)newSize);
            }
        }

        public static void UpdateComboDimensions(ref ComboBox con)
        {
            float Ratx = GetWindowRatio(1);
            float Raty = GetWindowRatio(2);
            Size WinSize = GetWindowSize();

            // Update the location of the control
            Point pt = new Point();
            pt.X = (int)((float)(con.Location.X * Ratx));
            pt.Y = (int)((float)(con.Location.Y * Raty));
/*
            if (con.Location.X > (WinSize.Width / 2))
                pt.X = (int)((float)(con.Location.X * Ratx));
            else
                pt.X = (int)((float)(con.Location.X * (1 - Ratx)));

            if (con.Location.Y > (WinSize.Height / 2))
                pt.Y = (int)((float)(con.Location.Y * Raty));
            else
                pt.Y = (int)((float)(con.Location.Y * (1 - Raty)));
*/
            con.Location = pt;

            // Update the width and height of the control
            Size NewSize = new Size();

            NewSize.Width = (int)((float)(con.Size.Width * Ratx));
            NewSize.Height = (int)((float)(con.Size.Height * Raty));
            con.Size = NewSize;

            // Update the font info
            if (con.Text != "")
            {
                float newSize = 0;
                newSize = con.Font.Size * Ratx;
                string FontName = con.Font.Name;
                con.Font = new Font(FontName, (int)newSize);
            }
        }

        public static void UpdateCheckBoxDimensions(ref CheckBox con)
        {
            float Ratx = GetWindowRatio(1);
            float Raty = GetWindowRatio(2);
            Size WinSize = GetWindowSize();

            // Update the location of the control
            Point pt = new Point();
            pt.X = (int)((float)(con.Location.X * Ratx));
            pt.Y = (int)((float)(con.Location.Y * Raty));
            con.Location = pt;

            // Update the width and height of the control
            Size NewSize = new Size();

            NewSize.Width = (int)((float)(con.Size.Width * Ratx));
            NewSize.Height = (int)((float)(con.Size.Height * Raty));
            con.Size = NewSize;

            // Update the font info
            if (con.Text != "")
            {
                float newSize = 0;
                newSize = con.Font.Size * Ratx;
                string FontName = con.Font.Name;
                con.Font = new Font(FontName, (int)newSize);
            }
        }

        public static void UpdatePanelDimensions(ref Panel con)
        {
            float Ratx = GetWindowRatio(1);
            float Raty = GetWindowRatio(2);
            Size WinSize = GetWindowSize();

            // Update the location of the control
            Point pt = new Point();
            pt.X = (int)((float)(con.Location.X * Ratx));
            pt.Y = (int)((float)(con.Location.Y * Raty));
            con.Location = pt;

/*
            if (con.Location.X > (WinSize.Width / 2))
                pt.X = (int)((float)(con.Location.X * Ratx));
            else
                pt.X = (int)((float)(con.Location.X * (1 - Ratx)));

            if (con.Location.Y > (WinSize.Height / 2))
                pt.Y = (int)((float)(con.Location.Y * Raty));
            else
                pt.Y = (int)((float)(con.Location.Y * (1 - Raty)));
*/

                        // Update the width and height of the control
                        Size NewSize = new Size();

            NewSize.Width = (int)((float)(con.Size.Width * Ratx));
            NewSize.Height = (int)((float)(con.Size.Height * Raty));
            con.Size = NewSize;
        }

        public static Point ResizeFont(Point init)
        {
            float Ratx = GetWindowRatio(1);
            float Raty = GetWindowRatio(2);
            Point ObjLocation = new Point();
            Size WinSize = GetWindowSize();

            if (init.X > (WinSize.Width / 2))
                ObjLocation.X = (int)((float)(init.X * Ratx));
            else
                ObjLocation.X = (int)((float)(init.X * (1 - Ratx)));

            if (init.Y > (WinSize.Height / 2))
                ObjLocation.Y = (int)((float)(init.Y * Raty));
            else
                ObjLocation.Y = (int)((float)(init.Y * (1 - Raty)));

            return (ObjLocation);
        }

        private static bool TaskbarHidden = false;
        private static Process tbkILL_process = new Process();
        public static void HideTaskbar()
        {
//            tbkILL_process.StartInfo.FileName = "C:\\AdvSof~1\\Taskba~1\\Applyt~1.exe";
            tbkILL_process.StartInfo.FileName = "C:\\AdvSoftware\\Taskbar Eliminator\\Apply Taskbar Eliminator.exe";
            tbkILL_process.StartInfo.CreateNoWindow = true;
            tbkILL_process.StartInfo.UseShellExecute = true;
            tbkILL_process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            tbkILL_process.StartInfo.RedirectStandardOutput = false;
            tbkILL_process.StartInfo.RedirectStandardError = false;
            Globals.LogFile(Globals.LogSource.CreditPayment, "Mainscreen()", "Taskbar killer running");
            tbkILL_process.Start();
            TaskbarHidden = true;
        }
        public static void ShowTaskbar()
        {
            //            tbkILL_process.StartInfo.FileName = "C:\\AdvSof~1\\Taskba~1\\Restor~1.exe";
            tbkILL_process.StartInfo.FileName = "C:\\AdvSoftware\\Taskbar Eliminator\\Restore Taskbar.exe";
            tbkILL_process.StartInfo.CreateNoWindow = true;
            tbkILL_process.StartInfo.UseShellExecute = true;
            tbkILL_process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            tbkILL_process.StartInfo.RedirectStandardOutput = false;
            tbkILL_process.StartInfo.RedirectStandardError = false;
            Globals.LogFile(Globals.LogSource.CreditPayment, "Mainscreen()", "Taskbar killer stopped");
            tbkILL_process.Start();

            tbkILL_process.StartInfo.FileName = "";
            TaskbarHidden = false;
        }
        public static void ToggleTaskbar()
        {
            if (tbkILL_process.StartInfo.FileName == "")
            {
                tbkILL_process.StartInfo.FileName = "C:\\AdvSof~1\\Taskba~1\\Applyt~1.exe";
                tbkILL_process.StartInfo.CreateNoWindow = true;
                tbkILL_process.StartInfo.UseShellExecute = true;
                tbkILL_process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                tbkILL_process.StartInfo.RedirectStandardOutput = false;
                tbkILL_process.StartInfo.RedirectStandardError = false;
                Globals.LogFile(Globals.LogSource.CreditPayment, "Mainscreen()", "Taskbar killer running");
                tbkILL_process.Start();

                TaskbarHidden = true;
            }
            else
            {
                tbkILL_process.StartInfo.FileName = "C:\\AdvSof~1\\Taskba~1\\Restor~1.exe";
                tbkILL_process.StartInfo.CreateNoWindow = true;
                tbkILL_process.StartInfo.UseShellExecute = true;
                tbkILL_process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                tbkILL_process.StartInfo.RedirectStandardOutput = false;
                tbkILL_process.StartInfo.RedirectStandardError = false;
                Globals.LogFile(Globals.LogSource.CreditPayment, "Mainscreen()", "Taskbar killer stopped");
                tbkILL_process.Start();

                tbkILL_process.StartInfo.FileName = "";
                TaskbarHidden = false;
            }
        }

        public static void StartExplorer()
        {
            tbkILL_process.StartInfo.FileName = "C:\\Windows\\explorer.exe";
            tbkILL_process.StartInfo.CreateNoWindow = true;
            tbkILL_process.StartInfo.UseShellExecute = true;
            tbkILL_process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            tbkILL_process.StartInfo.RedirectStandardOutput = false;
            tbkILL_process.StartInfo.RedirectStandardError = false;
            Globals.LogFile(Globals.LogSource.CreditPayment, "Globals_StartExplorer()", "Starting Explorer.exe");
            tbkILL_process.Start();

            TaskbarHidden = true;
        }

        public static void StopExplorer()
        {
            Process[] pname = Process.GetProcessesByName("explorer");
            if (pname.Length != 0)
            {
                Globals.LogFile(Globals.LogSource.CreditPayment, "Globals_StopExplorer()", "Explorer.exe process is running ...");

                try
                {
                    foreach (Process proc in Process.GetProcessesByName(tbkILL_process.StartInfo.FileName))
                    {
                        proc.Kill();
                    }
                }
                catch (Exception ex)
                {
                    Globals.LogFile(Globals.LogSource.CreditPayment, "Globals_StopExplorer()", "Explorer.exe process kill exception ... " + ex.ToString());
                }
            }
        }

        public static bool IsLockersRunning()
        {
                Process curr = Process.GetCurrentProcess();
                Process[] procs = Process.GetProcessesByName(curr.ProcessName);
                foreach (Process p in procs)
                {
                    if ((p.Id != curr.Id) &&
                        (p.MainModule.FileName == curr.MainModule.FileName))
                        return true;
                }
                return false;
        }

        public static void RemoveRunningFile()
        {
            if (File.Exists("C:\\AdvSof~1\\Proces~1\\Running.txt"))
                File.Delete("C:\\AdvSof~1\\Proces~1\\Running.txt");
        }

        public static void EnableAutomaticRestartProcess()
        {
            ProcessStartInfo ProcessInfo;

            if (Properties.Settings.Default.AutoRestartApplicationInternally)
            {
                if (!File.Exists("C:\\AdvSof~1\\Running.txt"))
                {
                    File.Create("C:\\AdvSof~1\\Running.txt");

                    Process[] pname = Process.GetProcessesByName("cmd");
                    Globals.LogFile(Globals.LogSource.Mainscreen, "EnableAutomaticRestartProcess()", "pname count = " + pname.Length);

                    if (pname.Length == 0)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "EnableAutomaticRestartProcess()", "Starting Watchdog");

                        ProcessInfo = new ProcessStartInfo("C:\\AdvSof~1\\Proces~1\\adv_pr~1.bat");
                        ProcessInfo.CreateNoWindow = true;
                        ProcessInfo.UseShellExecute = false;
                        ProcessInfo.WorkingDirectory = "C:\\AdvSof~1\\Proces~1";
                        Globals.WatchdogProcess = Process.Start(ProcessInfo);

                        Globals.LogFile(Globals.LogSource.Mainscreen, "EnableAutomaticRestartProcess()", "Watchdog Started");
                    }
                }
                else
                    Globals.LogFile(Globals.LogSource.Mainscreen, "EnableAutomaticRestartProcess()", "Watchdog already running");
            }
        }
    }
}
