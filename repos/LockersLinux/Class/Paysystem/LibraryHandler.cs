﻿

namespace Lockers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO.Ports;
    using System.Threading;
    using System.Diagnostics;
    using ITLlib;
    using System.IO;

    //    public static class LibraryHandler  // SMC Changed
    public class LibraryHandler
    {
        SSPComms m_LibHandle = new SSPComms();
        Stopwatch m_StopWatch = new Stopwatch();
        Exception m_LastEx;
        Object m_Lock = new Object();
        string m_PortName = "";
        bool m_PortOpen = false;

        public Exception LastException
        {
            get { return m_LastEx; }
            set { throw (new Exception("Can't modify this value!")); }
        }

        // This helper allows one port to be open at once
        public bool OpenPort(ref SSP_COMMAND cmd)
        {
            try
            {
                // Lock critical section
                lock (m_Lock)
                {
                    // If this exact port is already open, return true
                    if (cmd.ComPort == m_PortName && m_PortOpen)
                        return true;

                    // Open port
                    if (m_LibHandle.OpenSSPComPort(cmd))
                    {
                        // Remember details
                        m_PortName = cmd.ComPort;
                        m_PortOpen = true;
                        return true;
                    }

//                    m_LibHandle.CloseComPort();

                    return false;
                }
            }
            catch (Exception ex)
            {
                m_LastEx = ex;
                return false;
            }
        }

        public void ClosePort()
        {
            try
            {
                lock (m_Lock)
                {
                    // Close the COM port and reset vars
                    m_LibHandle.CloseComPort();
                    m_PortName = "";
                    m_PortOpen = false;
                }
            }
            catch (Exception ex)
            {
                m_LastEx = ex;
            }
        }

        public bool SendCommand(ref SSP_COMMAND cmd, ref SSP_COMMAND_INFO inf)
        {
            try
            {
                // Lock critical section to prevent multiple commands being sent simultaneously
                lock (m_Lock)
                {
                    string port = "Unknown Device";

                    switch (cmd.ComPort)
                    {
                        case "COM77":
                            port = "hopper";
                            break;

                        case "COMM76":
                            port = "NV11";
                            break;

                        case "COM76":
                            port = "SmartPayout";
                            break;
                    }

                    StreamWriter log;
                    string logfile = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "ITL_System.txt");
                    if (!File.Exists(logfile))
                    {
                        log = new StreamWriter(logfile);
                    }
                    else
                    {
                        log = File.AppendText(logfile);
                    }
                    // TODO : Check that the SSP logging is working OK.
                    log.WriteLine(DateTime.Now + " : " + "<SSP_COMMAND - \n Key = " + cmd.Key);
                    log.WriteLine("Fixed Key = " + cmd.Key.FixedKey.ToString());        // ulong 
                    log.WriteLine("Variable Key = " + cmd.Key.FixedKey.ToString());     // ulong
                    log.WriteLine("Com Port = " + cmd.ComPort);                         // string
                    log.WriteLine("Baud Rate = " + cmd.BaudRate.ToString());            // int
                    log.WriteLine("Timeout = " + cmd.Timeout.ToString());               // int
                    log.WriteLine("SSP Address = " + cmd.SSPAddress.ToString());        // byte
                    log.WriteLine("Retry Level = " + cmd.RetryLevel.ToString());        // byte 
                    log.WriteLine("Encryption Status = " + cmd.EncryptionStatus.ToString());        // bool 

                    log.WriteLine("Command Data Length = " + cmd.CommandDataLength.ToString());     // int
                    log.WriteLine("Command Data = ");     // byte[]
                                                           //                    log.WriteLine("Command Data[] = " + cmd.CommandData.ToString());     // byte[]
                    string temp = "";
                    for (int count = 0; count < cmd.CommandDataLength; count++)
                    {
                        temp = "0x";
                        if ((cmd.CommandData[count] >> 8) < 0x0A)
                            temp += (cmd.CommandData[count] >> 8) + 0x30;     // byte[]
                        else
                            temp += (cmd.CommandData[count] >> 8) + 0x37;     // byte[]

                        if ((cmd.CommandData[count] & 0x0F) < 0x0A)
                            temp += (cmd.CommandData[count] & 0x0F) + 0x30;     // byte[]
                        else
                            temp += (cmd.CommandData[count] & 0x0F) + 0x37;     // byte[]

                        temp = " ";
                    }

                    log.WriteLine("Response Data Length = " + cmd.ResponseDataLength.ToString());     // byte[]
                    log.WriteLine("Response Data = ");     // byte[]
                    for (int count = 0; count < cmd.ResponseDataLength; count++)
                    {
                        temp = "0x";
                        if ((cmd.ResponseData[count] >> 8) < 0x0A)
                            temp += (cmd.ResponseData[count] >> 8) + 0x30;     // byte[]
                        else
                            temp += (cmd.ResponseData[count] >> 8) + 0x37;     // byte[]

                        if ((cmd.ResponseData[count] & 0x0F) < 0x0A)
                            temp += (cmd.ResponseData[count] & 0x0F) + 0x30;     // byte[]
                        else
                            temp += (cmd.ResponseData[count] & 0x0F) + 0x37;     // byte[]

                        temp = " ";
                    }

                    log.WriteLine(DateTime.Now + " : <INF = " + inf + ">");
                    log.WriteLine("Response Status = " + cmd.ResponseStatus.ToString());     // byte[]
                    log.WriteLine("Response Data = ");     // byte[]
                    log.WriteLine("EncPktCnt = " + cmd.encPktCount.ToString());     // int
                    log.WriteLine("sspSeq = " + cmd.sspSeq.ToString());     // int

                    log.WriteLine(DateTime.Now + " : " + "<SSP_COMMAND_INFO - \n Key = " + inf.Encrypted.ToString());   // Bool
                    log.WriteLine("TxPacketTime = " + inf.Transmit.packetTime);   // Bool
                    log.WriteLine("TxPacketDataLength = " + inf.Transmit.PacketLength);   // Bool
                    for (int count = 0; count < inf.Transmit.PacketLength; count++)
                    {
                        temp = "0x";
                        if ((inf.Transmit.PacketData[count] >> 8) < 0x0A)
                            temp += (inf.Transmit.PacketData[count] >> 8) + 0x30;     // byte[]
                        else
                            temp += (inf.Transmit.PacketData[count] >> 8) + 0x37;     // byte[]

                        if ((inf.Transmit.PacketData[count] & 0x0F) < 0x0A)
                            temp += (inf.Transmit.PacketData[count] & 0x0F) + 0x30;     // byte[]
                        else
                            temp += (inf.Transmit.PacketData[count] & 0x0F) + 0x37;     // byte[]

                        temp = " ";
                    }
                    log.WriteLine(" " + temp);   // Byte [] 

                    log.WriteLine("TxPreEncPacketTime = " + inf.PreEncryptedTransmit.packetTime);   // Bool
                    log.WriteLine("TxPreEncPacketDataLength = " + inf.PreEncryptedTransmit.PacketLength.ToString());   // byte
                    log.WriteLine("TxPreEncData = ");   // Byte [] 
                    for (int count = 0; count < inf.PreEncryptedTransmit.PacketLength; count++)
                    {
                        temp = "0x";
                        if ((inf.PreEncryptedTransmit.PacketData[count] >> 8) < 0x0A)
                            temp += (inf.PreEncryptedTransmit.PacketData[count] >> 8) + 0x30;     // byte[]
                        else
                            temp += (inf.PreEncryptedTransmit.PacketData[count] >> 8) + 0x37;     // byte[]

                        if ((inf.PreEncryptedTransmit.PacketData[count] & 0x0F) < 0x0A)
                            temp += (inf.PreEncryptedTransmit.PacketData[count] & 0x0F) + 0x30;     // byte[]
                        else
                            temp += (inf.PreEncryptedTransmit.PacketData[count] & 0x0F) + 0x37;     // byte[]

                        temp = " ";
                    }
                    log.WriteLine(" " + temp);   // Byte [] 

                    log.WriteLine("RxPacketTime = " + inf.Receive.packetTime);   // Bool
                    log.WriteLine("RxPacketDataLength = " + inf.Receive.PacketLength.ToString());   // byte
                    log.WriteLine("RxData = ");   // Byte [] 
                    for (int count = 0; count < inf.Receive.PacketLength; count++)
                    {
                        temp = "0x";
                        if ((inf.Receive.PacketData[count] >> 8) < 0x0A)
                            temp += (inf.Receive.PacketData[count] >> 8) + 0x30;     // byte[]
                        else
                            temp += (inf.Receive.PacketData[count] >> 8) + 0x37;     // byte[]

                        if ((inf.Receive.PacketData[count] & 0x0F) < 0x0A)
                            temp += (inf.Receive.PacketData[count] & 0x0F) + 0x30;     // byte[]
                        else
                            temp += (inf.Receive.PacketData[count] & 0x0F) + 0x37;     // byte[]

                        temp = " ";
                    }
                    log.WriteLine(" " + temp);   // Byte [] 

                    log.WriteLine("RxPacketTime = " + inf.PreEncryptedRecieve.packetTime);   // Bool
                    log.WriteLine("RxPacketDataLength = " + inf.PreEncryptedRecieve.PacketLength.ToString());   // byte
                    log.WriteLine("RxData = ");   // Byte [] 
                    for (int count = 0; count < inf.PreEncryptedRecieve.PacketLength; count++)
                    {
                        temp = "0x";
                        if ((inf.PreEncryptedRecieve.PacketData[count] >> 8) < 0x0A)
                            temp += (inf.PreEncryptedRecieve.PacketData[count] >> 8) + 0x30;     // byte[]
                        else
                            temp += (inf.PreEncryptedRecieve.PacketData[count] >> 8) + 0x37;     // byte[]

                        if ((inf.PreEncryptedRecieve.PacketData[count] & 0x0F) < 0x0A)
                            temp += (inf.PreEncryptedRecieve.PacketData[count] & 0x0F) + 0x30;     // byte[]
                        else
                            temp += (inf.PreEncryptedRecieve.PacketData[count] & 0x0F) + 0x37;     // byte[]

                        temp = " ";
                    }
                    log.WriteLine(" " + temp);   // Byte [] 

                    log.Close();                    // Close the stream:

//                    Console.WriteLine(DateTime.Now.ToLongTimeString() + "  - SSPCommandPortr = " + cmd.ComPort + " port = " + port);
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + "  - SSPCommandPortr = " + cmd.ComPort);
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + "Calling m_LibHandle.SSPSendCommand(cmd, inf)");

                    return m_LibHandle.SSPSendCommand(cmd, inf);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(DateTime.Now.ToLongTimeString() + "  - Exception = " + ex.ToString());
                m_LastEx = ex;
                return false;
            }
        }

        public bool InitiateKeys(ref SSP_KEYS keys, ref SSP_COMMAND cmd)
        {
            try
            {
                lock (m_Lock)
                {
                    return m_LibHandle.InitiateSSPHostKeys(keys, cmd);
                }
            }
            catch (Exception ex)
            {
                m_LastEx = ex;
                return false;
            }
        }

        public bool CreateFullKey(ref SSP_KEYS keys)
        {
            try
            {
                lock (m_Lock)
                {
                    return m_LibHandle.CreateSSPHostEncryptionKey(keys);
                }
            }
            catch (Exception ex)
            {
                m_LastEx = ex;
                return false;
            }
        }
    }
}
