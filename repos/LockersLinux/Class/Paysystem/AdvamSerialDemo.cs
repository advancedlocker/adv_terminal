/*
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "windows.h"

#include "ProtocolMessages.h"

static TERMINAL_REQ Req;
static TERMINAL_RES Res;

static void CmdInit(void);
static void CmdLinkTest(void);
static void CmdPmtDet(void);
static void CmdPmt(void);
static void CmdPmtConf(void);
static void CmdEject(void);
static void CmdDisable(void);

static void DumpReq(TERMINAL_REQ* pReq);
static void DumpRes(TERMINAL_RES* pRes);

static BYTE pbyStanCurrent[6+1];
*/

using System;
using System.IO.Ports;

namespace Lockers
{
    public class Advam
    {
        //int i;
        private byte[] byBufGeneric = new byte[256];

        public AdvamProtocolSerial.TERMINAL_REQ_s Req;
        public AdvamProtocolSerial.TERMINAL_RES_s Res;
        public string pbyStanCurrent;


        //	    strcpy(byBufGeneric, "AdvamSerialDemo - v0.0.1\n");
        //	    printf(byBufGeneric);

        public int AdvamPayment(Decimal value)
        {
            Req = new AdvamProtocolSerial.TERMINAL_REQ_s();
            Req.Cmd = new AdvamProtocolSerial.TERMINAL_CMD();
            Req.Stats = new AdvamProtocolSerial.TERMINAL_STATS();

            Res = new AdvamProtocolSerial.TERMINAL_RES_s();
            Res.Cmd = new AdvamProtocolSerial.TERMINAL_CMD();
            Res.Stats = new AdvamProtocolSerial.TERMINAL_STATS();

//            pbyStanCurrent = new byte[6 + 1];

            AdvamProtocolMessage PM = new AdvamProtocolMessage();
            AdvamProtocolSerial PS = new AdvamProtocolSerial();
            PM.PMInit();

	        CmdInit();
	        CmdLinkTest();
	        CmdPmtDet();
	        CmdPmt();

	        if(Res.Stats == AdvamProtocolSerial.TERMINAL_STATS.STATS_OK)
	        {
                string tmp = System.Text.Encoding.UTF8.GetString(Res.pbyStan);
                pbyStanCurrent = tmp;
		        Req.Stats = AdvamProtocolSerial.TERMINAL_STATS.STATS_OK;
		        CmdPmtConf();
	        }

	        CmdEject();
	        CmdDisable();

            //for(i = 0; i < 258; i++)
            //{
            //	CmdLinkTest();
            //}

// TODO : Close port. 

            Console.WriteLine("\n\n\nSequence Finished\n\n\n");

	        return 0;
        }

        private void CmdInit()
        {
	        Req.Cmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_INIT;

            AdvamProtocolMessage PM = new AdvamProtocolMessage();
            Req.byVersion = PM.PMGetVersion();
            PM.PMSend(ref Req);
            PM.PMRecv(ref Res);
        }

        private void CmdLinkTest()
        {
	        Req.Cmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_LINK_TEST;
            AdvamProtocolMessage PM = new AdvamProtocolMessage();

            PM.PMSend(ref Req);
            PM.PMRecv(ref Res);
        }

        private void CmdPmtDet()
        {
	        Req.Cmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_PAYMENT_CARD_DET;
	        Req.byTimeout = 60;
	        Req.pbyAmount = "1.00";
	        Req.pbySurcharge = ""; // No surcharge set in this example.

            AdvamProtocolMessage PM = new AdvamProtocolMessage();
            PM.PMSend(ref Req);
            PM.PMRecv(ref Res);
            PM.PMRecv(ref Res);
        }

        private void CmdPmt()
        {
	        Req.Cmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_PAYMENT;
	        Req.pbyAmount = "1.00";
	        Req.pbySurcharge = ""; // No surcharge set in this example.
	        Req.pbyEquipId = "";
	        Req.pbyTxRef = "";

            AdvamProtocolMessage PM = new AdvamProtocolMessage();
            PM.PMSend(ref Req);
            PM.PMRecv(ref Res);
            PM.PMRecv(ref Res);	
        }

        private void CmdPmtConf()
        {
//	        memset(&Req, 0, sizeof(Req));
//	        memset(&Res, 0, sizeof(Res));

	        Req.Cmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_PAYMENT_CONF;
            Req.pbyStan = pbyStanCurrent;
	        Req.Stats = AdvamProtocolSerial.TERMINAL_STATS.STATS_OK;

            AdvamProtocolMessage PM = new AdvamProtocolMessage();
            PM.PMSend(ref Req);

	        int count = 0;
	        count++;
	        // No response so no PMRecv
        }

        private void CmdEject()
        {
//	        memset(&Req, 0, sizeof(Req));
//	        memset(&Res, 0, sizeof(Res));

	        Req.Cmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_CARD_EJECT;
	        Req.byTimeout = 60;

            AdvamProtocolMessage PM = new AdvamProtocolMessage();
            PM.PMSend(ref Req);
            PM.PMRecv(ref Res);
        }

        private void CmdDisable()
        {
            //	        memset(&Req, 0, sizeof(Req));
            //	        memset(&Res, 0, sizeof(Res));

	        Req.Cmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_DISABLE_READER;

            AdvamProtocolMessage PM = new AdvamProtocolMessage();
            PM.PMSend(ref Req);
            PM.PMRecv(ref Res);
        }

        private void DumpReq(ref AdvamProtocolSerial.TERMINAL_REQ_s pReq)
        {
	        // todo
        }

        private void DumpRes(ref AdvamProtocolSerial.TERMINAL_RES_s pRes)
        {
	        // todo
        }
    }   //  public class Advam
} // namespace Lockers
