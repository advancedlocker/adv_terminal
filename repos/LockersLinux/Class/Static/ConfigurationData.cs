﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;

namespace Lockers
{
    internal static class ConfigurationData
    {
        #region Fields

        /// <summary>
        /// Name of the content folder.
        /// </summary>
        private const string ContentFolderName = @"Content";

        /// <summary>
        /// Name of the configuration data folder.
        /// </summary>
        private const string ConfigurationDataFolderName = @"ConfigurationData";

      
        private const string LanguageConfigFileName = @"Languages.xml";
        

        /// <summary>
        /// Name of the title images configuration file.
        /// </summary>
        private const string TitleImagesConfigFileName = @"Titles.xml";

        /// <summary>
        /// Name of the background images configuration file.
        /// </summary>
        private const string BackgroundImagesConfigFileName = @"BackgroundImages.xml";


        private const string PINImageConfigFileName = @"Animals.xml";

        #endregion

        /// <summary>
        /// Gets the path to the application's executable file.
        /// </summary>
        public static string AppPath
        {
            get
            {
                return Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            }
        }


        /// <summary>
        /// Gets the path to the folder containing the configuration files.
        /// </summary>
        public static string ConfigPath
        {
            get
            {

                return AppPath
                    + Path.DirectorySeparatorChar
                    + ContentFolderName
                    + Path.DirectorySeparatorChar
                    + ConfigurationDataFolderName;


            }
        }

        /// <summary>
        /// Extracts the local path from a file path string.
        /// </summary>
        /// <param name="path">File path string.</param>
        /// <returns>The local path.</returns>
        public static string GetLocalPath(string path)
        {
            // Convert the Uri to a local system path.
            Uri pathUri = new Uri(path);
            return pathUri.LocalPath;
        }

        /// <summary>
        /// Gets the path to the title images configuration file.
        /// </summary>
        public static string TitleImagesFilePath
        {
            get
            {
                string path = ConfigPath
                    + Path.DirectorySeparatorChar
                    + TitleImagesConfigFileName;

                return GetLocalPath(path);
            }
        }

        /// <summary>
        /// Gets the path to the background images configuration file.
        /// </summary>
        public static string BackgroundImagesFilePath
        {
            get
            {
                string path = ConfigPath
                    + Path.DirectorySeparatorChar
                    + BackgroundImagesConfigFileName;

                return GetLocalPath(path);
            }
        }

        public static string PINImagesFilePath
        {
            get
            {
                string path = ConfigPath
                    + Path.DirectorySeparatorChar
                    + PINImageConfigFileName;

                return GetLocalPath(path);
            }
        }


        public static XDocument GetPINImagesData()
        {
            XDocument document = new XDocument();

            try
            {
                if (File.Exists(PINImagesFilePath))
                {
                    document = XDocument.Load(PINImagesFilePath);
                }
                else
                {
                    throw new FileNotFoundException("PIN Animal Images configuration file could not be found!");
                }
            }
            catch (IOException ex)
            {
#if DEBUG
                Console.WriteLine("IO Exception: {0}", ex.Message);
#endif
            }

            return document;
        }

        public static List<Animal> PINAnimalImages()
        {
            XDocument PINImagesDataFile = GetPINImagesData();

            List<Animal> Animals = new List<Animal>();
            List<Animal> AnimalPositions = PINAnimalPosition();
            int i = 0;
            var data = from item in PINImagesDataFile.Descendants("Animal")
                       select new
                       {
                           imgname = item.Element("ImgName").Value,
                           imgcode = item.Element("Codenum").Value,
                           imgpath=item.Element("ImgPath").Value,
                           imgwidth = item.Element("ControlSize").Attribute("W").Value,
                           imgheight = item.Element("ControlSize").Attribute("H").Value,
                           //buttonLocationX = item.Element("Position").Attribute("X").Value,
                           //buttonLocationY = item.Element("Position").Attribute("Y").Value,
                        
                       };

            foreach (var p in data)
            {
                Animal animal = new Animal();
                animal.Name  = p.imgname;
                animal.CodeNum = int.Parse( p.imgcode);
                //animal.Position = new Point(int.Parse(p.buttonLocationX), int.Parse(p.buttonLocationY));
                animal.Position = AnimalPositions[i].Position ;
                animal.ImgWidth = int.Parse(p.imgwidth);
                animal.ImgHeight = int.Parse(p.imgheight);
                animal.ImgPIN  = Image.FromFile(p.imgpath);
                Animals.Add(animal);
                i++;
            }
            Random r = new Random();
            var ranimal= Animals.OrderBy(a => r.Next()).ToList();
            //cards.OrderBy(a => Guid.NewGuid()).ToList();
            return ranimal;
        }

        public static List<Animal> PINAnimalPosition()
        {
            XDocument PINImagesDataFile = GetPINImagesData();

            List<Animal> Animals = new List<Animal>();

            var data = from item in PINImagesDataFile.Descendants("Animal")
                       select new
                       {
                           imgname = item.Element("ImgName").Value,
                           buttonLocationX = item.Element("Position").Attribute("X").Value,
                           buttonLocationY = item.Element("Position").Attribute("Y").Value,

                       };

            foreach (var p in data)
            {
                Animal animal = new Animal();
                animal.Name = p.imgname;
                animal.Position = new Point(int.Parse(p.buttonLocationX), int.Parse(p.buttonLocationY));
                Animals.Add(animal);
            }
            Random r = new Random();
            var ranimal = Animals.OrderBy(a => r.Next()).ToList();
            //cards.OrderBy(a => Guid.NewGuid()).ToList();
            return ranimal;
        }


        /// <summary>
        /// Gets the image associated with a background.
        /// </summary>
        /// <param name="background">The name of the background.</param>
        /// <returns>The background image.</returns>
        public static Image GetBackground(string background)
        {
            XDocument document = new XDocument();
            string filename = BackgroundImagesFilePath;

            try
            {
                if (File.Exists(filename))
                {
                    document = XDocument.Load(filename);
                }
                else
                {
                    throw new FileNotFoundException("BackgroundImages configuration file could not be found at '{0}'", filename);
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine("IO Exception: " + ex.ToString());
            }

            filename = (from item in document.Descendants("BackgroundImage")
                        where item.Element("Name").Value == background
                        select item.Element("ImagePath").Value).Single();

            return Image.FromFile(filename);
        }
    }
}
