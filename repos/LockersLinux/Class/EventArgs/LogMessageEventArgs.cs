﻿

namespace Lockers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class LogMessageEventArgs: EventArgs
    {
        #region Fields

        private string message;

        #endregion

        #region construction

        public LogMessageEventArgs(string message)
        {
             this.message = message;
        }
        
        #endregion

        public String Messsage
        {
            get { return this.message; }
        }
    }
}
