﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Lockers
{
//    public static class LockerDatabase : IDisposable
    public class LockerDatabase : IDisposable
    {
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

// TODO :Fix this to work under linux.
//        string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;
        string cs = "server=127.0.0.1;uid=lockers;pwd=locker2017;database=lockerdata";

        #region properties
        private eLockerSize lsize;
        private decimal mcost;
        private decimal mprice;
        private DateTime starttime;
        private DateTime endtime;
        private string pinicon;
        private decimal totalhrs;
        private int rentstat;
        private decimal exceedhrs;
        private DateTime exceedrent;

        private decimal uprince;
        private int locknum;
        private double xposition;
        private double yposition;
        private double xlocation;
        private double ylocation;
        private bool isfree;

        private int hrs;
        private int min;

        private TimeSpan duration;
        private TimeSpan remaining;
        private TimeSpan exceedtime;
        MySqlCommand cmd;


        private DataTable lockers;
        private DataTable lockertypes;

        private decimal amnttender;
        public void Amnt_Tender_Set(decimal value)
        {
            this.amnttender = value;

            return;
        }

        public decimal Amnt_Tender_Get()
        {
            return (this.amnttender);
        }

        private decimal amntchange;
/*      public decimal Amnt_Change
        {
            get { return this.amntchange; }
            set { this.amntchange = value; }
        }
*/
        public void Amnt_Change_Set(decimal value)
        {
            this.amntchange = value;

            return;
        }

        public decimal Amnt_Change_Get()
        {
            return (this.amntchange);
        }

        public DataTable Lockers
        {
            get { return this.lockers; }
            set { this.lockers = value; }
        }

        public DataTable LockerTypes
        {
            get { return this.lockertypes; }
            set { this.lockertypes = value; }
        }

        public TimeSpan Remaining
        {
            get { return this.remaining; }
        }

        public TimeSpan Duration
        {
            get { return this.duration; }
        }

        private string pinno;
        public void PINCode_Set(string value)
        {
            this.pinno = value;
        }

        public string PINCode_Get()
        {
            return (this.pinno);
        }

        public decimal TotalHrs
        {
            get { return this.totalhrs; }
            set { this.totalhrs = value; }
        }

        public DateTime Start_Time
        {
            get { return this.starttime; }
            set { this.starttime = value; }
        }

        public DateTime End_Time
        {
            get { return this.endtime; }
            set { this.endtime = value; }
        }
        public string PINIcon
        {
            get { return this.pinicon; }
            set { this.pinicon = value; }
        }
        public int Hours
        {
            get { return this.hrs; }
            set { this.hrs = value; }
        }

        public int Minutes
        {
            get { return this.min; }
            set { this.min = value; }
        }

        /*        public int LockNumber
                {
                    get { return this.locknum; }
                    set { this.locknum = value; }
                }
        */
        public int LockNumber_Get()
        {
            return (this.locknum);
        }
        public string LockNumber_GetString()
        {
            return (this.locknum.ToString());
        }
        public void LockNumber_Set(int value)
        {
            this.locknum = value;
        }
        public eLockerSize Locker_Size
        {
            get { return this.lsize; }
            set { this.lsize = value; }
        }


        public decimal UPrice
        {
            get { return this.mprice; }
            set { this.mprice = value; }
        }

        public decimal COST
        {
            get { return this.mcost; }
            set { this.mcost = value; }
        }

#endregion

#region events
        public event EventHandler<LogMessageEventArgs> LockerDatabaseError;
        protected void OnLockerDatabaseError(LogMessageEventArgs e)
        {
            EventHandler<LogMessageEventArgs> LockerDatabaseerror = this.LockerDatabaseError;
            if (LockerDatabaseerror != null)
            {
                LockerDatabaseerror(this, e);
            }

        }
        #endregion

        #region constructor
        /* 
                public LockerDatabase()
                {

                }
        */
        #endregion


        public bool testDatabaseConnection()
        {
            //            return (null);
            bool result = false;
            Globals.LogFile(Globals.LogSource.LockerData, "testDatabaseConnection()", "Connection String = " + cs);

            MySqlConnection pconn = new MySqlConnection(cs);
            this.lockers = new DataTable();

            try
            {
                pconn.Open();

                string sql = @"SELECT lockers.lockersize FROM lockers WHERE lockers.isavailable = 1";

                Globals.LogFile(Globals.LogSource.LockerData, "testDatabaseConnection()", " - sql - " + sql);

                using (MySqlCommand cmd = new MySqlCommand(sql, pconn))
                {
                    this.lockers.Load(cmd.ExecuteReader());
                }

                result = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("testDatabaseConnection() -Exception - " + e.ToString() + "\n");
                Globals.LogFile(Globals.LogSource.LockerData, "testDatabaseConnection()", " -Exception - " + e.ToString() + "\n");
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }
            finally
            {
                pconn.Close();
            }

            return result;
        }


        public DataTable GetLockerSizes()
        {
            Globals.LogFile(Globals.LogSource.LockerData, "GetLockerSizes()", "Connection String = " + cs);

            MySqlConnection pconn = new MySqlConnection(cs);
            DataTable lockersizes = new DataTable();

            try
            {
                pconn.Open();

                string sql = @"SELECT sizecode FROM lockersizes WHERE enabled = 1";

                Globals.LogFile(Globals.LogSource.LockerData, "GetLockerSizes()", " - sql - " + sql);

                using (MySqlCommand cmd = new MySqlCommand(sql, pconn))
                {
                    lockersizes.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("GetLockerSizes() -Exception - " + e.ToString() + "\n");
                Globals.LogFile(Globals.LogSource.LockerData, "GetLockerSizes()", " -Exception - " + e.ToString() + "\n");
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }
            finally
            {
                pconn.Close();
            }

            return lockersizes;
        }

        public DataTable Get_Lockers()
        {
            //            return (null);
            Globals.LogFile(Globals.LogSource.LockerData, "GetLockers()", "Connection String = " + cs);

            MySqlConnection pconn = new MySqlConnection(cs);
            this.lockers = new DataTable();

            try
            {
                pconn.Open();

                string sql = @"SELECT lockers.lockersize, lockers.lockerNum, lockers.xposition, lockers.yposition, lockers.xlocation, lockers.ylocation, lockers.isFree " +
                            " FROM lockers LEFT OUTER JOIN lockersizes ON lockers.lockersize = lockersizes.sizecode WHERE lockers.isavailable = 1 AND lockers.isenable = 1";
//                string sql = @"SELECT * FROM lockers LEFT OUTER JOIN lockersizes ON lockers.lockersize = lockersizes.sizecode WHERE lockers.isavailable = 1 AND lockers.isenable = 1";

                Globals.LogFile(Globals.LogSource.LockerData, "GetLockers()", " - sql - " + sql);

                using (MySqlCommand cmd = new MySqlCommand(sql, pconn))
                {
                    this.lockers.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception e)
            {
                Globals.LogFile(Globals.LogSource.LockerData, "GetLockers()", " -Exception - " + e.ToString() + "\n");
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }
            finally
            {
                pconn.Close();
            }
            return this.lockers;
        }

        /*
                public DataTable GetUnrentedLockers()
                {
                    Globals.LogFile(Globals.LogSource.LockerData, "GetUnrentedLockers()", "Entry");

                    MySqlConnection pconn = new MySqlConnection(cs);
                    this.lockers = new DataTable();

                    try
                    {
                        pconn.Open();

                        string sql = @"SELECT lockers.lockersize, lockers.lockerNum,  lockersizes.uprice, lockers.xposition, lockers.yposition, lockers.xlocation, lockers.ylocation, lockers.isFree " +
                                    " FROM lockers LEFT OUTER JOIN lockersizes ON lockers.lockersize = lockersizes.sizecode WHERE lockers.isavailable = 1";

                        Globals.LogFile(Globals.LogSource.LockerData, "GetUnrentedLockers()", " - sql - " + sql);

                        using (MySqlCommand cmd = new MySqlCommand(sql, pconn))
                        {
                            this.lockers.Load(cmd.ExecuteReader());
                        }
                    }
                    catch (Exception e)
                    {
                        Globals.LogFile(Globals.LogSource.LockerData, "GetUnrentedLockers()", " -Exception - " + e.ToString() + "\n");
                        this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
                    }
                    finally
                    {
                        pconn.Close();
                    }

                    return this.lockers;
                }

                public DataTable GetEnabledLockers()
                {
                    Globals.LogFile(Globals.LogSource.LockerData, "GetEnabledLockers()", "Entry");

                    MySqlConnection pconn = new MySqlConnection(cs);
                    this.lockers = new DataTable();

                    try
                    {
                        pconn.Open();

                        string sql = @"SELECT lockers.lockersize, lockers.lockerNum,  lockersizes.uprice, lockers.xposition, lockers.yposition, lockers.xlocation, lockers.ylocation, lockers.isFree " +
                                    " FROM lockers LEFT OUTER JOIN lockersizes ON lockers.lockersize = lockersizes.sizecode WHERE lockers.enabled = 1";

                        Globals.LogFile(Globals.LogSource.LockerData, "GetEnabledLockers()", " - sql - " + sql);

                        using (MySqlCommand cmd = new MySqlCommand(sql, pconn))
                        {
                            this.lockers.Load(cmd.ExecuteReader());
                        }
                    }
                    catch (Exception e)
                    {
                        Globals.LogFile(Globals.LogSource.LockerData, "GetEnabledLockers()", " -Exception - " + e.ToString() + "\n");
                        this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
                    }
                    finally
                    {
                        pconn.Close();
                    }

                    return this.lockers;
                }
        */


/*
        public DataTable GetLockerTypes()
        {
            return (null);
        }
*/

        public DataTable GetLockerTypes()
        {
            MySqlConnection pconn = new MySqlConnection(cs);
            this.lockers = new DataTable();

            try
            {
                pconn.Open();

                string sql = @"SELECT lockersizes.idsize, lockersizes.sizecode, lockersizes.enabled FROM lockersizes Where lockersizes.enabled   = '1'";
//                string sql = @"SELECT * FROM lockersizes Where lockersizes.enabled   = '1'";

                Globals.LogFile(Globals.LogSource.LockerData, "Get_Locker_Types()", "sql = " + sql);

                using (MySqlCommand cmd = new MySqlCommand(sql, pconn))
                {
                    this.lockers.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception e)
            {
                Globals.LogFile(Globals.LogSource.LockerData, "Get_Locker_Types()", "Exception = " + e.Message.ToString());
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }
            finally
            {
                pconn.Close();
            }
            return this.lockers;
        }


        public decimal GetLockerPrice(eLockerSize lsize)
        {
            decimal retvalue = 0;
            try
            {
                MySqlConnection pconn = new MySqlConnection(cs);
                pconn.Open();

                //                string sql = @"SELECT lockersizes.uprice FROM lockersizes WHERE lockersizes.sizecode = " + (int)lsize;
                string sql = @"SELECT * FROM lockersizes WHERE lockersizes.sizecode = " + (int)lsize;

                Globals.LogFile(Globals.LogSource.LockerData, "GetLockerPrice()", "sql = " + sql);

                cmd = new MySqlCommand();
                cmd.Connection = pconn;
                cmd.CommandText = sql;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                this.mprice = dt.Rows[0].Field<decimal>("uprice");
/*
                switch (period)
                {
                    case eLockerPayPeriod.HOURLY:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceHourly");
                        break;

                    case eLockerPayPeriod.BLOCK1:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock1");
                        break;

                    case eLockerPayPeriod.BLOCK2:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock2");
                        break;

                    case eLockerPayPeriod.BLOCK3:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock3");
                        break;

                    case eLockerPayPeriod.BLOCK4:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock4");
                        break;

                    case eLockerPayPeriod.BLOCK5:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock5");
                        break;

                    case eLockerPayPeriod.BLOCK6:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock6");
                        break;

                    case eLockerPayPeriod.BLOCK7:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock7");
                        break;

                    case eLockerPayPeriod.BLOCK8:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock8");
                        break;

                    case eLockerPayPeriod.BLOCK9:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock9");
                        break;

                    case eLockerPayPeriod.BLOCK10:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock10");
                        break;

                    case eLockerPayPeriod.DAILY:
                        this.mprice = dt.Rows[0].Field<decimal>("PriceBlock10");
                        break;

                    default:
                        this.mprice = dt.Rows[0].Field<decimal>("uprice");
                        break;
                }
*/
                retvalue = this.mprice;

                pconn.Close();

            }
            catch (Exception e)
            {
                Globals.LogFile(Globals.LogSource.LockerData, "GetLockers()", " -Exception - " + e.ToString() + "\n");
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }

            return retvalue;
        }

        public bool Lock_Available()
        {
            return (true);
        }

        public bool IsLockerAvailable(string LockerNo)
        {
            return (true);
        }

        public bool IsLockerEnabled(string LockerNo)
        {
            return (true);
        }

        public bool SwapLocker(string CurrentLockerNo, string NewLockerNo)
        {
            return (true);
        }

        public bool DisableLocker(string LockerNo)
        {
            return (false);
        }

        public bool EnableLocker(string LockerNo)
        {
            return (true);
        }

        public string GetIconsForPinCode(string mPINNO)
        {
            return ("1");
        }

        public string GetLockersForPinCode(string mPINNO)
        {
            return("1");
        }

        public bool Used_Access(string mPINNO, string mPINICON)
        {
            return (false);

        }

        public bool Check_Access(string mPINNO, string mPINICON)
        {
            return (false);

        }

        public bool Check_Access(string mPINNO, int LockNumber)
        {
            return (false);
        }

        public bool IsPINUnique(string mPINNO)
        {
            bool retvalue = false;

            return retvalue;
        }

        public bool IsPINActive(string mPINNO)
        {
            return (false);
        }

        public string UpdatePINonLocker(String LockerNumber, string mPINNO)
        {
            return ("123456");
        }

        /// <summary>
        /// update lockers table lock number to 1= available and rentlocker table rentstatus to 1=done rent
        /// </summary>
        /// <returns></returns>
        public bool Locker_Done()
        {
            return (false);

            bool retval = false;
            MySqlConnection myConnection = new MySqlConnection(cs);
            myConnection.Open();

            MySqlCommand myCommand = myConnection.CreateCommand();
            MySqlTransaction myTrans;

            // Start a local transaction
            myTrans = myConnection.BeginTransaction();
            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            myCommand.Connection = myConnection;
            myCommand.Transaction = myTrans;

            //    DateTime today = DateTime.Now;
            //TimeSpan rentduration = new TimeSpan(this.hrs, this.min, 0);
            //decimal totaltime = rentduration.Hours + (rentduration.Minutes > 0 ? rentduration.Minutes / 60 : 0);

            //DateTime endrent = today.Add(rentduration);
            /*
                    try
                    {
                        myCommand.CommandText = @"UPDATE rentlockers SET rentstatus = 1 WHERE PINcode = '" + this.pinno + "' AND PINicon = '" + this.pinicon + "' AND lockerNum = " + this.locknum + " AND rentstatus = 0";
                        myCommand.ExecuteNonQuery();
                        myCommand.CommandText = @"UPDATE lockers SET isavailable = 1 WHERE lockerNum = " + this.locknum;
                        myCommand.ExecuteNonQuery();
                        myTrans.Commit();
                        retval = true;
                    }
            */
            try
            {
                myCommand.CommandText = @"UPDATE rentlockers SET rentstatus = 1 WHERE lockerNum = " + this.locknum + " AND rentstatus = 0";
                Globals.LogFile(Globals.LogSource.LockerData, "Locker_sizes", " sql(0) = " + myCommand.CommandText);
                myCommand.ExecuteNonQuery();

                myCommand.CommandText = @"UPDATE lockers SET isavailable = 1 WHERE lockerNum = " + this.locknum;
                Globals.LogFile(Globals.LogSource.LockerData, "Locker_sizes", " sql(1) = " + myCommand.CommandText);
                myCommand.ExecuteNonQuery();

                myTrans.Commit();
                retval = true;
            }
            catch (Exception e)
            {
                try
                {
                    myTrans.Rollback();
                }
                catch (SqlException ex)
                {
                    if (myTrans.Connection != null)
                    {
                        //Console.WriteLine("An exception of type " + ex.GetType() +
                        //" was encountered while attempting to roll back the transaction.");
                        this.OnLockerDatabaseError(new LogMessageEventArgs("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction."));
                    }
                }

                //Console.WriteLine("An exception of type " + e.GetType() +
                //" was encountered while inserting the data.");
                //Console.WriteLine("Neither record was written to database.");

                this.OnLockerDatabaseError(new LogMessageEventArgs("An exception of type " + e.GetType() +
                " was encountered while inserting the data. \n" + "Neither record was written to database."));
            }
            finally
            {
                myConnection.Close();
            }
            return retval;
        }

        public bool Update_Rent()
        {
            return (false);
        }

        public bool Save_Rent(string cost)
        {
            return (false);
        }

        public string GetLockerSerial(int LockerNo)
        {
            return ("12345678");
        }

        private int GetLockerAvailabilityEnumValue(eLockerAvailability availabilityState)
        {
            return (0);
        }
        private int GetLockerSizeEnumValue(eLockerSize Lsize)
        {
            return (0);
        }

        public string[] GetLockerAvailability(eLockerAvailability availabilityState)
        {
            Globals.LogFile(Globals.LogSource.LockerData, "GetLockerAvailability", "Entry");

            string[] retvalue = new string[2];

            retvalue[0] = "0";
            retvalue[1] = "1";

            return (retvalue);
        }

        public string[] GetLockerAvailability(eLockerAvailability availabilityState, eLockerSize Lsize)
        {
            Globals.LogFile(Globals.LogSource.LockerData, "GetLockerAvailability_2", "Entry");
            string[] retvalue = new string[2];

            retvalue[0] = "0";
            retvalue[1] = "1";

            return (retvalue);
        }

        /***************************************************
         * 
         * Simplified functions from complex calls above.
         * 
        ****************************************************/

        public string[] GetRentedLockers()
        {
            string[] retvalue = new string[2];

            retvalue[0] = "0";
            retvalue[1] = "1";

            return (retvalue);
        }

        public string[] GetRentedLockers(eLockerSize size)
        {
            string[] retvalue = new string[2];

            retvalue[0] = "0";
            retvalue[1] = "1";

            return (retvalue);
        }

        public string[] GetAvailableLockers()
        {
            string[] retvalue = new string[2];

            retvalue[0] = "0";
            retvalue[1] = "1";

            return (retvalue);
        }

        public string[] GetAvailableLockers(eLockerSize size)
        {
            string[] retvalue = new string[2];

            retvalue[0] = "0";
            retvalue[1] = "1";

            return (retvalue);
        }

        public string[] GetDisabledLockers()
        {
            string[] retvalue = new string[2];

            retvalue[0] = "0";
            retvalue[1] = "1";

            return (retvalue);
        }

        public string[] GetDisabledLockers(eLockerSize size)
        {
            string[] retvalue = new string[2];

            retvalue[0] = "0";
            retvalue[1] = "1";

            return (retvalue);
        }

        public bool ClearAllHires()
        {
            return (false);
        }

        public int GetLockerLocation(string LockerNo)
        {
            return (0);
        }

        public bool IsLockerActive(string LockerNo)
        {
            return (true);
        }

    }
}
