﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Lockers
{
    public class LockerInterface : IDisposable
    {

        #region Consts/Default values
        const int DEFAULTTIMEOUT = 5000; //Default to 5 seconds on all timeouts
        const int RECONNECTINTERVAL = 2000; //Default to 2 seconds reconnect attempt rate
        #endregion

        #region Components, Events, Delegates, and CTOR
        //Timer used to detect receive timeouts
        private System.Timers.Timer tmrReceiveTimeout = new System.Timers.Timer();
        private System.Timers.Timer tmrSendTimeout = new System.Timers.Timer();
        private System.Timers.Timer tmrConnectTimeout = new System.Timers.Timer();
        public delegate void delDataReceived(LockerInterface sender, object data);
        public event delDataReceived DataReceived;
        public delegate void delConnectionStatusChanged(LockerInterface sender, ConnectionStatus status);
        public event delConnectionStatusChanged ConnectionStatusChanged;

        private string CRC16;
        private string sourcekey;
        private int session;
        private int locknum;
        private string lockcmd;

        /*              
                public string CRC16
                {
                    get { return this.crc16; }
                    set { this.crc16 = value; }
                }
        */
        public string SourceKey
        {
            get { return sourcekey; }
            set { sourcekey = value; }
        }
        public int SessionKey
        {
            get { return this.session; }
            set { this.session = value; }
        }

        public string LockerCommand
        {
            get { return this.lockcmd; }
            set { this.lockcmd = value; }
        }

        public enum ConnectionStatus
        {
            NeverConnected,
            Connecting,
            Connected,
            AutoReconnecting,
            DisconnectedByUser,
            DisconnectedByHost,
            ConnectFail_Timeout,
            ReceiveFail_Timeout,
            SendFail_Timeout,
            SendFail_NotConnected,
            Error
        }

        public LockerInterface(IPAddress ip, int port, bool autoreconnect = true)
        {
            this._IP = ip;
            this._Port = port;
            this._AutoReconnect = autoreconnect;
            this._client = new TcpClient(AddressFamily.InterNetwork);
            this._client.NoDelay = true; //Disable the nagel algorithm for simplicity
            ReceiveTimeout = DEFAULTTIMEOUT;
            SendTimeout = DEFAULTTIMEOUT;
            ConnectTimeout = DEFAULTTIMEOUT;
            ReconnectInterval = RECONNECTINTERVAL;
            tmrReceiveTimeout.AutoReset = false;
            tmrReceiveTimeout.Elapsed += new System.Timers.ElapsedEventHandler(tmrReceiveTimeout_Elapsed);
            tmrConnectTimeout.AutoReset = false;
            tmrConnectTimeout.Elapsed += new System.Timers.ElapsedEventHandler(tmrConnectTimeout_Elapsed);
            tmrSendTimeout.AutoReset = false;
            tmrSendTimeout.Elapsed += new System.Timers.ElapsedEventHandler(tmrSendTimeout_Elapsed);
            
            sourcekey = Properties.Settings.Default.sourceKey;
            ConnectionState = ConnectionStatus.NeverConnected;
        }
        #endregion
        #region Private methods/Event Handlers
        void tmrSendTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.ConnectionState = ConnectionStatus.SendFail_Timeout;
            DisconnectByHost();
        }
        void tmrReceiveTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.ConnectionState = ConnectionStatus.ReceiveFail_Timeout;
            DisconnectByHost();
        }
        void tmrConnectTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ConnectionState = ConnectionStatus.ConnectFail_Timeout;
            DisconnectByHost();
        }
        private void DisconnectByHost()
        {
            this.ConnectionState = ConnectionStatus.DisconnectedByHost;
            tmrReceiveTimeout.Stop();
            if (AutoReconnect)
                Reconnect();
        }
        private void Reconnect()
        {
            if (this.ConnectionState == ConnectionStatus.Connected)
                return;
            this.ConnectionState = ConnectionStatus.AutoReconnecting;
            try
            {
                this._client.Client.BeginDisconnect(true, new AsyncCallback(cbDisconnectByHostComplete), this._client.Client);
            }
            catch { }
        }
        #endregion
        #region Public Methods
        /// <summary>
        /// Try connecting to the remote host
        /// </summary>
        public void Connect()
        {
            if (ConnectionState == ConnectionStatus.Connected)
                return;
            ConnectionState = ConnectionStatus.Connecting;
            tmrConnectTimeout.Start();
            this._client.BeginConnect(this._IP, this._Port, new AsyncCallback(cbConnect), this._client.Client);
        }
        /// <summary>
        /// Try disconnecting from the remote host
        /// </summary>
        public void Disconnect()
        {
            if (ConnectionState != ConnectionStatus.Connected)
                return;
            this._client.Client.BeginDisconnect(true, new AsyncCallback(cbDisconnectComplete), this._client.Client);
        }
        /// <summary>
        /// Try sending a string to the remote host
        /// </summary>
        /// <param name="data">The data to send</param>
        public void Send(string data)
        {
            Globals.LogFile(Globals.LogSource.LockerInterface, "Send(string data)", "Call");

            if (this.ConnectionState != ConnectionStatus.Connected)
            {
                ConnectionState = ConnectionStatus.SendFail_NotConnected;
                Globals.LogFile(Globals.LogSource.LockerInterface, "Send()", " - ConnectionState = " + ConnectionState);
                return;
            }

            var bytes = _encode.GetBytes(data);
            Globals.LogFile(Globals.LogSource.LockerInterface, "Send()", " - ConnectionState = " + data);

            SocketError err = new SocketError();
            tmrSendTimeout.Start();
            _client.Client.BeginSend(bytes, 0, bytes.Length, SocketFlags.None, out err, new AsyncCallback(cbSendComplete), _client.Client);

            if (err != SocketError.Success)
            {
                Globals.LogFile(Globals.LogSource.LockerInterface, "Send()", " - doDCHost - err " + err.ToString());
                Action doDCHost = new Action(DisconnectByHost);
                doDCHost.Invoke();
            }
        }
        /// <summary>
        /// Try sending byte data to the remote host
        /// </summary>
        /// <param name="data">The data to send</param>
        public void Send(byte[] data)
        {
            if (Properties.Settings.Default.TestSocketReconnect) 
            {
                if ((this.ConnectionState != ConnectionStatus.Connected) || (this._client.Client.Connected == false))
                    Connect();
            }

            if (this.ConnectionState != ConnectionStatus.Connected)
                throw new InvalidOperationException("LockerInterface().Send()  Cannot send data, socket is not connected");
            SocketError err = new SocketError();

//            this._client.Client.Send(data);

            this._client.Client.BeginSend(data, 0, data.Length, SocketFlags.None, out err, new AsyncCallback(cbSendComplete), this._client.Client);
            if (err != SocketError.Success)
            {
                Action doDCHost = new Action(DisconnectByHost);
                doDCHost.Invoke();
            }
        }
        public void Dispose()
        {
            this._client.Close();
            try
            {
                this._client.Client.Dispose();
            }
            catch { }
        }
        #endregion
        #region Callbacks
        private void cbConnectComplete()
        {
            if (_client.Connected == true)
            {
                tmrConnectTimeout.Stop();
                ConnectionState = ConnectionStatus.Connected;
                this._client.Client.BeginReceive(this.dataBuffer, 0, this.dataBuffer.Length, SocketFlags.None, new AsyncCallback(cbDataReceived), this._client.Client);
            }
            else
            {
                ConnectionState = ConnectionStatus.Error;
            }
        }
        private void cbDisconnectByHostComplete(IAsyncResult result)
        {
            var r = result.AsyncState as Socket;
            if (r == null)
            {
                Globals.LogFile(Globals.LogSource.LockerData, "cbDisconnectByHostComplete", "Invalid IAsyncResult - Could not interpret as a socket object");
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
            }
            r.EndDisconnect(result);
            if (this.AutoReconnect)
            {
                Action doConnect = new Action(Connect);
                doConnect.Invoke();
                return;
            }
        }
        private void cbDisconnectComplete(IAsyncResult result)
        {
            var r = result.AsyncState as Socket;
            if (r == null)
            {
                Globals.LogFile(Globals.LogSource.LockerData, "cbDisconnectByHostComplete", "Invalid IAsyncResult - Could not interpret as a socket object");
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
            }
            r.EndDisconnect(result);
            this.ConnectionState = ConnectionStatus.DisconnectedByUser;

        }
        private void cbConnect(IAsyncResult result)
        {
            var sock = result.AsyncState as Socket;
            if (result == null)
            {
                Globals.LogFile(Globals.LogSource.LockerData, "cbDisconnectByHostComplete", "Invalid IAsyncResult - Could not interpret as a socket object");
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
            }
            if (!sock.Connected)
            {
                if (AutoReconnect)
                {
                    System.Threading.Thread.Sleep(ReconnectInterval);
                    Action reconnect = new Action(Connect);
                    reconnect.Invoke();
                    return;
                }
                else
                    return;
            }
            sock.EndConnect(result);
            var callBack = new Action(cbConnectComplete);
            callBack.Invoke();
        }
        private void cbSendComplete(IAsyncResult result)
        {
            Globals.LogFile(Globals.LogSource.LockerInterface, "cbSendComplete()", " - Entry");

            var r = result.AsyncState as Socket;
            if (r == null)
            {
                Globals.LogFile(Globals.LogSource.LockerData, "cbSendComplete", "Invalid IAsyncResult - Could not interpret as a socket object");
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
            }

            SocketError err = new SocketError();
            r.EndSend(result, out err);
            if (err != SocketError.Success)
            {
                Action doDCHost = new Action(DisconnectByHost);
                doDCHost.Invoke();
            }
            else
            {
                lock (SyncLock)
                {
                    tmrSendTimeout.Stop();
                }
            }
        }
        private void cbChangeConnectionStateComplete(IAsyncResult result)
        {
            var r = result.AsyncState as LockerInterface;
            if (r == null)
            {
                Globals.LogFile(Globals.LogSource.LockerInterface, "cbChangeConnectionStateComplete()", " - Socket == null");
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a EDTC object");
            }
            r.ConnectionStatusChanged.EndInvoke(result);
        }

        public void cbDataReceived(IAsyncResult result)
        {
            var sock = result.AsyncState as Socket;
            if (sock == null)
                throw new InvalidOperationException("Invalid IASyncResult - Could not interpret as a socket");
            try
            {
                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived(0)", " - Socket = " + sock.LocalEndPoint.ToString());

                SocketError err = new SocketError();

                int bytes = sock.EndReceive(result, out err);
                if (bytes == 0 || err != SocketError.Success)
                {
                    Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived(1)", " - bytes = 0 OR not succcess");
                    lock (SyncLock)
                    {
                        tmrReceiveTimeout.Start();
                        return;
                    }
                }
                else
                {
                    lock (SyncLock)
                    {
                        tmrReceiveTimeout.Stop();
                    }
                }

                if (DataReceived != null)
                {
//                    string strData = System.Text.Encoding.UTF8.GetString(dataBuffer, 0, dataBuffer.Length);
                    var strData = (Encoding.Default.GetString(dataBuffer, 0, dataBuffer.Length - 1)).Split(new string[] { "\r\n", "\r", "\n", "\0", null }, StringSplitOptions.RemoveEmptyEntries);
                    dataBuffer = new byte[5000];
                    Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived(2)", " - data = " + strData[0]);

                    switch (this.LockerCommand)
                    {
                        case Locker_cmd.Basic.GetSession:
                            {
                                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived()", " - GetSession");
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.AssignLock:
                            {
                                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived()", " - AssignLock");
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.UnassignLock:
                            {
                                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived()", " - UnassignLock");
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.AvailableLock:
                            {
                                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived()", " - AvailableLock");
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.UsedLock:
                            {
                                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived()", " - UsedLock");
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.OpenLock:
                            {
                                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived()", " - OpenLock");
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.CloseLock:
                            {
                                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived()", " - CloseLock");
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.LockStatus:
                            {
                                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived()", " - LockStatus");
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        default:
                            break;
                    }
                    DataReceived.BeginInvoke(this, _encode.GetString(dataBuffer, 0, bytes), new AsyncCallback(cbDataRecievedCallbackComplete), this);
                }
                else
                    Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataReceived(3)", " - data =  null");
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());               
            }
        }
        private void cbDataRecievedCallbackComplete(IAsyncResult result)
        {
            var r = result.AsyncState as LockerInterface;
            if (r == null)
            {
                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataRecievedCallbackComplete(3)", " - Could not interpret as EDTC object");
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as EDTC object");
            }

            r.DataReceived.EndInvoke(result);
            SocketError err = new SocketError();
            this._client.Client.BeginReceive(this.dataBuffer, 0, this.dataBuffer.Length, SocketFlags.None, out err, new AsyncCallback(cbDataReceived), this._client.Client);

            if (err != SocketError.Success)
            {
                Globals.LogFile(Globals.LogSource.LockerInterface, "cbDataRecievedCallbackComplete(3)", " - err = " + err.ToString());
                Action doDCHost = new Action(DisconnectByHost);
                doDCHost.Invoke();
            }
        }
        #endregion
        #region Properties and members
        private IPAddress _IP = IPAddress.None;
        private static ConnectionStatus _ConStat;
        private TcpClient _client;
        private byte[] dataBuffer = new byte[5000];
        private bool _AutoReconnect = false;
        private int _Port = 0;
        private static Encoding _encode = Encoding.Default;
        object _SyncLock = new object();
        /// <summary>
        /// Syncronizing object for asyncronous operations
        /// </summary>
        public object SyncLock
        {
            get
            {
                return _SyncLock;
            }
        }
        /// <summary>
        /// Encoding to use for sending and receiving
        /// </summary>
        public Encoding DataEncoding
        {
            get
            {
                return _encode;
            }
            set
            {
                _encode = value;
            }
        }
        /// <summary>
        /// Current state that the connection is in
        /// </summary>
        public ConnectionStatus ConnectionState
        {
            get
            {
                return _ConStat;
            }
            private set
            {
                bool raiseEvent = value != _ConStat;
                _ConStat = value;
                if (ConnectionStatusChanged != null && raiseEvent)
                    ConnectionStatusChanged.BeginInvoke(this, _ConStat, new AsyncCallback(cbChangeConnectionStateComplete), this);
            }
        }
        /// <summary>
        /// True to autoreconnect at the given reconnection interval after a remote host closes the connection
        /// </summary>
        public bool AutoReconnect
        {
            get
            {
                return _AutoReconnect;
            }
            set
            {
                _AutoReconnect = value;
            }
        }
        public int ReconnectInterval { get; set; }
        /// <summary>
        /// IP of the remote host
        /// </summary>
        public IPAddress IP
        {
            get
            {
                return _IP;
            }
        }
        /// <summary>
        /// Port to connect to on the remote host
        /// </summary>
        public int Port
        {
            get
            {
                return _Port;
            }
        }
        /// <summary>
        /// Time to wait after a receive operation is attempted before a timeout event occurs
        /// </summary>
        public int ReceiveTimeout
        {
            get
            {
                return (int)tmrReceiveTimeout.Interval;
            }
            set
            {
                tmrReceiveTimeout.Interval = (double)value;
            }
        }
        /// <summary>
        /// Time to wait after a send operation is attempted before a timeout event occurs
        /// </summary>
        public int SendTimeout
        {
            get
            {
                return (int)tmrSendTimeout.Interval;
            }
            set
            {
                tmrSendTimeout.Interval = (double)value;
            }
        }
        /// <summary>
        /// Time to wait after a connection is attempted before a timeout event occurs
        /// </summary>
        public int ConnectTimeout
        {
            get
            {
                return (int)tmrConnectTimeout.Interval;
            }
            set
            {
                tmrConnectTimeout.Interval = (double)value;
            }
        }
        #endregion

        #region controller
        /// <summary>
        /// get sessionkey
        /// The current session key (32Bytes) for the approved access device
        /// </summary>
        /// <param name="sourcekey"></param>
        public void GetSession()
        {
            Globals.LogFile(Globals.LogSource.LockerInterface, "GetSession()", "Call");

            //            lockcmd = Locker_cmd.Basic.GetSession;
            lockcmd = Locker_cmd.Basic.GetSession;
            Send(Locker_cmd.Basic.GetSession + "," + sourcekey); 
        }

        
        /// <summary>
        /// This assigns a passcode to the locker
        /// Note VerificationType is a dont care field currently.
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="lockNum"></param>
        /// <param name="Type"></param>
        /// <param name="verifyData"></param>
        /// <param name="CRC16"></param>
        public void AssignLock(int lockNum, int Type, int verifyData)
        {
            lockcmd = Locker_cmd.Basic.AssignLock;
            GetSession();
            string command = Locker_cmd.Basic.AssignLock + "," + SessionKey + "," + lockNum + "," + Type + "," + verifyData + ",";
            string CRC16 = CalculateCRC16(command);
            Send(command + CRC16);
        }
        
        
        /// <summary>
        /// This removes a passcode from the locker
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="lockNum"></param>
        /// <param name="CRC16"></param>
        public void Unassignlock(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.UnassignLock;
            GetSession();
            string command = Locker_cmd.Basic.UnassignLock + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }
        
        
        /// <summary>
        /// This gets all available (free) locks from the locker controller
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="CRC16"></param>
//        public void GetAvailableLocks(int sessionkey, string CRC16)
        public void GetAvailableLocks()
        {
            lockcmd = Locker_cmd.Basic.AvailableLock;
//            GetSession();

//            while (this.session == 0) ;

            string command = Locker_cmd.Basic.AvailableLock + "," + this.SessionKey + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }
        
        
        /// <summary>
        /// This gets all unavailable (used) locks from the locker controller
        /// E.g.GetUsedLocks,123456789,12
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="CRC16"></param>
//        public void GetUsedLocks(int sessionkey)
        public void GetUsedLocks()
        {
            lockcmd = Locker_cmd.Basic.UsedLock;
            GetSession();
            string command = Locker_cmd.Basic.UsedLock + "," + this.SessionKey + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }
        
        
        /// <summary>
        /// This opens the lock in the selected locker number which would then open the door (Locker 5 in the example)
        /// E.g. 'OpenLock,123456789,5,12'
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="LockNumber"></param>
        /// <param name="CRC16"></param>
//        public void OpenLock(int sessionkey, int lockNum)
        public void OpenLock(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.OpenLock;
            GetSession();
            string command = Locker_cmd.Basic.OpenLock + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }

        public void ABOpenLocker(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.LockStatus;
            GetSession();
            string command = Locker_cmd.Basic.LEDStatus + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }

        public void AOpen_Locker(int lockerNumber)
        {
            Globals.LogFile(Globals.LogSource.LockerInterface, "Open_Locker()", "Entry");

            // Get the server info for this lock.

            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            // USE SQL to update the address
            // TODO : Fix this serial number conbversion to byte array
            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };

            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetRear.CopyTo(sum, packetFront.Length + packetAddress.Length);

            try
            {
                Globals.LockerClient.Send(sum);
//                Thread.Sleep(100);
            }
            catch (Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.LockerInterface, "Open_Locker()", "SocketSend Exception - " + Ex.ToString());
            }

            Globals.LogFile(Globals.LogSource.LockerInterface, "Open_Locker()", "Exit");
        }


        /// <summary>
        /// This closes/latches the lock in the selected locker number which would then lock the door (Locker 5 in the example)
        /// E.g.  'CloseLock,123456789,5,12'
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="lockNum"></param>
        /// <param name="CRC16"></param>
        public void CloseLock(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.CloseLock;
            GetSession();
            string command = Locker_cmd.Basic.CloseLock + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }

        public void Close_Locker(int lockerNumber)
        {
            Globals.LogFile(Globals.LogSource.LockerInterface, "Close_Locker()", "Closing Locker");

            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetAddress.CopyTo(sum, packetAddress.Length);

            try
            {
                Globals.LockerClient.Send(sum);
            }
            catch (Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.LockerInterface, "Close_Locker()", "SocketSend Exception - " + Ex.ToString());
            }
        }


        /// <summary>
        /// This returns the open or closed state of the lock in the selected locker number (Locker 5 in the example)
        /// E.g.GetLockStatus,123456789,5,12
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="lockNum"></param>
        /// <param name="CRC16"></param>
        //        public void GetLockStatus(int sessionkey, int lockNum)
        public void GetLockStatus(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.LockStatus;
            GetSession();
            string command = Locker_cmd.Basic.LockStatus + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
            
        }
        
        
        /// <summary>
        /// This returns the ON or OFF state of the indicator LED on the selected locker number
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="lockNum"></param>
        /// <param name="CRC16"></param>
//        public void GetLEDStatus(int sessionkey, int lockNum)
        public void GetLEDStatus(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.LockStatus;
            GetSession();
            string command = Locker_cmd.Basic.LEDStatus + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }

        private string CalculateCRC16(string data)
        {
            return "12";
        }


        #endregion
    }
}
