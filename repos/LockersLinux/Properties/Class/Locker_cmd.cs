﻿namespace Lockers
{
    public class Locker_cmd
    {
        public struct Basic
        {
            public const string GetSession = "GetSessionKey";
            public const string AssignLock = "AssignLock";
            public const string UnassignLock = "UnassignLock";
            public const string AvailableLock = "GetAvailableLocks";
            public const string UsedLock = "GetUsedLocks";
            public const string OpenLock = "OpenLock";
            public const string CloseLock = "CloseLock";
            public const string LockStatus = "GetLockStatus";
            public const string DoorStatus = "GetDoorStatus";
            public const string LEDStatus = "GetLEDStatus";
        }
    }
}
