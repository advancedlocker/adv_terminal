﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Lockers
{
    public class LockerDatabase:IDisposable
    {

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~LockerDatabase() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

        string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;

        #region properties
        private LockerSize lsize;
        private decimal mcost;
        private decimal mprice;
        private DateTime starttime;
        private DateTime endtime;
        private string pinicon;
        private string pinno;
        private decimal totalhrs;
        private int rentstat;
        private decimal exceedhrs;
        private DateTime exceedrent;

        private decimal uprince;
        private int locknum;
        private double xposition;
        private double yposition;
        private double xlocation;
        private double ylocation;
        private bool isfree;

        private int hrs;
        private int min;

        private TimeSpan duration;
        private TimeSpan remaining;
        private TimeSpan exceedtime;
        MySqlCommand cmd;


        private DataTable lockers;
        private DataTable lockertypes;

        private decimal amnttender;
        private decimal amntchange;

        public decimal Amnt_Tender
        {
            get { return this.amnttender; }
            set { this.amnttender = value; }
        }

        public decimal Amnt_Change
        {
            get { return this.amntchange; }
            set { this.amntchange = value; }
        }

        public DataTable Lockers
        {
            get { return this.lockers; }
            set { this.lockers = value; }
        }

        public DataTable LockerTypes
        {
            get { return this.lockertypes; }
            set { this.lockertypes = value; }
        }

        public TimeSpan Remaining
        {
            get { return this.remaining;}
        }

        public TimeSpan Duration
        {
            get { return this.duration; }
        }
        
        public string PINCode
        {
            get { return this.pinno; }
            set { this.pinno = value; }
        }

        public decimal TotalHrs
        {
            get { return this.totalhrs; }
            set { this.totalhrs = value; }
        }

        public DateTime Start_Time
        {
            get { return this.starttime; }
            set { this.starttime = value; }
        }

        public DateTime End_Time
        {
            get { return this.endtime; }
            set { this.endtime = value; }
        }
        public string PINIcon
        {
            get { return this.pinicon; }
            set { this.pinicon = value; }
        }
        public int Hours
        {
            get { return this.hrs; }
            set { this.hrs = value; }
        }

        public int Minutes
        {
            get { return this.min; }
            set { this.min = value; }
        }

        public int LockNumber
        {
            get {return this.locknum; }
            set {this.locknum=value; }
        }
        public LockerSize Locker_Size
        {
            get { return this.lsize; }
            set { this.lsize = value; }
        }


        public decimal UPrice
        {
             get { return this.mprice; }
            set { this.mprice = value; }
        }

        public decimal  COST
        {
            get { return this.mcost; }
            set { this.mcost = value; }
        }

        #endregion

        #region events
        public event EventHandler<LogMessageEventArgs> LockerDatabaseError;
        protected void OnLockerDatabaseError(LogMessageEventArgs e)
        {
            EventHandler<LogMessageEventArgs> LockerDatabaseerror = this.LockerDatabaseError;
            if (LockerDatabaseerror != null)
            {
                LockerDatabaseerror(this, e);
            }

        }
        #endregion

        #region constructor
        public LockerDatabase()
        {

        }
        #endregion


        public DataTable Get_Lockers()
        {
            MySqlConnection pconn = new MySqlConnection(cs);
            this.lockers  = new DataTable();
            try
            {

                pconn.Open();

                string sql = @"SELECT lockers.lockersize, lockers.lockerNum,  lockersizes.uprice, lockers.xposition, lockers.yposition, lockers.xlocation, lockers.ylocation, lockers.isFree " +
                            " FROM lockers LEFT OUTER JOIN lockersizes ON lockers.lockersize = lockersizes.sizecode WHERE lockers.isavailable = 1 AND lockers.isenable = 1";
                using (MySqlCommand cmd = new MySqlCommand(sql, pconn))
                {
                    this.lockers.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }
            finally
            {
                pconn.Close();
            }
            return this.lockers;
        }

        public DataTable Get_Locker_Types()
        {
            MySqlConnection pconn = new MySqlConnection(cs);
            this.lockers = new DataTable();
            try
            {

                pconn.Open();

                string sql = @"SELECT lockersizes.idsize, lockersizes.sizecode, lockersizes.uprice, lockersizes.enabled FROM lockersizes Where lockersizes.enabled   = '1'";
                using (MySqlCommand cmd = new MySqlCommand(sql, pconn))
                {
                    this.lockers.Load(cmd.ExecuteReader());
                }
            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }
            finally
            {
                pconn.Close();
            }
            return this.lockers;
        }

        public decimal Locker_UPrice()
        {
            decimal retvalue = 0;
            try
            {
                //string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;
                MySqlConnection pconn = new MySqlConnection(cs);
                pconn.Open();

                string sql = @"SELECT lockersizes.uprice FROM lockersizes WHERE lockersizes.sizecode = " + (int)this.lsize;
                cmd = new MySqlCommand();
                cmd.Connection = pconn;
                cmd.CommandText = sql;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                this.mprice= dt.Rows[0].Field<decimal>("uprice");
                retvalue = this.mprice;
               
                pconn.Close();

            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }

            return  retvalue;
        }


        public bool Lock_Available()
        {
            bool retvalue = false;
            try
            {
                //string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;
                MySqlConnection pconn = new MySqlConnection(cs);
                pconn.Open();

                string sql = @"SELECT lockers.lockerNum FROM lockers WHERE lockers.isavailable = 1 AND lockers.isenable = 1 LIMIT 1";
                cmd = new MySqlCommand();
                cmd.Connection = pconn;
                cmd.CommandText = sql;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                if (dt.Rows.Count != 0)
                {
                    this.locknum = dt.Rows[0].Field<int>("lockerNum");
                    retvalue = true;
                }
                pconn.Close();

            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }

            return retvalue;

        }

        public string GetIconsForPinCode(string mPINNO)
        {
            string retvalue = "";
            try
            {
                //string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;
                MySqlConnection pconn = new MySqlConnection(cs);
                pconn.Open();

                string sql = @"SELECT rentlockers.PINicon FROM rentlockers " +
                                " WHERE rentlockers.PINcode = '" + mPINNO + "' AND rentlockers.rentstatus = 0";

                cmd = new MySqlCommand();
                cmd.Connection = pconn;
                cmd.CommandText = sql;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                if (dt.Rows.Count != 0)
                {
                    foreach(DataRow dr in dt.Rows)
                    {
                        retvalue += dr["PINicon"].ToString();
                        retvalue += ",";
                    }
                }
                pconn.Close();

            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }

            return retvalue;
        }

        public string GetLockersForPinCode(string mPINNO)
        {
            string retvalue = "";
            try
            {
                //string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;
                MySqlConnection pconn = new MySqlConnection(cs);
                pconn.Open();

                string sql = @"SELECT rentlockers.lockerNum FROM rentlockers " +
                                " WHERE rentlockers.PINcode = '" + mPINNO + "' AND rentlockers.rentstatus = 0";

                cmd = new MySqlCommand();
                cmd.Connection = pconn;
                cmd.CommandText = sql;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                if (dt.Rows.Count != 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        retvalue += dr["lockerNum"].ToString();
                        retvalue += ",";
                    }
                }
                pconn.Close();

            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }

            return retvalue;
        }

        public bool Used_Access(string mPINNO, string mPINICON)
        {
            bool retvalue = false;
            try
            {
                //string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;
                MySqlConnection pconn = new MySqlConnection(cs);
                pconn.Open();

                string sql = @"SELECT rentlockers.lockerNum FROM rentlockers " +
                                " WHERE rentlockers.PINcode = '" + mPINNO + "' AND rentlockers.PINicon = '" + mPINICON + "' AND rentlockers.rentstatus = 0";

                cmd = new MySqlCommand();
                cmd.Connection = pconn;
                cmd.CommandText = sql;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                if (dt.Rows.Count != 0)
                {
                    retvalue = true;
                }
                pconn.Close();

            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }

            return retvalue;
        }

        public bool Check_Access(string mPINNO, string mPINICON)
        {
            bool retvalue = false;
            try
            {
                //string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;
                MySqlConnection pconn = new MySqlConnection(cs);
                pconn.Open();

//                string sql = @"SELECT rentlockers.lockerNum, rentlockers.startrent, rentlockers.endrent, rentlockers.uprice, rentlockers.exceedrent  FROM rentlockers " +
//                                " WHERE rentlockers.PINcode = '"+ mPINNO  + "' AND rentlockers.PINicon = '"+ mPINICON  +"' AND rentlockers.rentstatus = 0";
                string sql = @"SELECT rentlockers.lockerNum, rentlockers.startrent, rentlockers.endrent, rentlockers.uprice, rentlockers.exceedrent  FROM rentlockers " +
                                " WHERE rentlockers.PINcode = '" + mPINNO + "' AND rentlockers.PINicon = '" + mPINICON + "' AND rentlockers.rentstatus = 0";
                cmd = new MySqlCommand();
                cmd.Connection = pconn;
                cmd.CommandText = sql;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                if (dt.Rows.Count != 0)
                {
                    this.locknum = dt.Rows[0].Field<int>("lockerNum");
                    this.starttime= dt.Rows[0].Field<DateTime>("startrent");
                    this.endtime = dt.Rows[0].Field<DateTime>("endrent");
                    this.exceedrent = dt.Rows[0].Field<DateTime>("exceedrent");
                    this.duration = this.endtime.Subtract(this.starttime);
                    this.remaining = this.exceedrent.Subtract(DateTime.Now);
                    this.exceedtime = DateTime.Now.Subtract(this.endtime);
                    this.mprice= dt.Rows[0].Field<decimal>("uprice");
                    retvalue = true;
                }
                pconn.Close();

            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }

            return retvalue;
        }


        public bool Check_Access(string mPINNO, int LockNumber)
        {
            bool retvalue = false;
            try
            {
                //string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;
                MySqlConnection pconn = new MySqlConnection(cs);
                pconn.Open();

                //                string sql = @"SELECT rentlockers.lockerNum, rentlockers.startrent, rentlockers.endrent, rentlockers.uprice, rentlockers.exceedrent  FROM rentlockers " +
                //                                " WHERE rentlockers.PINcode = '"+ mPINNO  + "' AND rentlockers.PINicon = '"+ mPINICON  +"' AND rentlockers.rentstatus = 0";
                string sql = @"SELECT rentlockers.lockerNum, rentlockers.startrent, rentlockers.endrent, rentlockers.uprice, rentlockers.exceedrent  FROM rentlockers " +
                                " WHERE rentlockers.PINcode = '" + mPINNO + "' AND rentlockers.lockerNum = '" + LockNumber + "' AND rentlockers.rentstatus = 0";
                cmd = new MySqlCommand();
                cmd.Connection = pconn;
                cmd.CommandText = sql;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                if (dt.Rows.Count != 0)
                {
                    this.locknum = dt.Rows[0].Field<int>("lockerNum");
                    this.starttime = dt.Rows[0].Field<DateTime>("startrent");
                    this.endtime = dt.Rows[0].Field<DateTime>("endrent");
                    this.exceedrent = dt.Rows[0].Field<DateTime>("exceedrent");
                    this.duration = this.endtime.Subtract(this.starttime);
                    this.remaining = this.exceedrent.Subtract(DateTime.Now);
                    this.exceedtime = DateTime.Now.Subtract(this.endtime);
                    this.mprice = dt.Rows[0].Field<decimal>("uprice");
                    retvalue = true;
                }
                pconn.Close();

            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }

            return retvalue;
        }


        public List<int> Locker_sizes()
        {
            List<int> retList = new List<int>();

            try
            {
                //string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;
                MySqlConnection pconn = new MySqlConnection(cs);
                pconn.Open();

                string sql = @"SELECT DISTINCTROW lockers.lockersize FROM lockers WHERE isavailable=1 AND isenable=1";
                cmd = new MySqlCommand();
                cmd.Connection = pconn;
                cmd.CommandText = sql;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                if (dt.Rows.Count != 0)
                {
                   retList = (from row in dt.AsEnumerable() select Convert.ToInt32(row["lockersize"])).ToList();
                    
                }
                pconn.Close();

            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }

            return retList;
        }

        /// <summary>
        /// update lockers table lock number to 1= available and rentlocker table rentstatus to 1=done rent
        /// </summary>
        /// <returns></returns>
        public bool Locker_Done()
        {
            bool retval = false;
        MySqlConnection myConnection = new MySqlConnection(cs);
        myConnection.Open();

            MySqlCommand myCommand = myConnection.CreateCommand();
        MySqlTransaction myTrans;

        // Start a local transaction
        myTrans = myConnection.BeginTransaction();
            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            myCommand.Connection = myConnection;
            myCommand.Transaction = myTrans;

            //    DateTime today = DateTime.Now;
            //TimeSpan rentduration = new TimeSpan(this.hrs, this.min, 0);
            //decimal totaltime = rentduration.Hours + (rentduration.Minutes > 0 ? rentduration.Minutes / 60 : 0);

            //DateTime endrent = today.Add(rentduration);
/*
        try
        {
            myCommand.CommandText = @"UPDATE rentlockers SET rentstatus = 1 WHERE PINcode = '" + this.pinno + "' AND PINicon = '" + this.pinicon + "' AND lockerNum = " + this.locknum + " AND rentstatus = 0";
            myCommand.ExecuteNonQuery();
            myCommand.CommandText = @"UPDATE lockers SET isavailable = 1 WHERE lockerNum = " + this.locknum;
            myCommand.ExecuteNonQuery();
            myTrans.Commit();
            retval = true;
        }
*/
            try
            {
                myCommand.CommandText = @"UPDATE rentlockers SET rentstatus = 1 WHERE lockerNum = " + this.locknum + " AND rentstatus = 0";
                myCommand.ExecuteNonQuery();
                myCommand.CommandText = @"UPDATE lockers SET isavailable = 1 WHERE lockerNum = " + this.locknum;
                myCommand.ExecuteNonQuery();
                myTrans.Commit();
                retval = true;
            }
            catch (Exception e)
            {
                try
                {
                    myTrans.Rollback();
                }
                catch (SqlException ex)
                {
                    if (myTrans.Connection != null)
                    {
                        //Console.WriteLine("An exception of type " + ex.GetType() +
                        //" was encountered while attempting to roll back the transaction.");
                        this.OnLockerDatabaseError(new LogMessageEventArgs("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction."));
                    }
                }

                //Console.WriteLine("An exception of type " + e.GetType() +
                //" was encountered while inserting the data.");
                //Console.WriteLine("Neither record was written to database.");

                this.OnLockerDatabaseError(new LogMessageEventArgs("An exception of type " + e.GetType() +
                " was encountered while inserting the data. \n" + "Neither record was written to database."));
            }
            finally
            {
                myConnection.Close();
            }
            return retval;
        }

        public bool Update_Rent()
        {
            bool retval = false;
            MySqlConnection myConnection = new MySqlConnection(cs);
            myConnection.Open();

            MySqlCommand myCommand = myConnection.CreateCommand();
            MySqlTransaction myTrans;

            // Start a local transaction
            myTrans = myConnection.BeginTransaction();
            myCommand.Connection = myConnection;
            myCommand.Transaction = myTrans;
               

            TimeSpan rentduration = this.exceedrent.Subtract(DateTime.Now).Duration();
            decimal totaltime = rentduration.Hours + (rentduration.Minutes > 0 ? Math.Round((decimal)rentduration.Minutes / 60, 2) : 0);

            DateTime endrent = this.exceedrent.Add(rentduration);

            try
            {
                myCommand.CommandText = @"UPDATE rentlockers SET exceedrent = '" + endrent.ToString("yyyy-MM-dd H:mm:ss") + "', exceedhrs = "+ totaltime + 
                              " WHERE PINcode = '"+ this.pinno  +"' AND PINicon = '"+ this.pinicon  +"' AND lockerNum = "+ this.locknum  + " AND rentstatus = 0";
                myCommand.ExecuteNonQuery();
               
                myTrans.Commit();
                retval = true;
            }
            catch (Exception e)
            {
                try
                {
                    myTrans.Rollback();
                }
                catch (SqlException ex)
                {
                    if (myTrans.Connection != null)
                    {
                        this.OnLockerDatabaseError(new LogMessageEventArgs("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction."));
                    }
                }
                this.OnLockerDatabaseError(new LogMessageEventArgs("An exception of type " + e.GetType() +
                " was encountered while inserting the data. \n" + "Neither record was written to database."));
            }
            finally
            {
                myConnection.Close();
            }
            return retval;

        }

        public bool Save_Rent()
        {
            bool retval = false;
            MySqlConnection myConnection = new MySqlConnection(cs);
            myConnection.Open();

            MySqlCommand myCommand = myConnection.CreateCommand();
            MySqlTransaction myTrans;

            // Start a local transaction
            myTrans = myConnection.BeginTransaction();
            // Must assign both transaction object and connection
            // to Command object for a pending local transaction
            myCommand.Connection = myConnection;
            myCommand.Transaction = myTrans;

            DateTime today = DateTime.Now;
            TimeSpan rentduration = new TimeSpan(this.hrs, this.min, 0);
            decimal totaltime= rentduration.Hours + (rentduration.Minutes > 0 ? Math.Round( (decimal)rentduration.Minutes / 60,2) : 0);

            DateTime endrent = today.Add(rentduration);

            try
            {
                myCommand.CommandText = @"INSERT INTO rentlockers (trandate, PINcode, PINicon, totalHrs, startrent, endrent, lockerNum, uprice, rentstatus, sizecode, exceedrent) " +
                                        " VALUES ('"+ today.ToString("yyyy-MM-dd H:mm:ss") +"', '"+ this.pinno  +"', '"+ this.pinicon  +"', "+ totaltime  + ", '" + today.ToString("yyyy-MM-dd H:mm:ss") + "', '" +
                                        endrent.ToString("yyyy-MM-dd H:mm:ss") + "',"+ this.locknum  +", "+ this.mprice  +", 0,"+ (int)this.lsize  +",'"+ endrent.ToString("yyyy-MM-dd H:mm:ss") + "')";
                myCommand.ExecuteNonQuery();
                if (myCommand.LastInsertedId != null)
                {
                    myCommand.CommandText = @"INSERT INTO payment (idrents, amntDue, amntrender, amntchange, startrent, endrent) " +
                                  " SELECT " + myCommand.LastInsertedId + "," + Globals.LockData.COST + "," + Globals.LockData.Amnt_Tender + "," + Globals.LockData.Amnt_Change + ",'"
                                    + today.ToString("yyyy-MM-dd H:mm:ss") + "','"+ endrent.ToString("yyyy-MM-dd H:mm:ss") + "'";
                    myCommand.ExecuteNonQuery();
                }

                myCommand.CommandText = @"UPDATE lockers SET isavailable = 0 WHERE lockerNum = " + this.locknum;
                myCommand.ExecuteNonQuery();
                myTrans.Commit();
                retval = true;
            }
            catch (Exception e)
            {
                try
                {
                    myTrans.Rollback();
                }
                catch (SqlException ex)
                {
                    if (myTrans.Connection != null)
                    {
                        //Console.WriteLine("An exception of type " + ex.GetType() +
                        //" was encountered while attempting to roll back the transaction.");
                        this.OnLockerDatabaseError(new LogMessageEventArgs("An exception of type " + ex.GetType() +
                        " was encountered while attempting to roll back the transaction."));
                    }
                }

                //Console.WriteLine("An exception of type " + e.GetType() +
                //" was encountered while inserting the data.");
                //Console.WriteLine("Neither record was written to database.");

                this.OnLockerDatabaseError(new LogMessageEventArgs("An exception of type " + e.GetType() +
                " was encountered while inserting the data. \n" + "Neither record was written to database."));
            }
            finally
            {
                myConnection.Close();
            }
            return retval;

        }

        public string GetLockerSerial(int LockerNo)
        {
            string retvalue = "";
            try
            {
                //string cs = System.Configuration.ConfigurationManager.ConnectionStrings["lockerconnection"].ConnectionString;
                MySqlConnection pconn = new MySqlConnection(cs);
                pconn.Open();

                string sql = @"SELECT serial FROM lockerdata.lockers WHERE lockerNum = '" + LockerNo + "'";

                cmd = new MySqlCommand();
                cmd.Connection = pconn;
                cmd.CommandText = sql;
                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());
                if (dt.Rows.Count != 0)
                {
                    retvalue = dt.Rows[0].Field<string>("serial");
                }
                pconn.Close();

            }
            catch (Exception e)
            {
                this.OnLockerDatabaseError(new LogMessageEventArgs(e.Message));
            }

            return (retvalue);
        }
    }
}
