﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lockers
{
    public enum PaySource
    {
        /// <summary>
        /// from bill acceptor
        /// </summary>
        Bills,

        /// <summary>
        /// from coin slot
        /// </summary>
        Coins,
    }
}
