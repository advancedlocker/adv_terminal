﻿using System.Drawing;

namespace Lockers
{
    public class Animal
    {
        private string name;
        private int codenum;
        private string imgpath;
        private int imgwidth;
        private int imgheight;
        private Point position;
        private Image imgPIN;
        public string Name
        {
            get { return this.name; }
            set { this.name  = value; }
        }

        public int CodeNum
        {
            get { return this.codenum; }
            set { this.codenum = value; }
        }
        
        public string ImgPath
        {
            get { return this.imgpath; }
            set { this.imgpath = value; }
        }

        public int ImgWidth
        {
            get { return this.imgwidth; }
            set { this.imgwidth = value; }
        }
        public int ImgHeight
        {
            get { return this.imgheight; }
            set { this.imgheight = value; }
        }
        public Point Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        public Image ImgPIN
        {
            get { return this.imgPIN; }
            set { this.imgPIN = value; }
        }


    }
}
