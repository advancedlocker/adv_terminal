﻿

namespace Lockers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class CmdEventArgs:EventArgs
    {
        #region Field
        private string values;
        #endregion

        public CmdEventArgs(string values)
        {
            this.values = values;
        }

        public string Values
        {
            get { return this.values; }
        }
    }
}
