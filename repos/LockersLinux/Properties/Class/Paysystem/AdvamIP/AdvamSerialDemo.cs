
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "windows.h"

#include "ProtocolMessages.h"

static TERMINAL_REQ Req;
static TERMINAL_RES Res;

static void CmdInit(void);
static void CmdLinkTest(void);
static void CmdPmtDet(void);
static void CmdPmt(void);
static void CmdPmtConf(void);
static void CmdEject(void);
static void CmdDisable(void);

static void DumpReq(TERMINAL_REQ* pReq);
static void DumpRes(TERMINAL_RES* pRes);

static BYTE pbyStanCurrent[6+1];

int main(void)
{
	//int i;
	BYTE byBufGeneric[256] = {0};

	strcpy(byBufGeneric, "AdvamSerialDemo - v0.0.1\n");
	printf(byBufGeneric);
	
	PMInit();

	CmdInit();
	CmdLinkTest();
	CmdPmtDet();
	CmdPmt();

	if(Res.Stats == STATS_OK)
	{
		strcpy(pbyStanCurrent, Res.pbyStan);
		Req.Stats = STATS_OK;
		CmdPmtConf();
	}

	CmdEject();
	CmdDisable();

	//for(i = 0; i < 258; i++)
	//{
	//	CmdLinkTest();
	//}

	PMDispose();

	printf("\n\n\nSequence Finished\n\n\n");

	while (1);

	return 0;
}

static void CmdInit(void)
{
	memset(&Req, 0, sizeof(Req));
	memset(&Res, 0, sizeof(Res));

	Req.Cmd = CMD_INIT;
	Req.byVersion = PMGetVersion();
	PMSend(&Req);

	PMRecv(&Res);
}

static void CmdLinkTest(void)
{
	memset(&Req, 0, sizeof(Req));
	memset(&Res, 0, sizeof(Res));

	Req.Cmd = CMD_LINK_TEST;
	PMSend(&Req);

	PMRecv(&Res);
}

static void CmdPmtDet(void)
{
	memset(&Req, 0, sizeof(Req));
	memset(&Res, 0, sizeof(Res));

	Req.Cmd = CMD_PAYMENT_CARD_DET;
	Req.byTimeout = 60;
	strcpy(Req.pbyAmount, "1.00");
	strcpy(Req.pbySurcharge, ""); // No surcharge set in this example.
	PMSend(&Req);

	PMRecv(&Res);
	PMRecv(&Res);
}

static void CmdPmt(void)
{
	memset(&Req, 0, sizeof(Req));
	memset(&Res, 0, sizeof(Res));

	Req.Cmd = CMD_PAYMENT;
	strcpy(Req.pbyAmount, "1.00");
	strcpy(Req.pbySurcharge, ""); // No surcharge set in this example.
	strcpy(Req.pbyEquipId, "");
	strcpy(Req.pbyTxRef, "");
	PMSend(&Req);

	PMRecv(&Res);
	PMRecv(&Res);	
}

static void CmdPmtConf(void)
{
	memset(&Req, 0, sizeof(Req));
	memset(&Res, 0, sizeof(Res));

	Req.Cmd = CMD_PAYMENT_CONF;
	strcpy(Req.pbyStan, pbyStanCurrent);
	Req.Stats = STATS_OK;

	PMSend(&Req);
	int count = 0;
	count++;
	// No response so no PMRecv
}

static void CmdEject(void)
{
	memset(&Req, 0, sizeof(Req));
	memset(&Res, 0, sizeof(Res));

	Req.Cmd = CMD_CARD_EJECT;
	Req.byTimeout = 60;
	PMSend(&Req);

	PMRecv(&Res);
}

static void CmdDisable(void)
{
	memset(&Req, 0, sizeof(Req));
	memset(&Res, 0, sizeof(Res));

	Req.Cmd = CMD_DISABLE_READER;
	PMSend(&Req);

	PMRecv(&Res);
}

static void DumpReq(TERMINAL_REQ* pReq)
{
	// todo
}

static void DumpRes(TERMINAL_RES* pRes)
{
	// todo
}