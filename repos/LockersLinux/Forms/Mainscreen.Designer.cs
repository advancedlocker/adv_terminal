﻿namespace Lockers
{
    partial class Mainscreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mainscreen));
            this.bOpen = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnAdmin = new System.Windows.Forms.Button();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lbltime = new System.Windows.Forms.Label();
            this.labTimeHead = new System.Windows.Forms.Label();
            this.bRent = new System.Windows.Forms.Button();
            this.labTapToRent = new System.Windows.Forms.Label();
            this.labTapToAccess = new System.Windows.Forms.Label();
            this.labRent = new System.Windows.Forms.Label();
            this.labLockerA = new System.Windows.Forms.Label();
            this.labLockerB = new System.Windows.Forms.Label();
            this.labOpen = new System.Windows.Forms.Label();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bOpen
            // 
            this.bOpen.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bOpen, "bOpen");
            this.bOpen.FlatAppearance.BorderSize = 0;
            this.bOpen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bOpen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bOpen.Name = "bOpen";
            this.bOpen.UseVisualStyleBackColor = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnAdmin
            // 
            this.btnAdmin.BackColor = System.Drawing.Color.Transparent;
            this.btnAdmin.FlatAppearance.BorderSize = 0;
            this.btnAdmin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnAdmin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnAdmin, "btnAdmin");
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.UseVisualStyleBackColor = false;
            this.btnAdmin.Click += new System.EventHandler(this.btnAdmin_Click);
            // 
            // timer2
            // 
            this.timer2.Interval = 10000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            // 
            // labTimeHead
            // 
            resources.ApplyResources(this.labTimeHead, "labTimeHead");
            this.labTimeHead.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHead.ForeColor = System.Drawing.Color.White;
            this.labTimeHead.Name = "labTimeHead";
            // 
            // bRent
            // 
            this.bRent.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bRent, "bRent");
            this.bRent.FlatAppearance.BorderSize = 0;
            this.bRent.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.bRent.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bRent.Name = "bRent";
            this.bRent.UseVisualStyleBackColor = false;
            this.bRent.Click += new System.EventHandler(this.bRent_Click);
            // 
            // labTapToRent
            // 
            this.labTapToRent.BackColor = System.Drawing.Color.Transparent;
            this.labTapToRent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labTapToRent, "labTapToRent");
            this.labTapToRent.ForeColor = System.Drawing.Color.White;
            this.labTapToRent.Name = "labTapToRent";
            // 
            // labTapToAccess
            // 
            this.labTapToAccess.BackColor = System.Drawing.Color.Transparent;
            this.labTapToAccess.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labTapToAccess, "labTapToAccess");
            this.labTapToAccess.ForeColor = System.Drawing.Color.White;
            this.labTapToAccess.Name = "labTapToAccess";
            // 
            // labRent
            // 
            this.labRent.BackColor = System.Drawing.Color.White;
            this.labRent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labRent, "labRent");
            this.labRent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labRent.Name = "labRent";
            this.labRent.Click += new System.EventHandler(this.LabRent_Click);
            // 
            // labLockerA
            // 
            this.labLockerA.BackColor = System.Drawing.Color.White;
            this.labLockerA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labLockerA, "labLockerA");
            this.labLockerA.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labLockerA.Name = "labLockerA";
            this.labLockerA.Click += new System.EventHandler(this.LabRent_Click);
            // 
            // labLockerB
            // 
            this.labLockerB.BackColor = System.Drawing.Color.White;
            this.labLockerB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labLockerB, "labLockerB");
            this.labLockerB.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labLockerB.Name = "labLockerB";
            this.labLockerB.Click += new System.EventHandler(this.LabOpen_Click);
            // 
            // labOpen
            // 
            this.labOpen.BackColor = System.Drawing.Color.White;
            this.labOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labOpen, "labOpen");
            this.labOpen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labOpen.Name = "labOpen";
            this.labOpen.Click += new System.EventHandler(this.LabOpen_Click);
            // 
            // timer3
            // 
            this.timer3.Enabled = true;
            this.timer3.Interval = 20000;
            this.timer3.Tick += new System.EventHandler(this.Timer3_Tick);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Name = "label1";
            // 
            // Mainscreen
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(195)))), ((int)(((byte)(65)))));
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labOpen);
            this.Controls.Add(this.labLockerB);
            this.Controls.Add(this.labLockerA);
            this.Controls.Add(this.labTapToAccess);
            this.Controls.Add(this.labTapToRent);
            this.Controls.Add(this.labTimeHead);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.btnAdmin);
            this.Controls.Add(this.bOpen);
            this.Controls.Add(this.labRent);
            this.Controls.Add(this.bRent);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "Mainscreen";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mainscreen_FormClosing);
            this.Load += new System.EventHandler(this.Mainscreen_Load);
            this.Shown += new System.EventHandler(this.Mainscreen_Shown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Mainscreen_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bRent;
        private System.Windows.Forms.Button bOpen;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnAdmin;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label labTimeHead;
        private System.Windows.Forms.Label labTapToRent;
        private System.Windows.Forms.Label labTapToAccess;
        private System.Windows.Forms.Label labRent;
        private System.Windows.Forms.Label labLockerA;
        private System.Windows.Forms.Label labLockerB;
        private System.Windows.Forms.Label labOpen;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Label label1;
    }
}