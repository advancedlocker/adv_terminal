﻿namespace Lockers
{
    partial class DoneForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DoneForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.labTimeHead = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.labThankYou = new System.Windows.Forms.Label();
            this.labNiceDay = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.labTimeHead);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Name = "panel1";
            // 
            // labTimeHead
            // 
            resources.ApplyResources(this.labTimeHead, "labTimeHead");
            this.labTimeHead.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHead.ForeColor = System.Drawing.Color.White;
            this.labTimeHead.Name = "labTimeHead";
            // 
            // button14
            // 
            resources.ApplyResources(this.button14, "button14");
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button14.Name = "button14";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Name = "label2";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // labThankYou
            // 
            resources.ApplyResources(this.labThankYou, "labThankYou");
            this.labThankYou.BackColor = System.Drawing.Color.Transparent;
            this.labThankYou.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labThankYou.ForeColor = System.Drawing.Color.White;
            this.labThankYou.Name = "labThankYou";
            // 
            // labNiceDay
            // 
            resources.ApplyResources(this.labNiceDay, "labNiceDay");
            this.labNiceDay.BackColor = System.Drawing.Color.Transparent;
            this.labNiceDay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labNiceDay.ForeColor = System.Drawing.Color.White;
            this.labNiceDay.Name = "labNiceDay";
            // 
            // DoneForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.labNiceDay);
            this.Controls.Add(this.labThankYou);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DoneForm";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.DoneForm_Load);
            this.Click += new System.EventHandler(this.DoneForm_Click);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labTimeHead;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timIdle;
        private System.Windows.Forms.Label labThankYou;
        private System.Windows.Forms.Label labNiceDay;
    }
}