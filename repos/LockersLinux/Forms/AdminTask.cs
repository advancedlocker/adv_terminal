﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Lockers
{
    public partial class AdminTask : Form 
    {
        private SmartPay mpay=new SmartPay();

        //public AdminTask()
        //{

        //}
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        string CCApproved = "CC_Approve.txt";
        string CCDeclined = "CC_Declined.txt";
        string CCFail = "CC_Fail.txt";
        string CCTimeout = "CC_Timeout.txt";
        string CCCancel = "CC_Cancel.txt";
        string CCError = "CC_Error.txt";
        string CCBusy = "CC_Busy.txt";
        string CCNotEmpty = "CC_NotEmpty.txt";
        string fpCancel = "CCReqCancel.txt";

        SmartPay smartpay;

        public AdminTask(SmartPay smartpay)
        {
            InitializeComponent();
            this.smartpay = smartpay;
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
            timIdle.Start();
        }

        private void bTroubleShootLocker_Click(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "\n\nTroubleShooting Locker\n\n", "");

            TroubleshootLocker troubleshoot = new TroubleshootLocker();
            troubleshoot.BringToFront();
            troubleshoot.TopMost = true;

            timIdle.Stop();

            troubleshoot.ShowDialog();

            timIdle.Start();
        }

        private void bPaymentSetup_Click(object sender, EventArgs e)
        {

            SetupPaysystemBasic setUpPaySystem = new SetupPaysystemBasic(smartpay);
            setUpPaySystem.BringToFront();
            setUpPaySystem.TopMost = true;
            setUpPaySystem.ShowDialog();
            setUpPaySystem = null;
            return;
        }

        private void bRestartTerminal_Click(object sender, EventArgs e)
        {
            RestartTerminal restartterminal = new RestartTerminal();
            restartterminal.BringToFront();
            restartterminal.TopMost = true;
            restartterminal.ShowDialog();
        }

        private void AdminTask_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
//            this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Properties.Settings.Default.DefaultLanguage + "/Blank_1182x789.tiff");

            label1.Text = Globals.SoftwareVersion;
        }

        private void bAdminCash_Click(object sender, EventArgs e)
        {
            timIdle.Stop();

            AdminPayment AdminPayment = new AdminPayment(smartpay);
            AdminPayment.ShowDialog();

            timIdle.Start();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void bCreditTest_Click(object sender, EventArgs e)
        {
            CCTest();
        }

        bool CCinProcess = false;
        Process process = new Process();

        static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //* Do your stuff with the output (write to console/log/StringBuilder)
            Console.WriteLine(outLine.Data);
        }

        private void CCProcess_Exited(object sender, System.EventArgs e)
        {
            CCinProcess = false;
            Console.WriteLine("Exit time:    {0}\r\n" +
                "Exit code:    {1}\r\n", process.ExitTime, process.ExitCode);
        }

        private void CCTest()
        {
            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Payment Setup");

            if(Properties.Settings.Default.ReleaseType == "Linux")
            process.StartInfo.FileName = Properties.Settings.Default.ProjectRootLinux + "Lockers\\bin\\Debug\\AdvamSerialDemo.exe";
            else
                process.StartInfo.FileName = Properties.Settings.Default.ProjectRootWindows + "\\AdvamSerialDemo.exe";

            if (Properties.Settings.Default.ReleaseType == "Windows")
                process.StartInfo.Arguments = " " + Properties.Settings.Default.AdvamReaderPortWindows + " " + Globals.DueAmnt.ToString($"1.00");
            else
                process.StartInfo.Arguments = " " + Properties.Settings.Default.AdvamReaderPortLinux + " " + Globals.DueAmnt.ToString($"1.00");

            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = true;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.RedirectStandardOutput = false;
            process.StartInfo.RedirectStandardError = false;

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Payment Setup complete");

            process.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
            process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);

            process.Exited += new EventHandler(CCProcess_Exited);
            process.Start();

            string output = "";
            string err = "";

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Payment Process started");

            // allow 50ms for the files to delete
            File.Delete(CCApproved);
            File.Delete(CCDeclined);
            File.Delete(CCFail);
            File.Delete(CCTimeout);
            File.Delete(CCCancel);
            File.Delete(CCError);
            File.Delete(CCBusy);
            File.Delete(CCNotEmpty);
            File.Delete(fpCancel);
            System.Threading.Thread.Sleep(50);

            try
            {
                //                output = process.StandardOutput.ReadToEnd();
                //                err = process.StandardError.ReadToEnd();

                //                process.BeginOutputReadLine();
                //                process.BeginErrorReadLine();

                CCinProcess = true;
                while (CCinProcess)
                {
                    if (File.Exists(CCApproved))
                    {
                        output = "Approved";
                        CCinProcess = false;
                        break;
                    }

                    if (File.Exists(CCDeclined))
                    {
                        output = "Declined";
                        CCinProcess = false;
                        break;
                    }

                    if (File.Exists(CCFail))
                    {
                        output = "CARD_READ_FAIL";
                        CCinProcess = false;
                        break;
                    }

                    if (File.Exists(CCCancel))
                    {
                        output = "TRANSACTION_CANCELLED";
                        CCinProcess = false;
                        break;
                    }

                    if (File.Exists(CCTimeout))
                    {
                        output = "Timeout";
                        CCinProcess = false;
                        break;
                    }

                    if (File.Exists(CCError))
                    {
                        output = "Error";
                        CCinProcess = false;
                        break;
                    }

                    if (File.Exists(CCBusy))
                    {
                        output = "TERMINAL BUSY";
                        CCinProcess = false;
                        break;
                    }

                    if (File.Exists(CCNotEmpty))
                    {
                        output = "NotEmpty";
                        CCinProcess = false;
                        break;
                    }

                    process.WaitForExit(50);
                    Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "!CCinProcess");
                    Application.DoEvents();
                    process.Refresh();
                    Thread.Sleep(25);

                    if (process.HasExited)
                    {
                        Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "process.HasExited");
                        CCinProcess = false;
                    }
                }
            }
            catch (Exception Ex)
            {
                // Log error.
                Console.WriteLine(Ex.ToString());
            }

/*
            File.Delete(CCApproved);
            File.Delete(CCDeclined);
            File.Delete(CCFail);
            File.Delete(CCTimeout);
            File.Delete(CCCancel);
            File.Delete(CCError);
            File.Delete(CCBusy);
            File.Delete(CCNotEmpty);
*/
            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "PaymentResult = " + output);

            if (output.Contains("Approved"))
            {
                this.DialogResult = DialogResult.Yes;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "(00) APPROVED");
            }
            else if (output.Contains("Declined"))
            {
                this.DialogResult = DialogResult.No;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "(05) DECLINED");
            }
            else if (
                output.Contains("CARD_PMT_FAIL") ||
                output.Contains("CARD_READ_FAIL") ||
                output.Contains("TRANSACTION_CANCELLED") ||
                output.Contains("CARD NOT ALLOWED") ||
                output.Contains("TERMINAL BUSY")
            //                output.Contains("MSGID007PMT_RESRCODE000RTEXT000TSTAN000RECPT000SCHID000TCARD000TAUTH000CTYPE000EAUTH000STATS0011")
            )
            {
                File.CreateText(fpCancel);
                this.DialogResult = DialogResult.Cancel;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CREDIT Cancelled");
            }
            else
            {
                this.DialogResult = DialogResult.Abort;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CREDIT Timeout");
            }

            this.Close();
        }

        private void CCTestOld()
        {
            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Payment Setup");

            process.StartInfo.FileName = Properties.Settings.Default.ProjectRootLinux + "Lockers\\bin\\Debug\\AdvamSerialDemo.exe";

            if (Properties.Settings.Default.ReleaseType == "Windows")
                process.StartInfo.Arguments = " " + Properties.Settings.Default.AdvamReaderPortWindows + " " + Globals.DueAmnt.ToString($"F{2}");
            else
                process.StartInfo.Arguments = " " + Properties.Settings.Default.AdvamReaderPortLinux + " " + Globals.DueAmnt.ToString($"F{2}");

            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Payment Setup complete");

            process.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);
            process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);

            process.Exited += new EventHandler(CCProcess_Exited);
            process.Start();

            string output = "";
            string err = "";

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "Payment Process started");

            try
            {
                //                output = process.StandardOutput.ReadToEnd();
                //                err = process.StandardError.ReadToEnd();

                //                process.BeginOutputReadLine();
                //                process.BeginErrorReadLine();

                while (!CCinProcess)
                {
                    output = process.StandardOutput.ReadToEnd();
                    err = process.StandardError.ReadToEnd();

                    process.WaitForExit(1000);
                    Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "!CCinProcess");
                    Application.DoEvents();
                    process.Refresh();
                    Thread.Sleep(25);

                    if (process.HasExited)
                    {
                        Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "process.HasExited");
                        CCinProcess = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                // Log error.
                Console.WriteLine(Ex.ToString());
            }

            Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "PaymentResult = " + output);

            if (output.Contains("(00) APPROVED"))
            {
                this.DialogResult = DialogResult.Yes;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "(00) APPROVED");
            }
            else if (output.Contains("(05) DECLINED"))
            {
                this.DialogResult = DialogResult.No;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "(05) DECLINED");
            }
            else if (
                output.Contains("CARD_PMT_FAIL") ||
                output.Contains("CARD_READ_FAIL") ||
                output.Contains("TRANSACTION_CANCELLED") ||
                output.Contains("CARD NOT ALLOWED") ||
                output.Contains("TERMINAL BUSY")
            //                output.Contains("MSGID007PMT_RESRCODE000RTEXT000TSTAN000RECPT000SCHID000TCARD000TAUTH000CTYPE000EAUTH000STATS0011")
            )
            {
                this.DialogResult = DialogResult.Cancel;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CREDIT Cancelled");
            }
            else
            {
                this.DialogResult = DialogResult.Abort;
                Globals.LogFile(Globals.LogSource.CreditPayment, "CCThread()", "CREDIT Timeout");
            }

            timIdle.Stop();
            this.Close();
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "timIdle_Tick()", "Idle timeout");

            timIdle.Stop();
            Globals.AbortForm = true;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                File.CreateText("CCReqCancel.txt");
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }

            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
