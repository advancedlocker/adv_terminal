﻿namespace Lockers
{
    partial class AdminTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminTask));
            this.bTroubleShootLocker = new System.Windows.Forms.Button();
            this.bAdminCash = new System.Windows.Forms.Button();
            this.bResetTerminal = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.bPaymentSetup = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.bCreditTest = new System.Windows.Forms.Button();
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // bTroubleShootLocker
            // 
            this.bTroubleShootLocker.Location = new System.Drawing.Point(375, 149);
            this.bTroubleShootLocker.Name = "bTroubleShootLocker";
            this.bTroubleShootLocker.Size = new System.Drawing.Size(317, 115);
            this.bTroubleShootLocker.TabIndex = 0;
            this.bTroubleShootLocker.Text = "TROUBLESHOOT LOCKER";
            this.bTroubleShootLocker.UseVisualStyleBackColor = true;
            this.bTroubleShootLocker.Click += new System.EventHandler(this.bTroubleShootLocker_Click);
            // 
            // bAdminCash
            // 
            this.bAdminCash.Enabled = false;
            this.bAdminCash.Location = new System.Drawing.Point(375, 270);
            this.bAdminCash.Name = "bAdminCash";
            this.bAdminCash.Size = new System.Drawing.Size(317, 115);
            this.bAdminCash.TabIndex = 1;
            this.bAdminCash.Text = "CASH ATM";
            this.bAdminCash.UseVisualStyleBackColor = true;
            this.bAdminCash.Click += new System.EventHandler(this.bAdminCash_Click);
            // 
            // bResetTerminal
            // 
            this.bResetTerminal.Location = new System.Drawing.Point(375, 512);
            this.bResetTerminal.Name = "bResetTerminal";
            this.bResetTerminal.Size = new System.Drawing.Size(317, 115);
            this.bResetTerminal.TabIndex = 2;
            this.bResetTerminal.Text = "TERMINAL MAINTENANCE";
            this.bResetTerminal.UseVisualStyleBackColor = true;
            this.bResetTerminal.Click += new System.EventHandler(this.bRestartTerminal_Click);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            this.bBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBack.BackgroundImage")));
            this.bBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBack.Location = new System.Drawing.Point(33, 648);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(80, 80);
            this.bBack.TabIndex = 4;
            this.bBack.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(416, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(240, 46);
            this.label2.TabIndex = 6;
            this.label2.Text = "Admin Task";
            // 
            // bPaymentSetup
            // 
            this.bPaymentSetup.Location = new System.Drawing.Point(375, 391);
            this.bPaymentSetup.Name = "bPaymentSetup";
            this.bPaymentSetup.Size = new System.Drawing.Size(136, 115);
            this.bPaymentSetup.TabIndex = 7;
            this.bPaymentSetup.Text = "PAYMENT SYSTEM SETUP";
            this.bPaymentSetup.UseVisualStyleBackColor = true;
            this.bPaymentSetup.Click += new System.EventHandler(this.bPaymentSetup_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(516, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "1.0.1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // bCreditTest
            // 
            this.bCreditTest.Location = new System.Drawing.Point(559, 391);
            this.bCreditTest.Name = "bCreditTest";
            this.bCreditTest.Size = new System.Drawing.Size(133, 115);
            this.bCreditTest.TabIndex = 9;
            this.bCreditTest.Text = "CREDIT SYSTEM TEST";
            this.bCreditTest.UseVisualStyleBackColor = true;
            this.bCreditTest.Click += new System.EventHandler(this.bCreditTest_Click);
            // 
            // timIdle
            // 
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // AdminTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.bCreditTest);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bPaymentSetup);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bResetTerminal);
            this.Controls.Add(this.bAdminCash);
            this.Controls.Add(this.bTroubleShootLocker);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AdminTask";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AdminTask";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.AdminTask_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bTroubleShootLocker;
        private System.Windows.Forms.Button bAdminCash;
        private System.Windows.Forms.Button bResetTerminal;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bPaymentSetup;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bCreditTest;
        private System.Windows.Forms.Timer timIdle;
    }
}