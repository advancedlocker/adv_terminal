﻿namespace Lockers
{
    partial class OpenLocker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OpenLocker));
            this.bOpenLocker = new System.Windows.Forms.Button();
            this.bEndRental = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.timRTC = new System.Windows.Forms.Timer(this.components);
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.labComing = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.labFinished = new System.Windows.Forms.Label();
            this.labBack = new System.Windows.Forms.Label();
            this.labHdgPressOK = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bOpenLocker
            // 
            this.bOpenLocker.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bOpenLocker, "bOpenLocker");
            this.bOpenLocker.FlatAppearance.BorderSize = 0;
            this.bOpenLocker.Name = "bOpenLocker";
            this.bOpenLocker.UseVisualStyleBackColor = false;
            this.bOpenLocker.Click += new System.EventHandler(this.bOpenLocker_Click);
            // 
            // bEndRental
            // 
            this.bEndRental.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bEndRental, "bEndRental");
            this.bEndRental.FlatAppearance.BorderSize = 0;
            this.bEndRental.Name = "bEndRental";
            this.bEndRental.UseVisualStyleBackColor = false;
            this.bEndRental.Click += new System.EventHandler(this.bEndRental_Click);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bBack, "bBack");
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.Name = "bBack";
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Name = "label1";
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            // 
            // timRTC
            // 
            this.timRTC.Interval = 1000;
            this.timRTC.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // labComing
            // 
            resources.ApplyResources(this.labComing, "labComing");
            this.labComing.BackColor = System.Drawing.Color.White;
            this.labComing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labComing.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.labComing.Name = "labComing";
            this.labComing.Click += new System.EventHandler(this.bOpenLocker_Click);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Name = "label3";
            this.label3.Click += new System.EventHandler(this.bOpenLocker_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Name = "label4";
            this.label4.Click += new System.EventHandler(this.bEndRental_Click);
            // 
            // labFinished
            // 
            resources.ApplyResources(this.labFinished, "labFinished");
            this.labFinished.BackColor = System.Drawing.Color.White;
            this.labFinished.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labFinished.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.labFinished.Name = "labFinished";
            this.labFinished.Click += new System.EventHandler(this.bEndRental_Click);
            // 
            // labBack
            // 
            this.labBack.BackColor = System.Drawing.Color.Transparent;
            this.labBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labBack.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.labBack, "labBack");
            this.labBack.Name = "labBack";
            // 
            // labHdgPressOK
            // 
            this.labHdgPressOK.BackColor = System.Drawing.Color.Transparent;
            this.labHdgPressOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labHdgPressOK, "labHdgPressOK");
            this.labHdgPressOK.ForeColor = System.Drawing.Color.White;
            this.labHdgPressOK.Name = "labHdgPressOK";
            // 
            // OpenLocker
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.labHdgPressOK);
            this.Controls.Add(this.labBack);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labFinished);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labComing);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bEndRental);
            this.Controls.Add(this.bOpenLocker);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OpenLocker";
            this.ShowIcon = false;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.OpenLocker_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bOpenLocker;
        private System.Windows.Forms.Button bEndRental;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timRTC;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer timIdle;
        private System.Windows.Forms.Label labComing;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labFinished;
        private System.Windows.Forms.Label labBack;
        private System.Windows.Forms.Label labHdgPressOK;
    }
}