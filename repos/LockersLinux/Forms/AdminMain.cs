﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Lockers

{
    public partial class AdminMain : Form  
    {

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        SmartPay smartpay;

        //public AdminMain()
        //{
        //    InitializeComponent();
        //}

        public AdminMain(SmartPay smartpay) 
        {
            InitializeComponent();
            this.smartpay = smartpay;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AdminMain_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            if(Properties.Settings.Default.ReleaseType == "Linux")
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Properties.Settings.Default.DefaultLanguage + "\\Blank_1182x789.tiff");
            else
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Properties.Settings.Default.DefaultLanguage + "\\Blank_1182x789.tiff");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AdminTask admintask = new AdminTask(smartpay );
            admintask.ShowDialog();
            admintask = null;


        }
    }
}
