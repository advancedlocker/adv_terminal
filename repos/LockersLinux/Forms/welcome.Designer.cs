﻿namespace Lockers
{
    partial class welcome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(welcome));
            this.timRTC = new System.Windows.Forms.Timer(this.components);
            this.lbltime = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labTime = new System.Windows.Forms.Label();
            this.labPaymentStatus = new System.Windows.Forms.Label();
            this.labWelcome = new System.Windows.Forms.Label();
            this.labPressFlag = new System.Windows.Forms.Label();
            this.timLanguage = new System.Windows.Forms.Timer(this.components);
            this.labDebug = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // timRTC
            // 
            this.timRTC.Enabled = true;
            this.timRTC.Interval = 1000;
            this.timRTC.Tick += new System.EventHandler(this.timRTC_Tick);
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.labTime);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Name = "panel1";
            // 
            // labTime
            // 
            this.labTime.BackColor = System.Drawing.Color.Transparent;
            this.labTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTime.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.labTime, "labTime");
            this.labTime.Name = "labTime";
            // 
            // labPaymentStatus
            // 
            this.labPaymentStatus.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.labPaymentStatus, "labPaymentStatus");
            this.labPaymentStatus.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labPaymentStatus.Name = "labPaymentStatus";
            // 
            // labWelcome
            // 
            resources.ApplyResources(this.labWelcome, "labWelcome");
            this.labWelcome.BackColor = System.Drawing.Color.Transparent;
            this.labWelcome.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labWelcome.Name = "labWelcome";
            this.labWelcome.Click += new System.EventHandler(this.LabWelcome_Click);
            // 
            // labPressFlag
            // 
            this.labPressFlag.BackColor = System.Drawing.Color.Transparent;
            this.labPressFlag.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            resources.ApplyResources(this.labPressFlag, "labPressFlag");
            this.labPressFlag.Name = "labPressFlag";
            // 
            // timLanguage
            // 
            this.timLanguage.Enabled = true;
            this.timLanguage.Interval = 5000;
            this.timLanguage.Tick += new System.EventHandler(this.TimLanguage_Tick);
            // 
            // labDebug
            // 
            this.labDebug.BackColor = System.Drawing.Color.Transparent;
            this.labDebug.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            resources.ApplyResources(this.labDebug, "labDebug");
            this.labDebug.Name = "labDebug";
            // 
            // welcome
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(195)))), ((int)(((byte)(65)))));
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.labPaymentStatus);
            this.Controls.Add(this.labDebug);
            this.Controls.Add(this.labPressFlag);
            this.Controls.Add(this.labWelcome);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "welcome";
            this.Load += new System.EventHandler(this.welcome_Load);
            this.Shown += new System.EventHandler(this.Welcome_Shown);
            this.Click += new System.EventHandler(this.welcome_Click);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Timer timRTC;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labTime;
        private System.Windows.Forms.Label labWelcome;
        private System.Windows.Forms.Label labPressFlag;
        private System.Windows.Forms.Timer timLanguage;
        private System.Windows.Forms.Label labDebug;
        private System.Windows.Forms.Label labPaymentStatus;
    }
}