﻿namespace Lockers
{
    partial class OpenLocker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OpenLocker));
            this.bOpenLocker = new System.Windows.Forms.Button();
            this.bEndRental = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bHelp = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bOpenLocker
            // 
            this.bOpenLocker.BackColor = System.Drawing.Color.Transparent;
            this.bOpenLocker.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bOpenLocker.FlatAppearance.BorderSize = 0;
            this.bOpenLocker.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bOpenLocker.Location = new System.Drawing.Point(87, 242);
            this.bOpenLocker.Name = "bOpenLocker";
            this.bOpenLocker.Size = new System.Drawing.Size(380, 306);
            this.bOpenLocker.TabIndex = 1;
            this.bOpenLocker.UseVisualStyleBackColor = false;
            this.bOpenLocker.Click += new System.EventHandler(this.bOpenLocker_Click);
            // 
            // bEndRental
            // 
            this.bEndRental.BackColor = System.Drawing.Color.Transparent;
            this.bEndRental.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bEndRental.FlatAppearance.BorderSize = 0;
            this.bEndRental.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bEndRental.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bEndRental.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bEndRental.Location = new System.Drawing.Point(560, 242);
            this.bEndRental.Name = "bEndRental";
            this.bEndRental.Size = new System.Drawing.Size(380, 306);
            this.bEndRental.TabIndex = 1;
            this.bEndRental.UseVisualStyleBackColor = false;
            this.bEndRental.Click += new System.EventHandler(this.bEndRental_Click);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            this.bBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBack.BackgroundImage")));
            this.bBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBack.Location = new System.Drawing.Point(35, 642);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(79, 111);
            this.bBack.TabIndex = 3;
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.bHelp);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 164);
            this.panel1.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(940, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 29);
            this.label2.TabIndex = 1;
            this.label2.Text = "help";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "TIME";
            // 
            // bHelp
            // 
            this.bHelp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bHelp.BackgroundImage")));
            this.bHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bHelp.FlatAppearance.BorderSize = 0;
            this.bHelp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bHelp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bHelp.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bHelp.Location = new System.Drawing.Point(928, 24);
            this.bHelp.Name = "bHelp";
            this.bHelp.Size = new System.Drawing.Size(79, 81);
            this.bHelp.TabIndex = 2;
            this.bHelp.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(7, 47);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(225, 55);
            this.lbltime.TabIndex = 1;
            this.lbltime.Text = "01:22 pm";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // OpenLocker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bEndRental);
            this.Controls.Add(this.bOpenLocker);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OpenLocker";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.OpenLocker_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button bOpenLocker;
        private System.Windows.Forms.Button bEndRental;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bHelp;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer timIdle;
    }
}