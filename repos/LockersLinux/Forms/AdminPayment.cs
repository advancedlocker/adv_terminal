﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
namespace Lockers
{
    public partial class AdminPayment : Form
    {
        private delegate void UpdatePayDelegate(float value1);
        private UpdatePayDelegate updatepaydelegate = null;

        private delegate void UpdateChangeDelegate(double  value1);
        private UpdateChangeDelegate updatechangedelegate = null;

        private delegate void EnableBtnDelegate(bool value);
        private EnableBtnDelegate enablebtndelegate = null;

//        SmartPay smartpay;
        float totalpaid = 0;
        double changeamnt = 0;
        private List<Payment> BillsInserted = new List<Payment> { };
        private List<Payment> CoinInserted = new List<Payment> { };

        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            decimal mtender;
            decimal mchange;

            decimal.TryParse(lblpay.Text, out mtender);
            decimal.TryParse(lblchange.Text,out mchange);

            Globals.LockData.Amnt_Tender_Set(mtender);
            Globals.LockData.Amnt_Change_Set(mchange);

            this.totalpaid = 0;
            //Globals.Paymode = false;

            Globals.smartpay.StopMainLoop = true;

            Globals.smartpay.VALIDATOR.DisableValidator();
            Globals.smartpay.HOPPER.DisableValidator();
            Globals.smartpay.PAYOUT.DisableValidator();

            Globals.smartpay.PAYOUT.PaidAmountEvent -= this.Smartpay_PaidAmount;
            Globals.smartpay.PAYOUT.Dispensed -= this.Validator_Dispensed;

            Globals.smartpay.VALIDATOR.PaidAmountEvent -= this.Smartpay_PaidAmount;
            Globals.smartpay.VALIDATOR.NoteStored -= this.Validator_NoteStored;
            Globals.smartpay.VALIDATOR.OutOfService -= this.Validator_OutofService;
            Globals.smartpay.VALIDATOR.CashBoxFull -= this.Stacker_Full;
            Globals.smartpay.VALIDATOR.FraudAttemp -= this.Fraud_Attempt;
            Globals.smartpay.VALIDATOR.UnSafe_Jam -= this.UnSafe_Jam;
            Globals.smartpay.VALIDATOR.Safe_Jam -= this.Safe_Jam;
            Globals.smartpay.VALIDATOR.RejectedNote -= this.RejectedNotes;
            //this.smartpay.VALIDATOR.NoEnoughPayout -= this.NoEnoughPayout;
            Globals.smartpay.VALIDATOR.Dispensed -= this.Validator_Dispensed;
            Globals.smartpay.VALIDATOR.ReadingNote -= this.Validator_Reading;
            Globals.smartpay.Done_Payout -= this.Smartpay_DonePayout;

            Globals.smartpay.HOPPER.PaidAmountEvent -= this.Smartpay_CoinPaidAmount;
            //this.smartpay.HOPPER.CoinDispened -= this.Coin_Dispensed;
        }

        public AdminPayment(SmartPay smartpay)
        {
            InitializeComponent();
            this.Show();

            if(Properties.Settings.Default.ReleaseType == "Linux")
               this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Properties.Settings.Default.DefaultLanguage + "/CashPayment.tiff");
            else
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Properties.Settings.Default.DefaultLanguage + "\\CashPayment.tiff");

            this.updatepaydelegate = new UpdatePayDelegate(updatePay);
            this.updatechangedelegate = new UpdateChangeDelegate(updatechange);
            this.enablebtndelegate = new EnableBtnDelegate(enableButton);

// SMC : changed this.smartpay to the gGlobals instance
            Globals.smartpay = new SmartPay();
//            Globals.smartpay.HOPPER.PaidAmountEvent += this.Smartpay_CoinPaidAmount;
//            Globals.smartpay.Start();         // SMC :this starts the threading. avoid this for the moment and handle onluy in the screen

            // Start the payment systems

//            Globals.smartpay.ConnectToSMARTPayout();

            Globals.smartpay.PAYOUT.PaidAmountEvent += this.Smartpay_PaidAmount;
/*            Globals.smartpay.PAYOUT.NoteStored += this.Validator_NoteStored;
            Globals.smartpay.PAYOUT.OutOfService += this.Validator_OutofService;
            Globals.smartpay.PAYOUT.CashBoxFull += this.Stacker_Full;
            Globals.smartpay.PAYOUT.FraudAttemp += this.Fraud_Attempt;
            Globals.smartpay.PAYOUT.UnSafe_Jam += this.UnSafe_Jam;
            Globals.smartpay.PAYOUT.Safe_Jam += this.Safe_Jam;
            Globals.smartpay.PAYOUT.RejectedNote += this.RejectedNotes;
*/
            Globals.smartpay.PAYOUT.Dispensed += this.Validator_Dispensed;
            //            Globals.smartpay.PAYOUT.ReadingNote += this.Validator_Reading;


            Globals.smartpay.VALIDATOR.PaidAmountEvent += this.Smartpay_PaidAmount;
            Globals.smartpay.VALIDATOR.NoteStored += this.Validator_NoteStored;
            Globals.smartpay.VALIDATOR.OutOfService += this.Validator_OutofService;
            Globals.smartpay.VALIDATOR.CashBoxFull += this.Stacker_Full;
            Globals.smartpay.VALIDATOR.FraudAttemp += this.Fraud_Attempt;
            Globals.smartpay.VALIDATOR.UnSafe_Jam += this.UnSafe_Jam;
            Globals.smartpay.VALIDATOR.Safe_Jam += this.Safe_Jam;
            Globals.smartpay.VALIDATOR.RejectedNote += this.RejectedNotes;
            Globals.smartpay.VALIDATOR.Dispensed += this.Validator_Dispensed;
            Globals.smartpay.VALIDATOR.ReadingNote += this.Validator_Reading;
            Globals.smartpay.Done_Payout += this.Smartpay_DonePayout;

            Globals.smartpay.HOPPER.PaidAmountEvent += this.Smartpay_CoinPaidAmount;

/*
            Globals.smartpay.PAYOUT.EnableValidator();
            CHelpers.Pause(300);

            Globals.smartpay.VALIDATOR.EnableValidator();
            CHelpers.Pause(300);

            Globals.smartpay.HOPPER.EnableValidator();
            CHelpers.Pause(200);
*/
            #region enablecashdevice
/*
            Globals.smartpay.VALIDATOR.EnableValidator();
            Globals.smartpay.HOPPER.EnableValidator();
*/

// SMC  : Removed the thread for the moment. 
/*
            ManualResetEvent syncEvent = new ManualResetEvent(false);
            Thread t1 = new Thread(
                () =>
                {

                    this.smartpay.VALIDATOR.EnableValidator();

                    syncEvent.Set();
                    //CHelpers.Pause(200);
                }
            );
            t1.Start();
*/
            //-----------------------------
            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
//            timIdle.Start();
            //------------------------------


            Globals.smartpay.SMC_MainLoop();
//            Globals.smartpay.MainLoop();
            fi.AMDesignator = "am";


            // SMC  : Removed the thread for the moment. 
            /*
                        Thread t2 = new Thread(
                            () =>
                            {
                                syncEvent.WaitOne();
                                this.smartpay.HOPPER.EnableValidator();
                                //CHelpers.Pause(200);
                            }
                        );
                        t2.Start();
            */
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            Close();
            #endregion
        }


        #region cashdevice event
        private void Smartpay_CoinPaidAmount(object sender, PaidEventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "Smartpay_CoinPaidAmount()", "Smartpay_CoinPaidAmount Event");

            this.totalpaid += e.Amount;
            Payment amntinserted = new Payment();
            amntinserted.AmountInserted = e.Amount;
            amntinserted.PSource = PaySource.Coins;
            this.CoinInserted.Add(amntinserted);

            this.Invoke(this.updatepaydelegate, new object[] { this.totalpaid });

            if (totalpaid > (double)Globals.DueAmnt)
            {
                Globals.LogFile(Globals.LogSource.SmartPay, "Smartpay_CoinPaidAmount()", "totalpaid " + totalpaid.ToString() + " > Globals.DueAmnt " + Globals.DueAmnt.ToString());

                double changeamnt = (Double)totalpaid - (double)Globals.DueAmnt;
                this.Invoke(this.updatechangedelegate, new object[] { changeamnt });
                this.Invoke(this.enablebtndelegate, new object[] { true });
                //btngo.Enabled = true;
                Globals.smartpay.Payout = (float)(Math.Round((Double)(changeamnt), 2)) * 100; // (float)changeamnt * 100;
                Globals.smartpay.CalculatePayout();
            }
            else if (totalpaid == (double)Globals.DueAmnt)
            {
                Globals.LogFile(Globals.LogSource.SmartPay, "Smartpay_CoinPaidAmount()", "totalpaid " + totalpaid.ToString() + " == Globals.DueAmnt " + Globals.DueAmnt.ToString());
                this.Invoke(this.enablebtndelegate, new object[] { true });
//                bOK.Enabled = true;
            }
            

            
            //this.BeginInvoke((Action)delegate ()
            //{
            //    lbltotal.Text = totalpaid.ToString("#,##0.00");
            //    button3.Enabled = true; label5.Visible = false;
            //});

        }

        private void RejectedNotes(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "RejectedNotes()", "RejectedNotes Event");
            //this.Invoke((Action)(() =>
            //{
            //    this.lblmsg.Text = "Invalid Notes inserted";
            //    this.lblmsg.Visible = true;
            //}));
        }
        private void UnSafe_Jam(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "UnSafe_Jam()", "UnSafe_Jam Event");
            //this.Invoke((Action)(() =>
            //{
            //    this.lblmsg.Text = "Notes Jam";
            //    this.lblmsg.Visible = true;
            //}));

        }
        private void Safe_Jam(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "Safe_Jam()", "Safe_Jam Event");
            //this.Invoke((Action)(() =>
            //{
            //    this.lblmsg.Text = "Notes Jam";
            //    this.lblmsg.Visible = true;
            //}));
        }

        private void Smartpay_DonePayout(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "Smartpay_DonePayout()", "Smartpay_DonePayout Event");
            this.DialogResult = DialogResult.OK;
            timer1.Stop();
            Close();

            /*
                        this.Invoke((Action)(() =>
                        {
                            //td.TotalPaidAmount = totalpaid;
                            //td.ChangeAmount = deference;
                            //// td.CustomerEmail = txtCustomerEmail.Text;
                            //this.totalpaid = 0;
                            //td.PaymentMethod = "Cash";
                            //td.TreatMode = "Sales";
                            //((Storyboard)this.Resources["StoryboardFadeOut"]).Begin(this);
                            this.Close();

                        }));
            */
        }

        private void Validator_Reading(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "Validator_Reading()", "Validator_Reading Event");
            //this.Dispatcher.Invoke((Action)(() =>
            //{
            //    this.lblerror1.Visibility = Visibility.Hidden;
            //    this.lblerror2.Visibility = Visibility.Hidden;
            //}));
        }

        private void Validator_Dispensed(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "Validator_Dispensed()", "Validator_Dispensed Event");
            Globals.smartpay.CalculatePayout();
        }

        private void Smartpay_PaidAmount(object sender, PaidEventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "Smartpay_PaidAmount()", "Smartpay_PaidAmount Event");

            this.totalpaid += e.Amount;
            Payment amntinserted = new Payment();
            amntinserted.AmountInserted = e.Amount;
            amntinserted.PSource = PaySource.Bills;
            BillsInserted.Add(amntinserted);

            this.Invoke(this.updatepaydelegate, new object[] { this.totalpaid });
            
            if (totalpaid > (double)Globals.DueAmnt)
            {
                double changeamnt = (Double)totalpaid - (double)Globals.DueAmnt;
                this.Invoke(this.updatechangedelegate, new object[] { changeamnt });
                this.Invoke(this.enablebtndelegate, new object[] { true });
                //btngo.Enabled = true;
                Globals.smartpay.Payout = (float)(Math.Round((Double)(changeamnt), 2)) * 100;// (float)changeamnt;
                Globals.smartpay.CalculatePayout();
            }
            else if (totalpaid == (double)Globals.DueAmnt)
            {
                this.Invoke(this.enablebtndelegate, new object[] { true });
                //btngo.Enabled = true;
            }
            //this.BeginInvoke((Action)delegate ()
            //{

            //    lbltotal.Text = totalpaid.ToString("#,##0.00");
            //    button3.Enabled = true; label5.Visible = false;
            //});
        }

        private void Validator_OutofService(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "Validator_OutofService()", "Validator_OutofService Event");
            //this.Invoke((Action)(() =>
            //{
            //    this.lblmsg.Text = "Cash device out of service";
            //    this.lblmsg.Visible = true;
            //}));
        }

        private void Validator_NoteStored(object sender, PaidEventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "Validator_NoteStored()", "Validator_NoteStored Event");

            if (totalpaid > (double)Globals.DueAmnt)
            {
                double changeamnt = (Double)totalpaid - (double)Globals.DueAmnt;
                //btngo.Enabled = true;
                Globals.smartpay.Payout = (float)(Math.Round((Double)(changeamnt), 2)) * 100;// (float)changeamnt;
                Globals.smartpay.CalculatePayout();
            }
            
        }

        private void Fraud_Attempt(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "Fraud_Attempt()", "Fraud_Attempt Event");

            //this.Invoke((Action)(() =>
            //{
            //    this.lblmsg.Text = "Fraud Attemp: Invalid Notes inserted";
            //    this.lblmsg.Visible = true;
            //}));
        }

        private void Stacker_Full(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "Stacker_Full()", "Stacker_Full Event");

            //this.Invoke((Action)(() =>
            //{
            //    this.lblmsg.Visible = true;
            //}));

        }
        #endregion


        private void updatePay(float value)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "updatePay()", "updatePay method for UI");

            lblpay.Text = value.ToString("#,##0.00");
        }

        private void enableButton(bool value)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "enableButton()", "Enable OK button");

            bOK.Enabled = value;
        }
        private void updatechange(Double  value)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "updatechange()", "updatechange method on UI");
            lblchange.Text = value.ToString("#,##0.00");
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.SmartPay, "bCancel_Click()", "bCancel_Click event");

            Globals.AbortForm = true;
            this.DialogResult = DialogResult.Cancel;
            timer1.Stop();
            CHelpers.Shutdown = true;
            this.Close();
        }

        private void CashPayment_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
            lbldue.Text = Globals.DueAmnt.ToString("#,##0.00");
        }

        private void CashPayment_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString().ToUpper().Equals("P"))
            {
                // Quit - ends kiosk    program
                e.Handled = true;
                timer1.Stop();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            // SMC added
            this.DialogResult = DialogResult.OK;
            timer1.Stop();
//            CHelpers.Shutdown = true;
            this.Close();
        }

        private void bTestPayout_Click(object sender, EventArgs e)
        {
            Globals.smartpay.Payout = (float)(2.00) * 100;// (float)changeamnt;
            Globals.smartpay.CalculatePayout();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
