﻿
// </summary>
// version locker01 - vic
// -----------------------------------------------------------------------

using System;
using System.Threading;
using System.Threading.Tasks;
/*
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.AspNetCore.SignalR.Protocol;
using Refit;
using LockerMonitor.Wrapper;
*/

namespace Lockers
{
//    using LockerMonitor.Wrapper.Models;
//    using Microsoft.AspNetCore.Http.Connections;
//    using Newtonsoft.Json.Linq;
    #region namespace
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Windows.Forms;
    using System.Xml;
    #endregion
    public partial class Mainscreen : Form
    {
        //        private String Country;
        //        private String[] Languages;
        //        private String[] Flags;
        //        Button[] LanguageButtons;

        //        DialogResult diagRes;

//        Process WatchdogProcess;

        private int runStep;
        private int mode = 0;
        private int PINStep = 0;

//        HubConnection connection;

        DateTimeFormatInfo fi = new DateTimeFormatInfo();

//        private LockerInterface LI = new LockerInterface();
        /*
                private LockerSelection lockerselection;
                private SelectSize selectsize;
                private SelectLogin selectlogin;
                private TimeSelection timeselection;
                private TimeDaySelection timedayselection;
                private PINReg pinreg;
                private PINLogin pinlogin;
                private PINImage pinimage;
                private PaymentSelection paymentscreen;
                private Unlocked unlocked;
                private RentalEnded rentend;
                private CashPayment cashpayment;
                private Paydone paydonescreen;
                private DoneForm donescreen;
                private OpenLocker openlocker;
                //        private BlackScreen blanckscreen;
        */
//        private LockerDatabase Globals.LockData;
//        private eLockerSize lsize;

#if (false)
        private SelectSize ssel;
        private TimeSelection TimSet;
#endif 

        //private Button prevBtn;


        public SmartPay smartpay;
        public Boolean deviceState = false;

        private LockerInterface LockerClient;
        private int mCRC16 = Properties.Settings.Default.CRC16;
//        private int sKey;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        //  SMC : Removed smart pay declaration
        //        public Mainscreen(SmartPay smartpay)

        private delegate void InvokeDelegate();
        private void SignalR_RXMessage()
        {
//            var newMessage = $"{user}: {message}";
            Console.WriteLine("Message recieved");
        }
        private async void SignalRStart()
        {
            /*
                        connection.On<List<String>>("openlocker", (ids) =>
                        {
                            this.BeginInvoke(new InvokeDelegate(SignalR_RXMessage));
                            Console.WriteLine("openlocker recieved");
                        });

                        connection.On<List<String>>("openLocker", (ids) =>
                        {
                            this.BeginInvoke(new InvokeDelegate(SignalR_RXMessage));
                            Console.WriteLine("openLocker recieved");
                        });

                        connection.On<List<String>>("OpenLocker", (ids) =>
                        {
                            this.BeginInvoke(new InvokeDelegate(SignalR_RXMessage));
                            Console.WriteLine("OpenLocker recieved");
                        });

                        try
                        {
                            await connection.StartAsync();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("connection.StartAsync Failed");
                        }
            */
        }

        private async void SignalRSend(string message)
        {
            return;
            /*
                        try
                        {
                            var data = new JObject();
                            data["isOpen"] = false;
                            data["isRented"] = true;
                            data["error"] = "None";

                            var jsonString = data.ToString();
                            await connection.InvokeAsync("SendNodeDataToMonitors", "test", jsonString);

                            var ll = new List<String>();
                            ll.Add("test");
                            await connection.InvokeAsync("RegisterNodes", ll);
            //                await connection.InvokeAsync("RegisterNode", ll);

                                            var CF = ClientFactory.CreateInstance("http://monitor.advancedlocker.com");
                                            var trans = new List<Transaction>();
                                            var transaction = new Transaction
                                            {
                                                TransactionId = "3",
                                                LockerId = "test",
                                                Type = TransactionType.Cash,
                                                KioskName = "Test-1",
                                                StartTime = DateTime.Now,
                                                EndTime = DateTime.Now,
                                                LockerName = "test",
                                                DurationSecs = 1000,
                                                AmountDue = 9.00,
                                                AmountTendered = 10.00,
                                                AmountChange = 1.00
                                            };

                                            trans.Add(transaction);
                                            var results = await CF.AddTransaction(trans);

                            timer3.Stop();

                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("SignalRSend Failed");
                        }

                        timer3.Stop();
            */
        }

        public Mainscreen()
        {
            InitializeComponent();

            // TODO : Fix Georges stuff
#if (false)
            connection = new HubConnectionBuilder()
                .WithUrl("http://monitor.advancedlocker.com/lockerHub", HttpTransportType.LongPolling)
                .Build();

            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await connection.StartAsync();
            };

            SignalRStart();
#endif
            timer2.Interval = Properties.Settings.Default.idletime;

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";

            // SMC removed
            //            this.smartpay = smartpay;
//            Globals.LockData = new LockerDatabase();
//            Globals.LockData = Globals.LockData;
//            string[] lockers = Globals.LockData.GetAvailableLockers();


            // Ensure that some security features are enabled if not enable LockerNumbers by default
            if (
                    (Properties.Settings.Default.SecurityFeatureAnimals == false) &&
                    (Properties.Settings.Default.SecurityFeatureLockerNumber == false)
                )
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen()", "No security set - defaulting to animals");
                Properties.Settings.Default.SecurityFeatureLockerNumber = true;
            }


            // SMC removed
            //            this.blanckscreen = new BlackScreen();

            //this.prevBtn = b1;
            //            Globals.prevBtn = Country1;

            //this.smartpay.VALIDATOR.PaidAmountEvent += this.Smartpay_PaidAmount;
            //this.smartpay.VALIDATOR.NoteStored += this.Validator_NoteStored;
            //this.smartpay.VALIDATOR.OutOfService += this.Validator_OutofService;
            //this.smartpay.VALIDATOR.CashBoxFull += this.Stacker_Full;
            //this.smartpay.VALIDATOR.FraudAttemp += this.Fraud_Attempt;
            //this.smartpay.VALIDATOR.UnSafe_Jam += this.UnSafe_Jam;
            //this.smartpay.VALIDATOR.Safe_Jam += this.Safe_Jam;
            //this.smartpay.VALIDATOR.RejectedNote += this.RejectedNotes;

            //this.smartpay.HOPPER.PaidAmountEvent += this.Smartpay_CoinPaidAmount;

            //this.smartpay.VALIDATOR.DisableValidator();
            //CHelpers.Pause(300);
            //this.smartpay.HOPPER.DisableValidator();
            //CHelpers.Pause(200);

            //ManualResetEvent syncEvent = new ManualResetEvent(false);
            //Thread t1 = new Thread(
            //    () =>
            //    {
            //        // Do some work...

            //        this.smartpay.VALIDATOR.DisableValidator();
            //        syncEvent.Set();
            //    }
            //);
            //t1.Start();

            //Thread t2 = new Thread(
            //    () =>
            //    {
            //        syncEvent.WaitOne();
            //        this.smartpay.HOPPER.DisableValidator();
            //                // Do some work...
            //            }
            //);
            //t2.Start();
        }



        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            CHelpers.Shutdown = true;
            if (this.smartpay != null)
            {
                this.smartpay.StopPaysystem();
                if (!this.smartpay.VALIDATOR.isDISABLED)
                {
                    this.smartpay.VALIDATOR.DisableValidator();
                    CHelpers.Pause(200);
                }

                if (!this.smartpay.HOPPER.Is_Disable)
                {
                    this.smartpay.HOPPER.DisableValidator();
                    CHelpers.Pause(200);
                }

                this.smartpay.Dispose();
            }
            //if (deviceState == true)
            //{
            //    Dll_Camera.ReleaseDevice();
            //}
            //else
            //{
            //    Dll_Camera.ReleaseLostDevice();
            //}

            if (e.CloseReason == CloseReason.WindowsShutDown)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen()", "\nWindows Closing Down\n");
                return;
            }
        }


        private void InvokeLanguage()
        {
            string language = Globals.GetLanguageCode();

            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            ComponentResourceManager resources = new ComponentResourceManager(typeof(Mainscreen));

            foreach (Control C in this.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    foreach (Control D in C.Controls)
                    {
                        resources.ApplyResources(D, D.Name, new CultureInfo(language));
                    }
                }
                else
                {
                    resources.ApplyResources(C, C.Name, new CultureInfo(language));
                }
            }

            this.Update();
        }

        private void Mainscreen_Load(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen()", "Mainscreen starting " + Globals.SoftwareVersion);

            Globals.EnableAutomaticRestartProcess();

            Globals.StopExplorer();

            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen()", "EnableAllowFocusChange");
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen()", "EnablePreventFocusChange");
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            labTapToRent.ForeColor = Properties.Settings.Default.TextColor;
            labTimeHead.ForeColor = Properties.Settings.Default.TextColor;
            labTapToAccess.ForeColor = Properties.Settings.Default.TextColor;
            lbltime.ForeColor = Properties.Settings.Default.TextColor;

            ResetBackground();
            timer1.Start();

            //start locker controller
            Start_LockControl();

            Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen()", "Start_LockControl() Finished");
        }

        private void Mainscreen_Shown(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Calling New WelcomeScreen");
            welcome welcomescreen = new welcome(this.smartpay);

//            this.Hide();

            Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "WelcomeScreen to front");
            welcomescreen.BringToFront();

            Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Calling Show dialog");
            DialogResult start = welcomescreen.ShowDialog();

            Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Welcome scren returned");
            if (start == DialogResult.OK)
            {
                ResetBackground();
                timer2.Start();
            }
#if (true)
            try
            {
                if (Globals.LockData == null)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData == null");
                    Globals.LockData = new LockerDatabase();
                }

                if (Globals.LockData.Lockers == null)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData.Lockers == null");
                    Globals.LockData.Lockers = new DataTable();
                }

#if (false)
                if (ssel == null)
                {
                    ssel = new SelectSize();
                    TimSet = new TimeSelection();
                }
#endif
                Globals.LockData.testDatabaseConnection();

                Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

                if ((Globals.LockData.Lockers.Rows.Count == 0) || (Properties.Settings.Default.RentalHold == true))
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData.Lockers.Rows.Count == 0  -   Renting Disabled");
                    try
                    {
                        // TODO : Update this on multiple languages
                        //                        labRent.Text = "No Lockers";
                        if(Properties.Settings.Default.RentalHold == true)
                        {
                            labRent.BackColor = Color.MistyRose;
                            labLockerA.BackColor = Color.MistyRose;
                            bRent.BackColor = Color.MistyRose;
                        }
                        else
                        {
                            labRent.BackColor = Color.White;
                            labLockerA.BackColor = Color.White;
                            bRent.BackColor = Color.White;
                        }

                        labRent.Text = "Sold Out";
                        labRent.Font = new Font("Microsoft Sans Serif", 30);
                        labLockerA.Text = "Try another terminal";
                        labLockerA.Font = new Font("Microsoft Sans Serif", 22);
                        bRent.Enabled = false;
                        labTapToRent.Visible = false;

//TODO : Fix for multiple languages
                        if (Properties.Settings.Default.ReleaseType == "Linux")
                            bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Globals.CurrentLanguage + "\\Icons\\NoLocker_482x427.tiff");
                        else
                            bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Globals.CurrentLanguage + "\\Icons\\NoLocker_482x427.tiff");
/*
                        if (Globals.CurrentLanguage == null)
                            bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\Icons\\NoLocker_482x427.tiff");
                        else
                            bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\Icons\\NoLocker_482x427.tiff");
*/
                    }
                    catch(Exception Ex)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "bRent.BackgroundImage not found, Exception - " + Ex.ToString());
                    }
                }
                else
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData.Lockers.Rows.Count = " + Globals.LockData.Lockers.Rows.Count.ToString() + "   - Renting Enabled");
                    labRent.BackColor = Color.White;
                    labLockerA.BackColor = Color.White;
                    bRent.BackColor = Color.White;

                    labRent.Text = "Rent";
                    labRent.Font = new Font("Microsoft Sans Serif", 64); ;
                    labLockerA.Text = "Locker";
                    labLockerA.Font = new Font("Microsoft Sans Serif", 48);
                    bRent.Enabled = true;
                    labTapToRent.Visible = true;
/*
                    try
                    {
                        if (Globals.CurrentLanguage == null)
                            bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\Icons\\RentLocker_482x427.tiff");
                        else
                            bRent.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\Icons\\RentLocker_482x427.tiff");
                    }
                    catch(Exception Ex)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "bRent.BackgroundImage not found, Exception - " + Ex.ToString());
                    }
*/
                }
            }
            catch (Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "MainScreen_Shown()", "Exception - " + Ex.ToString());
                Console.WriteLine("Exception = " + Ex.ToString());
            }
#endif
        }

        private void Start_LockControl()
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "Starting the lock controller");

            // Get all lock client links and open the sockets
            if (Globals.LockerClient == null)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "Globals.LockerClient == null");
                Globals.LockerClient = new LockerInterface(IPAddress.Parse(Properties.Settings.Default.ControllerIP), Properties.Settings.Default.ControllerPort, false);
            }

            LockerClient = Globals.LockerClient;
            if (LockerClient != null)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "lock client != null");
                LockerClient.DataReceived -= new LockerInterface.delDataReceived(Globals.Client_DataReceived);
                LockerClient.ConnectionStatusChanged -= new LockerInterface.delConnectionStatusChanged(Globals.Client_ConnectionStatusChanged);

                Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "lock client dispose");
                LockerClient.Dispose();
            }

            Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "Create new locker client and Set IP = " + Properties.Settings.Default.ControllerIP.ToString() + " Port = " + Properties.Settings.Default.ControllerPort.ToString());
            Globals.LockerClient = new LockerInterface(IPAddress.Parse(Properties.Settings.Default.ControllerIP), Properties.Settings.Default.ControllerPort, false);
            LockerClient = Globals.LockerClient;

            //Initialize the events
            Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "Attach events to locker client ");
            LockerClient.DataReceived += new LockerInterface.delDataReceived(Globals.Client_DataReceived);
            LockerClient.ConnectionStatusChanged += new LockerInterface.delConnectionStatusChanged(Globals.Client_ConnectionStatusChanged);

            Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "calling client connect()");
            this.LockerClient.Connect();
            Globals.LogFile(Globals.LogSource.Mainscreen, "Start_LockControl()", "connect() finished");
        }


#if (false)
        void client_ConnectionStatusChanged(LockerInterface sender, LockerInterface.ConnectionStatus status)
        {
            //Check if this event was fired on a different thread, if it is then we must invoke it on the UI thread
            if (InvokeRequired)
            {
                Invoke(new LockerInterface.delConnectionStatusChanged(client_ConnectionStatusChanged), sender, status);
                return;
            }
            //richTextBox1.Text += "Connection: " + status.ToString() + Environment.NewLine;
            Globals.LogFile("client_ConnectionStatusChanged() - connect status = " + status.ToString() + "\n");
            if (status.ToString() == LockerInterface.ConnectionStatus.Connected.ToString())
            {
                client.SourceKey = Properties.Settings.Default.sourceKey;
                Globals.client.GetSession();
            }

        }
        //Fired when new data is received in the TCP client
        void client_DataReceived(LockerInterface sender, object data)
        {
            //Again, check if this needs to be invoked in the UI thread
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new LockerInterface.delDataReceived(client_DataReceived), sender, data);
                }
                catch
                { }
                return;
            }
            //Interpret the received data object as a string
            string strData = data as string;
            Globals.LogFile("client_DataReceived() - data = " + strData + "\n");
            string values = strData.Split('-').Select(sValue => sValue.Trim()).First();
            /*
                        switch(Globals.client.LockerCommand)
                        {
                            case Locker_cmd.Basic.GetSession:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.AssignLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.UnassignLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.AvailableLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.UsedLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.OpenLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.CloseLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.LockStatus:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            default:
                                break;
                        }
            */
        }
#endif

        private void Run()
        {
            //            this.runStep = 0;

            if (this.runStep == 0)
                this.runStep = 5;
            //            this.NextRunStep(this, EventArgs.Empty);   // trigger first build step

            SimpleStateMachine(this, EventArgs.Empty);   // trigger first build step
        }

        public void SimpleStateMachine(object sender, EventArgs e)
        {
            /*
                        //            timer2.Start();
                        if (Globals.AbortForm == true)
                        {
                            //                this.blanckscreen.Hide();
                            this.runStep = 7;
                            this.Show();
                            ResetBackground();


                        }
                        else
                        {

                        }
            */

            this.Hide();

            if (Globals.UserProcessType == ProcessType.rent)
            {
                //                this.client.GetAvailableLocks(int sessionkey, int CRC16);

//                Globals.LockData = new LockerDatabase();

                Globals.LockData.LockNumber_Set(0);
                Globals.CurrentLanguage = "English";


                // TOD : Check if only one locker. If so then dont display the selection screen
                int LockerTypes = GetNumLockerTypes();

                // Select the locker size
                switch (LockerTypes)
                {
                    case 0:
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "LockerTypes == 0");
                            // TODO : Have error page for no lockers left to hire.
                        }
                        break;

                    case 1:
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "LockerTypes == 1");

                        if (Globals.LockData.Lockers == null)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Globals.LockData.Lockers == null");
                            Globals.LockData.Lockers = new DataTable();
                        }

                        Globals.LockData.LockerTypes = Globals.LockData.GetLockerTypes();
                        Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

                        // TODO : Make this more generic so that any size can be single type of any size
//                        switch (Globals.LockData.LockerTypes.Rows[0].ItemArray[1])
                        switch (Convert.ToInt16(Globals.LockData.LockerTypes.Rows[0].ItemArray[1]))
                        {
                            case 0:
                                Globals.LockData.Locker_Size = eLockerSize.SMALL;
                                break;
                            case 1:
                                Globals.LockData.Locker_Size = eLockerSize.MEDIUM;
                                break;
                            case 2:
                                Globals.LockData.Locker_Size = eLockerSize.LARGE;
                                break;
                            case 3:
                                Globals.LockData.Locker_Size = eLockerSize.XLARGE;
                                break;
                        }
                            
                        this.Get_LockAvailable();
                        Globals.LockData.Lock_SetReserved();
                        break;

                    default:
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "LockerTypes > 1 - Select a type to rent");
                            SelectSize ssel = new SelectSize();

                            if (ssel.ShowDialog() == DialogResult.Cancel)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "SelectSize returned DialogResult.Cancel");
                                this.Show();
                                Globals.AbortForm = false;
                                Globals.LockData.Lock_SetFree();

                                timer2.Start();
                                return;
                            }
                        }
                        break;
                }


                // Select the time for the locker
                // TODO : Check if rental type is daily 
                if (Properties.Settings.Default.VariableRentalTime == true)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "VariableRentalTime == true");
                    if (Properties.Settings.Default.HireTypePeriod == "Hour")
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "HireTypePeriod == Hour");
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "call TimeSelection");

                        // Enable hourly hire screen
                        TimeSelection TimSet = new TimeSelection();
                        if (TimSet.ShowDialog() == DialogResult.Cancel)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "TimeSelection returned DialogResult.Cancel");

                            this.Show();
                            Globals.AbortForm = false;
                            timer2.Start();
                            return;
                        }
                    }
                    else
                    {
                        if (Properties.Settings.Default.HireTypePeriod == "Block")
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "HireTypePeriod == Block");
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "call TimeSelection");

                            // Enable hourly hire screen
                            TimeSelectionBlock TimSet = new TimeSelectionBlock();
                            if (TimSet.ShowDialog() == DialogResult.Cancel)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "TimeSelection returned DialogResult.Cancel");

                                this.Show();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }
                        }
                        else
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "HireTypePeriod != Hour");
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "call TimeDaySelection");

                            // Enable daily hire screen
                            TimeDaySelection TimSet = new TimeDaySelection();
                            if (TimSet.ShowDialog() == DialogResult.Cancel)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "TimeDaySelection returned DialogResult.Cancel");

                                this.Show();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }
                        }
                    }
                }
                else
                {
                    // TODO : Fix the rental price based on the fixed period.
                    Globals.DueAmnt = Globals.LockData.GetLockerPrice(Globals.LockData.Locker_Size);
                }

                // Select the pin code
                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Call PINReg");

                Globals.LoginSelected = LoginTypes.PIN;
                PINReg PINReg = new PINReg(1);
                if (PINReg.ShowDialog() == DialogResult.Cancel)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PINReg returned DialogResult.Cancel");

                    this.Show();
                    Globals.AbortForm = false;
                    timer2.Start();
                    Globals.LockData.Lock_SetFree();
                    return;
                }

                // Select the security code
                if (Properties.Settings.Default.SecurityFeatureAnimals)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Properties.Settings.Default.SecurityFeatureAnimals -- call PINImage");

                    PINImage PINImage = new PINImage();
                    if (PINImage.ShowDialog() == DialogResult.Cancel)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PINImage returned DialogResult.Cancel");

                        this.Show();
                        Globals.AbortForm = false;
                        Globals.LockData.Lock_SetFree();
                        timer2.Start();
                        return;
                    }
                }
                else
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Properties.Settings.Default.SecurityFeatureAnimals -- call PINImage");

                // TODO : Fix this for fixed rentals
/*
                    Globals.DueAmnt = (double)Globals.LockData.Get_Locker_UPrice(Globals.LockData.Locker_Size);
                    Globals.LockData.COST = Globals.LockData.Get_Locker_UPrice(Globals.LockData.Locker_Size);
*/
                    Globals.LockData.PINIcon = "1";
                }
                // Select the payment type
                if (Properties.Settings.Default.PaymentEnableCredit && Properties.Settings.Default.PaymentEnableCash)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Properties.Settings.Default.SecurityFeatureAnimals -- call PINImage");

                    PaymentSelection Psel = new PaymentSelection();
                    if (Psel.ShowDialog() == DialogResult.Cancel)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PaymentSelection returned DialogResult.Cancel");
                        this.Show();
                        Globals.AbortForm = false;
                        Globals.LockData.Lock_SetFree();
                        timer2.Start();
                        return;
                    }
                }
                else
                {
                    if (Properties.Settings.Default.PaymentEnableCash)
                        Globals.PayTypes = PaymentTypes.Cash;

                    if (Properties.Settings.Default.PaymentEnableCredit)
                        Globals.PayTypes = PaymentTypes.CreditCard;
                }
                // Make the payment
                if (Properties.Settings.Default.SkipPhysicalPayment == false)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Payment Required");
                    switch (Globals.PayTypes)
                    {
                        case PaymentTypes.Cash:
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Startting Cash System");

                            Globals.AbortForm = false;
                            CashPayment cashpay = new CashPayment();
//                            DialogResult CashRes = cashpay.ShowDialog();
                            if (Globals.AbortForm == true)
//                            if(CashRes == DialogResult.Cancel)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "CashPayment returned DialogResult.Cancel");
                                this.Show();
                                Globals.AbortForm = false;
                                Globals.LockData.Lock_SetFree();
                                timer2.Start();
                                return;
                            }

                            Globals.LockData.Lock_SetRented();
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "CashPayment returned DialogResult.LOK");
                            break;

                        case PaymentTypes.CreditCard:
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Starting Credit Payment");

                            CreditPayment Cred = new CreditPayment();
                            DialogResult CredRes = Cred.ShowDialog();
                            if (CredRes == DialogResult.No)  // declined by advam
                            {
                                Forms.CreditFailure Cfail = new Forms.CreditFailure();
                                Cfail.ShowDialog();

                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "CreditPayment returned DialogResult.No");
                                this.Show();
                                Globals.LockData.Lock_SetFree();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }
                            if (CredRes == DialogResult.Cancel) // transaction cancelled
                            {
                                Forms.Credit_Cancel Ccancel = new Forms.Credit_Cancel();
                                Ccancel.ShowDialog();

                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "CreditPayment returned DialogResult.Cancel");
                                this.Show();
                                Globals.LockData.Lock_SetFree();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }
                            if (CredRes == DialogResult.Abort) // timeout 
                            {
                                Forms.Credit_NoResponse CNoResp = new Forms.Credit_NoResponse();
                                CNoResp.ShowDialog();

                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "CreditPayment returned DialogResult.Abort");
                                this.Show();
                                Globals.LockData.Lock_SetFree();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }
                            if (CredRes != DialogResult.Yes)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "CreditPayment didnt return DialogResult.Yes");
                                this.Show();
                                Globals.AbortForm = false;
                                Globals.LockData.Lock_SetFree();
                                timer2.Start();
                                return;
                            }

                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "CreditPayment returned DialogResult.OK");
                            break;
                    }
                }

                // Immediatelyy after payment update retal information
                Save_Rent();

                for (int lcount = 0; lcount < Properties.Settings.Default.LockerCommsRetries; lcount++)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Opening Locker  : Number - " + Globals.LockData.LockNumber_Get().ToString());

                    // TODO : FIX THIS CODE
//                    Globals.LockData.LockNumber_Set(Globals.LockData.LockNumber);
                    Globals.Open_Locker(Globals.LockData.LockNumber);

                    Thread.Sleep(Properties.Settings.Default.LockerCommsRetryPeriod);
                }

                // Display the completed payment
                Paydone paydone = new Paydone();
                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Payment Complete");
                paydone.ShowDialog();

                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Saving New rental");

// TODO : Add an option to have a thankyou screen or not.
//                DoneForm donescreen = new DoneForm();
//                DialogResult donescreen_result = donescreen.ShowDialog();

            }
            else
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Opening Locker");

                // Select the pin code
                Globals.LoginSelected = LoginTypes.PIN;
                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Get Pin");

                PINReg PINReg = new PINReg(0);
                if (PINReg.ShowDialog() == DialogResult.Cancel)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PINReg returned DialogResult.Cancel");

                    this.Show();
                    Globals.AbortForm = false;
                    timer2.Start();
                    return;
                }

//                LockerDatabase LockDB = new LockerDatabase();
                int LockerNumber = 0;
                bool access_granted = false;

                // TODO : Check is PIN is unique then dont do security check.
                if (Globals.LockData.IsPINActive(Globals.LockData.PINCode_Get()))
                {
                    if (!Globals.LockData.IsPINUnique(Globals.LockData.PINCode_Get()))
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PIN not unique");

                        // Select the security code
                        if (Properties.Settings.Default.SecurityFeatureAnimals)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Security Animal Selected");
                            PINImage PINImage = new PINImage();
                            if (PINImage.ShowDialog() == DialogResult.Cancel)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Incorrect Code Entered");
                                Forms.IncorrectCode inc = new Forms.IncorrectCode();
                                inc.ShowDialog();

                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "PINImage returned DialogResult.Cancel");

                                this.Show();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }
                            else
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Access Granted");
                                access_granted = true;
                            }
                        }
                        else if (Properties.Settings.Default.SecurityFeatureLockerNumber)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Security Locker Number Selected");
                            SecurityLockerNo SecLockNo = new SecurityLockerNo();
                            if (SecLockNo.ShowDialog() == DialogResult.Cancel)
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Incorrect Code Entered");
                                Forms.IncorrectCode inc = new Forms.IncorrectCode();
                                inc.ShowDialog();

                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "SecurityLockerNo returned DialogResult.Cancel");

                                this.Show();
                                Globals.AbortForm = false;
                                timer2.Start();
                                return;
                            }

                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Opening Locker Check");
                            
//                                if (LockDB.Check_Access(Globals.LockData.PINCode_Get(), Globals.LockData.LockNumber_Get()) == true)
                            if (Globals.LockData.Check_Access(Globals.LockData.PINCode_Get(), Globals.LockData.LockNumber_Get()) == true)
                            {
                                Globals.LockData.LockNumber_Set(Globals.LockData.LockNumber_Get());
                                Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Access Granted");
                                access_granted = true;
                            }
                        }
                    }
                    else
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Unique Access Code Detected");

                        string LockerNumbers = Globals.LockData.GetLockersForPinCode(Globals.LockData.PINCode_Get());
                        string[] LNUM = LockerNumbers.Split(',');
                        LockerNumber = Convert.ToInt32(LNUM[0]);

                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Opening Locker Check");
                        Globals.LockData.LockNumber_Set(LockerNumber);

                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Access Granted");
                        access_granted = true;
                    }

                    Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Show open locker dialog");
                    string[] LockoutTime = Properties.Settings.Default.EndRentalLockoutStartTime.Split(':');
                    System.DateTime DT = System.DateTime.Now;

                    if ((DT.Hour >= Convert.ToInt32(LockoutTime[0])) && (DT.Minute >= Convert.ToInt32(LockoutTime[1])))
                    {
                        // After the rental time then just open the door
                        Globals.UserProcessType = ProcessType.open;
                    }
                    else
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Show open locker dialog");
                        OpenLocker openLocker = new OpenLocker();

                        DialogResult OLresult = openLocker.ShowDialog();
                        if (OLresult == DialogResult.OK)
                        {
                            Globals.UserProcessType = ProcessType.open;
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine(OpenLocker)", "Returned OK = Open");
                        }

                        if (OLresult == DialogResult.Yes)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine(OpenLocker)", "Returned Yes = End Rent");
                            Globals.UserProcessType = ProcessType.endrent;
                        }

                        if (OLresult == DialogResult.Cancel)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachineOpenLocker()", "Returned Cancel = Rent");
                            access_granted = false;
                            Globals.UserProcessType = ProcessType.rent;
                        }
                    }

                    if (access_granted)
                    {
                        for (int lcount = 0; lcount < Properties.Settings.Default.LockerCommsRetries; lcount++)
                        {
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Opening Locker");
// TODO : FIX THIS CODE
                            Globals.Open_Locker(Globals.LockData.LockNumber);
                            Thread.Sleep(Properties.Settings.Default.LockerCommsRetryPeriod);
                        }
                    }

                    switch (Globals.UserProcessType)
                    {
                        case ProcessType.open:
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Continue rental");
                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Update the rental info");
                        this.UPdate_Rent();

                        Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Show continue rent final screen");
                        Unlocked UnlockScreen = new Unlocked();
                        UnlockScreen.ShowDialog();
                            break;

                        case ProcessType.endrent:
                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "End rental");

                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Update the rental");
                            this.Done_rent();

                            Globals.LogFile(Globals.LogSource.Mainscreen, "SimpleStateMachine()", "Show end rent final screen");
                            RentalEnded RentalEnded = new RentalEnded();
                            RentalEnded.ShowDialog();
                            break;

                        default:
                            break;
                    }
                }
                else
                {
                    // PIN is inactive
                    Forms.GeneralFailure GenFail = new Forms.GeneralFailure();
                    GenFail.ShowDialog();
                }

// TODO : Add an option to have a thankyou screen or not.
//                DoneForm donescreen = new DoneForm();
//                DialogResult donescreen_result = donescreen.ShowDialog();
            }

            this.timer2_Tick(this, EventArgs.Empty);
        }

        private int GetNumLockerTypes()
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "GetNumLockerTypes()", "GetNumLockerTypes Entry");

            if (Globals.LockData == null)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "GetNumLockerTypes()", "Globals.LockData Null");
                Globals.LockData = new LockerDatabase();
            }

            if (Globals.LockData.LockerTypes == null)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "GetNumLockerTypes()", "GetNumLockerTypes Null");
                Globals.LockData.LockerTypes = new DataTable();
            }

            Globals.LogFile(Globals.LogSource.Mainscreen, "GetNumLockerTypes()", "GetNumLockerTypes GetLockerTypes()");
            Globals.LockData.LockerTypes = Globals.LockData.GetLockerTypes();

            int lockercount = 0;
            try
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "GetNumLockerTypes()", "GetNumLockerTypes Trying DB Call");
                var Sizes = (from Rows in Globals.LockData.LockerTypes.AsEnumerable()
                             select Rows["enabled"]).Distinct().ToList();

                lockercount = Globals.LockData.LockerTypes.Rows.Count;
                Globals.LogFile(Globals.LogSource.Mainscreen, "GetNumLockerTypes()", "Lockercount = " + lockercount.ToString());
            }
            catch (Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "GetNumLockerTypes()", "Exception - " + Ex.ToString());
            }
            return (lockercount);
        }

        private Decimal GetLockerPrice(int LockerType)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "GetLockerPrice()", "LockerType - " + LockerType.ToString());

            if (Globals.LockData.LockerTypes == null)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "GetLockerPrice()", "Globals.LockData.LockerTypes == null");
                Globals.LockData.LockerTypes = new DataTable();
            }

            Globals.LockData.LockerTypes = Globals.LockData.GetLockerTypes();

            var Type = (from Rows in Globals.LockData.LockerTypes.AsEnumerable()
                        select Rows["sizecode"]).Distinct().ToList();

            var Price = (from Rows in Globals.LockData.LockerTypes.AsEnumerable()
                         select Rows["uprice"]).Distinct().ToList();

            Decimal cost = 0;
            /*
                        decimal cost = 0.00;
                        for(int count = 0; count < Type.Count; count++)
                        {
                            if(LockerType == Convert.ToInt16(Type[count]))
                            {
                                cost = Convert.ToDecimal(Price[count]);
                            }
                        }
            */
            return (cost);
        }
/*
        public void NextRunStep(object sender, EventArgs e)
        {
            //            timer2.Start();
            if (Globals.AbortForm == true)
            {
                //                this.blanckscreen.Hide();
                this.runStep = 7;
                this.Show();
                ResetBackground();


            }
            else
            {

            }
            switch (this.runStep)
            {
                case 0:

                    //                    this.blanckscreen.Show();
                    this.Hide();

                    if (Globals.Process == ProcessType.rent)
                    {
                        //                        this.SizeSelection();
                        SelectSize ssel = new SelectSize();
                        DialogResult ssel_result = ssel.ShowDialog();
                        //                        if (ssel_result == DialogResult.OK)
                        this.runStep++;
                    }
                    else
                    {
                        this.OpenLockers();
                        this.runStep++;

                    }
                    break;
                case 1:
                    //                    this.TimeSettings();
                    TimeSelection TimSet = new TimeSelection();
                    DialogResult TimSet_result = TimSet.ShowDialog();
                    this.runStep++;
                    break;
                //case 2:
                //    this.LoginSelection();
                //    break;
                case 2:
                    //goes here if rent, open and end lock security register and login
                    Globals.LoginSelected = LoginTypes.PIN;
                    this.LoginType();
                    break;
                case 3:
                    // SMC : removed payment selection for the moment. Cash only at this time. 
                    //                        this.PaymentSelect();
                    this.runStep++;
                    NextRunStep(this, EventArgs.Empty);
                    break;
                case 4:
                    //make payment
                    //if (this.mode == 0)
                    //{

                    // SMC  : Moved this into the payment screen for th emoment.
                    //                    this.smartpay.RunPaysystem();
                    this.Payment();
                    //}
                    //else
                    //{


                    //}
                    break;

                case 5:
                    if (this.mode == 0)
                    {
                        this.PayDone();
                    }
                    else
                    {
                        if (Globals.Process == ProcessType.open)
                        {
                            this.UnLockScreen();
                        }
                        else
                        {
                            this.RentEndScreen();
                        }
                        //this.runStep = 5;
                    }
                    break;
                case 6:
                    this.DoneScreens();
                    break;
                case 7:
                    this.Show();
                    this.Refresh();
                    this.Invalidate();
                    Globals.AbortForm = false;
                    this.Update();

                    // SMC : Removed smart payout for the moment. 
                    //                    this.smartpay.Payout = 0;
                    //                    this.smartpay.VALIDATOR.PAID_AMOUNT = 0;
                    //                    this.smartpay.StopPaysystem();

                    timer2.Start();
                    //this.Show();
                    //ManualResetEvent syncEvent = new ManualResetEvent(false);
                    //Thread t1 = new Thread(
                    //    () =>
                    //    {
                    //        // Do some work...
                    //        this.smartpay.VALIDATOR.DisableValidator();
                    //        syncEvent.Set();
                    //    }
                    //);
                    //t1.Start();

                    //Thread t2 = new Thread(
                    //    () =>
                    //    {
                    //        syncEvent.WaitOne();
                    //        this.smartpay.HOPPER.DisableValidator();
                    //        // Do some work...
                    //    }
                    //);
                    //t2.Start();
                    break;

                default:
                    this.runStep = 6;
                    break;


            }
            this.runStep++;
            //            timer2.Stop();
        }
*/

        private void ResetBackground()
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "ResetBackground()", "Resettting the background look");

            try
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "ResetBackground()", "Language undefined - Resettting to English as default");
                if (Globals.CurrentLanguage == null)
                    Globals.SetLanguage("en");
            }
            catch(Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "ResetBackground()", "Exception - " + Ex.ToString());
            }

            InvokeLanguage();

            if (Globals.LockData == null)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData == null");
                Globals.LockData = new LockerDatabase();
                if (Globals.LockData.Lockers == null)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData.Lockers == null");
                    Globals.LockData.Lockers = new DataTable();
                }
            }
            else
            {
                Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

                if ((Globals.LockData.Lockers.Rows.Count == 0) || (Properties.Settings.Default.RentalHold == true))
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "ResetBackground()", "Globals.LockData.Lockers.Rows.Count == 0  -   Renting Disabled");
                    try
                    {
                        // TODO : Update this on multiple languages
                        //                        labRent.Text = "No Lockers";
                        if (Properties.Settings.Default.RentalHold == true)
                        {
                            labRent.BackColor = Color.MistyRose;
                            labLockerA.BackColor = Color.MistyRose;
                            bRent.BackColor = Color.MistyRose;
                        }
                        else
                        {
                            labRent.BackColor = Color.White;
                            labLockerA.BackColor = Color.White;
                            bRent.BackColor = Color.White;
                        }

                        labRent.Text = "Sold Out";
                        labRent.Font = new Font("Microsoft Sans Serif", 30); ;
                        labLockerA.Text = "Try another terminal";
                        labLockerA.Font = new Font("Microsoft Sans Serif", 22);
                        labTapToRent.Visible = false;
                        bRent.Enabled = false;
                    }
                    catch (Exception Ex)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "ResetBackground()", "Ex = " + Ex.ToString());
                    }
                }
                else
                {
                    labRent.BackColor = Color.White;
                    labLockerA.BackColor = Color.White;
                    bRent.BackColor = Color.White;

                    labRent.Text = "Rent";
                    labRent.Font = new Font("Microsoft Sans Serif", 64); ;
                    labLockerA.Text = "Locker";
                    labLockerA.Font = new Font("Microsoft Sans Serif", 48);
					labTapToRent.Visible = false;
                    labTapToRent.Visible = true;
                    bRent.Enabled = true;
                }

            }
            this.Show();
        }

        /*        private void DoneScreens()
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "DoneScreens()", "Entry");
                    this.donescreen = new DoneForm();
                    this.donescreen.FormClosed += this.NextRunStep;
                    this.donescreen.Show(this);
                }

                private void Donescreen_Load(object sender, EventArgs e)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Donescreen_Load()", "Entry");

                    Globals.LockerClient.OpenLock(Globals.LockData.LockNumber);

                    switch (Globals.Process)
                    {
                        case ProcessType.rent:
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "Donescreen_Load()", "Saving new rental - Locker = " + Globals.LockData.LockNumber.ToString());
                                this.Save_Rent();
                            }
                            break;
                        case ProcessType.open:
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "Donescreen_Load()", "Updating Existing rental - Locker = " + Globals.LockData.LockNumber.ToString());
                                this.UPdate_Rent();
                            }
                            break;
                        case ProcessType.endrent:
                            {
                                Globals.LogFile(Globals.LogSource.Mainscreen, "Donescreen_Load()", "Ending rental - Locker = " + Globals.LockData.LockNumber.ToString());
                                this.Done_rent();
                            }
                            break;
                    }

                }

                private void OpenLockers()
                {
                    this.openlocker = new OpenLocker();
                    this.openlocker.FormClosed += this.NextRunStep;
                    this.openlocker.Show();
                }

                private void PayDoneOriginal()
                {
                    this.paydonescreen = new Paydone();
                    this.paydonescreen.FormClosed += this.NextRunStep;
                    this.paydonescreen.Show();
                }

                private void PayDone()
                {
                    paydonescreen = new Paydone();
                    paydonescreen.FormClosed += this.NextRunStep;
                    paydonescreen.Show();
                }

                public void Payment()
                {
                    switch (Globals.PayTypes)
                    {
                        case PaymentTypes.Cash:
                            this.cashpayment = new CashPayment();
                            this.cashpayment.FormClosed += this.NextRunStep;
                            //                    this.cashpayment.Show();
                            this.runStep++;
                            //                    this.runStep++;
                            NextRunStep(this, null);
                            break;
                        case PaymentTypes.CreditCard:
                            break;
                    }

                }

                private void RentEndScreen()
                {
                    this.rentend = new RentalEnded();
                    this.rentend.FormClosed += this.NextRunStep;
                    this.rentend.Show(this);

                }

                private void UnLockScreen()
                {
                    this.unlocked = new Unlocked();
                    this.unlocked.FormClosed += this.NextRunStep;
                    this.unlocked.Show(this);
                }

                private void PaymentSelect()
                {

                    this.paymentscreen = new PaymentSelection();
                    this.paymentscreen.FormClosed += this.NextRunStep;
                    this.paymentscreen.Show(this);

                }

                public void NextPINReg(object sender, EventArgs e)
                {
                    if (this.PINStep <= 1 && !Globals.AbortForm)
                    {
                        if (this.mode == 0 && this.PINStep == 0)
                        {
                            this.pinreg = new PINReg(1);
                            this.pinreg.FormClosed += this.NextPINReg;
                            this.pinreg.Show();

                        }
                        else if (this.mode == 1 && this.PINStep == 0)
                        {
                            this.pinlogin = new PINLogin();
                            this.pinlogin.FormClosed += this.NextPINReg;
                            this.pinlogin.Show();
                        }
                        else
                        {
                            this.pinimage = new PINImage();
                            this.pinimage.FormClosed += this.NextPINReg;
                            this.pinimage.Show();
                        }
                        this.PINStep++;
                    }
                    else
                    {
                        if (!Globals.AbortForm)
                        {
                            switch (this.mode)
                            {
                                case 0:
                                    // SMC : Removed for the moment as two lockers can have the same access code. 
                                    // Think about the validation when codes are the same. 
                                    if (Globals.LockData.Used_Access(Globals.LockData.PINCode, Globals.LockData.PINIcon))
                                    {
                                        this.PINStep = 0;
                                        this.runStep = 2;
                                        MessageBox.Show("Access Code already used", "ACCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                    break;
                                case 1:
                                    if (!Globals.LockData.Check_Access(Globals.LockData.PINCode, Globals.LockData.PINIcon))
                                    {
                                        this.PINStep = 0;
                                        this.runStep = 2;
                                        MessageBox.Show("Invaled Access", "ACCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                    else
                                    {
                                        if (Globals.LockData.Remaining.Minutes < 0)
                                        {
                                            decimal m_price = Globals.LockData.UPrice;
                                            decimal totalhrs = Globals.LockData.Remaining.Hours + ((decimal)Globals.LockData.Remaining.Minutes / 60);
                                            decimal cost = (totalhrs * -1) * m_price;
                                            Globals.DueAmnt = (decimal)Math.Round(cost, 2);
                                            Globals.LockData.COST = (decimal)Math.Round(cost, 2);
                                            this.runStep = 3;
                                        }
                                        else
                                        {

                                            this.runStep = 5;
                                        }

                                    }
                                    break;

                            }
                        }


                        //else
                        //{
                        ResetBackground();
                        this.NextRunStep(this, EventArgs.Empty);
                        //}
                    }
                }

                private void LoginType()
                {
                    if (Globals.LoginSelected == LoginTypes.PIN)
                    {
                        this.NextPINReg(this, EventArgs.Empty);
                    }
                }
                private void LoginSelection()
                {
                    this.selectlogin = new SelectLogin();
                    this.selectlogin.FormClosed += this.NextRunStep;
                    this.selectlogin.Show();
                }


                private void TimeSettings()
                {

                    this.timeselection = new TimeSelection();
                    this.timeselection.FormClosed += this.NextRunStep;
                    this.timeselection.Show();

                }


                private void LockerSelectionScreen()
                {

                    this.lockerselection = new LockerSelection();
                    this.lockerselection.FormClosed += Lockerselection_Closed;
                    this.lockerselection.Show();

                }

                private void Lockerselection_Closed(object sender, EventArgs e)
                {
                    lockerselection.Dispose();
                    lockerselection = null;
                    this.NextRunStep(this, EventArgs.Empty);
                }


                private void SizeSelection()
                {

                    this.selectsize = new SelectSize();
                    this.selectsize.FormClosed += this.NextRunStep;
                    this.selectsize.Show();
                    //                this.NextRunStep(this, EventArgs.Empty);
                }
*/
                private void Mainscreen_KeyPress(object sender, KeyPressEventArgs e)
                {
                    if (e.KeyChar.ToString().ToUpper().Equals("Q"))
                    {
                        // Quit - ends kiosk program
                        e.Handled = true;
                        Application.Exit();
                    }
                }
        /*
                        private void Save_Rent()
                        {
                            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
                            Globals.LockData.Save_Rent();

                        }

                        private void Done_rent()
                        {
                            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
                            Globals.LockData.Locker_Done();

                        }

                        private void UPdate_Rent()
                        {
                            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
                            Globals.LockData.Update_Rent();
                        }

                        private void LockData_LockerDataError(object sender, LogMessageEventArgs e)
                        {
                            MessageBox.Show(e.Messsage, "Error Saving", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Globals.LockData.LockerDatabaseError -= LockData_LockerDataError;
                        }
        */
        private void bRent_Click(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "bRent_Click()", "\n\n\n\n Starting new rental \n\n\n\n");

            if (labRent.Text.Contains("Sold"))
                return;

            this.mode = 0;
            this.runStep = 0;
            this.PINStep = 0;
            Globals.UserProcessType = ProcessType.rent;
            //this.Globals.LockData = new LockerData();
            // Globals.LockData = new LockerDatabase();
            timer2.Stop();

            Globals.LogFile(Globals.LogSource.Mainscreen, "bRent_Click()", "Checking Locker=null");

            if (Globals.LockData == null)
            { 
                Globals.LogFile(Globals.LogSource.Mainscreen, "bRent_Click()", "Globals.LockData = null");
                Globals.LockData = new LockerDatabase();
            }

            Globals.LogFile(Globals.LogSource.Mainscreen, "bRent_Click()", "Calling Get Lockers()");
            Globals.LockData.Get_Lockers();
            //            this.NextRunStep(this, EventArgs.Empty);

            Globals.LogFile(Globals.LogSource.Mainscreen, "bRent_Click()", "Go into SimpleStateMachine()");
            SimpleStateMachine(this, EventArgs.Empty);

            //if (Globals.LockData.Lock_Available() == true)
            //{
            //    this.NextRunStep(this, EventArgs.Empty);
            //}
            //else
            //{
            //    MessageBox.Show("No Locks available", "Locker", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
        }
/*
                private void bClose_Click(object sender, EventArgs e)
                {
                    timer2.Stop();
                    string mCode = "";
                    NumericKeypad kpad = new NumericKeypad("CLOSE PROGRAM ACCESS KEY");
                    //kpad.Data = txtValue1.Text;
                    DialogResult dr = kpad.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        mCode = kpad.Data;
                    }

                    kpad.Dispose();
                    kpad = null;

                    if (mCode == Properties.Settings.Default.ExitCode.ToString())
                    {
                        //                this.blanckscreen.Close();
                        this.Close();

                    }
                    else
                    {
                        timer2.Start();
                    }
                }
*/
        private void bOpen_Click(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "bOpen_Click()", "\n\n\n\n Opening Locker \n\n\n\n");

            this.mode = 1;
            this.runStep = 0;
            this.PINStep = 0;
            Globals.UserProcessType = ProcessType.open;
//            Globals.LockData = new LockerDatabase();
            timer2.Stop();

            SimpleStateMachine(this, EventArgs.Empty);
            //            this.NextRunStep(this, EventArgs.Empty);
        }

        private void Save_Rent()
        {
            Globals.LockData.Save_Rent();
        }

        private void Done_rent()
        {
            Globals.LockData.Locker_Done();
        }

        private void UPdate_Rent()
        {
            Globals.LockData.Update_Rent();
        }

        static int DBCheckCount = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            //lblsec.Text = DateTime.Now.ToString("ss");
            //lblsec.Location = new Point(lbltime.Location.X  + lbltime.Width,lblsec.Location.Y);

            //            if(DBCheckCount == 3)
            //            {
            //                DBCheckCount = 0;

            if (Globals.LockData == null)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData == null");
                Globals.LockData = new LockerDatabase();
                if (Globals.LockData.Lockers == null)
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Globals.LockData.Lockers == null");
                    Globals.LockData.Lockers = new DataTable();
                }
            }
            else
            {
                Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

                if ((Globals.LockData.Lockers.Rows.Count == 0) || (Properties.Settings.Default.RentalHold == true))
                {
                    Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_timer1_Tick()", "Globals.LockData.Lockers.Rows.Count == 0  -   Renting Disabled");
                    try
                    {
                        // TODO : Update this on multiple languages
                        if (Properties.Settings.Default.RentalHold == true)
                        {
                            labRent.BackColor = Color.MistyRose;
                            labLockerA.BackColor = Color.MistyRose;
                            bRent.BackColor = Color.MistyRose;
                        }
                        else
                        {
                            labRent.BackColor = Color.White;
                            labLockerA.BackColor = Color.White;
                            bRent.BackColor = Color.White;
                        }

                        labRent.Text = "Sold Out";
                        labRent.Font = new Font("Microsoft Sans Serif", 30); ;
                        labLockerA.Text = "Try another terminal";
                        labLockerA.Font = new Font("Microsoft Sans Serif", 22);
                        bRent.Enabled = false;
                        labTapToRent.Visible = false;
                    }
                    catch (Exception ex)
                    {
                        Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_timer1_Tick()", "Exception = " + ex.ToString());
                    }
                }
                else
                {
                    labRent.BackColor = Color.White;
                    labLockerA.BackColor = Color.White;
                    bRent.BackColor = Color.White;

                    labRent.Text = "Rent";
                    labRent.Font = new Font("Microsoft Sans Serif", 64); ;
                    labLockerA.Text = "Locker";
                    labLockerA.Font = new Font("Microsoft Sans Serif", 48);
                    labTapToRent.Visible = true;
                    bRent.Enabled = true;
                }
                this.Update();
            }
        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            timer2.Stop();
            //SetupPaysystem setUpPaySystem = new SetupPaysystem(this.smartpay);
            //setUpPaySystem.BringToFront();
            //setUpPaySystem.ShowDialog();
            //setUpPaySystem = null;
            int mcode = 0;
            NumericKeypad kpad = new NumericKeypad("MAINTENANCE ACCESS KEY");
            //kpad.Data = txtValue1.Text;
            DialogResult dr = kpad.ShowDialog();
            if (dr == DialogResult.OK)
            {
                if (kpad.Data == "") return;
                mcode = int.Parse(kpad.Data);
            }

            kpad.Dispose();
            kpad = null;

            if (mcode == Properties.Settings.Default.Adminpwd)
            {
                AdminTask admin = new AdminTask(smartpay);
                admin.BringToFront();
                admin.ShowDialog();

                admin.Dispose();
            }


            if (mcode == Properties.Settings.Default.ExitCode)
            {
                try
                {
                    Globals.WatchdogProcess.Kill();
                }
                catch
                {
                }

                if (Properties.Settings.Default.AutoRestartApplicationInternally)
                {
                    if(File.Exists("C:\\AdvSof~1\\Running.txt"))
                        File.Delete("C:\\AdvSof~1\\Running.txt");
                }


//                Globals.ShowTaskbar();
                Globals.StartExplorer();

                Application.Exit();
            }

            timer2.Start();
        }

        private void Mainscreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            //try
            //{
            if (this.smartpay != null)
            {
                this.smartpay.StopPaysystem();
                this.smartpay = null;
            }

            Application.Exit();
            //}
            //catch { }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            Globals.LogFile(Globals.LogSource.Mainscreen, "timer2_Tick()", "Entry");
            try
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "timer2_Tick()", "Getting locker information");

                // LOCKERDATABASE Not working on linux yet */
#if (false)
                if (Globals.LockData == null)
                    Globals.LockData = new LockerDatabase();

                if (Globals.LockData.Lockers == null)
                    Globals.LockData.Lockers = new DataTable();

                Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

                if (Globals.LockData.Lockers.Rows.Count == 0)
                {
                    bRent.Enabled = false;
                    labRent.Text = "No lockers";
                }
                else
                {
                    bRent.Enabled = true;
                }
#endif
            }
            catch (Exception Ex)
            {
                Console.WriteLine("Exception = " + Ex.ToString());
            }

            Globals.LogFile(Globals.LogSource.Mainscreen, "timer2_Tick()", "Stopping Timer1 and 2");
            timer1.Stop();
            timer2.Stop();

            Globals.LogFile(Globals.LogSource.Mainscreen, "timer2_Tick()", "Starting new welcome screen");
            welcome welcomescreen = new welcome(this.smartpay);
            welcomescreen.BringToFront();

            Globals.LogFile(Globals.LogSource.Mainscreen, "timer2_Tick()", "Calling welcomescreen show");
            DialogResult start = welcomescreen.ShowDialog();

            if (start == DialogResult.OK)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "timer2_Tick()", "Welcome Screen - DialogResult OK");
                Globals.LogFile(Globals.LogSource.Mainscreen, "timer2_Tick()", "Invoking language selection");

                ResetBackground();
                timer1.Start();
                timer2.Start();
            }
        }
        private void Get_LockAvailable()
        {
            try
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Get_LockAvailable()", "Entry");
            int lockersize = (int)Globals.LockData.Locker_Size;
            var mlock = (from Rows in Globals.LockData.Lockers.AsEnumerable()
                         where Rows.Field<string>("lockersize") == lockersize.ToString()
                         select new
                         {
                             rentLock = Rows.Field<int>("lockerNum")

                         }).OrderBy(x => Guid.NewGuid()).Take(1);
            Globals.LockData.LockNumber_Set(mlock.First().rentLock);

                Globals.LogFile(Globals.LogSource.Mainscreen, "Get_LockAvailable()", "LockNumber Updated to " + Globals.LockData.LockNumber_GetString().ToString());

            }
            catch(Exception Ex)
            {
                Globals.LogFile(Globals.LogSource.Mainscreen, "Get_LockAvailable()", "Ex = " + Ex.ToString());
            }
        }

        private void LabRent_Click(object sender, EventArgs e)
        {
            bRent_Click(this, e);
        }

        private void LabLockerA_Click(object sender, EventArgs e)
        {
            bRent_Click(this, e);
        }

        private void LabOpen_Click(object sender, EventArgs e)
        {
            bOpen_Click(this, e);
        }

        private void LabLockerB_Click(object sender, EventArgs e)
        {
            bOpen_Click(this, e);
        }

        private void Timer3_Tick(object sender, EventArgs e)
        {
            try
            {
                SignalRSend("");
            }
            catch(Exception Ex)
            {
                Console.WriteLine("SignalRSend Timer " + Ex.ToString());
            }
        }
    }
}
