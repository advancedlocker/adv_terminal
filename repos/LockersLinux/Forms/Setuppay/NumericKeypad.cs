﻿using System;
using System.Windows.Forms;

namespace Lockers
{
    public partial class NumericKeypad : Form
    {
        private string vData = "";
        private bool vAllowNegatives = false;


        public NumericKeypad()
        {
            InitializeComponent();
        }
        public NumericKeypad(string label)
        {
            InitializeComponent();
            label1.Text = label;
        }

        #region property
/*
        public string Data
        {
            get { return vData; }
            set {
                vData = value;
                if (value == "")
                {
                    buDisplay.Text = "0";
                }
                else {
                    buDisplay.Text = value;
                }
            }
        }
*/
        public string Data
        {
            get { return vData; }
            set
            {
                vData = value;
                if (value == "")
                {
                    buDisplay.Text = "";
                }
                else
                {
                    buDisplay.Text += "*";
                }
            }
        }

        public bool AllowNegatives
        { 
             get {return vAllowNegatives;}
        
            set {
                vAllowNegatives = value;
                buNegative.Visible = value;
                if( Data.Substring(0, 1) == "-" && value == false)
                {
                    Data = Data.Substring(1, Data.Length - 1);
                }
            }
        }
#endregion

        public void NumericMouseUp(object sender, MouseEventArgs e)
        {
            Button b = (Button)sender;
            if (b.Name=="Button0" && vData=="0")
            {
                return;
            }
            Data += b.Text;
        }

        private void buCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buDecimal_MouseUp(object sender, MouseEventArgs e)
        {
             if (!Data.Contains(".")){
                 Data += ".";
             }
        }

        private void buNegative_MouseUp(object sender, MouseEventArgs e)
        {
            if (!Data.Contains("-"))
            {
                Data = "-" + Data;
            }
            else { Data = Data.Substring(1, Data.Length - 1); }
        }

        private void buClear_MouseUp(object sender, MouseEventArgs e)
        {
            Data = "";
        }

        private void buBksp_MouseUp(object sender, MouseEventArgs e)
        {
            if (Data.Length == 0)
            {
                Data = "0";
            }
            else
            {
                Data = Data.Substring(0, Data.Length - 1);
            }
        }

        private void buAccept_MouseUp(object sender, MouseEventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void NumericKeypad_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
        }
    }
}
