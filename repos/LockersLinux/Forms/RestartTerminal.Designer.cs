﻿namespace Lockers
{
    partial class RestartTerminal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RestartTerminal));
            this.bTermOff = new System.Windows.Forms.Button();
            this.bTermRestart = new System.Windows.Forms.Button();
            this.bMaintLock = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.bHoldRentals = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bTermOff
            // 
            this.bTermOff.Location = new System.Drawing.Point(358, 174);
            this.bTermOff.Name = "bTermOff";
            this.bTermOff.Size = new System.Drawing.Size(125, 93);
            this.bTermOff.TabIndex = 0;
            this.bTermOff.Text = "Power Down Terminal";
            this.bTermOff.UseVisualStyleBackColor = true;
            this.bTermOff.Click += new System.EventHandler(this.BTermOff_Click);
            // 
            // bTermRestart
            // 
            this.bTermRestart.Location = new System.Drawing.Point(512, 174);
            this.bTermRestart.Name = "bTermRestart";
            this.bTermRestart.Size = new System.Drawing.Size(125, 93);
            this.bTermRestart.TabIndex = 1;
            this.bTermRestart.Text = "Restart Terminal";
            this.bTermRestart.UseVisualStyleBackColor = true;
            this.bTermRestart.Click += new System.EventHandler(this.BTermRestart_Click);
            // 
            // bMaintLock
            // 
            this.bMaintLock.Location = new System.Drawing.Point(358, 282);
            this.bMaintLock.Name = "bMaintLock";
            this.bMaintLock.Size = new System.Drawing.Size(125, 93);
            this.bMaintLock.TabIndex = 2;
            this.bMaintLock.Text = "Lock Terminal for Maintenance";
            this.bMaintLock.UseVisualStyleBackColor = true;
            this.bMaintLock.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Transparent;
            this.button12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button12.BackgroundImage")));
            this.button12.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.button12.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Location = new System.Drawing.Point(31, 626);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(79, 111);
            this.button12.TabIndex = 3;
            this.button12.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(270, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(431, 46);
            this.label1.TabIndex = 4;
            this.label1.Text = "Restart Terminal Task";
            // 
            // bHoldRentals
            // 
            this.bHoldRentals.Location = new System.Drawing.Point(512, 282);
            this.bHoldRentals.Name = "bHoldRentals";
            this.bHoldRentals.Size = new System.Drawing.Size(125, 93);
            this.bHoldRentals.TabIndex = 5;
            this.bHoldRentals.Text = "Hold New Rentals";
            this.bHoldRentals.UseVisualStyleBackColor = true;
            this.bHoldRentals.Click += new System.EventHandler(this.BHoldRentals_Click);
            // 
            // RestartTerminal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(195)))), ((int)(((byte)(65)))));
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.bHoldRentals);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.bMaintLock);
            this.Controls.Add(this.bTermRestart);
            this.Controls.Add(this.bTermOff);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "RestartTerminal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "RestartTerminal";
            this.Load += new System.EventHandler(this.RestartTerminal_Load);
            this.Shown += new System.EventHandler(this.RestartTerminal_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bTermOff;
        private System.Windows.Forms.Button bTermRestart;
        private System.Windows.Forms.Button bMaintLock;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bHoldRentals;
    }
}