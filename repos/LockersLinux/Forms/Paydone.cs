﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;

namespace Lockers
{
    public partial class Paydone : Form
    {
        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        public Paydone()
        {
            InitializeComponent();

//            String ImageFilename = Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Globals.CurrentLanguage + "\\PaymentComplete.tiff";
//            this.BackgroundImage = Image.FromFile(ImageFilename);

            InvokeLanguage();

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);

// TODO : Hide this for single day rentals
            lbltime.Visible = false;

            lbltime.ForeColor = Properties.Settings.Default.TextColor;
            labOK.ForeColor = Properties.Settings.Default.TextColor;

            timer1.Start();
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
            timIdle.Start();

        }

        private void button10_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void Paydone_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            lblLocknum.ForeColor = Properties.Settings.Default.TextColor;
            lblremaintime.ForeColor = Properties.Settings.Default.TextColor;
            lbltime.ForeColor = Properties.Settings.Default.TextColor;
            label1.ForeColor = Properties.Settings.Default.TextColor;
            

            string LockNumber = Globals.LockData.LockNumber_GetString();
            lblLocknum.Text = LockNumber;

            int LockerPosition = 0;
            LockerPosition = Globals.LockData.GetLockerLocation(LockNumber);

            switch (LockerPosition)
            {
                case 1:
                    picBoxArrow.Visible = true;

                    if(Properties.Settings.Default.ReleaseType == "Linux")
                        picBoxArrow.Image = System.Drawing.Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + "Common/Icons/ArrowLeft.png");
                    else
                        picBoxArrow.Image = System.Drawing.Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + "Common\\Icons\\ArrowLeft.png");
                    break;

                case 2:
                    picBoxArrow.Visible = true;
                    if (Properties.Settings.Default.ReleaseType == "Linux")
                        picBoxArrow.Image = System.Drawing.Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + "Common/Icons/ArrowRight.png");
                    else
                        picBoxArrow.Image = System.Drawing.Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + "Common\\Icons\\ArrowLeft.png");
                    break;

                default:
                    picBoxArrow.Visible = false;
                    break;
            }

            this.Update();

            //            lblremaintime.Text = string.Format("{0:00}:{1:00}", Globals.LockData.Hours ,Globals.LockData.Minutes);
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void InvokeLanguage()
        {
            String language = Globals.GetLanguageCode();

            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            ComponentResourceManager resources = new ComponentResourceManager(typeof(Paydone));

            foreach (Control C in this.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    foreach (Control D in C.Controls)
                    {
                        resources.ApplyResources(D, D.Name, new CultureInfo(language));
                    }
                }
                else
                {
                    resources.ApplyResources(C, C.Name, new CultureInfo(language));
                }
            }

            this.Update();
        }

        private void Paydone_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
