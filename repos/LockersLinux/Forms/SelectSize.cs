﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
namespace Lockers
{
    public partial class SelectSize : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }
       

        DateTimeFormatInfo fi = new DateTimeFormatInfo();
        public SelectSize()
        {
            
            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";

            InitializeComponent();
            LoadBackgroundImage();
            //lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);

            Init_buttons();

            timer1.Start();
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
            timIdle.Start();

        }

        private void SelectSize_Load(object sender, EventArgs e)
        {

            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            InvokeLanguage();
            
            //            this.timer1_Tick(this, EventArgs.Empty);
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            //System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ThreadStart(LoadBackgroundImage));
            //thrd.Start();

            //this.Refresh();
            this.Invalidate();

            labTimeHead.ForeColor = Properties.Settings.Default.TextColor;
            lbltime.ForeColor = Properties.Settings.Default.TextColor;
            labBack.ForeColor = Properties.Settings.Default.TextColor;
            labSelectSize.ForeColor = Properties.Settings.Default.TextColor;

        }

        private void Init_buttons()
        {
            if (Globals.LockData.Lockers == null)
            {
                Globals.LogFile(Globals.LogSource.SelectSize, "Init_buttons()", "Globals.LockData.Lockers NULL");
                Globals.LockData.Lockers = new DataTable();
            }

            Globals.LogFile(Globals.LogSource.SelectSize, "Init_buttons()", "Get_Lockers()");
            Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

            DataTable lsizes = Globals.LockData.GetLockerSizes();

            var Sizes = (from Rows in Globals.LockData.Lockers.AsEnumerable()
                         select Rows["lockersize"]).Distinct().ToList();

            if (!Sizes.Exists(e => e.ToString() == "0"))
            {
                Globals.LogFile(Globals.LogSource.SelectSize, "Init_buttons()", "small disabled");
                bSmallSize.Enabled = false;
                bSmallSize.Visible = false;
                //                bSmallSize.BackgroundImage = AlterTransparent(bExtraLargewSize.BackgroundImage, 40);
            }
            else
            {
                Globals.LogFile(Globals.LogSource.SelectSize, "Init_buttons()", "small enabled");
                bSmallSize.Enabled = true;
                bSmallSize.Visible = true;
            }

            if (!Sizes.Exists(e => e.ToString() == "1"))
            {
                Globals.LogFile(Globals.LogSource.SelectSize, "Init_buttons()", "medium disabled");
                bMediumSize.Enabled = false;
                bMediumSize.Visible = false;
                //                bMediumSize.BackgroundImage = AlterTransparent(bMediumSize.BackgroundImage, 40);
            }
            else
            {
                Globals.LogFile(Globals.LogSource.SelectSize, "Init_buttons()", "medium enabled");
                bMediumSize.Enabled = true;
                bMediumSize.Visible = true;
            }

            if (!Sizes.Exists(e => e.ToString() == "2"))
            {
                Globals.LogFile(Globals.LogSource.SelectSize, "Init_buttons()", "Large disabled");
                bLargeSize.Enabled = false;
                bLargeSize.Visible = false;
                //                bLargeSize.BackgroundImage = AlterTransparent(bLargeSize.BackgroundImage, 40);
            }
            else
            {
                Globals.LogFile(Globals.LogSource.SelectSize, "Init_buttons()", "Large enabled");
                bLargeSize.Enabled = true;
                bLargeSize.Visible = true;
            }

            if (!Sizes.Exists(e => e.ToString() == "3"))
            {
                Globals.LogFile(Globals.LogSource.SelectSize, "Init_buttons()", "ExtraLarge disabled");
                bExtraLargewSize.Enabled = false;
                bExtraLargewSize.Visible = false;
                //                bExtraLargewSize.BackgroundImage = AlterTransparent(bExtraLargewSize.BackgroundImage, 40);
            }
            else
            {
                Globals.LogFile(Globals.LogSource.SelectSize, "Init_buttons()", "ExtraLarge Enabled");
                bExtraLargewSize.Enabled = true;
                bExtraLargewSize.Visible = true;
            }
        }

        private void Get_LockAvailable()
        {
            Globals.LogFile(Globals.LogSource.SelectSize, "Get_LockAvailable()", "Entry");

            int lockersize = (int)Globals.LockData.Locker_Size;
            string Lstring = Convert.ToString(lockersize);

            var mlock = (from Rows in Globals.LockData.Lockers.AsEnumerable()
                         where Rows.Field<string>("lockersize") == lockersize.ToString()
//                         where Rows.Field<int>("sizecode").ToString() == Lstring
                         select new
                            {
                               rentLock = Rows.Field<int>("lockerNum")

                            }).OrderBy(x => Guid.NewGuid()).Take(1);
            Globals.LockData.LockNumber_Set(mlock.First().rentLock);
        }


        public Bitmap AlterTransparent(Image image, byte alpha)
        {
            Bitmap original = new Bitmap(image);
            Bitmap transparentimage = new Bitmap(image.Width, image.Height);
            Color c = Color.Black;
            Color v = Color.Black;

//            int av = 0;
            for (int i = 0; i < image.Width ; i++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    c = original.GetPixel(i, y);
                    v = Color.FromArgb(alpha, c.R, c.G, c.B);
                    transparentimage.SetPixel(i,y,v);

                }

            }
            return transparentimage;

        }

        private void LoadBackgroundImage()
        {

//            this.BackgroundImage = ConfigurationData.GetBackground("Size" + Globals.LanguageCode.ToString());
//            String ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\locker_size_1182x789.tiff";
//            this.BackgroundImage = Image.FromFile(ImageFilename);
        }

        private void btnSize_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            switch (b.Tag.ToString())
            {
                case "SMALL":
                    //Globals.LockData.Locker_Size
                    Globals.LockData.Locker_Size = eLockerSize.SMALL;
                    Globals.LogFile(Globals.LogSource.SelectSize, "btnSize_Click()", "Small");
                    break;
                case "MEDIUM":
                    Globals.LogFile(Globals.LogSource.SelectSize, "btnSize_Click()", "Medium");
                    Globals.LockData.Locker_Size = eLockerSize.MEDIUM;
                    break;
                case "LARGE":
                    Globals.LogFile(Globals.LogSource.SelectSize, "btnSize_Click()", "Large");
                    Globals.LockData.Locker_Size = eLockerSize.LARGE ;
                    break;
                case "XLARGE":
                    Globals.LogFile(Globals.LogSource.SelectSize, "btnSize_Click()", "ExtraLarge");
                    Globals.LockData.Locker_Size = eLockerSize.XLARGE;
                    break;

            }

            Globals.LogFile(Globals.LogSource.SelectSize, "btnSize_Click()", "Get_LockAvailable()");
            Get_LockAvailable();
            Globals.LockData.Lock_SetReserved();

            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void bBack_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            timer1.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        
        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
        private void InvokeLanguage()
        {
            String language = Globals.GetLanguageCode();

            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            ComponentResourceManager resources = new ComponentResourceManager(typeof(SelectSize));

            foreach (Control C in this.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    foreach (Control D in C.Controls)
                    {
                        resources.ApplyResources(D, D.Name, new CultureInfo(language));
                    }
                }
                else
                {
                    resources.ApplyResources(C, C.Name, new CultureInfo(language));
                }
            }

            this.Update();
        }
    }
}
