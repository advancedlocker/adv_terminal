﻿namespace Lockers
{
    partial class SecurityLockerNo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SecurityLockerNo));
            this.bNumber1 = new System.Windows.Forms.Button();
            this.bNumber2 = new System.Windows.Forms.Button();
            this.bNumber3 = new System.Windows.Forms.Button();
            this.bNumber6 = new System.Windows.Forms.Button();
            this.bNumber5 = new System.Windows.Forms.Button();
            this.bNumber4 = new System.Windows.Forms.Button();
            this.bNumber7 = new System.Windows.Forms.Button();
            this.bNumber8 = new System.Windows.Forms.Button();
            this.bNumber9 = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.lbl1 = new System.Windows.Forms.Label();
            this.bClear = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.bNumber0 = new System.Windows.Forms.Button();
            this.labTime = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.timRTC = new System.Windows.Forms.Timer(this.components);
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.labEnterLockerNo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labBack = new System.Windows.Forms.Label();
            this.labOK = new System.Windows.Forms.Label();
            this.labEnterLockerNoB = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bNumber1
            // 
            this.bNumber1.BackColor = System.Drawing.Color.White;
            this.bNumber1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber1, "bNumber1");
            this.bNumber1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bNumber1.Name = "bNumber1";
            this.bNumber1.UseVisualStyleBackColor = false;
            this.bNumber1.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber2
            // 
            this.bNumber2.BackColor = System.Drawing.Color.White;
            this.bNumber2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber2, "bNumber2");
            this.bNumber2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bNumber2.Name = "bNumber2";
            this.bNumber2.UseVisualStyleBackColor = false;
            this.bNumber2.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber3
            // 
            this.bNumber3.BackColor = System.Drawing.Color.White;
            this.bNumber3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber3, "bNumber3");
            this.bNumber3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bNumber3.Name = "bNumber3";
            this.bNumber3.UseVisualStyleBackColor = false;
            this.bNumber3.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber6
            // 
            this.bNumber6.BackColor = System.Drawing.Color.White;
            this.bNumber6.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber6.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber6, "bNumber6");
            this.bNumber6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bNumber6.Name = "bNumber6";
            this.bNumber6.UseVisualStyleBackColor = false;
            this.bNumber6.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber5
            // 
            this.bNumber5.BackColor = System.Drawing.Color.White;
            this.bNumber5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber5.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber5, "bNumber5");
            this.bNumber5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bNumber5.Name = "bNumber5";
            this.bNumber5.UseVisualStyleBackColor = false;
            this.bNumber5.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber4
            // 
            this.bNumber4.BackColor = System.Drawing.Color.White;
            this.bNumber4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber4, "bNumber4");
            this.bNumber4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bNumber4.Name = "bNumber4";
            this.bNumber4.UseVisualStyleBackColor = false;
            this.bNumber4.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber7
            // 
            this.bNumber7.BackColor = System.Drawing.Color.White;
            this.bNumber7.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber7.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber7, "bNumber7");
            this.bNumber7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bNumber7.Name = "bNumber7";
            this.bNumber7.UseVisualStyleBackColor = false;
            this.bNumber7.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber8
            // 
            this.bNumber8.BackColor = System.Drawing.Color.White;
            this.bNumber8.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber8.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber8, "bNumber8");
            this.bNumber8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bNumber8.Name = "bNumber8";
            this.bNumber8.UseVisualStyleBackColor = false;
            this.bNumber8.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber9
            // 
            this.bNumber9.BackColor = System.Drawing.Color.White;
            this.bNumber9.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber9.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber9, "bNumber9");
            this.bNumber9.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bNumber9.Name = "bNumber9";
            this.bNumber9.UseVisualStyleBackColor = false;
            this.bNumber9.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bOK, "bOK");
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.Name = "bOK";
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // lbl1
            // 
            this.lbl1.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.lbl1, "lbl1");
            this.lbl1.Name = "lbl1";
            // 
            // bClear
            // 
            this.bClear.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bClear, "bClear");
            this.bClear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bClear.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bClear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bClear.Name = "bClear";
            this.bClear.Tag = "0";
            this.bClear.UseVisualStyleBackColor = false;
            this.bClear.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bBack, "bBack");
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.Name = "bBack";
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // bNumber0
            // 
            this.bNumber0.BackColor = System.Drawing.Color.White;
            this.bNumber0.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber0.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber0.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber0.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.bNumber0, "bNumber0");
            this.bNumber0.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bNumber0.Name = "bNumber0";
            this.bNumber0.UseVisualStyleBackColor = false;
            this.bNumber0.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // labTime
            // 
            this.labTime.BackColor = System.Drawing.Color.Transparent;
            this.labTime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labTime, "labTime");
            this.labTime.ForeColor = System.Drawing.Color.White;
            this.labTime.Name = "labTime";
            this.labTime.Click += new System.EventHandler(this.LabTime_Click);
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            this.lbltime.Click += new System.EventHandler(this.Lbltime_Click);
            // 
            // timRTC
            // 
            this.timRTC.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // labEnterLockerNo
            // 
            resources.ApplyResources(this.labEnterLockerNo, "labEnterLockerNo");
            this.labEnterLockerNo.BackColor = System.Drawing.Color.Transparent;
            this.labEnterLockerNo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labEnterLockerNo.ForeColor = System.Drawing.Color.White;
            this.labEnterLockerNo.Name = "labEnterLockerNo";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.ForeColor = System.Drawing.Color.White;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // labBack
            // 
            this.labBack.BackColor = System.Drawing.Color.Transparent;
            this.labBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labBack, "labBack");
            this.labBack.ForeColor = System.Drawing.Color.White;
            this.labBack.Name = "labBack";
            this.labBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // labOK
            // 
            this.labOK.BackColor = System.Drawing.Color.Transparent;
            this.labOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labOK, "labOK");
            this.labOK.ForeColor = System.Drawing.Color.White;
            this.labOK.Name = "labOK";
            this.labOK.Click += new System.EventHandler(this.PINReg_Load);
            // 
            // labEnterLockerNoB
            // 
            resources.ApplyResources(this.labEnterLockerNoB, "labEnterLockerNoB");
            this.labEnterLockerNoB.BackColor = System.Drawing.Color.Transparent;
            this.labEnterLockerNoB.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labEnterLockerNoB.ForeColor = System.Drawing.Color.White;
            this.labEnterLockerNoB.Name = "labEnterLockerNoB";
            // 
            // SecurityLockerNo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.labEnterLockerNoB);
            this.Controls.Add(this.labBack);
            this.Controls.Add(this.labOK);
            this.Controls.Add(this.labTime);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labEnterLockerNo);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bNumber0);
            this.Controls.Add(this.bNumber9);
            this.Controls.Add(this.bNumber8);
            this.Controls.Add(this.bNumber7);
            this.Controls.Add(this.bNumber4);
            this.Controls.Add(this.bNumber5);
            this.Controls.Add(this.bNumber6);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bNumber3);
            this.Controls.Add(this.bNumber2);
            this.Controls.Add(this.bNumber1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SecurityLockerNo";
            this.Load += new System.EventHandler(this.PINReg_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bNumber1;
        private System.Windows.Forms.Button bNumber2;
        private System.Windows.Forms.Button bNumber3;
        private System.Windows.Forms.Button bNumber6;
        private System.Windows.Forms.Button bNumber5;
        private System.Windows.Forms.Button bNumber4;
        private System.Windows.Forms.Button bNumber7;
        private System.Windows.Forms.Button bNumber8;
        private System.Windows.Forms.Button bNumber9;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Button bNumber0;
        private System.Windows.Forms.Label labTime;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer timRTC;
        private System.Windows.Forms.Timer timIdle;
        private System.Windows.Forms.Label labEnterLockerNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labBack;
        private System.Windows.Forms.Label labOK;
        private System.Windows.Forms.Label labEnterLockerNoB;
    }
}