﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace Lockers
{
    public partial class PINLogin : Form
    {
        string m_PIN;
        int[] Keymap = new int[10];

        DateTimeFormatInfo fi = new DateTimeFormatInfo();
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void RandomiseButtons()
        {
            bOK.Visible = false;

            // Randomise the numbers
            Random randomGen = new Random();
            Boolean RandomisationFinished = false;
            int[] CheckMap = new int[10];

            for (int count = 0; count < 10; count++)
            {
                Keymap[count] = 10;
                CheckMap[count] = 0;
            }

            int randomSeqA = randomGen.Next(0123456789, 2147483647);      // Random 32bit number
            int tmp = randomSeqA;

            for (int count = 0; count < 10; count++)
            {
                int CellValue = randomSeqA % 10;
                randomSeqA /= 10;
                if (CheckMap[CellValue] == 0)
                {
                    CheckMap[CellValue] = 1;
                    Keymap[count] = CellValue;
                }
            }

            RandomisationFinished = true;
            for (int count = 0; count < 10; count++)
            {
                if (CheckMap[count] == 0)
                    RandomisationFinished = false;
            }

            while (RandomisationFinished == false)
            {
                randomSeqA = randomGen.Next(0123456789, 2147483647);      // Random 32bit number
                                                                          //                int randomSeqB = randomGen.Next(2147483647, 2147483647);      // Random 32bit number

                tmp = randomSeqA;

                for (int count = 0; count < 10; count++)
                {
                    int CellValue = randomSeqA % 10;
                    randomSeqA /= 10;
                    if (CheckMap[CellValue] == 0)
                    {
                        CheckMap[CellValue] = 1;
                        for (int j = 0; j < 10; j++)
                        {
                            if (Keymap[j] == 10)
                            {
                                Keymap[j] = CellValue;
                                break;
                            }
                        }
                    }
                }

                // Crawl checkkeymap to add the missing numbers
                RandomisationFinished = true;
                for (int count = 0; count < 10; count++)
                {
                    if (CheckMap[count] == 0)
                        RandomisationFinished = false;
                }
            }

            bNumber0.Text = Keymap[0].ToString();
            bNumber1.Text = Keymap[1].ToString();
            bNumber2.Text = Keymap[2].ToString();
            bNumber3.Text = Keymap[3].ToString();
            bNumber4.Text = Keymap[4].ToString();
            bNumber5.Text = Keymap[5].ToString();
            bNumber6.Text = Keymap[6].ToString();
            bNumber7.Text = Keymap[7].ToString();
            bNumber8.Text = Keymap[8].ToString();
            bNumber9.Text = Keymap[9].ToString();

            this.Update();
        }

        public PINLogin()
        {
            InitializeComponent();
            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();

            // What is the difference
            /* SMC
                        if (Globals.Process == ProcessType.endrent)
                        {

                            this.BackgroundImage = ConfigurationData.GetBackground("PINEnd" + Globals.LanguageCode.ToString());

                        }
                        else
                        {
                            this.BackgroundImage = ConfigurationData.GetBackground("PINLog" + Globals.LanguageCode.ToString());
                        }
            */
            //            this.BackgroundImage = ConfigurationData.GetBackground("PINReg" + Globals.LanguageCode.ToString());
            if (Properties.Settings.Default.ReleaseType == "Linux")
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + Properties.Settings.Default.DefaultLanguage + "/Enter_Code.tiff");
            else
                this.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + Properties.Settings.Default.DefaultLanguage + "\\Enter_Code.tiff");

            RandomiseButtons();
            bOK.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblmsg.Text = "";
            lblerror.Visible = false;
            Button b = (Button)sender;

            if (b.Name == "bClear")
            {
                lbl1.Text = "";
                m_PIN = "";
                bOK.Visible = false;
            }
            else
            {
                if (lbl1.Text.Length < Properties.Settings.Default.MaxUserPinLen)
                {
                    lbl1.Text += "*";//b.Text;
                    m_PIN += b.Text;// lbl1.Text;
                }

                if (lbl1.Text.Length >= Properties.Settings.Default.MinUserPinLength)
                    bOK.Visible = true;
                else
                    bOK.Visible = false;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (m_PIN ==null)
            {
                lblerror.Visible = true;
            }
            else if (m_PIN.Length >= Properties.Settings.Default.MinUserPinLength)
            {
                Globals.LockData.PINCode_Set(m_PIN); 

                this.Close();
            }
            else
            {

                //lblmsg.Text = "Invalid Pin code";
                //lblmsg.Visible = true;
                lblerror.Visible = true;
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            this.Close();
        }

        private void PINLogin_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }
    }
}
