﻿using System;
using System.Globalization;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using System.ComponentModel;

namespace Lockers
{
    public partial class PaymentSelection : Form
    {

        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        public PaymentSelection()
        {
            InitializeComponent();

            String ImageFilename = "";
            /*
                        if (Globals.CurrentLanguage == null)
                            ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\PaymentMethod_1189x789.tiff";
                        else
                            ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\PaymentMethod_1189x789.tiff";
                        this.BackgroundImage = Image.FromFile(ImageFilename);
            */

            lbldue.Text = Globals.DueAmnt.ToString("#,##0.00");
//            lbldue.Text = Globals.DueAmnt.ToString("C");

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();
            timIdle.Interval = Properties.Settings.Default.ScreenTimeout;
            timIdle.Start();
        }

        private void PaymentSelection_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            InvokeLanguage();

            if (Properties.Settings.Default.ReleaseType == "Windows")
                bCreditPay.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.ImagesDirectoryWindows + "Common\\Icons\\Credit_Cards.jpg");
            else
                bCreditPay.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.ImagesDirectoryLinux + "Common/Icons/Credit_Cards.jpg");

            lbldue.Text = Globals.DueAmnt.ToString("#,##0.00");

            lbltime.ForeColor = Properties.Settings.Default.TextColor;
            lbldue.ForeColor = Properties.Settings.Default.TextColor;
            labLocalCurrency.ForeColor = Properties.Settings.Default.TextColor;
            labPaymentSelectHeading.ForeColor = Properties.Settings.Default.TextColor;
            label1.ForeColor = Properties.Settings.Default.TextColor;
            labBack.ForeColor = Properties.Settings.Default.TextColor;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void bCreditPay_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Globals.PayTypes = PaymentTypes.CreditCard;
            this.Close();
        }

        private void bCashPay_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Globals.PayTypes = PaymentTypes.Cash;
            this.Close();
        }

        private void bBack_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void timIdle_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timIdle.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void InvokeLanguage()
        {
            String language = Globals.GetLanguageCode();

            Thread.CurrentThread.CurrentCulture = new CultureInfo(language);
            ComponentResourceManager resources = new ComponentResourceManager(typeof(PaymentSelection));

            foreach (Control C in this.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    foreach (Control D in C.Controls)
                    {
                        resources.ApplyResources(D, D.Name, new CultureInfo(language));
                    }
                }
                else
                {
                    resources.ApplyResources(C, C.Name, new CultureInfo(language));
                }
            }

            this.Update();
        }
    }
}
