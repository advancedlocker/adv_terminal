﻿using System;
using System.Globalization;
using System.Windows.Forms;
using System.Drawing;
using System.Xml;
using System.IO;
using System.Threading;
using System.Resources;
using System.ComponentModel;

namespace Lockers
{
    public partial class welcome : Form
    {
        SmartPay smartpay;
        DateTimeFormatInfo fi = new DateTimeFormatInfo();
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void keypressedHandler(object sender, EventArgs e)
        {
            string lang = "en";

            Globals.SetLanguage(((Button)sender).Name);

            this.DialogResult = DialogResult.OK;

            timRTC.Stop();
            timLanguage.Stop();

            this.Close();
        }

        public welcome(SmartPay smartpay)
        {
            InitializeComponent();

#if (false)
            //            Size sz = new Size(640, 480);
            this.AutoScaleDimensions = new System.Drawing.SizeF(1F, 2F);
            this.PerformAutoScale();
#endif 

            Globals.LogFile(Globals.LogSource.Welcomescreen, "welcome(SmartPay smartpay)", "Getting country information");

/*            
            String ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\Welcome_1182x788.tiff";
            this.BackgroundImage = Image.FromFile(ImageFilename);
*/
            String Country = Properties.Settings.Default.Country;
            Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Country =  " + Country);

            Globals.LogFile(Globals.LogSource.Mainscreen, "Mainscreen_Shown()", "Using XML reader to get information");
            XmlTextReader xmlReader = new XmlTextReader(Properties.Settings.Default.CountryConfigurationLinux);

            XmlDataDocument xmldoc = new XmlDataDocument();
            XmlNodeList xmlnode;

            FileStream fs;
            if (Properties.Settings.Default.ReleaseType == "Windows")
                fs = new FileStream(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.CountryConfigurationWindows, FileMode.Open, FileAccess.Read);
            else
                fs = new FileStream(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.CountryConfigurationLinux, FileMode.Open, FileAccess.Read);

            xmldoc.Load(fs);
            xmlnode = xmldoc.GetElementsByTagName("Country");
            int NumSupportedLanguages = 0;

            String[] Languages = new String[0];
            String[] Flags = new String[0];

            for (int i = 0; i < xmlnode.Count; i++)
            {
                if (xmlnode[i].ChildNodes[0].Name == Country)
                {
                    for (int j = 0; j < xmlnode[i].ChildNodes[0].ChildNodes.Count; j++)
                    {
                        switch (xmlnode[i].ChildNodes[0].ChildNodes[j].Name)
                        {
                            case "NumSupportedLanguages":
                                {
                                    NumSupportedLanguages = Convert.ToInt16(xmlnode[i].ChildNodes[0].ChildNodes[j].InnerText);
                                    //                                    LanguageButtons = new Button[NumSupportedLanguages];
                                    Languages = new String[NumSupportedLanguages];
                                    Flags = new String[NumSupportedLanguages];
                                }
                                break;
                            case "Languages":
                                for (int k = 0; k < xmlnode[i].ChildNodes[0].ChildNodes[j].ChildNodes.Count; k++)
                                {
                                    Languages[k] = xmlnode[i].ChildNodes[0].ChildNodes[j].ChildNodes[k].InnerText;
                                }
                                break;
                            case "Flags":
                                for (int k = 0; k < xmlnode[i].ChildNodes[0].ChildNodes[j].ChildNodes.Count; k++)
                                {
                                    Flags[k] = xmlnode[i].ChildNodes[0].ChildNodes[j].ChildNodes[k].InnerText;
                                }
                                break;
                        }
                    }
                }
            }

            Button[] LanguageButtons = new Button[NumSupportedLanguages];

            int leftFlags = 0;
            int Spacing = 0;
            bool NumFlagsOdd = false;

            if ((NumSupportedLanguages % 2) == 1)
            {
                NumFlagsOdd = true;
                leftFlags = (NumSupportedLanguages - 1) / 2;
                Spacing = 0;
            }
            else
            {
                NumFlagsOdd = false;
                leftFlags = NumSupportedLanguages / 2;
                Spacing = 1;
            }

            for (int i = 0; i < NumSupportedLanguages; i++)
            {
                // Todo : change the ratios for different screens
                LanguageButtons[i] = new Button();
                LanguageButtons[i].Top = (this.Height * 2/3);
                LanguageButtons[i].Name = Languages[i];
                LanguageButtons[i].BackColor = Color.Red;
                LanguageButtons[i].Click += new EventHandler(keypressedHandler);

                if (Properties.Settings.Default.ReleaseType == "Windows")
                    LanguageButtons[i].BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootWindows + Properties.Settings.Default.FlagsDirectoryWindows + Flags[i] + "_Flag_112x70.tiff");
                else
                    LanguageButtons[i].BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRootLinux + Properties.Settings.Default.FlagsDirectoryLinux + Flags[i] + "_Flag_112x70.tiff");

                LanguageButtons[i].Size = new Size(LanguageButtons[i].BackgroundImage.Width, LanguageButtons[i].BackgroundImage.Height);

                if(Spacing <= 1)
                    Spacing *= LanguageButtons[i].BackgroundImage.Width;

                int CentrePosn = ((this.Width / 2) - (LanguageButtons[i].BackgroundImage.Width / 2));
                if (NumFlagsOdd == true) // The number of falgs requested is odd
                {
                    if (i < leftFlags)
                    {
                        // Close Spacing
//                        LanguageButtons[i].Left = (this.Width / 2) - (LanguageButtons[i].BackgroundImage.Width / 2) - ((leftFlags - i) * (LanguageButtons[i].BackgroundImage.Width));
                        // Large Spacing
                        LanguageButtons[i].Left = (this.Width / 2) - (LanguageButtons[i].BackgroundImage.Width)/2 - ((leftFlags - i) * (LanguageButtons[i].BackgroundImage.Width));
                    }
                    else
                    {
                        if (i == leftFlags)
                            LanguageButtons[i].Left = (this.Width / 2) - (LanguageButtons[i].BackgroundImage.Width / 2);
                        else
                        {
                            // Close Spacing
                            LanguageButtons[i].Left = ((this.Width / 2) - (LanguageButtons[i].BackgroundImage.Width / 2)) + ((i - leftFlags) * (LanguageButtons[i].BackgroundImage.Width));
                        }
                    }
                }
                else                    // Even number of flags
                {
                    if (i < leftFlags)
                        LanguageButtons[i].Left = (this.Width / 2) - ((leftFlags - i) * (LanguageButtons[i].BackgroundImage.Width));
                    else
                        LanguageButtons[i].Left = (this.Width / 2) + ((i - leftFlags) * (LanguageButtons[i].BackgroundImage.Width));
                }

                this.Controls.Add(LanguageButtons[i]);
            }

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timRTC.Start();
        }

        private void welcome_Load(object sender, EventArgs e)
        {
            string name = Properties.Settings.Default.NoteRecycle;
            string[] sArr = name.Split(' ');

            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                Globals.SendCommsLogReport("AllowWindowFocusChange = True");
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                Globals.SendCommsLogReport("AllowWindowFocusChange = False");
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            if (Properties.Settings.Default.MaintenanceLockout)
            {
                labPaymentStatus.Text = "";
                labWelcome.Text = "Terminal Out of Service";

                for (int i = 0; i < this.Controls.Count; i++)
                {
                    Type ctype = this.Controls[i].GetType();
                    if (ctype.Name == "Button")
                        this.Controls[i].Visible = false;
                }
            }
            else
            {
                labWelcome.Text = "Welcome";

                if ((Properties.Settings.Default.PaymentEnableCash == false) && (Properties.Settings.Default.PaymentEnableCredit == true))
                    labPaymentStatus.Text = "Card Only Terminal";

                if ((Properties.Settings.Default.PaymentEnableCash == true) && (Properties.Settings.Default.PaymentEnableCredit == false))
                    labPaymentStatus.Text = "Cash Only Terminal";

                if ((Properties.Settings.Default.PaymentEnableCash == true) && (Properties.Settings.Default.PaymentEnableCredit == true))
                    labPaymentStatus.Text = "Cash and Card Available";
            }
            // TODO : Disable the terminal if cash and credit are disabled
            // TODO : Disable the terminal in maintenance mode. 
            /*
                        if ((Properties.Settings.Default.PaymentEnableCash == false) && (Properties.Settings.Default.PaymentEnableCredit == false))
                        {
                            for (int i = 0; i < this.Controls.Count; i++)
                            {
                                Type ctype = this.Controls[i].GetType();
                                if (ctype.Name == "Button")
                                    this.Controls[i].Visible = false;
                            }

                            labPaymentStatus.Text = "Payment Not Available";
                        }
                        else
                        {
                            for (int i = 0; i < this.Controls.Count; i++)
                            {
                                Type ctype = this.Controls[i].GetType();
                                if (ctype.Name == "Button")
                                    this.Controls[i].Visible = true;
                            }

                            if ((Properties.Settings.Default.PaymentEnableCash == false) && (Properties.Settings.Default.PaymentEnableCredit == true))
                                labPaymentStatus.Text = "Card Only Terminal";

                            if ((Properties.Settings.Default.PaymentEnableCash == true) && (Properties.Settings.Default.PaymentEnableCredit == false))
                                labPaymentStatus.Text = "Cash Only Terminal";

                            if ((Properties.Settings.Default.PaymentEnableCash == true) && (Properties.Settings.Default.PaymentEnableCredit == true))
                                labPaymentStatus.Text = "Cash and Card Available";
                        }

                        if (Properties.Settings.Default.ShowPaymentStatus == true)
                            labPaymentStatus.Visible = true;
                        else
                            labPaymentStatus.Visible = false;
            */

            labPaymentStatus.ForeColor = Properties.Settings.Default.TextColor;
            labPressFlag.ForeColor = Properties.Settings.Default.TextColor;
            labTime.ForeColor = Properties.Settings.Default.TextColor;
            labWelcome.ForeColor = Properties.Settings.Default.TextColor;
            lbltime.ForeColor = Properties.Settings.Default.TextColor;
        }

        private void welcome_Click(object sender, EventArgs e)
        {
//            this.DialogResult = DialogResult.OK;
//            this.Close();
        }

        private static bool RentalReset = false;
        private void timRTC_Tick(object sender, EventArgs e)
        {
            try
            {
                if (Properties.Settings.Default.MaintenanceLockout)
                {
                    labPaymentStatus.Text = "";
                    labWelcome.Text = "Terminal Out of Service";
                    labWelcome.Font = new Font("Microsoft Sans Serif", 32);
                    labPressFlag.Visible = false;

                    for (int i = 0; i < this.Controls.Count; i++)
                    {
                        Type ctype = this.Controls[i].GetType();
                        if (ctype.Name == "Button")
                            this.Controls[i].Visible = false;
                    }
                }
                else
                {
                    labWelcome.Text = "Welcome";
                    labWelcome.Font = new Font("Microsoft Sans Serif", 64);
                    labPressFlag.Visible = true;

                    for (int i = 0; i < this.Controls.Count; i++)
                    {
                        Type ctype = this.Controls[i].GetType();
                        if (ctype.Name == "Button")
                            this.Controls[i].Visible = true;
                    }

                    if ((Properties.Settings.Default.PaymentEnableCash == false) && (Properties.Settings.Default.PaymentEnableCredit == true))
                        labPaymentStatus.Text = "Card Only Terminal";

                    if ((Properties.Settings.Default.PaymentEnableCash == true) && (Properties.Settings.Default.PaymentEnableCredit == false))
                        labPaymentStatus.Text = "Cash Only Terminal";

                    if ((Properties.Settings.Default.PaymentEnableCash == true) && (Properties.Settings.Default.PaymentEnableCredit == true))
                        labPaymentStatus.Text = "Cash and Card Available";
                }

                lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);

                string[] ClearHireTime = Properties.Settings.Default.ResetRentalTime.Split(':');
                string[] DT = System.DateTime.Now.ToString("HH:mm").Split(':');

                Globals.LogFile(Globals.LogSource.Welcomescreen, "welcome(SmartPay smartpay)", "Properties.Settings.Default.ResetRentalTime");

                if ((Convert.ToInt32(DT[0]) == Convert.ToInt32(ClearHireTime[0])) && (Convert.ToInt32(DT[1]) == Convert.ToInt32(ClearHireTime[1])))
                {
                    if (RentalReset == false)
                    {
                        Globals.LogFile(Globals.LogSource.Welcomescreen, "welcome(SmartPay smartpay)", "Resetting rentals");
                        RentalReset = true;
                            Globals.LockData.ClearAllHires();
                    }
                }
                else
                    RentalReset = false;
            }
            catch (Exception ex)
            {
                Globals.LogFile(Globals.LogSource.Welcomescreen, "welcome(SmartPay smartpay)", "Exception = " + ex.ToString());
            }

            this.Update();
        }

        private void TimLanguage_Tick(object sender, EventArgs e)
        {
            //TODO : TO fix - leave in English for the moment. 
            return;

            string lang = CultureInfo.CurrentCulture.ToString();
            switch (lang)
            {
                case "af":          // Africans
                    lang = "ar";
                    break;
                case "ar":          // Arabic
                    lang = "zh";
                    break;
                case "zh":          // Chinese
                    lang = "de";
                    break;
                case "de":          // German
                    lang = "en";
                    break;
                case "en-AU":       // Eng Australian
                    lang = "es";
                    break;
                case "en":          // Eng Generic
                    lang = "es";
                    break;
                case "es":          // Spanish
                    lang = "fr";
                    break;
                case "fr":          // French
                    lang = "it";
                    break;
                case "it":          // Italian
                    lang = "ar";
                    break;
                default:            // Goto english
                    lang = "en";
                    break;
            }

            Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);

            foreach (Control C in this.Controls)
            {
                if (C.Controls.Count > 0)
                {
                    foreach (Control D in C.Controls)
                    {
                        ComponentResourceManager resources = new ComponentResourceManager(typeof(welcome));
                        resources.ApplyResources(D, D.Name, new CultureInfo(lang));
                    }
                }
                else
                {
                    ComponentResourceManager resources = new ComponentResourceManager(typeof(welcome));
                    resources.ApplyResources(C, C.Name, new CultureInfo(lang));
                }
            }
        }

        private void LabWelcome_Click(object sender, EventArgs e)
        {
            if (
                ((Properties.Settings.Default.PaymentEnableCash == false) && (Properties.Settings.Default.PaymentEnableCredit == false)) || 
                (Properties.Settings.Default.MaintenanceLockout == true)
                )
            {
                // Open Maintenance Access Code
                int mcode = 0;
                NumericKeypad kpad = new NumericKeypad("MAINTENANCE ACCESS KEY");
                //kpad.Data = txtValue1.Text;
                DialogResult dr = kpad.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    if (kpad.Data == "") return;
                    mcode = int.Parse(kpad.Data);
                }

                kpad.Dispose();
                kpad = null;

                if (mcode == Properties.Settings.Default.Adminpwd)
                {
                    AdminTask admin = new AdminTask(smartpay);
                    admin.BringToFront();
                    admin.ShowDialog();

                    admin.Dispose();

                    this.Show();
                }


                if (mcode == Properties.Settings.Default.ExitCode)
                {
                    try
                    {
                        Globals.WatchdogProcess.Kill();
                    }
                    catch
                    {
                    }

                    if (Properties.Settings.Default.AutoRestartApplicationInternally)
                        Globals.RemoveRunningFile();

                    Application.Exit();
                }
            }
        }

        private void Welcome_Shown(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.ShowPaymentStatus == true)
                labPaymentStatus.Visible = true;
            else
                labPaymentStatus.Visible = false;

        }
    }
}
