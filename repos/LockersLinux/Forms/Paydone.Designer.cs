﻿namespace Lockers
{
    partial class Paydone
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Paydone));
            this.bOK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblLocknum = new System.Windows.Forms.Label();
            this.lblremaintime = new System.Windows.Forms.Label();
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.picBoxArrow = new System.Windows.Forms.PictureBox();
            this.labOK = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxArrow)).BeginInit();
            this.SuspendLayout();
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bOK, "bOK");
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bOK.Name = "bOK";
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Click += new System.EventHandler(this.button10_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Name = "label1";
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblLocknum
            // 
            resources.ApplyResources(this.lblLocknum, "lblLocknum");
            this.lblLocknum.BackColor = System.Drawing.Color.Transparent;
            this.lblLocknum.Name = "lblLocknum";
            // 
            // lblremaintime
            // 
            resources.ApplyResources(this.lblremaintime, "lblremaintime");
            this.lblremaintime.BackColor = System.Drawing.Color.Transparent;
            this.lblremaintime.Name = "lblremaintime";
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // picBoxArrow
            // 
            resources.ApplyResources(this.picBoxArrow, "picBoxArrow");
            this.picBoxArrow.Name = "picBoxArrow";
            this.picBoxArrow.TabStop = false;
            // 
            // labOK
            // 
            this.labOK.BackColor = System.Drawing.Color.Transparent;
            this.labOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            resources.ApplyResources(this.labOK, "labOK");
            this.labOK.ForeColor = System.Drawing.Color.White;
            this.labOK.Name = "labOK";
            this.labOK.Click += new System.EventHandler(this.button10_Click);
            // 
            // Paydone
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.labOK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.picBoxArrow);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.lblremaintime);
            this.Controls.Add(this.lblLocknum);
            this.Controls.Add(this.bOK);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Paydone";
            this.Load += new System.EventHandler(this.Paydone_Load);
            this.Click += new System.EventHandler(this.Paydone_Click);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxArrow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblLocknum;
        private System.Windows.Forms.Label lblremaintime;
        private System.Windows.Forms.Timer timIdle;
        private System.Windows.Forms.PictureBox picBoxArrow;
        private System.Windows.Forms.Label labOK;
    }
}