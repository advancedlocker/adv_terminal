﻿namespace Lockers
{
    partial class TimeSelectionBlock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimeSelectionBlock));
            this.btnMinusMin = new System.Windows.Forms.Button();
            this.btnMinusHrs = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.lblsize = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labTimeHead = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timIdle = new System.Windows.Forms.Timer(this.components);
            this.lab3Hrs = new System.Windows.Forms.Label();
            this.labAllDay = new System.Windows.Forms.Label();
            this.lab3HrPrice = new System.Windows.Forms.Label();
            this.labAllDayPrice = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMinusMin
            // 
            this.btnMinusMin.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.btnMinusMin, "btnMinusMin");
            this.btnMinusMin.FlatAppearance.BorderSize = 0;
            this.btnMinusMin.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnMinusMin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnMinusMin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnMinusMin.Name = "btnMinusMin";
            this.btnMinusMin.UseVisualStyleBackColor = false;
            this.btnMinusMin.Click += new System.EventHandler(this.btnMinusMin_Click);
            // 
            // btnMinusHrs
            // 
            this.btnMinusHrs.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this.btnMinusHrs, "btnMinusHrs");
            this.btnMinusHrs.FlatAppearance.BorderSize = 0;
            this.btnMinusHrs.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnMinusHrs.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnMinusHrs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnMinusHrs.Name = "btnMinusHrs";
            this.btnMinusHrs.UseVisualStyleBackColor = false;
            this.btnMinusHrs.Click += new System.EventHandler(this.btnMinusHrs_Click);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bBack, "bBack");
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.Name = "bBack";
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bback_Click);
            // 
            // lblsize
            // 
            resources.ApplyResources(this.lblsize, "lblsize");
            this.lblsize.BackColor = System.Drawing.Color.Transparent;
            this.lblsize.ForeColor = System.Drawing.Color.White;
            this.lblsize.Name = "lblsize";
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.labTimeHead);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Name = "panel1";
            // 
            // labTimeHead
            // 
            resources.ApplyResources(this.labTimeHead, "labTimeHead");
            this.labTimeHead.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHead.ForeColor = System.Drawing.Color.White;
            this.labTimeHead.Name = "labTimeHead";
            // 
            // button14
            // 
            resources.ApplyResources(this.button14, "button14");
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button14.Name = "button14";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Name = "label2";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timIdle
            // 
            this.timIdle.Interval = 10000;
            this.timIdle.Tick += new System.EventHandler(this.timIdle_Tick);
            // 
            // lab3Hrs
            // 
            resources.ApplyResources(this.lab3Hrs, "lab3Hrs");
            this.lab3Hrs.BackColor = System.Drawing.Color.White;
            this.lab3Hrs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lab3Hrs.ForeColor = System.Drawing.Color.Black;
            this.lab3Hrs.Name = "lab3Hrs";
            this.lab3Hrs.Click += new System.EventHandler(this.btnMinusHrs_Click);
            // 
            // labAllDay
            // 
            resources.ApplyResources(this.labAllDay, "labAllDay");
            this.labAllDay.BackColor = System.Drawing.Color.White;
            this.labAllDay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labAllDay.ForeColor = System.Drawing.Color.Black;
            this.labAllDay.Name = "labAllDay";
            this.labAllDay.Click += new System.EventHandler(this.btnMinusMin_Click);
            // 
            // lab3HrPrice
            // 
            resources.ApplyResources(this.lab3HrPrice, "lab3HrPrice");
            this.lab3HrPrice.BackColor = System.Drawing.Color.White;
            this.lab3HrPrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lab3HrPrice.ForeColor = System.Drawing.Color.Black;
            this.lab3HrPrice.Name = "lab3HrPrice";
            this.lab3HrPrice.Click += new System.EventHandler(this.btnMinusHrs_Click);
            // 
            // labAllDayPrice
            // 
            resources.ApplyResources(this.labAllDayPrice, "labAllDayPrice");
            this.labAllDayPrice.BackColor = System.Drawing.Color.White;
            this.labAllDayPrice.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labAllDayPrice.ForeColor = System.Drawing.Color.Black;
            this.labAllDayPrice.Name = "labAllDayPrice";
            this.labAllDayPrice.Click += new System.EventHandler(this.btnMinusMin_Click);
            // 
            // TimeSelectionBlock
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(195)))), ((int)(((byte)(65)))));
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.labAllDayPrice);
            this.Controls.Add(this.lab3HrPrice);
            this.Controls.Add(this.labAllDay);
            this.Controls.Add(this.lab3Hrs);
            this.Controls.Add(this.lblsize);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.btnMinusHrs);
            this.Controls.Add(this.btnMinusMin);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TimeSelectionBlock";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TimeSelectionBlock_FormClosed);
            this.Load += new System.EventHandler(this.TimeSelectionBlock_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnMinusMin;
        private System.Windows.Forms.Button btnMinusHrs;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Label lblsize;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labTimeHead;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timIdle;
        private System.Windows.Forms.Label lab3Hrs;
        private System.Windows.Forms.Label labAllDay;
        private System.Windows.Forms.Label lab3HrPrice;
        private System.Windows.Forms.Label labAllDayPrice;
    }
}