﻿

namespace Lockers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class PaidEventArgs: EventArgs
    {
        #region Fields
        private float amount;
        private string sourcetype; //bills or coins
        #endregion

        public PaidEventArgs(string sourcetype, float amount)
        {
            this.sourcetype = sourcetype;
            this.amount = amount;
        }
        public String Source
        {
            get { return this.sourcetype; }
        }

        public float Amount
        {
            get { return this.amount; }
        }
    }
}
