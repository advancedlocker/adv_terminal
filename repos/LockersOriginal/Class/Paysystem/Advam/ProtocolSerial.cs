/*
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "windows.h"

#include "CRC16.h"
#include "ProtocolSerial.h"

static BOOL CheckCRC(BYTE* pbyBuf);
static USHORT CalcCRC(BYTE* pbyBuf);
static void ExtractPayload(BYTE* pbyBuf, char* pcPayLoad);
*/

using System;
using System.IO.Ports;
using System.Text;
using System.Threading;

namespace Lockers
{
    public class AdvamProtocolSerial
    {
        public enum TERMINAL_CMD
        {
            CMD_INIT,
            CMD_LINK_TEST,
            CMD_TICKET_READ,
            CMD_PAYMENT_CARD_DET,
            CMD_PAYMENT,
            CMD_PAYMENT_CONF,
            CMD_CARD_EJECT,
            CMD_DISABLE_READER,
            CMD_SETTLEMENT,
            CMD_DISPLAY_MSG
        }

        public enum TERMINAL_STATS
        {
            STATS_OK = 0,
            STATS_FAIL,
            STATS_TIMEOUT,
            STATS_CANCEL,
            STATS_CARDREADFAILED,
            STATS_READER_NOT_EMPTY,
            STATS_CARD_NOT_ALLOWED,
            STATS_TERMINAL_ERR = 9
        }

        public struct TERMINAL_REQ_s
        {
            public TERMINAL_CMD Cmd;
            public byte byVersion;
            public byte byTimeout;
            public string pbyAmount;
            public string pbySurcharge;
            public string pbyEquipId;
            public string pbyTxRef;
            public string pbyStan;
/*            byte[] pbyAmount[8 + 1];
            byte[] pbySurcharge[256 + 1];
            byte[] pbyEquipId[12 + 1];
            byte[] pbyTxRef[24 + 1];
            byte[] pbyStan[6 + 1];
*/
            public TERMINAL_STATS Stats;
        }

        public struct TERMINAL_RES_s
        {
            public TERMINAL_CMD Cmd;
            public byte byVersion;
            public TERMINAL_STATS Stats;
            public byte byDisplayId;
            public byte[] pbyDisplayText;
            public byte[] pbyResponseCode;
            public byte[] pbyResponseText;
            public byte[] pbyStan;
            public byte[] pbyAuth;
            public byte[] pbyTruncCardData;
            public byte[] pbySurchargeId;
            public byte[] pbyCardType;
            public byte[] pbyReceipt;
/*
            byte pbyDisplayText[64];
            byte pbyResponseCode[1 + 1];
            byte pbyResponseText[64];
            byte pbyStan[6 + 1];
            byte pbyAuth[64];
            byte pbyTruncCardData[19 + 1];
            byte pbySurchargeId[8 + 1];
            byte pbyCardType[64];
            byte pbyReceipt[2048];
*/
        }

        const byte STX = 0x02;
        const byte ETX = 0x03;
        const byte ACK = 0x06;
        const byte NAK = 0x15;
        const byte WACK = 0x13;

        public int byPrevSeqNum;
        public int byPrevSentSeqNum;

        public SerialPort CreditPort;
        public int dwBytesRead;

        public int DataReceived;
        public byte[] RxBuffer = new byte[1];

        public void PSInit()
        {
            if (CreditPort == null)
                CreditPort = new SerialPort(Properties.Settings.Default.AdvamReaderPort, Properties.Settings.Default.AdvamReaderBaud);

            // Add serial port handlers here
            CreditPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
            CreditPort.Open();


            // Set message sequence number to the startup value
            byPrevSeqNum = 255;
            byPrevSentSeqNum = 255;
        }

        private void DataReceivedHandler(
                        object sender,
                        SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            byte[] tmp = new byte[1024];
            int loc = 0;

            int startloc = 0;
            int finishloc = 0;

            bool finished = false;
            bool started = false;
            while (!finished)
            {
                int bread = sp.BytesToRead;
                if (bread > 0)
                {
                    byte[] indata = new byte[bread];
                    sp.Read(indata, 0, bread);

                    for (int count = 0; count < bread; count++)
                    {
                        if ((indata[count] == ACK) || (indata[count] == NAK))
                        {
                            startloc = count;
                            started = true;
                            Array.Copy(indata, startloc, tmp, 0 + loc, bread - startloc);
                            loc = bread - startloc;
                            break;
                        }
                        if ((started == true) && (indata[count] == ETX))
                        {
                            finishloc = count;
                            finished = true;
                            Array.Copy(indata, 0, tmp, 0 + loc, bread);
                            break;
                        }
                    }
                }
            }

            SetText(tmp);

            Console.WriteLine("Data Received:");
            Console.Write(tmp);
        }

        delegate void SetTextCallback(string text);
        private void SetText(byte[] text)
	    {
            RxBuffer = text;
        }

        public void PSInitC()
        {
            /*
                        DCB dcbSerialParams = { 0 };
                        COMMTIMEOUTS timeouts = { 0 };

                        printf("PSInit -\n");

                        hSerial = CreateFile("COM8",
                                  GENERIC_READ | GENERIC_WRITE,
                                  0,
                                  0,
                                  OPEN_EXISTING,
                                  FILE_ATTRIBUTE_NORMAL,
                                  0);

                        if (hSerial == INVALID_HANDLE_VALUE)
                        {
                            if (GetLastError() == ERROR_FILE_NOT_FOUND)
                            {
                                //serial port does not exist. Inform user.
                            }
                            //some other error occurred. Inform user.
                        }

                        if (!GetCommState(hSerial, &dcbSerialParams))
                        {
                            //error getting state
                        }

                        dcbSerialParams.BaudRate = CBR_9600;
                        dcbSerialParams.ByteSize = 8;
                        dcbSerialParams.StopBits = ONESTOPBIT;
                        dcbSerialParams.Parity = NOPARITY;

                        if (!SetCommState(hSerial, &dcbSerialParams))
                        {
                            //error setting serial port state
                        }

                        timeouts.ReadIntervalTimeout = 50;
                        timeouts.ReadTotalTimeoutConstant = 50;
                        timeouts.ReadTotalTimeoutMultiplier = 10;
                        timeouts.WriteTotalTimeoutConstant = 50;
                        timeouts.WriteTotalTimeoutMultiplier = 10;

                        if (!SetCommTimeouts(hSerial, &timeouts))
                        {
                            //error occureed. Inform user
                        }

                        // Set message sequence number to the startup value
                        byPrevSeqNum = 255;
                        byPrevSentSeqNum = 255;
            */
        }

        public void PSDispose()
        {
            CreditPort.Close();
        }

        public void PSWriteRead(byte[] pbyMessage)
        {
            // Format:
            // STX | NUM | ... data bytes ... | ETX | CRC1 (lowbyte) | CRC2 (highbyte)

            byte[] pbyBuf = new byte[4096];
            int byCurrSeqNum;
            UInt32 dwBytesRead;
            UInt32 dwBytesWritten;
            UInt16 CRC;
            byte CRC1, CRC2;
            UInt16 mask, tmp;
            int i, j;
            byte c;
            UInt32 dwTickStart;
            UInt32 dwTickCheck;
            UInt32 dwTickDiff;
            bool bDone = false;

            Array.Clear(pbyBuf, 0, pbyBuf.Length);
            i = 0;

            int pbyMessage_len = 0;
            for (pbyMessage_len = 0; pbyMessage_len < pbyMessage.Length; pbyMessage_len++)
            {
                if (pbyMessage[pbyMessage_len] == 0x00)
                    break;
            }

            // First char must be STX
            pbyBuf[i++] = STX;

            // Next is Sequence Number
            switch (byPrevSentSeqNum)
            {
                case 254:
                    // Roll over to 0
                    byCurrSeqNum = 0;
                    byPrevSentSeqNum = 0;
                    break;

                case 255:
                    // Startup value, set current to this value...
                    byCurrSeqNum = 255;
                    // and decrement previous sent value, so that the next time it will be 0
                    byPrevSentSeqNum--;
                    break;

                default:
                    byCurrSeqNum = ++byPrevSentSeqNum;
                    break;
            }

            pbyBuf[i++] = (byte)byCurrSeqNum;

            // Data bytes
            Array.Copy(pbyMessage, 0, pbyBuf, 2, pbyMessage_len);
            /*
                        for (j = 0; j < pbyMessage_len; j++)
                        {
                            pbyBuf[i++] = pbyMessage[j];
                        }
            */
            i += pbyMessage_len;
            // Append ETX
            pbyBuf[i++] = ETX;

            // Calculate CRC
            CRC = CalcCRC(ref pbyBuf);

            // CRC1 (lowbyte)
            mask = 0x00FF;
            tmp = CRC;
            tmp &= mask;
            CRC1 = (byte)tmp;

            // CRC2 (highbyte)
            tmp = (UInt16)(CRC >> 8);
            CRC2 = (byte)tmp;

            // Append CRC
            pbyBuf[i++] = CRC1;
            pbyBuf[i++] = CRC2;

            // OK complete message built, send off
            CreditPort.Write(pbyBuf, 0, i);
            //            WriteFile(hSerial, pbyBuf, i, &dwBytesWritten, NULL);

            Thread.Sleep(50);
            while (RxBuffer[0] == 0x00) ;
            // TODO  : put protection for timeouts


            bDone = false;
            while (!bDone)
            {
                // Wait for reception of ACK, NAK or WACK during Answer Timeout of 1500 ms
                dwBytesRead = (uint)RxBuffer.Length;

                if (dwBytesRead == 0)
                {
                    Console.WriteLine("PSWrite - Answer Timeout happened, resending\n");
                    CreditPort.Write(pbyBuf, 0, i);
                    //                   WriteFile(hSerial, pbyBuf, i, &dwBytesWritten, NULL);
                    continue;
                }

                c = (byte)RxBuffer[0];
                switch (c)
                {
                    case ACK:
                        // Read Sequence Number
                        // TODO : Fix this ReadFile
                        //                        ReadFile(hSerial, &c, 1, &dwBytesRead, NULL);

                        c = (byte)RxBuffer[1];
                        if (dwBytesRead > 0)
                        {
                            // Check Sequence Number
                            if (c == byCurrSeqNum)
                            {
                                byte[] Msg = new byte[dwBytesRead - 2];
                                Array.Copy(RxBuffer, 2, Msg, 0, dwBytesRead - 2);
                                PSRead(ref Msg);
                                bDone = true;
                            }
                            else
                            {
                                Console.WriteLine("PSWrite - Received incorrect message number %d, keep listening\n", c);
                            }
                        }
                        else
                        {
                            Console.WriteLine("PSWrite - ICT while expecting Sequence Number\n");
                        }

                        break;

                    case NAK:
                        Console.WriteLine("PSWrite - Received a NAK, resending\n");
                        CreditPort.Write(pbyBuf, 0, i);
                        //                        WriteFile(hSerial, pbyBuf, i, &dwBytesWritten, NULL);
                        break;

                    case WACK:
                        Console.WriteLine("PSWrite - Received a Wait_ACK, keep listening\n");
                        break;

                    default:
                        Console.WriteLine("PSWrite - Received unknown response %d, keep listening\n", c);
                        break;
                }
            }
        }

        public void PSWrite(byte[] pbyMessage)
        {
            // Format:
            // STX | NUM | ... data bytes ... | ETX | CRC1 (lowbyte) | CRC2 (highbyte)

            byte[] pbyBuf = new byte[4096];
            int byCurrSeqNum;
            UInt32 dwBytesRead;
            UInt32 dwBytesWritten;
            UInt16 CRC;
            byte CRC1, CRC2;
            UInt16 mask, tmp;
            int i, j;
            byte c;
            UInt32 dwTickStart;
            UInt32 dwTickCheck;
            UInt32 dwTickDiff;
            bool bDone = false;

            Array.Clear(pbyBuf, 0, pbyBuf.Length);
            i = 0;

            int pbyMessage_len = 0;
            for (pbyMessage_len = 0; pbyMessage_len < pbyMessage.Length; pbyMessage_len++)
            {
                if (pbyMessage[pbyMessage_len] == 0x00)
                    break;
            }

            // First char must be STX
            pbyBuf[i++] = STX;

            // Next is Sequence Number
            switch (byPrevSentSeqNum)
            {
                case 254:
                    // Roll over to 0
                    byCurrSeqNum = 0;
                    byPrevSentSeqNum = 0;
                    break;

                case 255:
                    // Startup value, set current to this value...
                    byCurrSeqNum = 255;
                    // and decrement previous sent value, so that the next time it will be 0
                    byPrevSentSeqNum--;
                    break;

                default:
                    byCurrSeqNum = ++byPrevSentSeqNum;
                    break;
            }

            pbyBuf[i++] = (byte)byCurrSeqNum;

            // Data bytes
            Array.Copy(pbyMessage, 0, pbyBuf, 2, pbyMessage_len);
            /*
                        for (j = 0; j < pbyMessage_len; j++)
                        {
                            pbyBuf[i++] = pbyMessage[j];
                        }
            */
            i += pbyMessage_len;
            // Append ETX
            pbyBuf[i++] = ETX;

            // Calculate CRC
            CRC = CalcCRC(ref pbyBuf);

            // CRC1 (lowbyte)
            mask = 0x00FF;
            tmp = CRC;
            tmp &= mask;
            CRC1 = (byte)tmp;

            // CRC2 (highbyte)
            tmp = (UInt16)(CRC >> 8);
            CRC2 = (byte)tmp;

            // Append CRC
            pbyBuf[i++] = CRC1;
            pbyBuf[i++] = CRC2;

            // OK complete message built, send off
            CreditPort.Write(pbyBuf, 0, i);
            //            WriteFile(hSerial, pbyBuf, i, &dwBytesWritten, NULL);

            Thread.Sleep(50);
            while (RxBuffer[0] == 0x00);
// TODO  : put protection for timeouts


            bDone = false;
            while (!bDone)
            {
                // Wait for reception of ACK, NAK or WACK during Answer Timeout of 1500 ms
                dwBytesRead = (uint)RxBuffer.Length;

                if (dwBytesRead == 0)
                {
                    Console.WriteLine("PSWrite - Answer Timeout happened, resending\n");
                    CreditPort.Write(pbyBuf, 0, i);
//                   WriteFile(hSerial, pbyBuf, i, &dwBytesWritten, NULL);
                    continue;
                }

                c = (byte)RxBuffer[0];
                switch (c)
                {
                    case ACK:
                        // Read Sequence Number
                        // TODO : Fix this ReadFile
                        //                        ReadFile(hSerial, &c, 1, &dwBytesRead, NULL);

                        c = (byte)RxBuffer[1];
                        if (dwBytesRead > 0)
                        {
                            // Check Sequence Number
                            if (c == byCurrSeqNum)
                            {
                                bDone = true;
                            }
                            else
                            {
                                Console.WriteLine("PSWrite - Received incorrect message number %d, keep listening\n", c);
                            }
                        }
                        else
                        {
                            Console.WriteLine("PSWrite - ICT while expecting Sequence Number\n");
                        }

                        break;

                    case NAK:
                        Console.WriteLine("PSWrite - Received a NAK, resending\n");
                        CreditPort.Write(pbyBuf, 0, i);
//                        WriteFile(hSerial, pbyBuf, i, &dwBytesWritten, NULL);
                        break;

                    case WACK:
                        Console.WriteLine("PSWrite - Received a Wait_ACK, keep listening\n");
                        break;

                    default:
                        Console.WriteLine("PSWrite - Received unknown response %d, keep listening\n", c);
                        break;
                }
            }
        }

        public void PSRead(ref byte[] pbyMessage)
        {
            // Format:
            // STX | NUM | ... data bytes ... | ETX | CRC1 (lowbyte) | CRC2 (highbyte)

            byte[] pbyBuf = new byte[4096];
            byte[] pbyBufAck = new byte[3];
            UInt32 dwBytesRead;
            UInt32 dwBytesWritten;
            UInt32 i, j;
            byte c;
            int byCurrSeqNum;
            int byExpSeqNum;

            while (true)
            {
                Array.Clear(pbyBuf, 0, pbyBuf.Length);
                i = 0;

                dwBytesRead = 0;
                if (DataReceived == 1)
                {
                    if (dwBytesRead == 0)
                    {
                        continue;
                    }
                }

                c = (byte)RxBuffer[0];
                if (c != STX)
                {
                    Console.WriteLine("PSRead - Need STX but got %02X, discarding\n", c);
                    continue;
                }

                pbyBuf[i] = c;

                // Read in Sequence Number
// FIX
//                ReadFile(hSerial, &c, 1, &dwBytesRead, NULL);

                // Check if Inter Character Timer timeout occurred
                if (dwBytesRead == 0)
                {
                    Console.WriteLine("PSRead - ICT while expecting Sequence Number\n");
                    continue;
                }

                pbyBuf[++i] = c;

                // Read in remainder of message up to and including ETX using
                // Inter Character Timer timeout value
                do
                {
// FIX
//                    ReadFile(hSerial, &c, 1, &dwBytesRead, NULL);
                    pbyBuf[++i] = c;
                }
                while (pbyBuf[i] != ETX && (dwBytesRead > 0));

                // Check if Inter Character Timer timeout occurred
                if (dwBytesRead == 0)
                {
                    Console.WriteLine("PSRead - ICT while reading in bulk of message\n");
                    continue;
                }

                // Read in the two CRC bytes
                for (j = 0; j < 2; j++)
                {
// FIX
//                    ReadFile(hSerial, &c, 1, &dwBytesRead, NULL);
                    pbyBuf[++i] = c;
                    if (dwBytesRead == 0)
                        break;
                }

                // Check if Inter Character Timer timeout occurred
                if (dwBytesRead == 0)
                {
                    Console.WriteLine("PSRead - ICT while reading in CRC bytes\n");
                    continue;
                }

                // OK complete message received
                byCurrSeqNum = pbyBuf[1];

                //Check CRC Validity
                Array.Clear(pbyBufAck, 0, pbyBufAck.Length);

                if (CheckCRC(ref pbyBuf))
                {
                    // TODO : Fix
                    /*
                                    // Send ACK + Sequence Number
                                        sprintf(pbyBufAck, "%c%c", ACK, byCurrSeqNum);
                                        WriteFile(hSerial, pbyBufAck, 2, &dwBytesWritten, NULL);
                    */
                }
                else
                {
                    // TODO : Fix
                    /*
                        // Send NAK
                        sprintf(pbyBufAck, "%c", NAK, byCurrSeqNum);
                        WriteFile(hSerial, pbyBufAck, 1, &dwBytesWritten, NULL);
                    */
                    Console.WriteLine("PSRead - CRC check failed\n");
                    continue;
                }

                // Check Sequence Number Validity
                switch (byPrevSeqNum)
                {
                    case 254:
                        byExpSeqNum = 0;
                        break;

                    default:
                        byExpSeqNum = byPrevSeqNum + 1;
                        break;
                }

                if (byCurrSeqNum >= byExpSeqNum || byCurrSeqNum == 255)
                {
                    // Extract payload
                    ExtractPayload(pbyBuf, ref pbyMessage);

                    // Update Previous Sequence Number
                    byPrevSeqNum = byCurrSeqNum;

                    // Break out of loop
                    break;
                }
                else if (byCurrSeqNum == byPrevSeqNum)
                {
                    // Ignore when current same as previous
                    Console.WriteLine("PSRead - Message number same as previous, ignoring message\n");
                    break;
                }
                else
                {
                    // Doc says this is an "error" but says nothing about how to handle it...
                    Console.WriteLine("PSRead - Message number not expected\n");
                    break;
                }

            }
        }

        public bool CheckCRC(ref byte[] pbyBuf)
        {
            // Packet format:
            // STX | NUM | ... data bytes ... | ETX | CRC1 (lowbyte) | CRC2 (highbyte)
            // The CRC must be calculated over:
            // NUM | ... data bytes ... | ETX

            int i, j;
            byte[] pbyTest = new byte[4096];
            byte CRC1, CRC2;
            UInt16 recvCRC, calcCRC;

            Array.Clear(pbyTest, 0, pbyTest.Length);

            i = 0;
            j = 1;

            // Copy NUM
            pbyTest[i] = pbyBuf[j];

            // Copy data bytes
            do
            {
                i++;
                j++;

                pbyTest[i] = pbyBuf[j];
            }
            while (pbyBuf[j] != ETX);

            // Copy ETX byte
            pbyTest[i++] = ETX;

            // CRC1 (lowbyte)
            CRC1 = pbyBuf[++j];

            // CRC2 (highbyte)
            CRC2 = pbyBuf[++j];

            recvCRC = (UInt16)(CRC2 << 8);
            recvCRC += CRC1;

            // Calculate checksum
            AdvamCRC16 CRC16Calc = new AdvamCRC16();
            CRC16Calc.setPoly();
            calcCRC = CRC16Calc.getCRC16(ref pbyTest, i, 0);

            if (recvCRC == calcCRC)
            {
                Console.WriteLine("CRC match, OK\n");
                return true;
            }
            else
            {
                Console.WriteLine("CRC no match, FAIL\n");
                return false;
            }
        }

        public UInt16 CalcCRC(ref byte[] pbyBuf)
        {
            // Packet format:
            // STX | NUM | ... data bytes ... | ETX | CRC1 (lowbyte) | CRC2 (highbyte)
            // The CRC must be calculated over:
            // NUM | ... data bytes ... | ETX

            int i, j;
            byte[] pbyTest = new byte[4096];
            UInt16 calcCRC;

            Array.Clear(pbyTest, 0, pbyTest.Length);

            i = 0;
            j = 1;

            // Copy NUM
            pbyTest[i] = pbyBuf[j];

            // Copy data bytes
            do
            {
                i++;
                j++;

                pbyTest[i] = pbyBuf[j];
            }
            while (pbyBuf[j] != ETX);

            // Copy ETX byte
            pbyTest[i++] = ETX;

            // Calculate checksum
            AdvamCRC16 CRC16Calc = new AdvamCRC16();
            CRC16Calc.setPoly();
            calcCRC = CRC16Calc.getCRC16(ref pbyTest, i, 0);

            return calcCRC;
        }

        public void ExtractPayload(byte[] pbyBuf, ref byte[] pcPayLoad)
        {
            // Message format:
            // STX | NUM | ... data bytes ... | ETX | CRC1 (lowbyte) | CRC2 (highbyte)
            // Here we need to extract the data bytes

            int i, j;

            i = 0;
            j = 2;

            while (pbyBuf[j] != ETX)
            {
                pcPayLoad[i++] = pbyBuf[j++];
            }
        }
    }
}
 