//#include "CRC16.h"
using System;

namespace Lockers
{
    public class AdvamCRC16
    {
        UInt16[] crctab = new UInt16[256];

        const UInt16 DIN = 0x8005;
        const UInt16 CCITT = 0x1021;


        //        void setPoly(UInt16 poly)
        public void setPoly()
        {
            UInt16 poly = DIN;
            byte ch;
            UInt16 j, i, check, check2;

            for (i = 0; i < 256; i++)
            {
                check = 0;
                ch = (byte)i;
                for (j = 0; j < 8; j++)
                {
                    if ((check & 0x8000) == 0x8000)
                    {
                        check <<= 1;
                        if ((ch & 0x01) == 0)
                            check ^= poly;
                    }
                    else
                    {
                        check <<= 1;
                        if ((ch & 0x01) == 1)
                            check ^= poly;
                    }
                    ch >>= 1;
                }

                check2 = 0;
                for (j = 0; j < 16; j++)
                {
                    ch = (byte)(check & 1);
                    check >>= 1;
                    check2 <<= 1;
                    check2 |= ch;
                }
                crctab[i] = check2;
            }

            return;
        }

        public UInt16 getCRC16(ref byte[] mem, int length, UInt16 crcIn)
        {
            byte[] block = new byte[length];
            Array.Copy(mem, 0, block, 0, length);

            UInt16 crc = crcIn;

            int i;
            for (i = 0; i < length; i++)
            {
                // TODO : Fix
                //                crc = (crc >> 8) ^ crctab[(block[i] ^ crc) & 0xff];
                UInt16 CRCt = crctab[(block[i] ^ crc) & 0xff];
                crc = (UInt16)((UInt16)(crc >> 8) ^ CRCt);
            };

            return (crc);
        }
    }
}