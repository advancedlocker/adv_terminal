﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lockers
{
    public enum LockerSize
    {
        SMALL,
        MEDIUM,
        LARGE,
        XLARGE
    }
}
