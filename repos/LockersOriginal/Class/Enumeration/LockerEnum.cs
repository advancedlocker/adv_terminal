﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lockers
{
    public enum eLockerSize
    {
        SMALL,
        MEDIUM,
        LARGE,
        XLARGE,
        UMBRELLA,
    }

    public enum eLockerPayPeriod
    {
        HOURLY,
        BLOCK1,
        BLOCK2,
        BLOCK3,
        BLOCK4,
        BLOCK5,
        BLOCK6,
        BLOCK7,
        BLOCK8,
        BLOCK9,
        BLOCK10,
        DAILY,
    }

    public enum eLockerAvailability
    {
        FREE,
        RENTED,
        DISABLED,
        RESERVED
    };
}
