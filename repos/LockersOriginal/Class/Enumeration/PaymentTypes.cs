﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lockers
{
    public enum PaymentTypes
    {
        Cash,
        CreditCard,
        Token,
        StoreValue,
        EzLink,
        WeChat,
        OctopusCard,
        ApplePay,
        GooglePay
    }
}
