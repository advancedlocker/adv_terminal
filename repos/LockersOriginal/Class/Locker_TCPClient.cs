﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Lockers
{
    public class Locker_TCPClient : IDisposable
    {

        #region Consts/Default values
        const int DEFAULTTIMEOUT = 5000; //Default to 5 seconds on all timeouts
        const int RECONNECTINTERVAL = 2000; //Default to 2 seconds reconnect attempt rate
        #endregion
        #region Components, Events, Delegates, and CTOR
        //Timer used to detect receive timeouts
        private System.Timers.Timer tmrReceiveTimeout = new System.Timers.Timer();
        private System.Timers.Timer tmrSendTimeout = new System.Timers.Timer();
        private System.Timers.Timer tmrConnectTimeout = new System.Timers.Timer();
        public delegate void delDataReceived(Locker_TCPClient sender, object data);
        public event delDataReceived DataReceived;
        public delegate void delConnectionStatusChanged(Locker_TCPClient sender, ConnectionStatus status);
        public event delConnectionStatusChanged ConnectionStatusChanged;

        private string CRC16;
        private string sourcekey;
        private int session;
        private int locknum;
        private string lockcmd;

        /*              
                public string CRC16
                {
                    get { return this.crc16; }
                    set { this.crc16 = value; }
                }
        */
        public string SourceKey
        {
            get { return sourcekey; }
            set { sourcekey = value; }
        }
        public int SessionKey
        {
            get { return this.session; }
            set { this.session = value; }
        }

        public string LockerCommand
        {
            get { return this.lockcmd; }
            set { this.lockcmd = value; }
        }

        public enum ConnectionStatus
        {
            NeverConnected,
            Connecting,
            Connected,
            AutoReconnecting,
            DisconnectedByUser,
            DisconnectedByHost,
            ConnectFail_Timeout,
            ReceiveFail_Timeout,
            SendFail_Timeout,
            SendFail_NotConnected,
            Error
        }

        public Locker_TCPClient(IPAddress ip, int port, bool autoreconnect = true)
        {
            this._IP = ip;
            this._Port = port;
            this._AutoReconnect = autoreconnect;
            this._client = new TcpClient(AddressFamily.InterNetwork);
            this._client.NoDelay = true; //Disable the nagel algorithm for simplicity
            ReceiveTimeout = DEFAULTTIMEOUT;
            SendTimeout = DEFAULTTIMEOUT;
            ConnectTimeout = DEFAULTTIMEOUT;
            ReconnectInterval = RECONNECTINTERVAL;
            tmrReceiveTimeout.AutoReset = false;
            tmrReceiveTimeout.Elapsed += new System.Timers.ElapsedEventHandler(tmrReceiveTimeout_Elapsed);
            tmrConnectTimeout.AutoReset = false;
            tmrConnectTimeout.Elapsed += new System.Timers.ElapsedEventHandler(tmrConnectTimeout_Elapsed);
            tmrSendTimeout.AutoReset = false;
            tmrSendTimeout.Elapsed += new System.Timers.ElapsedEventHandler(tmrSendTimeout_Elapsed);
            
            sourcekey = Properties.Settings.Default.sourceKey;
            ConnectionState = ConnectionStatus.NeverConnected;
        }
        #endregion
        #region Private methods/Event Handlers
        void tmrSendTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.ConnectionState = ConnectionStatus.SendFail_Timeout;
            DisconnectByHost();
        }
        void tmrReceiveTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.ConnectionState = ConnectionStatus.ReceiveFail_Timeout;
            DisconnectByHost();
        }
        void tmrConnectTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ConnectionState = ConnectionStatus.ConnectFail_Timeout;
            DisconnectByHost();
        }
        private void DisconnectByHost()
        {
            this.ConnectionState = ConnectionStatus.DisconnectedByHost;
            tmrReceiveTimeout.Stop();
            if (AutoReconnect)
                Reconnect();
        }
        private void Reconnect()
        {
            if (this.ConnectionState == ConnectionStatus.Connected)
                return;
            this.ConnectionState = ConnectionStatus.AutoReconnecting;
            try
            {
                this._client.Client.BeginDisconnect(true, new AsyncCallback(cbDisconnectByHostComplete), this._client.Client);
            }
            catch { }
        }
        #endregion
        #region Public Methods
        /// <summary>
        /// Try connecting to the remote host
        /// </summary>
        public void Connect()
        {
            if (ConnectionState == ConnectionStatus.Connected)
                return;
            ConnectionState = ConnectionStatus.Connecting;
            tmrConnectTimeout.Start();
            this._client.BeginConnect(this._IP, this._Port, new AsyncCallback(cbConnect), this._client.Client);
        }
        /// <summary>
        /// Try disconnecting from the remote host
        /// </summary>
        public void Disconnect()
        {
            if (ConnectionState != ConnectionStatus.Connected)
                return;
            this._client.Client.BeginDisconnect(true, new AsyncCallback(cbDisconnectComplete), this._client.Client);
        }
        /// <summary>
        /// Try sending a string to the remote host
        /// </summary>
        /// <param name="data">The data to send</param>
        public void Send(string data)
        {
            if (this.ConnectionState != ConnectionStatus.Connected)
            {
                ConnectionState = ConnectionStatus.SendFail_NotConnected;
                Globals.LogFile("Locker_TCPClient().TCPIP Send() - ConnectionState = " + ConnectionState + "\n");
                return;
            }

            var bytes = _encode.GetBytes(data);
            Globals.LogFile("Locker_TCPClient().TCPIP Send() - data = " + data + " \n");

            SocketError err = new SocketError();
            tmrSendTimeout.Start();

			try
			{
				_client.Client.BeginSend(bytes, 0, bytes.Length, SocketFlags.None, out err, new AsyncCallback(cbSendComplete), _client.Client);
			}
			catch(Exception ex)
			{
				Globals.LogFile(ex.Message);
				_client.Client.Disconnect(true);
				_client.Client.Connect(this.IP, this.Port);
				_client.Client.BeginSend(bytes, 0, bytes.Length, SocketFlags.None, out err, new AsyncCallback(cbSendComplete), _client.Client);
			}


			if (err != SocketError.Success)
            {
                Globals.LogFile("Locker_TCPClient().TCPIP Send() - doDCHost \n");
                Action doDCHost = new Action(DisconnectByHost);
                doDCHost.Invoke();
            }
        }
        /// <summary>
        /// Try sending byte data to the remote host
        /// </summary>
        /// <param name="data">The data to send</param>
        public void Send(byte[] data)
        {
            if (this.ConnectionState != ConnectionStatus.Connected)
                throw new InvalidOperationException("Locker_TCPClient().Send()  Cannot send data, socket is not connected");
            SocketError err = new SocketError();

//            this._client.Client.Send(data);

			try
			{
				this._client.Client.BeginSend(data, 0, data.Length, SocketFlags.None, out err, new AsyncCallback(cbSendComplete), this._client.Client);
			}
			catch (Exception ex)
			{
				Globals.LogFile(ex.Message);
				_client.Client.Disconnect(true);
				_client.Client.Connect(this.IP, this.Port);
				this._client.Client.BeginSend(data, 0, data.Length, SocketFlags.None, out err, new AsyncCallback(cbSendComplete), this._client.Client);
			}

			if (err != SocketError.Success)
            {
                Action doDCHost = new Action(DisconnectByHost);
                doDCHost.Invoke();
            }
        }
        public void Dispose()
        {
            this._client.Close();
            try
            {
                this._client.Client.Dispose();
            }
            catch { }
        }
        #endregion
        #region Callbacks
        private void cbConnectComplete()
        {
            if (_client.Connected == true)
            {
                tmrConnectTimeout.Stop();
                ConnectionState = ConnectionStatus.Connected;
                this._client.Client.BeginReceive(this.dataBuffer, 0, this.dataBuffer.Length, SocketFlags.None, new AsyncCallback(cbDataReceived), this._client.Client);
            }
            else
            {
                ConnectionState = ConnectionStatus.Error;
            }
        }
        private void cbDisconnectByHostComplete(IAsyncResult result)
        {
            var r = result.AsyncState as Socket;
            if (r == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
            r.EndDisconnect(result);
            if (this.AutoReconnect)
            {
                Action doConnect = new Action(Connect);
                doConnect.Invoke();
                return;
            }
        }
        private void cbDisconnectComplete(IAsyncResult result)
        {
            var r = result.AsyncState as Socket;
            if (r == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
            r.EndDisconnect(result);
            this.ConnectionState = ConnectionStatus.DisconnectedByUser;

        }
        private void cbConnect(IAsyncResult result)
        {
            var sock = result.AsyncState as Socket;
            if (result == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
            if (!sock.Connected)
            {
                if (AutoReconnect)
                {
                    System.Threading.Thread.Sleep(ReconnectInterval);
                    Action reconnect = new Action(Connect);
                    reconnect.Invoke();
                    return;
                }
                else
                    return;
            }
            sock.EndConnect(result);
            var callBack = new Action(cbConnectComplete);
            callBack.Invoke();
        }
        private void cbSendComplete(IAsyncResult result)
        {
            Globals.LogFile("Locker_TCPClient().cbSendComplete() called \n");
            var r = result.AsyncState as Socket;
            if (r == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");
            SocketError err = new SocketError();
            r.EndSend(result, out err);
            if (err != SocketError.Success)
            {
                Action doDCHost = new Action(DisconnectByHost);
                doDCHost.Invoke();
            }
            else
            {
                lock (SyncLock)
                {
                    tmrSendTimeout.Stop();
                }
            }
        }
        private void cbChangeConnectionStateComplete(IAsyncResult result)
        {
            var r = result.AsyncState as Locker_TCPClient;
            if (r == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a EDTC object");
            r.ConnectionStatusChanged.EndInvoke(result);
        }

        public void cbDataReceived(IAsyncResult result)
        {
            var sock = result.AsyncState as Socket;
            if (sock == null)
                throw new InvalidOperationException("Invalid IASyncResult - Could not interpret as a socket");
            try
            {
                Globals.LogFile("Locker_TCPClient().cbDataReceived(0) called Socket= " + sock.LocalEndPoint.ToString() + "\n");

                SocketError err = new SocketError();

                int bytes = sock.EndReceive(result, out err);
                if (bytes == 0 || err != SocketError.Success)
                {
                    Globals.LogFile("Locker_TCPClient().cbDataReceived(1) bytes = 0 OR not succcess\n");
                    lock (SyncLock)
                    {
                        tmrReceiveTimeout.Start();
                        return;
                    }
                }
                else
                {
                    lock (SyncLock)
                    {
                        tmrReceiveTimeout.Stop();
                    }
                }

                if (DataReceived != null)
                {
//                    string strData = System.Text.Encoding.UTF8.GetString(dataBuffer, 0, dataBuffer.Length);
                    var strData = (Encoding.Default.GetString(dataBuffer, 0, dataBuffer.Length - 1)).Split(new string[] { "\r\n", "\r", "\n", "\0", null }, StringSplitOptions.RemoveEmptyEntries);
                    dataBuffer = new byte[5000];
                    Globals.LogFile("Locker_TCPClient().cbDataReceived(2) - data = " + strData[0] + "\n");

                    switch (this.LockerCommand)
                    {
                        case Locker_cmd.Basic.GetSession:
                            {
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.AssignLock:
                            {
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.UnassignLock:
                            {
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.AvailableLock:
                            {
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.UsedLock:
                            {
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.OpenLock:
                            {
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.CloseLock:
                            {
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        case Locker_cmd.Basic.LockStatus:
                            {
                                int sKey;
                                int.TryParse(strData[0], out sKey);
                                this.SessionKey = sKey;
                            }
                            break;

                        default:
                            break;
                    }
                    DataReceived.BeginInvoke(this, _encode.GetString(dataBuffer, 0, bytes), new AsyncCallback(cbDataRecievedCallbackComplete), this);
                }
                else
                    Globals.LogFile("Locker_TCPClient().cbDataReceived(3) - data =  null\n");
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());               
            }
        }
        private void cbDataRecievedCallbackComplete(IAsyncResult result)
        {
            var r = result.AsyncState as Locker_TCPClient;
            if (r == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as EDTC object");
            r.DataReceived.EndInvoke(result);
            SocketError err = new SocketError();
            this._client.Client.BeginReceive(this.dataBuffer, 0, this.dataBuffer.Length, SocketFlags.None, out err, new AsyncCallback(cbDataReceived), this._client.Client);
            if (err != SocketError.Success)
            {
                Action doDCHost = new Action(DisconnectByHost);
                doDCHost.Invoke();
            }
        }
        #endregion
        #region Properties and members
        private IPAddress _IP = IPAddress.None;
        private static ConnectionStatus _ConStat;
        private TcpClient _client;
        private byte[] dataBuffer = new byte[5000];
        private bool _AutoReconnect = false;
        private int _Port = 0;
        private static Encoding _encode = Encoding.Default;
        object _SyncLock = new object();
        /// <summary>
        /// Syncronizing object for asyncronous operations
        /// </summary>
        public object SyncLock
        {
            get
            {
                return _SyncLock;
            }
        }
        /// <summary>
        /// Encoding to use for sending and receiving
        /// </summary>
        public Encoding DataEncoding
        {
            get
            {
                return _encode;
            }
            set
            {
                _encode = value;
            }
        }
        /// <summary>
        /// Current state that the connection is in
        /// </summary>
        public ConnectionStatus ConnectionState
        {
            get
            {
                return _ConStat;
            }
            private set
            {
                bool raiseEvent = value != _ConStat;
                _ConStat = value;
                if (ConnectionStatusChanged != null && raiseEvent)
                    ConnectionStatusChanged.BeginInvoke(this, _ConStat, new AsyncCallback(cbChangeConnectionStateComplete), this);
            }
        }
        /// <summary>
        /// True to autoreconnect at the given reconnection interval after a remote host closes the connection
        /// </summary>
        public bool AutoReconnect
        {
            get
            {
                return _AutoReconnect;
            }
            set
            {
                _AutoReconnect = value;
            }
        }
        public int ReconnectInterval { get; set; }
        /// <summary>
        /// IP of the remote host
        /// </summary>
        public IPAddress IP
        {
            get
            {
                return _IP;
            }
        }
        /// <summary>
        /// Port to connect to on the remote host
        /// </summary>
        public int Port
        {
            get
            {
                return _Port;
            }
        }
        /// <summary>
        /// Time to wait after a receive operation is attempted before a timeout event occurs
        /// </summary>
        public int ReceiveTimeout
        {
            get
            {
                return (int)tmrReceiveTimeout.Interval;
            }
            set
            {
                tmrReceiveTimeout.Interval = (double)value;
            }
        }
        /// <summary>
        /// Time to wait after a send operation is attempted before a timeout event occurs
        /// </summary>
        public int SendTimeout
        {
            get
            {
                return (int)tmrSendTimeout.Interval;
            }
            set
            {
                tmrSendTimeout.Interval = (double)value;
            }
        }
        /// <summary>
        /// Time to wait after a connection is attempted before a timeout event occurs
        /// </summary>
        public int ConnectTimeout
        {
            get
            {
                return (int)tmrConnectTimeout.Interval;
            }
            set
            {
                tmrConnectTimeout.Interval = (double)value;
            }
        }
        #endregion

        #region controller
        /// <summary>
        /// get sessionkey
        /// The current session key (32Bytes) for the approved access device
        /// </summary>
        /// <param name="sourcekey"></param>
        public void GetSession()
        {
            //            lockcmd = Locker_cmd.Basic.GetSession;
            lockcmd = Locker_cmd.Basic.GetSession;
            Send(Locker_cmd.Basic.GetSession + "," + sourcekey); 
        }
        /// <summary>
        /// This assigns a passcode to the locker
        /// Note VerificationType is a dont care field currently.
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="lockNum"></param>
        /// <param name="Type"></param>
        /// <param name="verifyData"></param>
        /// <param name="CRC16"></param>
        public void AssignLock(int lockNum, int Type, int verifyData)
        {
            lockcmd = Locker_cmd.Basic.AssignLock;
            GetSession();
            string command = Locker_cmd.Basic.AssignLock + "," + SessionKey + "," + lockNum + "," + Type + "," + verifyData + ",";
            string CRC16 = CalculateCRC16(command);
            Send(command + CRC16);
        }
        /// <summary>
        /// This removes a passcode from the locker
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="lockNum"></param>
        /// <param name="CRC16"></param>
        public void Unassignlock(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.UnassignLock;
            GetSession();
            string command = Locker_cmd.Basic.UnassignLock + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }
        /// <summary>
        /// This gets all available (free) locks from the locker controller
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="CRC16"></param>
//        public void GetAvailableLocks(int sessionkey, string CRC16)
        public void GetAvailableLocks()
        {
            lockcmd = Locker_cmd.Basic.AvailableLock;
//            GetSession();

//            while (this.session == 0) ;

            string command = Locker_cmd.Basic.AvailableLock + "," + this.SessionKey + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }
        /// <summary>
        /// This gets all unavailable (used) locks from the locker controller
        /// E.g.GetUsedLocks,123456789,12
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="CRC16"></param>
//        public void GetUsedLocks(int sessionkey)
        public void GetUsedLocks()
        {
            lockcmd = Locker_cmd.Basic.UsedLock;
            GetSession();
            string command = Locker_cmd.Basic.UsedLock + "," + this.SessionKey + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }
        /// <summary>
        /// This opens the lock in the selected locker number which would then open the door (Locker 5 in the example)
        /// E.g. 'OpenLock,123456789,5,12'
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="LockNumber"></param>
        /// <param name="CRC16"></param>
//        public void OpenLock(int sessionkey, int lockNum)
        public void OpenLock(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.OpenLock;
            GetSession();
            string command = Locker_cmd.Basic.OpenLock + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }
        /// <summary>
        /// This closes/latches the lock in the selected locker number which would then lock the door (Locker 5 in the example)
        /// E.g.  'CloseLock,123456789,5,12'
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="lockNum"></param>
        /// <param name="CRC16"></param>
        public void CloseLock(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.CloseLock;
            GetSession();
            string command = Locker_cmd.Basic.CloseLock + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }
        /// <summary>
        /// This returns the open or closed state of the lock in the selected locker number (Locker 5 in the example)
        /// E.g.GetLockStatus,123456789,5,12
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="lockNum"></param>
        /// <param name="CRC16"></param>
//        public void GetLockStatus(int sessionkey, int lockNum)
        public void GetLockStatus(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.LockStatus;
            GetSession();
            string command = Locker_cmd.Basic.LockStatus + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
            
        }
        /// <summary>
        /// This returns the ON or OFF state of the indicator LED on the selected locker number
        /// </summary>
        /// <param name="sessionkey"></param>
        /// <param name="lockNum"></param>
        /// <param name="CRC16"></param>
//        public void GetLEDStatus(int sessionkey, int lockNum)
        public void GetLEDStatus(int lockNum)
        {
            lockcmd = Locker_cmd.Basic.LockStatus;
            GetSession();
            string command = Locker_cmd.Basic.LEDStatus + "," + this.SessionKey + "," + lockNum + ",";
            string CRC16 = CalculateCRC16(command);
            this.Send(command + CRC16);
        }

        private string CalculateCRC16(string data)
        {
            return "12";
        }
        #endregion
    }
}
