/*#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "windows.h"

#include "ProtocolSerial.h"
#include "ProtocolMessages.h"

static void Serialize(TERMINAL_REQ* pReq, BYTE* pbyMsg);
static void SerializeInitReq(TERMINAL_REQ* pReq, BYTE* pbyMsg);
static void SerializeLinkTestReq(TERMINAL_REQ* pReq, BYTE* pbyMsg);
static void SerializePmtDetReq(TERMINAL_REQ* pReq, BYTE* pbyMsg);
static void SerializePmtReq(TERMINAL_REQ* pReq, BYTE* pbyMsg);
static void SerializePmtConf(TERMINAL_REQ* pReq, BYTE* pbyMsg);
static void SerializeEjectReq(TERMINAL_REQ* pReq, BYTE* pbyMsg);
static void SerializeDisableReq(TERMINAL_REQ* pReq, BYTE* pbyMsg);
static void DeSerialize(BYTE* pbyMsg, TERMINAL_RES* pRes);
static WORD ConstructTLV(BYTE* pbyTag, BYTE* pbyValue, BYTE* pbyTLV);
static void ConvertStr2Cmd(BYTE* pbyStr, TERMINAL_CMD* pCmd);
*/

using System;

namespace Lockers
{
    public class AdvamProtocolMessage
    {
        const int CUR_VERSN = 1;

        const string STR_INIT_REQ = "INIT_REQ";
        const string STR_LINK_REQ = "LINK_REQ";
        const string STR_TKTREAD_REQ = "TKTREAD_REQ";
        const string STR_PMTDET_REQ = "PMTDET_REQ";
        const string STR_PAYMENT_REQ = "PMT_REQ";
        const string STR_PAYMENT_CONF = "PMT_CONF";
        const string STR_EJECT_REQ = "EJECT_REQ";
        const string STR_DISABLE_REQ = "DISABLE_REQ";
        const string STR_SETT_REQ = "SETT_REQ";
        const string STR_DISP_REQ = "DISP_REQ";

        const string STR_PMTDET_CAN = "PMTDET_CAN";
        const string STR_TKTREAD_CAN = "TKTREAD_CAN";
        const string STR_EJECT_CAN = "EJECT_CAN";

        const string STR_INIT_RES = "INIT_RES";
        const string STR_LINK_RES = "LINK_RES";
        const string STR_TKTREAD_RES = "TKTREAD_RES";
        const string STR_PMTDET_RES = "PMTDET_RES";
        const string STR_PAYMENT_RES = "PMT_RES";
        const string STR_EJECT_RES = "EJECT_RES";
        const string STR_DISABLE_RES = "DISABLE_RES";
        const string STR_SETT_RES = "SETT_RES";

        const string TAG_MSGID = "MSGID";
        const string TAG_VERSN = "VERSN";
        const string TAG_STATS = "STATS";
        const string TAG_AMNTX = "AMTTX";
        const string TAG_SURCH = "SURCH";
        const string TAG_TMOUT = "TMOUT";
        const string TAG_EQUIP = "EQUIP";
        const string TAG_TXREF = "TXREF";
        const string TAG_CRDHA = "CRDHA";
        const string TAG_CRDTD = "CRDTD";
        const string TAG_PRFIX = "PRFIX";
        const string TAG_ISPAY = "ISPAY";
        const string TAG_DISID = "DISID";
        const string TAG_DTEXT = "DTEXT";
        const string TAG_RCODE = "RCODE";
        const string TAG_RTEXT = "RTEXT";
        const string TAG_TSTAN = "TSTAN";
        const string TAG_TCARD = "TCARD";
        const string TAG_RECPT = "RECPT";
        const string TAG_SCHID = "SCHID";
        const string TAG_CTYPE = "CTYPE";
        const string TAG_TAUTH = "TAUTH";

        AdvamProtocolSerial PS;

        public void PMInit()
        {
            PS = new AdvamProtocolSerial();
            Console.WriteLine("vPMInit -\n");
            PS.PSInit();
        }

        /*
            void PMDispose()
            {
                PSDispose();
            }
    */
        public byte PMGetVersion()
        {
            return CUR_VERSN;
        }

        public void PMSend(ref AdvamProtocolSerial.TERMINAL_REQ_s pReq)
        {
            byte[] pbyMsg = new byte[4096];

            Array.Clear(pbyMsg, 0, pbyMsg.Length);

            Serialize(ref pReq, pbyMsg);
            Console.WriteLine("Sending: %s\n", pbyMsg);
            AdvamProtocolSerial PS = new AdvamProtocolSerial();
            PS.PSWrite(pbyMsg);
        }

        public void PMRecv(ref AdvamProtocolSerial.TERMINAL_RES_s pRes)
        {
            byte[] pbyMsg = new byte[4096];

            Array.Clear(pbyMsg, 0, pbyMsg.Length);

            AdvamProtocolSerial PS = new AdvamProtocolSerial();
            PS.PSRead(ref pbyMsg);
            Console.WriteLine("Received: %s\n", pbyMsg);
            DeSerialize(pbyMsg, ref pRes);
        }

        public void Serialize(ref AdvamProtocolSerial.TERMINAL_REQ_s pReq, byte[] pbyMsg)
        {
            switch (pReq.Cmd)
            {
                case AdvamProtocolSerial.TERMINAL_CMD.CMD_INIT:
                    SerializeInitReq(ref pReq, pbyMsg);
                    break;

                case AdvamProtocolSerial.TERMINAL_CMD.CMD_LINK_TEST:
                    SerializeLinkTestReq(ref pReq, pbyMsg);
                    break;

                case AdvamProtocolSerial.TERMINAL_CMD.CMD_PAYMENT_CARD_DET:
                    SerializePmtDetReq(ref pReq, pbyMsg);
                    break;

                case AdvamProtocolSerial.TERMINAL_CMD.CMD_PAYMENT:
                    SerializePmtReq(ref pReq, pbyMsg);
                    break;

                case AdvamProtocolSerial.TERMINAL_CMD.CMD_PAYMENT_CONF:
                    SerializePmtConf(ref pReq, pbyMsg);
                    break;

                case AdvamProtocolSerial.TERMINAL_CMD.CMD_CARD_EJECT:
                    SerializeEjectReq(ref pReq, pbyMsg);
                    break;

                case AdvamProtocolSerial.TERMINAL_CMD.CMD_DISABLE_READER:
                    SerializeDisableReq(ref pReq, pbyMsg);
                    break;

                default:
                    break;
            }
        }

        public void SerializeInitReq(ref AdvamProtocolSerial.TERMINAL_REQ_s pReq, byte[] pbyMsg)
        {
            byte[] pbyValue = new  byte[16];
            byte[] pbyTLV;

            pbyTLV = pbyMsg;

            // TAG_MSGID	
            ConstructTLV(TAG_MSGID, STR_INIT_REQ, ref pbyTLV);

            // TAG_VERSN
//            sprintf(pbyValue, "%d", pReq->byVersion);
            ConstructTLV(TAG_VERSN, pReq.byVersion.ToString(), ref pbyTLV);
        }

        public void SerializeLinkTestReq(ref AdvamProtocolSerial.TERMINAL_REQ_s pReq, byte[] pbyMsg)
        {
            byte[] pbyTLV;

            pbyTLV = pbyMsg;

            // TAG_MSGID	
            ConstructTLV(TAG_MSGID, STR_LINK_REQ, ref pbyTLV);
        }

        public void SerializePmtDetReq(ref AdvamProtocolSerial.TERMINAL_REQ_s pReq, byte[] pbyMsg)
        {
            byte[] pbyValue = new byte[16];
            byte[] pbyTLV;

            pbyTLV = pbyMsg;

            // TAG_MSGID	
            ConstructTLV(TAG_MSGID, STR_PMTDET_REQ, ref pbyTLV);

            // TAG_TMOUT
//            sprintf(pbyValue, "%d", pReq->byTimeout);
            ConstructTLV(TAG_TMOUT, pReq.byTimeout.ToString(), ref pbyTLV);

            // TAG_AMNTX
            ConstructTLV(TAG_AMNTX, pReq.pbyAmount, ref pbyTLV);

            // TAG_SURCH
            ConstructTLV(TAG_SURCH, pReq.pbySurcharge, ref pbyTLV);
        }

        public void SerializePmtReq(ref AdvamProtocolSerial.TERMINAL_REQ_s pReq, byte[] pbyMsg)
        {
            byte[] pbyTLV;

            pbyTLV = pbyMsg;

            // TAG_MSGID	
            ConstructTLV(TAG_MSGID, STR_PAYMENT_REQ, ref pbyTLV);

            // TAG_AMNTX
            ConstructTLV(TAG_AMNTX, pReq.pbyAmount, ref pbyTLV);

            // TAG_SURCH
            ConstructTLV(TAG_SURCH, pReq.pbySurcharge, ref pbyTLV);

            // TAG_EQUIP
            ConstructTLV(TAG_EQUIP, pReq.pbyEquipId, ref pbyTLV);

            // TAG_TXREF
            ConstructTLV(TAG_TXREF, pReq.pbyTxRef, ref pbyTLV);
        }

        public void SerializePmtConf(ref AdvamProtocolSerial.TERMINAL_REQ_s pReq, byte[] pbyMsg)
        {
            byte[] pbyValue = new byte[16];
            byte[] pbyTLV;

            pbyTLV = pbyMsg;

            // TAG_MSGID	
//            pbyTLV += ConstructTLV(TAG_MSGID, STR_PAYMENT_CONF, ref pbyTLV);
            ConstructTLV(TAG_MSGID, STR_PAYMENT_CONF, ref pbyTLV);

            // TAG_TSTAN
//            pbyTLV += ConstructTLV(TAG_TSTAN, pReq->pbyStan, pbyTLV);
            ConstructTLV(TAG_TSTAN, pReq.pbyStan, ref pbyTLV);

            // TAG_STATS
            //            sprintf(pbyValue, "%d", pReq->Stats);
            ConstructTLV(TAG_STATS, pReq.Stats.ToString(), ref pbyTLV);
        }

        public void SerializeEjectReq(ref AdvamProtocolSerial.TERMINAL_REQ_s pReq, byte[] pbyMsg)
        {
            byte[] pbyValue = new byte[16];
            byte[] pbyTLV;

            pbyTLV = pbyMsg;

            // TAG_MSGID	
//            pbyTLV += ConstructTLV(TAG_MSGID, STR_EJECT_REQ, ref pbyTLV);
            ConstructTLV(TAG_MSGID, STR_EJECT_REQ, ref pbyTLV);

            // TAG_TMOUT
            //            sprintf(pbyValue, "%d", pReq->byTimeout);
            ConstructTLV(TAG_TMOUT, pReq.byTimeout.ToString(), ref pbyTLV);
        }

        public void SerializeDisableReq(ref AdvamProtocolSerial.TERMINAL_REQ_s pReq, byte[] pbyMsg)
        {
            byte[] pbyTLV;

            pbyTLV = pbyMsg;

            // TAG_MSGID	
            ConstructTLV(TAG_MSGID, STR_DISABLE_REQ, ref pbyTLV);
        }

        public void DeSerialize(byte[] pbyMsg, ref AdvamProtocolSerial.TERMINAL_RES_s pRes)
        {
            byte[] pbyBuf;
            byte[] pbyTag = new byte[5 + 1];
            byte[] pbyLen = new byte[3 + 1];
            byte[] pbyValue = new  byte[2048];
            Int16 dwLen;
            UInt32 i;

            i = 0;
            pbyBuf = pbyMsg;

            while (pbyBuf[i] != 0x00)
            {
                Array.Clear(pbyTag, 0, pbyTag.Length);
                Array.Clear(pbyLen, 0, pbyLen.Length);
                Array.Clear(pbyValue, 0, pbyValue.Length);

                // Get tag
                Array.Copy(pbyBuf, 0, pbyTag, 0, 5);
//                pbyBuf += 5;
                string pbyTagSt = System.Text.Encoding.UTF8.GetString(pbyBuf);
                Console.WriteLine("Found tag: %s\n", pbyTag);

                // Get length (as string)
                Array.Copy(pbyBuf, 6, pbyLen, 0, 3);
                Console.WriteLine("With length (as string): %s\n", pbyLen);

                string Stdwlen = System.Text.Encoding.UTF8.GetString(pbyLen);
                dwLen = Convert.ToInt16(Stdwlen);
                Console.WriteLine("With length (as number): %d\n", dwLen);

                // Get value
                Array.Copy(pbyBuf, 9, pbyLen, 0, dwLen);
//                strncpy(pbyValue, pbyBuf, dwLen);
//                pbyBuf += dwLen;
                Console.WriteLine("With value: %s\n", pbyValue);

                if (pbyTagSt == TAG_MSGID)
                {
                    ConvertStr2Cmd(pbyValue, ref pRes.Cmd);
                }
                else if (pbyTagSt == TAG_VERSN)
                {
                    pRes.byVersion = pbyValue[0];
                }
                //else if(pbyTagSt ==  TAG_AMNTX)
                //{
                //}
                //else if(pbyTagSt ==  TAG_SURCH)
                //{
                //}
                //else if(pbyTagSt ==  TAG_EQUIP)
                //{
                //}
                //else if(pbyTagSt ==  TAG_TXREF)
                //{
                //}
                else if (pbyTagSt == TAG_CRDHA)
                {
                }
                else if (pbyTagSt == TAG_CRDTD)
                {
                }
                else if (pbyTagSt == TAG_PRFIX)
                {
                }
                else if (pbyTagSt == TAG_ISPAY)
                {
                }
                else if (pbyTagSt == TAG_DISID)
                {
                    pRes.byDisplayId = pbyValue[0];
                }
                else if (pbyTagSt == TAG_DTEXT)
                {
                    pRes.pbyDisplayText = pbyValue;
                }
                else if (pbyTagSt == TAG_RCODE)
                {
                    pRes.pbyResponseCode = pbyValue;
                }
                else if (pbyTagSt == TAG_RTEXT)
                {
                    pRes.pbyResponseText = pbyValue;
                }
                else if (pbyTagSt == TAG_TSTAN)
                {
                    pRes.pbyStan = pbyValue;
                }
                else if (pbyTagSt == TAG_TCARD)
                {
// Fix                    pRes.pbyTruncCardData = pbyValue);
                }
                else if (pbyTagSt == TAG_RECPT)
                {
                    pRes.pbyReceipt = pbyValue;
                }
                else if (pbyTagSt == TAG_SCHID)
                {
                    pRes.pbySurchargeId = pbyValue;
                }
                else if (pbyTagSt == TAG_CTYPE)
                {
                    pRes.pbyCardType = pbyValue;
                }
                else if (pbyTagSt == TAG_TAUTH)
                {
                    pRes.pbyAuth = pbyValue;
                }
                else if (pbyTagSt == TAG_STATS)
                {
                    // TODO : Fix this
                    pRes.Stats = (AdvamProtocolSerial.TERMINAL_STATS)pbyValue[0];
                }
                else
                {
                    Console.WriteLine("Unknown tag found: %s\n", pbyTag);
                }
            }

            Console.WriteLine("Number of tags found: %d\n", i);
        }

        public UInt32 ConstructTLV(string pbyTag, string pbyValue, ref byte[] pbyTLV)
        {
            UInt32 wLen;

            wLen = (UInt32)pbyValue.Length;

// Fix
//            sprintf(pbyTLV, "%s%03x%s", pbyTag, wLen, pbyValue);

//            wLen = (WORD)strlen(pbyTLV);
            wLen = (UInt32)pbyTLV.Length;

            return wLen;
        }

        public void ConvertStr2Cmd(byte[] pbyStr, ref AdvamProtocolSerial.TERMINAL_CMD pCmd)
        {
            string TestStr = System.Text.Encoding.UTF8.GetString(pbyStr);
            if (TestStr == STR_INIT_RES)
            {
                pCmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_INIT;
            }
            else if (TestStr == STR_LINK_RES)
            {
                pCmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_LINK_TEST;
            }
            else if (TestStr == STR_TKTREAD_RES)
            {
                pCmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_TICKET_READ;
            }
            else if (TestStr == STR_PMTDET_RES)
            {
                pCmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_PAYMENT_CARD_DET;
            }
            else if (TestStr == STR_PAYMENT_RES)
            {
                pCmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_PAYMENT;
            }
            else if (TestStr == STR_EJECT_RES)
            {
                pCmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_CARD_EJECT;
            }
            else if (TestStr == STR_DISABLE_RES)
            {
                pCmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_DISABLE_READER;
            }
            else if (TestStr == STR_SETT_RES)
            {
                pCmd = AdvamProtocolSerial.TERMINAL_CMD.CMD_SETTLEMENT;
            }
            else
            {
                Console.WriteLine("Unknown string found: %s\n", pbyStr);
            }
        }
    }
}