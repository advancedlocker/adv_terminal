﻿

namespace Lockers
{
    using ITLlib;
    using System;
    using System.ComponentModel;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Windows.Forms;

    public class SmartPay: IDisposable
    {
        /// <summary>
        /// Background worker for the controller monitoring thread.
        /// </summary>
        private BackgroundWorker controllerMonitor;

        public Boolean StopMainLoop = false;

        SSPComms sspLib = new SSPComms();
        volatile public bool hopperRunning = false, NV11Running = false, payoutConnecting = false, payoutRunning = false;
        volatile bool hopperConnecting = false;
        volatile bool NV11Connecting = false;
        int pollTimer = 250; // timer in ms
        CHopper Hopper; // Class to interface with the Hopper
        CNV11 NV11; // Class to interface with the NV11
        CPayout SmartPayout; //class to interfce with smart hopper
        bool FormSetup = false;
        bool isDeviceInit = false;
        delegate void OutputMessage(string s);
        System.Timers.Timer timer1 = new System.Timers.Timer();

        string coinlevel;
        string notestoredinfo;
        decimal totalcoin = 0.00M;
        decimal totalnote = 0.00M;
        float payout = 0f;

        Boolean paymode = false;
        Boolean donepayout = false;
        Boolean inMaintenance = false;

        // Flag: Has Dispose already been called? 
        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new Microsoft.Win32.SafeHandles.SafeFileHandle(IntPtr.Zero, true);

        Thread tHopRec=null, tSPRec=null; // Handles to each of the reconnection threads for the units
        Thread tNV11Rec = null;

        #region events
        public event EventHandler<LogMessageEventArgs> LogMessageEvent;
        public event EventHandler<EventArgs> DeviceOn;
        public event EventHandler<EventArgs> DeviceInit;
        public event EventHandler<EventArgs> NVConnected;
        public event EventHandler<EventArgs> HopperConnected;
        public event EventHandler<EventArgs> Update_UI;
        public event EventHandler<EventArgs> Done_Payout;

        protected void OnLogMessageEvent(LogMessageEventArgs e)
        {
            EventHandler<LogMessageEventArgs> logmessageevent = this.LogMessageEvent;
            if (logmessageevent != null) logmessageevent(this, e);
        }

        protected void OnDeviceOn(EventArgs e)
        {
            EventHandler<EventArgs> deviceon = this.DeviceOn;
            if (deviceon != null)
            {
                deviceon(this, e);
            }
        }


        protected void OnDeviceINIT(EventArgs e)
        {
            EventHandler<EventArgs> deviceinit = this.DeviceInit;
            if (deviceinit != null)
            {
                deviceinit(this, e);
            }

        }

        protected void OnNVConnected(EventArgs e)
        {
            EventHandler<EventArgs> connected = this.NVConnected;
            if (connected != null) connected(this, e);
        }

        protected void OnHopperConnected(EventArgs e)
        {
            EventHandler<EventArgs> connected = this.HopperConnected;
            if (connected != null) connected(this, e);
        }

        protected void OnUpdate_UI(EventArgs e)
        {
            EventHandler<EventArgs> updateui = this.Update_UI;
            if (updateui != null) updateui(this, e);
        }

        protected void OnDone_Payout(EventArgs e)
        {
            EventHandler<EventArgs> donepay = this.Done_Payout;
            if (donepay != null) donepay(this, e);
        }

        #endregion

        #region properties

        public Boolean isDonePayout
        {
            get { return this.donepayout; }
        }


        public float Payout
        {
            set { this.payout = value; }
            get { return this.payout; }
        }

        public string CoinLevel
        {
            get { return coinlevel; }
        }
        public decimal TotalCoin
        {
            get { return totalcoin; }
        }

        public decimal TotalNote
        {
            get { return totalnote; }
        }
        public string NoteStoredInfo
        {
            get { return notestoredinfo; }
        }

        public CHopper HOPPER
        {
            get { return Hopper; }
        }
        public CNV11 VALIDATOR
        {
            get { return NV11; }
        }
        public CPayout PAYOUT
        {
            get { return SmartPayout; }
        }
        #endregion

        #region constructor
        public SmartPay()  //TextBox log =null 
        {
            Ini();

            timer1.Interval = pollTimer;
            timer1.Elapsed += reconnectionTimerEvent;

// SMC : Removed the threaded worker for the moment. 
/*
            // Initialize the controller monitoring background worker.
            this.controllerMonitor = new BackgroundWorker();
            this.controllerMonitor.WorkerSupportsCancellation = true;
            this.controllerMonitor.WorkerReportsProgress = true;
            this.controllerMonitor.DoWork += this.ControllerMonitor_DoWork;
            this.controllerMonitor.ProgressChanged += this.ControllerMonitor_ProgressChanged;
            this.controllerMonitor.RunWorkerCompleted += this.ControllerMonitor_RunWorkerCompleted;
*/
        }
        #endregion

        #region methods
        private void Ini()
        {
            // Create instances of the validator classes
            this.Hopper = new CHopper();
            this.NV11 = new CNV11();
            this.SmartPayout = new CPayout();
            if (Hopper == null || NV11 == null)
            {
                Globals.LogFile(DateTime.Now + "| Procedure:Ini() - error Hopper/NV11 instance exiting \r\n", "PaySystemLog.txt");
            }
        }

        public void Start()
        {
            try
            {
             
                this.controllerMonitor.RunWorkerAsync();
            }
            catch (Exception  ex)
            {

                Globals.LogFile( DateTime.Now +"\r\n"+ ex.Message + "\r\n; Start method: \r\n  ", "PaySystemLog.txt");
            }
        }

        private void ControllerMonitor_DoWork(object sender, DoWorkEventArgs e)
        {
            if (Properties.Settings.Default.EnableSmartPayout)
            {
                e.Result = this.MainLoop_();
            }
            else
            {
                e.Result = this.MainLoop();
            }
        }

        private void ControllerMonitor_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string statmsg = e.UserState.ToString();
            switch (statmsg)
            {
                case "HopperConnection":
                    Globals.SendCommsLogReport("Lost connection to SMART Hopper\r\n");
                    this.OnLogMessageEvent(new LogMessageEventArgs("Lost connection to SMART Hopper\r\n"));
                    break;
                case  "NVConnection":
                    Globals.SendCommsLogReport("Lost connection to NV11\r\n");
                    this.OnLogMessageEvent(new LogMessageEventArgs("Lost connection to NV11\r\n"));
                    break;
            }
        }

        private void ControllerMonitor_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // First, handle the case where an exception was thrown.
            if (e.Error != null)
            {
                MessageBox.Show(
                        "sorry but the application has encountered an error and needs to close."
                        + Environment.NewLine
                        + "Error: " + e.Error.Message ,
                        "Application Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                  Application.Exit();
            }
            else if (e.Cancelled)
            {
                // Next, handle the case where the operation was canceled
                // for some other reason.

            }
            if (!CHelpers.Shutdown)
            {
                CHelpers.Shutdown=true;
            }
        }

        public void StopPaysystem()
        {
            hopperRunning = false;
            NV11Running = false;
        }

        public void RunPaysystem()
        {
            hopperRunning = true;
            NV11Running = true;
        }

        public double CalculateCoinDifference(string level1, string level2)
        {
            double ammount = 0;

//            string level1 = "0.05 AUD [0] = 0.00 AUD\r\n0.10 AUD [1] = 0.10 AUD\r\n0.20 AUD [5] = 1.00 AUD\r\n1.00 AUD [2] = 2.00 AUD\r\n2.00 AUD [9] = 18.00 AUD\r\n";
//            string level2 = "0.05 AUD [1] = 0.05 AUD\r\n0.10 AUD [1] = 0.10 AUD\r\n0.20 AUD [5] = 1.00 AUD\r\n1.00 AUD [2] = 2.00 AUD\r\n2.00 AUD [10] = 20.00 AUD\r\n";

            string[] denom;
            string[] coins1;
            string[] coins2;

            denom = level1.Split('\n');

            coins1 = level1.Split('[');
            coins2 = level2.Split('[');

            int idx;
            for (int count = 0; count < denom.Length - 1; count++)
            {
                idx = denom[count].IndexOf(' ');
                denom[count] = denom[count].Remove(idx);
            }


            for (int count = 1; count < coins1.Length; count++)
            {
                idx = coins1[count].IndexOf(']');
                coins1[count] = coins1[count].Remove(idx);

                idx = coins2[count].IndexOf(']');
                coins2[count] = coins2[count].Remove(idx);

                ammount += Convert.ToDouble(denom[count - 1]) * (Convert.ToDouble(coins2[count]) - Convert.ToDouble(coins1[count]));
            }

            return (ammount);
        }

        public int CalculateCoinDifference(string level1, string level2, double coin_value)
        {
            //            coinlevel = "0.05 AUD [0] = 0.00 AUD\r\n0.10 AUD [1] = 0.10 AUD\r\n0.20 AUD [5] = 1.00 AUD\r\n1.00 AUD [2] = 2.00 AUD\r\n2.00 AUD [10] = 20.00 AUD\r\n"
            return (0);
        }

        public int SMC_MainLoop()
        {
            string OrgCoinlevel = "";
            SmartPayout.SetReferenceValue();

            hopperRunning = false;
            NV11Running = false;
            payoutRunning = false;

            // Connect to the validators
            Console.WriteLine("Connecting to devices");
            if (Properties.Settings.Default.EnableNV)
            {
                Console.WriteLine("Connecting to NV11");
                ConnectToNV11();
                NV11.EnableValidator();
                Console.WriteLine("Connect (NV) Finished\n");
                NV11Running = true;
            }

            if (Properties.Settings.Default.EnableSmartPayout)
            {
                Console.WriteLine("Connecting to Smart Payout");
                ConnectToSMARTPayout();
                SmartPayout.EnableValidator();
                Console.WriteLine("Connect (SP) Finished\n");
                payoutRunning = true;
            }

            if (Properties.Settings.Default.HopperEnabled)
            {
                Console.WriteLine("Connecting to Hopper");
                ConnectToHopper();
                Hopper.EnableValidator();
                OrgCoinlevel = Hopper.GetChannelLevelInfo();
                Console.WriteLine("Connect (HOP) Finished\n");
                hopperRunning = true;
            }

            // While application is still active
            double coinammt = 0;
            while ((!CHelpers.Shutdown) && (StopMainLoop == false))
            {
                // Setup form layout on first run
                //if (this.SmartpayMonitor.CancellationPending)
                //{
                //    break;
                //}

                if (!FormSetup)
                {
                    FormSetup = true;
                    //SetupFormLayout();
                    if (Properties.Settings.Default.HopperEnabled)
                    {
                        coinlevel = Hopper.GetChannelLevelInfo();
                    }
                    FormSetup = true;
                    this.OnDeviceOn(EventArgs.Empty);
                }

                // If the hopper is supposed to be running but the poll fails
                int trialcount = 100;
                while ((trialcount-- > 0) && (StopMainLoop == false) && (!CHelpers.Shutdown))
                {
                    Globals.LogFile("running NV11 and hopper " + trialcount.ToString() + "\n");
                    if (Properties.Settings.Default.HopperEnabled)
                    {
                        Globals.LogFile("Hopper Enabled\n");
                        if (hopperRunning)
                        {
                            if (!this.Hopper.DoPoll())
                            {
                                this.controllerMonitor.ReportProgress(0, "HopperConnection");
                                // this.OnLogMessageEvent(new LogMessageEventArgs("Lost connection to SMART Hopper\r\n"));
                                Globals.LogFile("Lost connection to SMART Hopper\r\n");

                                // If the other unit isn't running, refresh the port by closing it
                                Hopper.DisableValidator();
                                if (hopperRunning)
                                    this.Hopper.ClosePort();
                                hopperRunning = false;
                                tHopRec = new Thread(() => ReconnectHopper());
                                tHopRec.Name = "ReconnectHopper" + tHopRec.ManagedThreadId;
                                tHopRec.Start();
                            }
                            else
                            {
                                coinlevel = Hopper.GetChannelLevelInfo();
                                coinammt = CalculateCoinDifference(OrgCoinlevel, coinlevel);
                            }
                        }
                    }
                    // If the NV11 is supposed to be running but the poll fails
                    /*
                                        if (NV11Running && !NV11.DoPoll())
                                        {
                                            this.controllerMonitor.ReportProgress(0, "NVConnection");
                                            //this.OnLogMessageEvent(new LogMessageEventArgs("Lost connection to NV11\r\n"));
                                            Globals.LogFile("Lost connection to NV11\r\n");

                                            // If the other unit isn't running, refresh the port by closing it
                                            if (!hopperRunning) LibraryHandler.ClosePort();
                                            NV11Running = false;
                                            tNV11Rec = new Thread(() => ReconnectNV11());
                                            tNV11Rec.Name = "ReconnectNV11" + tNV11Rec.ManagedThreadId;
                                            tNV11Rec.Start();
                                        }
                    */
                    // If the SmartPayout is supposed to be running but the poll fails
                    byte[] Response = new byte[20];
                    int len = 0;
                    Thread.Sleep(5);

                    if (Properties.Settings.Default.EnableSmartPayout)
                    {
                        Globals.LogFile("Smart Payout Enabled\n");
                        if (payoutRunning)
                        {
                            if (!SmartPayout.DoPoll(ref Response, ref len))
                            {
                                Globals.LogFile("Lost connection to SmartPayout - restarting\r\n");
                                //                        payoutRunning = false;
                                SmartPayout.DisablePayout();
                                if (payoutRunning)
                                    this.SmartPayout.ClosePort();
                                payoutRunning = false;
                                ConnectToSMARTPayout();
                                Globals.smartpay.PAYOUT.EnableValidator();
                                /*
                                                        this.controllerMonitor.ReportProgress(0, "PayoutConnection");
                                                      //this.OnLogMessageEvent(new LogMessageEventArgs("Lost connection to SmartPayout\r\n"));

                                                        // If the other unit isn't running, refresh the port by closing it
                                                        if (!hopperRunning) LibraryHandler.ClosePort();
                                                        payoutRunning = false;
                                                        tSPRec = new Thread(() => ReconnectPayout());
                                                        tSPRec.Name = "ReconnectPayout" + tSPRec.ManagedThreadId;
                                                        tSPRec .Start();
                                */
                            }
                        }
                    }

                    try
                    {
                        if (Properties.Settings.Default.HopperEnabled)
                            Globals.LogFile("Hopper coin level = " + coinlevel.ToString() + ", enetered money" + coinammt.ToString() + "\n");

                        if (Properties.Settings.Default.EnableSmartPayout)
                            Globals.LogFile("SAmartPayout note level = " + ", enetered money" + PAYOUT.paidamnt.ToString() + "\n");
                    }
                    catch { }

                    if (Globals.DueAmnt <= (coinammt + PAYOUT.paidamnt))
                        StopMainLoop = true;


                    /*
                    UpdateUI();
                    System.Windows.Forms.Application.DoEvents();
                                        timer1.Enabled = true;
                                        if (!isDeviceInit)
                                        {
                                            if (Properties.Settings.Default.HopperEnabled)
                                            {
                                                coinlevel = Hopper.GetChannelLevelInfo();
                                            }
                                            isDeviceInit = true;
                                            this.OnDeviceINIT(EventArgs.Empty);
                                        }


                                        while (timer1.Enabled)
                                        {
                                            System.Windows.Forms.Application.DoEvents();
                                            Thread.Sleep(1);
                                        }
                    */

                    /*
                                        while ((timer1.Enabled) && (!CHelpers.Shutdown))
                                        {
                                            UpdateUI();
                                            System.Windows.Forms.Application.DoEvents();
                                        }
                    */
                    System.Windows.Forms.Application.DoEvents();
                    UpdateUI();
                }
            }

            StopMainLoop = false;
            CHelpers.Shutdown = false;

            //close com port
            if (hopperRunning)
            {
                Hopper.DisableValidator();
                this.Hopper.ClosePort();
            }

            if (NV11Running)
            {
                NV11.DisableValidator();
                this.NV11.ClosePort();
            }

            if (payoutRunning)
            {
                SmartPayout.DisablePayout();
                this.SmartPayout.ClosePort();
            }


            return 0;
        }



        // The main program loop, this is to control the validator, it polls at
        // a value set in this class (pollTimer).
        public int MainLoop()
        {


            // Connect to the validators
            ConnectToNV11();
            NV11.EnableValidator();

            if (Properties.Settings.Default.HopperEnabled)
            {
                ConnectToHopper();
                Hopper.EnableValidator();
            }
            else
            {
                hopperRunning = false;
            }

            // While application is still active
            while (!CHelpers.Shutdown)
            {
                // Setup form layout on first run
                //if (this.SmartpayMonitor.CancellationPending)
                //{
                //    break;
                //}

                if (!FormSetup)
                {
                    FormSetup = true;
                    //SetupFormLayout();
                    if (Properties.Settings.Default.HopperEnabled)
                    {
                        coinlevel = Hopper.GetChannelLevelInfo();
                    }
                    FormSetup = true;
                    this.OnDeviceOn(EventArgs.Empty);
                }

                // If the hopper is supposed to be running but the poll fails
                if (Properties.Settings.Default.HopperEnabled)
                {
                    if (hopperRunning && !this.Hopper.DoPoll())
                    {
                        this.controllerMonitor.ReportProgress(0, "HopperConnection");
                       // this.OnLogMessageEvent(new LogMessageEventArgs("Lost connection to SMART Hopper\r\n"));
                        Globals.LogFile("Lost connection to SMART Hopper\r\n");

                        // If the other unit isn't running, refresh the port by closing it
                        if (!hopperRunning)
                            this.Hopper.ClosePort();
                        hopperRunning = false;
                        tHopRec = new Thread(() => ReconnectHopper());
                        tHopRec.Name = "ReconnectHopper" + tHopRec.ManagedThreadId;
                        tHopRec.Start();
                    }
                }
                // If the NV11 is supposed to be running but the poll fails
                if (NV11Running && !NV11.DoPoll())
                {
                    this.controllerMonitor.ReportProgress(0, "NVConnection");
                    //this.OnLogMessageEvent(new LogMessageEventArgs("Lost connection to NV11\r\n"));
                    Globals.LogFile("Lost connection to NV11\r\n");

                    // If the other unit isn't running, refresh the port by closing it
                    if (!hopperRunning)
                        this.NV11.ClosePort();
                    NV11Running = false;
                    tNV11Rec = new Thread(() => ReconnectNV11());
                    tNV11Rec.Name = "ReconnectNV11" + tNV11Rec.ManagedThreadId;
                    tNV11Rec.Start();
                }

                UpdateUI();
                timer1.Enabled = true;
                if (!isDeviceInit)
                {
                    if (Properties.Settings.Default.HopperEnabled)
                    {
                        coinlevel = Hopper.GetChannelLevelInfo();
                    }
                    isDeviceInit = true;
                    this.OnDeviceINIT(EventArgs.Empty);
                }


                while (timer1.Enabled)
                {
                    System.Windows.Forms.Application.DoEvents();
                    Thread.Sleep(1);
                }

            }


            //close com port
            if (!hopperRunning)
                this.Hopper.ClosePort();

            if (!NV11Running)
                this.NV11.ClosePort();

            if (!payoutRunning)
                this.SmartPayout.ClosePort();

            return 0;
        }
        /// <summary>
        /// Main loop for smartpayout+smarthopper
        /// </summary>
        public int MainLoop_()
        {
            //this.Enabled = true;
            //btnRun.Enabled = false;
            //btnHalt.Enabled = true;

            // Connect to the validators (non-threaded for initial connect)
            ConnectToSMARTPayout();
            if (Properties.Settings.Default.HopperEnabled)
            {
                ConnectToHopper();
            }
            // Enable validators
            SmartPayout.EnableValidator();

            if (Properties.Settings.Default.HopperEnabled)
            {
                Hopper.EnableValidator();
            }
            // While app active
            while (!CHelpers.Shutdown)
            {
                // Setup form layout on first run
                if (!FormSetup)
                {
                    FormSetup = true;
                    if (Properties.Settings.Default.HopperEnabled)
                    {
                        coinlevel = Hopper.GetChannelLevelInfo();
                    }
                    FormSetup = true;
                    //SetupFormLayout();
                   
                }

                // If the Hopper is supposed to be running but the poll fails
                if (hopperRunning && !hopperConnecting && !Hopper.DoPoll())
                {
                    //textBox1.AppendText("Lost connection to SMART Hopper\r\n");
                    try
                    {
                        this.OnLogMessageEvent(new LogMessageEventArgs("Lost connection to SMART Hopper\r\n"));
                    }
                    catch { }
                    // If the other unit isn't running, refresh the port by closing it
                    if (!hopperRunning)
                        this.Hopper.ClosePort();

                    hopperRunning = false;
                    // Create and start a reconnection thread, this allows this loop to continue executing
                    // and polling the other validator
                    tHopRec = new Thread(() => ReconnectHopper());
                    tHopRec.Start();
                }
                // Same as above but for the Payout
                byte[] Response = new byte[20];
                int len = 0;

                if (payoutRunning && !payoutConnecting && !SmartPayout.DoPoll(ref Response, ref len))
                {
                    //textBox1.AppendText("Lost connection to SMART Payout\r\n");
                    try
                    {
                        this.OnLogMessageEvent(new LogMessageEventArgs("Lost connection to SMART Payout\r\n"));
                    }
                    catch { }
                    // If the other unit isn't running, refresh the port by closing it
                    if (!payoutRunning)
                        this.SmartPayout.ClosePort();
                    payoutRunning = false;
                    // Create and start a reconnection thread, this allows this loop to continue executing
                    // and polling the other validator
                    tSPRec = new Thread(() => ReconnectPayout());
                    tSPRec.Start();
                }
                UpdateUI();
                timer1.Enabled = true;
                while (timer1.Enabled) Application.DoEvents();
            }
            return 0;
            //btnRun.Enabled = true;
            //btnHalt.Enabled = false;
        }

        public void ConnectToSMARTPayoutTEST()
        {
            payoutConnecting = true;
            // setup timer, timeout delay and number of attempts to connect
            System.Windows.Forms.Timer reconnectionTimer = new System.Windows.Forms.Timer();
            reconnectionTimer.Tick += new EventHandler(reconnectionTimer_Tick);
            reconnectionTimer.Interval = 1000; // ms
            int attempts = 10;

            // Setup connection info
            SmartPayout.CommandStructure.ComPort = Properties.Settings.Default.SmartPayoutPort;
            SmartPayout.CommandStructure.SSPAddress = Properties.Settings.Default.SSP1;
            SmartPayout.CommandStructure.BaudRate = 9600;
            SmartPayout.CommandStructure.Timeout = 1000;
            SmartPayout.CommandStructure.RetryLevel = 3;

            // Run for number of attempts specified
            for (int i = 0; i < attempts; i++)
            {
                //if (log != null) log.AppendText("Trying connection to SMART Payout\r\n");
                try
                {
                    Console.WriteLine("Trying connection to SMART Payout\r\n");
                    this.OnLogMessageEvent(new LogMessageEventArgs("Trying connection to SMART Payout\r\n"));
                }
                catch { }
                // turn encryption off for first stage
                SmartPayout.CommandStructure.EncryptionStatus = false;

                // Open port first, if the key negotiation is successful then set the rest up
                if (SmartPayout.OpenPort())
                {
                    if (SmartPayout.NegotiateKeys())
                    {
                        SmartPayout.CommandStructure.EncryptionStatus = true; // now encrypting
                                                                              // find the max protocol version this validator supports
                        byte maxPVersion = FindMaxPayoutProtocolVersion();
                        if (maxPVersion >= 6)
                            SmartPayout.SetProtocolVersion(maxPVersion);
                        else
                        {
                            MessageBox.Show("This program does not support units under protocol 6!", "ERROR");
                            payoutConnecting = false;
                            return;
                        }
                        // get info from the validator and store useful vars
                        SmartPayout.SetupRequest();
                        // check the right unit is connected
                        if (!IsValidatorSupported(SmartPayout.UnitType))
                        {
                            MessageBox.Show("Unsupported type shown by SMART Payout, this SDK supports the SMART Payout and the SMART Hopper only");
                            payoutConnecting = false;
                            Application.Exit();
                            return;
                        }
                        // inhibits, this sets which channels can receive notes
                        SmartPayout.SetInhibits();
                        // enable payout
                        SmartPayout.EnablePayout();
                        // set running to true so the validator begins getting polled
                        payoutRunning = true;
                        payoutConnecting = false;
                    }
                    else
                    {
                        // reset timer
                        reconnectionTimer.Enabled = true;
                        while (reconnectionTimer.Enabled)
                        {
                            if (CHelpers.Shutdown)
                            {
                                payoutConnecting = false;
                                return;
                            }

                            Application.DoEvents();
                            Thread.Sleep(1);
                        }
                    }
                    SmartPayout.ClosePort();
                }
            }
            payoutConnecting = false;
            return;
        }

        public void ConnectToSMARTPayout()
        {
//            ConnectToSMARTPayoutTEST();



            payoutConnecting = true;
            // setup timer, timeout delay and number of attempts to connect
            System.Windows.Forms.Timer reconnectionTimer = new System.Windows.Forms.Timer();
            reconnectionTimer.Tick += new EventHandler(reconnectionTimer_Tick);
            reconnectionTimer.Interval = 1000; // ms
            int attempts = 10;

            // Setup connection info
            SmartPayout.CommandStructure.ComPort = Properties.Settings.Default.SmartPayoutPort;
            SmartPayout.CommandStructure.SSPAddress = Properties.Settings.Default.SSP1;
            SmartPayout.CommandStructure.BaudRate = 9600;
            SmartPayout.CommandStructure.Timeout = 1000;
            SmartPayout.CommandStructure.RetryLevel = 3;

            // Run for number of attempts specified
            for (int i = 0; i < attempts; i++)
            {
                //if (log != null) log.AppendText("Trying connection to SMART Payout\r\n");
                try
                {
                    Console.WriteLine("Trying connection to SMART Payout\r\n");
                    this.OnLogMessageEvent(new LogMessageEventArgs("Trying connection to SMART Payout\r\n"));
                }
                catch { }
                // turn encryption off for first stage
                SmartPayout.CommandStructure.EncryptionStatus = false;

                // Open port first, if the key negotiation is successful then set the rest up
                SmartPayout.ClosePort();
                if (SmartPayout.OpenPort())
                {
                    if (SmartPayout.NegotiateKeys())
                    {
                        SmartPayout.CommandStructure.EncryptionStatus = true; // now encrypting
                                                                              // find the max protocol version this validator supports
                        byte maxPVersion = FindMaxPayoutProtocolVersion();
                        if (maxPVersion >= 6)
                            SmartPayout.SetProtocolVersion(maxPVersion);
                        else
                        {
                            MessageBox.Show("This program does not support units under protocol 6!", "ERROR");
                            payoutConnecting = false;
                            return;
                        }
                        // get info from the validator and store useful vars
                        SmartPayout.SetupRequest();
                        // check the right unit is connected
                        if (!IsValidatorSupported(SmartPayout.UnitType))
                        {
                            MessageBox.Show("Unsupported type shown by SMART Payout, this SDK supports the SMART Payout and the SMART Hopper only");
                            payoutConnecting = false;
                            Application.Exit();
                            return;
                        }
                        // inhibits, this sets which channels can receive notes
                        SmartPayout.SetInhibits();
                        // enable payout
                        SmartPayout.EnablePayout();
                        // set running to true so the validator begins getting polled
                        payoutRunning = true;
                        payoutConnecting = false;
                        return;
                    }

                    SmartPayout.ClosePort();

                    // reset timer
                    reconnectionTimer.Enabled = true;
                    while (reconnectionTimer.Enabled)
                    {
                        if (CHelpers.Shutdown)
                        {
                            payoutConnecting = false;
                            return;
                        }

                        Application.DoEvents();
                        Thread.Sleep(1);
                    }
                }
            }
            payoutConnecting = false;
            return;
        }

        private bool IsValidatorSupported(char type)
        {
            if (type == (char)0x06) //smartpayout
                return true;
            return false;
        }

        private void reconnectionTimer_Tick(object sender, EventArgs e)
        {
            if (sender is System.Timers.Timer)
            {
                System.Timers.Timer t = sender as System.Timers.Timer;
                t.Enabled = false;
            }
        }

        // This updates UI variables such as textboxes etc.
        void UpdateUI()
        {
            decimal GetTotalValue = SmartPayout.GetAddedValue();

            // Get stored notes info from NV11
            notestoredinfo = NV11.GetStorageInfo();
            totalnote = NV11.TOTAL_NOTES;      //.ToString("#,##0.00");

            //paidamnt  = NV11.PAID_AMOUNT + Hopper.COIN_INSERTED ;   //.ToString("#,##0.00");

            // Get channel info from hopper
            coinlevel = Hopper.GetChannelLevelInfo();
            totalcoin = Hopper.TOTAL_COIN;           //.ToString("#,##0.00");
            NV11.CoinFloat = Hopper.TOTAL_COIN;
//            NV11.MinFloat_AMOUNT = (NV11.TOTAL_NOTES + Hopper.TOTAL_COIN);
            NV11.MinFloat_AMOUNT = (GetTotalValue + NV11.TOTAL_NOTES + Hopper.TOTAL_COIN);
            this.OnUpdate_UI(EventArgs.Empty);
           
        }

        private void ReconnectPayout()
        {
            //OutputMessage m = new OutputMessage(AppendToTextBox);
            while (!payoutRunning)
            {
                //if (textBox1.InvokeRequired)
                //    textBox1.Invoke(m, new object[] { "Attempting to reconnect to SMART Payout...\r\n" });
                //else
                //    textBox1.AppendText("Attempting to reconnect to SMART Payout...\r\n");

                try
                {
                    this.OnLogMessageEvent(new LogMessageEventArgs("Attempting to reconnect to SMART Payout...\r\n"));
                }
                catch { }

                ConnectToSMARTPayout(); // Have to pass null as can't update text box from a different thread without invoking
            
                CHelpers.Pause(1000);
                if (CHelpers.Shutdown) return;
            }

            try
            {
                this.OnLogMessageEvent(new LogMessageEventArgs("Reconnected to SMART Payout\r\n"));
            }
            catch { }

            //if (textBox1.InvokeRequired)
            //    textBox1.Invoke(m, new object[] { "Reconnected to SMART Payout\r\n" });
            //else
            //    textBox1.AppendText("Reconnected to SMART Payout\r\n");
            SmartPayout.EnableValidator();
        }

        // These functions are run in a seperate thread to allow the main loop to continue executing.
        private void ReconnectHopper()
        {
            this.hopperConnecting = true;
            while (!hopperRunning)
            {
                Globals.LogFile("Attempting to reconnect to SMART Hopper...\r\n");

                ConnectToHopper();
                CHelpers.Pause(1000);
                if (CHelpers.Shutdown) return;
            }
            Globals.LogFile("Reconnected to SMART Hopper\r\n");
            if (inMaintenance || paymode)
            {
                Hopper.EnableValidator();
            }
            this.hopperConnecting = false;
        }

        private void ReconnectNV11()
        {
            this.NV11Connecting = true;
            while (!NV11Running)
            {
                Globals.LogFile("Attempting to reconnect to NV11...\r\n");

                ConnectToNV11(); // Have to pass null as can't update text box from a different thread without invoking

                CHelpers.Pause(1000);
                if (CHelpers.Shutdown) return;
            }
            Globals.LogFile("Reconnected to NV11\r\n");
            //this.SmartpayMonitor.ReportProgress(0, "NVReconnect");

            this.OnLogMessageEvent(new LogMessageEventArgs("Reconnected to NV11\r\n"));

            if (inMaintenance || paymode)
            {
                NV11.EnableValidator();
            }
            this.NV11Connecting = false;
        }

        public void ConnectToNV11()
        {
            //TextBox logs = (TextBox)log; 
            // setup timer
            System.Timers.Timer reconnectionTimer = new System.Timers.Timer();
            reconnectionTimer.Elapsed += reconnectionTimerEvent;
            reconnectionTimer.Interval = 1000; // ms
            int attempts = 10;

            // Setup connection info
            this.NV11.CommandStructure.ComPort = Properties.Settings.Default.NV11ComPort;
            this.NV11.CommandStructure.SSPAddress = Properties.Settings.Default.SSP1;
            this.NV11.CommandStructure.BaudRate = 9600;
            this.NV11.CommandStructure.Timeout = 1000;  //1000
            this.NV11.CommandStructure.RetryLevel = 4;

            // Run for number of attempts specified
            for (int i = 0; i < attempts; i++)
            {

                this.OnLogMessageEvent(new LogMessageEventArgs("Trying connection to NV11 port:" + this.NV11.CommandStructure.ComPort + "\r\n"));

                // turn encryption off for first stage
                this.NV11.CommandStructure.EncryptionStatus = false;

                // if the key negotiation is successful then set the rest up
                if (this.NV11.OpenPort() && NV11.NegotiateKeys())
                {
                    this.NV11.CommandStructure.EncryptionStatus = true; // now encrypting
                    // find the max protocol version this validator supports
                    byte maxPVersion = FindMaxNV11ProtocolVersion();
                    if (maxPVersion >= 6)
                        this.NV11.SetProtocolVersion(maxPVersion);
                    else
                    {
                        //MessageBox.Show("This program does not support slaves under protocol 6!", "ERROR");
                        Globals.LogFile("This program does not support slaves under protocol 6!=> Connect To NV");
                        return;
                    }
                    // get info from the validator and store useful vars
                    NV11.SetupRequest();
                    // inhibits, this sets which channels can receive notes
                    NV11.SetInhibits();
                  
                    // enable payout to begin with
                    NV11.EnablePayout();
                    
                    // check for any stored notes
                    NV11.CheckForStoredNotes();
                    // report by the 4 byte value of the note, not the channel
                    NV11.SetValueReportingType(false);
                    // set running to true so the NV11 begins getting polled
                    NV11Running = true;

                    this.OnNVConnected(EventArgs.Empty);
                    return;
                }
                // reset timer
                reconnectionTimer.Enabled = true;

                //System.Windows.Application.Current.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                //{
                //    while (reconnectionTimer.Enabled)
                //    {
                //        if (CHelpers.Shutdown)
                //            return;
                //    }
                //}));
                while (reconnectionTimer.Enabled)
                {
                    System.Windows.Forms.Application.DoEvents();
                    if (CHelpers.Shutdown)
                        return;

                }
            }
        }
        public void ConnectToHopper()
        {
            // setup timer
            System.Timers.Timer reconnectionTimer = new System.Timers.Timer(1000);
            //reconnectionTimer.Interval = 1000; // ms
            reconnectionTimer.Elapsed += reconnectionTimerEvent;
            int attempts = 10;

            // Setup connection info
            Hopper.CommandStructure.ComPort = Properties.Settings.Default.HopperComPort;
            Hopper.CommandStructure.SSPAddress = Properties.Settings.Default.SSP2;
            Hopper.CommandStructure.BaudRate = 9600;
            Hopper.CommandStructure.Timeout = 1000;
            Hopper.CommandStructure.RetryLevel = 3;

            // Run for number of attempts specified
            for (int i = 0; i < attempts; i++)
            {
                this.OnLogMessageEvent(new LogMessageEventArgs(" ConnectToHopper - Trying connection to SMART Hopper port:" + Hopper.CommandStructure.ComPort + "\r\n"));
                Globals.LogFile(" ConnectToHopper - Trying connection to SMART Hopper\r\n");

                // turn encryption off for first stage
                Hopper.CommandStructure.EncryptionStatus = false;

                // if the key negotiation is successful then set the rest up
                if (Hopper.OpenPort() && Hopper.NegotiateKeys())
                {
                    Hopper.CommandStructure.EncryptionStatus = true; // now encrypting
                    // find the max protocol version this validator supports
                    byte maxPVersion = FindMaxHopperProtocolVersion();
                    if (maxPVersion >= 6)
                        this.Hopper.SetProtocolVersion(maxPVersion);
                    else
                    {
                        //MessageBox.Show("This program does not support slaves under protocol 6!", "ERROR");
                        Globals.LogFile("This program does not support slaves under protocol 6!=> Connect To Hopper Error");
                        return;
                    }
                    // get info from the validator and store useful vars
                    Hopper.SetupRequest();
                    // inhibits, this sets which channels can receive notes
                    //VIC this is for coin mech. disable if no coin slot
                    //Hopper.SetInhibits(log);

                    // set running to true so the hopper begins getting polled
                    hopperRunning = true;
                    Globals.LogFile("Smart Hopper now running\n");

                    this.OnHopperConnected(EventArgs.Empty);
                    return;
                }
                // reset timer
                reconnectionTimer.Enabled = true;

                //System.Windows.Application.Current.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background, new Action(() =>
                //{
                //    while (reconnectionTimer.Enabled)
                //    {
                //        if (CHelpers.Shutdown)
                //            return;

                //    }
                //}));
                while (reconnectionTimer.Enabled)
                {
                    System.Windows.Forms.Application.DoEvents();
                    if (CHelpers.Shutdown)
                        return;
                }

            }
        }

        // This function finds the maximum protocol version that a validator supports. To do this
        // it attempts to set a protocol version starting at 6 in this case, and then increments the
        // version until error 0xF8 is returned from the validator which indicates that it has failed
        // to set it. The function then returns the version number one less than the failed version.
        private byte FindMaxHopperProtocolVersion()
        {
            // not dealing with protocol under level 6
            // attempt to set in validator
            byte b = 0x06;
            while (true)
            {
                Hopper.SetProtocolVersion(b);
                // If it fails then it can't be set so fall back to previous iteration and return it
                if (Hopper.CommandStructure.ResponseData[0] == CCommands.Generic.SSP_RESPONSE_FAIL)
                    return --b;
                b++;
                if (b > 20) return 0x06; // return default if p version runs too high
            }
        }


        private byte FindMaxNV11ProtocolVersion()
        {
            // not dealing with protocol under level 6
            // attempt to set in validator
            byte b = 0x06;
            while (true)
            {
                NV11.SetProtocolVersion(b);
                // If it fails then it can't be set so fall back to previous iteration and return it
                if (NV11.CommandStructure.ResponseData[0] == CCommands.Generic.SSP_RESPONSE_FAIL)
                    return --b;
                b++;
                if (b > 20) return 0x06; // return default if p version runs too high
            }
        }

        private byte FindMaxPayoutProtocolVersion()
        {
            // not dealing with protocol under level 6
            // attempt to set in validator
            byte b = 0x06;
            while (true)
            {
                SmartPayout.SetProtocolVersion(b);
                // If it fails then it can't be set so fall back to previous iteration and return it
                if (SmartPayout.CommandStructure.ResponseData[0] == CCommands.Generic.SSP_RESPONSE_FAIL)
                    return --b;
                b++;

                // If the protocol version 'runs away' because of a drop in comms. Return the default value.
                if (b > 20)
                    return 0x06;
            }
        }

        public void CalculatePayout()
        {

            //decimal amnt = (decimal)this.payout;
            //this.smartpay.VALIDATOR.Integer_Part = (int)Decimal.ToInt16(amnt);

            ////Decimal decimal_part;
            //this.smartpay.VALIDATOR.Decimap_Part = (amnt - Decimal.Truncate(this.smartpay.VALIDATOR.Integer_Part));
            // Now we have a number we have to work out which validator needs to process the payout

            // This section can be modified to payout in any way needed. Currently it pays out one
            // note and the rest from the hopper - this could be extended to payout multiple notes.

            // First work out if we need a note, if it's less than the value of the first channel (assuming channels are in value order)
            // then we know it doesn't need one
            this.donepayout = false;

// TODO : Elaborate on the change process to account for different change levels in the machines. 

            int OriginalPayout = Convert.ToInt32(this.payout);

            if (this.payout >= 500)
            {
                int tmp = OriginalPayout / 500;
                int value = tmp * 500;

                if (payoutRunning)
                {
                    //                    this.payout = PAYOUT.PayoutAmount((int)(this.payout), Properties.Settings.Default.CurrencyCode.ToCharArray(), false);
                    PAYOUT.EnableValidator();
                    PAYOUT.PayoutAmount((int)(value), Properties.Settings.Default.CurrencyCode.ToCharArray(), false);
                    this.payout -= value;
                }
            }

            if(this.payout > 0)
            {
                if(hopperRunning)
                {
                    Hopper.EnableValidator();
                    HOPPER.PayoutAmount((int)(this.payout), Properties.Settings.Default.CurrencyCode.ToCharArray());
                }
            }

// TODO : issue a receipt if not enough cash remains for change

            this.donepayout = true;
            this.OnDone_Payout(EventArgs.Empty);

#if (false)
            int noteValc = VALIDATOR.GetLastNoteValue();
            if ((this.payout) < noteValc && this.payout != 0 || noteValc == 0)
            {
                // So we can send the command straight to the hopper
                //Hopper.PayoutAmount((int)payoutAmount, currency.ToCharArray(), textBox1);
                HOPPER.PayoutAmount((int)(this.payout), Properties.Settings.Default.CurrencyCode.ToCharArray());
                this.donepayout = true;
                this.OnDone_Payout(EventArgs.Empty);
            }
            else
            {
                // Otherwise we need to work out how to payout notes
                // Check what value the last note stored in the payout was

                PAYOUT.PayoutAmount((int)(this.payout), Properties.Settings.Default.CurrencyCode.ToCharArray(), false);

/*
                int noteVal = 0;

                if (NV11Running)
                    noteVal = VALIDATOR.GetLastNoteValue();

                // If less or equal to payout, pay it out
                if (noteVal <= (this.payout) && noteVal > 0)
                {
                    this.payout -= noteVal;   // take the note off the total requested

                    if()                        
                    VALIDATOR.PayoutNextNote();
                }

                if (this.payout <= 0)
                {
                    this.donepayout = true;
                    this.OnDone_Payout(EventArgs.Empty);
                }
*/
            }
#endif // (false)
        }

        #endregion

        private void reconnectionTimerEvent(object source, System.Timers.ElapsedEventArgs e)
        {
            if (source is System.Timers.Timer)
            {
                System.Timers.Timer t = source as System.Timers.Timer;
                t.Enabled = false;
            }
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
            CHelpers.Shutdown = true;
            Dispose(true);
            GC.SuppressFinalize(this);

        }
        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                handle.Dispose();
                // Free any other managed objects here. 
                //
            }

            // Free any unmanaged objects here. 
            //
            disposed = true;
        }


        // Use interop to call the method necessary
        // to clean up the unmanaged resource.
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);


        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        ~SmartPay()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
    }
}
