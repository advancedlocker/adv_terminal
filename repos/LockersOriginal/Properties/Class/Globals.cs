﻿

namespace Lockers
{
    using System;
    using System.IO;
    using System.Net.Sockets;
    using System.Windows.Forms;
    internal static class Globals
    {
        private static bool abortfrm;
        private static LoginTypes login;
        private static PaymentTypes paytype;
        private static ProcessType process;
        private static bool accessgranted;
        private static bool onpaysetup;
        private static Double dueAmnt=0;
        private static LockerDatabase lockdatabase;
        
        public static String CurrentLanguage;

        public static int LockerCommsRetries = 2;


        public static SmartPay smartpay;

        private static System.IO.Ports.SerialPort RTDebuggingPort;

        private static int langcode = Properties.Settings.Default.DefaultLangCode ;

        public static Button prevBtn { get; set; }

        public static LockerDatabase LockData { get; set; }

        public static Locker_TCPClient client;
/*        public static Locker_TCPClient client
        {
            get { return Client; }
            set { Client = value; }
        }
*/
        public static int LanguageCode
        {
            get { return langcode; }
            set { langcode = value; }
        }

        public static Boolean AccessGranted
        {
            get { return accessgranted; }
            set { accessgranted = value; }
        }

        public static ProcessType Process
        {
            get { return process; }
            set { process = value; }
        }

        public static Double DueAmnt
        {
            get { return dueAmnt; }
            set { dueAmnt = value; }
        }
        public static Boolean OnpaySetup
        {
            get { return onpaysetup; }
            set { onpaysetup = value; }
        }

        public static bool Maintenance
        { get; set; }

        public static bool UpdateLanguage
        { get; set; }

       
        public static PaymentTypes PayTypes
        {
            get { return paytype; }
            set { paytype = value; }
        }
        public static LoginTypes LoginSelected
        {
            get { return login; }
            set { login = value; }
        }
        public static bool AbortForm
        {
            get { return abortfrm;  }
            set { abortfrm = value; }
        }

        public static bool InvokeRequired { get; private set; }

        //module to create error log
        public static void LogFile(string sExceptionMessage)
        {
            try
            {
                SendCommsLogReport(sExceptionMessage);

                StreamWriter log;
                string logfile = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), "PaySystemLog.txt");
                if (!File.Exists(logfile))
                {

                    log = new StreamWriter(logfile);

                }

                else
                {

                    log = File.AppendText(logfile);

                }

                // Write to the file:

                log.WriteLine(DateTime.Now + " :" + sExceptionMessage);

                // Close the stream:

                log.Close();
            }
            catch { }
        }

        public static void LogFile(string sExceptionMessage, string filename)
        {

            StreamWriter log;
            try
            {
                SendCommsLogReport(sExceptionMessage);

                string logfile = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location), filename);
                if (!File.Exists(logfile))
                {

                    log = new StreamWriter(logfile);

                }

                else
                {

                    log = File.AppendText(logfile);

                }

                // Write to the file:

                log.WriteLine(DateTime.Now + " :" + sExceptionMessage);

                // Close the stream:

                log.Close();
            }
            catch { }
        }

        public static void OpenCommsLogInterface()
        {
            if (Properties.Settings.Default.RTCommsLogPortEnabled)
            {
                try
                {
                    if (RTDebuggingPort == null)
                    {
                        RTDebuggingPort = new System.IO.Ports.SerialPort();
                        RTDebuggingPort.BaudRate = Convert.ToInt32(Properties.Settings.Default.RTCommsLogBaud);
                        RTDebuggingPort.PortName = Properties.Settings.Default.RTCommsLogPort;

                        RTDebuggingPort.Open();
                        RTDebuggingPort.WriteLine("RTComms Port Opened\n");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

            }
        }

        public static void DestroyCommsLogInterface()
        {
            try
            {
                if(RTDebuggingPort != null)
                {
                    if(RTDebuggingPort.IsOpen)
                    {
                        RTDebuggingPort.Close();
                    }

                    RTDebuggingPort.Dispose();
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public static void SendCommsLogReport(string message)
        {

            if (Properties.Settings.Default.RTCommsLogPortEnabled)
            {
                try
                {
                    if (!RTDebuggingPort.IsOpen)
                    {
                        RTDebuggingPort.Open();
                        RTDebuggingPort.WriteLine("RTComms Port Reopened");
                    }

                    RTDebuggingPort.WriteLine(DateTime.Now + " :" + message + "\n");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

#if (true)
        public static void Client_ConnectionStatusChanged(Locker_TCPClient sender, Locker_TCPClient.ConnectionStatus status)
        {
            //Check if this event was fired on a different thread, if it is then we must invoke it on the UI thread
            if (InvokeRequired)
            {
                Invoke(new Locker_TCPClient.delConnectionStatusChanged(Client_ConnectionStatusChanged), sender, status);
                return;
            }
            //richTextBox1.Text += "Connection: " + status.ToString() + Environment.NewLine;
            Globals.LogFile("client_ConnectionStatusChanged() - connect status = " + status.ToString() + "\n");
            if (status.ToString() == Locker_TCPClient.ConnectionStatus.Connected.ToString())
            {
                sender.SourceKey = Properties.Settings.Default.sourceKey;
                sender.GetSession();
            }

        }

        private static void Invoke(Locker_TCPClient.delConnectionStatusChanged delConnectionStatusChanged, Locker_TCPClient sender, Locker_TCPClient.ConnectionStatus status)
        {
            throw new NotImplementedException();
        }

        //Fired when new data is received in the TCP client
        public static void Client_DataReceived(Locker_TCPClient sender, object data)
        {
            //Again, check if this needs to be invoked in the UI thread
/*
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new Locker_TCPClient.delDataReceived(Client_DataReceived), sender, data);
                }
                catch
                { }
                return;
            }
*/
            //Interpret the received data object as a string
            string strData = data as string;
            Globals.LogFile("client_DataReceived() - data = " + strData + "\n");
//            string values = strData.Split('-').Select(sValue => sValue.Trim()).First();
            int sKey;
/*
            switch (sender.LockerCommand)
            {
                case Locker_cmd.Basic.GetSession:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.AssignLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.UnassignLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.AvailableLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.UsedLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.OpenLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.CloseLock:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                case Locker_cmd.Basic.LockStatus:
                    {
                        int.TryParse(strData, out sKey);
                        sender.SessionKey = sKey;
                    }
                    break;

                default:
                    break;
            }
*/
        }
#endif
    }
}
