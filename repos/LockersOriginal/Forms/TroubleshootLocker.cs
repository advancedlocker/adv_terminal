﻿using System;
using System.Windows.Forms;

namespace Lockers
{
    public partial class TroubleshootLocker : Form
    {


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        int focusEntry = 0;
        TextBox t;

        public TroubleshootLocker()
        {
            InitializeComponent();
            this.txtFrom.Enter += new EventHandler(textBox_Enter); // enter event==get focus event 
            this.txtTo.Enter += new EventHandler(textBox_Enter); // enter event==get focus event 
            t = txtFrom;
        }

        private void textBox_Enter(object sender, EventArgs e)
        {
            t = (TextBox)sender;
            //TextBox t = (TextBox)sender;
            //focusEntry=t.Name=="txtFrom" ? 0:1;
        }

        private void bbtnNum_Click(object sender, EventArgs e)
        {
            if (t == null) return;
    
            Button b = (Button)sender;
            if (b.Text == "")
            {
                if (t.Text.Length > 1)
                {
                    t.Text = t.Text.Substring(0, t.Text.Length - 1);
                }
                else
                {
                    t.Text = "";
                }
                
            }
            else
            {
                if (t.Text == "0") t.Clear();
                t.Text += b.Text;
            }
            //if (b.Tag != null)
            //{
            //    if (focusEntry == 0)
            //    {
            //        txtFrom.Clear();
            //    }
            //    else if (focusEntry == 1)
            //    {
            //        txtTo.Clear();
            //    }

            //}
            //else
            //{
            //    if (focusEntry == 0)
            //    {
            //        if (txtFrom.Text == "0") txtFrom.Clear();
            //        txtFrom.Text+= b.Text;
            //    }
            //    else if (focusEntry == 1)
            //    {
            //        if (txtTo.Text == "0") txtTo.Clear();
            //        txtTo.Text += b.Text;
            //    }
            //}
        }

        private void button16_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            LockerController frmController = new LockerController();
            frmController.BringToFront();
            frmController.TopMost = true;

            frmController.ShowDialog();
        }

        private void TroubleshootLocker_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            this.BackgroundImage = ConfigurationData.GetBackground("Blank");
        }


        void Open_Locker(int lockerNumber)
        {
            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            // USE SQL to update the address
            // TODO : Fix this serial number conbversion to byte array
            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };

            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetRear.CopyTo(sum, packetFront.Length + packetAddress.Length);

            Globals.client.Send(sum);
        }

        void Close_Locker(int lockerNumber)
        {
            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetAddress.CopyTo(sum, packetAddress.Length);

            Globals.client.Send(sum);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            int LockerNumber = 0;
            try {
                LockerNumber = Convert.ToInt16(txtFrom.Text);

                if(LockerNumber > 0)
                    Open_Locker(LockerNumber);
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }
    }
}
