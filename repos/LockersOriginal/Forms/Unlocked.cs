﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace Lockers
{
    public partial class Unlocked : Form
    {
        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        public Unlocked()
        {
            InitializeComponent();
            lblLocknum.Text = Globals.LockData.LockNumber.ToString();
            lblremaintime.Text = (Globals.LockData.Remaining < TimeSpan.Zero ? "-" : "") + String.Format("{0:hh\\:mm}", Globals.LockData.Remaining); //String.Format("{0:00}:{1:00} ", Globals.LockData.Remaining.Hours, Globals.LockData.Remaining.Minutes);

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnback_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void Unlocked_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

        }
    }
}
