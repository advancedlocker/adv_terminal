﻿namespace Lockers
{
    partial class NumericKeypad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NumericKeypad));
            this.buNegative = new System.Windows.Forms.Button();
            this.buDecimal = new System.Windows.Forms.Button();
            this.buAccept = new System.Windows.Forms.Button();
            this.buDisplay = new System.Windows.Forms.Button();
            this.buClear = new System.Windows.Forms.Button();
            this.buBksp = new System.Windows.Forms.Button();
            this.Button0 = new System.Windows.Forms.Button();
            this.Button9 = new System.Windows.Forms.Button();
            this.Button8 = new System.Windows.Forms.Button();
            this.Button7 = new System.Windows.Forms.Button();
            this.Button6 = new System.Windows.Forms.Button();
            this.Button5 = new System.Windows.Forms.Button();
            this.Button4 = new System.Windows.Forms.Button();
            this.Button3 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.Button1 = new System.Windows.Forms.Button();
            this.buCancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buNegative
            // 
            this.buNegative.BackColor = System.Drawing.Color.White;
            this.buNegative.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buNegative.ForeColor = System.Drawing.Color.Black;
            this.buNegative.Location = new System.Drawing.Point(23, 410);
            this.buNegative.Name = "buNegative";
            this.buNegative.Size = new System.Drawing.Size(75, 75);
            this.buNegative.TabIndex = 33;
            this.buNegative.Text = "-";
            this.buNegative.UseVisualStyleBackColor = false;
            this.buNegative.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buNegative_MouseUp);
            // 
            // buDecimal
            // 
            this.buDecimal.BackColor = System.Drawing.Color.White;
            this.buDecimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buDecimal.ForeColor = System.Drawing.Color.Black;
            this.buDecimal.Location = new System.Drawing.Point(185, 410);
            this.buDecimal.Name = "buDecimal";
            this.buDecimal.Size = new System.Drawing.Size(75, 75);
            this.buDecimal.TabIndex = 32;
            this.buDecimal.Text = ".";
            this.buDecimal.UseVisualStyleBackColor = false;
            this.buDecimal.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buDecimal_MouseUp);
            // 
            // buAccept
            // 
            this.buAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buAccept.BackColor = System.Drawing.Color.Chartreuse;
            this.buAccept.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buAccept.ForeColor = System.Drawing.Color.Black;
            this.buAccept.Location = new System.Drawing.Point(288, 422);
            this.buAccept.Name = "buAccept";
            this.buAccept.Size = new System.Drawing.Size(127, 59);
            this.buAccept.TabIndex = 31;
            this.buAccept.Text = "Accept";
            this.buAccept.UseVisualStyleBackColor = false;
            this.buAccept.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buAccept_MouseUp);
            // 
            // buDisplay
            // 
            this.buDisplay.BackColor = System.Drawing.Color.WhiteSmoke;
            this.buDisplay.Enabled = false;
            this.buDisplay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 45F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buDisplay.Location = new System.Drawing.Point(23, 76);
            this.buDisplay.Name = "buDisplay";
            this.buDisplay.Size = new System.Drawing.Size(392, 75);
            this.buDisplay.TabIndex = 29;
            this.buDisplay.TabStop = false;
            this.buDisplay.Text = "0";
            this.buDisplay.UseVisualStyleBackColor = false;
            // 
            // buClear
            // 
            this.buClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buClear.BackColor = System.Drawing.Color.Chartreuse;
            this.buClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buClear.ForeColor = System.Drawing.Color.Black;
            this.buClear.Location = new System.Drawing.Point(288, 248);
            this.buClear.Name = "buClear";
            this.buClear.Size = new System.Drawing.Size(127, 59);
            this.buClear.TabIndex = 28;
            this.buClear.Text = "Clear";
            this.buClear.UseVisualStyleBackColor = false;
            this.buClear.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buClear_MouseUp);
            // 
            // buBksp
            // 
            this.buBksp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buBksp.BackColor = System.Drawing.Color.Chartreuse;
            this.buBksp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buBksp.ForeColor = System.Drawing.Color.Black;
            this.buBksp.Image = ((System.Drawing.Image)(resources.GetObject("buBksp.Image")));
            this.buBksp.Location = new System.Drawing.Point(288, 167);
            this.buBksp.Name = "buBksp";
            this.buBksp.Size = new System.Drawing.Size(127, 59);
            this.buBksp.TabIndex = 27;
            this.buBksp.UseVisualStyleBackColor = false;
            this.buBksp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.buBksp_MouseUp);
            // 
            // Button0
            // 
            this.Button0.BackColor = System.Drawing.Color.White;
            this.Button0.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button0.ForeColor = System.Drawing.Color.Black;
            this.Button0.Location = new System.Drawing.Point(104, 410);
            this.Button0.Name = "Button0";
            this.Button0.Size = new System.Drawing.Size(75, 75);
            this.Button0.TabIndex = 26;
            this.Button0.Text = "0";
            this.Button0.UseVisualStyleBackColor = false;
            this.Button0.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // Button9
            // 
            this.Button9.BackColor = System.Drawing.Color.White;
            this.Button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button9.ForeColor = System.Drawing.Color.Black;
            this.Button9.Location = new System.Drawing.Point(185, 167);
            this.Button9.Name = "Button9";
            this.Button9.Size = new System.Drawing.Size(75, 75);
            this.Button9.TabIndex = 25;
            this.Button9.Text = "9";
            this.Button9.UseVisualStyleBackColor = false;
            this.Button9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // Button8
            // 
            this.Button8.BackColor = System.Drawing.Color.White;
            this.Button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button8.ForeColor = System.Drawing.Color.Black;
            this.Button8.Location = new System.Drawing.Point(104, 167);
            this.Button8.Name = "Button8";
            this.Button8.Size = new System.Drawing.Size(75, 75);
            this.Button8.TabIndex = 24;
            this.Button8.Text = "8";
            this.Button8.UseVisualStyleBackColor = false;
            this.Button8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // Button7
            // 
            this.Button7.BackColor = System.Drawing.Color.White;
            this.Button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button7.ForeColor = System.Drawing.Color.Black;
            this.Button7.Location = new System.Drawing.Point(23, 167);
            this.Button7.Name = "Button7";
            this.Button7.Size = new System.Drawing.Size(75, 75);
            this.Button7.TabIndex = 23;
            this.Button7.Text = "7";
            this.Button7.UseVisualStyleBackColor = false;
            this.Button7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // Button6
            // 
            this.Button6.BackColor = System.Drawing.Color.White;
            this.Button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button6.ForeColor = System.Drawing.Color.Black;
            this.Button6.Location = new System.Drawing.Point(185, 248);
            this.Button6.Name = "Button6";
            this.Button6.Size = new System.Drawing.Size(75, 75);
            this.Button6.TabIndex = 22;
            this.Button6.Text = "6";
            this.Button6.UseVisualStyleBackColor = false;
            this.Button6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // Button5
            // 
            this.Button5.BackColor = System.Drawing.Color.White;
            this.Button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button5.ForeColor = System.Drawing.Color.Black;
            this.Button5.Location = new System.Drawing.Point(104, 248);
            this.Button5.Name = "Button5";
            this.Button5.Size = new System.Drawing.Size(75, 75);
            this.Button5.TabIndex = 21;
            this.Button5.Text = "5";
            this.Button5.UseVisualStyleBackColor = false;
            this.Button5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // Button4
            // 
            this.Button4.BackColor = System.Drawing.Color.White;
            this.Button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button4.ForeColor = System.Drawing.Color.Black;
            this.Button4.Location = new System.Drawing.Point(23, 248);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(75, 75);
            this.Button4.TabIndex = 20;
            this.Button4.Text = "4";
            this.Button4.UseVisualStyleBackColor = false;
            this.Button4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // Button3
            // 
            this.Button3.BackColor = System.Drawing.Color.White;
            this.Button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button3.ForeColor = System.Drawing.Color.Black;
            this.Button3.Location = new System.Drawing.Point(185, 329);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(75, 75);
            this.Button3.TabIndex = 19;
            this.Button3.Text = "3";
            this.Button3.UseVisualStyleBackColor = false;
            this.Button3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // Button2
            // 
            this.Button2.BackColor = System.Drawing.Color.White;
            this.Button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button2.ForeColor = System.Drawing.Color.Black;
            this.Button2.Location = new System.Drawing.Point(104, 329);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(75, 75);
            this.Button2.TabIndex = 18;
            this.Button2.Text = "2";
            this.Button2.UseVisualStyleBackColor = false;
            this.Button2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // Button1
            // 
            this.Button1.BackColor = System.Drawing.Color.White;
            this.Button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button1.ForeColor = System.Drawing.Color.Black;
            this.Button1.Location = new System.Drawing.Point(23, 329);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(75, 75);
            this.Button1.TabIndex = 17;
            this.Button1.Text = "1";
            this.Button1.UseVisualStyleBackColor = false;
            this.Button1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.NumericMouseUp);
            // 
            // buCancel
            // 
            this.buCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buCancel.BackColor = System.Drawing.Color.Chartreuse;
            this.buCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buCancel.ForeColor = System.Drawing.Color.Black;
            this.buCancel.Location = new System.Drawing.Point(288, 343);
            this.buCancel.Name = "buCancel";
            this.buCancel.Size = new System.Drawing.Size(127, 59);
            this.buCancel.TabIndex = 34;
            this.buCancel.Text = "Cancel";
            this.buCancel.UseVisualStyleBackColor = false;
            this.buCancel.Click += new System.EventHandler(this.buCancel_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(392, 38);
            this.label1.TabIndex = 35;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NumericKeypad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(230)))), ((int)(((byte)(228)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(438, 509);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buCancel);
            this.Controls.Add(this.buNegative);
            this.Controls.Add(this.buDecimal);
            this.Controls.Add(this.buAccept);
            this.Controls.Add(this.buDisplay);
            this.Controls.Add(this.buClear);
            this.Controls.Add(this.buBksp);
            this.Controls.Add(this.Button0);
            this.Controls.Add(this.Button6);
            this.Controls.Add(this.Button5);
            this.Controls.Add(this.Button4);
            this.Controls.Add(this.Button3);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.Button9);
            this.Controls.Add(this.Button8);
            this.Controls.Add(this.Button7);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NumericKeypad";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Keypad";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Load += new System.EventHandler(this.NumericKeypad_Load);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Button buNegative;
        internal System.Windows.Forms.Button buDecimal;
        internal System.Windows.Forms.Button buAccept;
        internal System.Windows.Forms.Button buDisplay;
        internal System.Windows.Forms.Button buClear;
        internal System.Windows.Forms.Button buBksp;
        internal System.Windows.Forms.Button Button0;
        internal System.Windows.Forms.Button Button9;
        internal System.Windows.Forms.Button Button8;
        internal System.Windows.Forms.Button Button7;
        internal System.Windows.Forms.Button Button6;
        internal System.Windows.Forms.Button Button5;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.Button buCancel;
        private System.Windows.Forms.Label label1;
    }
}