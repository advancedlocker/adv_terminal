﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace Lockers
{
    public partial class BlackScreen : Form
    {
       
        public BlackScreen()
        {
            InitializeComponent();
            this.BackgroundImage = ConfigurationData.GetBackground("Blank");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           
        }

        private void BlackScreen_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
        }
    }
}
