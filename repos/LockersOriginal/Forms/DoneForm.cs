﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
namespace Lockers
{
    public partial class DoneForm : Form
    {

        DateTimeFormatInfo fi = new DateTimeFormatInfo();
        System.Timers.Timer timer2 = new System.Timers.Timer();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        public DoneForm()
        {
            InitializeComponent();
            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);

            timer2.Elapsed += this.timer2_Tick;
            timer2.Interval = 5000;

            timer1.Start();
            timer2.Start();
        }

        private void DoneForm_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
        }

        private void DoneForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void timer2_Tick(Object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (InvokeRequired)
                {
                    timer2.Stop();
                    Invoke(new MethodInvoker(Close));
                }
                else
                {
                    // Do Something
                    timer2.Stop();
                    Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
