﻿using System;
using System.Globalization;
using System.Windows.Forms;
using System.Drawing;
using System.Xml;
using System.IO;

namespace Lockers
{
    public partial class welcome : Form
    {
        SmartPay smartpay;
        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        private void keypressedHandler(object sender, EventArgs e)
        {
            Globals.CurrentLanguage = ((Button)sender).Name;
            this.DialogResult = DialogResult.OK;
            timer1.Stop();
            this.Close();
        }

        public welcome(SmartPay smartpay)
        {
            InitializeComponent();

            String ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\Welcome_1182x788.tiff";
            this.BackgroundImage = Image.FromFile(ImageFilename);

            String Country = Properties.Settings.Default.Country;
            XmlTextReader xmlReader = new XmlTextReader(Properties.Settings.Default.CountryConfigurations);

            XmlDataDocument xmldoc = new XmlDataDocument();
            XmlNodeList xmlnode;
            FileStream fs = new FileStream(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.CountryConfigurations, FileMode.Open, FileAccess.Read);
            xmldoc.Load(fs);
            xmlnode = xmldoc.GetElementsByTagName("Country");
            int NumSupportedLanguages = 0;

            String[] Languages = new String[0];
            String[] Flags = new String[0];

            for (int i = 0; i < xmlnode.Count; i++)
            {
                if (xmlnode[i].ChildNodes[0].Name == Country)
                {
                    for (int j = 0; j < xmlnode[i].ChildNodes[0].ChildNodes.Count; j++)
                    {
                        switch (xmlnode[i].ChildNodes[0].ChildNodes[j].Name)
                        {
                            case "NumSupportedLanguages":
                                {
                                    NumSupportedLanguages = Convert.ToInt16(xmlnode[i].ChildNodes[0].ChildNodes[j].InnerText);
                                    //                                    LanguageButtons = new Button[NumSupportedLanguages];
                                    Languages = new String[NumSupportedLanguages];
                                    Flags = new String[NumSupportedLanguages];
                                }
                                break;
                            case "Languages":
                                for (int k = 0; k < xmlnode[i].ChildNodes[0].ChildNodes[j].ChildNodes.Count; k++)
                                {
                                    Languages[k] = xmlnode[i].ChildNodes[0].ChildNodes[j].ChildNodes[k].InnerText;
                                }
                                break;
                            case "Flags":
                                for (int k = 0; k < xmlnode[i].ChildNodes[0].ChildNodes[j].ChildNodes.Count; k++)
                                {
                                    Flags[k] = xmlnode[i].ChildNodes[0].ChildNodes[j].ChildNodes[k].InnerText;
                                }
                                break;
                        }
                    }
                }
            }

            Button[] LanguageButtons = new Button[NumSupportedLanguages];

            int leftFlags = 0;
            if ((NumSupportedLanguages % 2) == 1)
                leftFlags = (NumSupportedLanguages - 1) / 2;
            else
                leftFlags = NumSupportedLanguages / 2;

            for (int i = 0; i < NumSupportedLanguages; i++)
            {
                // Todo : change the ratios for different screens
                LanguageButtons[i] = new Button();
                LanguageButtons[i].Top = (this.Height * 2/3);
                LanguageButtons[i].Name = Languages[i];
                LanguageButtons[i].BackColor = Color.Red;
                LanguageButtons[i].Click += new EventHandler(keypressedHandler);

                LanguageButtons[i].BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.FlagsDirectory + Flags[i] + "_Flag_112x70.tiff");

/*
                switch (Flags[i])
                {
                    case "SaudiArabia":
                        LanguageButtons[i].BackgroundImage = Image.FromFile(Properties.Settings.Default.FlagsDirectory + "Saudi_Flag_112x70.tiff");
                        break;
                    case "Qatar":
                        LanguageButtons[i].BackgroundImage = Image.FromFile(Properties.Settings.Default.FlagsDirectory + "Qatar_Flag_112x70.tiff");
                        break;
                    case "Australia":
                        LanguageButtons[i].BackgroundImage = Image.FromFile(Properties.Settings.Default.FlagsDirectory + "GB_Flag_112x70.tiff");
                        break;
                    case "UK":
                        LanguageButtons[i].BackgroundImage = Image.FromFile(Properties.Settings.Default.FlagsDirectory + "GB_Flag_112x70.tiff");
                        break;
                    case "AUS":
                        LanguageButtons[i].BackgroundImage = Image.FromFile(Properties.Settings.Default.FlagsDirectory + "AUS_Flag_112x70.tiff");
                        break;
                }
*/                LanguageButtons[i].Size = new Size(LanguageButtons[i].BackgroundImage.Width, LanguageButtons[i].BackgroundImage.Height);

                if ((NumSupportedLanguages % 2) == 1)       // Odd number of flags
                {
                    if (i < leftFlags)
                    {
                        // Close Spacing
//                        LanguageButtons[i].Left = (this.Width / 2) - (LanguageButtons[i].BackgroundImage.Width / 2) - ((leftFlags - i) * (LanguageButtons[i].BackgroundImage.Width));
                        // Large Spacing
                        LanguageButtons[i].Left = (this.Width / 2) - (LanguageButtons[i].BackgroundImage.Width) - ((leftFlags - i) * (LanguageButtons[i].BackgroundImage.Width));
                    }
                    else
                    {
                        if (i == leftFlags)
                            LanguageButtons[i].Left = (this.Width / 2) - (LanguageButtons[i].BackgroundImage.Width / 2);
                        else
                        {
                            // Close Spacing
//                            LanguageButtons[i].Left = (this.Width / 2) - (LanguageButtons[i].BackgroundImage.Width / 2) + ((i - leftFlags) * (LanguageButtons[i].BackgroundImage.Width));
                            // Close Spacing
                            LanguageButtons[i].Left = (this.Width / 2) + ((i - leftFlags) * (LanguageButtons[i].BackgroundImage.Width));
                        }
                    }
                }
                else                    // Even number of flags
                {
                    if (i < leftFlags)
                        LanguageButtons[i].Left = (this.Width / 2) - (leftFlags * (LanguageButtons[i].BackgroundImage.Width / 2)) - (LanguageButtons[i].BackgroundImage.Width / 2);
                    else
                        LanguageButtons[i].Left = (this.Width / 2) + (leftFlags * (LanguageButtons[i].BackgroundImage.Width / 2)) + (LanguageButtons[i].BackgroundImage.Width / 2);
                }

                this.Controls.Add(LanguageButtons[i]);
                //                LanguageButtons[i].Visible = true;
            }

// SMC - shouldnt be in this code. 
//            this.smartpay = smartpay;

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();

            //ManualResetEvent syncEvent = new ManualResetEvent(false);
            //Thread t1 = new Thread(
            //    () =>
            //    {
            //        // Do some work...
            //        this.smartpay.VALIDATOR.DisableValidator();
            //        syncEvent.Set();
            //        CHelpers.Pause(200);                }
            //);
            //t1.Start();

            //Thread t2 = new Thread(
            //    () =>
            //    {
            //        syncEvent.WaitOne();
            //        this.smartpay.HOPPER.DisableValidator();
            //        CHelpers.Pause(200);
            //        // Do some work...
            //    }
            //);
            //t2.Start();
        }

        private void welcome_Load(object sender, EventArgs e)
        {
            string name = Properties.Settings.Default.NoteRecycle;
            string[] sArr = name.Split(' ');

            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                Globals.SendCommsLogReport("AllowWindowFocusChange = True");
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                Globals.SendCommsLogReport("AllowWindowFocusChange = False");
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

/*          // This should be at the main state machine. 
            try
            {
                this.smartpay.VALIDATOR.ChangeNoteRoute(Int32.Parse(sArr[0]) * 100, sArr[1].ToCharArray(), false);
            }
            catch (Exception ex)
            {
                Globals.LogFile("ChangeNoteRoute:" + ex.ToString(), "PaysystemLog.txt");
                //MessageBox.Show(ex.ToString());
                return;
            }
*/
        }

        private void welcome_Click(object sender, EventArgs e)
        {
//            this.DialogResult = DialogResult.OK;
//            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }
    }
}
