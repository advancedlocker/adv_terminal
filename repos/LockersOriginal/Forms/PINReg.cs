﻿using System;
using System.Globalization;
using System.Windows.Forms;

namespace Lockers
{
    public partial class PINReg : Form
    {

        string m_PIN;
        string m_PIN2;
        int Regmode = 0;

        int[] Keymap = new int[10];

        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        public PINReg()
        {
            InitializeComponent();

            this.BackgroundImage = ConfigurationData.GetBackground("PINReg" + Globals.LanguageCode.ToString());
            bOK.Visible = false;

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();


            // Randomise the buttons
            RandomiseButtons();
        }


        private void RandomiseButtons()
        {
            bOK.Visible = false;

            // Randomise the numbers
            Random randomGen = new Random();
            Boolean RandomisationFinished = false;
            int[] CheckMap = new int[10];

            for (int count = 0; count < 10; count++)
            {
                Keymap[count] = 10;
                CheckMap[count] = 0;
            }

            int randomSeqA = randomGen.Next(0123456789, 2147483647);      // Random 32bit number
            int tmp = randomSeqA;

            for (int count = 0; count < 10; count++)
            {
                int CellValue = randomSeqA % 10;
                randomSeqA /= 10;
                if (CheckMap[CellValue] == 0)
                {
                    CheckMap[CellValue] = 1;
                    Keymap[count] = CellValue;
                }
            }

            RandomisationFinished = true;
            for (int count = 0; count < 10; count++)
            {
                if (CheckMap[count] == 0)
                    RandomisationFinished = false;
            }

            while (RandomisationFinished == false)
            {
                randomSeqA = randomGen.Next(0123456789, 2147483647);      // Random 32bit number
                                                                          //                int randomSeqB = randomGen.Next(2147483647, 2147483647);      // Random 32bit number

                tmp = randomSeqA;

                for (int count = 0; count < 10; count++)
                {
                    int CellValue = randomSeqA % 10;
                    randomSeqA /= 10;
                    if (CheckMap[CellValue] == 0)
                    {
                        CheckMap[CellValue] = 1;
                        for (int j = 0; j < 10; j++)
                        {
                            if (Keymap[j] == 10)
                            {
                                Keymap[j] = CellValue;
                                break;
                            }
                        }
                    }
                }

                // Crawl checkkeymap to add the missing numbers
                RandomisationFinished = true;
                for (int count = 0; count < 10; count++)
                {
                    if (CheckMap[count] == 0)
                        RandomisationFinished = false;
                }
            }

            bNumber0.Text = Keymap[0].ToString();
            bNumber1.Text = Keymap[1].ToString();
            bNumber2.Text = Keymap[2].ToString();
            bNumber3.Text = Keymap[3].ToString();
            bNumber4.Text = Keymap[4].ToString();
            bNumber5.Text = Keymap[5].ToString();
            bNumber6.Text = Keymap[6].ToString();
            bNumber7.Text = Keymap[7].ToString();
            bNumber8.Text = Keymap[8].ToString();
            bNumber9.Text = Keymap[9].ToString();

            this.Update();
        }

        private void bNumeric_Click(object sender, EventArgs e)
        {
            lblmsg.Text = "";
            lblmsg.Visible = false;

            Button b = (Button)sender;

            if (b.Name == "bClear")
            {
                lbl1.Text = "";
                bOK.Visible = false;
                if (Regmode == 0)
                    m_PIN = "";
                else
                    m_PIN2 = "";
            }
            else
            {
                if (lbl1.Text.Length < Properties.Settings.Default.MaxUserPinLen)
                {
                    lbl1.Text += "*";
                    if (Regmode == 0)
                    {
                        m_PIN += b.Text;
                        if (m_PIN.Length >= Properties.Settings.Default.MinUserPinLength)
                            bOK.Visible = true;
                        else
                            bOK.Visible = false;
                    }
                    else
                    {
                        m_PIN2 += b.Text;
                        if (m_PIN2.Length >= Properties.Settings.Default.MinUserPinLength)
                            bOK.Visible = true;
                        else
                            bOK.Visible = false;
                    }
                }
            }
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            switch (Regmode)
            {
                case 0:
                    RandomiseButtons();
                    Regmode = 1;
                    lblmsg.Text = "Please re-enter your Pin Code";
                    lblmsg.Visible = true;
                    lbl1.Text = "";
                    break;

                case 1:
                    if (m_PIN2 == m_PIN)
                    {
                        Globals.LockData.PINCode = m_PIN;
                        timer1.Stop();
                        this.DialogResult = DialogResult.OK;
//                        this.Close();
                    }
                    else
                    {

                        lblmsg.Text = "Second PIN Number not match";
                        lblmsg.Visible = true;
                        bOK.Visible = false;
                        m_PIN2 = "";
                        lbl1.Text = "";
                    }
                    break;
            }
        }

        private void PINReg_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
        }

        private void bBack_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            timer1.Stop();
            this.DialogResult = DialogResult.Cancel;
//            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }
    }
}
