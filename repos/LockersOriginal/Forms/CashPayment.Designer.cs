﻿namespace Lockers
{
    partial class CashPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CashPayment));
            this.bCancel = new System.Windows.Forms.Button();
            this.lbldue = new System.Windows.Forms.Label();
            this.lblpay = new System.Windows.Forms.Label();
            this.lblchange = new System.Windows.Forms.Label();
            this.bOK = new System.Windows.Forms.Button();
            this.bTestPay = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bCancel
            // 
            this.bCancel.BackColor = System.Drawing.Color.Transparent;
            this.bCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bCancel.BackgroundImage")));
            this.bCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.FlatAppearance.BorderSize = 0;
            this.bCancel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bCancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCancel.Location = new System.Drawing.Point(35, 642);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(79, 111);
            this.bCancel.TabIndex = 2;
            this.bCancel.UseVisualStyleBackColor = false;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // lbldue
            // 
            this.lbldue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbldue.BackColor = System.Drawing.Color.Transparent;
            this.lbldue.Font = new System.Drawing.Font("Microsoft Sans Serif", 58F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldue.Location = new System.Drawing.Point(687, 208);
            this.lbldue.Name = "lbldue";
            this.lbldue.Size = new System.Drawing.Size(287, 99);
            this.lbldue.TabIndex = 4;
            this.lbldue.Text = "0.00";
            this.lbldue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblpay
            // 
            this.lblpay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblpay.BackColor = System.Drawing.Color.Transparent;
            this.lblpay.Font = new System.Drawing.Font("Microsoft Sans Serif", 58F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblpay.Location = new System.Drawing.Point(687, 331);
            this.lblpay.Name = "lblpay";
            this.lblpay.Size = new System.Drawing.Size(287, 99);
            this.lblpay.TabIndex = 4;
            this.lblpay.Text = "0.00";
            this.lblpay.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblchange
            // 
            this.lblchange.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblchange.BackColor = System.Drawing.Color.Transparent;
            this.lblchange.Font = new System.Drawing.Font("Microsoft Sans Serif", 58F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblchange.Location = new System.Drawing.Point(687, 467);
            this.lblchange.Name = "lblchange";
            this.lblchange.Size = new System.Drawing.Size(287, 99);
            this.lblchange.TabIndex = 4;
            this.lblchange.Text = "0.00";
            this.lblchange.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            this.bOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bOK.BackgroundImage")));
            this.bOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bOK.Enabled = false;
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bOK.Location = new System.Drawing.Point(907, 642);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(81, 112);
            this.bOK.TabIndex = 6;
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Visible = false;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bTestPay
            // 
            this.bTestPay.Location = new System.Drawing.Point(755, 663);
            this.bTestPay.Name = "bTestPay";
            this.bTestPay.Size = new System.Drawing.Size(75, 69);
            this.bTestPay.TabIndex = 7;
            this.bTestPay.Text = "Test pay";
            this.bTestPay.UseVisualStyleBackColor = true;
            this.bTestPay.Visible = false;
            this.bTestPay.Click += new System.EventHandler(this.bTestPayout_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 164);
            this.panel1.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "TIME";
            // 
            // button14
            // 
            this.button14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button14.BackgroundImage")));
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button14.Location = new System.Drawing.Point(928, 24);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(79, 81);
            this.button14.TabIndex = 2;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(7, 47);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(0, 55);
            this.lbltime.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(940, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 29);
            this.label2.TabIndex = 20;
            this.label2.Text = "help";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // CashPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(254)))), ((int)(((byte)(205)))));
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.bTestPay);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.lblchange);
            this.Controls.Add(this.lblpay);
            this.Controls.Add(this.lbldue);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "CashPayment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CashPayment";
            this.Load += new System.EventHandler(this.CashPayment_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CashPayment_KeyPress);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label lbldue;
        private System.Windows.Forms.Label lblpay;
        private System.Windows.Forms.Label lblchange;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bTestPay;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
    }
}