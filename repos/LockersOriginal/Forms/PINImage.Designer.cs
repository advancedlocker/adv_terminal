﻿namespace Lockers
{
    partial class PINImage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PINImage));
            this.bOK = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.labTimeHead = new System.Windows.Forms.Label();
            this.bHelp = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bImage1 = new System.Windows.Forms.Button();
            this.bImage2 = new System.Windows.Forms.Button();
            this.bImage3 = new System.Windows.Forms.Button();
            this.bImage4 = new System.Windows.Forms.Button();
            this.bImage5 = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            this.bOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bOK.BackgroundImage")));
            this.bOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bOK.Location = new System.Drawing.Point(907, 642);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(81, 112);
            this.bOK.TabIndex = 0;
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            this.bBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBack.BackgroundImage")));
            this.bBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBack.Location = new System.Drawing.Point(35, 642);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(79, 111);
            this.bBack.TabIndex = 2;
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.labTimeHead);
            this.panel1.Controls.Add(this.bHelp);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 143);
            this.panel1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(940, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 29);
            this.label2.TabIndex = 3;
            this.label2.Text = "help";
            // 
            // labTimeHead
            // 
            this.labTimeHead.AutoSize = true;
            this.labTimeHead.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHead.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labTimeHead.ForeColor = System.Drawing.Color.White;
            this.labTimeHead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labTimeHead.Location = new System.Drawing.Point(12, 27);
            this.labTimeHead.Name = "labTimeHead";
            this.labTimeHead.Size = new System.Drawing.Size(71, 29);
            this.labTimeHead.TabIndex = 1;
            this.labTimeHead.Text = "TIME";
            // 
            // bHelp
            // 
            this.bHelp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bHelp.BackgroundImage")));
            this.bHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bHelp.FlatAppearance.BorderSize = 0;
            this.bHelp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bHelp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bHelp.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bHelp.Location = new System.Drawing.Point(928, 24);
            this.bHelp.Name = "bHelp";
            this.bHelp.Size = new System.Drawing.Size(79, 81);
            this.bHelp.TabIndex = 2;
            this.bHelp.UseVisualStyleBackColor = true;
            this.bHelp.Click += new System.EventHandler(this.bHelp_Click);
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(7, 47);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(0, 55);
            this.lbltime.TabIndex = 1;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // bImage1
            // 
            this.bImage1.BackColor = System.Drawing.Color.Transparent;
            this.bImage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bImage1.FlatAppearance.BorderSize = 0;
            this.bImage1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bImage1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bImage1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bImage1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bImage1.Location = new System.Drawing.Point(48, 442);
            this.bImage1.Name = "bImage1";
            this.bImage1.Size = new System.Drawing.Size(115, 115);
            this.bImage1.TabIndex = 7;
            this.bImage1.UseVisualStyleBackColor = false;
            this.bImage1.Click += new System.EventHandler(this.bImage_Click);
            // 
            // bImage2
            // 
            this.bImage2.BackColor = System.Drawing.Color.Transparent;
            this.bImage2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bImage2.FlatAppearance.BorderSize = 0;
            this.bImage2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bImage2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bImage2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bImage2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bImage2.Location = new System.Drawing.Point(251, 442);
            this.bImage2.Name = "bImage2";
            this.bImage2.Size = new System.Drawing.Size(115, 115);
            this.bImage2.TabIndex = 8;
            this.bImage2.UseVisualStyleBackColor = false;
            this.bImage2.Click += new System.EventHandler(this.bImage_Click);
            // 
            // bImage3
            // 
            this.bImage3.BackColor = System.Drawing.Color.Transparent;
            this.bImage3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bImage3.FlatAppearance.BorderSize = 0;
            this.bImage3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bImage3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bImage3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bImage3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bImage3.Location = new System.Drawing.Point(458, 442);
            this.bImage3.Name = "bImage3";
            this.bImage3.Size = new System.Drawing.Size(115, 115);
            this.bImage3.TabIndex = 9;
            this.bImage3.UseVisualStyleBackColor = false;
            this.bImage3.Click += new System.EventHandler(this.bImage_Click);
            // 
            // bImage4
            // 
            this.bImage4.BackColor = System.Drawing.Color.Transparent;
            this.bImage4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bImage4.FlatAppearance.BorderSize = 0;
            this.bImage4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bImage4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bImage4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bImage4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bImage4.Location = new System.Drawing.Point(662, 442);
            this.bImage4.Name = "bImage4";
            this.bImage4.Size = new System.Drawing.Size(115, 115);
            this.bImage4.TabIndex = 10;
            this.bImage4.UseVisualStyleBackColor = false;
            this.bImage4.Click += new System.EventHandler(this.bImage_Click);
            // 
            // bImage5
            // 
            this.bImage5.BackColor = System.Drawing.Color.Transparent;
            this.bImage5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bImage5.FlatAppearance.BorderSize = 0;
            this.bImage5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bImage5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bImage5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bImage5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bImage5.Location = new System.Drawing.Point(864, 442);
            this.bImage5.Name = "bImage5";
            this.bImage5.Size = new System.Drawing.Size(115, 115);
            this.bImage5.TabIndex = 11;
            this.bImage5.UseVisualStyleBackColor = false;
            this.bImage5.Click += new System.EventHandler(this.bImage_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Lockers.Properties.Resources.IconTransSelect;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(14, 408);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(185, 185);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(218, 408);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(185, 185);
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(422, 408);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(185, 185);
            this.pictureBox3.TabIndex = 19;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(626, 408);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(185, 185);
            this.pictureBox4.TabIndex = 20;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Visible = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(830, 408);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(185, 185);
            this.pictureBox5.TabIndex = 21;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            // 
            // PINImage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.bImage5);
            this.Controls.Add(this.bImage4);
            this.Controls.Add(this.bImage3);
            this.Controls.Add(this.bImage2);
            this.Controls.Add(this.bImage1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PINImage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PINImage";
            this.Load += new System.EventHandler(this.PINImage_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labTimeHead;
        private System.Windows.Forms.Button bHelp;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button bImage1;
        private System.Windows.Forms.Button bImage2;
        private System.Windows.Forms.Button bImage3;
        private System.Windows.Forms.Button bImage4;
        private System.Windows.Forms.Button bImage5;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
    }
}