﻿using System;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace Lockers
{
    public partial class LockerController : Form
    {

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        Locker_TCPClient client;
        TextBox t;
        int mCRC16 = Properties.Settings.Default.CRC16;

        public LockerController()
        {
            InitializeComponent();
            txtIP.Text = Properties.Settings.Default.ControllerIP;
            txtPort.Text = Properties.Settings.Default.ControllerPort.ToString();

            client = Globals.client;
        }

        private void LockerController_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
            this.BackgroundImage = ConfigurationData.GetBackground("Blank");
        }

#if (true)
        void client_ConnectionStatusChanged(Locker_TCPClient sender, Locker_TCPClient.ConnectionStatus status)
        {
            //Check if this event was fired on a different thread, if it is then we must invoke it on the UI thread
            if (InvokeRequired)
            {
                Invoke(new Locker_TCPClient.delConnectionStatusChanged(client_ConnectionStatusChanged), sender, status);
                return;
            }
            richTextBox1.Text += "Connection: " + status.ToString() + Environment.NewLine;
            if (status.ToString() == Locker_TCPClient.ConnectionStatus.Connected.ToString())
            {
                button1.Enabled = false;
            }
            
        }
        //Fired when new data is received in the TCP client
        void client_DataReceived(Locker_TCPClient sender, object data)
        {
            //Again, check if this needs to be invoked in the UI thread
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new Locker_TCPClient.delDataReceived(client_DataReceived), sender, data);
                }
                catch
                { }
                return;
            }
            //Interpret the received data object as a string
            string strData = data as string;
            string values = strData.Split('-').Select(sValue => sValue.Trim()).First();
                        
            //Add the received data to a rich text box
            richTextBox1.Text += strData + Environment.NewLine;
/*
            if (client.LockerCommand == Locker_cmd.Basic.GetSession)
            {
                txtKey.Text = strData;

            }
*/
            //switch (values)
            //{
            //    case "GetSessionKey":
            //        
            //        break;
            //    case "AssignLock":
            //       
            //    case "UnassignLock":
            //        
            //        break;
            //    case "GetAvailableLocks":
            //        
            //        break;
            //    case "GetUsedLocks":
            //       
            //        break;
            //    case "OpenLock":
            //        
            //        break;
            //    case "CloseLock":
            //        
            //        break;
            //    case "GetLockStatus":
            //        
            //        break;
            //    default:
            //       
            //        break;

            //}
        }
#endif
        private void bbtnNum_Click(object sender, EventArgs e)
        {
            if (t == null) return;

            Button b = (Button)sender;
            if (b.Text == "")
            {
                if (t.Text.Length > 1)
                {
                    t.Text = t.Text.Substring(0, t.Text.Length - 1);
                }
                else
                {
                    t.Text = "";
                }

            }
            else
            {
                if (t.Text == "0") t.Clear();
                t.Text += b.Text;
            }
        
        }

        private void txtField_Enter(object sender, EventArgs e)
        {
            t = (TextBox)sender;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            client.SourceKey = Properties.Settings.Default.sourceKey;
            client.GetSession();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            client = Globals.client;
            if (client != null)
            {
                client.DataReceived -= new Locker_TCPClient.delDataReceived(Globals.Client_DataReceived);
                client.ConnectionStatusChanged -= new Locker_TCPClient.delConnectionStatusChanged(Globals.Client_ConnectionStatusChanged);
                client.Dispose();
            }

            //Initialize the events
            Globals.client = new Locker_TCPClient(IPAddress.Parse(txtIP.Text), int.Parse(txtPort.Text),false);
            client = Globals.client;
            client.DataReceived += new Locker_TCPClient.delDataReceived(Globals.Client_DataReceived);
            client.ConnectionStatusChanged += new Locker_TCPClient.delConnectionStatusChanged(Globals.Client_ConnectionStatusChanged);

            client.Connect();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button1.Enabled = true;
            Globals.client.Disconnect();
        }
        /// <summary>
        /// assign locker 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if (txtTo.Text.Trim() == "0" || txtTo.Text.Trim() == "")
            {
                return;
            }
            if (txtpwd.Text  == "") return;

            //            client.AssignLock(int.Parse(txtKey.Text), int.Parse(txtTo.Text), 0, int.Parse(txtpwd.Text), mCRC16);
            Globals.client.AssignLock(int.Parse(txtTo.Text), 0, int.Parse(txtpwd.Text));

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (txtTo.Text.Trim() == "0" || txtTo.Text.Trim() == "")
            {
                return;
            }
            if (txtpwd.Text == "") return;
            if (txtKey.Text == "") return;
            //            client.Unassignlock(int.Parse(txtKey.Text), int.Parse(txtTo.Text), mCRC16);
            Globals.client.Unassignlock(int.Parse(txtTo.Text));
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //if (txtTo.Text.Trim() == "0" || txtTo.Text.Trim() == "")
            //{
            //    return;
            //}

//            if (txtKey.Text  == "") return;
            //            client.GetAvailableLocks(int.Parse(txtKey.Text), mCRC16);
            Globals.client.GetAvailableLocks();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            //if (txtTo.Text.Trim() == "0" || txtTo.Text.Trim() == "")
            //{
            //    return;
            //}

            if (txtKey.Text == "") return;
            //            client.GetUsedLocks(int.Parse(txtKey.Text), mCRC16);
            Globals.client.GetUsedLocks();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (txtTo.Text.Trim() == "0" || txtTo.Text.Trim() == "")
            {
                return;
            }

            if (txtKey.Text == "") return;
            //            client.OpenLock(int.Parse(txtKey.Text), int.Parse(txtTo.Text), mCRC16);
            Globals.client.OpenLock(int.Parse(txtTo.Text));
        }

        private void button20_Click(object sender, EventArgs e)
        {
            if (txtTo.Text.Trim() == "0" || txtTo.Text.Trim() == "")
            {
                return;
            }

            if (txtKey.Text == "") return;
            //            client.CloseLock(int.Parse(txtKey.Text), int.Parse(txtTo.Text), mCRC16);
            Globals.client.CloseLock(int.Parse(txtTo.Text));
        }

        private void button21_Click(object sender, EventArgs e)
        {
            if (txtTo.Text.Trim() == "0" || txtTo.Text.Trim() == "")
            {
                return;
            }

            if (txtKey.Text == "") return;
            //            client.GetLockStatus(int.Parse(txtKey.Text), int.Parse(txtTo.Text), mCRC16);
            Globals.client.GetLockStatus(int.Parse(txtTo.Text));
        }

        private void button22_Click(object sender, EventArgs e)
        {
            if (txtTo.Text.Trim() == "0" || txtTo.Text.Trim() == "")
            {
                return;
            }

            if (txtKey.Text == "") return;
            //            client.GetLEDStatus(int.Parse(txtKey.Text), int.Parse(txtTo.Text), mCRC16);
            Globals.client.GetLEDStatus(int.Parse(txtTo.Text));
        }
    }
}
