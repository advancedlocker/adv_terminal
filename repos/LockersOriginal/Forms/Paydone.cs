﻿using System;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;

namespace Lockers
{
    public partial class Paydone : Form
    {
        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        public Paydone()
        {
            InitializeComponent();

            String ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\PaymentComplete.tiff";
            this.BackgroundImage = Image.FromFile(ImageFilename);

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);

            timer1.Start();

        }

        private void button10_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void Paydone_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            lblLocknum.Text = Globals.LockData.LockNumber.ToString();
//            lblremaintime.Text = string.Format("{0:00}:{1:00}", Globals.LockData.Hours ,Globals.LockData.Minutes);
        }
    }
}
