﻿namespace Lockers
{
    partial class SelectSize
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectSize));
            this.bSmallSize = new System.Windows.Forms.Button();
            this.bMediumSize = new System.Windows.Forms.Button();
            this.bLargeSize = new System.Windows.Forms.Button();
            this.bExtraLargewSize = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.bBack = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.bHelp = new System.Windows.Forms.Button();
            this.labTimeHead = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bSmallSize
            // 
            this.bSmallSize.BackColor = System.Drawing.Color.Transparent;
            this.bSmallSize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bSmallSize.BackgroundImage")));
            this.bSmallSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bSmallSize.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.bSmallSize.FlatAppearance.BorderSize = 0;
            this.bSmallSize.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bSmallSize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGreen;
            this.bSmallSize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bSmallSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bSmallSize.Location = new System.Drawing.Point(45, 352);
            this.bSmallSize.Margin = new System.Windows.Forms.Padding(0);
            this.bSmallSize.Name = "bSmallSize";
            this.bSmallSize.Size = new System.Drawing.Size(223, 252);
            this.bSmallSize.TabIndex = 0;
            this.bSmallSize.Tag = "SMALL";
            this.bSmallSize.UseVisualStyleBackColor = false;
            this.bSmallSize.Click += new System.EventHandler(this.btnSize_Click);
            // 
            // bMediumSize
            // 
            this.bMediumSize.BackColor = System.Drawing.Color.Transparent;
            this.bMediumSize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bMediumSize.BackgroundImage")));
            this.bMediumSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bMediumSize.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.bMediumSize.FlatAppearance.BorderSize = 0;
            this.bMediumSize.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bMediumSize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGreen;
            this.bMediumSize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bMediumSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bMediumSize.Location = new System.Drawing.Point(280, 301);
            this.bMediumSize.Name = "bMediumSize";
            this.bMediumSize.Size = new System.Drawing.Size(222, 303);
            this.bMediumSize.TabIndex = 0;
            this.bMediumSize.Tag = "MEDIUM";
            this.bMediumSize.UseVisualStyleBackColor = false;
            this.bMediumSize.Click += new System.EventHandler(this.btnSize_Click);
            // 
            // bLargeSize
            // 
            this.bLargeSize.BackColor = System.Drawing.Color.Transparent;
            this.bLargeSize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bLargeSize.BackgroundImage")));
            this.bLargeSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bLargeSize.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.bLargeSize.FlatAppearance.BorderSize = 0;
            this.bLargeSize.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bLargeSize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGreen;
            this.bLargeSize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bLargeSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bLargeSize.Location = new System.Drawing.Point(514, 249);
            this.bLargeSize.Name = "bLargeSize";
            this.bLargeSize.Size = new System.Drawing.Size(225, 355);
            this.bLargeSize.TabIndex = 0;
            this.bLargeSize.Tag = "LARGE";
            this.bLargeSize.UseVisualStyleBackColor = false;
            this.bLargeSize.Click += new System.EventHandler(this.btnSize_Click);
            // 
            // bExtraLargewSize
            // 
            this.bExtraLargewSize.BackColor = System.Drawing.Color.Transparent;
            this.bExtraLargewSize.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bExtraLargewSize.BackgroundImage")));
            this.bExtraLargewSize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bExtraLargewSize.FlatAppearance.BorderColor = System.Drawing.Color.LightGreen;
            this.bExtraLargewSize.FlatAppearance.BorderSize = 0;
            this.bExtraLargewSize.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bExtraLargewSize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.LightGreen;
            this.bExtraLargewSize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bExtraLargewSize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bExtraLargewSize.Location = new System.Drawing.Point(753, 194);
            this.bExtraLargewSize.Name = "bExtraLargewSize";
            this.bExtraLargewSize.Size = new System.Drawing.Size(234, 410);
            this.bExtraLargewSize.TabIndex = 0;
            this.bExtraLargewSize.Tag = "XLARGE";
            this.bExtraLargewSize.UseVisualStyleBackColor = false;
            this.bExtraLargewSize.Click += new System.EventHandler(this.btnSize_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            this.bBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBack.BackgroundImage")));
            this.bBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBack.Location = new System.Drawing.Point(35, 642);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(79, 111);
            this.bBack.TabIndex = 1;
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(7, 47);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(0, 55);
            this.lbltime.TabIndex = 1;
            // 
            // bHelp
            // 
            this.bHelp.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bHelp.BackgroundImage")));
            this.bHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bHelp.FlatAppearance.BorderSize = 0;
            this.bHelp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bHelp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bHelp.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bHelp.Location = new System.Drawing.Point(928, 24);
            this.bHelp.Name = "bHelp";
            this.bHelp.Size = new System.Drawing.Size(79, 81);
            this.bHelp.TabIndex = 2;
            this.bHelp.UseVisualStyleBackColor = true;
            // 
            // labTimeHead
            // 
            this.labTimeHead.AutoSize = true;
            this.labTimeHead.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHead.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labTimeHead.ForeColor = System.Drawing.Color.White;
            this.labTimeHead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labTimeHead.Location = new System.Drawing.Point(12, 27);
            this.labTimeHead.Name = "labTimeHead";
            this.labTimeHead.Size = new System.Drawing.Size(71, 29);
            this.labTimeHead.TabIndex = 1;
            this.labTimeHead.Text = "TIME";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.labTimeHead);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.bHelp);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 146);
            this.panel1.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(939, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "help";
            // 
            // SelectSize
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bExtraLargewSize);
            this.Controls.Add(this.bLargeSize);
            this.Controls.Add(this.bMediumSize);
            this.Controls.Add(this.bSmallSize);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectSize";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SelectSize";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.SelectSize_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bSmallSize;
        private System.Windows.Forms.Button bMediumSize;
        private System.Windows.Forms.Button bLargeSize;
        private System.Windows.Forms.Button bExtraLargewSize;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Button bHelp;
        private System.Windows.Forms.Label labTimeHead;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
    }
}