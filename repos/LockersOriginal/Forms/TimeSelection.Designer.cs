﻿namespace Lockers
{
    partial class TimeSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimeSelection));
            this.btnAddHrs = new System.Windows.Forms.Button();
            this.btnaddMin = new System.Windows.Forms.Button();
            this.btnMinusMin = new System.Windows.Forms.Button();
            this.btnMinusHrs = new System.Windows.Forms.Button();
            this.txthrs = new System.Windows.Forms.TextBox();
            this.txtmin = new System.Windows.Forms.TextBox();
            this.lblCost = new System.Windows.Forms.Label();
            this.bBack = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lblrate = new System.Windows.Forms.Label();
            this.lblsize = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labTimeHead = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddHrs
            // 
            this.btnAddHrs.BackColor = System.Drawing.Color.Transparent;
            this.btnAddHrs.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddHrs.BackgroundImage")));
            this.btnAddHrs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAddHrs.FlatAppearance.BorderSize = 0;
            this.btnAddHrs.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnAddHrs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnAddHrs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddHrs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddHrs.Location = new System.Drawing.Point(456, 146);
            this.btnAddHrs.Name = "btnAddHrs";
            this.btnAddHrs.Size = new System.Drawing.Size(79, 80);
            this.btnAddHrs.TabIndex = 0;
            this.btnAddHrs.UseVisualStyleBackColor = false;
            this.btnAddHrs.Click += new System.EventHandler(this.btnAddHrs_Click);
            // 
            // btnaddMin
            // 
            this.btnaddMin.BackColor = System.Drawing.Color.Transparent;
            this.btnaddMin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnaddMin.BackgroundImage")));
            this.btnaddMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnaddMin.FlatAppearance.BorderSize = 0;
            this.btnaddMin.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnaddMin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnaddMin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnaddMin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnaddMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddMin.Location = new System.Drawing.Point(716, 146);
            this.btnaddMin.Name = "btnaddMin";
            this.btnaddMin.Size = new System.Drawing.Size(79, 80);
            this.btnaddMin.TabIndex = 0;
            this.btnaddMin.UseVisualStyleBackColor = false;
            this.btnaddMin.Click += new System.EventHandler(this.btnaddMin_Click);
            // 
            // btnMinusMin
            // 
            this.btnMinusMin.BackColor = System.Drawing.Color.Transparent;
            this.btnMinusMin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMinusMin.BackgroundImage")));
            this.btnMinusMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMinusMin.FlatAppearance.BorderSize = 0;
            this.btnMinusMin.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnMinusMin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnMinusMin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnMinusMin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinusMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinusMin.Location = new System.Drawing.Point(716, 427);
            this.btnMinusMin.Name = "btnMinusMin";
            this.btnMinusMin.Size = new System.Drawing.Size(85, 79);
            this.btnMinusMin.TabIndex = 0;
            this.btnMinusMin.UseVisualStyleBackColor = false;
            this.btnMinusMin.Click += new System.EventHandler(this.btnMinusMin_Click);
            // 
            // btnMinusHrs
            // 
            this.btnMinusHrs.BackColor = System.Drawing.Color.Transparent;
            this.btnMinusHrs.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMinusHrs.BackgroundImage")));
            this.btnMinusHrs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnMinusHrs.FlatAppearance.BorderSize = 0;
            this.btnMinusHrs.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnMinusHrs.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnMinusHrs.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnMinusHrs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinusHrs.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinusHrs.Location = new System.Drawing.Point(465, 427);
            this.btnMinusHrs.Name = "btnMinusHrs";
            this.btnMinusHrs.Size = new System.Drawing.Size(85, 79);
            this.btnMinusHrs.TabIndex = 0;
            this.btnMinusHrs.UseVisualStyleBackColor = false;
            this.btnMinusHrs.Click += new System.EventHandler(this.btnMinusHrs_Click);
            // 
            // txthrs
            // 
            this.txthrs.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txthrs.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txthrs.Location = new System.Drawing.Point(465, 286);
            this.txthrs.Name = "txthrs";
            this.txthrs.Size = new System.Drawing.Size(83, 91);
            this.txthrs.TabIndex = 1;
            this.txthrs.Text = "00";
            this.txthrs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtmin
            // 
            this.txtmin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmin.Location = new System.Drawing.Point(707, 286);
            this.txtmin.Name = "txtmin";
            this.txtmin.Size = new System.Drawing.Size(83, 91);
            this.txtmin.TabIndex = 1;
            this.txtmin.Text = "00";
            this.txtmin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCost
            // 
            this.lblCost.AutoSize = true;
            this.lblCost.BackColor = System.Drawing.Color.Transparent;
            this.lblCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCost.ForeColor = System.Drawing.Color.Black;
            this.lblCost.Location = new System.Drawing.Point(858, 551);
            this.lblCost.Margin = new System.Windows.Forms.Padding(0);
            this.lblCost.Name = "lblCost";
            this.lblCost.Size = new System.Drawing.Size(114, 53);
            this.lblCost.TabIndex = 3;
            this.lblCost.Text = "0.00";
            this.lblCost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            this.bBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBack.BackgroundImage")));
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBack.Location = new System.Drawing.Point(35, 642);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(79, 111);
            this.bBack.TabIndex = 4;
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bback_Click);
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            this.bOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bOK.BackgroundImage")));
            this.bOK.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bOK.Location = new System.Drawing.Point(907, 642);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(81, 112);
            this.bOK.TabIndex = 5;
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 34F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(814, 551);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 65);
            this.label5.TabIndex = 2;
            this.label5.Text = "$";
            // 
            // lblrate
            // 
            this.lblrate.AutoSize = true;
            this.lblrate.BackColor = System.Drawing.Color.Transparent;
            this.lblrate.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrate.ForeColor = System.Drawing.Color.White;
            this.lblrate.Location = new System.Drawing.Point(160, 545);
            this.lblrate.Name = "lblrate";
            this.lblrate.Size = new System.Drawing.Size(331, 59);
            this.lblrate.TabIndex = 2;
            this.lblrate.Text = "0.00 Per hour";
            // 
            // lblsize
            // 
            this.lblsize.AutoSize = true;
            this.lblsize.BackColor = System.Drawing.Color.Transparent;
            this.lblsize.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsize.ForeColor = System.Drawing.Color.White;
            this.lblsize.Location = new System.Drawing.Point(65, 167);
            this.lblsize.Margin = new System.Windows.Forms.Padding(0);
            this.lblsize.Name = "lblsize";
            this.lblsize.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblsize.Size = new System.Drawing.Size(249, 59);
            this.lblsize.TabIndex = 2;
            this.lblsize.Text = "X-LARGE";
            this.lblsize.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.labTimeHead);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 140);
            this.panel1.TabIndex = 23;
            // 
            // labTimeHead
            // 
            this.labTimeHead.AutoSize = true;
            this.labTimeHead.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHead.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.labTimeHead.ForeColor = System.Drawing.Color.White;
            this.labTimeHead.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labTimeHead.Location = new System.Drawing.Point(12, 27);
            this.labTimeHead.Name = "labTimeHead";
            this.labTimeHead.Size = new System.Drawing.Size(71, 29);
            this.labTimeHead.TabIndex = 1;
            this.labTimeHead.Text = "TIME";
            // 
            // button14
            // 
            this.button14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button14.BackgroundImage")));
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button14.Location = new System.Drawing.Point(928, 24);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(79, 81);
            this.button14.TabIndex = 2;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(4, 47);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(0, 55);
            this.lbltime.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(940, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 29);
            this.label2.TabIndex = 20;
            this.label2.Text = "help";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 38F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(117, 545);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 62);
            this.label3.TabIndex = 2;
            this.label3.Text = "$";
            // 
            // TimeSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(195)))), ((int)(((byte)(65)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lblsize);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtmin);
            this.Controls.Add(this.txthrs);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.lblCost);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblrate);
            this.Controls.Add(this.btnMinusHrs);
            this.Controls.Add(this.btnMinusMin);
            this.Controls.Add(this.btnaddMin);
            this.Controls.Add(this.btnAddHrs);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "TimeSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TimeSelection";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.TimeSelection_FormClosed);
            this.Load += new System.EventHandler(this.TimeSelection_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddHrs;
        private System.Windows.Forms.Button btnaddMin;
        private System.Windows.Forms.Button btnMinusMin;
        private System.Windows.Forms.Button btnMinusHrs;
        private System.Windows.Forms.TextBox txthrs;
        private System.Windows.Forms.TextBox txtmin;
        private System.Windows.Forms.Label lblCost;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblrate;
        private System.Windows.Forms.Label lblsize;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labTimeHead;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
    }
}