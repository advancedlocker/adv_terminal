﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
namespace Lockers
{
    public partial class SelectSize : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }
       

        DateTimeFormatInfo fi = new DateTimeFormatInfo();
        public SelectSize()
        {
            
            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";

            InitializeComponent();
            LoadBackgroundImage();
            //lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);

            Init_buttons();

            timer1.Start();

        }

        private void SelectSize_Load(object sender, EventArgs e)
        {

            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

//            this.timer1_Tick(this, EventArgs.Empty);
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            //System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ThreadStart(LoadBackgroundImage));
            //thrd.Start();

            //this.Refresh();
            this.Invalidate();
            
        }

        private void Init_buttons()
        {
            if (Globals.LockData.Lockers == null)
                Globals.LockData.Lockers = new DataTable();

            Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

            var Sizes = (from Rows in Globals.LockData.Lockers.AsEnumerable()
                         select Rows["lockersize"]).Distinct().ToList();

            if (!Sizes.Exists(e => e.ToString() == "0"))
            {
                bSmallSize.Enabled = false;
                bSmallSize.Visible = false;
                //                bSmallSize.BackgroundImage = AlterTransparent(bExtraLargewSize.BackgroundImage, 40);
            }
            else
            {
                bSmallSize.Enabled = true;
                bSmallSize.Visible = true;
            }

            if (!Sizes.Exists(e => e.ToString() == "1"))
            {
                bMediumSize.Enabled = false;
                bMediumSize.Visible = false;
                //                bMediumSize.BackgroundImage = AlterTransparent(bMediumSize.BackgroundImage, 40);
            }
            else
            {
                bMediumSize.Enabled = true;
                bMediumSize.Visible = true;
            }

            if (!Sizes.Exists(e => e.ToString() == "2"))
            {
                bLargeSize.Enabled = false;
                bLargeSize.Visible = false;
                //                bLargeSize.BackgroundImage = AlterTransparent(bLargeSize.BackgroundImage, 40);
            }
            else
            {
                bLargeSize.Enabled = true;
                bLargeSize.Visible = true;
            }

            if (!Sizes.Exists(e => e.ToString() == "3"))
            {
                bExtraLargewSize.Enabled = false;
                bExtraLargewSize.Visible = false;
                //                bExtraLargewSize.BackgroundImage = AlterTransparent(bExtraLargewSize.BackgroundImage, 40);
            }
            else
            {
                bExtraLargewSize.Enabled = true;
                bExtraLargewSize.Visible = true;
            }
        }

        private void Get_LockAvailable()
        {
            int lockersize = (int)Globals.LockData.Locker_Size;
            var mlock = (from Rows in Globals.LockData.Lockers.AsEnumerable()
                         where Rows.Field<string>("lockersize") == lockersize.ToString()
                         select new
                            {
                               rentLock = Rows.Field<int>("lockerNum")

                            }).OrderBy(x => Guid.NewGuid()).Take(1);
            Globals.LockData.LockNumber = mlock.First().rentLock;
        }


        public Bitmap AlterTransparent(Image image, byte alpha)
        {
            Bitmap original = new Bitmap(image);
            Bitmap transparentimage = new Bitmap(image.Width, image.Height);
            Color c = Color.Black;
            Color v = Color.Black;

            int av = 0;
            for (int i = 0; i < image.Width ; i++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    c = original.GetPixel(i, y);
                    v = Color.FromArgb(alpha, c.R, c.G, c.B);
                    transparentimage.SetPixel(i,y,v);

                }

            }
            return transparentimage;

        }

        private void LoadBackgroundImage()
        {

//            this.BackgroundImage = ConfigurationData.GetBackground("Size" + Globals.LanguageCode.ToString());
            String ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\locker_size_1182x789.tiff";
            this.BackgroundImage = Image.FromFile(ImageFilename);
        }

        private void btnSize_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            switch (b.Tag.ToString())
            {
                case "SMALL":
                    //Globals.LockData.Locker_Size
                   Globals.LockData.Locker_Size = LockerSize.SMALL;
                    break;
                case "MEDIUM":
                    Globals.LockData.Locker_Size = LockerSize.MEDIUM;
                    break;
                case "LARGE":
                    Globals.LockData.Locker_Size = LockerSize.LARGE ;
                    break;
                case "XLARGE":
                    Globals.LockData.Locker_Size = LockerSize.XLARGE;
                    break;

            }
            Get_LockAvailable();

            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void bBack_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            timer1.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        
        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }
    }
}
