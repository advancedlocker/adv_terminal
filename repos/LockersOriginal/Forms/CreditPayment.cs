﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

namespace Lockers
{
    public partial class CreditPayment : Form
    {
        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        protected override void OnClosed(EventArgs e)
        {
/*
            base.OnClosed(e);

            decimal mtender;
            decimal mchange;

            decimal.TryParse(lblpay.Text, out mtender);
            decimal.TryParse(lblchange.Text,out mchange);

            Globals.LockData.Amnt_Tender = mtender;
            Globals.LockData.Amnt_Change = mchange;

            this.totalpaid = 0;
            //Globals.Paymode = false;
*/

        }

        public CreditPayment()
        {
            InitializeComponent();

            String ImageFilename = "";
            if (Globals.CurrentLanguage == null)
                ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\CardPayment_1182x789.tiff";
            else
                ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\CardPayment_1182x789.tiff";
            this.BackgroundImage = Image.FromFile(ImageFilename);


            #region enablecashdevice

            //-----------------------------
            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();
            //------------------------------

            fi.AMDesignator = "am";

            timer1.Start();

/*
            Process process = new Process();
            process.StartInfo.FileName = "AdvamSerialDemo.exe";
            process.StartInfo.Arguments = " COM8 1.00";
            process.StartInfo.CreateNoWindow = false;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;

            process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);

            process.Start();

            string output = "";
            string err = "";

            try
            {
                output = process.StandardOutput.ReadToEnd();
//                Console.WriteLine(output);
                err = process.StandardError.ReadToEnd();
//                Console.WriteLine(err);
                process.WaitForExit();
            }
            catch(Exception Ex)
            {
                // Log error.
                Console.WriteLine(Ex.ToString());
            }


            if (output.Contains("(00) APPROVED"))
            {
                Console.WriteLine("(00) APPROVED");
            }

            if (output.Contains("(05) DECLINED"))
            {
                Console.WriteLine("(05) DECLINED");
            }
*/

            /*
                        Advam ADV = new Advam();

                        Decimal value = 2.00m;
                        ADV.AdvamPayment(value);
            */

            //            this.DialogResult = DialogResult.OK;
            #endregion
        }

        static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //* Do your stuff with the output (write to console/log/StringBuilder)
            Console.WriteLine(outLine.Data);
        }

        private void CCThread()
        {
/*
            BYTE byBufGeneric[256] = { 0 };

            strcpy(byBufGeneric, "AdvamSerialDemo - v0.0.1\n");
            printf(byBufGeneric);

            PMInit();

            CmdInit();
            CmdLinkTest();
            CmdPmtDet();
            CmdPmt();

            if (Res.Stats == STATS_OK)
            {
                strcpy(pbyStanCurrent, Res.pbyStan);
                Req.Stats = STATS_OK;
                CmdPmtConf();
            }

            CmdEject();
            CmdDisable();

            //for(i = 0; i < 258; i++)
            //{
            //	CmdLinkTest();
            //}

            PMDispose();

            printf("\n\n\nSequence Finished\n\n\n");
*/
        }


        private void updatePay(float value)
        {
            Globals.LogFile("updatePay method\n");

            lblpay.Text = value.ToString("#,##0.00");
        }

        private void enableButton(bool value)
        {
            Globals.LogFile("Enable OK button\n");

//            bOK.Enabled = value;
        }
        private void updatechange(Double  value)
        {
            Globals.LogFile("updatechange method\n");
//            lblchange.Text = value.ToString("#,##0.00");
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            Globals.LogFile("bCancel_Click event\n");

            Globals.AbortForm = true;
            this.DialogResult = DialogResult.Cancel;
            timer1.Stop();
            CHelpers.Shutdown = true;
            this.Close();
        }

        private void CreditPayment_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
            lbldue.Text = Globals.DueAmnt.ToString("#,##0.00");
        }

        private void CreditPayment_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString().ToUpper().Equals("P"))
            {
                // Quit - ends kiosk    program
                e.Handled = true;
                timer1.Stop();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            Process process = new Process();
            process.StartInfo.FileName = "AdvamSerialDemo.exe";
            process.StartInfo.Arguments = " COM8 " + Globals.DueAmnt.ToString();
            process.StartInfo.CreateNoWindow = false;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;

            process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);

            process.Start();

            string output = "";
            string err = "";

            try
            {
                output = process.StandardOutput.ReadToEnd();
                //                Console.WriteLine(output);
                err = process.StandardError.ReadToEnd();
                //                Console.WriteLine(err);
                process.WaitForExit();
            }
            catch (Exception Ex)
            {
                // Log error.
                Console.WriteLine(Ex.ToString());
            }


            if (output.Contains("(00) APPROVED"))
            {
                this.DialogResult = DialogResult.OK;
                Console.WriteLine("(00) APPROVED");
            }

            if (output.Contains("(05) DECLINED"))
            {
                this.DialogResult = DialogResult.Cancel;
                Console.WriteLine("(05) DECLINED");
            }

            // SMC added
            timer1.Stop();
//            CHelpers.Shutdown = true;
            this.Close();
        }

        private void bTestPayout_Click(object sender, EventArgs e)
        {
            Globals.smartpay.Payout = (float)(2.00) * 100;// (float)changeamnt;
            Globals.smartpay.CalculatePayout();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }
    }
}
