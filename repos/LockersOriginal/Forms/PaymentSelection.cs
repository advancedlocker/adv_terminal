﻿using System;
using System.Globalization;
using System.Windows.Forms;
using System.Drawing;

namespace Lockers
{
    public partial class PaymentSelection : Form
    {

        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        public PaymentSelection()
        {
            InitializeComponent();

            String ImageFilename = "";
            if (Globals.CurrentLanguage == null)
                ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\PaymentMethod_1189x789.tiff";
            else
                ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\PaymentMethod_1189x789.tiff";
            this.BackgroundImage = Image.FromFile(ImageFilename);

            lbldue.Text = Globals.DueAmnt.ToString("#,##0.00");

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();
        }

        private void PaymentSelection_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void bCreditPay_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Globals.PayTypes = PaymentTypes.CreditCard;
            this.Close();
        }

        private void bCashPay_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Globals.PayTypes = PaymentTypes.Cash;
            this.Close();
        }

        private void bBack_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
