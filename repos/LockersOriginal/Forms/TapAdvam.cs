﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lockers.Forms
{
    public partial class TapAdvam : Form
    {
        public TapAdvam()
        {
            InitializeComponent();
        }

        private void pay()
        {
            Process process = new Process();
            process.StartInfo.FileName = Properties.Settings.Default.ProjectRoot + "Lockers\\bin\\Debug\\AdvamSerialDemo.exe";
            process.StartInfo.Arguments = " " + Properties.Settings.Default.AdvamReaderPort + " " + Globals.DueAmnt.ToString();
//            process.StartInfo.Arguments = " COM8 " + Globals.DueAmnt.ToString();
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;

            process.ErrorDataReceived += new DataReceivedEventHandler(OutputHandler);

            process.Start();

            string output = "";
            string err = "";

            try
            {
                output = process.StandardOutput.ReadToEnd();
                //                Console.WriteLine(output);
                err = process.StandardError.ReadToEnd();
                //                Console.WriteLine(err);
                process.WaitForExit();
            }
            catch (Exception Ex)
            {
                // Log error.
                Console.WriteLine(Ex.ToString());
            }


            if (output.Contains("(00) APPROVED"))
            {
                this.DialogResult = DialogResult.OK;
                Console.WriteLine("(00) APPROVED");
            }
            else // if (output.Contains("(05) DECLINED"))
            {
                this.DialogResult = DialogResult.Cancel;
                Console.WriteLine("(05) DECLINED");
            }

            timer1.Stop();
            this.Close();
        }

        static void OutputHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            //* Do your stuff with the output (write to console/log/StringBuilder)
            Console.WriteLine(outLine.Data);
        }
    
        private void TapAdvam_Load(object sender, EventArgs e)
        {
            timer1.Interval = 1000;
            timer1.Start();
         }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            pay();
        }
    }
}
