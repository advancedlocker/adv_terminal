﻿using System;
using System.Globalization;
using System.Windows.Forms;
namespace Lockers
{
    public partial class RentalEnded : Form
    {
        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        public RentalEnded()
        {
            InitializeComponent();

            lblLocknum.Text = Globals.LockData.LockNumber.ToString();
            lblremaintime.Text = (Globals.LockData.Remaining < TimeSpan.Zero ? "-" : "") + String.Format("{0:hh\\:mm}", Globals.LockData.Remaining); //String.Format("{0:00}:{1:00} ", Globals.LockData.Remaining.Hours, Globals.LockData.Remaining.Minutes);

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void RentalEnded_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

        }

        private void bBack_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            timer1.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }
    }
}
