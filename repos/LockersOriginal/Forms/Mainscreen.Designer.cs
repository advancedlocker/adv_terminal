﻿namespace Lockers
{
    partial class Mainscreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Mainscreen));
            this.bRent = new System.Windows.Forms.Button();
            this.bOpen = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.btnPaysetup = new System.Windows.Forms.Button();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.lbltime = new System.Windows.Forms.Label();
            this.labTimeHead = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bRent
            // 
            this.bRent.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bRent, "bRent");
            this.bRent.FlatAppearance.BorderSize = 0;
            this.bRent.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bRent.Name = "bRent";
            this.bRent.UseVisualStyleBackColor = false;
            this.bRent.Click += new System.EventHandler(this.bRent_Click);
            // 
            // bOpen
            // 
            this.bOpen.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.bOpen, "bOpen");
            this.bOpen.FlatAppearance.BorderSize = 0;
            this.bOpen.Name = "bOpen";
            this.bOpen.UseVisualStyleBackColor = false;
            this.bOpen.Click += new System.EventHandler(this.bOpen_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btnPaysetup
            // 
            this.btnPaysetup.BackColor = System.Drawing.Color.Transparent;
            this.btnPaysetup.FlatAppearance.BorderSize = 0;
            this.btnPaysetup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnPaysetup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.btnPaysetup, "btnPaysetup");
            this.btnPaysetup.Name = "btnPaysetup";
            this.btnPaysetup.UseVisualStyleBackColor = false;
            this.btnPaysetup.Click += new System.EventHandler(this.btnPaysetup_Click);
            // 
            // timer2
            // 
            this.timer2.Interval = 10000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // lbltime
            // 
            resources.ApplyResources(this.lbltime, "lbltime");
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.Name = "lbltime";
            this.lbltime.Click += new System.EventHandler(this.bClose_Click);
            // 
            // labTimeHead
            // 
            resources.ApplyResources(this.labTimeHead, "labTimeHead");
            this.labTimeHead.BackColor = System.Drawing.Color.Transparent;
            this.labTimeHead.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labTimeHead.ForeColor = System.Drawing.Color.White;
            this.labTimeHead.Name = "labTimeHead";
            // 
            // Mainscreen
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(195)))), ((int)(((byte)(65)))));
            this.Controls.Add(this.labTimeHead);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.btnPaysetup);
            this.Controls.Add(this.bOpen);
            this.Controls.Add(this.bRent);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Mainscreen";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mainscreen_FormClosing);
            this.Load += new System.EventHandler(this.Mainscreen_Load);
            this.Shown += new System.EventHandler(this.Mainscreen_Shown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Mainscreen_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bRent;
        private System.Windows.Forms.Button bOpen;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnPaysetup;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label labTimeHead;
    }
}