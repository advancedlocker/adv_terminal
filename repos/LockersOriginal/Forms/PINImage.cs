﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;


namespace Lockers
{
    public partial class PINImage : Form
    {
        string prevbutton;
        string used_icons = "";
        PictureBox previmg = new PictureBox();
        PictureBox imagepin = new PictureBox();
        int[] ImageMap = new int[5];

        DateTimeFormatInfo fi = new DateTimeFormatInfo();
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        private void RandomiseImages()
        {
            // Randomise the numbers
            Random randomGen = new Random();

            for (int i = 0; i < 5; i++)
                ImageMap[i] = 0;

            if (used_icons != null)
            {
                string[] icons = used_icons.Split(',');

                for (int count = 0; count < (icons.Count() - 1); count++)
                {
                    int tmp = randomGen.Next(1, 5);      // Random 32bit number
                    ImageMap[tmp] = Convert.ToInt32(icons[count]);
                }
            }
            
            for (int count = 0; count < 5; count++)
            {
                int tmp = randomGen.Next(1, 25);      // Random 32bit number
                for (int i = 0; i < 5; i++)
                {
                    if (ImageMap[i] == 0)
                    {
                        ImageMap[i] = tmp;
                        break;
                    }
                    else
                    {
                        if (ImageMap[i] == tmp)
                        {
                            count--;
                            break;
                        }
                    }
                }
            }

            string fname = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + "Common\\Icons\\White\\IconTrans" + ImageMap[0] + ".tiff";
            bImage1.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + "Common\\Icons\\White\\IconTrans" + ImageMap[0] + ".tiff");
            bImage2.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + "Common\\Icons\\White\\IconTrans" + ImageMap[1] + ".tiff");
            bImage3.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + "Common\\Icons\\White\\IconTrans" + ImageMap[2] + ".tiff");
            bImage4.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + "Common\\Icons\\White\\IconTrans" + ImageMap[3] + ".tiff");
            bImage5.BackgroundImage = Image.FromFile(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + "Common\\Icons\\White\\IconTrans" + ImageMap[4] + ".tiff");

            this.Update();
        }

        public PINImage(string used_images)
        {
            InitializeComponent();

//            this.BackgroundImage = ConfigurationData.GetBackground("PINimage" + Globals.LanguageCode.ToString());
            String ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\ChooseSecurityCode.tiff";
            this.BackgroundImage = Image.FromFile(ImageFilename);

            //            imagepin.BackgroundImage = Image.FromFile(@"Content/Images/label/circle_Highlight.png");
            //            imagepin.Location = new Point(0, 0);//newPoint;

            used_icons = used_images;

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            timer1.Start();
        }

        private void PINImage_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            RandomiseImages();
//            SetupPINButtons();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void bBack_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            this.DialogResult = DialogResult.Cancel;
            timer1.Stop();
            this.Close();
        }

        private void bImage_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            switch (button.Name)
            {
                case "bImage1":
                    Globals.LockData.PINIcon = ImageMap[0].ToString();
                    pictureBox1.Visible = true;
                    pictureBox2.Visible = false;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = false;
                    pictureBox5.Visible = false;
                    break;
                case "bImage2":
                    Globals.LockData.PINIcon = ImageMap[1].ToString();
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = true;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = false;
                    pictureBox5.Visible = false;
                    break;
                case "bImage3":
                    Globals.LockData.PINIcon = ImageMap[2].ToString();
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = false;
                    pictureBox3.Visible = true;
                    pictureBox4.Visible = false;
                    pictureBox5.Visible = false;
                    break;
                case "bImage4":
                    Globals.LockData.PINIcon = ImageMap[3].ToString();
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = false;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = true;
                    pictureBox5.Visible = false;
                    break;
                case "bImage5":
                    Globals.LockData.PINIcon = ImageMap[4].ToString();
                    pictureBox1.Visible = false;
                    pictureBox2.Visible = false;
                    pictureBox3.Visible = false;
                    pictureBox4.Visible = false;
                    pictureBox5.Visible = true;
                    break;
            }


            /* SMC            
                        if (prevbutton != "")
                        {
                            foreach (Button  ctrl in Controls.OfType<Button>())
                            {
                                if (ctrl.Name == prevbutton )
                                {
                                    ctrl.Controls.Remove(imagepin);
                                    //ctrl.BackgroundImage = previmg.BackgroundImage ;
                                    //ctrl.Dispose();
                                }
                            }

                        }

                        previmg.BackgroundImage = button.BackgroundImage;

                imagepin.Size = button.Size;
                imagepin.BackgroundImageLayout = ImageLayout.Stretch;
                button.Controls.Add(imagepin);
                prevbutton = button.Name  ;
            */

            //            Globals.LockData.PINIcon = button.Name;
            //Graphics g = this.button4.CreateGraphics();
            //Pen pen = new Pen(Color.Red);
            //g.DrawEllipse(pen, 0, 0, 152, 135);
        }



        private void SetupPINButtonsA()
        {
            List<Animal> Animals = ConfigurationData.PINAnimalImages();
            //Random r = new Random();
            //var a = Animals.OrderBy(asd => r.Next()).ToList();

            foreach (Animal animal in Animals)
            {
                 Button button = new Button
                {
                    Name = animal.Name,

                    BackgroundImage = animal.ImgPIN,

                    Location = animal.Position,

                };
                button.BackgroundImageLayout = ImageLayout.Center;
                button.Size =new Size(animal.ImgWidth,animal.ImgHeight);
                button.BackColor = Color.Transparent;
                button.FlatAppearance.BorderSize = 0;
                //button.FlatAppearance.BorderColor = Color.Magenta;
                button.FlatAppearance.MouseDownBackColor = Color.Transparent;
                button.FlatAppearance.MouseOverBackColor = Color.Transparent;
                button.FlatStyle = FlatStyle.Flat;
                button.TextImageRelation = TextImageRelation.ImageAboveText;
                button.UseVisualStyleBackColor = false;
                //button.Visible = frozenMixDispenser.ButtonEnable;
                // Register the Click event handler.
                button.Click += this.bImage_Click;

                // Add the new button to the form.
                this.Controls.Add(button);
            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
        }

        private void bHelp_Click(object sender, EventArgs e)
        {

        }
    }
}
