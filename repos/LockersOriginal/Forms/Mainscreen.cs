﻿
// </summary>
// version locker01 - vic
// -----------------------------------------------------------------------
namespace Lockers
{
    #region namespace
    using System;
    using System.Data;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Windows.Forms;
    using System.Xml;
    #endregion
    public partial class Mainscreen :  Form
    {
        private String Country;
        private String[] Languages;
        private String[] Flags;
//        Button[] LanguageButtons;

        DialogResult diagRes;
        private int runStep;
        private int mode=0;
        private int PINStep=0;

        DateTimeFormatInfo fi = new DateTimeFormatInfo();

        private LockerSelection lockerselection;
        private SelectSize selectsize;
        private SelectLogin selectlogin;
        private TimeSelection timeselection;
        private TimeDaySelection timedayselection;
        private PINReg pinreg;
        private PINLogin pinlogin;
        private PINImage pinimage;
        private PaymentSelection paymentscreen;
        private Unlocked unlocked;
        private RentalEnded rentend;
        private CashPayment cashpayment;
        private Paydone paydonescreen;
        private DoneForm donescreen;
        private OpenLocker openlocker;
        private LockerDatabase oLockerData;
//        private BlackScreen blanckscreen;
        private LockerSize lsize;

        //private Button prevBtn;


        public SmartPay smartpay;
        public Boolean deviceState = false;

        private Locker_TCPClient client;
        private int mCRC16 = Properties.Settings.Default.CRC16;
        private int sKey; 

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }


        //  SMC : Removed smart pay declaration
        //        public Mainscreen(SmartPay smartpay)
       
        public Mainscreen()
        {
            InitializeComponent();

            timer2.Interval = Properties.Settings.Default.idletime;

            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";

            // SMC removed
            //            this.smartpay = smartpay;
            this.oLockerData = new LockerDatabase();


            // SMC removed
            //            this.blanckscreen = new BlackScreen();

            //this.prevBtn = b1;
            //            Globals.prevBtn = Country1;

            //this.smartpay.VALIDATOR.PaidAmountEvent += this.Smartpay_PaidAmount;
            //this.smartpay.VALIDATOR.NoteStored += this.Validator_NoteStored;
            //this.smartpay.VALIDATOR.OutOfService += this.Validator_OutofService;
            //this.smartpay.VALIDATOR.CashBoxFull += this.Stacker_Full;
            //this.smartpay.VALIDATOR.FraudAttemp += this.Fraud_Attempt;
            //this.smartpay.VALIDATOR.UnSafe_Jam += this.UnSafe_Jam;
            //this.smartpay.VALIDATOR.Safe_Jam += this.Safe_Jam;
            //this.smartpay.VALIDATOR.RejectedNote += this.RejectedNotes;

            //this.smartpay.HOPPER.PaidAmountEvent += this.Smartpay_CoinPaidAmount;

            //this.smartpay.VALIDATOR.DisableValidator();
            //CHelpers.Pause(300);
            //this.smartpay.HOPPER.DisableValidator();
            //CHelpers.Pause(200);

            //ManualResetEvent syncEvent = new ManualResetEvent(false);
            //Thread t1 = new Thread(
            //    () =>
            //    {
            //        // Do some work...

            //        this.smartpay.VALIDATOR.DisableValidator();
            //        syncEvent.Set();
            //    }
            //);
            //t1.Start();

            //Thread t2 = new Thread(
            //    () =>
            //    {
            //        syncEvent.WaitOne();
            //        this.smartpay.HOPPER.DisableValidator();
            //                // Do some work...
            //            }
            //);
            //t2.Start();
        }



        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            CHelpers.Shutdown = true;
            if (this.smartpay != null)
            {
                this.smartpay.StopPaysystem();
                if (!this.smartpay.VALIDATOR.isDISABLED)
                {
                    this.smartpay.VALIDATOR.DisableValidator();
                    CHelpers.Pause(200);
                }

                if (!this.smartpay.HOPPER.Is_Disable)
                {
                    this.smartpay.HOPPER.DisableValidator();
                    CHelpers.Pause(200);
                }

                this.smartpay.Dispose();
            }
            //if (deviceState == true)
            //{
            //    Dll_Camera.ReleaseDevice();
            //}
            //else
            //{
            //    Dll_Camera.ReleaseLostDevice();
            //}

            if (e.CloseReason == CloseReason.WindowsShutDown) return;



        }


        private void Mainscreen_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            ResetBackground();
            timer1.Start();

            //start locker controller
            Start_LockControl(); 
//            this.Run();
        }

        private void Mainscreen_Shown(object sender, EventArgs e)
        {
            welcome welcomescreen = new welcome(this.smartpay);
            welcomescreen.BringToFront();
            DialogResult start = welcomescreen.ShowDialog();
            if (start == DialogResult.OK)
            {
                ResetBackground();
                timer2.Start();

            }
        }

        private void Start_LockControl()
        {
            Globals.LogFile("Start_LockControl() - Starting the lock controller\n");

            if (Globals.client == null)
                Globals.client = new Locker_TCPClient(IPAddress.Parse(Properties.Settings.Default.ControllerIP), Properties.Settings.Default.ControllerPort, false);
            client = Globals.client;

            if (client != null)
            {
                Globals.LogFile("Start_LockControl() - lock client = null\n");
                client.DataReceived -= new Locker_TCPClient.delDataReceived(Globals.Client_DataReceived);
                //                Globals.client.DataReceived -= new Locker_TCPClient.delDataReceived();
                client.ConnectionStatusChanged -= new Locker_TCPClient.delConnectionStatusChanged(Globals.Client_ConnectionStatusChanged);
                client.Dispose();
            }

            Globals.client = new Locker_TCPClient(IPAddress.Parse(Properties.Settings.Default.ControllerIP), Properties.Settings.Default.ControllerPort, false);
            client = Globals.client;

            //Initialize the events
            client.DataReceived += new Locker_TCPClient.delDataReceived(Globals.Client_DataReceived);
            client.ConnectionStatusChanged += new Locker_TCPClient.delConnectionStatusChanged(Globals.Client_ConnectionStatusChanged);

            Globals.LogFile("Start_LockControl() - client connect()\n");
            this.client.Connect();
        }


        void Open_Locker(int lockerNumber)
        {
            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

// USE SQL to update the address
// TODO : Fix this serial number conbversion to byte array
            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };

            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }
            
            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetRear.CopyTo(sum, packetFront.Length + packetAddress.Length);

            client.Send(sum);
        }

        void Close_Locker(int lockerNumber)
        {
            // TODO : Make this generic.
            byte[] packetFront = { 0x3C, 0x83, 0x78, 0x56, 0x34, 0x12 };
            byte[] packetAddress = { 0xFF, 0xFD, 0xDA, 0xD7 };
            byte[] packetRear = { 0x01, 0x01, 0xE9, 0x3E };

            LockerDatabase LockerData = new LockerDatabase();
            String Serial = LockerData.GetLockerSerial(lockerNumber);
            char[] SerArr = Serial.ToCharArray();

            byte tmp = 0;
            for (int count = 0; count < 4; count++)
            {
                tmp = (byte)SerArr[2 * count];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] = tmp;
                packetAddress[count] <<= 4;

                tmp = (byte)SerArr[(2 * count) + 1];
                if (tmp > 0x39)
                    tmp -= 0x37;
                else
                    tmp -= 0x30;
                packetAddress[count] += tmp;
            }

            int length = packetFront.Length + packetAddress.Length + packetRear.Length;
            byte[] sum = new byte[length];
            packetFront.CopyTo(sum, 0);
            packetAddress.CopyTo(sum, packetFront.Length);
            packetAddress.CopyTo(sum, packetAddress.Length);

            client.Send(sum);
        }


#if (false)
        void client_ConnectionStatusChanged(Locker_TCPClient sender, Locker_TCPClient.ConnectionStatus status)
        {
            //Check if this event was fired on a different thread, if it is then we must invoke it on the UI thread
            if (InvokeRequired)
            {
                Invoke(new Locker_TCPClient.delConnectionStatusChanged(client_ConnectionStatusChanged), sender, status);
                return;
            }
            //richTextBox1.Text += "Connection: " + status.ToString() + Environment.NewLine;
            Globals.LogFile("client_ConnectionStatusChanged() - connect status = " + status.ToString() + "\n");
            if (status.ToString() == Locker_TCPClient.ConnectionStatus.Connected.ToString())
            {
                client.SourceKey = Properties.Settings.Default.sourceKey;
                Globals.client.GetSession();
            }

        }
        //Fired when new data is received in the TCP client
        void client_DataReceived(Locker_TCPClient sender, object data)
        {
            //Again, check if this needs to be invoked in the UI thread
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new Locker_TCPClient.delDataReceived(client_DataReceived), sender, data);
                }
                catch
                { }
                return;
            }
            //Interpret the received data object as a string
            string strData = data as string;
            Globals.LogFile("client_DataReceived() - data = " + strData + "\n");
            string values = strData.Split('-').Select(sValue => sValue.Trim()).First();
            /*
                        switch(Globals.client.LockerCommand)
                        {
                            case Locker_cmd.Basic.GetSession:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.AssignLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.UnassignLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.AvailableLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.UsedLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.OpenLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.CloseLock:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            case Locker_cmd.Basic.LockStatus:
                            {
                                int.TryParse(strData, out sKey);
                                    Globals.client.SessionKey = sKey;
                            }
                            break;

                            default:
                                break;
                        }
            */
        }
#endif 

        private void Run()
        {
            //            this.runStep = 0;

            if(this.runStep == 0)
                this.runStep = 5;
//            this.NextRunStep(this, EventArgs.Empty);   // trigger first build step

            SimpleStateMachine(this, EventArgs.Empty);   // trigger first build step
        }

        public void SimpleStateMachine(object sender, EventArgs e)
        {
/*
            //            timer2.Start();
            if (Globals.AbortForm == true)
            {
                //                this.blanckscreen.Hide();
                this.runStep = 7;
                this.Show();
                ResetBackground();


            }
            else
            {

            }
*/

            this.Hide();

            if (Globals.Process == ProcessType.rent)
            {
//                this.client.GetAvailableLocks(int sessionkey, int CRC16);

                Globals.LockData = new LockerDatabase();
                Globals.LockData.LockNumber = 1;
                Globals.CurrentLanguage = "English";


                // TOD : Check if only one locker. If so then dont display the selection screen
                int LockerTypes = GetNumLockerTypes();

                // Select the locker size
                switch(LockerTypes)
                {
                    case 0:
                        // Error page for no lockers left to hire.
                        ImagePage lockersFull = new ImagePage(Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\LockerFull1182x789.jpg");
                        if (lockersFull.ShowDialog() == DialogResult.Cancel)
                        {
                            this.Show();
                            Globals.AbortForm = false;
                            timer2.Start();
                            return;
                        }
                        break;

                    case 1:
                        if (Globals.LockData.Lockers == null)
                            Globals.LockData.Lockers = new DataTable();

                        Globals.LockData.Lockers = Globals.LockData.Get_Lockers();

                        // TODO : Make this more generic so that any size can be single
                        Globals.LockData.Locker_Size = LockerSize.MEDIUM;
                        break;

                    default:
                        {
                            SelectSize ssel = new SelectSize();

                            if (ssel.ShowDialog() == DialogResult.Cancel)
                            {
                                this.Show();
                                Globals.AbortForm = false;
                                //                    ssel.Close();
                                timer2.Start();
                                return;
                            }
                        }
                        break;
                }


                // Select the time for the locker
                // TODO : Check if rental type is daily 
                if (Properties.Settings.Default.HireTypePeriod == "Hour")
                {
                    // Enable hourly hire screen
                    TimeSelection TimSet = new TimeSelection();
                    if (TimSet.ShowDialog() == DialogResult.Cancel)
                    {
                        this.Show();
                        Globals.AbortForm = false;
                        timer2.Start();
                        return;
                    }
                }
                else
                {
                    // Enable daily hire screen
                    TimeDaySelection TimSet = new TimeDaySelection();
                    if (TimSet.ShowDialog() == DialogResult.Cancel)
                    {
                        this.Show();
                        Globals.AbortForm = false;
                        timer2.Start();
                        return;
                    }
                }

                // Select the pin code
                Globals.LoginSelected = LoginTypes.PIN;
                PINReg PINReg = new PINReg();
                if (PINReg.ShowDialog() == DialogResult.Cancel)
                {
                    this.Show();
                    Globals.AbortForm = false;
                    timer2.Start();
                    return;
                }

                // Select the security code
                PINImage PINImage = new PINImage(null);
                if (PINImage.ShowDialog() == DialogResult.Cancel)
                {
                    this.Show();
                    Globals.AbortForm = false;
                    timer2.Start();
                    return;
                }

                // Select the payment type
                PaymentSelection Psel = new PaymentSelection();
                if (Psel.ShowDialog() == DialogResult.Cancel)
                {
                    this.Show();
                    Globals.AbortForm = false;
                    timer2.Start();
                    return;
                }

                // Make the payment
                switch (Globals.PayTypes)
                {
                    case PaymentTypes.Cash:
                        CashPayment cashpay = new CashPayment(this.smartpay);
                        if(Globals.AbortForm == true)
                        {
                            this.Show();
                            Globals.AbortForm = false;
                            timer2.Start();
                            return;
                        }

                        break;
                    case PaymentTypes.CreditCard:
                        CreditPayment Cred = new CreditPayment();
                        if (Cred.ShowDialog() == DialogResult.Cancel)
                        {
                            this.Show();
                            Globals.AbortForm = false;
                            timer2.Start();
                            return;
                        }
                        break;
                }

                // Complete the payment
                Paydone paydone = new Paydone();
                if (paydone.ShowDialog() == DialogResult.Cancel)
                {
                    ResetBackground();
                    this.Show();
                    timer2.Start();
                    return;
                }

                Open_Locker(Globals.LockData.LockNumber);

                Save_Rent();

                DoneForm donescreen = new DoneForm();
                DialogResult donescreen_result = donescreen.ShowDialog();

            }
            else
            {
                OpenLocker openLocker = new OpenLocker();
                DialogResult openLocker_result = openLocker.ShowDialog();
                //                this.OpenLockers();

                // Select the pin code
                Globals.LoginSelected = LoginTypes.PIN;
                PINReg PINReg = new PINReg();
                if (PINReg.ShowDialog() == DialogResult.Cancel)
                {
                    this.Show();
                    Globals.AbortForm = false;
                    timer2.Start();
                    return;
                }
                /*
                                LockerDatabase LockDB = new LockerDatabase();
                                string Temp = LockDB.GetIconsForPinCode(Globals.LockData.PINCode);
                                string[] LockerNums = Temp.Split(',');

                                // Select the security code
                                PINImage PINImage = new PINImage(Temp);
                                if (PINImage.ShowDialog() == DialogResult.Cancel)
                                {
                                    this.Show();
                                    Globals.AbortForm = false;
                                    timer2.Start();
                                    return;
                                }
                                if (LockDB.Check_Access(Globals.LockData.PINCode, Globals.LockData.PINIcon) == true)
                                {
                                    int LockerNumber = LockDB.LockNumber;
                                    Globals.client.OpenLock(LockerNumber);
                                }

                */
                LockerDatabase LockDB = new LockerDatabase();
                int LockerNumber = 0;
                if (LockDB.Check_Access(Globals.LockData.PINCode, Globals.LockData.PINIcon) == true)
                {
//                    LockerNumber = LockDB.LockNumber;
                    Globals.LockData.LockNumber = LockDB.LockNumber;
                    //                    Globals.client.OpenLock(LockerNumber);
                    Open_Locker(Globals.LockData.LockNumber);
                }

                if (Globals.Process == ProcessType.open)
                {
                    Unlocked UnlockScreen = new Unlocked();
                    if (UnlockScreen.ShowDialog() == DialogResult.Cancel)
                    {
                        this.Show();
                        Globals.AbortForm = false;
                        timer2.Start();
                        return;
                    }
                    else
                        this.UPdate_Rent();
                }
                else
                {
                    RentalEnded RentalEnded = new RentalEnded();
                    if (RentalEnded.ShowDialog() == DialogResult.Cancel)
                    {
                        this.Show();
                        Globals.AbortForm = false;
                        timer2.Start();
                        return;
                    }
                    else
                       this.Done_rent();
                }

                DoneForm donescreen = new DoneForm();
                DialogResult donescreen_result = donescreen.ShowDialog();
            }

            this.timer2_Tick(this, EventArgs.Empty);
        }

        private int GetNumLockerTypes()
        {
            if (Globals.LockData.LockerTypes == null)
                Globals.LockData.LockerTypes = new DataTable();

            Globals.LockData.LockerTypes = Globals.LockData.Get_Locker_Types();

            var Sizes = (from Rows in Globals.LockData.LockerTypes.AsEnumerable()
                         select Rows["enabled"]).Distinct().ToList();

            int lockercount = Sizes.Count();

            return (lockercount);
        }

        private Decimal GetLockerPrice(int LockerType)
        {
            if (Globals.LockData.LockerTypes == null)
                Globals.LockData.LockerTypes = new DataTable();

            Globals.LockData.LockerTypes = Globals.LockData.Get_Locker_Types();

            var Type = (from Rows in Globals.LockData.LockerTypes.AsEnumerable()
                           select Rows["sizecode"]).Distinct().ToList();

            var Price = (from Rows in Globals.LockData.LockerTypes.AsEnumerable()
                           select Rows["uprice"]).Distinct().ToList();

            Decimal cost = 0;
/*
            decimal cost = 0.00;
            for(int count = 0; count < Type.Count; count++)
            {
                if(LockerType == Convert.ToInt16(Type[count]))
                {
                    cost = Convert.ToDecimal(Price[count]);
                }
            }
*/
            return (cost);
        }

        public void NextRunStep(object sender, EventArgs e)
        {
//            timer2.Start();
            if (Globals.AbortForm == true)
            {
//                this.blanckscreen.Hide();
                this.runStep = 7;
                this.Show();
                ResetBackground();


            }
            else
            {
               
            }
            switch (this.runStep)
            {
                case 0:
                    
//                    this.blanckscreen.Show();
                    this.Hide();

                    if (Globals.Process == ProcessType.rent)
                    {
//                        this.SizeSelection();
                        SelectSize ssel = new SelectSize();
                        DialogResult ssel_result = ssel.ShowDialog();
//                        if (ssel_result == DialogResult.OK)
                        this.runStep++;
                    }
                    else
                    {
                        this.OpenLockers();
                        this.runStep++;

                    }
                    break;
                case 1:
                    //                    this.TimeSettings();
                    TimeSelection TimSet = new TimeSelection();
                    DialogResult TimSet_result = TimSet.ShowDialog();
                    this.runStep++;
                    break;
                //case 2:
                //    this.LoginSelection();
                //    break;
                case 2:
                    //goes here if rent, open and end lock security register and login
                    Globals.LoginSelected = LoginTypes.PIN;
                    this.LoginType();
                    break;
                case 3:
                    // SMC : removed payment selection for the moment. Cash only at this time. 
                    //                        this.PaymentSelect();
                    this.runStep++;
                    NextRunStep(this, EventArgs.Empty);
                    break;
                case 4:
                    //make payment
                    //if (this.mode == 0)
                    //{

// SMC  : Moved this into the payment screen for th emoment.
//                    this.smartpay.RunPaysystem();
                    this.Payment();
                    //}
                    //else
                    //{


                    //}
                    break;

                case 5:
                    if (this.mode == 0)
                    {
                        this.PayDone();
                    }
                    else
                    {
                        if (Globals.Process == ProcessType.open)
                        {
                            this.UnLockScreen();
                        }
                        else
                        {
                            this.RentEndScreen();
                        }
                        //this.runStep = 5;
                    }
                    break;
                case 6:
                    this.DoneScreens();
                    break;
                case 7:
                    this.Show();
                    this.Refresh();
                    this.Invalidate();
                    Globals.AbortForm = false;
                    this.Update();

// SMC : Removed smart payout for the moment. 
//                    this.smartpay.Payout = 0;
//                    this.smartpay.VALIDATOR.PAID_AMOUNT = 0;
//                    this.smartpay.StopPaysystem();
                    
                    timer2.Start();
                    //this.Show();
                    //ManualResetEvent syncEvent = new ManualResetEvent(false);
                    //Thread t1 = new Thread(
                    //    () =>
                    //    {
                    //        // Do some work...
                    //        this.smartpay.VALIDATOR.DisableValidator();
                    //        syncEvent.Set();
                    //    }
                    //);
                    //t1.Start();

                    //Thread t2 = new Thread(
                    //    () =>
                    //    {
                    //        syncEvent.WaitOne();
                    //        this.smartpay.HOPPER.DisableValidator();
                    //        // Do some work...
                    //    }
                    //);
                    //t2.Start();
                    break;

                default:
                    this.runStep = 6;
                    break;


            }
            this.runStep++;
//            timer2.Stop();
        }

        private void ResetBackground()
        {
            //            this.BackgroundImage = ConfigurationData.GetBackground("Language" + Globals.LanguageCode.ToString());
            String ImageFilename = "";
            if (Globals.CurrentLanguage == null)
                ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Properties.Settings.Default.DefaultLanguage + "\\RentalStart_1189x789.tiff";
            else
                ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\RentalStart_1189x789.tiff";
            this.BackgroundImage = Image.FromFile(ImageFilename);

/*
            switch (this.runStep)
            {
                case 0:
                    
                    break;

                default:
                    break;

            }
*/
            //this.idleSlides = ConfigurationData.GetIdleSlideImages();
            //if (Globals.ChooseALanguage)
            //{
            //    this.BackgroundImage = (from Image image in this.idleSlides
            //                            select image).Last();
            //}
            //else
            //{
            //    this.BackgroundImage = (from Image image in this.idleSlides
            //                            select image).First();
            //}
            //// Set the background image.


            //this.LanguageSelectionButtons();
            ////this.ShowFlavorsOfTheDay();

            //if (Properties.Settings.Default.PayGoInstalled)
            //{
            //    // start image rotation
            //    this.rotateImageTimer.Interval = 4000;   // 4 seconds vs standard 6 seconds
            //    this.rotateImageTimer.Enabled = true;
            //}


            //if (Properties.Settings.Default.EnablePromo)
            //{
            //    ButtonPromo.Visible = true;
            //    this.LOADPromocode();
            //}
            //else
            //{
            //    ButtonPromo.Visible = false;
            //}
            //ButtonPromo.BackgroundImage = Image.FromFile(@"Content/" + Properties.Settings.Default.Skin + @"/Images/Buttons/Promocode/" + ConfigurationData.GetLanguageName(Globals.LangCode.ToString()) + ".png");

            //if (this.backpanel != null)
            //{
            //    backpanel.SetupFrozenMixSelectionButtons();
            //    this.BackSlides = ConfigurationData.BackPanelSlideImages();
            //    this.backpanel.BackgroundImage = (from Image image in this.BackSlides
            //                                      select image).First();
            //}

            //this.BackgroundImage = ConfigurationData.GetBackground("Cupselection");
        }

        private void DoneScreens()
        {
            this.donescreen = new DoneForm();
            this.donescreen.FormClosed += this.NextRunStep;
//            this.donescreen.Load += this.Donescreen_Load;
            this.donescreen.Show(this);
        }

        private void Donescreen_Load(object sender, EventArgs e)
        {
            //            client.OpenLock(sKey, Globals.LockData.LockNumber, mCRC16);
            Globals.client.OpenLock(Globals.LockData.LockNumber);

            switch (Globals.Process)
            {
                case ProcessType.rent:
                    this.Save_Rent();
                    break;
                case ProcessType.open:
                    this.UPdate_Rent();
                    break;
                case ProcessType.endrent:
                    this.Done_rent();
                    break;

            }
            
        }

        private void OpenLockers()
        {
            this.openlocker = new OpenLocker();
            this.openlocker.FormClosed += this.NextRunStep;
            this.openlocker.Show(); 
        }

        private void PayDoneOriginal()
        {
            this.paydonescreen = new Paydone();
            this.paydonescreen.FormClosed += this.NextRunStep;
            this.paydonescreen.Show();
        }

        private void PayDone()
        {
            paydonescreen = new Paydone();
            paydonescreen.FormClosed += this.NextRunStep;
            paydonescreen.Show();

//            int count = 0;
//            this.NextRunStep(this, EventArgs.Empty);   // trigger first build step
        }


        public void Payment()
        {
            switch (Globals.PayTypes)
            {
                case PaymentTypes.Cash:
                    this.cashpayment = new CashPayment(this.smartpay);
                    this.cashpayment.FormClosed += this.NextRunStep;
                    //                    this.cashpayment.Show();
                    this.runStep++;
//                    this.runStep++;
                    NextRunStep(this, null);
                    break;
                case PaymentTypes.CreditCard:
                    break;
            }

        }

        private void RentEndScreen()
        {
            this.rentend = new RentalEnded();
            this.rentend.FormClosed += this.NextRunStep;
            this.rentend.Show(this);

        }

        private void UnLockScreen()
        {
            this.unlocked = new Unlocked();
            this.unlocked.FormClosed += this.NextRunStep;
            this.unlocked.Show(this);
        }

        private void PaymentSelect()
        {
            
            this.paymentscreen = new PaymentSelection();
            this.paymentscreen.FormClosed += this.NextRunStep;
            this.paymentscreen.Show(this);

        }

        public void NextPINReg(object sender,EventArgs e)
        {
            if (this.PINStep <= 1 && !Globals.AbortForm)
            {
                if (this.mode == 0 && this.PINStep == 0)
                {
                    this.pinreg = new PINReg();
                    this.pinreg.FormClosed += this.NextPINReg;
                    this.pinreg.Show();

                }
                else if (this.mode == 1 && this.PINStep == 0)
                {
                    this.pinlogin = new PINLogin();
                    this.pinlogin.FormClosed += this.NextPINReg;
                    this.pinlogin.Show();
                }
                else
                {
                    this.pinimage = new PINImage(null);
                    this.pinimage.FormClosed += this.NextPINReg;
                    this.pinimage.Show();
                }
                this.PINStep++;
            }
            else
            {
                if (!Globals.AbortForm)
                {
                    switch (this.mode)
                    {
                        case 0:
// SMC : Removed for the moment as two lockers can have the same access code. 
// Think about the validation when codes are the same. 
                            if (Globals.LockData.Used_Access(Globals.LockData.PINCode, Globals.LockData.PINIcon))
                            {
                                this.PINStep = 0;
                                this.runStep = 2;
                                MessageBox.Show("Access Code already used", "ACCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            break;
                        case 1:
                            if (!Globals.LockData.Check_Access(Globals.LockData.PINCode, Globals.LockData.PINIcon))
                            {
                                this.PINStep = 0;
                                this.runStep = 2;
                                MessageBox.Show("Invaled Access", "ACCESS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                if (Globals.LockData.Remaining.Minutes  < 0)
                                {
                                    decimal m_price = Globals.LockData.UPrice;
                                    decimal totalhrs = Globals.LockData.Remaining.Hours + ((decimal)Globals.LockData.Remaining.Minutes  / 60);
                                    decimal cost = (totalhrs * -1) * m_price;
                                    Globals.DueAmnt = (double)Math.Round(cost, 2);
                                    Globals.LockData.COST = (decimal)Math.Round(cost, 2);
                                    this.runStep = 3;
                                }
                                else
                                {

                                    this.runStep = 5;
                                }
                                
                            }
                            break;

                    }
                }
                       

                    //else
                    //{
                    ResetBackground();
                    this.NextRunStep(this, EventArgs.Empty);
                //}
            }
        }

        private void LoginType()
        {
            if (Globals.LoginSelected == LoginTypes.PIN)
            {
                this.NextPINReg(this,EventArgs.Empty );
            }
        }
        private void LoginSelection()
        {
            this.selectlogin = new SelectLogin();
            this.selectlogin.FormClosed += this.NextRunStep;
            this.selectlogin.Show(); 
        }


        private void TimeSettings()
        {
            
                this.timeselection = new TimeSelection();
                this.timeselection.FormClosed += this.NextRunStep;
                this.timeselection.Show();
            
        }


        private void LockerSelectionScreen()
        {
            
                this.lockerselection = new LockerSelection();
                this.lockerselection.FormClosed += Lockerselection_Closed;
                this.lockerselection.Show();
           
        }

        private void Lockerselection_Closed(object sender, EventArgs e)
        {
            lockerselection.Dispose();
            lockerselection = null;
            this.NextRunStep(this, EventArgs.Empty);
        }


        private void SizeSelection()
        {
            
                this.selectsize = new SelectSize();
                this.selectsize.FormClosed += this.NextRunStep;
                this.selectsize.Show();
//                this.NextRunStep(this, EventArgs.Empty);
        }

        private void Mainscreen_KeyPress(object sender, KeyPressEventArgs e)
        {
             if (e.KeyChar.ToString().ToUpper().Equals("Q"))
            {
                // Quit - ends kiosk program
                e.Handled = true;
                Application.Exit();
            }
        }


        private void Save_Rent()
        {
            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
            Globals.LockData.Save_Rent();

        }

        private void Done_rent()
        {
            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
            Globals.LockData.Locker_Done();

        }

        private void UPdate_Rent()
        {
            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
            Globals.LockData.Update_Rent();
        }

        private void LockData_LockerDataError(object sender, LogMessageEventArgs e)
        {
            MessageBox.Show(e.Messsage,"Error Saving", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Globals.LockData.LockerDatabaseError -= LockData_LockerDataError;
        }

        private void bRent_Click(object sender, EventArgs e)
        {
            this.mode = 0;
            this.runStep = 0;
            this.PINStep = 0;
            Globals.Process = ProcessType.rent;
            //this.oLockerData = new LockerData();
            Globals.LockData = new LockerDatabase();
            timer2.Stop();

            Globals.LockData.Get_Lockers();
//            this.NextRunStep(this, EventArgs.Empty);
            SimpleStateMachine(this, EventArgs.Empty);

            //if (Globals.LockData.Lock_Available() == true)
            //{
            //    this.NextRunStep(this, EventArgs.Empty);
            //}
            //else
            //{
            //    MessageBox.Show("No Locks available", "Locker", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}


        }

        private void bClose_Click(object sender, EventArgs e)
        {
            timer2.Stop();
            string mCode = "";
            NumericKeypad kpad = new NumericKeypad("CLOSE PROGRAM ACCESS KEY");
            //kpad.Data = txtValue1.Text;
            DialogResult dr = kpad.ShowDialog();
            if (dr == DialogResult.OK)
            {
                mCode = kpad.Data;
            }

            kpad.Dispose();
            kpad = null;

            if (mCode == Properties.Settings.Default.ExitCode.ToString())
            {
//                this.blanckscreen.Close();
                this.Close();
                
            }
            else
            { 
                timer2.Start();
            }
        }

        private void bOpen_Click(object sender, EventArgs e)
        {
            this.mode = 1;
            this.runStep = 0;
            this.PINStep = 0;
            Globals.Process = ProcessType.open;
            Globals.LockData = new LockerDatabase();
            timer2.Stop();

            SimpleStateMachine(this, EventArgs.Empty);
//            this.NextRunStep(this, EventArgs.Empty);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);
            //lblsec.Text = DateTime.Now.ToString("ss");
            //lblsec.Location = new Point(lbltime.Location.X  + lbltime.Width,lblsec.Location.Y);
        }

        private void btnPaysetup_Click(object sender, EventArgs e)
        {
            timer2.Stop();
            //SetupPaysystem setUpPaySystem = new SetupPaysystem(this.smartpay);
            //setUpPaySystem.BringToFront();
            //setUpPaySystem.ShowDialog();
            //setUpPaySystem = null;
            int mcode=0;
            NumericKeypad kpad = new NumericKeypad("MAINTENANCE ACCESS KEY");
            //kpad.Data = txtValue1.Text;
            DialogResult dr = kpad.ShowDialog();
            if (dr == DialogResult.OK)
            {
                if (kpad.Data == "") return;
                mcode = int.Parse(kpad.Data);
            }

            kpad.Dispose();
            kpad = null;

            if (mcode == Properties.Settings.Default.Adminpwd)
            {
                AdminMain admin = new AdminMain(smartpay );
                admin.BringToFront();
                admin.ShowDialog();

                admin.Dispose();


            }
            timer2.Start();
        }

        private void Mainscreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            //try
            //{
            if (this.smartpay != null)
            {
                this.smartpay.StopPaysystem();
                this.smartpay = null;
            }
                
                Application.Exit();
            //}
            //catch { }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            timer2.Stop();
            welcome welcomescreen = new welcome(this.smartpay);
            welcomescreen.BringToFront();
            DialogResult start = welcomescreen.ShowDialog();
            if (start == DialogResult.OK)
            {
                //                this.runStep = 0;
                //                this.NextRunStep(this, EventArgs.Empty);
                this.Show();
                ResetBackground();
                timer1.Start();
                timer2.Start();
            }
        }

        /*
         * // SMC - Unused
            private void btnLanguage_Click(object sender, EventArgs e)
            {
                    timer2.Stop();
                    Button btn = (Button)sender;
                    Globals.LanguageCode = Convert.ToInt32(btn.Tag);
                    //================reset font ========================
                    Globals.prevBtn.Font= new Font
                 (
                    btn.Font,
                    FontStyle.Regular 
                 );
                    //======================================

                    btn.Font = new Font
                 (
                    btn.Font,
                    FontStyle.Underline | FontStyle.Bold
                 );
                    ResetBackground();
                    Globals.prevBtn = btn;
                    timer2.Start();
            }
        */
    }
}
