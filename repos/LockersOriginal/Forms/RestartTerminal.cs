﻿using System.Windows.Forms;

namespace Lockers
{
    public partial class RestartTerminal : Form
    {
        public RestartTerminal()
        {
            InitializeComponent();
        }

        private void RestartTerminal_Load(object sender, System.EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

        }
    }
}
