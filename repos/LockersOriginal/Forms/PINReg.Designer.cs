﻿namespace Lockers
{
    partial class PINReg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PINReg));
            this.bNumber1 = new System.Windows.Forms.Button();
            this.bNumber2 = new System.Windows.Forms.Button();
            this.bNumber3 = new System.Windows.Forms.Button();
            this.bNumber6 = new System.Windows.Forms.Button();
            this.bNumber5 = new System.Windows.Forms.Button();
            this.bNumber4 = new System.Windows.Forms.Button();
            this.bNumber7 = new System.Windows.Forms.Button();
            this.bNumber8 = new System.Windows.Forms.Button();
            this.bNumber9 = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.lbl1 = new System.Windows.Forms.Label();
            this.bClear = new System.Windows.Forms.Button();
            this.lblmsg = new System.Windows.Forms.Label();
            this.bBack = new System.Windows.Forms.Button();
            this.bNumber0 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bNumber1
            // 
            this.bNumber1.BackColor = System.Drawing.Color.White;
            this.bNumber1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bNumber1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNumber1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.bNumber1.Location = new System.Drawing.Point(374, 177);
            this.bNumber1.Name = "bNumber1";
            this.bNumber1.Size = new System.Drawing.Size(74, 75);
            this.bNumber1.TabIndex = 0;
            this.bNumber1.Text = "1";
            this.bNumber1.UseVisualStyleBackColor = false;
            this.bNumber1.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber2
            // 
            this.bNumber2.BackColor = System.Drawing.Color.White;
            this.bNumber2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bNumber2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNumber2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.bNumber2.Location = new System.Drawing.Point(476, 177);
            this.bNumber2.Name = "bNumber2";
            this.bNumber2.Size = new System.Drawing.Size(74, 75);
            this.bNumber2.TabIndex = 0;
            this.bNumber2.Text = "2";
            this.bNumber2.UseVisualStyleBackColor = false;
            this.bNumber2.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber3
            // 
            this.bNumber3.BackColor = System.Drawing.Color.White;
            this.bNumber3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bNumber3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNumber3.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.bNumber3.Location = new System.Drawing.Point(576, 177);
            this.bNumber3.Name = "bNumber3";
            this.bNumber3.Size = new System.Drawing.Size(74, 75);
            this.bNumber3.TabIndex = 0;
            this.bNumber3.Text = "3";
            this.bNumber3.UseVisualStyleBackColor = false;
            this.bNumber3.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber6
            // 
            this.bNumber6.BackColor = System.Drawing.Color.White;
            this.bNumber6.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber6.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bNumber6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNumber6.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.bNumber6.Location = new System.Drawing.Point(576, 277);
            this.bNumber6.Name = "bNumber6";
            this.bNumber6.Size = new System.Drawing.Size(74, 75);
            this.bNumber6.TabIndex = 0;
            this.bNumber6.Text = "6";
            this.bNumber6.UseVisualStyleBackColor = false;
            this.bNumber6.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber5
            // 
            this.bNumber5.BackColor = System.Drawing.Color.White;
            this.bNumber5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber5.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bNumber5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNumber5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.bNumber5.Location = new System.Drawing.Point(476, 277);
            this.bNumber5.Name = "bNumber5";
            this.bNumber5.Size = new System.Drawing.Size(74, 75);
            this.bNumber5.TabIndex = 0;
            this.bNumber5.Text = "5";
            this.bNumber5.UseVisualStyleBackColor = false;
            this.bNumber5.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber4
            // 
            this.bNumber4.BackColor = System.Drawing.Color.White;
            this.bNumber4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bNumber4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNumber4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.bNumber4.Location = new System.Drawing.Point(374, 277);
            this.bNumber4.Name = "bNumber4";
            this.bNumber4.Size = new System.Drawing.Size(74, 75);
            this.bNumber4.TabIndex = 0;
            this.bNumber4.Text = "4";
            this.bNumber4.UseVisualStyleBackColor = false;
            this.bNumber4.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber7
            // 
            this.bNumber7.BackColor = System.Drawing.Color.White;
            this.bNumber7.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber7.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bNumber7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNumber7.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.bNumber7.Location = new System.Drawing.Point(374, 377);
            this.bNumber7.Name = "bNumber7";
            this.bNumber7.Size = new System.Drawing.Size(74, 75);
            this.bNumber7.TabIndex = 0;
            this.bNumber7.Text = "7";
            this.bNumber7.UseVisualStyleBackColor = false;
            this.bNumber7.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber8
            // 
            this.bNumber8.BackColor = System.Drawing.Color.White;
            this.bNumber8.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber8.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bNumber8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNumber8.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.bNumber8.Location = new System.Drawing.Point(476, 377);
            this.bNumber8.Name = "bNumber8";
            this.bNumber8.Size = new System.Drawing.Size(74, 75);
            this.bNumber8.TabIndex = 0;
            this.bNumber8.Text = "8";
            this.bNumber8.UseVisualStyleBackColor = false;
            this.bNumber8.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bNumber9
            // 
            this.bNumber9.BackColor = System.Drawing.Color.White;
            this.bNumber9.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber9.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bNumber9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNumber9.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.bNumber9.Location = new System.Drawing.Point(576, 377);
            this.bNumber9.Name = "bNumber9";
            this.bNumber9.Size = new System.Drawing.Size(74, 75);
            this.bNumber9.TabIndex = 0;
            this.bNumber9.Text = "9";
            this.bNumber9.UseVisualStyleBackColor = false;
            this.bNumber9.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // bOK
            // 
            this.bOK.BackColor = System.Drawing.Color.Transparent;
            this.bOK.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bOK.BackgroundImage")));
            this.bOK.FlatAppearance.BorderSize = 0;
            this.bOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bOK.Location = new System.Drawing.Point(907, 642);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(81, 112);
            this.bOK.TabIndex = 0;
            this.bOK.UseVisualStyleBackColor = false;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // lbl1
            // 
            this.lbl1.BackColor = System.Drawing.Color.Transparent;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(722, 478);
            this.lbl1.Margin = new System.Windows.Forms.Padding(0);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(218, 60);
            this.lbl1.TabIndex = 1;
            this.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // bClear
            // 
            this.bClear.BackColor = System.Drawing.Color.Transparent;
            this.bClear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bClear.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bClear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bClear.Location = new System.Drawing.Point(576, 479);
            this.bClear.Margin = new System.Windows.Forms.Padding(0);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(74, 74);
            this.bClear.TabIndex = 0;
            this.bClear.Tag = "0";
            this.bClear.UseVisualStyleBackColor = false;
            this.bClear.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // lblmsg
            // 
            this.lblmsg.AutoSize = true;
            this.lblmsg.BackColor = System.Drawing.Color.Transparent;
            this.lblmsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmsg.ForeColor = System.Drawing.Color.Green;
            this.lblmsg.Location = new System.Drawing.Point(710, 532);
            this.lblmsg.Name = "lblmsg";
            this.lblmsg.Size = new System.Drawing.Size(248, 20);
            this.lblmsg.TabIndex = 2;
            this.lblmsg.Text = "Please re-enter your Pin Code";
            this.lblmsg.Visible = false;
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            this.bBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBack.BackgroundImage")));
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBack.Location = new System.Drawing.Point(35, 642);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(79, 111);
            this.bBack.TabIndex = 3;
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // bNumber0
            // 
            this.bNumber0.BackColor = System.Drawing.Color.White;
            this.bNumber0.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.bNumber0.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.bNumber0.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.bNumber0.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.bNumber0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bNumber0.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.bNumber0.Location = new System.Drawing.Point(476, 478);
            this.bNumber0.Name = "bNumber0";
            this.bNumber0.Size = new System.Drawing.Size(74, 75);
            this.bNumber0.TabIndex = 0;
            this.bNumber0.Text = "0";
            this.bNumber0.UseVisualStyleBackColor = false;
            this.bNumber0.Click += new System.EventHandler(this.bNumeric_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 153);
            this.panel1.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "TIME";
            // 
            // button14
            // 
            this.button14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button14.BackgroundImage")));
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button14.Location = new System.Drawing.Point(928, 24);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(79, 81);
            this.button14.TabIndex = 2;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(7, 47);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(0, 55);
            this.lbltime.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(940, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 29);
            this.label2.TabIndex = 20;
            this.label2.Text = "help";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // PINReg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lblmsg);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.bNumber0);
            this.Controls.Add(this.bNumber9);
            this.Controls.Add(this.bNumber8);
            this.Controls.Add(this.bNumber7);
            this.Controls.Add(this.bNumber4);
            this.Controls.Add(this.bNumber5);
            this.Controls.Add(this.bNumber6);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bNumber3);
            this.Controls.Add(this.bNumber2);
            this.Controls.Add(this.bNumber1);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PINReg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PIN Registration";
            this.Load += new System.EventHandler(this.PINReg_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bNumber1;
        private System.Windows.Forms.Button bNumber2;
        private System.Windows.Forms.Button bNumber3;
        private System.Windows.Forms.Button bNumber6;
        private System.Windows.Forms.Button bNumber5;
        private System.Windows.Forms.Button bNumber4;
        private System.Windows.Forms.Button bNumber7;
        private System.Windows.Forms.Button bNumber8;
        private System.Windows.Forms.Button bNumber9;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Label lblmsg;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Button bNumber0;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
    }
}