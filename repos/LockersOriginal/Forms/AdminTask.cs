﻿using System;
using System.Windows.Forms;

namespace Lockers
{
    public partial class AdminTask : Form 
    {
        private SmartPay mpay=new SmartPay();

        //public AdminTask()
        //{

        //}
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        SmartPay smartpay;

        public AdminTask(SmartPay smartpay)
        {
            InitializeComponent();
            this.smartpay = smartpay;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TroubleshootLocker troubleshoot = new TroubleshootLocker();
            troubleshoot.BringToFront();
            troubleshoot.TopMost = true;

            troubleshoot.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SetupPaysystem setUpPaySystem = new SetupPaysystem( smartpay  );
            setUpPaySystem.BringToFront();
            setUpPaySystem.TopMost = true;
            setUpPaySystem.ShowDialog();
            setUpPaySystem = null;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RestartTerminal restartterminal = new RestartTerminal();
            restartterminal.BringToFront();
            restartterminal.TopMost = true;
            restartterminal.ShowDialog();

        }

        private void AdminTask_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
            this.BackgroundImage = ConfigurationData.GetBackground("Blank");
        }
    }
}
