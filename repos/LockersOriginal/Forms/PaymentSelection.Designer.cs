﻿namespace Lockers
{
    partial class PaymentSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PaymentSelection));
            this.button2 = new System.Windows.Forms.Button();
            this.bCreditPay = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.bCashPay = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.bBack = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.lbltime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbldue = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(377, 270);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(114, 112);
            this.button2.TabIndex = 0;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Visible = false;
            // 
            // bCreditPay
            // 
            this.bCreditPay.BackColor = System.Drawing.Color.Transparent;
            this.bCreditPay.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bCreditPay.BackgroundImage")));
            this.bCreditPay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bCreditPay.FlatAppearance.BorderSize = 0;
            this.bCreditPay.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bCreditPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCreditPay.Location = new System.Drawing.Point(66, 270);
            this.bCreditPay.Name = "bCreditPay";
            this.bCreditPay.Size = new System.Drawing.Size(114, 112);
            this.bCreditPay.TabIndex = 0;
            this.bCreditPay.UseVisualStyleBackColor = false;
            this.bCreditPay.Click += new System.EventHandler(this.bCreditPay_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Transparent;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Location = new System.Drawing.Point(688, 397);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(114, 112);
            this.button3.TabIndex = 0;
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Transparent;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(532, 270);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(114, 112);
            this.button4.TabIndex = 0;
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Visible = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(377, 397);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(114, 112);
            this.button5.TabIndex = 0;
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Visible = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Transparent;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(66, 397);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(114, 112);
            this.button6.TabIndex = 0;
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Visible = false;
            // 
            // bCashPay
            // 
            this.bCashPay.BackColor = System.Drawing.Color.Transparent;
            this.bCashPay.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bCashPay.BackgroundImage")));
            this.bCashPay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bCashPay.FlatAppearance.BorderSize = 0;
            this.bCashPay.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bCashPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bCashPay.Location = new System.Drawing.Point(221, 270);
            this.bCashPay.Name = "bCashPay";
            this.bCashPay.Size = new System.Drawing.Size(114, 112);
            this.bCashPay.TabIndex = 0;
            this.bCashPay.UseVisualStyleBackColor = false;
            this.bCashPay.Click += new System.EventHandler(this.bCashPay_Click);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Transparent;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Location = new System.Drawing.Point(221, 397);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(114, 112);
            this.button8.TabIndex = 0;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Visible = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Transparent;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Location = new System.Drawing.Point(532, 397);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(114, 112);
            this.button9.TabIndex = 0;
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Visible = false;
            // 
            // bBack
            // 
            this.bBack.BackColor = System.Drawing.Color.Transparent;
            this.bBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bBack.BackgroundImage")));
            this.bBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bBack.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bBack.FlatAppearance.BorderSize = 0;
            this.bBack.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.bBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bBack.Location = new System.Drawing.Point(35, 642);
            this.bBack.Name = "bBack";
            this.bBack.Size = new System.Drawing.Size(79, 111);
            this.bBack.TabIndex = 2;
            this.bBack.UseVisualStyleBackColor = false;
            this.bBack.Click += new System.EventHandler(this.bBack_Click);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Transparent;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Location = new System.Drawing.Point(688, 270);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(114, 112);
            this.button11.TabIndex = 0;
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Visible = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Transparent;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Location = new System.Drawing.Point(844, 270);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(114, 112);
            this.button12.TabIndex = 0;
            this.button12.UseVisualStyleBackColor = false;
            this.button12.Visible = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Transparent;
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Location = new System.Drawing.Point(844, 397);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(114, 112);
            this.button13.TabIndex = 0;
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Visible = false;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.lbltime);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1023, 150);
            this.panel1.TabIndex = 24;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "TIME";
            // 
            // button14
            // 
            this.button14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button14.BackgroundImage")));
            this.button14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.button14.Location = new System.Drawing.Point(928, 24);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(79, 81);
            this.button14.TabIndex = 2;
            this.button14.UseVisualStyleBackColor = true;
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.BackColor = System.Drawing.Color.Transparent;
            this.lbltime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lbltime.ForeColor = System.Drawing.Color.White;
            this.lbltime.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbltime.Location = new System.Drawing.Point(7, 47);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(225, 55);
            this.lbltime.TabIndex = 1;
            this.lbltime.Text = "01:22 pm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(940, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 29);
            this.label2.TabIndex = 20;
            this.label2.Text = "help";
            // 
            // lbldue
            // 
            this.lbldue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbldue.BackColor = System.Drawing.Color.Transparent;
            this.lbldue.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldue.ForeColor = System.Drawing.Color.White;
            this.lbldue.Location = new System.Drawing.Point(821, 531);
            this.lbldue.Name = "lbldue";
            this.lbldue.Size = new System.Drawing.Size(170, 77);
            this.lbldue.TabIndex = 25;
            this.lbldue.Text = "0.00";
            this.lbldue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // PaymentSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.Controls.Add(this.lbldue);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bBack);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.bCashPay);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.bCreditPay);
            this.Controls.Add(this.button2);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PaymentSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PaymentSelection";
            this.Load += new System.EventHandler(this.PaymentSelection_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bCreditPay;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button bCashPay;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button bBack;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbldue;
        private System.Windows.Forms.Timer timer1;
    }
}