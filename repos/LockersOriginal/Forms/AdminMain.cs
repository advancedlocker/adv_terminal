﻿using System;
using System.Windows.Forms;

namespace Lockers

{
    public partial class AdminMain : Form  
    {

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        SmartPay smartpay;

        //public AdminMain()
        //{
        //    InitializeComponent();
        //}

        public AdminMain(SmartPay smartpay) 
        {
            InitializeComponent();
            this.smartpay = smartpay;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AdminMain_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }
            this.BackgroundImage = ConfigurationData.GetBackground("Blank");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AdminTask admintask = new AdminTask(smartpay );
            admintask.ShowDialog();
            admintask = null;


        }
    }
}
