﻿namespace Lockers
{

    using System;
    using System.Data;
    using System.Drawing;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Forms;

    public partial class TimeDaySelection : Form
    {
        private delegate void ComputeDelegate(int value1, int value2);
        private ComputeDelegate computeDelegate = null;

        int timeDays = 1;
        DateTimeFormatInfo fi = new DateTimeFormatInfo();
        decimal mRate = Properties.Settings.Default.rate;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;  // Turn on WS_EX_COMPOSITED
                return cp;
            }
        }

        public TimeDaySelection()
        {
            InitializeComponent();

            String ImageFilename = Properties.Settings.Default.ProjectRoot + Properties.Settings.Default.ImagesDirectory + Globals.CurrentLanguage + "\\Select_Duration_D.tiff";
            this.BackgroundImage = Image.FromFile(ImageFilename);

            Globals.LockData.LockerDatabaseError += LockData_LockerDataError;
            this.computeDelegate = new ComputeDelegate(ComputeCost);



            fi.AMDesignator = "am";
            fi.PMDesignator = "pm";
            lbltime.Text = DateTime.Now.ToString("hh:mm tt", fi);

            //mRate = Globals.LockData.Locker_UPrice();
            int lockersize = (int)Globals.LockData.Locker_Size;
            var mvarrate = (from Rows in Globals.LockData.Lockers.AsEnumerable()
                            where Rows.Field<string>("lockersize") == lockersize.ToString()
                            select new
                            {
                                RateLock = Rows.Field<decimal>("uprice")

                            }).First();
            mRate = mvarrate.RateLock;
            lblrate.Text = mRate.ToString("#,##0.00") + " Per day";

            timer1.Start();
        }

        private void LockData_LockerDataError(object sender, LogMessageEventArgs e)
        {
            MessageBox.Show(e.Messsage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void TimeDaySelection_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.AllowWindowFocusChange)
            {
                this.AutoValidate = AutoValidate.EnableAllowFocusChange;
                this.TopMost = false;
            }
            else
            {
                this.AutoValidate = AutoValidate.EnablePreventFocusChange;
                this.TopMost = true;
            }

            switch (Globals.LockData.Locker_Size)
            {
                case LockerSize.SMALL:
                    lblsize.Text = "SMALL";
                    break;
                case LockerSize.MEDIUM:
                    lblsize.Text = "MEDIUM";
                    break;
                case LockerSize.LARGE:
                    lblsize.Text = "LARGE";
                    break;
                case LockerSize.XLARGE:
                    lblsize.Text = "X-LARGE";
                    break;
            }

            UpdateDisplay();
        }

        private void UpdateDisplay()
        {
            txtDays.Text = timeDays.ToString();
            this.Invoke(this.computeDelegate, new object[] { timeDays, 0});
        }

        private void ComputeCost(int hrs, int min)
        {
            decimal m_price = mRate;
            decimal totalhrs = hrs + ((decimal)min / 60);
            decimal cost = totalhrs * m_price;
            lblCost.Text = cost.ToString("#,##0.00");
            Globals.DueAmnt = (double)Math.Round(cost, 2);
            Globals.LockData.COST = (decimal)Math.Round(cost, 2);
            Globals.LockData.Hours = hrs;
            Globals.LockData.Minutes = min;
        }

        private void bAddDays_Click(object sender, EventArgs e)
        {
            timeDays++;

            UpdateDisplay();
        }

        private void bDaysMinus_Click(object sender, EventArgs e)
        {
            if(timeDays > 1)
                timeDays--;

            UpdateDisplay();
        }

		private void lblrate_Click(object sender, EventArgs e)
		{
		}

		private void bBack_Click(object sender, EventArgs e)
        {
            Globals.AbortForm = true;
            timer1.Stop();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

    }
}
