﻿using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LockerTester
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		HubConnection connection;

		public MainWindow()
		{
			InitializeComponent();

			connection = new HubConnectionBuilder()
				.WithUrl("http://monitor.advancedlocker.com/lockerHub")
				//.WithUrl("http://localhost:5000/lockerHub")
				.Build();

			connection.Closed += async (error) =>
			{
				await Task.Delay(new Random().Next(0, 5) * 1000);
				messagesList.Items.Add("Connection stopped");
				await connection.StartAsync();
			};
		}

		private async void Button_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				connection.On<string>("LockLocker", (locker) =>
				{
					this.Dispatcher.Invoke(() =>
					{
						var newMessage = $"lock {locker}";
						messagesList.Items.Add(newMessage);
					});
				});

				connection.On<string>("OpenLocker", (locker) =>
				{
					this.Dispatcher.Invoke(() =>
					{
						var newMessage = $"open {locker}";
						messagesList.Items.Add(newMessage);
					});
				});

				connection.On<string>("DisableLocker", (locker) =>
				{
					this.Dispatcher.Invoke(() =>
					{
						var newMessage = $"disable {locker}";
						messagesList.Items.Add(newMessage);
					});
				});

				connection.On<string>("EnableLocker", (locker) =>
				{
					this.Dispatcher.Invoke(() =>
					{
						var newMessage = $"enable {locker}";
						messagesList.Items.Add(newMessage);
					});
				});

				connection.On<string,string>("ChangeLockerPin", (locker, pin) =>
				{
					this.Dispatcher.Invoke(() =>
					{
						var newMessage = $"change pin {locker} {pin}";
						messagesList.Items.Add(newMessage);
					});
				});

				connection.On<string,string>("ReassignHire", (locker, newLockerId) =>
				{
					this.Dispatcher.Invoke(() =>
					{
						var newMessage = $"reassign {locker} to {newLockerId}";
						messagesList.Items.Add(newMessage);
					});
				});

				connection.On<string>("SendData", (locker) =>
				{
					this.Dispatcher.Invoke(() =>
					{
						var newMessage = $"data send requested {locker}";
						messagesList.Items.Add(newMessage);
					});
				});


				await connection.StartAsync();
				messagesList.Items.Add("Connection started");

				await connection.InvokeAsync("RegisterNodes", new List<string> { "test" });
				await connection.InvokeAsync("RegisterKiosk", "Test-1" );

				messagesList.Items.Add("Test node registered");
			}
			catch (Exception ex)
			{
				messagesList.Items.Add(ex.Message);
			}
		}

		private async void Button_Click_1(object sender, RoutedEventArgs e)
		{
			try
			{
				var data = new JObject();
				data["isOpen"] = true;
				data["isRented"] = true;
				data["isEnabled"] = true;
				//data["error"] = "Exploded"
				var jsonString = data.ToString();
				await connection.InvokeAsync("SendNodeDataToMonitors", "test", jsonString);
			}
			catch (Exception ex)
			{
				messagesList.Items.Add(ex.Message);
			}
		}

		private async void Button_Click_2(object sender, RoutedEventArgs e)
		{
			try
			{
				var data = new JObject();
				var errorList = new List<string> { "Test 1", "Test 2" };
				data["powerErrors"] = JArray.FromObject(errorList);
				data["processorErrors"] = JArray.FromObject(errorList);
				data["hopperErrors"] = JArray.FromObject(errorList);
				data["payoutErrors"] = JArray.FromObject(errorList);
				data["creditCardErrors"] = JArray.FromObject(errorList);
				data["rfidErrors"] = JArray.FromObject(errorList);
				data["doorGatewayErrors"] = JArray.FromObject(errorList);
				data["doorControllerErrors"] = JArray.FromObject(errorList);
				data["cameraErrors"] = JArray.FromObject(errorList);
				var jsonString = data.ToString();
				await connection.InvokeAsync("SendKioskDataToMonitors", "Test-1", jsonString);
			}
			catch (Exception ex)
			{
				messagesList.Items.Add(ex.Message);
			}
		}
	}
}
