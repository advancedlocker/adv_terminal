@echo off
setlocal
set file="adv_mon.txt"
REM set appn="C:\Progra~1\Checki~1\motelk~1\MotelBs.exe"
set appn="C:\Projects\Terminal\Lockers\Lockers\bin\Debug\Lockers.exe"
set maxbytesize=100

:BEGIN

del adv_mon.txt
tasklist.exe /fi "ImageName eq Lockers.exe" > adv_mon.txt

FOR /F "usebackq" %%A IN ('%file%') DO set size=%%~zA

if %size% LSS %maxbytesize% (
	echo.starting Aplication
	date /T >> adv_crash_date.txt
	time /T >> adv_crash_date.txt
	start C:\Projects\Terminal\Lockers\Lockers\bin\Debug\Lockers.exe
)

sleep 5

goto BEGIN